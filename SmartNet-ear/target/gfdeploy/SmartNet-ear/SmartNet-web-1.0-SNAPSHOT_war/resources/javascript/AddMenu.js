/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload=function init(){
    
    PF('layout').hide('east');
    
    PF('layout').hide('west');
    
   
}

function showConfirmDialog(header,body){
    
    PF('dialog').jq.find('.ui-dialog-title').text(header);
    
    PF('dialog').jq.find('.ui-dialog-content').text(body);
    
    PF('dialog').show();
    
    
}


function showDialog(){
    
    PF('confirmDialog').jq.find('.ui-dialog-title').text('Delete Addition');
    
    PF('confirmDialog').jq.find('.ui-dialog-content').text('Are you sure you want to delete this addition?');
    
    PF('confirmDialog').show();
    
}