/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var geocoder=new google.maps.Geocoder();


window.onload=function init(){
   
    var gpsColumn=$(PrimeFaces.escapeClientId('form:gpsColumn'));
    
    PF('map').jq.width(gpsColumn.width());
    
    PF('map').jq.height(gpsColumn.height());
    
   console.log(gpsColumn.width());
    
    changeMapCenter();
    
    google.maps.event.trigger(PF('map').getMap(), 'resize');
    
   
    
};


var coordinates;

function changeMapCenter(){
    
    if(coordinates==null)
    
    {
    
       if(navigator.geolocation){
       
       navigator.geolocation.getCurrentPosition(function(pos){
           
               coordinates={'latitude':pos.coords.latitude,'longitude':pos.coords.longitude};
           
          PF('map').getMap().setCenter(new google.maps.LatLng(coordinates.latitude, coordinates.longitude));
          
  
          var marker = new google.maps.Marker({
              
              position : {lat:coordinates.latitude,lng:coordinates.longitude},
              
              map : PF('map').getMap(),
              
              icon : "/images/person.png"
              
          });
  
              console.log("Coordinates is null");

                
       },function(){
           
           handleLocationError(true);
           
       });
       
   }
   else{
       
       handleLocationError(false);
       
   }
  
    
    }
    
    else{
        
          console.log("Coordinates is not null");
        
          PF('map').getMap().setCenter(new google.maps.LatLng(coordinates.latitude,coordinates.longitude));
       
         var marker = new google.maps.Marker({
              
              position : {lat:coordinates.latitude,lng:coordinates.longitude},
              
              map : PF('map').getMap(),
              
              icon : "/images/person.png"
              
          });
        
    }
    
    document.getElementById("latitude").value = coordinates.latitude;
    
    document.getElementById("longitude").value = coordinates.longitude;


}

function handleLocationError(browseHasGeolocation){
    
    if(browseHasGeolocation){
        
        alert("Error the geolocation service failed");
        
    }
    else{
        
        alert("Error your browser does not support geolocation");
        
    }
    
}

/*

google.maps.event.addDomListener(window,'load',function(){
    
    
 

   
   if(navigator.geolocation){
       
       navigator.geolocation.getCurrentPosition(function(pos){
           
          PF('map').getMap().setCenter(new google.maps.LatLng(parseFloat(pos.coords.lat,pos.coords.lng)));
          
                
       },function(){
           
           handleLocationError(true);
           
       });
       
   }
   else{
       
       handleLocationError(false);
       
   }
  
    
    google.maps.event.trigger(PF('map').getMap(), 'resize');
    
});

function handleLocationError(browseHasGeolocation){
    
    if(browseHasGeolocation){
        
        alert("Error the geolocation service failed");
        
    }
    else{
        
        alert("Error your browser does not support geolocation");
        
    }
    
}*/

function update(){

    var gpsColumn=$(PrimeFaces.escapeClientId('form:gpsColumn'));
    
    PF('map').jq.width(gpsColumn.width());
    
    PF('map').jq.height(gpsColumn.height());

    
    changeMapCenter();
    
    google.maps.event.trigger(PF('map').getMap(), 'resize');  
    
     console.log("UPDATING");
    
}