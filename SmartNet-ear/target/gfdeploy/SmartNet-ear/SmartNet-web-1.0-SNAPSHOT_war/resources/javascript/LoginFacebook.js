
//new script

window.fbAsyncInit = function() {
    FB.init({
        appId: '686394994789028',
        status: true,
        cookie: true,
        xfbml: true,
        oauth: true
    });

    FB.Event.subscribe('auth.login', function(response) {
        // {
        //   status: "",         /* Current status of the session */
        //   authResponse: {          /* Information about the current session */
        //      userID: ""          /* String representing the current user's ID */
        //      signedRequest: "",  /* String with the current signedRequest */
        //      expiresIn: "",      /* UNIX time when the session expires */
        //      accessToken: "",    /* Access token of the user */
        //   }
        // }
        
        // alert('event status: ' + response.status);
    });

    FB.getLoginStatus(function(response) {
        //  {
        //     status: 'connected',
        //     authResponse: {
        //        accessToken: '...',
        //        expiresIn:'...',
        //        signedRequest:'...',
        //        userID:'...'
        //     }
        //  }
        
        //alert('getLoginStatus: ' + response.status);
        
        if (response.status=='connected') {
            FB.api('/me',function(response){
                alert('Welcome back ' + response.name);
            });
        }
    });

};
(function(d) {
    var js, id = 'facebook-jssdk';
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    d.getElementsByTagName('head')[0].appendChild(js);
}(document));