/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var jsonObject;

function removeImage(path){
    
    if(jsonObject==null){
        
        jsonObject={'identifier':'edenIdentifier','values':[]};
        
    }
    
    if(checkExistence('removePath')){
        
        jsonObject.values[index('removePath')].value=path;
        
    }
    else{
        
      var cache={'id':'removePath','value':path};  
      
      jsonObject.values.push(cache);
        
    }
    
    document.getElementById("hiddenPath").value=jsonObject.values[index('removePath')].value;
   
    console.log(document.getElementById('hiddenPath').value);
    
    showDialog();
    
}

function showDialog(){
    
    PF('confirmDialog').jq.find('.ui-dialog-title').text("Delete Image");
    
    PF('confirmDialog').jq.find('.ui-dialog-content').text("Are you sure you want to delete "+document.getElementById('hiddenPath').value+'?');
    
    PF('confirmDialog').show();
}

function checkExistence(id){
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return true;
            
        }
        
    }
    
    return false;
    
}

function index(id){
    
    for(var key in jsonObject.values){
        
        
        if(jsonObject.values[key].id==id){
            
            return key;
            
        }
        
    }
    
    return 0;
}