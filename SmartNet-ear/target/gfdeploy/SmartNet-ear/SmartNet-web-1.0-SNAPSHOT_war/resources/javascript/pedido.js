/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var jsonObject;

window.onload=function init(){
    
    PF('layout').hide('east');
    
    PF('layout').hide('west');
    
    
    $("#pagosOnline").on("click",function(){
        
     document.getElementById("amount").value=document.getElementById("form:price").value;  
        
    });
    
    if(jsonObject==null){
        
        
        jsonObject={'identifier':'eden','values':[]};
        
    }

if(checkExistence('height')){
  
        jsonObject.values[index('height')].value=PF('layout').layout.state.center.innerHeight;
    
}
else{
    
    var cache={'id':'height','value':PF('layout').layout.state.center.innerHeight};
    
    jsonObject.values.push(cache);
    
}

if(checkExistence('width')){
    
    jsonObject.values[index('width')].value=PF('layout').layout.state.center.innerWidth*.9;
    
}
else{
    
    var cache={'id':'width','value':PF('layout').layout.state.center.innerWidth*.9};
    
    jsonObject.values.push(cache);
    
}

alert(jsonObject.values[index('height')].value+"-"+jsonObject.values[index('width')].value);

PF('map').jq.width(jsonObject.values[index('width')].value);

PF('map').jq.height(jsonObject.values[index('height')].value);
 
// alert("Price "+document.getElementById("form:price").value);
    
  //  alert("pedido initialized");
    
    
    
   // PF('central').jq.css('width',width);
    
    //PF('central').jq.css('height',height);
    
    //alert("Central Width "+PF('central').jq.width());
    
  
    
    var pedidoOperation=document.getElementById("form:operation").value;
    
    
    initialize();
    
    google.maps.event.trigger(PF('map').getMap(),'resize');
    
    $('table','#form').each(function(){
        
       if($(this).attr('id').indexOf('mainPanelGrid')!=-1){
           
           $(this).css('vertical-align','top');
           
       }
        
    });
}



var geocoder;
var map;
var marker;
var markers = [];
var infowindow = new google.maps.InfoWindow();
var addresss;

var city;

function initialize() {
    
    
  //   alert("initializing");
     
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
 
  marker = new google.maps.Marker({  
    map: PF('map').getMap(),
    zoom: 15,
    draggable:true,
});

 
 
 map =PF('map').getMap();
 

 google.maps.event.addListener(marker, 'dragend', function() {
  
 
    
 document.getElementById("latitudeValue").value = marker.getPosition().lat().toLocaleString();
 document.getElementById("longitudeValue").value = marker.getPosition().lng().toLocaleString();

codeLatLng(marker.getPosition().lat().toLocaleString()+","+marker.getPosition(). lng().toLocaleString());

setMapCenter(parseFloat(marker.getPosition().lat()), parseFloat(marker.getPosition().lng()));
 


  });
 
 
 if (navigator.geolocation) {
     
        checkGeolocationByHTML5();
    
 } 
    else {
        
        checkGeolocationByLoaderAPI(); // HTML5 not supported! Fall back to Loader API.
    
 }
    } 
    
    
    
    function checkGeolocationByHTML5() {
        
        map=PF('map').getMap();
        
       navigator.geolocation.getCurrentPosition(function(position) {
           
  PF('map').getMap().setCenter(new google.maps.LatLng(parseFloat(position.coords.latitude.toLocaleString()) , parseFloat(position.coords.longitude.toLocaleString())));
     
 
     
         
marker.setMap(PF('map').getMap());
  
marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
  
marker.setTitle("Your Locaton");
     
document.getElementById("latitudeValue").value=position.coords.latitude;

document.getElementById("longitudeValue").value=position.coords.longitude;
 
 codeLatLng(marker.getPosition().lat().toLocaleString()+","+marker.getPosition(). lng().toLocaleString());

 

      
   
        }, function() {
            checkGeolocationByLoaderAPI(); // Error! Fall back to Loader API.
        });
        
    }
    
    
    
    
     function checkGeolocationByLoaderAPI() {
        if (google.loader.ClientLocation) {
        // setMapCenter(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
        } else {
            // Unsupported! Show error/warning?
        }
    }
    
    function setMapCenter(latitude, longitude) {
        
          PF('map').getMap().setCenter(new google.maps.LatLng(latitude, longitude));
        
    }
    
    
    
    
    
    
    
function codeAddress() {
 
 
var address= PF('tipo').getSelectedValue()+" "+PF('primerDigito').jq.val()+"#"+PF('segundoDigito').jq.val()+"-"+PF('tercerDigito').jq.val()+" "+document.getElementById("form:hiddenCity").value+","+document.getElementById("form:hiddenState").value+",Colombia";



map=  PF('map').getMap();

geocoder = new google.maps.Geocoder(); 

  //var address = document.getElementById("formulario:hiddenDir").value;
  
  geocoder.geocode( { 'address': address}, function(results, status) {
      
    if (status == google.maps.GeocoderStatus.OK) {     
    
  //PF('w_gmap').getMap().setCenter(results[0].geometry.location);
  
  marker.setPosition(results[0].geometry.location);
  
  marker.setMap(PF('map').getMap());
   
  //alert(results[0].geometry.location.lat().toLocaleString());
   
 document.getElementById("latitudeValue").value=results[0].geometry.location.lat().toLocaleString();
            
 document.getElementById("longitudeValue").value=results[0].geometry.location.lng().toLocaleString();

        
 setMapCenter(parseFloat(results[0].geometry.location.lat()),parseFloat(results[0].geometry.location.lng()))         
 infowindow.setContent(address);
 infowindow.open(PF('map').getMap(), marker);
     
       
    }
    else {
        
      alert('Geocode was not successful for the following reason: ' + status);
      
    }
  });
  
}


function getReverseGeocodingData(lat, lng) {
    
  //  alert('GetReverseGeocoding');
    
    var latlng = new google.maps.LatLng(lat, lng);
    
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
           console.log(results);
           addresss = (results[0].address_components);
        }
    });
}

    function checkGeolocationByLoaderAPI() {
        if (google.loader.ClientLocation) {
            setMapCenter(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
        } else {
            // Unsupported! Show error/warning?
        }
    }


function codeLatLng(input) {
 
 
  var latlngStr = input.split(',', 2);
  //Latitude
  var lat = parseFloat(latlngStr[0]);
  //Longitude
  var lng = parseFloat(latlngStr[1]);
  
  var latlng = new google.maps.LatLng(lat, lng);
  
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
     var result = results[0];
     
         city = "";
         var state = "";
         var street_number="";
         var country;
         
for(var i=0, len=result.address_components.length; i<len; i++) {
    
	var ac = result.address_components[i];
        
  	if(ac.types.toLocaleString()=="locality,political"){ city = ac.long_name;}
	if(ac.types.toLocaleString()=="administrative_area_level_1,political") {state = ac.long_name;}
        if(ac.types.toLocaleString()=="country,political") {street_number = ac.long_name;}

if(ac.types.indexOf("country")>=0){
    
    country=ac.long_name;
    
}

}

//document.getElementById("city").value=city;




 var address = results[0].formatted_address.split(',', 1);
 
 
              //divide by white spaces
 var sections=address.toLocaleString().split(" ");
 var type="";
 var primer_digito="";
 var segundo_digito="";
 var tercer_digito="";
 
 for(var i=0,len=sections.length;i<len;i++){
     
     var flag=0;
     
     
     
     if(sections[i].toLocaleString().indexOf('-')!=-1){
      
     // alert("it contains -");
      
      var filter= sections[i].toLocaleString().split('-');
     
                      //alert("filtrado "+filter[0]);
      
     if(type==""){
    
        type=filter[0];
        flag==1;
    }
    
    
    if(sections[i].toLocaleString().indexOf('0')!=-1 || sections[i].toLocaleString().indexOf('1')!=-1 || sections[i].toLocaleString().indexOf('2')!=-1 || sections[i].toLocaleString().indexOf('3')!=-1 || sections[i].toLocaleString().indexOf('4')!=-1 || sections[i].toLocaleString().indexOf('5')!=-1 || sections[i].toLocaleString().indexOf('6')!=-1 || sections[i].toLocaleString().indexOf('7')!=-1  || sections[i].toLocaleString().indexOf('8')!=-1 || sections[i].toLocaleString().indexOf('9')!=-1){
    
   
    
  if(primer_digito=="" && flag==0){
        
   primer_digito= filter[0];
   
   primer_digito=primer_digito.replace("#","");
   
   flag=1;
    }
    
    if(segundo_digito=="" && flag==0){
        
        segundo_digito=filter[0];
        
        segundo_digito=segundo_digito.replace("#","");
        
        flag=1;
    }
    
    if(tercer_digito=="" && flag==0){
        if(filter.length<2){
            
      tercer_digito=filter[0];
      
      tercer_digito=tercer_digito.replace("#","");
      
      flag=1;
  }
  else{
     tercer_digito=filter[1];
      flag=1; 
      
  }
        
    }
         
         
         
     }
 }
                  
    else if(sections[i].toLocaleString().indexOf('#')==-1){
     
    if(type==""){
       // alert("type is null");
        type=sections[i];
        flag=1;
    }
    
    
    if(sections[i].toLocaleString().indexOf('0')!=-1 || sections[i].toLocaleString().indexOf('1')!=-1 || sections[i].toLocaleString().indexOf('2')!=-1 || sections[i].toLocaleString().indexOf('3')!=-1 || sections[i].toLocaleString().indexOf('4')!=-1 || sections[i].toLocaleString().indexOf('5')!=-1 || sections[i].toLocaleString().indexOf('6')!=-1 || sections[i].toLocaleString().indexOf('7')!=-1  || sections[i].toLocaleString().indexOf('8')!=-1 || sections[i].toLocaleString().indexOf('9')!=-1){
    
   
    
    if(primer_digito=="" && flag==0){
        
   primer_digito= sections[i];  
   
   primer_digito=primer_digito.replace("#","");
   
   flag=1;
    }
    
    if(segundo_digito=="" && flag==0){
        segundo_digito=sections[i];
        
        segundo_digito=segundo_digito.replace("#","");
        
        flag=1;
    }
    
    if(tercer_digito=="" && flag==0){
      tercer_digito=  sections[i];
      
      tercer_digito=tercer_digito.replace("#","");
      
        flag=1;
    }
    
                      
                      
    }
    
     }
     
     else if(sections[i].toLocaleString().indexOf('#')!=-1){
         
      
     }
    
    
 }
 
 var selected="";
 
 var coincidence=0;

 
 $("select[name='form:tipo_input'] option").each(function(){
     
 if(similar(type,$(this).val().toLocaleString())>coincidence){
     
     coincidence=similar(type,$(this).val().toLocaleString());
     
     selected=$(this).val().toLocaleString();
     
 }
     
 });

 
 
 PF('tipo').selectValue(selected);
 
 selected="";
 
 coincidence=0;
 
 $("select[name='form:state_input'] option").each(function(){
     
    if(similar(state,$(this).val().toLocaleString())>coincidence){
        
        coincidence=similar(state,$(this).val().toLocaleString());
        
        selected=$(this).val().toLocaleString();
        
    }
     
 });
 
 PF('primerDigito').jq.val(primer_digito);  
 PF('segundoDigito').jq.val(segundo_digito);
 PF('tercerDigito').jq.val(tercer_digito);
 
 marker.setMap(PF('map').getMap());
 marker.setPosition(latlng);
 marker.setTitle("Current Position");
 
document.getElementById("HiddenCity").value=city;

document.getElementById("HiddenState").value=state;

document.getElementById("form:hiddenCity").value=city;

document.getElementById("form:hiddenState").value=state;
         
   infowindow.setContent(PF('tipo').getSelectedValue()+" "+PF('primerDigito').jq.val()+"#"+PF('segundoDigito').jq.val()+"-"+PF('tercerDigito').jq.val()+" "+city+","+state);
        infowindow.open(PF('map').getMap(), marker);
        
PF('state').selectValue(selected);
PF('city').selectValue(city);



      } else {
        alert('No results found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}

function checkExistence(id){

 
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return true;
            
        }
        
    }
    
    return false;
    
}

function index(id){
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return key;
            
        }
        
    }
    return 0;
}

function submitForm(){
    
  document.getElementById("latitude").value=document.getElementById("latitudeValue").value;
    
   document.getElementById("longitude").value= document.getElementById("longitudeValue").value;
    
   document.getElementById("form").submit();
    
}

function similar(a,b) {
    var lengthA = a.length;
    var lengthB = b.length;
    var equivalency = 0;
    var minLength = (a.length > b.length) ? b.length : a.length;    
    var maxLength = (a.length < b.length) ? b.length : a.length;    
    for(var i = 0; i < minLength; i++) {
        if(a[i] == b[i]) {
            equivalency++;
        }
    }


    var weight = equivalency / maxLength;
    return (weight * 100);
}

function submitform(){
    
increment();
    /*
    
    document.getElementById("segundoDigito").value=PF('segundoDigito').jq.val();
    
    document.getElementById("tercerDigito").value=PF('tercerDigito').jq.val();
    
    document.getElementById("latitude").value=document.getElementById("form:latitudeValue").value;
    
    document.getElementById("longitude").value=document.getElementById("form:longitudeValue").value;
    
    alert(PF('primerDigito').jq.val());
    */
    
}

function submitform() {
            PrimeFaces.ajax.AjaxRequest('/ui/bewertung.jsf', 
                  { formId : 'form',         
                    source: 'form',
                    process: 'form'
                  });
                 
}