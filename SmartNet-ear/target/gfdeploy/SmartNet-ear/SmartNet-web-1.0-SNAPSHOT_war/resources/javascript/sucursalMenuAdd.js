/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 var jsonObject;

var AddMenu;

window.onload=function init(){
   
    PF('layout').hide('west');
    
    PF('layout').hide('east');
    
   resize();
    
    
}


function checkExistence(id){
    
    for(var key in jsonObject.values)
    {
        
        if(jsonObject.values[key].id==id){
            
            return true;
            
        }
        
    }
    return false;
    
}

function index(id){
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return key;
            
        }
        
    }
    
    return 0;
}

function addMenu(idmenu){
    
    
    
    if(!checkExistence('menuId')){
      
        var cache={'id':'menuId','value':idmenu};
        
        jsonObject.values.push(cache);
        
    }
    
    else{
        
     jsonObject.values[index('menuId')].value=idmenu;   
        
    }
    
   document.getElementById("hiddenIdMenu").value=jsonObject.values[index('menuId')].value;
    
}

function resize(){
    
    
    $("#form").children().each(function(){
        
        
        if($(this).attr('id')!=null){
            
          if($(this).attr('id').indexOf('mainPanelGrid')!=-1){
              
            if(jsonObject==null){
                
                jsonObject={'identifier':'eden','values':[]};
                
            }
            
            if(!checkExistence('width')){
                
               var cache={'id':'width','value':$(this).width()};
               
               jsonObject.values.push(cache);
                
            }
            else{
                
                jsonObject.values[index('width')].value=$(this).width();
                
            }
              
          }
            
        }
        
    });
    
    PF('productGrid').jq.width((jsonObject.values[index('width')].value)/2);
    
    PF('selectedTable').jq.width((jsonObject.values[index('width')].value)/2);
    
}