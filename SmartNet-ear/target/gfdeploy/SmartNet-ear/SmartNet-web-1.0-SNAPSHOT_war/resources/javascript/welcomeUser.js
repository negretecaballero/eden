/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var navigator;

var jsonObject;

var marker;

function initialize()
{
    
    if(jsonObject==null){
        
           jsonObject={'identifier':'eden','values':[]};
        
    }

navigator= new google.maps.Geocoder();
  
  if(navigator.geolocation){
   
        navigator.geolocation.getCurrentPosition(
                function(position){
                 
                      
                    
            if(checkExistence('latitude')){
                
                jsonObject.values[index('latitude')].value=parseFloat(position.coords.latitude.toLocaleString());
                
            }
            
           
            
            else{
                
                var latitude={'id':'latitude','value':parseFloat(position.coords.latitude.toLocaleString())};
                
                jsonObject.values.push(latitude);
                
              
                
            }
            
            if(checkExistence('longitude')){
                
                jsonObject.values[index('longitude')].value=parseFloat(position.coords.longitude.toLocaleString());
                
            }
            else{
                
           
                
               var longitude={'id':'longitude','value':parseFloat(position.coords.longitude.toLocaleString())}; 
               
               jsonObject.values.push(longitude);
               
               
                
            }
              var latlng=new google.maps.LatLng(parseFloat(position.coords.latitude),parseFloat(position.coords.longitude));     
                   
              document.getElementById('latitude').value=position.coords.latitude.toLocaleString();
              
              document.getElementById('longitude').value=position.coords.longitude.toLocaleString();
                  
              alert("Map Loaded");
                     
                },function(){
                    
                    alert("Error Geolocating");
                    
                }
                
                );
      
  }
  else{
      
      alert('Yout browser does not support geolocation');
      
  }
  

 
        
  
}
  
  
  
google.maps.event.addDomListener(window, 'load', initialize);

window.onload=function init(){


   
   PF('layout').hide('west');
   
   PF('layout').hide('east');

  
  if (jsonObject==null){
      
      jsonObject={'identifier':'eden','values':[]};
      
  }
  
  if(checkExistence('height')){
      
      jsonObject.values[index('height')].value=PF('layout').layout.state.center.innerHeight;
      
  }
  else{
      
      var height={'id':'height','value':PF('layout').layout.state.center.innerHeight};
      
      jsonObject.values.push(height);
      
  }
  
  if(checkExistence('width')){
      
      jsonObject.values[index('width')].value=PF('layout').layout.state.center.innerWidth;
      
  }else{
      
      var width={'id':'width','value':PF('layout').layout.state.center.innerWidth};
      
      jsonObject.values.push(width);
      
  }
  
PF('gMap').jq.width(jsonObject.values[index('width')].value);

PF('gMap').jq.height(jsonObject.values[index('height')].value);


  
google.maps.event.trigger(PF('gMap').getMap(),'resize');
   
  

  
  
   
/*PF('tabView').jq.click(function (){
    
    alert("tab Changed");
    alert($( this ).firstTab());
    
});*/
    
    /*PF('tabView').jq.on('tabChange',function (){
        
        alert('tabChanged');
        
    });*/
    
    
    /*
    
    var width=PF('layout').layout.state.center.innerWidth;
    
    var height=PF('layout').layout.state.center.innerHeight;
    
    PF('westPanel').jq.css('width',width);
    
    PF('westPanel').jq.css('height',height);
    
    var contentWidth=PF('layout').layout.state.center.innerWidth;
    
    var contentHeight=PF('layout').layout.state.center.innerHeight;
    
    PF('contentPanel').jq.css('width',contentWidth);
    
    PF('contentPanel').jq.css('height',contentHeight);
    */
    
    
  
}

function checkExistence(id){
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return true;
            
        }
        
    }
    
    return false;
    
}

function index(id){
    
    for(var key in jsonObject.values){
        
        if(jsonObject.values[key].id==id){
            
            return key;
            
        }
        
    }
   return 0; 
}

 function setMapCenter(latitude, longitude) {
        
          PF('gMap').getMap().setCenter(new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)));
        
    }
    
    function showMap(){
               
        PF('mapDialog').show();
        
    }
    
    function resizeMap(){
           
        PF('gMap').jq.width(jsonObject.values[index('width')].value);
        
        PF('gMap').jq.height(jsonObject.values[index('height')].value);
             
        PF('mapDialog').show();
        
        google.maps.event.trigger(PF('gMap').getMap(),'resize');
        
        PF('gMap').getMap().setCenter(new google.maps.LatLng(jsonObject.values[index('latitude')].value,jsonObject.values[index('longitude')].value));
     
    }


 