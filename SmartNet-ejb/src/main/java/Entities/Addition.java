/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "addition")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Addition.findAll", query = "SELECT a FROM Addition a"),
    @NamedQuery(name = "Addition.findByFranchise", query = "SELECT a from Addition a where a.additionPK.franquiciaIdfranquicia = :franchiseId" ),
    @NamedQuery(name = "Addition.findByIdaddition", query = "SELECT a FROM Addition a WHERE a.additionPK.idaddition = :idaddition"),
    @NamedQuery(name = "Addition.findByName", query = "SELECT a FROM Addition a WHERE a.name = :name AND a.additionPK.franquiciaIdfranquicia = :franchiseId"),    
    @NamedQuery(name = "Addition.findByValue", query = "SELECT a FROM Addition a WHERE a.value = :value")})
public class Addition implements Serializable {
   
  

    @ManyToMany(mappedBy = "additionCollection")
    private Collection<Producto> productoCollection;
 
     
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AdditionPK additionPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "value")
    private Double value;
    @JoinTable(name = "addition_has_imagen", joinColumns = {
        @JoinColumn(name = "addition_idaddition", referencedColumnName = "idaddition"),
        @JoinColumn(name="addition_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia")
        }, inverseJoinColumns = {
        @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")})
    @ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Imagen> imagenCollection;
    
 @OneToMany(cascade = CascadeType.ALL,mappedBy="addition")
 private Collection<AdditionConsumesInventario> additionConsumesInventarioCollection;
 
 @javax.persistence.JoinColumn(name= "franquicia_idfranquicia", referencedColumnName="idfranquicia",insertable=false,updatable=false)
 @ManyToOne(optional=false)
 private Entities.Franquicia franquicia;
 
 
 @javax.persistence.ManyToMany(mappedBy="additionCollection")
 private Collection<Entities.Sucursal>sucursalCollection;
 
 public Collection<Entities.Sucursal>getSucursalCollection(){
 
     return this.sucursalCollection;
 
 }

 public void setSucursalCollection(Collection<Entities.Sucursal>sucursalCollection){
 
 this.sucursalCollection=sucursalCollection;
 
 }
 
    public Addition() {
    }

    public Addition(AdditionPK additionPK) {
        this.additionPK = additionPK;
    }

    public Addition(AdditionPK additionPK, String name) {
        this.additionPK = additionPK;
        this.name = name;
    }

    public Addition(int idaddition, int franquiciaIdfranquicia) {
        this.additionPK = new AdditionPK(idaddition, franquiciaIdfranquicia);
    }

    public Entities.Franquicia getFranquicia(){
    
    return this.franquicia;
    
    }
    
    public void setFranquicia(Entities.Franquicia franquicia){
    
    this.franquicia=franquicia;
    
    }
    
    public AdditionPK getAdditionPK() {
        return additionPK;
    }

    public void setAdditionPK(AdditionPK additionPK) {
        this.additionPK = additionPK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }


    @XmlTransient
    public Collection<AdditionConsumesInventario> getAdditionConsumesInventarioCollection() {
        return additionConsumesInventarioCollection;
    }

    public void setAdditionConsumesInventarioCollection(Collection<AdditionConsumesInventario> additionConsumesInventarioCollection) {
        this.additionConsumesInventarioCollection = additionConsumesInventarioCollection;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (additionPK != null ? additionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Addition)) {
            return false;
        }
        Addition other = (Addition) object;
        if ((this.additionPK == null && other.additionPK != null) || (this.additionPK != null && !this.additionPK.equals(other.additionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Addition[ additionPK=" + additionPK + " ]";
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

 
    
}
