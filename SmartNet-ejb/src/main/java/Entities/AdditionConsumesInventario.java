/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "addition_consumes_inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdditionConsumesInventario.findAll", query = "SELECT a FROM AdditionConsumesInventario a"),
    @NamedQuery(name="AdditionConsumesInventario.findByAddition",query="SELECT a from AdditionConsumesInventario a where a.addition.additionPK = :additionPK"),
    @NamedQuery(name = "AdditionConsumesInventario.findByAdditionIdaddition", query = "SELECT a FROM AdditionConsumesInventario a WHERE a.additionConsumesInventarioPK.additionIdaddition = :additionIdaddition"),
    @NamedQuery(name = "AdditionConsumesInventario.findByInventarioIdinventario", query = "SELECT a FROM AdditionConsumesInventario a WHERE a.additionConsumesInventarioPK.inventarioIdinventario = :inventarioIdinventario"),
    @NamedQuery(name = "AdditionConsumesInventario.findByQuantity", query = "SELECT a FROM AdditionConsumesInventario a WHERE a.quantity = :quantity")})
public class AdditionConsumesInventario implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AdditionConsumesInventarioPK additionConsumesInventarioPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantity")
    @Min(1) @Max(1000000)
    private Double quantity;
    @JoinColumns({
        @JoinColumn(name = "addition_idaddition", referencedColumnName = "idaddition", insertable = false, updatable = false),
        @JoinColumn(name = "addition_franquicia_idfranquicia", referencedColumnName="franquicia_idfranquicia",insertable=false,updatable=false)})
      @ManyToOne(optional = false)
    private Addition addition;
    @JoinColumns({
        @JoinColumn(name = "inventario_idinventario", referencedColumnName = "idinventario", insertable = false, updatable = false),
        @javax.persistence.JoinColumn(name= "inventario_franquicia_idfranquicia",referencedColumnName= "franquicia_idfranquicia",insertable=false,updatable=false)})
         @ManyToOne(optional = false)
    private Inventario inventario;

    public AdditionConsumesInventario() {
    }

    public AdditionConsumesInventario(AdditionConsumesInventarioPK additionConsumesInventarioPK) {
        this.additionConsumesInventarioPK = additionConsumesInventarioPK;
    }

    public AdditionConsumesInventario(int additionIdaddition, int additionFranquiciaIdFranquicia, int inventarioIdinventario, int inventarioFranquiciaIdFranquicia) {
        this.additionConsumesInventarioPK = new AdditionConsumesInventarioPK(additionIdaddition, additionFranquiciaIdFranquicia, inventarioIdinventario, inventarioFranquiciaIdFranquicia);
    }

    public AdditionConsumesInventarioPK getAdditionConsumesInventarioPK() {
        return additionConsumesInventarioPK;
    }

    public void setAdditionConsumesInventarioPK(AdditionConsumesInventarioPK additionConsumesInventarioPK) {
        this.additionConsumesInventarioPK = additionConsumesInventarioPK;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Addition getAddition() {
        return addition;
    }

    public void setAddition(Addition addition) {
        this.addition = addition;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (additionConsumesInventarioPK != null ? additionConsumesInventarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdditionConsumesInventario)) {
            return false;
        }
        AdditionConsumesInventario other = (AdditionConsumesInventario) object;
        if ((this.additionConsumesInventarioPK == null && other.additionConsumesInventarioPK != null) || (this.additionConsumesInventarioPK != null && !this.additionConsumesInventarioPK.equals(other.additionConsumesInventarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AdditionConsumesInventario[ additionConsumesInventarioPK=" + additionConsumesInventarioPK + " ]";
    }
    
}
