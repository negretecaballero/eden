/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class AdditionConsumesInventarioPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "addition_idaddition")
    private int additionIdaddition;
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="addition_franquicia_idfranquicia")
    private int additionFranquiciaIdFranquicia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_idinventario")
    private int inventarioIdinventario;
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="inventario_franquicia_idfranquicia")
    private int inventarioFranquiciaIdFranquicia;

    public AdditionConsumesInventarioPK() {
    }

    public AdditionConsumesInventarioPK(int additionIdaddition, int additionFranquiciaIdFranquicia, int inventarioIdinventario, int inventarioFranquiciaIdFranquicia) {
        this.additionIdaddition = additionIdaddition;
        this.additionFranquiciaIdFranquicia=additionFranquiciaIdFranquicia;
        this.inventarioIdinventario = inventarioIdinventario;
        this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
        }

    public int getAdditionIdaddition() {
        return additionIdaddition;
    }

    public void setAdditionIdaddition(int additionIdaddition) {
        this.additionIdaddition = additionIdaddition;
    }

 
    public int getAdditionFranquiciaIdFranquicia(){
    
        return this.additionFranquiciaIdFranquicia;
    
    }
    
    public void setAdditionFranquiciaIdFranquicia(int additionFranquiciaIdFranquicia){
    
    
    this.additionFranquiciaIdFranquicia=additionFranquiciaIdFranquicia;
    
    }
    
    public int getInventarioIdinventario() {
        return inventarioIdinventario;
    }

    public void setInventarioIdinventario(int inventarioIdinventario) {
        this.inventarioIdinventario = inventarioIdinventario;
    }

    
    public int getInventarioFranquiciaIdFranquicia(){
    
        return this.inventarioFranquiciaIdFranquicia;
    
    }
    
    public void setInventarioFranquiciaIdFranquicia(int inventarioFranquiciaIdFranquicia){
    
    this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
    
    }
   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) additionIdaddition;
       hash += (int)additionFranquiciaIdFranquicia;
        hash += (int) inventarioIdinventario;
        hash += (int) inventarioFranquiciaIdFranquicia;
        
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdditionConsumesInventarioPK)) {
            return false;
        }
        AdditionConsumesInventarioPK other = (AdditionConsumesInventarioPK) object;
        if (this.additionIdaddition != other.additionIdaddition) {
            return false;
        }
        if (this.additionFranquiciaIdFranquicia != other.additionFranquiciaIdFranquicia) {
            return false;
        }
       
        if (this.inventarioIdinventario != other.inventarioIdinventario) {
            return false;
        }
           if (this.inventarioFranquiciaIdFranquicia != other.inventarioFranquiciaIdFranquicia) {
            return false;
        }
      
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AdditionConsumesInventarioPK[ additionIdaddition=" + additionIdaddition + ",additionFranquiciaIdFranquicia = "+additionFranquiciaIdFranquicia+", inventarioIdinventario=" + inventarioIdinventario + ", inventarioFranquiciaIdFranquicia=" + inventarioFranquiciaIdFranquicia + " ]";
    }
    
}
