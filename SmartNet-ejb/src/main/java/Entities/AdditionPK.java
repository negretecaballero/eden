/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class AdditionPK implements Serializable {
    
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="MySequenceGenerator")
    @SequenceGenerator(allocationSize=1, schema="myschema",  name="MySequenceGenerator", sequenceName = "mysequence")
    @Basic(optional = false)
    @Column(name = "idaddition")
    private int idaddition;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="franquicia_idfranquicia")
    private int franquiciaIdfranquicia;
    

    public AdditionPK() {
    }

    public AdditionPK(int idaddition, int franquciaIdfranquicia) {
        this.idaddition = idaddition;
        this.franquiciaIdfranquicia=franquiciaIdfranquicia;
    }

    public int getIdaddition() {
        return idaddition;
    }

    public void setIdaddition(int idaddition) {
        this.idaddition = idaddition;
    }

  
    public int getFranquiciaIdfranquicia(){
    
    return this.franquiciaIdfranquicia; 
        
    }
    
    public void setFranquiciaIdfranquicia(int franquiciaIdfranquicia){
    
    this.franquiciaIdfranquicia=franquiciaIdfranquicia;
    
    }
    
    @Override
    public int hashCode() {
        
        //  hash += ( idBug!=null? idBug.hashCode() : 0);
        
        int hash = 0;
        hash += (int) idaddition;
        hash += (int) franquiciaIdfranquicia;
       
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdditionPK)) {
            return false;
        }
        AdditionPK other = (AdditionPK) object;
        if (this.idaddition != other.idaddition) {
            return false;
        }
        
         if (this.franquiciaIdfranquicia != other.franquiciaIdfranquicia) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AdditionPK[ idaddition=" + idaddition + ", franquiciaIdfranquicia=" + franquiciaIdfranquicia + " ]";
    }
    
}
