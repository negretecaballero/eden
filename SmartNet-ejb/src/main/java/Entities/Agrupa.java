/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "agrupa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Agrupa.findAll", query = "SELECT a FROM Agrupa a"),
    @NamedQuery(name="Agrupa.findByProduct",query="SELECT a from Agrupa a where a.producto.productoPK = :productPK"),
    @NamedQuery(name = "Agrupa.findByFranchise",query="SELECT a from Agrupa a where a.agrupaPK.productoCategoriaFranquiciaIdFranquicia = :franchiseid"),
    @NamedQuery(name = "Agrupa.findByProductoIdproducto", query = "SELECT a FROM Agrupa a WHERE a.agrupaPK.productoIdproducto = :productoIdproducto"),
    @NamedQuery(name = "Agrupa.findByMenuIdmenu", query = "SELECT a FROM Agrupa a WHERE a.agrupaPK.menuIdmenu = :menuIdmenu"),
    @NamedQuery(name = "Agrupa.findByCantidadProducto", query = "SELECT a FROM Agrupa a WHERE a.cantidadProducto = :cantidadProducto")})
public class Agrupa implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AgrupaPK agrupaPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_producto")
    private int cantidadProducto;
    @JoinColumn(name = "menu_idmenu", referencedColumnName = "idmenu", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;
    @javax.persistence.JoinColumns({
    
        @JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto", insertable = false, updatable = false),
        
        @javax.persistence.JoinColumn(name ="producto_categoria_idcategoria",referencedColumnName="categoria_idcategoria",insertable=false,updatable=false),
        
        @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName = "categoria_franquicia_idfranquicia",insertable=false,updatable=false)
    
    })
     @ManyToOne(optional = false)
    private Producto producto;

    public Agrupa() {
    }

    public Agrupa(AgrupaPK agrupaPK) {
        this.agrupaPK = agrupaPK;
    }

    public Agrupa(AgrupaPK agrupaPK, int cantidadProducto) {
        this.agrupaPK = agrupaPK;
        this.cantidadProducto = cantidadProducto;
    }

    public Agrupa(int productoIdproducto, int menuIdmenu,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia) {
        this.agrupaPK = new AgrupaPK(productoIdproducto, menuIdmenu,productoCategoriaIdCategoria,productoCategoriaFranquiciaIdFranquicia);
    }

    public AgrupaPK getAgrupaPK() {
        return agrupaPK;
    }

    public void setAgrupaPK(AgrupaPK agrupaPK) {
        this.agrupaPK = agrupaPK;
    }

    public int getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(int cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agrupaPK != null ? agrupaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Agrupa)) {
            return false;
        }
        Agrupa other = (Agrupa) object;
        if ((this.agrupaPK == null && other.agrupaPK != null) || (this.agrupaPK != null && !this.agrupaPK.equals(other.agrupaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Agrupa[ agrupaPK=" + agrupaPK + " ]";
    }
    
}
