/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class AgrupaPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria")
    private int productoCategoriaIdCategoria;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia")
    private int productoCategoriaFranquiciaIdFranquicia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_idmenu")
    private int menuIdmenu;

    
    
    
    public AgrupaPK() {
    }

    public AgrupaPK(int productoIdproducto, int menuIdmenu,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia) {
        this.productoIdproducto = productoIdproducto;
        this.menuIdmenu = menuIdmenu;
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
        
    }
    
    public int getProductoCategoriaIdCategoria(){
    
        return this.productoCategoriaIdCategoria;
    
    }
    
    public void setProductoCategoriaIdCategoria(int productoCategoriaIdCategoria){
    
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
    
    }
    
    public int getProductoCategoriaFranquiciaIdFranquicia(){
    
        return this.productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    public void setProductoCategoriaFranquiciaIdFranquicia(int productoCategoriaFranquiciaIdFranquicia){
    
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
    
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getMenuIdmenu() {
        return menuIdmenu;
    }

    public void setMenuIdmenu(int menuIdmenu) {
        this.menuIdmenu = menuIdmenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productoIdproducto;
        hash += (int) menuIdmenu;
        hash += (int)productoCategoriaIdCategoria;
        hash += (int)productoCategoriaFranquiciaIdFranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgrupaPK)) {
            return false;
        }
        AgrupaPK other = (AgrupaPK) object;
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.menuIdmenu != other.menuIdmenu) {
            return false;
        }
        if (this.productoCategoriaIdCategoria != other.productoCategoriaIdCategoria) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdFranquicia != other.productoCategoriaFranquiciaIdFranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AgrupaPK[ productoIdproducto=" + productoIdproducto + ", menuIdmenu=" + menuIdmenu + " ]";
    }
    
}
