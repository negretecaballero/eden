/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.EdenList;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "APP_GROUP")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AppGroup.findAll", query = "SELECT a FROM AppGroup a"),
    @NamedQuery(name = "AppGroup.findByUsername",query="SELECT a FROM AppGroup a WHERE a.loginAdministrador.username = :username"),
    @NamedQuery(name = "AppGroup.findByGroupid", query = "SELECT a FROM AppGroup a WHERE a.appGroupPK.groupid = :groupid"),
    @NamedQuery(name = "AppGroup.findByLoginAdministradorusername", query = "SELECT a FROM AppGroup a WHERE a.appGroupPK.loginAdministradorusername = :loginAdministradorusername"),
    @NamedQuery(name="AppGroup.findSpecific",query="SELECT a FROM AppGroup a WHERE a.appGroupPK.groupid = :appgroup AND a.loginAdministrador.username = :loginAdministrador")

})public class AppGroup implements Serializable {

   
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AppGroupPK appGroupPK;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private LoginAdministrador loginAdministrador;

    public AppGroup() {
    }

    public AppGroup(AppGroupPK appGroupPK) {
        this.appGroupPK = appGroupPK;
    }

    public AppGroup(String groupid, String loginAdministradorusername) {
        this.appGroupPK = new AppGroupPK(groupid, loginAdministradorusername);
    }

    public AppGroupPK getAppGroupPK() {
        return appGroupPK;
    }

    public void setAppGroupPK(AppGroupPK appGroupPK) {
        this.appGroupPK = appGroupPK;
    }

    public LoginAdministrador getLoginAdministrador() {
        return loginAdministrador;
    }

    public void setLoginAdministrador(LoginAdministrador loginAdministrador) {
        this.loginAdministrador = loginAdministrador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (appGroupPK != null ? appGroupPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppGroup)) {
            return false;
        }
        AppGroup other = (AppGroup) object;
        if ((this.appGroupPK == null && other.appGroupPK != null) || (this.appGroupPK != null && !this.appGroupPK.equals(other.appGroupPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AppGroup[ appGroupPK=" + appGroupPK + " ]";
    }

 

 
    
}
