/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class AppGroupPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "groupid")
    private String groupid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "loginAdministrador_username")
    private String loginAdministradorusername;

    public AppGroupPK() {
    }

    public AppGroupPK(String groupid, String loginAdministradorusername) {
        this.groupid = groupid;
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(String loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupid != null ? groupid.hashCode() : 0);
        hash += (loginAdministradorusername != null ? loginAdministradorusername.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AppGroupPK)) {
            return false;
        }
        AppGroupPK other = (AppGroupPK) object;
        if ((this.groupid == null && other.groupid != null) || (this.groupid != null && !this.groupid.equals(other.groupid))) {
            return false;
        }
        if ((this.loginAdministradorusername == null && other.loginAdministradorusername != null) || (this.loginAdministradorusername != null && !this.loginAdministradorusername.equals(other.loginAdministradorusername))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.AppGroupPK[ groupid=" + groupid + ", loginAdministradorusername=" + loginAdministradorusername + " ]";
    }
    
}
