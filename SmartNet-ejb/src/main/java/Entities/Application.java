/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Entity
@javax.persistence.Table(name="application")
@XmlRootElement

@javax.persistence.NamedQueries({

    @javax.persistence.NamedQuery(name="Application.findByMail",query="SELECT a from Application a where a.mail = :mail"),
    
    @javax.persistence.NamedQuery(name="Application.findByNit",query="SELECT a from Application a where a.applicationPK.nit = :nit"),

    @NamedQuery(name="Application.findAll", query="SELECT a from Application a ORDER BY a.franchiseName"),
    
    @NamedQuery(name="Application.findByStatus",query="select a from Application a where a.idApplicationStatus.status = :status")
        
})

public class Application implements Serializable{
    @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")
    @ManyToOne(optional=true,cascade=javax.persistence.CascadeType.ALL)
    private Imagen imagenIdimagen;
   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "mail")
    @Validators.MailValidator
    private String mail;
    
    @EmbeddedId
    protected Entities.ApplicationPK applicationPK;
    
    @javax.persistence.Column(name="franchise_name")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=100)
    private String franchiseName;
    
    @Basic(optional = false)
    
    @Column(name = "fecha_expedicion")
    @Temporal(TemporalType.DATE)
    private Date fechaExpedicion;
    

    @Basic(optional = false)
    @Column(name = "codigo_ciuu")
    @Validators.NumberValidator
    private String codigoCiuu;
    @JoinColumns({
        @JoinColumn(name = "CITY_idCITY", referencedColumnName = "idCITY"),
        @JoinColumn(name = "CITY_STATE_idSTATE", referencedColumnName = "STATE_idSTATE")})
    @ManyToOne(optional = false)
    private City city;

    
    
    @javax.persistence.JoinColumns({
    
        @javax.persistence.JoinColumn(name="subtype_idsubtype",referencedColumnName="idsubtype"),
        
        @javax.persistence.JoinColumn(name="subtype_tipo_idtipo",referencedColumnName="tipo_idtipo")
    
    })
    @javax.persistence.ManyToOne
    private Entities.Subtype idSubtype;
  
    
    @javax.persistence.JoinColumn(name="application_status_id",referencedColumnName="id")
    @javax.persistence.ManyToOne
    private Entities.ApplicationStatus idApplicationStatus;
    
    
    
    @javax.persistence.OneToOne(mappedBy="idApplication")
  private Entities.Franquicia idFranquicia;
    
    public Entities.Franquicia getIdFranquicia(){
    
        return this.idFranquicia;
    
    }
    
    public void setIdFranquicia(Entities.Franquicia idFranquicia){
    
        this.idFranquicia=idFranquicia;
    
    }
    
    public Entities.ApplicationStatus getIdApplicationStatus(){
    
        return this.idApplicationStatus;
    
    }
    
    public void setIdApplicationStatus(Entities.ApplicationStatus idApplicationStatus){
    
        this.idApplicationStatus=idApplicationStatus;
    
    }
    
    
  
    public String getFranchiseName(){
    
    return this.franchiseName;
    
    }
    
    public void setFranchiseName(String franchiseName){
    
    this.franchiseName=franchiseName;
    
    }
    
    public Entities.Subtype getIdSubtype(){
    
    return this.idSubtype;
    
    }
    
    public void setIdSubtype(Entities.Subtype idSubtype){
    
        this.idSubtype=idSubtype;
    
    }
    
    public Application() {
    
        
    
    }
    
  
    
    public String getMail()
    {
    
        return this.mail;
    
    }
    
    public void setMail(String mail){
    
        this.mail=mail;
    
    }
    
    
    public java.util.Date getFechaExpedicion(){
    
        return this.fechaExpedicion;
    
    }
    
    public void setFechaExpedicion(java.util.Date fechaExpedicion){
    
    
        this.fechaExpedicion=fechaExpedicion;
    
    }
    
    public String getCodigoCiuu(){
    
    return this.codigoCiuu;
    
    }
    
    public void setCodigoCiuu(String codigoCiuu){
    
    this.codigoCiuu=codigoCiuu;
    
    }
    
    
    
   
    

    public Application(String mail,  Date fechaExpedicion, String codigoCiuu) {
       
        this.mail = mail;
        this.fechaExpedicion = fechaExpedicion;
        this.codigoCiuu = codigoCiuu;
    }


    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
         if (!(object instanceof Application)) {
            return false;
        }
        Application other = (Application) object;
        if ((this.applicationPK == null && other.applicationPK != null) || (this.applicationPK != null && !this.applicationPK.equals(other.applicationPK))) {
            return false;
        }
        return true;
    }

    public Entities.ApplicationPK getApplicationPK() {
        return applicationPK;
    }

    public void setApplicationPK(Entities.ApplicationPK applicationPK) {
        this.applicationPK = applicationPK;
    }

    public Imagen getImagenIdimagen() {
        return imagenIdimagen;
    }

    public void setImagenIdimagen(Imagen imagenIdimagen) {
        this.imagenIdimagen = imagenIdimagen;
    }

    
    @Override
public String toString(){

return this.applicationPK.toString();    

}
   

   
    
    
}

