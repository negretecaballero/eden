/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable
public class ApplicationPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @javax.validation.constraints.Min(1000000)
 
    @Column(name = "cedula")
    private long cedula;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "nit")
    private String nit;
   
    
    public ApplicationPK(){
    
    
    }
    
    public ApplicationPK(long cedula,String nit){
    
        this.cedula=cedula;
        
        this.nit=nit;
    
    }
    
   public long getCedula(){
   
   return this.cedula;
   
   }
    
   
   public void setCedula(long cedula){
   
   this.cedula=cedula;
   
   }
    
    public String getNit(){
    
    return this.nit;
    
    }
    
    public void setNit(String nit){
    
    this.nit=nit;
    
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) cedula;
        return hash;
    }
    
    
      @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicationPK)) {
            return false;
        }
        ApplicationPK other = (ApplicationPK) object;
        if (this.cedula!=other.cedula) {
            return false;
        }
        if (!this.nit.equals( other.nit)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nit+","+cedula;
    }
    
}
