/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Table(name="application_status")
@javax.persistence.Entity
@javax.xml.bind.annotation.XmlRootElement
@NamedQueries({
    
    @NamedQuery(name="ApplicationStatus.findAll",query="SELECT a from ApplicationStatus a"),

    @NamedQuery(name="ApplicationStatus.findByStatus",query="SELECT a from ApplicationStatus a where a.status = :status")

})
public class ApplicationStatus implements java.io.Serializable{
    
    private static final long serialVersionUID=1L;
   
    @javax.persistence.Id
    @javax.persistence.Column(name="id")
    @javax.validation.constraints.NotNull
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    private Integer id;
    
    
    @javax.persistence.Column(name="status")
    @javax.persistence.Enumerated(javax.persistence.EnumType.STRING)
    @javax.validation.constraints.NotNull
    private Entitites.Enum.ApplicationStatusENUM status;
    
    @javax.persistence.OneToMany(mappedBy="idApplicationStatus")
    private java.util.Collection<Entities.Application> idApplication;
    
    public java.util.Collection<Entities.Application> getIdApplication(){
    
       return this.idApplication;
    
    }
    
    public void setIdApplication(java.util.Collection<Entities.Application> idApplication){
    
    this.idApplication=idApplication;
    
    }
    
    public Integer getId(){
    
        return this.id;
    
    }
    
    public void setId(Integer id){
    
        this.id=id;
    
    }
    
    public Entitites.Enum.ApplicationStatusENUM getStatus(){
    
        return this.status;
    
    }
    
    public void setStatus(Entitites.Enum.ApplicationStatusENUM status){
    
        this.status=status;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=id!=null?id.hashCode():0;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ApplicationStatus other = (ApplicationStatus) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return "Entities.ApplicationStatus[ id = "+this.id+" ]";
    
    }
    
}
