/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Collection;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="barcode")

@XmlRootElement

@javax.persistence.NamedQueries({

    @javax.persistence.NamedQuery(name="Barcode.findByBarcode",query="SELECT b from Barcode b where b.barcode = :barcode")

})

public class Barcode implements java.io.Serializable{
    
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @javax.persistence.Id
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="barcodeid")
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)  
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.persistence.Basic(optional=false)
    private int barcodeId;

    @javax.validation.constraints.NotNull
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="barcode")
    private String barcode;
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="barcode")
    private Collection<Entities.Producto> productoCollection;

public Barcode(){


}
   
@Override
public int hashCode(){

int hash=0;

hash+=(int)barcodeId;

return hash;
}


public int getBarcodeId(){

return this.barcodeId;

}

public void setBarcodeId(int barcodeId){

this.barcodeId=barcodeId;

}

public Barcode(String barcode){

    this.barcode=barcode;

}

public String getBarcode(){

    return this.barcode;

}

public void setBarcode(String barcode){

this.barcode=barcode;

}

public Collection<Entities.Producto>getProductoCollection(){

    return this.productoCollection;

}


public void setProductoCollection(Collection<Entities.Producto>productoCollection){

this.productoCollection=productoCollection;

}



    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Barcode other = (Barcode) obj;
        if (!this.barcode.equals( other.barcode) ){
            return false;
        }
        return true;
    }

@Override
public String toString(){

    return "Entities.Barcode [ barcodeId = "+barcodeId+" ]";

}


}
