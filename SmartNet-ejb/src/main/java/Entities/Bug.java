/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Entity

@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.Table(name="bug")

@javax.persistence.NamedQueries({


})

public class Bug implements java.io.Serializable {
    
    private final static long serialVersionUID=1L;
    
    
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @javax.persistence.Id
    @javax.persistence.Column(name="idbug")
    @javax.persistence.Basic(optional=false)
    private Integer idBug;
    
    @javax.persistence.Temporal(javax.persistence.TemporalType.TIMESTAMP)
   
    @javax.persistence.Column(name="date")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    private java.util.Date date;
    
    @javax.validation.constraints.Size(min=0,max=21844)
    @javax.persistence.Column(name="comments")
    @javax.persistence.Basic(optional=false)
    private String comments;
    
    @javax.persistence.JoinColumn(name="loginAdministrador_username",referencedColumnName="username")
    @javax.persistence.ManyToOne
    private Entities.LoginAdministrador idLoginAdministrador;
    
    public java.util.Date getDate(){
    
        return this.date;
    
    }
    
    public void setDate(java.util.Date date){
    
        this.date=date;
    
    }
    
    public String getComments(){
    
        return this.comments;
    
    }
    
    public void setComments(String comments){
    
        this.comments=comments;
    
    }
    
    public Integer getIdBug(){
    
        return this.idBug;
    
    }
    
    public void setIdBug(Integer idBug){
    
        this.idBug=idBug;
    
    }
    
    
    
    public Entities.LoginAdministrador getIdLoginAdministrador(){
    
        return this.idLoginAdministrador;
    
    }
    
    public void setIdLoginAdministrador(Entities.LoginAdministrador idLoginAdministrador){
    
        this.idLoginAdministrador=idLoginAdministrador;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash += ( idBug!=null? idBug.hashCode() : 0);
        
        return hash;
 
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bug other = (Bug) obj;
        if (!Objects.equals(this.idBug, other.idBug)) {
            return false;
        }
        return true;
    }
    
}
