/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "categoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categoria.findAll", query = "SELECT c FROM Categoria c ORDER BY c.nombre ASC"),
    @NamedQuery( name= "Categoria.findByFranchise",query="SELECT c from Categoria c where c.categoriaPK.franquiciaIdfranquicia = :idfranchise"),
    @NamedQuery(name = "Categoria.findByIdcategoria", query = "SELECT c FROM Categoria c WHERE c.categoriaPK.idcategoria = :idcategoria"),
    @NamedQuery(name="Categoria.findByName", query="SELECT c FROM Categoria c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Categoria.findByNombre", query = "SELECT c FROM Categoria c WHERE c.nombre = :nombre AND c.categoriaPK.franquiciaIdfranquicia = :idfranquicia")})
  public class Categoria implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CategoriaPK categoriaPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
    @JoinTable(name = "categoria_has_imagen", joinColumns = {
        @JoinColumn(name = "categoria_idcategoria", referencedColumnName = "idcategoria"),
        @JoinColumn(name = "categoria_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia")}, inverseJoinColumns = {
        @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")})
    @ManyToMany(cascade=javax.persistence.CascadeType.PERSIST)
    private Collection<Imagen> imagenCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoria")
    @javax.persistence.OrderBy("nombre")
    private Collection<Producto> productoCollection;
    @JoinColumn(name = "franquicia_idfranquicia", referencedColumnName = "idfranquicia", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Franquicia franquicia;

    public Categoria() {
    }

    public Categoria(CategoriaPK categoriaPK) {
        this.categoriaPK = categoriaPK;
    }

    public Categoria(CategoriaPK categoriaPK, String nombre) {
        this.categoriaPK = categoriaPK;
        this.nombre = nombre;
    }

    public Categoria(int idcategoria, int franquiciaIdfranquicia) {
        this.categoriaPK = new CategoriaPK(idcategoria, franquiciaIdfranquicia);
    }

    public CategoriaPK getCategoriaPK() {
        return categoriaPK;
    }

    public void setCategoriaPK(CategoriaPK categoriaPK) {
        this.categoriaPK = categoriaPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    public Franquicia getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(Franquicia franquicia) {
        this.franquicia = franquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (categoriaPK != null ? categoriaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categoria)) {
            return false;
        }
        Categoria other = (Categoria) object;
        if ((this.categoriaPK == null && other.categoriaPK != null) || (this.categoriaPK != null && !this.categoriaPK.equals(other.categoriaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Categoria[ categoriaPK=" + categoriaPK + " ]";
    }
    
}
