/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.Key;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class CategoriaPK extends Key implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcategoria")
    private int idcategoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "franquicia_idfranquicia")
    private int franquiciaIdfranquicia;

    public CategoriaPK() {
    }

    public CategoriaPK(int idcategoria, int franquiciaIdfranquicia) {
        this.idcategoria = idcategoria;
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    public int getFranquiciaIdfranquicia() {
        return franquiciaIdfranquicia;
    }

    public void setFranquiciaIdfranquicia(int franquiciaIdfranquicia) {
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idcategoria;
        hash += (int) franquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoriaPK)) {
            return false;
        }
        CategoriaPK other = (CategoriaPK) object;
        if (this.idcategoria != other.idcategoria) {
            return false;
        }
        if (this.franquiciaIdfranquicia != other.franquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.CategoriaPK[ idcategoria=" + idcategoria + ", franquiciaIdfranquicia=" + franquiciaIdfranquicia + " ]";
    }
    
}
