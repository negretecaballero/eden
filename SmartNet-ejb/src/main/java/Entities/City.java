/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "CITY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "City.findAll", query = "SELECT c FROM City c"),
    @NamedQuery(name = "City.findByIdCITY", query = "SELECT c FROM City c WHERE c.cityPK.idCITY = :idCITY"),
    @NamedQuery(name = "City.findByName", query = "SELECT c FROM City c WHERE c.name = :name"),
    @NamedQuery(name = "City.findBySTATEidSTATE", query = "SELECT c FROM City c WHERE c.cityPK.sTATEidSTATE = :sTATEidSTATE")})
public class City implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "city")
    private Collection<Application> applicationCollection;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CityPK cityPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "city")
    private Collection<Direccion> direccionCollection;
    @JoinColumn(name = "STATE_idSTATE", referencedColumnName = "idSTATE", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private State state;

    
    
    public City() {
    }

    public City(CityPK cityPK) {
        this.cityPK = cityPK;
    }

    public City(CityPK cityPK, String name) {
        this.cityPK = cityPK;
        this.name = name;
    }

    public City(int idCITY, int sTATEidSTATE) {
        this.cityPK = new CityPK(idCITY, sTATEidSTATE);
    }

    public CityPK getCityPK() {
        return cityPK;
    }

    public void setCityPK(CityPK cityPK) {
        this.cityPK = cityPK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection() {
        return direccionCollection;
    }

    public void setDireccionCollection(Collection<Direccion> direccionCollection) {
        this.direccionCollection = direccionCollection;
    }

    
    @XmlTransient
    public Collection<Entities.Application>getApplicationCollection(){
    
        return this.applicationCollection;
    
    }
    
    public void setApplicationCollection(Collection<Entities.Application>applicationCollection){
    
        this.applicationCollection=applicationCollection;
    
    }
    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cityPK != null ? cityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof City)) {
            return false;
        }
        City other = (City) object;
        if ((this.cityPK == null && other.cityPK != null) || (this.cityPK != null && !this.cityPK.equals(other.cityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.City[ cityPK=" + cityPK + " ]";
    }

   
}
