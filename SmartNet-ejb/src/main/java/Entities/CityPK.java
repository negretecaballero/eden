/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class CityPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCITY")
    private int idCITY;
    @Basic(optional = false)
    @NotNull
    @Column(name = "STATE_idSTATE")
    private int sTATEidSTATE;

    public CityPK() {
    }

    public CityPK(int idCITY, int sTATEidSTATE) {
        this.idCITY = idCITY;
        this.sTATEidSTATE = sTATEidSTATE;
    }

    public int getIdCITY() {
        return idCITY;
    }

    public void setIdCITY(int idCITY) {
        this.idCITY = idCITY;
    }

    public int getSTATEidSTATE() {
        return sTATEidSTATE;
    }

    public void setSTATEidSTATE(int sTATEidSTATE) {
        this.sTATEidSTATE = sTATEidSTATE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idCITY;
        hash += (int) sTATEidSTATE;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CityPK)) {
            return false;
        }
        CityPK other = (CityPK) object;
        if (this.idCITY != other.idCITY) {
            return false;
        }
        if (this.sTATEidSTATE != other.sTATEidSTATE) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.CityPK[ idCITY=" + idCITY + ", sTATEidSTATE=" + sTATEidSTATE + " ]";
    }
    
}
