/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "compone")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compone.findAll", query = "SELECT c FROM Compone c"),
    @NamedQuery(name = "Compone.findByProductoIdproducto", query = "SELECT c FROM Compone c WHERE c.producto.productoPK = :idProduct"),
    @NamedQuery(name = "Compone.findByCantidadInventario", query = "SELECT c FROM Compone c WHERE c.cantidadInventario = :cantidadInventario"),
    @NamedQuery(name = "Compone.findByInventarioIdinventario", query = "SELECT c FROM Compone c WHERE c.componePK.inventarioIdinventario = :inventarioIdinventario")})
    public class Compone implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComponePK componePK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    
    @javax.validation.constraints.Min(1)
    @Column(name = "cantidad_inventario")
    private Double cantidadInventario;
    @JoinColumns(
            {
                @javax.persistence.JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto", insertable = false, updatable = false),
            
                @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria", referencedColumnName="categoria_idcategoria",insertable=false,updatable=false),
                
                @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName = "categoria_franquicia_idfranquicia", insertable=false,updatable=false)
            
            
            }
    
    )
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumns({
        @JoinColumn(name = "inventario_idinventario", referencedColumnName = "idinventario", insertable = false, updatable = false),
        @JoinColumn(name = "inventario_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Inventario inventario;

    public Compone() {
    }

    public Compone(ComponePK componePK) {
        this.componePK = componePK;
    }

    public Compone(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int inventarioIdinventario, int inventarioFranquiciaIdFranquicia) {
        this.componePK = new ComponePK(productoIdproducto,productoCategoriaIdCategoria,productoCategoriaFranquiciaIdFranquicia, inventarioIdinventario, inventarioFranquiciaIdFranquicia);
    }

    public ComponePK getComponePK() {
        return componePK;
    }

    public void setComponePK(ComponePK componePK) {
        this.componePK = componePK;
    }

    public Double getCantidadInventario() {
        return cantidadInventario;
    }

    public void setCantidadInventario(Double cantidadInventario) {
        this.cantidadInventario = cantidadInventario;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (componePK != null ? componePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compone)) {
            return false;
        }
        Compone other = (Compone) object;
        if ((this.componePK == null && other.componePK != null) || (this.componePK != null && !this.componePK.equals(other.componePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        
        
        return componePK.toString();
    }
    
}
