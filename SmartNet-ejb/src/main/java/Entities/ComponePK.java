/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.Key;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class ComponePK extends Key implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria")
    private int productoCategoriaIdCategoria;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia")
    private int productoCategoriaFranquiciaIdFranquicia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_idinventario")
    private int inventarioIdinventario;
    
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="inventario_franquicia_idfranquicia")
    private int inventarioFranquiciaIdFranquicia;

    public int getInventarioFranquiciaIdFranquicia(){
    
    return this.inventarioFranquiciaIdFranquicia;
    
    }
    
    public void setInventarioFranquiciaIdFranquicia(int inventarioFranquiciaIdFranquicia){
    
        this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
    
    }
    
    public int getProductoCategoriaIdCategoria(){
    
        return this.productoCategoriaIdCategoria;
    
    }
    
    public void setProductoCategoriaIdCategoria(int productoCategoriaIdCategoria){
    
    this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
    
    }
    
    public int getProductoCategoriaFranquiciaIdFranquicia(){

return this.productoCategoriaFranquiciaIdFranquicia;
    
}
    
    public void setProductoCategoriaFranquiciaIdFranquicia(int productoCategoriaFranquiciaIdFranquicia){
    
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
    
    
    }
    
    
    
    public ComponePK() {
    }

    public ComponePK(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int inventarioIdinventario, int inventarioFranquiciaIdFranquicia) {
        this.productoIdproducto = productoIdproducto;
        this.inventarioIdinventario = inventarioIdinventario;
        this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
       this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
        
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getInventarioIdinventario() {
        return inventarioIdinventario;
    }

    public void setInventarioIdinventario(int inventarioIdinventario) {
        this.inventarioIdinventario = inventarioIdinventario;
    }

 

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productoIdproducto;
        hash += (int) productoCategoriaIdCategoria;
        hash += (int) productoCategoriaFranquiciaIdFranquicia;
        hash += (int) inventarioIdinventario;
        hash += (int) inventarioFranquiciaIdFranquicia;
       
        
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComponePK)) {
            return false;
        }
        ComponePK other = (ComponePK) object;
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.inventarioIdinventario != other.inventarioIdinventario) {
            return false;
        }
         if (this.inventarioFranquiciaIdFranquicia != other.inventarioFranquiciaIdFranquicia) {
            return false;
        }
        
        if (this.productoCategoriaIdCategoria != other.productoCategoriaIdCategoria) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdFranquicia != other.productoCategoriaFranquiciaIdFranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.productoIdproducto+","+this.productoCategoriaIdCategoria+","+this.productoCategoriaFranquiciaIdFranquicia+","+this.inventarioIdinventario+","+this.inventarioFranquiciaIdFranquicia;
    }
    }
