/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "confirmation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Confirmation.findAll", query = "SELECT c FROM Confirmation c"),
    @NamedQuery(name = "Confirmation.findByConfirmed", query = "SELECT c FROM Confirmation c WHERE c.confirmed = :confirmed"),
    @NamedQuery(name = "Confirmation.exists",query="SELECT c from Confirmation c where c.loginAdministradorusername = :username AND c.confirmed = :confirmed"),
    @NamedQuery(name = "Confirmation.findByLoginAdministradorusername", query = "SELECT c FROM Confirmation c WHERE c.loginAdministradorusername = :loginAdministradorusername")})
public class Confirmation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "confirmed")
    private boolean confirmed;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "loginAdministrador_username")
    private String loginAdministradorusername;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private LoginAdministrador loginAdministrador;

    public Confirmation() {
    }

    public Confirmation(String loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public Confirmation(String loginAdministradorusername, boolean confirmed) {
        this.loginAdministradorusername = loginAdministradorusername;
        this.confirmed = confirmed;
    }

    public boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(String loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public LoginAdministrador getLoginAdministrador() {
        return loginAdministrador;
    }

    public void setLoginAdministrador(LoginAdministrador loginAdministrador) {
        this.loginAdministrador = loginAdministrador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginAdministradorusername != null ? loginAdministradorusername.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Confirmation)) {
            return false;
        }
        Confirmation other = (Confirmation) object;
        if ((this.loginAdministradorusername == null && other.loginAdministradorusername != null) || (this.loginAdministradorusername != null && !this.loginAdministradorusername.equals(other.loginAdministradorusername))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Confirmation[ loginAdministradorusername=" + loginAdministradorusername + " ]";
    }
    
}
