/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Entity
@javax.persistence.Table(name="day")
@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.NamedQueries({

    @NamedQuery(name="Day.findByName",query="SELECT d from Day d where d.dayName = :dayName")

})

public class Day implements java.io.Serializable{
    
      private static final long serialVersionUID = 1L;
      
      @javax.persistence.Id
      @javax.validation.constraints.NotNull
      @javax.validation.constraints.Min(1)
      @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
      @javax.persistence.Basic(optional=false)
      @javax.persistence.Column(name="idday")
      private int idDay;
    
      @javax.persistence.Column(name="day_name")
      @javax.persistence.Basic(optional=false)
      @javax.validation.constraints.Size(min=1,max=9)
      @javax.validation.constraints.NotNull
      private String dayName;
      
      @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="dayId")
      private java.util.Collection<Entities.SucursalHasDay>sucursalHasDayCollection;
      
      public int getIdDay(){
      
          return this.idDay;
      
      }
      
      public void setIdDay(int idDay){
      
          this.idDay=idDay;
      
      }
      
      
      public String getDayName(){
      
      return this.dayName;
      
      }
      
      public void setDayName(String dayName){
      
      this.dayName=dayName;
      
      }
      
      
      @javax.xml.bind.annotation.XmlTransient
      public java.util.Collection<Entities.SucursalHasDay>getSucursalHasDayCollection(){
      
          return this.sucursalHasDayCollection;
      
      }
      
      public void setSucursalHasDayCollection(java.util.Collection<Entities.SucursalHasDay>sucursalHasDayCollection){
      
      this.sucursalHasDayCollection=sucursalHasDayCollection;
      
      }
      
      public Day(){
      
      
      }
      
      public Day(String dayName){
      
          this.dayName=dayName;
      
      }
      @Override
      public int hashCode(){
      
          int hash=0;
          
          hash+=this.idDay;
          
          return hash;
      
      }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Day other = (Day) obj;
        if (this.idDay != other.idDay) {
            return false;
        }
        if (!Objects.equals(this.dayName, other.dayName)) {
            return false;
        }
        return true;
    }
    
    
    
}
