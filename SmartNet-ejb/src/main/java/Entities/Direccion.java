/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "DIRECCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Direccion.findAll", query = "SELECT d FROM Direccion d"),
    @NamedQuery(name = "Direccion.findByPrimerDigito", query = "SELECT d FROM Direccion d WHERE d.primerDigito = :primerDigito"),
    @NamedQuery(name = "Direccion.findBySegundoDigito", query = "SELECT d FROM Direccion d WHERE d.segundoDigito = :segundoDigito"),
    @NamedQuery(name = "Direccion.findByTercerDigito", query = "SELECT d FROM Direccion d WHERE d.tercerDigito = :tercerDigito"),
    @NamedQuery(name = "Direccion.findByAdicional", query = "SELECT d FROM Direccion d WHERE d.adicional = :adicional"),
    @NamedQuery(name = "Direccion.findByIdDireccion", query = "SELECT d FROM Direccion d WHERE d.direccionPK.idDireccion = :idDireccion"),
    @NamedQuery(name = "Direccion.findByCITYidCITY", query = "SELECT d FROM Direccion d WHERE d.direccionPK.cITYidCITY = :cITYidCITY"),
    @NamedQuery(name = "Direccion.findByCITYSTATEidSTATE", query = "SELECT d FROM Direccion d WHERE d.direccionPK.cITYSTATEidSTATE = :cITYSTATEidSTATE")})
public class Direccion implements Serializable {
    @JoinTable(name = "loginAdministrador_has_DIRECCION", joinColumns = {
        @JoinColumn(name = "DIRECCION_idDireccion", referencedColumnName = "idDireccion"),
        @JoinColumn(name = "DIRECCION_CITY_idCITY", referencedColumnName = "CITY_idCITY"),
        @JoinColumn(name = "DIRECCION_CITY_STATE_idSTATE", referencedColumnName = "CITY_STATE_idSTATE")}, inverseJoinColumns = {
        @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username")})
    @ManyToMany
    private Collection<LoginAdministrador> loginAdministradorCollection;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccion")
    private Collection<Pedido> pedidoCollection;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DireccionPK direccionPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "primer_digito")
    private String primerDigito;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "segundo_digito")
    private String segundoDigito;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tercer_digito")
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    private short tercerDigito;
    @Size(max = 45)
    @Column(name = "adicional")
    private String adicional;
    
    @JoinColumn(name = "TYPE_ADDRESS_idTYPE_ADDRESS", referencedColumnName = "idTYPE_ADDRESS")
    @ManyToOne(optional = false)
    private TypeAddress tYPEADDRESSidTYPEADDRESS;
    @JoinColumn(name = "gpsCoordinates_idgpsCoordinated", referencedColumnName = "idgpsCoordinated")
    @ManyToOne(optional=false,cascade=javax.persistence.CascadeType.ALL)
    private GpsCoordinates gpsCoordinatesidgpsCoordinated;
    @JoinColumns({
        @JoinColumn(name = "CITY_idCITY", referencedColumnName = "idCITY", insertable = false, updatable = false),
        @JoinColumn(name = "CITY_STATE_idSTATE", referencedColumnName = "STATE_idSTATE", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private City city;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "direccion")
    private Collection<Sucursal> sucursalCollection;

    public Direccion() {
    }

    public Direccion(DireccionPK direccionPK) {
        this.direccionPK = direccionPK;
    }

    public Direccion(DireccionPK direccionPK, String primerDigito, String segundoDigito, short tercerDigito) {
        this.direccionPK = direccionPK;
        this.primerDigito = primerDigito;
        this.segundoDigito = segundoDigito;
        this.tercerDigito = tercerDigito;
    }

    public Direccion(int idDireccion, int cITYidCITY, int cITYSTATEidSTATE) {
        this.direccionPK = new DireccionPK(idDireccion, cITYidCITY, cITYSTATEidSTATE);
    }

    public DireccionPK getDireccionPK() {
        return direccionPK;
    }

    public void setDireccionPK(DireccionPK direccionPK) {
        this.direccionPK = direccionPK;
    }

    public String getPrimerDigito() {
        return primerDigito;
    }

    public void setPrimerDigito(String primerDigito) {
        this.primerDigito = primerDigito;
    }

    public String getSegundoDigito() {
        return segundoDigito;
    }

    public void setSegundoDigito(String segundoDigito) {
        this.segundoDigito = segundoDigito;
    }

    public short getTercerDigito() {
        return tercerDigito;
    }

    public void setTercerDigito(short tercerDigito) {
        this.tercerDigito = tercerDigito;
    }

    public String getAdicional() {
        return adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    @XmlTransient
    public Collection<LoginAdministrador> getLoginAdministradorCollection() {
        return loginAdministradorCollection;
    }

    public void setLoginAdministradorCollection(Collection<LoginAdministrador> loginAdministradorCollection) {
        this.loginAdministradorCollection = loginAdministradorCollection;
    }

    public TypeAddress getTYPEADDRESSidTYPEADDRESS() {
        return tYPEADDRESSidTYPEADDRESS;
    }

    public void setTYPEADDRESSidTYPEADDRESS(TypeAddress tYPEADDRESSidTYPEADDRESS) {
        this.tYPEADDRESSidTYPEADDRESS = tYPEADDRESSidTYPEADDRESS;
    }

    public GpsCoordinates getGpsCoordinatesidgpsCoordinated() {
        return gpsCoordinatesidgpsCoordinated;
    }

    public void setGpsCoordinatesidgpsCoordinated(GpsCoordinates gpsCoordinatesidgpsCoordinated) {
        this.gpsCoordinatesidgpsCoordinated = gpsCoordinatesidgpsCoordinated;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @XmlTransient
    public Collection<Sucursal> getSucursalCollection() {
        return sucursalCollection;
    }

    public void setSucursalCollection(Collection<Sucursal> sucursalCollection) {
        this.sucursalCollection = sucursalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (direccionPK != null ? direccionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Direccion)) {
            return false;
        }
        Direccion other = (Direccion) object;
        if ((this.direccionPK == null && other.direccionPK != null) || (this.direccionPK != null && !this.direccionPK.equals(other.direccionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Direccion[ direccionPK=" + direccionPK + " ]";
    }

 

    @XmlTransient
    public Collection<Pedido> getPedidoCollection() {
        return pedidoCollection;
    }

    public void setPedidoCollection(Collection<Pedido> pedidoCollection) {
        this.pedidoCollection = pedidoCollection;
    }

   
   
  
    
}
