/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class DireccionPK implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idDireccion")
    private int idDireccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CITY_idCITY")
    private int cITYidCITY;
    @Basic(optional = false)
    @NotNull
    @Column(name = "CITY_STATE_idSTATE")
    private int cITYSTATEidSTATE;

    public DireccionPK() {
    }

    public DireccionPK(int idDireccion, int cITYidCITY, int cITYSTATEidSTATE) {
        this.idDireccion = idDireccion;
        this.cITYidCITY = cITYidCITY;
        this.cITYSTATEidSTATE = cITYSTATEidSTATE;
    }

    public int getIdDireccion() {
        return idDireccion;
    }

    public void setIdDireccion(int idDireccion) {
        this.idDireccion = idDireccion;
    }

    public int getCITYidCITY() {
        return cITYidCITY;
    }

    public void setCITYidCITY(int cITYidCITY) {
        this.cITYidCITY = cITYidCITY;
    }

    public int getCITYSTATEidSTATE() {
        return cITYSTATEidSTATE;
    }

    public void setCITYSTATEidSTATE(int cITYSTATEidSTATE) {
        this.cITYSTATEidSTATE = cITYSTATEidSTATE;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idDireccion;
        hash += (int) cITYidCITY;
        hash += (int) cITYSTATEidSTATE;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DireccionPK)) {
            return false;
        }
        DireccionPK other = (DireccionPK) object;
        if (this.idDireccion != other.idDireccion) {
            return false;
        }
        if (this.cITYidCITY != other.cITYidCITY) {
            return false;
        }
        if (this.cITYSTATEidSTATE != other.cITYSTATEidSTATE) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.DireccionPK[ idDireccion=" + idDireccion + ", cITYidCITY=" + cITYidCITY + ", cITYSTATEidSTATE=" + cITYSTATEidSTATE + " ]";
    }
    
}
