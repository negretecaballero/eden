/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "franquicia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Franquicia.findAll", query = "SELECT f FROM Franquicia f"),
    @NamedQuery(name = "Franquicia.findByType",query ="SELECT f from Franquicia f where f.idSubtype.idTipo = :type"),
    @NamedQuery(name = "Franquicia.findByTypeName",query="SELECT f from Franquicia f where f.idSubtype.idTipo.nombre = :name"),
    @NamedQuery(name = "Franquicia.findByUsername", query="SELECT f FROM Franquicia f WHERE f.loginAdministradorusername.username = :username"),
    @NamedQuery(name = "Franquicia.findByIdfranquicia", query = "SELECT f FROM Franquicia f WHERE f.idfranquicia = :idfranquicia"),
    @NamedQuery(name = "Franquicia.findByNombre", query = "SELECT f FROM Franquicia f WHERE f.nombre = :nombre")})
public class Franquicia implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "franquicia")
    private Collection<Inventario> inventarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "franquicia")
    private Collection<Addition> additionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "franquicia")
    @javax.persistence.OrderBy("nombre ASC")
    private Collection<Categoria> categoriaCollection;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "franquicia")
    private Collection<Sale> saleCollection;
    
    
    @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")
    @ManyToOne(cascade=javax.persistence.CascadeType.ALL)
    private Imagen imagenIdimagen;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfranquicia")
    private Integer idfranquicia;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "franquicia")
    private Collection<Sucursal> sucursalCollection;
    
    @javax.persistence.JoinColumns({
    
        @javax.persistence.JoinColumn(name="subtype_idsubtype",referencedColumnName="idsubtype"),
        
        @javax.persistence.JoinColumn(name="subtype_tipo_idtipo",referencedColumnName="tipo_idtipo")
    
    })
    
    @ManyToOne(optional=false)
    private Entities.Subtype idSubtype;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username")
     private LoginAdministrador loginAdministradorusername;
    
    
    @javax.persistence.JoinColumn(name="supplier_type_idsupplier_type",referencedColumnName="idsupplier_type",insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    
    private Entities.SupplierType idSupplierType;
   
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idFranquicia")
    private java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection;
    
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idBuyer")
    private java.util.Collection<Entities.PartnershipRequest>idBuyerCollection;
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idSupplier")
    private java.util.Collection<Entities.PartnershipRequest>idSupplierCollection;
    
      @javax.persistence.OneToOne(optional=false,cascade=javax.persistence.CascadeType.ALL)
    @javax.persistence.JoinColumns({
    
        @javax.persistence.JoinColumn(name="application_nit",referencedColumnName="nit"),
        @javax.persistence.JoinColumn(name="application_cedula",referencedColumnName="cedula"),
        
    
    })
      private Entities.Application idApplication;
      
      public Entities.Application getIdApplication(){
      
      return this.idApplication;
      
      }
      
      public void setIdApplication(Entities.Application idApplication){
      
          this.idApplication=idApplication;
      
      }
      
 
    
    public java.util.Collection<Entities.PartnershipRequest>getIdSupplierCollection(){
    
        return this.idSupplierCollection;
    
    }
    
    public void setIdSupplierCollection(java.util.Collection<Entities.PartnershipRequest>idSupplierCollection){
    
        this.idSupplierCollection=idSupplierCollection;
    
    }
    
    public java.util.Collection<Entities.PartnershipRequest> getIdBuyerCollection(){
    
    return this.idBuyerCollection;
    
    }
    
    public void setIdBuyerCollection(java.util.Collection<Entities.PartnershipRequest>idSupplierCollection){
    
        this.idSupplierCollection=idSupplierCollection;
    
    }
    
   
    
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.QualificationVariableHasRate>getQualificationVariableHasRateCollection(){
    
    return this.qualificationVariableHasRateCollection;
    
    }
    
    public void setQualificationVariableHasRateCollection(java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection){
    
    this.qualificationVariableHasRateCollection=qualificationVariableHasRateCollection;
    
    }
    
    
    public Entities.SupplierType getIdSupplierType(){
    
    return this.idSupplierType;
    
    }
    
    public void setIdSupplierType(Entities.SupplierType idSupplierType){
    
        this.idSupplierType=idSupplierType;
    
    }
   

    public Collection<Entities.Inventario>getInventarioCollection(){
    
    return this.inventarioCollection;
    
    }
    
    
    
    
    public Collection<Entities.Addition>getAdditionCollection(){
    
    return this.additionCollection;
    
    }
    
    public void setAdditionCollection(Collection<Entities.Addition>additionCollection){
    
        this.additionCollection=additionCollection;
    
    }
    
    public Franquicia() {
    }

    public Franquicia(Integer idfranquicia) {
        this.idfranquicia = idfranquicia;
    }

    public Integer getIdfranquicia() {
        return idfranquicia;
    }

    public void setIdfranquicia(Integer idfranquicia) {
        this.idfranquicia = idfranquicia;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Sucursal> getSucursalCollection() {
        return sucursalCollection;
    }

    public void setSucursalCollection(Collection<Sucursal> sucursalCollection) {
        this.sucursalCollection = sucursalCollection;
    }

    public Entities.Subtype getIdSubtype() {
        return this.idSubtype;
    }

    public void setIdSubtype(Entities.Subtype idSubtype) {
        this.idSubtype = idSubtype;
    }

    public LoginAdministrador getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(LoginAdministrador loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfranquicia != null ? idfranquicia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Franquicia)) {
            return false;
        }
        Franquicia other = (Franquicia) object;
        if ((this.idfranquicia == null && other.idfranquicia != null) || (this.idfranquicia != null && !this.idfranquicia.equals(other.idfranquicia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Franquicia[ idfranquicia=" + idfranquicia + " ]";
    }

    public Imagen getImagenIdimagen() {
        return imagenIdimagen;
    }

    public void setImagenIdimagen(Imagen imagenIdimagen) {
        this.imagenIdimagen = imagenIdimagen;
    }

    @XmlTransient
    public Collection<Sale> getSaleCollection() {
        return saleCollection;
    }

    public void setSaleCollection(Collection<Sale> saleCollection) {
        this.saleCollection = saleCollection;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection() {
        return categoriaCollection;
    }

    public void setCategoriaCollection(Collection<Categoria> categoriaCollection) {
        this.categoriaCollection = categoriaCollection;
    }

   

    public void setInventarioCollection(Collection<Inventario> inventarioCollection) {
        this.inventarioCollection = inventarioCollection;
    }

   
    
}
