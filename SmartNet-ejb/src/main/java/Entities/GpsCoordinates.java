/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "gpsCoordinates")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GpsCoordinates.findAll", query = "SELECT g FROM GpsCoordinates g"),
    @NamedQuery(name="GpsCoordinates.findByCoordinates",query="SELECT g FROM GpsCoordinates g where g.latitude = :latitude AND g.longitude = :longitude"),
    @NamedQuery(name = "GpsCoordinates.findByIdgpsCoordinated", query = "SELECT g FROM GpsCoordinates g WHERE g.idgpsCoordinated = :idgpsCoordinated"),
    @NamedQuery(name = "GpsCoordinates.findByLatitude", query = "SELECT g FROM GpsCoordinates g WHERE g.latitude = :latitude"),
    @NamedQuery(name = "GpsCoordinates.findByLongitude", query = "SELECT g FROM GpsCoordinates g WHERE g.longitude = :longitude")})
public class GpsCoordinates implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgpsCoordinated")
    private Integer idgpsCoordinated;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    
    @OneToMany(mappedBy = "gpsCoordinatesidgpsCoordinated")
    private Collection<Direccion> direccionCollection;

    public GpsCoordinates() {
    }

    public GpsCoordinates(Integer idgpsCoordinated) {
        this.idgpsCoordinated = idgpsCoordinated;
    }

    public GpsCoordinates(Integer idgpsCoordinated, double latitude, double longitude) {
        this.idgpsCoordinated = idgpsCoordinated;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Integer getIdgpsCoordinated() {
        return idgpsCoordinated;
    }

    public void setIdgpsCoordinated(Integer idgpsCoordinated) {
        this.idgpsCoordinated = idgpsCoordinated;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection() {
        return direccionCollection;
    }

    public void setDireccionCollection(Collection<Direccion> direccionCollection) {
        this.direccionCollection = direccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgpsCoordinated != null ? idgpsCoordinated.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GpsCoordinates)) {
            return false;
        }
        GpsCoordinates other = (GpsCoordinates) object;
        if ((this.idgpsCoordinated == null && other.idgpsCoordinated != null) || (this.idgpsCoordinated != null && !this.idgpsCoordinated.equals(other.idgpsCoordinated))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.GpsCoordinates[ idgpsCoordinated=" + idgpsCoordinated + " ]";
    }
    
}
