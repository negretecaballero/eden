/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "imagen")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imagen.findAll", query = "SELECT i FROM Imagen i"),
    @NamedQuery(name = "Imagen.findByIdimagen", query = "SELECT i FROM Imagen i WHERE i.idimagen = :idimagen"),
    @NamedQuery(name = "Imagen.findByExtension", query = "SELECT i FROM Imagen i WHERE i.extension = :extension"),
   })
public class Imagen implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "imagen")
    private byte[] imagen;
    @OneToMany(mappedBy = "imagenIdimagen")
    private Collection<Application> applicationCollection;
  
    
    @javax.persistence.OneToOne(mappedBy="logo", cascade=javax.persistence.CascadeType.ALL)
  private Entities.Supplier idSupplier;
   
    public Entities.Supplier getIdSupplier(){
    
        return this.idSupplier;
    
    }
    
    public void setIdSupplier(Entities.Supplier idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    @javax.persistence.OneToOne(mappedBy="logo",cascade=javax.persistence.CascadeType.ALL)
    private Entities.SupplierApplication idSupplierApplication;
    
    public Entities.SupplierApplication getIdSupplierApplication(){
    
    return this.idSupplierApplication;
    
    }
    
    public void setIdSupplierApplication(Entities.SupplierApplication idSupplierApplication){
    
        this.idSupplierApplication=idSupplierApplication;
    
    }
    
    
    @javax.persistence.ManyToMany(mappedBy="imagenCollection",cascade=javax.persistence.CascadeType.ALL)
    private java.util.Collection<Entities.SupplierProductCategory>supplierProductCategoryCollection;
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.SupplierProductCategory>getSupplierProductCategoryCollection(){
    
        return this.supplierProductCategoryCollection;
    
    }
    
    public void setSupplierProductCategoryCollection(java.util.Collection<Entities.SupplierProductCategory>supplierProductCategoryCollection){
    
        this.supplierProductCategoryCollection=supplierProductCategoryCollection;
    
    }
    
    
    @OneToMany(mappedBy = "imagenIdimagen")
    private Collection<Franquicia> franquiciaCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idimagen")
    private Integer idimagen;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "extension")
    private String extension;
    @ManyToMany(mappedBy = "imagenCollection")
    private Collection<Producto> productoCollection;
   @ManyToMany(mappedBy = "imagenCollection")
    private Collection<Sucursal> sucursalCollection;
    @ManyToMany(mappedBy = "imagenCollection")
    private Collection<Menu> menuCollection;
    @ManyToMany(mappedBy = "imagenCollection")
    private Collection<Categoria> categoriaCollection;
    @ManyToMany(mappedBy = "imagenCollection")
    private Collection<Sale> saleCollection;
      @OneToMany(cascade = CascadeType.ALL, mappedBy = "imagenIdimagen")
    private Collection<LoginAdministrador> loginAdministradorCollection;
    @ManyToMany(mappedBy="imagenCollection")
    private Collection<Addition>additionCollection;
    
    public Collection<Addition>getAdditionCollection(){
    
        return this.additionCollection;
    
    }
    
    public void setAdditionCollection(Collection<Addition>additionCollection){
    
        this.additionCollection=additionCollection;
    
    }

    public Imagen() {
    }

    public Imagen(Integer idimagen) {
        this.idimagen = idimagen;
    }

    public Imagen(Integer idimagen, byte[] imagen, String extension) {
        this.idimagen = idimagen;
        this.imagen = imagen;
        this.extension = extension;
    }

    public Collection<LoginAdministrador> getLoginAdminstradorCollection(){
    
        return this.loginAdministradorCollection;
    
    }
    
    public void setLoginAdministradorCollection(Collection<LoginAdministrador>loginAdministradorCollection){
    
    this.loginAdministradorCollection=loginAdministradorCollection;
    
    }
    
    public Integer getIdimagen() {
        return idimagen;
    }

    public void setIdimagen(Integer idimagen) {
        this.idimagen = idimagen;
    }


    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @XmlTransient
    public Collection<Producto> getProductoCollection() {
        return productoCollection;
    }

    public void setProductoCollection(Collection<Producto> productoCollection) {
        this.productoCollection = productoCollection;
    }

    @XmlTransient
    public Collection<Sucursal> getSucursalCollection() {
        return sucursalCollection;
    }

    public void setSucursalCollection(Collection<Sucursal> sucursalCollection) {
        this.sucursalCollection = sucursalCollection;
    }

    @XmlTransient
    public Collection<Menu> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Collection<Menu> menuCollection) {
        this.menuCollection = menuCollection;
    }

    @XmlTransient
    public Collection<Categoria> getCategoriaCollection() {
        return categoriaCollection;
    }

    public void setCategoriaCollection(Collection<Categoria> categoriaCollection) {
        this.categoriaCollection = categoriaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idimagen != null ? idimagen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imagen)) {
            return false;
        }
        Imagen other = (Imagen) object;
        if ((this.idimagen == null && other.idimagen != null) || (this.idimagen != null && !this.idimagen.equals(other.idimagen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Imagen[ idimagen=" + idimagen + " ]";
    }





 

    @XmlTransient
    public Collection<Franquicia> getFranquiciaCollection() {
        return franquiciaCollection;
    }

    public void setFranquiciaCollection(Collection<Franquicia> franquiciaCollection) {
        this.franquiciaCollection = franquiciaCollection;
    }



    @XmlTransient
    public Collection<Sale> getSaleCollection() {
        return saleCollection;
    }

    public void setSaleCollection(Collection<Sale> saleCollection) {
        this.saleCollection = saleCollection;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    @XmlTransient
    public Collection<Application> getApplicationCollection() {
        return applicationCollection;
    }

    public void setApplicationCollection(Collection<Application> applicationCollection) {
        this.applicationCollection = applicationCollection;
    }

   
    
   
}
  

   
   

 

   


 



    

