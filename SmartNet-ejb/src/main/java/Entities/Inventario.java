/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inventario.findAll", query = "SELECT i FROM Inventario i"),
    
    @NamedQuery(name = "Inventario.findByName",query = "SELECT i FROM Inventario i WHERE i.nombre = :name AND i.inventarioPK.franquiciaIdFranquicia = :idFranchise"),
    @NamedQuery(name = "Inventario.findByFranchise",query = "SELECT i FROM Inventario i WHERE i.inventarioPK.franquiciaIdFranquicia = :franchiseId"),
    @NamedQuery(name = "Inventario.findByIdinventario", query = "SELECT i FROM Inventario i WHERE i.inventarioPK.idinventario = :idinventario"),
    @NamedQuery(name = "Inventario.findByNombre", query = "SELECT i FROM Inventario i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Inventario.findByHabilitado", query = "SELECT i FROM Inventario i WHERE i.habilitado = :habilitado")})
   public class Inventario implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventario")
    private Collection<InventarioHasSucursal> inventarioHasSucursalCollection;
    @JoinColumn(name = "franquicia_idfranquicia", referencedColumnName = "idfranquicia", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Franquicia franquicia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventario")
    private Collection<AdditionConsumesInventario> additionConsumesInventarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventario")
    private Collection<SimilarInventario> similarInventarioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventario1")
    private Collection<SimilarInventario> similarInventarioCollection1;
    @JoinTable(name = "similar_inventario", joinColumns = {
        @JoinColumn(name = "inventario_idinventario", referencedColumnName = "idinventario"),
        @JoinColumn(name = "inventario_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia")}, inverseJoinColumns = {
        @JoinColumn(name = "inventario_idinventario1", referencedColumnName = "idinventario"),
         @JoinColumn(name = "inventario_franquicia_idfranquicia1", referencedColumnName = "franquicia_idfranquicia")})
    @ManyToMany
    private Collection<Inventario> inventarioCollection;
    @ManyToMany(mappedBy = "inventarioCollection")
    private Collection<Inventario> inventarioCollection1;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InventarioPK inventarioPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
  
    @Lob
    @Size(max = 65535)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "habilitado")
    private short habilitado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inventario")
    private Collection<Compone> componeCollection;
  

    public Inventario() {
    }

    public Inventario(InventarioPK inventarioPK) {
        this.inventarioPK = inventarioPK;
    }

    public Inventario(InventarioPK inventarioPK, String nombre, double cantidad, short habilitado) {
        this.inventarioPK = inventarioPK;
        this.nombre = nombre;
        
        this.habilitado = habilitado;
    }

    public Inventario(int idinventario, int franquiciaIdFranquicia) {
        this.inventarioPK = new InventarioPK(idinventario, franquiciaIdFranquicia);
    }

    public InventarioPK getInventarioPK() {
        return inventarioPK;
    }

    public void setInventarioPK(InventarioPK inventarioPK) {
        this.inventarioPK = inventarioPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

  public Entities.Franquicia getFranquicia(){
  
  return this.franquicia;
  
  }
  
  public void setFranquicia(Entities.Franquicia franquicia){
  
      this.franquicia=franquicia;
  
  }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public short getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(short habilitado) {
        this.habilitado = habilitado;
    }

    @XmlTransient
    public Collection<Compone> getComponeCollection() {
        return componeCollection;
    }

    public void setComponeCollection(Collection<Compone> componeCollection) {
        this.componeCollection = componeCollection;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inventarioPK != null ? inventarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inventario)) {
            return false;
        }
        Inventario other = (Inventario) object;
        if ((this.inventarioPK == null && other.inventarioPK != null) || (this.inventarioPK != null && !this.inventarioPK.equals(other.inventarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Inventario[ inventarioPK=" + inventarioPK + " ]";
    }

    @XmlTransient
    public Collection<Inventario> getInventarioCollection() {
        return inventarioCollection;
    }

    public void setInventarioCollection(Collection<Inventario> inventarioCollection) {
        this.inventarioCollection = inventarioCollection;
    }

    @XmlTransient
    public Collection<Inventario> getInventarioCollection1() {
        return inventarioCollection1;
    }

    public void setInventarioCollection1(Collection<Inventario> inventarioCollection1) {
        this.inventarioCollection1 = inventarioCollection1;
    }

    @XmlTransient
    public Collection<SimilarInventario> getSimilarInventarioCollection() {
        return similarInventarioCollection;
    }

    public void setSimilarInventarioCollection(Collection<SimilarInventario> similarInventarioCollection) {
        this.similarInventarioCollection = similarInventarioCollection;
    }

    @XmlTransient
    public Collection<SimilarInventario> getSimilarInventarioCollection1() {
        return similarInventarioCollection1;
    }

    public void setSimilarInventarioCollection1(Collection<SimilarInventario> similarInventarioCollection1) {
        this.similarInventarioCollection1 = similarInventarioCollection1;
    }

    @XmlTransient
    public Collection<AdditionConsumesInventario> getAdditionConsumesInventarioCollection() {
        return additionConsumesInventarioCollection;
    }

    public void setAdditionConsumesInventarioCollection(Collection<AdditionConsumesInventario> additionConsumesInventarioCollection) {
        this.additionConsumesInventarioCollection = additionConsumesInventarioCollection;
    }

    @XmlTransient
    public Collection<InventarioHasSucursal> getInventarioHasSucursalCollection() {
        return inventarioHasSucursalCollection;
    }

    public void setInventarioHasSucursalCollection(Collection<InventarioHasSucursal> inventarioHasSucursalCollection) {
        this.inventarioHasSucursalCollection = inventarioHasSucursalCollection;
    }

    
}
