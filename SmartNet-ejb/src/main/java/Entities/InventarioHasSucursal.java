/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "inventario_has_sucursal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InventarioHasSucursal.findAll", query = "SELECT i FROM InventarioHasSucursal i"),
    @NamedQuery(name = "InventarioHasSucursal.findByInventarioIdinventario", query = "SELECT i FROM InventarioHasSucursal i WHERE i.inventarioHasSucursalPK.inventarioIdinventario = :inventarioIdinventario"),
    @NamedQuery(name = "InventarioHasSucursal.findByInventarioFranquiciaIdfranquicia", query = "SELECT i FROM InventarioHasSucursal i WHERE i.inventarioHasSucursalPK.inventarioFranquiciaIdfranquicia = :inventarioFranquiciaIdfranquicia"),
    @NamedQuery(name = "InventarioHasSucursal.findBySucursalIdsucursal", query = "SELECT i FROM InventarioHasSucursal i WHERE i.inventarioHasSucursalPK.sucursalIdsucursal = :sucursalIdsucursal"),
    @NamedQuery(name = "InventarioHasSucursal.findBySucursalPK",query = "SELECT i from InventarioHasSucursal i where i.inventarioHasSucursalPK.sucursalFranquiciaIdfranquicia = :idFranchise AND i.inventarioHasSucursalPK.sucursalIdsucursal = :idSucursal"),
    @NamedQuery(name = "InventarioHasSucursal.findBySucursalFranquiciaIdfranquicia", query = "SELECT i FROM InventarioHasSucursal i WHERE i.inventarioHasSucursalPK.sucursalFranquiciaIdfranquicia = :sucursalFranquiciaIdfranquicia"),
    @NamedQuery(name = "InventarioHasSucursal.findByQuantity", query = "SELECT i FROM InventarioHasSucursal i WHERE i.quantity = :quantity")})
public class InventarioHasSucursal implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected InventarioHasSucursalPK inventarioHasSucursalPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantity")
    private Double quantity;
    @JoinColumns({
        @JoinColumn(name = "inventario_idinventario", referencedColumnName = "idinventario", insertable = false, updatable = false),
        @JoinColumn(name = "inventario_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Inventario inventario;
    @JoinColumns({
        @JoinColumn(name = "sucursal_idsucursal", referencedColumnName = "idsucursal", insertable = false, updatable = false),
        @JoinColumn(name = "sucursal_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Sucursal sucursal;

    public InventarioHasSucursal() {
    }

    public InventarioHasSucursal(InventarioHasSucursalPK inventarioHasSucursalPK) {
        this.inventarioHasSucursalPK = inventarioHasSucursalPK;
    }

    public InventarioHasSucursal(int inventarioIdinventario, int inventarioFranquiciaIdfranquicia, int sucursalIdsucursal, int sucursalFranquiciaIdfranquicia) {
        this.inventarioHasSucursalPK = new InventarioHasSucursalPK(inventarioIdinventario, inventarioFranquiciaIdfranquicia, sucursalIdsucursal, sucursalFranquiciaIdfranquicia);
    }

    public InventarioHasSucursalPK getInventarioHasSucursalPK() {
        return inventarioHasSucursalPK;
    }

    public void setInventarioHasSucursalPK(InventarioHasSucursalPK inventarioHasSucursalPK) {
        this.inventarioHasSucursalPK = inventarioHasSucursalPK;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (inventarioHasSucursalPK != null ? inventarioHasSucursalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventarioHasSucursal)) {
            return false;
        }
        InventarioHasSucursal other = (InventarioHasSucursal) object;
        if ((this.inventarioHasSucursalPK == null && other.inventarioHasSucursalPK != null) || (this.inventarioHasSucursalPK != null && !this.inventarioHasSucursalPK.equals(other.inventarioHasSucursalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
   
        return this.inventarioHasSucursalPK.toString();
    }
    
}
