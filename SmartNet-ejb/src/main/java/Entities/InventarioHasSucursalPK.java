/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class InventarioHasSucursalPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_idinventario")
    private int inventarioIdinventario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_franquicia_idfranquicia")
    private int inventarioFranquiciaIdfranquicia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sucursal_idsucursal")
    private int sucursalIdsucursal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sucursal_franquicia_idfranquicia")
    private int sucursalFranquiciaIdfranquicia;

    public InventarioHasSucursalPK() {
    }

    public InventarioHasSucursalPK(int inventarioIdinventario, int inventarioFranquiciaIdfranquicia, int sucursalIdsucursal, int sucursalFranquiciaIdfranquicia) {
        this.inventarioIdinventario = inventarioIdinventario;
        this.inventarioFranquiciaIdfranquicia = inventarioFranquiciaIdfranquicia;
        this.sucursalIdsucursal = sucursalIdsucursal;
        this.sucursalFranquiciaIdfranquicia = sucursalFranquiciaIdfranquicia;
    }

    public int getInventarioIdinventario() {
        return inventarioIdinventario;
    }

    public void setInventarioIdinventario(int inventarioIdinventario) {
        this.inventarioIdinventario = inventarioIdinventario;
    }

    public int getInventarioFranquiciaIdfranquicia() {
        return inventarioFranquiciaIdfranquicia;
    }

    public void setInventarioFranquiciaIdfranquicia(int inventarioFranquiciaIdfranquicia) {
        this.inventarioFranquiciaIdfranquicia = inventarioFranquiciaIdfranquicia;
    }

    public int getSucursalIdsucursal() {
        return sucursalIdsucursal;
    }

    public void setSucursalIdsucursal(int sucursalIdsucursal) {
        this.sucursalIdsucursal = sucursalIdsucursal;
    }

    public int getSucursalFranquiciaIdfranquicia() {
        return sucursalFranquiciaIdfranquicia;
    }

    public void setSucursalFranquiciaIdfranquicia(int sucursalFranquiciaIdfranquicia) {
        this.sucursalFranquiciaIdfranquicia = sucursalFranquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) inventarioIdinventario;
        hash += (int) inventarioFranquiciaIdfranquicia;
        hash += (int) sucursalIdsucursal;
        hash += (int) sucursalFranquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventarioHasSucursalPK)) {
            return false;
        }
        InventarioHasSucursalPK other = (InventarioHasSucursalPK) object;
        if (this.inventarioIdinventario != other.inventarioIdinventario) {
            return false;
        }
        if (this.inventarioFranquiciaIdfranquicia != other.inventarioFranquiciaIdfranquicia) {
            return false;
        }
        if (this.sucursalIdsucursal != other.sucursalIdsucursal) {
            return false;
        }
        if (this.sucursalFranquiciaIdfranquicia != other.sucursalFranquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        System.out.print("Before Return "+(this.inventarioIdinventario+","+this.inventarioFranquiciaIdfranquicia+","+this.sucursalIdsucursal+","+this.sucursalFranquiciaIdfranquicia));
        
        return (this.inventarioIdinventario+","+this.inventarioFranquiciaIdfranquicia+","+this.sucursalIdsucursal+","+this.sucursalFranquiciaIdfranquicia);
    }
    
}
