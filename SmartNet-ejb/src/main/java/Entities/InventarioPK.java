/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class InventarioPK implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idinventario")
    private int idinventario;
   
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="franquicia_idfranquicia")        
    private int franquiciaIdFranquicia;
    public InventarioPK() {
    }

    public InventarioPK(int idinventario, int franquiciaIdFranquicia) {
        this.idinventario = idinventario;
        this.franquiciaIdFranquicia=franquiciaIdFranquicia;
      }

    public int getIdinventario() {
        return idinventario;
    }

    public void setIdinventario(int idinventario) {
        this.idinventario = idinventario;
    }

    public int getFranquiciaIdFranquicia(){
    
    return this.franquiciaIdFranquicia;
    
    }
    
    public void setFranquiciaIdFranquicia(int franquiciaIdFranquicia){
    
    this.franquiciaIdFranquicia=franquiciaIdFranquicia;
    
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idinventario;
        hash += (int) franquiciaIdFranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventarioPK)) {
            return false;
        }
        InventarioPK other = (InventarioPK) object;
        if (this.idinventario != other.idinventario) {
            return false;
        }
        if (this.franquiciaIdFranquicia != other.franquiciaIdFranquicia) {
            return false;
        }
       
        return true;
    }

    @Override
    public String toString() {
        return "Entities.InventarioPK[ idinventario=" + idinventario + ", franquiciaIdFranquicia="+franquiciaIdFranquicia+" ]";
    }
    
}
