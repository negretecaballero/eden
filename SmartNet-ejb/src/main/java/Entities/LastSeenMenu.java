/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.EdenLastSeen;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "last_seen_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LastSeenMenu.findAll", query = "SELECT l FROM LastSeenMenu l"),
    @NamedQuery(name = "LastSeenMenu.findByLoginAdministradorusername", query = "SELECT l FROM LastSeenMenu l WHERE l.lastSeenMenuPK.loginAdministradorusername = :loginAdministradorusername"),
    @NamedQuery(name = "LastSeenMenu.findByMenuIdmenu", query = "SELECT l FROM LastSeenMenu l WHERE l.lastSeenMenuPK.menuIdmenu = :menuIdmenu"),
    @NamedQuery(name = "LastSeenMenu.findByDateTime", query = "SELECT l FROM LastSeenMenu l WHERE l.dateTime = :dateTime")})
public class LastSeenMenu extends EdenLastSeen implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LastSeenMenuPK lastSeenMenuPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private LoginAdministrador loginAdministrador;
    @JoinColumn(name = "menu_idmenu", referencedColumnName = "idmenu", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;

    public LastSeenMenu() {
    }

    public LastSeenMenu(LastSeenMenuPK lastSeenMenuPK) {
        this.lastSeenMenuPK = lastSeenMenuPK;
    }

    public LastSeenMenu(LastSeenMenuPK lastSeenMenuPK, Date dateTime) {
        this.lastSeenMenuPK = lastSeenMenuPK;
        this.dateTime = dateTime;
    }

    public LastSeenMenu(String loginAdministradorusername, int menuIdmenu) {
        this.lastSeenMenuPK = new LastSeenMenuPK(loginAdministradorusername, menuIdmenu);
    }

    public LastSeenMenuPK getLastSeenMenuPK() {
        return lastSeenMenuPK;
    }

    public void setLastSeenMenuPK(LastSeenMenuPK lastSeenMenuPK) {
        this.lastSeenMenuPK = lastSeenMenuPK;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public LoginAdministrador getLoginAdministrador() {
        return loginAdministrador;
    }

    public void setLoginAdministrador(LoginAdministrador loginAdministrador) {
        this.loginAdministrador = loginAdministrador;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lastSeenMenuPK != null ? lastSeenMenuPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LastSeenMenu)) {
            return false;
        }
        LastSeenMenu other = (LastSeenMenu) object;
        if ((this.lastSeenMenuPK == null && other.lastSeenMenuPK != null) || (this.lastSeenMenuPK != null && !this.lastSeenMenuPK.equals(other.lastSeenMenuPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.LastSeenMenu[ lastSeenMenuPK=" + lastSeenMenuPK + " ]";
    }
    
}
