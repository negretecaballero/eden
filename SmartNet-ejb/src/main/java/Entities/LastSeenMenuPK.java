/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class LastSeenMenuPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "loginAdministrador_username")
    private String loginAdministradorusername;
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_idmenu")
    private int menuIdmenu;

    public LastSeenMenuPK() {
    }

    public LastSeenMenuPK(String loginAdministradorusername, int menuIdmenu) {
        this.loginAdministradorusername = loginAdministradorusername;
        this.menuIdmenu = menuIdmenu;
    }

    public String getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(String loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public int getMenuIdmenu() {
        return menuIdmenu;
    }

    public void setMenuIdmenu(int menuIdmenu) {
        this.menuIdmenu = menuIdmenu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginAdministradorusername != null ? loginAdministradorusername.hashCode() : 0);
        hash += (int) menuIdmenu;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LastSeenMenuPK)) {
            return false;
        }
        LastSeenMenuPK other = (LastSeenMenuPK) object;
        if ((this.loginAdministradorusername == null && other.loginAdministradorusername != null) || (this.loginAdministradorusername != null && !this.loginAdministradorusername.equals(other.loginAdministradorusername))) {
            return false;
        }
        if (this.menuIdmenu != other.menuIdmenu) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.LastSeenMenuPK[ loginAdministradorusername=" + loginAdministradorusername + ", menuIdmenu=" + menuIdmenu + " ]";
    }
    
}
