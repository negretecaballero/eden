/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "last_seen_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LastSeenProducto.findAll", query = "SELECT l FROM LastSeenProducto l"),
    @NamedQuery(name = "LastSeenProducto.findByLoginAdministradorusername", query = "SELECT l FROM LastSeenProducto l WHERE l.lastSeenProductoPK.loginAdministradorusername = :loginAdministradorusername"),
    @NamedQuery(name = "LastSeenProducto.findByDateTime", query = "SELECT l FROM LastSeenProducto l WHERE l.dateTime = :dateTime"),
    @NamedQuery(name = "LastSeenProducto.findByProductoIdproducto", query = "SELECT l FROM LastSeenProducto l WHERE l.lastSeenProductoPK.productoIdproducto = :productoIdproducto"),
    @NamedQuery(name = "LastSeenProducto.findByProductoCategoriaIdcategoria", query = "SELECT l FROM LastSeenProducto l WHERE l.lastSeenProductoPK.productoCategoriaIdcategoria = :productoCategoriaIdcategoria"),
    @NamedQuery(name = "LastSeenProducto.findByProductoCategoriaFranquiciaIdfranquicia", query = "SELECT l FROM LastSeenProducto l WHERE l.lastSeenProductoPK.productoCategoriaFranquiciaIdfranquicia = :productoCategoriaFranquiciaIdfranquicia")})
public class LastSeenProducto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LastSeenProductoPK lastSeenProductoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateTime;
    @JoinColumns({
        @JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto", insertable = false, updatable = false),
        @JoinColumn(name = "producto_categoria_idcategoria", referencedColumnName = "categoria_idcategoria", insertable = false, updatable = false),
        @JoinColumn(name = "producto_categoria_franquicia_idfranquicia", referencedColumnName = "categoria_franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Producto producto;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private LoginAdministrador loginAdministrador;

    public LastSeenProducto() {
    }

    public LastSeenProducto(LastSeenProductoPK lastSeenProductoPK) {
        this.lastSeenProductoPK = lastSeenProductoPK;
    }

    public LastSeenProducto(LastSeenProductoPK lastSeenProductoPK, Date dateTime) {
        this.lastSeenProductoPK = lastSeenProductoPK;
        this.dateTime = dateTime;
    }

    public LastSeenProducto(String loginAdministradorusername, int productoIdproducto, int productoCategoriaIdcategoria, int productoCategoriaFranquiciaIdfranquicia) {
        this.lastSeenProductoPK = new LastSeenProductoPK(loginAdministradorusername, productoIdproducto, productoCategoriaIdcategoria, productoCategoriaFranquiciaIdfranquicia);
    }

    public LastSeenProductoPK getLastSeenProductoPK() {
        return lastSeenProductoPK;
    }

    public void setLastSeenProductoPK(LastSeenProductoPK lastSeenProductoPK) {
        this.lastSeenProductoPK = lastSeenProductoPK;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public LoginAdministrador getLoginAdministrador() {
        return loginAdministrador;
    }

    public void setLoginAdministrador(LoginAdministrador loginAdministrador) {
        this.loginAdministrador = loginAdministrador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lastSeenProductoPK != null ? lastSeenProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LastSeenProducto)) {
            return false;
        }
        LastSeenProducto other = (LastSeenProducto) object;
        if ((this.lastSeenProductoPK == null && other.lastSeenProductoPK != null) || (this.lastSeenProductoPK != null && !this.lastSeenProductoPK.equals(other.lastSeenProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.LastSeenProducto[ lastSeenProductoPK=" + lastSeenProductoPK + " ]";
    }
    
}
