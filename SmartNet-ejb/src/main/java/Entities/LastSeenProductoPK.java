/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class LastSeenProductoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "loginAdministrador_username")
    private String loginAdministradorusername;
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_categoria_idcategoria")
    private int productoCategoriaIdcategoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_categoria_franquicia_idfranquicia")
    private int productoCategoriaFranquiciaIdfranquicia;

    public LastSeenProductoPK() {
    }

    public LastSeenProductoPK(String loginAdministradorusername, int productoIdproducto, int productoCategoriaIdcategoria, int productoCategoriaFranquiciaIdfranquicia) {
        this.loginAdministradorusername = loginAdministradorusername;
        this.productoIdproducto = productoIdproducto;
        this.productoCategoriaIdcategoria = productoCategoriaIdcategoria;
        this.productoCategoriaFranquiciaIdfranquicia = productoCategoriaFranquiciaIdfranquicia;
    }

    public String getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(String loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getProductoCategoriaIdcategoria() {
        return productoCategoriaIdcategoria;
    }

    public void setProductoCategoriaIdcategoria(int productoCategoriaIdcategoria) {
        this.productoCategoriaIdcategoria = productoCategoriaIdcategoria;
    }

    public int getProductoCategoriaFranquiciaIdfranquicia() {
        return productoCategoriaFranquiciaIdfranquicia;
    }

    public void setProductoCategoriaFranquiciaIdfranquicia(int productoCategoriaFranquiciaIdfranquicia) {
        this.productoCategoriaFranquiciaIdfranquicia = productoCategoriaFranquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (loginAdministradorusername != null ? loginAdministradorusername.hashCode() : 0);
        hash += (int) productoIdproducto;
        hash += (int) productoCategoriaIdcategoria;
        hash += (int) productoCategoriaFranquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LastSeenProductoPK)) {
            return false;
        }
        LastSeenProductoPK other = (LastSeenProductoPK) object;
        if ((this.loginAdministradorusername == null && other.loginAdministradorusername != null) || (this.loginAdministradorusername != null && !this.loginAdministradorusername.equals(other.loginAdministradorusername))) {
            return false;
        }
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.productoCategoriaIdcategoria != other.productoCategoriaIdcategoria) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdfranquicia != other.productoCategoriaFranquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.LastSeenProductoPK[ loginAdministradorusername=" + loginAdministradorusername + ", productoIdproducto=" + productoIdproducto + ", productoCategoriaIdcategoria=" + productoCategoriaIdcategoria + ", productoCategoriaFranquiciaIdfranquicia=" + productoCategoriaFranquiciaIdfranquicia + " ]";
    }
    
}
