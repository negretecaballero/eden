/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Validators.MailValidator;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "loginAdministrador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LoginAdministrador.findAll", query = "SELECT l FROM LoginAdministrador l"),
    @NamedQuery(name = "LoginAdministrador.findByUsername", query = "SELECT l FROM LoginAdministrador l WHERE l.username = :username"),
    @NamedQuery(name = "LoginAdministrador.findByPassword", query = "SELECT l FROM LoginAdministrador l WHERE l.password = :password"),
    @NamedQuery(name = "LoginAdministrador.findByName", query = "SELECT l FROM LoginAdministrador l WHERE l.name = :name"),
    @NamedQuery(name = "LoginAdministrador.findByZipCode", query = "SELECT l FROM LoginAdministrador l WHERE l.zipCode = :zipCode"),
    @NamedQuery(name = "LoginAdministrador.findByBirthday", query = "SELECT l FROM LoginAdministrador l WHERE l.birthday = :birthday"),
    @NamedQuery(name = "LoginAdministrador.findByGender", query = "SELECT l FROM LoginAdministrador l WHERE l.gender = :gender")})
public class LoginAdministrador implements Serializable {
   

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "loginAdministrador")
    private Confirmation confirmation;

  
    @JoinColumn(name="imagen_idimagen",referencedColumnName="idimagen")
    @ManyToOne(optional=false,cascade=javax.persistence.CascadeType.ALL)
    private Entities.Imagen imagenIdimagen;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministrador")
    private Collection<LastSeenMenu> lastSeenMenuCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministrador")
    private Collection<LastSeenProducto> lastSeenProductoCollection;

    @ManyToMany(mappedBy = "loginAdministradorCollection")
    private Collection<Entities.Sucursal> sucursalCollection;
   
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 64)
    @Column(name = "username")
    @MailValidator
    private String username;
    @Basic(optional = false)
    //@NotNull
    @Size(min = 1, max = 64)
    @Column(name = "password")
    private String password;
    @Size(max = 100)
    @Column(name = "name")
    private String name;
    @Size(max = 45)
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Size(max = 45)
    @Column(name = "gender")
    private String gender;
    
    @JoinTable(name = "loginAdministrador_has_DIRECCION", inverseJoinColumns = {
        @JoinColumn(name = "DIRECCION_idDireccion", referencedColumnName = "idDireccion"),
        @JoinColumn(name = "DIRECCION_CITY_idCITY", referencedColumnName = "CITY_idCITY"),
        @JoinColumn(name = "DIRECCION_CITY_STATE_idSTATE", referencedColumnName = "CITY_STATE_idSTATE")},  joinColumns= {
        @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username")})
    @ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Direccion> direccionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministradorusername")
    private Collection<Pedido> pedidoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministradorusername")
    private Collection<Phone> phoneCollection;
    @JoinColumn(name = "profession_idprofession", referencedColumnName = "idprofession")
    @ManyToOne(optional=false)
    private Profession professionIdprofession;
    
    
    @javax.persistence.ManyToMany(mappedBy="workerCollection", cascade=javax.persistence.CascadeType.ALL)
    //@javax.persistence.OrderBy("franquicia.nombre ASC")
    private java.util.Collection<Entities.Sucursal>sucursalWorkerCollection;
    
    @javax.persistence.OneToMany(mappedBy="idLoginAdministrador",cascade=javax.persistence.CascadeType.ALL)
    private java.util.Collection<Entities.Supplier>supplierCollection;
  
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.Sucursal>getSucursalWorkerCollection(){
    
    return this.sucursalWorkerCollection;
    
    }
    
    public void setSucursalWorkerCollection(java.util.Collection<Entities.Sucursal>sucursalWorkerCollection){
    
        this.sucursalWorkerCollection=sucursalWorkerCollection;
    
    }
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministrador")
    private Collection<AppGroup> appGroupCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "loginAdministradorusername")
    private Collection<Franquicia> franquiciaCollection;
    
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idLoginAdministrador")
    private java.util.Collection<Entities.Bug>bugCollection;
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.Bug>getBugCollection(){
    
        return this.bugCollection;
    
    }
    
    public void setBugCollection(java.util.Collection<Entities.Bug>bugCollection){
    
        this.bugCollection=bugCollection;
    
    }

    public LoginAdministrador() {
    }

    public LoginAdministrador(String username) {
        this.username = username;
    }

    public LoginAdministrador(String username, String password) {
        this.username = username;
        this.password = password;
    }

    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.Supplier>getSupplierCollection(){
    
        return this.supplierCollection;
    
    }
    
    public void setSupplierCollection(java.util.Collection<Entities.Supplier>supplierCollection){
    
        this.supplierCollection=supplierCollection;
    
    }
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection() {
        return direccionCollection;
    }

    public void setDireccionCollection(Collection<Direccion> direccionCollection) {
        this.direccionCollection = direccionCollection;
    }

    @XmlTransient
    public Collection<Pedido> getPedidoCollection() {
        return pedidoCollection;
    }

    public void setPedidoCollection(Collection<Pedido> pedidoCollection) {
        this.pedidoCollection = pedidoCollection;
    }

    @XmlTransient
    public Collection<Phone> getPhoneCollection() {
        return phoneCollection;
    }

    public void setPhoneCollection(Collection<Phone> phoneCollection) {
        this.phoneCollection = phoneCollection;
    }

    public Profession getProfessionIdprofession() {
        return professionIdprofession;
    }

    public void setProfessionIdprofession(Profession professionIdprofession) {
        this.professionIdprofession = professionIdprofession;
    }

    @XmlTransient
    public Collection<AppGroup> getAppGroupCollection() {
        return appGroupCollection;
    }

    public void setAppGroupCollection(Collection<AppGroup> appGroupCollection) {
        this.appGroupCollection = appGroupCollection;
    }

    @XmlTransient
    public Collection<Franquicia> getFranquiciaCollection() {
        return franquiciaCollection;
    }

    public void setFranquiciaCollection(Collection<Franquicia> franquiciaCollection) {
        this.franquiciaCollection = franquiciaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (username != null ? username.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LoginAdministrador)) {
            return false;
        }
        LoginAdministrador other = (LoginAdministrador) object;
        if ((this.username == null && other.username != null) || (this.username != null && !this.username.equals(other.username))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.LoginAdministrador[ username=" + username + " ]";
    }

   

    @XmlTransient
    public Collection<Entities.Sucursal> getSucursalCollection() {
        return this.sucursalCollection;
    }

    public void setSucursalCollection(Collection<Entities.Sucursal> sucursalCollection) {
        this.sucursalCollection = sucursalCollection;
    }

   

    @XmlTransient
    public Collection<LastSeenMenu> getLastSeenMenuCollection() {
        return lastSeenMenuCollection;
    }

    public void setLastSeenMenuCollection(Collection<LastSeenMenu> lastSeenMenuCollection) {
        this.lastSeenMenuCollection = lastSeenMenuCollection;
    }

    @XmlTransient
    public Collection<LastSeenProducto> getLastSeenProductoCollection() {
        return lastSeenProductoCollection;
    }

    public void setLastSeenProductoCollection(Collection<LastSeenProducto> lastSeenProductoCollection) {
        this.lastSeenProductoCollection = lastSeenProductoCollection;
    }

    public Entities.Imagen getImagenIdimagen() {
        return imagenIdimagen;
    }

    public void setImagenIdimagen(Entities.Imagen imagenIdimagen) {
        this.imagenIdimagen = imagenIdimagen;
    }

   

    public Confirmation getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Confirmation confirmation) {
        this.confirmation = confirmation;
    }

    
  
  

   

    

   

 
   

   


 
  

   
}
