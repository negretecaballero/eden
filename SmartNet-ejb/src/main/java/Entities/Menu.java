/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.Item;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m"),
    @NamedQuery(name = "Menu.findByIdmenu", query = "SELECT m FROM Menu m WHERE m.idmenu = :idmenu"),
    @NamedQuery(name = "Menu.findByNombre", query = "SELECT m FROM Menu m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "Menu.findByPrecio", query = "SELECT m FROM Menu m WHERE m.precio = :precio")})
public class Menu extends Item implements Serializable {
  
    
  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Collection<SaleHasMenu> saleHasMenuCollection;

    public Collection<SaleHasMenu> getSaleHasMenuCollection() {
        return saleHasMenuCollection;
    }

    public void setSaleHasMenuCollection(Collection<SaleHasMenu> saleHasMenuCollection) {
        this.saleHasMenuCollection = saleHasMenuCollection;
    }
    
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Collection<LastSeenMenu> lastSeenMenuCollection;
   
    @javax.persistence.ManyToMany(mappedBy="menuCollection")
    private Collection<Entities.Sucursal>sucursalCollection;
    
    
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
 
     
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Collection<MenuhasPEDIDO> menuhasPEDIDOCollection;
    
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmenu")
    private Integer idmenu;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
   // @javax.validation.constraints.Max(value=java.lang.Double.MAX_VALUE)  @javax.validation.constraints.Min(value=java.lang.Double.MIN_VALUE)
    @Column(name = "precio")
    private Double precio;
   @JoinTable(name = "menu_has_imagen",  inverseJoinColumns= {
        @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")},  joinColumns= {
        @JoinColumn(name = "menu_idmenu", referencedColumnName = "idmenu")})
   @ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Imagen> imagenCollection;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private Collection<Agrupa> agrupaCollection;

    public Menu() {
    }

    public Menu(Integer idmenu) {
        this.idmenu = idmenu;
    }

    public Menu(Integer idmenu, String nombre) {
        this.idmenu = idmenu;
        this.nombre = nombre;
    }

    public Integer getIdmenu() {
        return idmenu;
    }

    public void setIdmenu(Integer idmenu) {
        this.idmenu = idmenu;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }

    public Collection<Entities.Sucursal>getSucursalCollection(){
    
        return this.sucursalCollection;
    
    }
    
    public void setSucursalCollection(Collection <Entities.Sucursal>sucursalCollection){
    
        this.sucursalCollection=sucursalCollection;
    
    }
  

    @XmlTransient
    public Collection<Agrupa> getAgrupaCollection() {
        return agrupaCollection;
    }

    public void setAgrupaCollection(Collection<Agrupa> agrupaCollection) {
        this.agrupaCollection = agrupaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmenu != null ? idmenu.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.idmenu == null && other.idmenu != null) || (this.idmenu != null && !this.idmenu.equals(other.idmenu))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Menu[ idmenu=" + idmenu + " ]";
    }

    

    @XmlTransient
    public Collection<MenuhasPEDIDO> getMenuhasPEDIDOCollection() {
        return menuhasPEDIDOCollection;
    }

    public void setMenuhasPEDIDOCollection(Collection<MenuhasPEDIDO> menuhasPEDIDOCollection) {
        this.menuhasPEDIDOCollection = menuhasPEDIDOCollection;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    @XmlTransient
    public Collection<LastSeenMenu> getLastSeenMenuCollection() {
        return lastSeenMenuCollection;
    }

    public void setLastSeenMenuCollection(Collection<LastSeenMenu> lastSeenMenuCollection) {
        this.lastSeenMenuCollection = lastSeenMenuCollection;
    }

/*
    @XmlTransient
    public Collection<SaleHasMenu> getSaleHasMenuCollection() {
        return saleHasMenuCollection;
    }

    public void setSaleHasMenuCollection(Collection<SaleHasMenu> saleHasMenuCollection) {
        this.saleHasMenuCollection = saleHasMenuCollection;
    }

   
*/

  
  

  
   
  

   

   

  

  

   


   
}
