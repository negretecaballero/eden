/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "menu_has_PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MenuhasPEDIDO.findAll", query = "SELECT m FROM MenuhasPEDIDO m"),
    @NamedQuery(name = "MenuhasPEDIDO.findByMenuIdmenu", query = "SELECT m FROM MenuhasPEDIDO m WHERE m.menuhasPEDIDOPK.menuIdmenu = :menuIdmenu"),
    @NamedQuery(name = "MenuhasPEDIDO.findByPEDIDOidPEDIDO", query = "SELECT m FROM MenuhasPEDIDO m WHERE m.menuhasPEDIDOPK.pEDIDOidPEDIDO = :pEDIDOidPEDIDO"),
    @NamedQuery(name = "MenuhasPEDIDO.findByIdPedido&Sucursal",query="SELECT m FROM MenuhasPEDIDO m where m.pedido.idPEDIDO = :idPedido AND m.pedido.sucursal.sucursalPK = :sucursalPK"),
    @NamedQuery(name = "MenuhasPEDIDO.findByQuantity", query = "SELECT m FROM MenuhasPEDIDO m WHERE m.quantity = :quantity")})
public class MenuhasPEDIDO implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MenuhasPEDIDOPK menuhasPEDIDOPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    @JoinColumn(name = "menu_idmenu", referencedColumnName = "idmenu", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;
    @JoinColumn(name = "PEDIDO_idPEDIDO", referencedColumnName = "idPEDIDO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedido pedido;

    public MenuhasPEDIDO() {
    }

    public MenuhasPEDIDO(MenuhasPEDIDOPK menuhasPEDIDOPK) {
        this.menuhasPEDIDOPK = menuhasPEDIDOPK;
    }

    public MenuhasPEDIDO(MenuhasPEDIDOPK menuhasPEDIDOPK, int quantity) {
        this.menuhasPEDIDOPK = menuhasPEDIDOPK;
        this.quantity = quantity;
    }

    public MenuhasPEDIDO(int menuIdmenu, int pEDIDOidPEDIDO) {
        this.menuhasPEDIDOPK = new MenuhasPEDIDOPK(menuIdmenu, pEDIDOidPEDIDO);
    }

    public MenuhasPEDIDOPK getMenuhasPEDIDOPK() {
        return menuhasPEDIDOPK;
    }

    public void setMenuhasPEDIDOPK(MenuhasPEDIDOPK menuhasPEDIDOPK) {
        this.menuhasPEDIDOPK = menuhasPEDIDOPK;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuhasPEDIDOPK != null ? menuhasPEDIDOPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuhasPEDIDO)) {
            return false;
        }
        MenuhasPEDIDO other = (MenuhasPEDIDO) object;
        if ((this.menuhasPEDIDOPK == null && other.menuhasPEDIDOPK != null) || (this.menuhasPEDIDOPK != null && !this.menuhasPEDIDOPK.equals(other.menuhasPEDIDOPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.MenuhasPEDIDO[ menuhasPEDIDOPK=" + menuhasPEDIDOPK + " ]";
    }
    
}
