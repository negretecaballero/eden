/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class MenuhasPEDIDOPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_idmenu")
    private int menuIdmenu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PEDIDO_idPEDIDO")
    private int pEDIDOidPEDIDO;

    public MenuhasPEDIDOPK() {
    }

    public MenuhasPEDIDOPK(int menuIdmenu, int pEDIDOidPEDIDO) {
        this.menuIdmenu = menuIdmenu;
        this.pEDIDOidPEDIDO = pEDIDOidPEDIDO;
    }

    public int getMenuIdmenu() {
        return menuIdmenu;
    }

    public void setMenuIdmenu(int menuIdmenu) {
        this.menuIdmenu = menuIdmenu;
    }

    public int getPEDIDOidPEDIDO() {
        return pEDIDOidPEDIDO;
    }

    public void setPEDIDOidPEDIDO(int pEDIDOidPEDIDO) {
        this.pEDIDOidPEDIDO = pEDIDOidPEDIDO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) menuIdmenu;
        hash += (int) pEDIDOidPEDIDO;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MenuhasPEDIDOPK)) {
            return false;
        }
        MenuhasPEDIDOPK other = (MenuhasPEDIDOPK) object;
        if (this.menuIdmenu != other.menuIdmenu) {
            return false;
        }
        if (this.pEDIDOidPEDIDO != other.pEDIDOidPEDIDO) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "Entities.MenuhasPEDIDOPK[ menuIdmenu=" + menuIdmenu + ", pEDIDOidPEDIDO=" + pEDIDOidPEDIDO + " ]";
    }
    
}
