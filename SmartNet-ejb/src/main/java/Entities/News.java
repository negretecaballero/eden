/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author luisnegrete
 */

@javax.xml.bind.annotation.XmlRootElement
@javax.persistence.Entity
@javax.persistence.Table(name="News")
@javax.persistence.NamedQueries({

    @javax.persistence.NamedQuery(name="News.orderedByDate",query="SELECT n from News n ORDER BY n.date"),

    @NamedQuery(name="News.findByType",query="SELECT n from News n where n.newsTypeId.type =:type OR n.newsTypeId.type = :global AND n.tipoId = :tipo"),
    
    @NamedQuery(name="News.findByTipo",query="SELECT n from News n where n.tipoId = :null")
  
})

public class News implements java.io.Serializable{
    @javax.persistence.Id
    @javax.persistence.Basic(optional=false)
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @javax.persistence.Column(name="id")  
    private Integer id;
    
    @Temporal(TemporalType.TIMESTAMP)
    @javax.persistence.Column(name="date")
    private java.util.Date date;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=100)
    @javax.persistence.Column(name="title")
    private String title;
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1)
    @javax.persistence.Basic(optional=true)
    @javax.persistence.Column(name="body")
    private String body;
    
    @javax.persistence.JoinColumn(name="news_type_id",referencedColumnName="id")
    @javax.persistence.ManyToOne
    private Entities.NewsType newsTypeId;
    
    
    @javax.persistence.JoinColumn(name="tipo_idtipo",referencedColumnName="idtipo")
    @javax.persistence.ManyToOne
    private Entities.Tipo tipoId;
    
    public Integer getId(){
    
        return this.id;
    
    }
    
    public void setId(Integer id){
    
    this.id=id;
    
    }
    
    public void setBody(String body){
    
        this.body=body;
    
    }
    
    public String getBody(){
    
    return this.body;
    
    }
    
    public void setTitle(String title){
    
    this.title=title;
    
    }
    
    public String getTitle(){
    
    return this.title;
    
    }
    
    
    public Entities.NewsType getNewsTypeId(){
    
    return this.newsTypeId;
    
    }
    
    public void setNewsTypeId(Entities.NewsType newsTypeId){
    
    this.newsTypeId=newsTypeId;
    
    }
    
    public Entities.Tipo getTipoId(){
    
        return this.tipoId;
    
    }
    
    public void setTipoId(Entities.Tipo tipoId){
    
        this.tipoId=tipoId;
    
    }
    
    
    public News(){
    
    
    }
    
    
    public News(Integer id)
    {
    
        this.id=id;
    
    }
    
    public News(Integer id, String title){
    
        this.id=id;
        
        this.title=title;
    
    }
    
    public News(Integer id, String title,String body){
    
    this.id=id;
    
    this.title=title;
    
    this.body=body;
    
    }
    
    
    @Override
    public int hashCode(){
    
    int hash=0;
    
    hash += (id != null ? id.hashCode() : 0);
    
    return hash;
    
    }
    
    public java.util.Date getDate(){
    
        return this.date;
    
    }
    
    public void setDate(java.util.Date date){
    
        this.date=date;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final News other = (News) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
    @Override
    public String toString(){
    
    return "Entities.News [ id = "+id+" ]";
    
    }
    
}
