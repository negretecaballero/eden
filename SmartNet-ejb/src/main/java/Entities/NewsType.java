/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity
@javax.persistence.Table(name="news_type")
@XmlRootElement
@javax.persistence.NamedQueries({

@javax.persistence.NamedQuery(name="NewsType.findByType",query="SELECT n from NewsType n where n.type = :type")

})

public class NewsType implements java.io.Serializable{
    @javax.persistence.Id
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="id")
    private Integer id;
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=9)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="type")
    private String type;
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="newsTypeId")
    private java.util.Collection<Entities.News>newsCollection;
    
    
    public Integer getId(){
    
    return this.id;
    
    }
    
    public void setId(Integer id){
    
        this.id=id;
    
    }
    
    public String getType(){
    
    return this.type;
    
    }
    
    public void setType(String type){
    
    this.type=type;
    
    }
    
    public NewsType(){
    
    
    
    }
    
    public NewsType(Integer id){
    
    this.id=id;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=id;
        
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NewsType other = (NewsType) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   @javax.xml.bind.annotation.XmlTransient
   public java.util.Collection<Entities.News>getNewsCollection(){
   
   return this.newsCollection;
   
   }
   
   public void setNewsCollection(java.util.Collection<Entities.News>newsCollection){
   
   this.newsCollection=newsCollection;
   
   }
   
   @Override
   public String toString(){
   
       return "Entities.NewsType [ id = "+id+" ]";
   
   }
   
}
