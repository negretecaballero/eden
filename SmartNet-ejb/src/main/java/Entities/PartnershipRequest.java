/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import Entitites.Enum.StateEnum;
import java.util.Objects;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity
@javax.persistence.Table(name="partnership_request")
@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.NamedQueries({

    @NamedQuery(name="PartnershipRequest.findAll",query="SELECT p from PartnershipRequest p"),
    
    @NamedQuery(name="PertnershipRequest.findByState",query="SELECT p from PartnershipRequest p where p.state = :state"),

    @NamedQuery(name="PartnershipRequest.findByStateBuyer",query="SELECT p from PartnershipRequest p where p.partnershipRequestPK.idBuyer = :buyer AND p.state = :state")
        
})

public class PartnershipRequest implements java.io.Serializable{
    
    private static final long serialVersionUID=1L;
    
    @javax.persistence.Temporal(javax.persistence.TemporalType.DATE)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="date")
    private java.util.Date date;
  
    
    @javax.persistence.EmbeddedId
    private Entities.PartnershipRequestPK partnershipRequestPK;
    
    @javax.persistence.JoinColumn(name="buyer",referencedColumnName="idfranquicia", insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Franquicia idBuyer;
    
    
    @javax.persistence.JoinColumn(name="supplier",referencedColumnName="idfranquicia", insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Franquicia idSupplier;
    
    
    @javax.persistence.Column(name="state")
    @javax.persistence.Enumerated(javax.persistence.EnumType.STRING)
    private StateEnum state;
    
    public StateEnum getState(){
    
        return this.state;
    
    }
    
    public void setState(StateEnum state){
    
    this.state=state;
    
    }
    
    public Entities.PartnershipRequestPK getPartnershipRequestPK(){
    
        return this.partnershipRequestPK;
    
    }
    
    public void setPartnershipRequestPK(Entities.PartnershipRequestPK partnershipRequestPK){
    
        this.partnershipRequestPK=partnershipRequestPK;
    
    }
    
    public java.util.Date getDate(){
    
    return this.date;
    
    }
    
    public void setDate(java.util.Date date){
    
    this.date=date;
    
    }
    
    public Entities.Franquicia getIdBuyer(){
    
    return this.idBuyer;
    
    }
    
    public void setIdBuyer(Entities.Franquicia idBuyer){
    
        this.idBuyer=idBuyer;
    
    }
    
    public Entities.Franquicia getIdSupplier(){
    
       return this.idSupplier;
    
    }
    
    public void setIdSupplier(Entities.Franquicia idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    public void PartnershipRequest(){
    
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=(this.partnershipRequestPK!=null)?this.partnershipRequestPK.hashCode():0;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PartnershipRequest other = (PartnershipRequest) obj;
        if (!Objects.equals(this.partnershipRequestPK, other.partnershipRequestPK)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return this.partnershipRequestPK.toString();
    
    }
    
}
