/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable
public class PartnershipRequestPK {
    
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="buyer")
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.NotNull
    private Integer idBuyer;
    
    
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="supplier")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    private Integer idSupplier;
    
    
    public PartnershipRequestPK(){
    
    }
    
    public Integer getIdBuyer(){
    
        return this.idBuyer;
    
    }
    
    public void setIdBuyer(Integer idBuyer){
    
        this.idBuyer=idBuyer;
    
    }
    
    public Integer getIdSupplier(){
    
        return this.idSupplier;
    
    }
    
    public void setIdSupplier(Integer idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=this.idBuyer;
        
        hash+=this.idSupplier;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PartnershipRequestPK other = (PartnershipRequestPK) obj;
        if (!Objects.equals(this.idBuyer, other.idBuyer)) {
            return false;
        }
        if (!Objects.equals(this.idSupplier, other.idSupplier)) {
            return false;
        }
        return true;
    }
    
    
    @Override
    public String toString(){
    
        return "Entities.PartnershipRequestPK [ idBuyer="+this.idBuyer+", idSupplier="+this.idSupplier+" ]";
    
    }
    
}
