/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pedido.findAll", query = "SELECT p FROM Pedido p ORDER BY p.date ASC"),
    @NamedQuery(name="Pedido.FindSpecificByFranchise",query="SELECT p from Pedido p where p.idPEDIDO = :idPedido AND p.sucursal.sucursalPK.franquiciaIdfranquicia = :franchiseId"),
    @NamedQuery(name = "Pedido.orderedByState",query = "SELECT p from Pedido p where p.sucursal.sucursalPK = :sucursalPK ORDER BY p.pedidoStatestateId.state"),
    @NamedQuery(name = "Pedido.findByIdFranquicia", query="SELECT p from Pedido p where p.sucursal.sucursalPK.franquiciaIdfranquicia = :idfranquicia"),
    @NamedQuery(name = "Pedido.findByUser",query ="SELECT p from Pedido p where p.loginAdministradorusername.username = :loginAdministrador and p.pedidoStatestateId.state = :pedidoStateId"),
    @NamedQuery(name = "Pedido.findByState", query="SELECT p FROM Pedido p where p.pedidoStatestateId.state = :pedidoStateId AND p.sucursal.sucursalPK = :sucursalPK"),
    @NamedQuery(name = "Pedido.findByIdPEDIDO", query = "SELECT p FROM Pedido p WHERE p.idPEDIDO = :idPEDIDO"),
    @NamedQuery(name = "Pedido.findBySucursal",query="SELECT p from Pedido p where p.sucursal.sucursalPK = :sucursalPK ORDER BY p.date ASC"),
    @NamedQuery(name = "Pedido.findByDateInterval", query="SELECT p from Pedido p where p.date >= :from AND p.date <= :to AND p.sucursal.sucursalPK = :sucursalPK ORDER BY p.pedidoStatestateId.state"),
    @NamedQuery(name = "Pedido.findByType", query = "SELECT p FROM Pedido p WHERE p.type = :type")})
public class Pedido implements Serializable {
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "pedido")
    private RatehasPEDIDO ratehasPEDIDO;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pedido")
    private Collection<RatehasPEDIDO> ratehasPEDIDOCollection;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 45)
    @Column(name = "description")
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pedido")
    private Collection<ProductohasPEDIDO> productohasPEDIDOCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "pedido")
    private Collection<MenuhasPEDIDO> menuhasPEDIDOCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPEDIDO")
    private Integer idPEDIDO;
   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "type")
    private String type;
    @JoinColumns({
        @JoinColumn(name = "DIRECCION_idDireccion", referencedColumnName = "idDireccion"),
        @JoinColumn(name = "DIRECCION_CITY_idCITY", referencedColumnName = "CITY_idCITY"),
        @JoinColumn(name = "DIRECCION_CITY_STATE_idSTATE", referencedColumnName = "CITY_STATE_idSTATE")})
    @ManyToOne(optional = false)
    private Direccion direccion;
    @JoinColumn(name = "loginAdministrador_username", referencedColumnName = "username")
    @ManyToOne(optional = false)
    private LoginAdministrador loginAdministradorusername;
    @JoinColumn(name = "pedidoState_stateId", referencedColumnName = "stateId")
    @ManyToOne(optional = false)
    private PedidoState pedidoStatestateId;
    @JoinColumns({
        @JoinColumn(name = "sucursal_idsucursal", referencedColumnName = "idsucursal"),
        @JoinColumn(name = "sucursal_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia")})
    @ManyToOne(optional = false)
    private Sucursal sucursal;

    public Pedido() {
    }

    public Pedido(Integer idPEDIDO) {
        this.idPEDIDO = idPEDIDO;
    }

    public Pedido(Integer idPEDIDO, String type) {
        this.idPEDIDO = idPEDIDO;
        this.type = type;
    }

    public Integer getIdPEDIDO() {
        return idPEDIDO;
    }

    public void setIdPEDIDO(Integer idPEDIDO) {
        this.idPEDIDO = idPEDIDO;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public LoginAdministrador getLoginAdministradorusername() {
        return loginAdministradorusername;
    }

    public void setLoginAdministradorusername(LoginAdministrador loginAdministradorusername) {
        this.loginAdministradorusername = loginAdministradorusername;
    }

    public PedidoState getPedidoStatestateId() {
        return pedidoStatestateId;
    }

    public void setPedidoStatestateId(PedidoState pedidoStatestateId) {
        this.pedidoStatestateId = pedidoStatestateId;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPEDIDO != null ? idPEDIDO.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pedido)) {
            return false;
        }
        Pedido other = (Pedido) object;
        if ((this.idPEDIDO == null && other.idPEDIDO != null) || (this.idPEDIDO != null && !this.idPEDIDO.equals(other.idPEDIDO))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Pedido[ idPEDIDO=" + idPEDIDO + " ]";
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<ProductohasPEDIDO> getProductohasPEDIDOCollection() {
        return productohasPEDIDOCollection;
    }

    public void setProductohasPEDIDOCollection(Collection<ProductohasPEDIDO> productohasPEDIDOCollection) {
        this.productohasPEDIDOCollection = productohasPEDIDOCollection;
    }

    @XmlTransient
    public Collection<MenuhasPEDIDO> getMenuhasPEDIDOCollection() {
        return menuhasPEDIDOCollection;
    }

    public void setMenuhasPEDIDOCollection(Collection<MenuhasPEDIDO> menuhasPEDIDOCollection) {
        this.menuhasPEDIDOCollection = menuhasPEDIDOCollection;
    }

    @XmlTransient
    public Collection<RatehasPEDIDO> getRatehasPEDIDOCollection() {
        return ratehasPEDIDOCollection;
    }

    public void setRatehasPEDIDOCollection(Collection<RatehasPEDIDO> ratehasPEDIDOCollection) {
        this.ratehasPEDIDOCollection = ratehasPEDIDOCollection;
    }

    public RatehasPEDIDO getRatehasPEDIDO() {
        return ratehasPEDIDO;
    }

    public void setRatehasPEDIDO(RatehasPEDIDO ratehasPEDIDO) {
        this.ratehasPEDIDO = ratehasPEDIDO;
    }
    
}
