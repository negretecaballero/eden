/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import SessionClasses.Item;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p"),
     @NamedQuery(name = "Producto.findByName",query = "SELECT p FROM Producto p where p.categoria.categoriaPK = :categoryPK AND p.nombre = :name"),
    @NamedQuery(name = "Producto.findByIdproducto", query = "SELECT p FROM Producto p WHERE p.productoPK.idproducto = :idproducto"),
    @NamedQuery(name = "Producto.findByNombre", query = "SELECT p FROM Producto p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Producto.findByPrecio", query = "SELECT p FROM Producto p WHERE p.precio = :precio"),
    @NamedQuery(name = "Producto.findByCategoriaIdcategoria", query = "SELECT p FROM Producto p WHERE p.productoPK.categoriaIdcategoria = :categoriaIdcategoria"),
    @NamedQuery(name = "Producto.findByBarcode",query="SELECT p from Producto p where p.barcode.barcode = :barcode"),
    @NamedQuery(name = "Producto.findByCategoriaFranquiciaIdfranquicia", query = "SELECT p FROM Producto p WHERE p.productoPK.categoriaFranquiciaIdfranquicia = :categoriaFranquiciaIdfranquicia")})
public class Producto extends Item implements Serializable {
   

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductoPK productoPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "precio")
    private Double precio;
    @Lob
    @Size(max = 65535)
    @Column(name = "description")
    private String description;
 @javax.persistence.JoinTable(name="producto_has_addition", joinColumns={
 
     @javax.persistence.JoinColumn(name = "producto_idproducto",referencedColumnName="idproducto"),
     
     @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName="categoria_idcategoria"),
     
     @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName="categoria_franquicia_idfranquicia")
     
 },inverseJoinColumns={

     @javax.persistence.JoinColumn(name = "addition_idaddition",referencedColumnName = "idaddition"),
     
     @javax.persistence.JoinColumn( name = "addition_franquicia_idfranquicia",referencedColumnName ="franquicia_idfranquicia")
     
})
@ManyToMany(cascade=javax.persistence.CascadeType.ALL)
 private  Collection<Addition>additionCollection;
   
   @javax.persistence.JoinTable(name="producto_has_imagen", joinColumns={
   
       @javax.persistence.JoinColumn(name = "producto_idproducto", referencedColumnName="idproducto"),
       
       @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName="categoria_idcategoria"),
   
       @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName="categoria_franquicia_idfranquicia")
   },
   inverseJoinColumns={
   
     @javax.persistence.JoinColumn(name = "imagen_idimagen",referencedColumnName="idimagen")
   
   }
   
   )
   @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)
   private Collection<Entities.Imagen>imagenCollection;
   
    @JoinColumns({
        @JoinColumn(name = "categoria_idcategoria", referencedColumnName = "idcategoria", insertable = false, updatable = false),
        @JoinColumn(name = "categoria_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Categoria categoria;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<LastSeenProducto> lastSeenProductoCollection;
    
    @ManyToMany(mappedBy = "productoCollection")
    private Collection<Entities.Sucursal> sucursalCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<Compone> componeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<SimilarProducts> similarProductsCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto1")
    private Collection<SimilarProducts> similarProductsCollection1;
   
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<ProductohasPEDIDO> productohasPEDIDOCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<SaleHasProducto> saleHasProductoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "producto")
    private Collection<Agrupa> agrupaCollection;

    @javax.persistence.JoinColumn(name="barcode_barcodeid",referencedColumnName="barcodeid")
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Barcode barcode;
    public Producto() {
        
    }

    
    public Entities.Barcode getBarcode(){
    
    
    return this.barcode;
    
    }
    
    public void setBarcode(Entities.Barcode barcode){
    
        this.barcode=barcode;
    
    }
    
    
    public Collection<Entities.Sucursal>getSucursalCollection(){
    
    return this.sucursalCollection;
    
    }
    
    public void setSucursalCollection(Collection<Entities.Sucursal>sucursalCollection){
    
    this.sucursalCollection=sucursalCollection;
    
    }
    
    public Producto(ProductoPK productoPK) {
        this.productoPK = productoPK;
    }

    public Producto(ProductoPK productoPK, String nombre) {
        this.productoPK = productoPK;
        this.nombre = nombre;
    }

    public Producto(int idproducto, int categoriaIdcategoria, int categoriaFranquiciaIdfranquicia) {
        this.productoPK = new ProductoPK(idproducto, categoriaIdcategoria, categoriaFranquiciaIdfranquicia);
    }

    public ProductoPK getProductoPK() {
        return productoPK;
    }

    public void setProductoPK(ProductoPK productoPK) {
        this.productoPK = productoPK;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }

    @XmlTransient
    public Collection<Addition> getAdditionCollection() {
        return additionCollection;
    }

    public void setAdditionCollection(Collection<Addition> additionCollection) {
        this.additionCollection = additionCollection;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @XmlTransient
    public Collection<LastSeenProducto> getLastSeenProductoCollection() {
        return lastSeenProductoCollection;
    }

    public void setLastSeenProductoCollection(Collection<LastSeenProducto> lastSeenProductoCollection) {
        this.lastSeenProductoCollection = lastSeenProductoCollection;
    }

    @XmlTransient
    public Collection<Compone> getComponeCollection() {
        return componeCollection;
    }

    public void setComponeCollection(Collection<Compone> componeCollection) {
        this.componeCollection = componeCollection;
    }

    @XmlTransient
    public Collection<SimilarProducts> getSimilarProductsCollection() {
        return similarProductsCollection;
    }

    public void setSimilarProductsCollection(Collection<SimilarProducts> similarProductsCollection) {
        this.similarProductsCollection = similarProductsCollection;
    }

    @XmlTransient
    public Collection<SimilarProducts> getSimilarProductsCollection1() {
        return similarProductsCollection1;
    }

    public void setSimilarProductsCollection1(Collection<SimilarProducts> similarProductsCollection1) {
        this.similarProductsCollection1 = similarProductsCollection1;
    }

  

    @XmlTransient
    public Collection<ProductohasPEDIDO> getProductohasPEDIDOCollection() {
        return productohasPEDIDOCollection;
    }

    public void setProductohasPEDIDOCollection(Collection<ProductohasPEDIDO> productohasPEDIDOCollection) {
        this.productohasPEDIDOCollection = productohasPEDIDOCollection;
    }

    @XmlTransient
    public Collection<SaleHasProducto> getSaleHasProductoCollection() {
        return saleHasProductoCollection;
    }

    public void setSaleHasProductoCollection(Collection<SaleHasProducto> saleHasProductoCollection) {
        this.saleHasProductoCollection = saleHasProductoCollection;
    }

    @XmlTransient
    public Collection<Agrupa> getAgrupaCollection() {
        return agrupaCollection;
    }

    public void setAgrupaCollection(Collection<Agrupa> agrupaCollection) {
        this.agrupaCollection = agrupaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productoPK != null ? productoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto)) {
            return false;
        }
        Producto other = (Producto) object;
        if ((this.productoPK == null && other.productoPK != null) || (this.productoPK != null && !this.productoPK.equals(other.productoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return productoPK.toString();
    }

  

   
}
