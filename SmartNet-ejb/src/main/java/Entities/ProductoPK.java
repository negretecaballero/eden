/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class ProductoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "idproducto")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idproducto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "categoria_idcategoria")
    private int categoriaIdcategoria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "categoria_franquicia_idfranquicia")
    private int categoriaFranquiciaIdfranquicia;

    public ProductoPK() {
    }

    public ProductoPK(int idproducto, int categoriaIdcategoria, int categoriaFranquiciaIdfranquicia) {
        this.idproducto = idproducto;
        this.categoriaIdcategoria = categoriaIdcategoria;
        this.categoriaFranquiciaIdfranquicia = categoriaFranquiciaIdfranquicia;
    }

    public int getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(int idproducto) {
        this.idproducto = idproducto;
    }

    public int getCategoriaIdcategoria() {
        return categoriaIdcategoria;
    }

    public void setCategoriaIdcategoria(int categoriaIdcategoria) {
        this.categoriaIdcategoria = categoriaIdcategoria;
    }

    public int getCategoriaFranquiciaIdfranquicia() {
        return categoriaFranquiciaIdfranquicia;
    }

    public void setCategoriaFranquiciaIdfranquicia(int categoriaFranquiciaIdfranquicia) {
        this.categoriaFranquiciaIdfranquicia = categoriaFranquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idproducto;
        hash += (int) categoriaIdcategoria;
        hash += (int) categoriaFranquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductoPK)) {
            return false;
        }
        ProductoPK other = (ProductoPK) object;
        if (this.idproducto != other.idproducto) {
            return false;
        }
        if (this.categoriaIdcategoria != other.categoriaIdcategoria) {
            return false;
        }
        if (this.categoriaFranquiciaIdfranquicia != other.categoriaFranquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return idproducto+","+categoriaIdcategoria+","+categoriaFranquiciaIdfranquicia;
    }
    
}
