/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "producto_has_PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductohasPEDIDO.findAll", query = "SELECT p FROM ProductohasPEDIDO p"),
    @NamedQuery(name = "ProductohasPEDIDO.findByPedido&Sucursal",query="SELECT p FROM ProductohasPEDIDO p where p.productohasPEDIDOPK.pEDIDOidPEDIDO = :idPedido AND p.pedido.sucursal.sucursalPK = :sucursalPK"),
    @NamedQuery(name = "ProductohasPEDIDO.findByProductoIdproducto", query = "SELECT p FROM ProductohasPEDIDO p WHERE p.productohasPEDIDOPK.productoIdproducto = :productoIdproducto"),
    @NamedQuery(name = "ProductohasPEDIDO.findByPEDIDOidPEDIDO", query = "SELECT p FROM ProductohasPEDIDO p WHERE p.productohasPEDIDOPK.pEDIDOidPEDIDO = :pEDIDOidPEDIDO"),
    @NamedQuery(name = "ProductohasPEDIDO.findByQuantity", query = "SELECT p FROM ProductohasPEDIDO p WHERE p.quantity = :quantity")})
public class ProductohasPEDIDO implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductohasPEDIDOPK productohasPEDIDOPK;
   @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;
    @JoinColumn(name = "PEDIDO_idPEDIDO", referencedColumnName = "idPEDIDO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedido pedido;
    @javax.persistence.JoinColumns({
      @javax.persistence.JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto", insertable = false, updatable = false),
        
        @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName= "categoria_idcategoria",insertable=false,updatable=false),
        
        @javax.persistence.JoinColumn(name= "producto_categoria_franquicia_idfranquicia",referencedColumnName="categoria_franquicia_idfranquicia",insertable=false,updatable=false)
            
    })
    @ManyToOne(optional = false)
    private Producto producto;

    public ProductohasPEDIDO() {
    }

    public ProductohasPEDIDO(ProductohasPEDIDOPK productohasPEDIDOPK) {
        this.productohasPEDIDOPK = productohasPEDIDOPK;
    }

    public ProductohasPEDIDO(ProductohasPEDIDOPK productohasPEDIDOPK, int quantity) {
        this.productohasPEDIDOPK = productohasPEDIDOPK;
        this.quantity = quantity;
    }

    public ProductohasPEDIDO(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int pEDIDOidPEDIDO) {
        this.productohasPEDIDOPK = new ProductohasPEDIDOPK(productoIdproducto,productoCategoriaIdCategoria,productoCategoriaFranquiciaIdFranquicia, pEDIDOidPEDIDO);
    }

    public ProductohasPEDIDOPK getProductohasPEDIDOPK() {
        return productohasPEDIDOPK;
    }

    public void setProductohasPEDIDOPK(ProductohasPEDIDOPK productohasPEDIDOPK) {
        this.productohasPEDIDOPK = productohasPEDIDOPK;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productohasPEDIDOPK != null ? productohasPEDIDOPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductohasPEDIDO)) {
            return false;
        }
        ProductohasPEDIDO other = (ProductohasPEDIDO) object;
        if ((this.productohasPEDIDOPK == null && other.productohasPEDIDOPK != null) || (this.productohasPEDIDOPK != null && !this.productohasPEDIDOPK.equals(other.productohasPEDIDOPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.ProductohasPEDIDO[ productohasPEDIDOPK=" + productohasPEDIDOPK + " ]";
    }
    
}
