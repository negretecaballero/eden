/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class ProductohasPEDIDOPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria")
    private int productoCategoriaIdCategoria;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia")
    int productoCategoriaFranquiciaIdFranquicia;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "PEDIDO_idPEDIDO")
    private int pEDIDOidPEDIDO;

    public ProductohasPEDIDOPK() {
    }

    public ProductohasPEDIDOPK(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int pEDIDOidPEDIDO) {
        this.productoIdproducto = productoIdproducto;
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
        this.pEDIDOidPEDIDO = pEDIDOidPEDIDO;
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getProductoCategoriaIdCategoria(){
    
    return this.productoCategoriaIdCategoria;
    
    }
    
    public void setProductoCategoriaIdCategoria(int productoCategoriaIdCategoria){
    
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
    
    }
    
    public int getproductoCategoriaFranquiciaIdFranquicia(){
    
    return this.productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    public void setproductoCategoriaFranquiciaIdFranquicia(int productoCategoriaFranquiciaIdFranquicia){
    
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    
    public int getPEDIDOidPEDIDO() {
        return pEDIDOidPEDIDO;
    }

    public void setPEDIDOidPEDIDO(int pEDIDOidPEDIDO) {
        this.pEDIDOidPEDIDO = pEDIDOidPEDIDO;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productoIdproducto;
        hash += (int)productoCategoriaIdCategoria;
        hash += (int)productoCategoriaFranquiciaIdFranquicia;
        hash += (int) pEDIDOidPEDIDO;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductohasPEDIDOPK)) {
            return false;
        }
        ProductohasPEDIDOPK other = (ProductohasPEDIDOPK) object;
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.productoCategoriaIdCategoria != other.productoCategoriaIdCategoria) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdFranquicia != other.productoCategoriaFranquiciaIdFranquicia) {
            return false;
        }
        
        if (this.pEDIDOidPEDIDO != other.pEDIDOidPEDIDO) {
            return false;
        }
        
        return true;
    }

    @Override
    public String toString() {
        return "Entities.ProductohasPEDIDOPK[ productoIdproducto=" + productoIdproducto + ", productoCategoriaIdCategoria="+productoCategoriaIdCategoria+", productoCategoriaFranquiciaIdFranquicia="+productoCategoriaFranquiciaIdFranquicia+", pEDIDOidPEDIDO=" + pEDIDOidPEDIDO + " ]";
    }
    
}
