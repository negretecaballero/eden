/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "profession")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profession.findAll", query = "SELECT p FROM Profession p ORDER BY p.profession ASC"),
    @NamedQuery(name = "Profession.findByIdprofession", query = "SELECT p FROM Profession p WHERE p.idprofession = :idprofession"),
    @NamedQuery(name = "Profession.findByProfession", query = "SELECT p FROM Profession p WHERE p.profession = :profession")})
public class Profession implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idprofession")
    private Integer idprofession;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "profession")
    private String profession;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professionIdprofession")
    private Collection<LoginAdministrador> loginAdministradorCollection;

    public Profession() {
    }

    public Profession(Integer idprofession) {
        this.idprofession = idprofession;
    }

    public Profession(Integer idprofession, String profession) {
        this.idprofession = idprofession;
        this.profession = profession;
    }

    public Integer getIdprofession() {
        return idprofession;
    }

    public void setIdprofession(Integer idprofession) {
        this.idprofession = idprofession;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @XmlTransient
    public Collection<LoginAdministrador> getLoginAdministradorCollection() {
        return loginAdministradorCollection;
    }

    public void setLoginAdministradorCollection(Collection<LoginAdministrador> loginAdministradorCollection) {
        this.loginAdministradorCollection = loginAdministradorCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprofession != null ? idprofession.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profession)) {
            return false;
        }
        Profession other = (Profession) object;
        if ((this.idprofession == null && other.idprofession != null) || (this.idprofession != null && !this.idprofession.equals(other.idprofession))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Profession[ idprofession=" + idprofession + " ]";
    }
    
}
