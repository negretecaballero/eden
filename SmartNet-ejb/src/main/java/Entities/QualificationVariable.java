/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */

@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.Entity

@javax.persistence.Table(name="qualification_variable")

@javax.persistence.NamedQueries({

    @NamedQuery(name="QualificationVariable.findAll",query="SELECT q FROM QualificationVariable q"),
        
    @NamedQuery(name="QualificationVariable.findById",query="SELECT q FROM QualificationVariable q where q.idQualificationVariable = :id")
        
})

public class QualificationVariable implements java.io.Serializable{
    
    
    private static final long serialVersionUID=1L;
    
    @javax.persistence.Basic(optional=false)
    
    @javax.validation.constraints.NotNull
    
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    
    @javax.persistence.Column(name="idqualification_variable")
    
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    
    @javax.persistence.Id
    
    private Integer idQualificationVariable;
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL, mappedBy="idQualificationVariable")
    private java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection;
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection getQualificationVariableHasRateCollection(){
    
    return this.qualificationVariableHasRateCollection;
    
    }
    
    public void setQualificationVariableHasRateCollection(java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection){
    
        this.qualificationVariableHasRateCollection=qualificationVariableHasRateCollection;
    
    }
    
    
    public Integer getIdQualificationVariable(){
    
        return this.idQualificationVariable;
    
    }
    
    public void setIdQualificationVariable(Integer idQualificationVariable){
    
    this.idQualificationVariable=idQualificationVariable;
    
    }
    
    
    @javax.persistence.Basic(optional=false)
    
    @javax.validation.constraints.NotNull
    
    @javax.validation.constraints.Size(min= 1,max=45)
    
    @javax.persistence.Column(name="name")
    
    private String name;
    
    public String getName(){
    
        return this.name;
    
    }
    
    
    public void setName(String name){
    
    this.name=name;
    
    }
    
    public QualificationVariable(){
    
    }
    
    public QualificationVariable(Integer idQualificationVariable){
    
        this.idQualificationVariable=idQualificationVariable;
        
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash=( idQualificationVariable!=null ? idQualificationVariable.hashCode():0 );
        
        return hash;
        
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QualificationVariable other = (QualificationVariable) obj;
        if (!Objects.equals(this.idQualificationVariable, other.idQualificationVariable)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return "Entities.QualificationVariable [ idQualificationVariable="+this.idQualificationVariable+" ]";
    
    }
    
}
