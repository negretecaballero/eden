/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="qualification_variable_has_rate")

@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.NamedQueries({

    @NamedQuery(name="QualificationVariableHasRate.findAll",query="SELECT q from QualificationVariableHasRate q"),
    
    @NamedQuery(name="QualificationVariableHasRate.findByFranchiseId",query="SELECT q from QualificationVariableHasRate q where q.qualificationVariableHasRatePK.franquiciaIdfranquicia = :idFranchise")

})

public class QualificationVariableHasRate implements java.io.Serializable{
    
    @javax.persistence.EmbeddedId
    
  protected  Entities.QualificationVariableHasRatePK qualificationVariableHasRatePK;
    
    public Entities.QualificationVariableHasRatePK getQualificationVariableHasRatePK(){
    
        return this.qualificationVariableHasRatePK;
    
    }
    
    public void setQualificationVariableHasRatePK(Entities.QualificationVariableHasRatePK qualificationVariableHasRatePK){
    
        this.qualificationVariableHasRatePK=qualificationVariableHasRatePK;
    
    }
    
    
    @javax.persistence.JoinColumn(name="franquicia_idfranquicia",referencedColumnName="idfranquicia",insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Franquicia idFranquicia;
    
    
    @javax.persistence.JoinColumn(name="rate_idrate",referencedColumnName="idrate",insertable=false,updatable=false)
    @javax.persistence.ManyToOne
    private Entities.Rate idRate;
    
    
    @javax.persistence.JoinColumn(name="qualification_variable_idqualification_variable",referencedColumnName="",insertable=false,updatable=false)
    @javax.persistence.ManyToOne
    private Entities.QualificationVariable idQualificationVariable;
    
    
    public Entities.Franquicia getIdFranquicia(){
    
        return this.idFranquicia;
    
    }
    
    public void setIdFranquicia(Entities.Franquicia idFranquicia)
    {
    
    this.idFranquicia=idFranquicia;
    
    }
    
    public Entities.Rate getIdRate(){
    
        return this.idRate;
    
    }
    
    public void setIdRate(Entities.Rate idRate){
    
        this.idRate=idRate;
    
    }
    
    public Entities.QualificationVariable getIdQualificationVariable(){
    
    return this.idQualificationVariable;
    
    }
    
    public void setIdQualificationVariable(Entities.QualificationVariable idQualificationVariable){
    
        this.idQualificationVariable=idQualificationVariable;
    
    }
    
    @Override
    public int hashCode(){
    
    int hash=0;
    
    hash=this.qualificationVariableHasRatePK!=null ? qualificationVariableHasRatePK.hashCode() :0;
    
    return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QualificationVariableHasRate other = (QualificationVariableHasRate) obj;
        if (!Objects.equals(this.qualificationVariableHasRatePK, other.qualificationVariableHasRatePK)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
       return "Entities.QualificationVariableHasRate [ qualificationVariableHasRatePK="+qualificationVariableHasRatePK+" ]";
    
    }
    
}
