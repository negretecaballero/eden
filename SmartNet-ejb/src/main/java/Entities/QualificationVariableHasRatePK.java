/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable
public class QualificationVariableHasRatePK implements java.io.Serializable{
    
    
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="qualification_variable_idqualification_variable")
    private Integer qualificationVariableIdqualificationVariable;
    
    public Integer getQualificationVariableIdqualificationVariable()
            {
            
                return this.qualificationVariableIdqualificationVariable;
            
            }
    
    public void setQualificationVariableIdqualificationVariable(Integer qualificationVariableIdqualificationVariable){
    
    this.qualificationVariableIdqualificationVariable=qualificationVariableIdqualificationVariable;
    
    }
    
    
    
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="rate_idrate")
    private Integer rateIdrate;
    
    public Integer getRateIdrate(){
    
    return this.rateIdrate;
    
    }
    
    public void setRateIdrate(Integer rateIdrate){
    
        this.rateIdrate=rateIdrate;
    
    }
    
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="franquicia_idfranquicia")
    private Integer franquiciaIdfranquicia;
    
    public Integer getFranquiciaIdfranquicia(){
    
        return this.franquiciaIdfranquicia;
    
    }
    
    public void setFranquiciaIdfranquicia(Integer franquiciaIdfranquicia){
    
        this.franquiciaIdfranquicia=franquiciaIdfranquicia;
    
    }
    
    public QualificationVariableHasRatePK(){
    
    
    }
    
    public QualificationVariableHasRatePK(Integer qualificationVariableIdqualificationVariable){
    
        this.qualificationVariableIdqualificationVariable=qualificationVariableIdqualificationVariable;
    
    }
    
    public QualificationVariableHasRatePK(Integer qualificationVariableIdqualificationVariable, Integer rateIdrate){
    
        this.qualificationVariableIdqualificationVariable=qualificationVariableIdqualificationVariable;
        
        this.rateIdrate=rateIdrate;
    
    }
    
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=this.qualificationVariableIdqualificationVariable;
        
        hash+=this.rateIdrate;
        
        hash+=this.franquiciaIdfranquicia;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final QualificationVariableHasRatePK other = (QualificationVariableHasRatePK) obj;
        if (!Objects.equals(this.qualificationVariableIdqualificationVariable, other.qualificationVariableIdqualificationVariable)) {
            return false;
        }
        if (!Objects.equals(this.rateIdrate, other.rateIdrate)) {
            return false;
        }
        if (!Objects.equals(this.franquiciaIdfranquicia, other.franquiciaIdfranquicia)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return "Entities.QualificationVariableHasRatePK [ qualificationVariableIdqualificationVariable="+qualificationVariableIdqualificationVariable+", rateIdrate="+this.rateIdrate+", franquiciaIdfranquicia="+franquiciaIdfranquicia+" ]";
    
    }
    
}
