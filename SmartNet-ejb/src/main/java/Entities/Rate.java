/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "rate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rate.findAll", query = "SELECT r FROM Rate r"),
    @NamedQuery(name = "Rate.findByIdrate", query = "SELECT r FROM Rate r WHERE r.idrate = :idrate"),
    @NamedQuery(name = "Rate.findByStars", query = "SELECT r FROM Rate r WHERE r.stars = :stars")})
public class Rate implements Serializable {
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "rateIdrate")
    private Collection<RatehasPEDIDO> ratehasPEDIDOCollection;
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idrate")
    private Short idrate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stars")
    private short stars;
    
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idRate")
    private java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection;
    
    
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.QualificationVariableHasRate>getQualificationVariableHasRateCollection(){

        return this.qualificationVariableHasRateCollection;
    
    }
    
    public void setQualificationVariableHasRateCollection(java.util.Collection<Entities.QualificationVariableHasRate>qualificationVariableHasRateCollection){
    
       this.qualificationVariableHasRateCollection=qualificationVariableHasRateCollection;
    
    }
    
    
    public Rate() {
    }

    public Rate(Short idrate) {
        this.idrate = idrate;
    }

    public Rate(Short idrate, short stars) {
        this.idrate = idrate;
        this.stars = stars;
    }

    public Short getIdrate() {
        return idrate;
    }

    public void setIdrate(Short idrate) {
        this.idrate = idrate;
    }

    public short getStars() {
        return stars;
    }

    public void setStars(short stars) {
        this.stars = stars;
    }

  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idrate != null ? idrate.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rate)) {
            return false;
        }
        Rate other = (Rate) object;
        if ((this.idrate == null && other.idrate != null) || (this.idrate != null && !this.idrate.equals(other.idrate))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Rate[ idrate=" + idrate + " ]";
    }

    @XmlTransient
    public Collection<RatehasPEDIDO> getRatehasPEDIDOCollection() {
        return ratehasPEDIDOCollection;
    }

    public void setRatehasPEDIDOCollection(Collection<RatehasPEDIDO> ratehasPEDIDOCollection) {
        this.ratehasPEDIDOCollection = ratehasPEDIDOCollection;
    }
    
}
