/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "rate_has_PEDIDO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RatehasPEDIDO.findAll", query = "SELECT r FROM RatehasPEDIDO r"),
    @NamedQuery(name = "RatehasPEDIDO.findByPEDIDOidPEDIDO", query = "SELECT r FROM RatehasPEDIDO r WHERE r.rateHasPedidoPK.pEDIDOidPEDIDO = :pEDIDOidPEDIDO"),
    @NamedQuery(name = "RatehasPEDIDO.findByState", query = "SELECT r FROM RatehasPEDIDO r WHERE r.state = :state")})
public class RatehasPEDIDO implements Serializable {
    private static final long serialVersionUID = 1L;
  
  
    @javax.persistence.EmbeddedId
    protected Entities.RatehasPEDIDOPK rateHasPedidoPK;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "state")
    private boolean state;
    @JoinColumn(name = "PEDIDO_idPEDIDO", referencedColumnName = "idPEDIDO", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Pedido pedido;
    @JoinColumn(name = "rate_idrate", referencedColumnName = "idrate", insertable=false,updatable=false)
    @ManyToOne(optional = false)
    private Rate rateIdrate;

    public RatehasPEDIDO() {
    }

   public Entities.RatehasPEDIDOPK getRateHasPedidoPK(){
   
       return this.rateHasPedidoPK;
   
   }
   
   public void setRateHasPedidoPK(Entities.RatehasPEDIDOPK rateHasPedidoPK){
           
       this.rateHasPedidoPK=rateHasPedidoPK;
       
           }

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Rate getRateIdrate() {
        return rateIdrate;
    }

    public void setRateIdrate(Rate rateIdrate) {
        this.rateIdrate = rateIdrate;
    }

    @Override
    public int hashCode() {
      
        int hash=0;
        
        hash+=(this.rateHasPedidoPK!=null?this.rateHasPedidoPK.hashCode():0);
        
        return hash;
        
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RatehasPEDIDO other = (RatehasPEDIDO) obj;
        if (!Objects.equals(this.rateHasPedidoPK, other.rateHasPedidoPK)) {
            return false;
        }
        return true;
    }

  

    @Override
    public String toString() {
       
        return this.rateHasPedidoPK.toString();
        
    }
    
}
