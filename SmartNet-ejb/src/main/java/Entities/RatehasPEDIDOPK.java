/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Embeddable
public class RatehasPEDIDOPK {
    
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.persistence.Column(name="PEDIDO_idPEDIDO")
    @javax.persistence.Basic(optional=false)
    private int pEDIDOidPEDIDO;
    
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.persistence.Column(name="rate_idrate")
    @javax.persistence.Basic(optional=false)
    private int rateIdrate;
    
    public RatehasPEDIDOPK(int pEDIDOidPEDIDO, int  rateIdrate){
   
        this.pEDIDOidPEDIDO=pEDIDOidPEDIDO;
        
        this.rateIdrate=rateIdrate;
        
    }
    
    public RatehasPEDIDOPK(){
    
    }
    
    
    public int getPEDIDOidPEDIDO(){
    
        return this.pEDIDOidPEDIDO;
    
    }
    
    public void setPEDIDOidPEDIDO(int pEDIDOidPEDIDO){
    
        this.pEDIDOidPEDIDO=pEDIDOidPEDIDO;
    
    }
    
    public int getRateIdrate(){
    
        return this.rateIdrate;
    
    }
    
    public void setRateIdrate(int rateIdrate){
    
    this.rateIdrate=rateIdrate;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
    
        hash+=(int)this.pEDIDOidPEDIDO;
        
        hash+=(int)this.rateIdrate;
        
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RatehasPEDIDOPK other = (RatehasPEDIDOPK) obj;
        if (this.pEDIDOidPEDIDO != other.pEDIDOidPEDIDO) {
            return false;
        }
        if (this.rateIdrate != other.rateIdrate) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return "Entities.RatehasPEDIDOPK [ pEDIDOidPEDIDO = "+this.pEDIDOidPEDIDO+", rateIdrate = "+this.rateIdrate+" ]";
    
    }
    
}
