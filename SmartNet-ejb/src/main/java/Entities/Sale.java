/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "sale")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sale.findAll", query = "SELECT s FROM Sale s"),
    @NamedQuery(name = "Sale.findByIdsale", query = "SELECT s FROM Sale s WHERE s.salePK.idsale = :idsale"),
    @NamedQuery(name = "Sale.findByName", query = "SELECT s FROM Sale s WHERE s.name = :name"),
    @NamedQuery(name = "Sale.findByValue", query = "SELECT s FROM Sale s WHERE s.value = :value"),
    @NamedQuery(name = "Sale.findByStartDate", query = "SELECT s FROM Sale s WHERE s.startDate = :startDate"),
    @NamedQuery(name = "Sale.findByEndDate", query = "SELECT s FROM Sale s WHERE s.endDate = :endDate"),
    @NamedQuery(name = "Sale.findByFranquiciaIdfranquicia", query = "SELECT s FROM Sale s WHERE s.salePK.franquiciaIdfranquicia = :franquiciaIdfranquicia")})
public class Sale implements Serializable {
    
     private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SalePK salePK;
    @Size(min=1,max = 100)
    @Column(name = "name")
    private String name;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "value")
    private Double value;
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    @javax.validation.constraints.NotNull
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    @javax.validation.constraints.NotNull
    private Date endDate;
    @javax.persistence.JoinTable(name="sale_has_imagen", joinColumns={
    
        @JoinColumn(name="sale_idsale", referencedColumnName="idsale"),
        
        @javax.persistence.JoinColumn(name="sale_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia")
    
    },inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name="imagen_idimagen",referencedColumnName="idimagen")
        
    })
    @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)
     private Collection<Imagen> imagenCollection;
    @JoinColumn(name = "franquicia_idfranquicia", referencedColumnName = "idfranquicia", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Franquicia franquicia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sale")
    private Collection<SaleHasMenu> saleHasMenuCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sale")
    private Collection<SaleHasProducto> saleHasProductoCollection;

    @javax.persistence.ManyToMany(mappedBy="saleCollection")
    private Collection<Entities.Sucursal>sucursalCollection;
    
    public Collection<Entities.Sucursal>getSucursalCollection(){
    
    return this.sucursalCollection;
    
    }
    
    public void setSucursalCollection(Collection<Entities.Sucursal>sucursalCollection){
    
        this.sucursalCollection=sucursalCollection;
    
    }

    public Sale() {
    }

    public Sale(SalePK salePK) {
        this.salePK = salePK;
    }

    public Sale(int idsale, int franquiciaIdfranquicia) {
        this.salePK = new SalePK(idsale, franquiciaIdfranquicia);
    }

    public SalePK getSalePK() {
        return salePK;
    }

    public void setSalePK(SalePK salePK) {
        this.salePK = salePK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }

    public Franquicia getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(Franquicia franquicia) {
        this.franquicia = franquicia;
    }

    @XmlTransient
    public Collection<SaleHasMenu> getSaleHasMenuCollection() {
        return saleHasMenuCollection;
    }

    public void setSaleHasMenuCollection(Collection<SaleHasMenu> saleHasMenuCollection) {
        this.saleHasMenuCollection = saleHasMenuCollection;
    }

    @XmlTransient
    public Collection<SaleHasProducto> getSaleHasProductoCollection() {
        return saleHasProductoCollection;
    }

    public void setSaleHasProductoCollection(Collection<SaleHasProducto> saleHasProductoCollection) {
        this.saleHasProductoCollection = saleHasProductoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salePK != null ? salePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sale)) {
            return false;
        }
        Sale other = (Sale) object;
        if ((this.salePK == null && other.salePK != null) || (this.salePK != null && !this.salePK.equals(other.salePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Sale[ salePK=" + salePK + " ]";
    }


  
    
}
