/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "sale_has_menu")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaleHasMenu.findAll", query = "SELECT s FROM SaleHasMenu s"),
    @NamedQuery(name = "SaleHasMenu.findBySale",query = "SELECT s from SaleHasMenu s where s.sale.salePK = :salePK"),
    @NamedQuery(name = "SaleHasMenu.findByMenuIdmenu", query = "SELECT s FROM SaleHasMenu s WHERE s.saleHasMenuPK.menuIdmenu = :menuIdmenu"),
    @NamedQuery(name = "SaleHasMenu.findByQuantityLimit", query = "SELECT s FROM SaleHasMenu s WHERE s.quantityLimit = :quantityLimit"),
    @NamedQuery(name = "SaleHasMenu.findBySaleIdsale", query = "SELECT s FROM SaleHasMenu s WHERE s.saleHasMenuPK.saleIdsale = :saleIdsale"),
    @NamedQuery(name = "SaleHasMenu.findBySaleFranquiciaIdfranquicia", query = "SELECT s FROM SaleHasMenu s WHERE s.saleHasMenuPK.saleFranquiciaIdfranquicia = :saleFranquiciaIdfranquicia")})
public class SaleHasMenu implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SaleHasMenuPK saleHasMenuPK;
    @Column(name = "quantity_limit")
    private Integer quantityLimit;
    @JoinColumns({
        @JoinColumn(name = "sale_idsale", referencedColumnName = "idsale", insertable = false, updatable = false),
        @JoinColumn(name = "sale_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Sale sale;
    @JoinColumn(name = "menu_idmenu", referencedColumnName = "idmenu", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Menu menu;

    public SaleHasMenu() {
    }

    public SaleHasMenu(SaleHasMenuPK saleHasMenuPK) {
        this.saleHasMenuPK = saleHasMenuPK;
    }

    public SaleHasMenu(int menuIdmenu, int saleIdsale, int saleFranquiciaIdfranquicia) {
        this.saleHasMenuPK = new SaleHasMenuPK(menuIdmenu, saleIdsale, saleFranquiciaIdfranquicia);
    }

    public SaleHasMenuPK getSaleHasMenuPK() {
        return saleHasMenuPK;
    }

    public void setSaleHasMenuPK(SaleHasMenuPK saleHasMenuPK) {
        this.saleHasMenuPK = saleHasMenuPK;
    }

    public Integer getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(Integer quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saleHasMenuPK != null ? saleHasMenuPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleHasMenu)) {
            return false;
        }
        SaleHasMenu other = (SaleHasMenu) object;
        if ((this.saleHasMenuPK == null && other.saleHasMenuPK != null) || (this.saleHasMenuPK != null && !this.saleHasMenuPK.equals(other.saleHasMenuPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SaleHasMenu[ saleHasMenuPK=" + saleHasMenuPK + " ]";
    }
    
}
