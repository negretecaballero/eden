/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SaleHasMenuPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "menu_idmenu")
    private int menuIdmenu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_idsale")
    private int saleIdsale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_franquicia_idfranquicia")
    private int saleFranquiciaIdfranquicia;

    public SaleHasMenuPK() {
    }

    public SaleHasMenuPK(int menuIdmenu, int saleIdsale, int saleFranquiciaIdfranquicia) {
        this.menuIdmenu = menuIdmenu;
        this.saleIdsale = saleIdsale;
        this.saleFranquiciaIdfranquicia = saleFranquiciaIdfranquicia;
    }

    public int getMenuIdmenu() {
        return menuIdmenu;
    }

    public void setMenuIdmenu(int menuIdmenu) {
        this.menuIdmenu = menuIdmenu;
    }

    public int getSaleIdsale() {
        return saleIdsale;
    }

    public void setSaleIdsale(int saleIdsale) {
        this.saleIdsale = saleIdsale;
    }

    public int getSaleFranquiciaIdfranquicia() {
        return saleFranquiciaIdfranquicia;
    }

    public void setSaleFranquiciaIdfranquicia(int saleFranquiciaIdfranquicia) {
        this.saleFranquiciaIdfranquicia = saleFranquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) menuIdmenu;
        hash += (int) saleIdsale;
        hash += (int) saleFranquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleHasMenuPK)) {
            return false;
        }
        SaleHasMenuPK other = (SaleHasMenuPK) object;
        if (this.menuIdmenu != other.menuIdmenu) {
            return false;
        }
        if (this.saleIdsale != other.saleIdsale) {
            return false;
        }
        if (this.saleFranquiciaIdfranquicia != other.saleFranquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SaleHasMenuPK[ menuIdmenu=" + menuIdmenu + ", saleIdsale=" + saleIdsale + ", saleFranquiciaIdfranquicia=" + saleFranquiciaIdfranquicia + " ]";
    }
    
}
