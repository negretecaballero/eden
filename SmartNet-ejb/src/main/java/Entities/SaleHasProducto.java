/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "sale_has_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SaleHasProducto.findAll", query = "SELECT s FROM SaleHasProducto s"),
    @NamedQuery(name = "SaleHasProducto.findByProductoIdproducto", query = "SELECT s FROM SaleHasProducto s WHERE s.saleHasProductoPK.productoIdproducto = :productoIdproducto"),
    @NamedQuery(name = "SaleHasProducto.findByQuantityLimit", query = "SELECT s FROM SaleHasProducto s WHERE s.quantityLimit = :quantityLimit"),
    @NamedQuery(name = "SaleHasProducto.findBySaleIdsale", query = "SELECT s FROM SaleHasProducto s WHERE s.saleHasProductoPK.saleIdsale = :saleIdsale"),
    @NamedQuery(name = "SaleHasProducto.findBySaleFranquiciaIdfranquicia", query = "SELECT s FROM SaleHasProducto s WHERE s.saleHasProductoPK.saleFranquiciaIdfranquicia = :saleFranquiciaIdfranquicia"),
   @NamedQuery(name = "SaleHasProducto.findBySale",query="select s from SaleHasProducto s where s.sale.salePK = :salePK")
})
public class SaleHasProducto implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SaleHasProductoPK saleHasProductoPK;
    @Column(name = "quantity_limit")
    private Integer quantityLimit;
    @JoinColumns({
        @JoinColumn(name = "sale_idsale", referencedColumnName = "idsale", insertable = false, updatable = false),
        @JoinColumn(name = "sale_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia", insertable = false, updatable = false)})
    @ManyToOne(optional = false)
    private Sale sale;
    @javax.persistence.JoinColumns(
    {
    @javax.persistence.JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto", insertable = false, updatable = false),
    
    @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName="categoria_idcategoria",insertable=false,updatable=false),
    
    @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia", referencedColumnName="categoria_franquicia_idfranquicia",insertable=false,updatable=false)
    
    
    }
    )
    @ManyToOne(optional = false)
    private Producto producto;

    public SaleHasProducto() {
    }

    public SaleHasProducto(SaleHasProductoPK saleHasProductoPK) {
        this.saleHasProductoPK = saleHasProductoPK;
    }

    public SaleHasProducto(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int saleIdsale, int saleFranquiciaIdfranquicia) {
        this.saleHasProductoPK = new SaleHasProductoPK(productoIdproducto, productoCategoriaIdCategoria,productoCategoriaFranquiciaIdFranquicia,saleIdsale, saleFranquiciaIdfranquicia);
    }

    public SaleHasProductoPK getSaleHasProductoPK() {
        return saleHasProductoPK;
    }

    public void setSaleHasProductoPK(SaleHasProductoPK saleHasProductoPK) {
        this.saleHasProductoPK = saleHasProductoPK;
    }

    public Integer getQuantityLimit() {
        return quantityLimit;
    }

    public void setQuantityLimit(Integer quantityLimit) {
        this.quantityLimit = quantityLimit;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (saleHasProductoPK != null ? saleHasProductoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleHasProducto)) {
            return false;
        }
        SaleHasProducto other = (SaleHasProducto) object;
        if ((this.saleHasProductoPK == null && other.saleHasProductoPK != null) || (this.saleHasProductoPK != null && !this.saleHasProductoPK.equals(other.saleHasProductoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SaleHasProducto[ saleHasProductoPK=" + saleHasProductoPK + " ]";
    }
    
}
