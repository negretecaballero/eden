/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SaleHasProductoPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria")
    private int productoCategoriaIdCategoria;
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia")
    private int productoCategoriaFranquiciaIdFranquicia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_idsale")
    private int saleIdsale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sale_franquicia_idfranquicia")
    private int saleFranquiciaIdfranquicia;

    public int getProductoCategoriaIdCategoria(){
    
        return this.productoCategoriaIdCategoria;
    
    }
    
    public void setProductoCategoriaIdCategoria(int productoCategoriaIdCategoria){
    
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
    
    }
    
    public int getProductoCategoriaFranquiciaIdFranquicia(){
    
    return this.productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    public void setProductoCategoriaFranquiciaIdFranquicia(int productoCategoriaFranquiciaIdFranquicia){
    
    
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    
    public SaleHasProductoPK() {
    }

    public SaleHasProductoPK(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int saleIdsale, int saleFranquiciaIdfranquicia) {
        this.productoIdproducto = productoIdproducto;
        this.saleIdsale = saleIdsale;
        this.saleFranquiciaIdfranquicia = saleFranquiciaIdfranquicia;
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
        
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getSaleIdsale() {
        return saleIdsale;
    }

    public void setSaleIdsale(int saleIdsale) {
        this.saleIdsale = saleIdsale;
    }

    public int getSaleFranquiciaIdfranquicia() {
        return saleFranquiciaIdfranquicia;
    }

    public void setSaleFranquiciaIdfranquicia(int saleFranquiciaIdfranquicia) {
        this.saleFranquiciaIdfranquicia = saleFranquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productoIdproducto;
        hash += (int) saleIdsale;
        hash += (int) saleFranquiciaIdfranquicia;
        hash += (int) productoCategoriaIdCategoria;
        hash += (int) productoCategoriaFranquiciaIdFranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleHasProductoPK)) {
            return false;
        }
        SaleHasProductoPK other = (SaleHasProductoPK) object;
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.saleIdsale != other.saleIdsale) {
            return false;
        }
        if (this.saleFranquiciaIdfranquicia != other.saleFranquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SaleHasProductoPK[ productoIdproducto=" + productoIdproducto + ",productoCategoriaIdCategoria="+productoCategoriaIdCategoria+",productoCategoriaFranquiciaIdFranquicia="+productoCategoriaFranquiciaIdFranquicia+", saleIdsale=" + saleIdsale + ", saleFranquiciaIdfranquicia=" + saleFranquiciaIdfranquicia + " ]";
    }
    
}
