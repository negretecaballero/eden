/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SalePK implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsale")
    private int idsale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "franquicia_idfranquicia")
    private int franquiciaIdfranquicia;

    public SalePK() {
    }

    public SalePK(int idsale, int franquiciaIdfranquicia) {
        this.idsale = idsale;
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    public int getIdsale() {
        return idsale;
    }

    public void setIdsale(int idsale) {
        this.idsale = idsale;
    }

    public int getFranquiciaIdfranquicia() {
        return franquiciaIdfranquicia;
    }

    public void setFranquiciaIdfranquicia(int franquiciaIdfranquicia) {
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idsale;
        hash += (int) franquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalePK)) {
            return false;
        }
        SalePK other = (SalePK) object;
        if (this.idsale != other.idsale) {
            return false;
        }
        if (this.franquiciaIdfranquicia != other.franquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SalePK[ idsale=" + idsale + ", franquiciaIdfranquicia=" + franquiciaIdfranquicia + " ]";
    }
    
}
