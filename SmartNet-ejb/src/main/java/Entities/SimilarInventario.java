/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "similar_inventario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SimilarInventario.findAll", query = "SELECT s FROM SimilarInventario s"),
    @NamedQuery(name = "SimilarInventario.findByInventarioIdinventario", query = "SELECT s FROM SimilarInventario s WHERE s.similarInventarioPK.inventarioIdinventario = :inventarioIdinventario"),
    @NamedQuery(name = "SimilarInventario.findByInventarioIdinventario1", query = "SELECT s FROM SimilarInventario s WHERE s.similarInventarioPK.inventarioIdinventario1 = :inventarioIdinventario1")})
   public class SimilarInventario implements Serializable {
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_diference")
    private Double priceDiference;
    @Basic(optional = false)
    @NotNull
    @Column(name = "equivalencia")
    private double equivalencia;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SimilarInventarioPK similarInventarioPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
   
    @JoinColumns({
        @JoinColumn(name = "inventario_idinventario", referencedColumnName = "idinventario", insertable=false,updatable=false),
          @JoinColumn(name = "inventario_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia",insertable=false,updatable=false)})
    @ManyToOne
    private Inventario inventario;
    @JoinColumns({
        @JoinColumn(name = "inventario_idinventario1", referencedColumnName = "idinventario",insertable=false,updatable=false),
         @JoinColumn(name = "inventario_franquicia_idfranquicia1", referencedColumnName = "franquicia_idfranquicia",insertable=false,updatable=false)})
    @ManyToOne
    private Inventario inventario1;

    public SimilarInventario() {
    }

    public SimilarInventario(SimilarInventarioPK similarInventarioPK) {
        this.similarInventarioPK = similarInventarioPK;
    }

    public SimilarInventario(int inventarioIdinventario, int inventarioFranquiciaIdFranquicia, int inventarioIdinventario1, int inventarioFranquiciaIdFranquicia1) {
        this.similarInventarioPK = new SimilarInventarioPK(inventarioIdinventario, inventarioFranquiciaIdFranquicia, inventarioIdinventario1, inventarioFranquiciaIdFranquicia1);
    }

    public SimilarInventarioPK getSimilarInventarioPK() {
        return similarInventarioPK;
    }

    public void setSimilarInventarioPK(SimilarInventarioPK similarInventarioPK) {
        this.similarInventarioPK = similarInventarioPK;
    }

 
    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public Inventario getInventario1() {
        return inventario1;
    }

    public void setInventario1(Inventario inventario1) {
        this.inventario1 = inventario1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (similarInventarioPK != null ? similarInventarioPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SimilarInventario)) {
            return false;
        }
        SimilarInventario other = (SimilarInventario) object;
        if ((this.similarInventarioPK == null && other.similarInventarioPK != null) || (this.similarInventarioPK != null && !this.similarInventarioPK.equals(other.similarInventarioPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SimilarInventario[ similarInventarioPK=" + similarInventarioPK + " ]";
    }

    public Double getPriceDiference() {
        return priceDiference;
    }

    public void setPriceDiference(Double priceDiference) {
        this.priceDiference = priceDiference;
    }

    public double getEquivalencia() {
        return equivalencia;
    }

    public void setEquivalencia(double equivalencia) {
        this.equivalencia = equivalencia;
    }
    
}
