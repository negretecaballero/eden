/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SimilarInventarioPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_idinventario")
    private int inventarioIdinventario;
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="inventario_franquicia_idfranquicia")
    private int inventarioFranquiciaIdFranquicia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "inventario_idinventario1")
    private int inventarioIdinventario1;
   @javax.persistence.Basic(optional=true)
   @javax.validation.constraints.NotNull
   @javax.persistence.Column(name="inventario_franquicia_idfranquicia1")
   private int inventarioFranquiciaIdFranquicia1;

    public SimilarInventarioPK() {
    }

    public SimilarInventarioPK(int inventarioIdinventario, int inventarioFranquiciaIdFranquicia, int inventarioIdinventario1, int inventarioFranquiciaIdFranquicia1) {
        this.inventarioIdinventario = inventarioIdinventario;
        this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
        this.inventarioIdinventario1 = inventarioIdinventario1;
        this.inventarioFranquiciaIdFranquicia1=inventarioFranquiciaIdFranquicia1;
      }

    public int getInventarioIdinventario() {
        return inventarioIdinventario;
    }

    public void setInventarioIdinventario(int inventarioIdinventario) {
        this.inventarioIdinventario = inventarioIdinventario;
    }
    
    public int getInventarioFranquiciaIdFranquicia(){
    
       return this.inventarioFranquiciaIdFranquicia;
    
    }
    
    public void setInventarioFranquiciaIdFranquicia(int inventarioFranquiciaIdFranquicia){
    
    this.inventarioFranquiciaIdFranquicia=inventarioFranquiciaIdFranquicia;
    
    }
    
    public int getInventarioFranquicioaIdFranquicia1(){
    
        return this.inventarioFranquiciaIdFranquicia1;
    
    }
    
    public void setInventarioFranquiciaIdFranquicia1(int inventarioFranquiciaIdFranquicia1){
    
        this.inventarioFranquiciaIdFranquicia1=inventarioFranquiciaIdFranquicia1;
    
    }
    
    public int getInventarioIdinventario1() {
        return inventarioIdinventario1;
    }

    public void setInventarioIdinventario1(int inventarioIdinventario1) {
        this.inventarioIdinventario1 = inventarioIdinventario1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) inventarioIdinventario;
        hash += (int) inventarioFranquiciaIdFranquicia;
        
        hash += (int) inventarioIdinventario1;
    
        hash += (int) inventarioFranquiciaIdFranquicia1;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SimilarInventarioPK)) {
            return false;
        }
        SimilarInventarioPK other = (SimilarInventarioPK) object;
        if (this.inventarioIdinventario != other.inventarioIdinventario) {
            return false;
        }
        if (this.inventarioFranquiciaIdFranquicia != other.inventarioFranquiciaIdFranquicia) {
            return false;
        }
       
        if (this.inventarioIdinventario1 != other.inventarioIdinventario1) {
            return false;
        }
       
        if (this.inventarioFranquiciaIdFranquicia1 != other.inventarioFranquiciaIdFranquicia1) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SimilarInventarioPK[ inventarioIdinventario=" + inventarioIdinventario + ", inventarioFranquiciaIdFranquicia=" + inventarioFranquiciaIdFranquicia + ", inventarioIdinventario1=" + inventarioIdinventario1 + ", inventarioFranquiciaIdFranquicia1=" + inventarioFranquiciaIdFranquicia1 + " ]";
    }
    
}
