/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "similar_products")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SimilarProducts.findAll", query = "SELECT s FROM SimilarProducts s"),
    @NamedQuery(name = "SimilarProducts.findByFirstProduct",query="SELECT s from SimilarProducts s where s.producto.productoPK = :productPK"),
    @NamedQuery(name = "SimilarProducts.findByProductoIdproducto", query = "SELECT s FROM SimilarProducts s WHERE s.similarProductsPK.productoIdproducto = :productoIdproducto"),
    @NamedQuery(name = "SimilarProducts.findByProductoIdproducto1", query = "SELECT s FROM SimilarProducts s WHERE s.similarProductsPK.productoIdproducto1 = :productoIdproducto1"),
    @NamedQuery(name = "SimilarProducts.findByPriceDiference", query = "SELECT s FROM SimilarProducts s WHERE s.priceDiference = :priceDiference")})
public class SimilarProducts implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SimilarProductsPK similarProductsPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_diference")
    private Double priceDiference;
    @javax.persistence.JoinColumns({
   
    @javax.persistence.JoinColumn(name = "producto_idproducto", referencedColumnName = "idproducto",insertable=false,updatable=false),
            
    @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName="categoria_idcategoria",insertable=false,updatable=false),
    
    @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName="categoria_franquicia_idfranquicia",insertable=false,updatable=false)
    
            })
    @ManyToOne(optional = false)
    private Producto producto;
    
    @javax.persistence.JoinColumns({
        
        @javax.persistence.JoinColumn(name = "producto_idproducto1", referencedColumnName = "idproducto",insertable=false,updatable=false),
    
        @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria1",referencedColumnName="categoria_idcategoria",insertable=false,updatable=false),
        
        
        @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia1",referencedColumnName="categoria_franquicia_idfranquicia",insertable=false,updatable=false)
    
    })
    @ManyToOne(optional = false)
    private Producto producto1;

    public SimilarProducts() {
    }

    public SimilarProducts(SimilarProductsPK similarProductsPK) {
        this.similarProductsPK = similarProductsPK;
    }

    public SimilarProducts(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int productoIdproducto1, int productoCategoriaIdCategoria1,int productoCategoriaFranquiciaIdFranquicia1) {
        this.similarProductsPK = new SimilarProductsPK(productoIdproducto,productoCategoriaIdCategoria,productoCategoriaFranquiciaIdFranquicia, productoIdproducto1,productoCategoriaIdCategoria1,productoCategoriaFranquiciaIdFranquicia1);
    }

    public SimilarProductsPK getSimilarProductsPK() {
        return similarProductsPK;
    }

    public void setSimilarProductsPK(SimilarProductsPK similarProductsPK) {
        this.similarProductsPK = similarProductsPK;
    }

    public Double getPriceDiference() {
        return priceDiference;
    }

    public void setPriceDiference(Double priceDiference) {
        this.priceDiference = priceDiference;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Producto getProducto1() {
        return producto1;
    }

    public void setProducto1(Producto producto1) {
        this.producto1 = producto1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (similarProductsPK != null ? similarProductsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SimilarProducts)) {
            return false;
        }
        SimilarProducts other = (SimilarProducts) object;
        if ((this.similarProductsPK == null && other.similarProductsPK != null) || (this.similarProductsPK != null && !this.similarProductsPK.equals(other.similarProductsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SimilarProducts[ similarProductsPK=" + similarProductsPK + " ]";
    }
    
}
