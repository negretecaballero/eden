/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SimilarProductsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto")
    private int productoIdproducto;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria")
    private int productoCategoriaIdCategoria;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia")
    private int productoCategoriaFranquiciaIdFranquicia;
    
    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "producto_idproducto1")
    private int productoIdproducto1;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_idcategoria1")
    private int productoCategoriaIdCategoria1;
    
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.persistence.Column(name="producto_categoria_franquicia_idfranquicia1")
    private int productoCategoriaFranquiciaIdFranquicia1;

    public SimilarProductsPK() {
    }

    public SimilarProductsPK(int productoIdproducto,int productoCategoriaIdCategoria,int productoCategoriaFranquiciaIdFranquicia, int productoIdproducto1,int productoCategoriaIdCategoria1,int productoCategoriaFranquiciaIdFranquicia1) {
        this.productoIdproducto = productoIdproducto;
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
        this.productoIdproducto1 = productoIdproducto1;
        this.productoCategoriaIdCategoria1=productoCategoriaIdCategoria1;
        this.productoCategoriaFranquiciaIdFranquicia1=productoCategoriaFranquiciaIdFranquicia1;
    }

    public int getProductoIdproducto() {
        return productoIdproducto;
    }

    public void setProductoIdproducto(int productoIdproducto) {
        this.productoIdproducto = productoIdproducto;
    }

    public int getProductoIdproducto1() {
        return productoIdproducto1;
    }

    public void setProductoIdproducto1(int productoIdproducto1) {
        this.productoIdproducto1 = productoIdproducto1;
    }
    
    public int getProductoCategoriaIdCategoria(){
    
        return this.productoCategoriaIdCategoria;
    
    }
    
    public void setProductoCategoriaIdCategoria(int productoCategoriaIdCategoria){
    
        this.productoCategoriaIdCategoria=productoCategoriaIdCategoria;
        
    }
    
    public int getProductoCategoriaFranquiciaIdFranquicia(){
    
        return this.productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    public void setProductoCategoriaFranquciaIdFranquicia(int productoCategoriaFranquiciaIdFranquicia){
    
    this.productoCategoriaFranquiciaIdFranquicia=productoCategoriaFranquiciaIdFranquicia;
    
    }
    
    public int getProductoCategoriaIdCategoria1(){
    
    return this.productoCategoriaIdCategoria1;
    
    }
    
    public void setProductoCategoriaIdCategoria1(int productoCategoriaIdCategoria1){
    
    
    this.productoCategoriaIdCategoria1=productoCategoriaIdCategoria1;
    
    }
    
    public int getProductoCategoriaFranquiciaIdFranquicia1(){
    
    return this.productoCategoriaFranquiciaIdFranquicia1;
    
    }
    
    
    public void setProductoCategoriaFranquiciaIdFranquicia1(int productoCategoriaFranquiciaIdFranquicia1){
    
    this.productoCategoriaFranquiciaIdFranquicia1=productoCategoriaFranquiciaIdFranquicia1;
    
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) productoIdproducto;
        hash += (int)productoCategoriaIdCategoria;
        hash += (int)productoCategoriaFranquiciaIdFranquicia;
        hash += (int) productoIdproducto1;
        hash += (int)productoCategoriaIdCategoria1;
        hash += (int)productoCategoriaFranquiciaIdFranquicia1;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SimilarProductsPK)) {
            return false;
        }
        SimilarProductsPK other = (SimilarProductsPK) object;
        if (this.productoIdproducto != other.productoIdproducto) {
            return false;
        }
        if (this.productoCategoriaIdCategoria != other.productoCategoriaIdCategoria) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdFranquicia != other.productoCategoriaFranquiciaIdFranquicia) {
            return false;
        }
        if (this.productoIdproducto1 != other.productoIdproducto1) {
            return false;
        }
        if (this.productoCategoriaIdCategoria1 != other.productoCategoriaIdCategoria1) {
            return false;
        }
        if (this.productoCategoriaFranquiciaIdFranquicia1 != other.productoCategoriaFranquiciaIdFranquicia1) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SimilarProductsPK[ productoIdproducto=" + productoIdproducto + ", productoCategoriaIdCategoria="+productoCategoriaIdCategoria+", productoCategoriaFranquiciaIdFranquicia="+productoCategoriaFranquiciaIdFranquicia+", productoIdproducto1=" + productoIdproducto1 + ",productoCategoriaIdCategoria1="+productoCategoriaIdCategoria1+", productoCategoriaFranquiciaIdFranquicia1="+productoCategoriaFranquiciaIdFranquicia1+" ]";
    }
    
}
