/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "STATE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "State.findAll", query = "SELECT s FROM State s ORDER BY s.name"),
    @NamedQuery(name = "State.findByIdSTATE", query = "SELECT s FROM State s WHERE s.idSTATE = :idSTATE"),
    @NamedQuery(name = "State.findByName", query = "SELECT s FROM State s WHERE s.name = :name")})
public class State implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idSTATE")
    private Integer idSTATE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "state")
    @javax.persistence.OrderBy("name ASC")
    private Collection<City> cityCollection;

    public State() {
    }

    public State(Integer idSTATE) {
        this.idSTATE = idSTATE;
    }

    public State(Integer idSTATE, String name) {
        this.idSTATE = idSTATE;
        this.name = name;
    }

    public Integer getIdSTATE() {
        return idSTATE;
    }

    public void setIdSTATE(Integer idSTATE) {
        this.idSTATE = idSTATE;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<City> getCityCollection() {
        
      
        return cityCollection;
    }

    public void setCityCollection(Collection<City> cityCollection) {
        this.cityCollection = cityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSTATE != null ? idSTATE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof State)) {
            return false;
        }
        State other = (State) object;
        if ((this.idSTATE == null && other.idSTATE != null) || (this.idSTATE != null && !this.idSTATE.equals(other.idSTATE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.State[ idSTATE=" + idSTATE + " ]";
    }
    
}
