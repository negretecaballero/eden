/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="subtype")

@XmlRootElement

@javax.persistence.NamedQueries({

    @javax.persistence.NamedQuery(name="Subtype.findByName",query="SELECT s from Subtype s where s.name = :name"),
    
    @NamedQuery(name="Subtype.findByTypeSubtypeName",query="SELECT s from Subtype s where s.name = :subtypeName AND s.idTipo.nombre = :typeName"),
    
    @NamedQuery(name="Subtype.findAll",query="SELECT s from Subtype s ORDER BY s.name ASC")

})

public class Subtype implements java.io.Serializable{
    
    @javax.persistence.EmbeddedId
    
    private Entities.SubtypePK subtypePK;
    
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=45)
    @javax.persistence.Column(name="name")
    private String name;
    
    @javax.persistence.JoinColumn(name="tipo_idtipo",referencedColumnName="idtipo",insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Tipo idTipo;
    
    @javax.persistence.OneToMany(cascade=CascadeType.ALL,mappedBy="idSubtype")
    @javax.persistence.OrderBy("nombre ASC")
    private Collection<Entities.Franquicia>franquiciaCollection;
    
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="idSubtype")
    private java.util.Collection<Entities.Application>applicationCollection;
    
    

    
    public Subtype(){
    
        
    
    }
    
    
   
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.Application>getApplicationCollection(){
    
    return this.applicationCollection;
    
    }
    
    public void setApplicationCollection(java.util.Collection<Entities.Application>applicationCollection){
    
        this.applicationCollection=applicationCollection;
    
    }
    
    public Entities.SubtypePK getSubtypePK(){
    
    return this.subtypePK;
    
    }
    
    public void setSubtypePK(Entities.SubtypePK subtypePK){
    
    this.subtypePK=subtypePK;
    
    }
    
    public String getName(){
    
        return this.name;
    
    }
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    public Entities.Tipo getIdTipo(){
    
        return this.idTipo;
    
    }
    
    public void setIdTipo(Entities.Tipo idTipo){
    
    
        this.idTipo=idTipo;
    
    }
    
    public Collection<Entities.Franquicia>getFranquiciaCollection(){
    
    return this.franquiciaCollection;
    
    }
    
    public void setFranquiciaCollection(Collection<Entities.Franquicia>franquiciaCollection){
    
    this.franquiciaCollection=franquiciaCollection;
    
    }
    
    public Subtype(String name){
    
        this.name=name;
    
    }
    
      @Override
    public int hashCode() {
        int hash = 0;
        hash += (subtypePK != null ? subtypePK.hashCode() : 0);
        return hash;
    }
    
   @Override
    public boolean equals(Object object){
    
         if (!(object instanceof Entities.Subtype)) {
            return false;
        }
        Entities.Subtype other = (Entities.Subtype) object;
        if ((this.subtypePK == null && other.subtypePK != null) || (this.subtypePK != null && !this.subtypePK.equals(other.subtypePK))) {
            return false;
        }
        
        return true;
    
    }
    
    @Override
    public String toString(){
    
        return "Entities.Subtype[ subtypePK=" + subtypePK + " ]";
    
    }
}
