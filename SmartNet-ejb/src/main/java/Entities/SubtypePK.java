/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable

public class SubtypePK implements java.io.Serializable{
    
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.persistence.Column(name="idsubtype")
    private int idSubtype;
    
    
    
    @javax.persistence.Basic(optional=true)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.persistence.Column(name="tipo_idtipo")
    private int tipoIdtipo;
    
    public SubtypePK(){
    
    
    }
    
    public SubtypePK(int idSubtype,int tipoIdtipo){
    
    this.idSubtype=idSubtype;
    
    this.tipoIdtipo=tipoIdtipo;
    
    }
    
    public int getIdSubtype(){
    
        
        return this.idSubtype;
    
    }
    
    
    public void setIdSubtype(int idSubtype){
    
        this.idSubtype=idSubtype;
    
    }
    
    public int getTipoIdtipo(){
    
    return this.tipoIdtipo;
    
    }
    
    public void setTipoIdtipo(int tipoIdtipo){
    
    this.tipoIdtipo=tipoIdtipo;
        
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=(int)idSubtype;
        
        hash+=(int)tipoIdtipo;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubtypePK other = (SubtypePK) obj;
        if (this.idSubtype != other.idSubtype) {
            return false;
        }
        if (this.tipoIdtipo != other.tipoIdtipo) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return"Entities.SubtypePK[ idSubtype="+this.idSubtype+", tipoIdtipo="+this.tipoIdtipo+"]";
    
    }
    
}
