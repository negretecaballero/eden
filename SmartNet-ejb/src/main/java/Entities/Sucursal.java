/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "sucursal")
@javax.xml.bind.annotation.XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sucursal.findAll", query = "SELECT s FROM Sucursal s"),
    @NamedQuery(name = "Sucursal.findByIdsucursal", query = "SELECT s FROM Sucursal s WHERE s.sucursalPK.idsucursal = :idsucursal"),
    @NamedQuery(name = "Sucursal.findByFranquiciaIdfranquicia", query = "SELECT s FROM Sucursal s WHERE s.sucursalPK.franquiciaIdfranquicia = :franquiciaIdfranquicia"),
    @NamedQuery(name = "Sucursal.findByAlcance", query = "SELECT s FROM Sucursal s WHERE s.alcance = :alcance")})
public class Sucursal implements Serializable {
  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sucursal")
    private Collection<InventarioHasSucursal> inventarioHasSucursalCollection;

    @javax.persistence.JoinTable(
    
    name="producto_has_sucursal",joinColumns={
    
        @javax.persistence.JoinColumn(name="sucursal_idsucursal",referencedColumnName="idsucursal"),
        
        @javax.persistence.JoinColumn(name="sucursal_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia")
    
    }, inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name = "producto_idproducto",referencedColumnName = "idproducto"),
        
        @javax.persistence.JoinColumn(name = "producto_categoria_idcategoria",referencedColumnName = "categoria_idcategoria"),
        
        @javax.persistence.JoinColumn(name = "producto_categoria_franquicia_idfranquicia",referencedColumnName = "categoria_franquicia_idfranquicia")
    
    }
    
    )
    
    @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)

    private Collection<Entities.Producto>productoCollection;
    
    @javax.persistence.JoinTable(name="menu_has_sucursal",joinColumns={
    
        @javax.persistence.JoinColumn(name = "sucursal_idsucursal",referencedColumnName = "idsucursal"),
        
        @javax.persistence.JoinColumn(name = "sucursal_franquicia_idfranquicia",referencedColumnName = "franquicia_idfranquicia")
    
    },inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name="menu_idmenu",referencedColumnName="idmenu")
    
    })
    @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Entities.Menu>menuCollection;
    
    
   
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="sucursalId")
    private Collection<Entities.SucursalHasDay>sucursalHasDayCollection;
    
    
    @javax.persistence.JoinTable(name="sucursal_has_loginAdministrador",joinColumns={
    
        @javax.persistence.JoinColumn(name="sucursal_idsucursal",referencedColumnName="idsucursal"),
        
         @javax.persistence.JoinColumn(name="sucursal_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia")
        
    },inverseJoinColumns={
    
       @javax.persistence.JoinColumn(name="loginAdministrador_username",referencedColumnName="username")
    
    })
    @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Entities.LoginAdministrador> loginAdministradorCollection;
   
    
    @javax.persistence.JoinTable(name="sucursal_has_worker",joinColumns={
    
        @javax.persistence.JoinColumn(name="sucursal_idsucursal",referencedColumnName="idsucursal"),
        
        @javax.persistence.JoinColumn(name="sucursal_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia")
    
    },inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name="loginAdministrador_username",referencedColumnName="username")
    
    })
    @javax.persistence.ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Entities.LoginAdministrador>workerCollection;
    
    @javax.xml.bind.annotation.XmlTransient
    public Collection<Entities.LoginAdministrador>getWorkerCollection(){
    
    return this.workerCollection;
    
    }
    
    public void setWorkerCollection(java.util.Collection<Entities.LoginAdministrador>workerCollection){
    
    this.workerCollection=workerCollection;
    
    }
    
    @javax.xml.bind.annotation.XmlTransient
    public Collection<Entities.SucursalHasDay>getSucursalHasDayCollection(){
    
        return this.sucursalHasDayCollection;
    
    }
    
    public void setSucursalHasDayCollection(java.util.Collection<Entities.SucursalHasDay>sucursalHasDayCollection){
    
        this.sucursalHasDayCollection=sucursalHasDayCollection;
    
    }
    
    
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SucursalPK sucursalPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "alcance")
    private Double alcance;
     @JoinTable(name = "imagen_has_sucursal",  inverseJoinColumns= {
        @JoinColumn(name = "imagen_idimagen", referencedColumnName = "idimagen")}, joinColumns = {
        @JoinColumn(name = "sucursal_idsucursal", referencedColumnName = "idsucursal"),
        @JoinColumn(name = "sucursal_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia")})
    @ManyToMany(cascade=javax.persistence.CascadeType.ALL)
    private Collection<Imagen> imagenCollection;
  
    @OneToMany(mappedBy = "sucursal")
    private Collection<Pedido> pedidoCollection;
    @JoinColumn(name = "franquicia_idfranquicia", referencedColumnName = "idfranquicia", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Franquicia franquicia;
  
    @JoinColumns({
        @JoinColumn(name = "DIRECCION_idDireccion", referencedColumnName = "idDireccion"),
        @JoinColumn(name = "DIRECCION_CITY_idCITY", referencedColumnName = "CITY_idCITY"),
        @JoinColumn(name = "DIRECCION_CITY_STATE_idSTATE", referencedColumnName = "CITY_STATE_idSTATE")})
    @ManyToOne(optional = false, cascade=javax.persistence.CascadeType.ALL)
    private Direccion direccion;
  

    @javax.persistence.JoinTable(name="addition_has_sucursal",joinColumns={
    
        @javax.persistence.JoinColumn(name = "sucursal_idsucursal", referencedColumnName = "idsucursal"),
        
        @javax.persistence.JoinColumn(name = "sucursal_franquicia_idfranquicia",referencedColumnName = "franquicia_idfranquicia")
    
    },inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name = "addition_idaddition",referencedColumnName = "idaddition"),
        
        @javax.persistence.JoinColumn(name = "addition_franquicia_idfranquicia",referencedColumnName = "franquicia_idfranquicia")
    
    })
    @javax.persistence.ManyToMany
    private Collection<Entities.Addition>additionCollection;
    
    
    
    @javax.persistence.JoinTable(name = "sale_has_sucursal",joinColumns={
    
        @javax.persistence.JoinColumn(name = "sucursal_idsucursal", referencedColumnName="idsucursal"),
        
        @javax.persistence.JoinColumn(name = "sucursal_franquicia_idfranquicia",referencedColumnName= "franquicia_idfranquicia")
    
    },inverseJoinColumns={
    
        @javax.persistence.JoinColumn(name = "sale_idsale", referencedColumnName = "idsale"),
        
        @javax.persistence.JoinColumn(name = "sale_franquicia_idfranquicia", referencedColumnName = "franquicia_idfranquicia")
        
    })
    @javax.persistence.ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    private java.util.Collection <Entities.Sale> saleCollection;
    
    //Minutes
    @javax.persistence.Column(name="delivery_time")
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.persistence.Basic(optional=false)
    private Integer deliveryTime;
    
    //minial_delivery
    @javax.persistence.Column(name="minimal_delivery")
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.NotNull
    @javax.persistence.Basic(optional=false)
    private Integer minimalDelivery;
    
    public java.lang.Integer getMinimalDelivery(){
    
    return this.minimalDelivery;
    
    }
    
    public void setMinimalDelivery(int minimalDelivery){
    
        this.minimalDelivery=minimalDelivery;
    
    }
    
    
    public Integer getDeliveryTime(){
    
        return this.deliveryTime;
    
    }
    
    public void setDeliveryTime(int deliveryTime){
    
        this.deliveryTime=deliveryTime;
    
    }
    
    public java.util.Collection<Entities.Sale>getSaleCollection(){
    
    return this.saleCollection;
    
    }
    
    public void setSaleCollection(java.util.Collection<Entities.Sale>saleCollection){
    
    this.saleCollection=saleCollection;
    
    }
    
    
    public Collection<Entities.Addition>getAdditionCollection(){
    
    return this.additionCollection;
    
    }
    
    public void setAdditionCollection(Collection<Entities.Addition>additionCollection){
    
    this.additionCollection=additionCollection;
    
    }
    
    public Collection<Entities.Menu>getMenuCollection(){
    
        return this.menuCollection;
    
    }
    
    public void setMenuCollection(Collection<Entities.Menu>menuCollection){
    
        this.menuCollection=menuCollection;
    
    }
    
    public Sucursal() {
    }

    public Sucursal(SucursalPK sucursalPK) {
        this.sucursalPK = sucursalPK;
    }

    public Sucursal(int idsucursal, int franquiciaIdfranquicia) {
        this.sucursalPK = new SucursalPK(idsucursal, franquiciaIdfranquicia);
    }

    public SucursalPK getSucursalPK() {
        return sucursalPK;
    }

    public void setSucursalPK(SucursalPK sucursalPK) {
        this.sucursalPK = sucursalPK;
    }

    public Double getAlcance() {
        return alcance;
    }

    public void setAlcance(Double alcance) {
        this.alcance = alcance;
    }

    @XmlTransient
    public Collection<Imagen> getImagenCollection() {
        return imagenCollection;
    }

    public void setImagenCollection(Collection<Imagen> imagenCollection) {
        this.imagenCollection = imagenCollection;
    }

    @XmlTransient
    public Collection<Pedido> getPedidoCollection() {
        return pedidoCollection;
    }

    public void setPedidoCollection(Collection<Pedido> pedidoCollection) {
        this.pedidoCollection = pedidoCollection;
    }

    public Franquicia getFranquicia() {
        return franquicia;
    }

    public void setFranquicia(Franquicia franquicia) {
        this.franquicia = franquicia;
    }

   

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }
    
    

   
    @XmlTransient
    public Collection<Entities.Producto>getProductoCollection(){
    
        return this.productoCollection;
    
    }
    
    public void setProductoCollection(Collection<Entities.Producto>productoCollection){
    
        this.productoCollection=productoCollection;
    
    }
    
    
    
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="delivery_price")
    private int deliveryPrice;
    
    public int getDeliveryPrice(){
    
        return deliveryPrice;
          
    }
    
    public void setDeliveryPrice(int deliveryPrice){
    
    this.deliveryPrice=deliveryPrice;
    
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sucursalPK != null ? sucursalPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sucursal)) {
            return false;
        }
        Sucursal other = (Sucursal) object;
        if ((this.sucursalPK == null && other.sucursalPK != null) || (this.sucursalPK != null && !this.sucursalPK.equals(other.sucursalPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.Sucursal[ sucursalPK=" + sucursalPK + " ]";
    }

    @XmlTransient
    public Collection<Entities.LoginAdministrador> getLoginAdministradorCollection() {
        return this.loginAdministradorCollection;
    }

    public void setLoginAdministradorCollection(Collection<Entities.LoginAdministrador> loginAdministradorCollection) {
        this.loginAdministradorCollection = loginAdministradorCollection;
    }


    
    @XmlTransient
    public Collection<InventarioHasSucursal> getInventarioHasSucursalCollection() {
        return inventarioHasSucursalCollection;
    }

    public void setInventarioHasSucursalCollection(Collection<InventarioHasSucursal> inventarioHasSucursalCollection) {
        this.inventarioHasSucursalCollection = inventarioHasSucursalCollection;
    }



 


   


   

   
  

 

  

  
   
    
 

    
  
}
