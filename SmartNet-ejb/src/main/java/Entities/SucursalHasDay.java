/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Table(name="sucursal_has_day")
@javax.persistence.Entity
@javax.xml.bind.annotation.XmlRootElement
public class SucursalHasDay implements java.io.Serializable{
    
    private static final long serialVersionUID=1L;
    
    @javax.persistence.EmbeddedId
    private SucursalHasDayPK sucursalHasDayPK;
    
    @javax.persistence.Column(name="opentime")
    @javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
    private java.util.Date openTime;
    
    @javax.persistence.Column(name="closetime")
    @javax.persistence.Temporal(javax.persistence.TemporalType.TIME)
    private java.util.Date closeTime;
    
    
    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumn(name="day_idday",referencedColumnName="idday",insertable=false,updatable=false)
    private Entities.Day dayId;
    
    @javax.persistence.ManyToOne
    @javax.persistence.JoinColumns({
        
    @javax.persistence.JoinColumn(name="sucursal_idsucursal",referencedColumnName="idsucursal",insertable=false,updatable=false),
        
    @javax.persistence.JoinColumn(name="sucursal_franquicia_idfranquicia",referencedColumnName="franquicia_idfranquicia",insertable=false,updatable=false)
    
    })
    private Entities.Sucursal sucursalId;
    
    public Entities.Day getDayId(){
    
    return this.dayId;
        
    }
    
    public void setDayId(Entities.Day dayId){
    
    this.dayId=dayId;
    
    }
    
    public Entities.Sucursal getSucursalId(){
    
    return this.sucursalId;
    
    }
    
    public void setSucursalId(Entities.Sucursal sucursalId){
    
    this.sucursalId=sucursalId;
    
    }
    
    public Entities.SucursalHasDayPK getSucursalHasDayPK(){
    
    return this.sucursalHasDayPK;
    
    }
    
    public void setSucursalHasDayPK(Entities.SucursalHasDayPK sucursalHasDayPK){
    
    this.sucursalHasDayPK=sucursalHasDayPK;
    
    }
    
    public java.util.Date getOpenTime(){
    
    return this.openTime;
    
    }
    
    public void setOpenTime(java.util.Date openTime)
    
    {
    
    this.openTime=openTime;
    
    }
    
    public java.util.Date getCloseTime(){
    
        return this.closeTime;
    
    }
    
    public void setCloseTime(java.util.Date closeTime){
    
        this.closeTime=closeTime;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash=(this.sucursalHasDayPK!=null ?this.sucursalHasDayPK.hashCode():0);
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SucursalHasDay other = (SucursalHasDay) obj;
        if (!Objects.equals(this.sucursalHasDayPK, other.sucursalHasDayPK)) {
            return false;
        }
        return true;
    }
}
