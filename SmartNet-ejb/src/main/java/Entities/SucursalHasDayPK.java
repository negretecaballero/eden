/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

/**
 *
 * @author luisnegrete
 */
@javax.persistence.Embeddable
public class SucursalHasDayPK implements java.io.Serializable{
    
    @javax.persistence.Column(name="sucursal_idsucursal")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.Min(1)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.NotNull
    private int sucursalIdSucursal;
    
    @javax.persistence.Column(name="sucursal_franquicia_idfranquicia")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.Min(1)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.NotNull
    private int sucursalFranquiciaIdfranquicia;
    
    @javax.persistence.Column(name="day_idday")
    @javax.persistence.Basic(optional=false)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Min(1)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    private int dayIdday;
    
    public SucursalHasDayPK(){
    
        
    
    }
    
    public SucursalHasDayPK(int sucursalIdSucursal,int sucursalFranquiciaIdfranquicia,int dayIdday){
    
        this.sucursalIdSucursal=sucursalIdSucursal;
        
        this.sucursalFranquiciaIdfranquicia=sucursalFranquiciaIdfranquicia;
        
        this.dayIdday=dayIdday;
    
    }
    
    public int getSucursalIdSucursal(){
    
    return this.sucursalIdSucursal;
    
    }
    
    public void setSucursalIdSucursal(int sucursalIdSucursal){
    
    this.sucursalIdSucursal=sucursalIdSucursal;
    
    }
    
    public int getSucursalFranquiciaIdfranquicia(){
    
    return this.sucursalFranquiciaIdfranquicia;
    
    }
    
    public void setSucursalFranquiciaIdfranquicia(int sucursalFranquiciaIdfranquicia){
    
    this.sucursalFranquiciaIdfranquicia=sucursalFranquiciaIdfranquicia;
    
    }
    
    public int getDayIdday(){
    
        return this.dayIdday;
    
    }
    
    public void setDayIdday(int dayIdday){
    
        this.dayIdday=dayIdday;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=(int)this.dayIdday;
        
        hash+=(int)this.sucursalFranquiciaIdfranquicia;
        
        hash+=(int)this.sucursalIdSucursal;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SucursalHasDayPK other = (SucursalHasDayPK) obj;
        if (this.sucursalIdSucursal != other.sucursalIdSucursal) {
            return false;
        }
        if (this.sucursalFranquiciaIdfranquicia != other.sucursalFranquiciaIdfranquicia) {
            return false;
        }
        if (this.dayIdday != other.dayIdday) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
      return "Entities.SucursalHasDayPK [ sucursalIdSucursal="+this.sucursalIdSucursal+", sucursalFranquiciaIdfranquicia= "+this.sucursalFranquiciaIdfranquicia+", dayIdday="+this.dayIdday+" ]"; 
    
    }
}
