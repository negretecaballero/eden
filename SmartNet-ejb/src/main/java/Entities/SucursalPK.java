/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author luisnegrete
 */
@Embeddable
public class SucursalPK implements Serializable {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsucursal")
    private int idsucursal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "franquicia_idfranquicia")
    private int franquiciaIdfranquicia;

    public SucursalPK() {
    }

    public SucursalPK(int idsucursal, int franquiciaIdfranquicia) {
        this.idsucursal = idsucursal;
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    public int getIdsucursal() {
        return idsucursal;
    }

    public void setIdsucursal(int idsucursal) {
        this.idsucursal = idsucursal;
    }

    public int getFranquiciaIdfranquicia() {
        return franquiciaIdfranquicia;
    }

    public void setFranquiciaIdfranquicia(int franquiciaIdfranquicia) {
        this.franquiciaIdfranquicia = franquiciaIdfranquicia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idsucursal;
        hash += (int) franquiciaIdfranquicia;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SucursalPK)) {
            return false;
        }
        SucursalPK other = (SucursalPK) object;
        if (this.idsucursal != other.idsucursal) {
            return false;
        }
        if (this.franquiciaIdfranquicia != other.franquiciaIdfranquicia) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entities.SucursalPK[ idsucursal=" + idsucursal + ", franquiciaIdfranquicia=" + franquiciaIdfranquicia + " ]";
    }
    
}
