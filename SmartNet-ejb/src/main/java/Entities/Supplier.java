/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="supplier")

@javax.persistence.NamedQueries({

    @javax.persistence.NamedQuery(name="Supplier.findAll",query="Select s from Supplier s")

})

@javax.xml.bind.annotation.XmlRootElement

public class Supplier implements java.io.Serializable {
    
    
    @javax.persistence.EmbeddedId
    private Entities.SupplierPK supplierPK;
    
    
    
    @javax.persistence.OneToMany(mappedBy="idSupplier",cascade=javax.persistence.CascadeType.ALL)
    private java.util.Collection<Entities.SupplierProductCategory>supplierProductCategoryCollection;
    
    public java.util.Collection<Entities.SupplierProductCategory>getSupplierProductCategoryCollection(){

        return supplierProductCategoryCollection;
    
    }
    
    public void setSupplierProductCategoryController(java.util.Collection<Entities.SupplierProductCategory>supplierProductCategoryCollection){
    
        this.supplierProductCategoryCollection=supplierProductCategoryCollection;
    
    }
    
    @javax.persistence.JoinColumn(name="logo",referencedColumnName="idimagen",insertable=false,updatable=false)
    @javax.persistence.OneToOne(optional=false)
    private Entities.Imagen logo;
    
    
    @javax.persistence.JoinColumn(name="loginAdministrador_username",referencedColumnName="username",insertable=false,updatable=false)
    @javax.persistence.ManyToOne(optional=false)
    private Entities.LoginAdministrador idLoginAdministrador;
    
  
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=45)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="name")
    private String name;
    
    @javax.persistence.JoinColumn(name="supplier_application_nit",referencedColumnName="nit")
    @javax.persistence.OneToOne(optional=false)
    private Entities.SupplierApplication idSupplierApplication;
  
    
    public Entities.SupplierApplication getIdSupplierApplication(){
    
        return this.idSupplierApplication;
    
    }
    
    public void setIdSupplierApplication(Entities.SupplierApplication idSupplierApplication){
    
        this.idSupplierApplication=idSupplierApplication;
    
    }
    
    public Entities.SupplierPK getSupplierPK(){
    
        return this.supplierPK;
        
    }
    
    public void setSupplierPK(Entities.SupplierPK supplierPK){
    
        this.supplierPK=supplierPK;
    
    }
    
    
    public Entities.Imagen getLogo(){
    
    return this.logo;
    
    }
    
    public void setLogo(Entities.Imagen logo){
    
    this.logo=logo;
    
    }
    
    public Entities.LoginAdministrador getIdLoginAdministrador(){
    
        return this.idLoginAdministrador;
    
    }
    
    public void setIdLoginAdministrador(Entities.LoginAdministrador idLoginAdministrador){
    
        this.idLoginAdministrador=idLoginAdministrador;
        
    }
 
    
    public String getName(){
    
        return this.name;
    
    }
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=(this.supplierPK!=null?this.supplierPK.hashCode():0);
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Supplier other = (Supplier) obj;
        if (!Objects.equals(this.supplierPK, other.supplierPK)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
    return this.supplierPK.toString();
    
    }
    
}
