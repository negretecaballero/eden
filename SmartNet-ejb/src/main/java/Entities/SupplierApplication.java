/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.NamedQuery;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="supplier_application")

@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.NamedQueries({

    @NamedQuery(name="SupplierApplication.findByState",query="SELECT s from SupplierApplication s where s.applicationState = :applicationState"),
    
    @NamedQuery(name="SupplierApplication.findByStateUsername",query="Select s from SupplierApplication s where s.mail = :mail and s.applicationState = :applicationState")

})

public class SupplierApplication implements java.io.Serializable{
    
    private final static long serialVersionUID=1L;
    
    
    @javax.persistence.OneToOne(mappedBy="idSupplierApplication",cascade=javax.persistence.CascadeType.ALL)
    private Entities.Supplier idSupplier;
    
    public Entities.Supplier getIdSupplier(){
    
        return this.idSupplier;
    
    }
    
    public void setIdSupplier(Entities.Supplier idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    @Validators.MailValidator
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=64)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="mail")
    private String mail;
    
    public String getMail(){
    
        return this.mail;
    
    }
    
    public void setMail(String mail){
    
        this.mail=mail;
    
    }
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=1,max=45)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="name")
    private String name;
    
    public String getName(){
    
        return this.name;
    
    }
    
    public void setName(String name){
    
    this.name=name;
    
    }
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=11,max=11)
    @javax.persistence.Id
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="nit")
    private String nit;
    
    public String getNit(){
    
        return this.nit;
    
    }
    
    public void setNit(String nit){
    
        this.nit=nit;
    
    }
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Long.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Long.MIN_VALUE)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="cedula")
    private long cedula;
    
    public long getCedula(){
    
    return this.cedula;
    
    }
    
    public void setCedula(long cedula){
    
        this.cedula=cedula;
    
    }
    
    @javax.persistence.Temporal(javax.persistence.TemporalType.DATE)
    @javax.validation.constraints.NotNull
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="fecha_expedicion")
    private java.util.Date fechaExpedicion;

    public java.util.Date getFechaExpedicion(){
    
    return this.fechaExpedicion;
    
    }
    
    public void setFechaExpedicion(java.util.Date fechaExpedicion){
    
        this.fechaExpedicion=fechaExpedicion;
    
    }
    
    
    @javax.validation.constraints.Size(min=4,max=4)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="codigo_ciuu")
    private String codigoCiuu;
    
    public String getCodigoCiuu(){
    
        return this.codigoCiuu;
    
    }
    
    public void setCodigoCiuu(String codigoCiuu){
    
    this.codigoCiuu=codigoCiuu;
    
    }
    
    @javax.persistence.JoinColumn(name="logo",referencedColumnName="idimagen")
    @javax.persistence.OneToOne(optional=false,cascade=javax.persistence.CascadeType.PERSIST)
    private Entities.Imagen logo;
    
    public Entities.Imagen getLogo(){
    
    return this.logo;
    
    }
    
    public void setLogo(Entities.Imagen logo){
    
        this.logo=logo;
    
    }
    
    @javax.persistence.ManyToOne(optional=false)
    @javax.persistence.JoinColumns({
    
        @javax.persistence.JoinColumn(name="CITY_idCITY",referencedColumnName="idCITY"),
        
        @javax.persistence.JoinColumn(name="CITY_STATE_idSTATE",referencedColumnName="STATE_idSTATE")
    
    })
    private Entities.City city;
    
    public Entities.City getCity(){
    
        return this.city;
    
    }
    
    public void setCity(Entities.City city){
    
    this.city=city;
    
    }
    
    
 
   @javax.persistence.Column(name="application_state")
   @javax.persistence.Enumerated(javax.persistence.EnumType.STRING)
   private Entitites.Enum.SupplierApplicationState applicationState;
   
   public Entitites.Enum.SupplierApplicationState getApplicationState(){
   
       return this.applicationState;
   
   }
   
   public void setApplicationState(Entitites.Enum.SupplierApplicationState applicationState){
   
       this.applicationState=applicationState;
   
   }
    
   @javax.persistence.JoinColumn(name="supplier_type_idsupplier_type",referencedColumnName="idsupplier_type")
   @javax.persistence.ManyToOne(optional=false)
   private Entities.SupplierType idSupplierType;
    
   public Entities.SupplierType getIdSupplierType(){
   
       return this.idSupplierType;
   
   }
   
   public void setIdSupplierType(Entities.SupplierType idSupplierType){
   
       this.idSupplierType=idSupplierType;
   
   }
   
   @javax.persistence.JoinColumn(name="supplier_product_type_idsupplier_product_type",referencedColumnName="idsupplier_product_type")
   @javax.persistence.ManyToOne(optional=false)
   private Entities.SupplierProductType idSupplierProductType;
   
   public Entities.SupplierProductType getIdSupplierProductType(){
   
       return this.idSupplierProductType;
   
   }
   
   public void setIdSupplierProductType(Entities.SupplierProductType idSupplierProductType){
   
   this.idSupplierProductType=idSupplierProductType;
   
   }
   
   
    public SupplierApplication(){
    
    
    }
    
    
    
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=this.nit!=null?this.nit.hashCode():0;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierApplication other = (SupplierApplication) obj;
        if (!Objects.equals(this.nit, other.nit)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
    return "Entities.SupplierApplication [ nit = "+this.nit+" ]";
    
    }
    
}
