/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable
public class SupplierPK {

    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="idsupplier")
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    private int idSupplier;
    
    @javax.validation.constraints.NotNull
    @Validators.MailValidator
    @javax.validation.constraints.Size(max=64,min=3)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="loginAdministrador_username")
    private String loginAdministradorUsername;
    
    
    public int getIdSupplier(){
    
        return this.idSupplier;
    
    }
    
    public void setIdSupplier(int idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    public String getLoginAdministradorUsername(){
    
        return this.loginAdministradorUsername;
    
    }
    
    public void setLoginAdministradorUsername(String loginAdministradorUsername){
    
    this.loginAdministradorUsername=loginAdministradorUsername;
    
    }
    
    public SupplierPK(){
    
    }
    
    public SupplierPK(Integer idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    public SupplierPK(Integer idSupplier,String loginAdministradorUsername){
    
        this.idSupplier=idSupplier;
        
        this.loginAdministradorUsername=loginAdministradorUsername;
    
    }
    
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
      
        
        hash+=(this.loginAdministradorUsername!=null?this.loginAdministradorUsername.hashCode():0);
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierPK other = (SupplierPK) obj;
        if (!Objects.equals(this.idSupplier, other.idSupplier)) {
            return false;
        }
        if (!Objects.equals(this.loginAdministradorUsername, other.loginAdministradorUsername)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return "Entities.SupplierPK [ idSupplier = "+this.idSupplier+", loginAdministradorUsername = "+this.loginAdministradorUsername+" ]";
    
    }
    
}
