/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;
import javax.persistence.JoinColumn;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="supplier_product_category")

@javax.xml.bind.annotation.XmlRootElement

@javax.persistence.NamedQueries({


})

public class SupplierProductCategory implements java.io.Serializable {
 
    private static final long serialVersionUID=1L;
    
    @javax.persistence.EmbeddedId
    private Entities.SupplierProductCategoryPK supplierProductCategoryPK;
    
    
    public Entities.SupplierProductCategoryPK getSupplierProductCategoryPK(){
    
        return this.supplierProductCategoryPK;
    
    }
    
    public void setSupplierProductCategoryPK(Entities.SupplierProductCategoryPK supplierProductCategoryPK){
    
        this.supplierProductCategoryPK=supplierProductCategoryPK;
    
    }
    
    @javax.persistence.JoinColumns({
    
        @javax.persistence.JoinColumn(name="supplier_idsupplier",referencedColumnName="idsupplier",insertable=false,updatable=false),
        
        @javax.persistence.JoinColumn(name="supplier_loginAdministrador_username",referencedColumnName="loginAdministrador_username",insertable=false,updatable=false)
    
    })
    @javax.persistence.ManyToOne(optional=false)
    private Entities.Supplier idSupplier;
    
    public Entities.Supplier getIdSupplier(){
    
    return this.idSupplier;
    
    }
    
    public void setIdSupplier(Entities.Supplier idSupplier){
    
        this.idSupplier=idSupplier;
    
    }
    
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="name")
    @javax.validation.constraints.Size(min=1,max=45)
    @javax.validation.constraints.NotNull
    private String name;
    
    public String getName(){
    
        return this.name;
    
    }
    
    
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    
    @javax.persistence.JoinTable(name="supplier_product_category_has_imagen",
            
            joinColumns={@JoinColumn(name="supplier_product_category_id_supplier_product_category",referencedColumnName="id_supplier_product_category",insertable=false,updatable=false),
            
                @javax.persistence.JoinColumn(name="supplier_product_category_supplier_loginAdministrador_username",referencedColumnName="supplier_loginAdministrador_username",insertable=false,updatable=false),
                
                @javax.persistence.JoinColumn(name="supplier_product_category_supplier_idsupplier",referencedColumnName="supplier_idsupplier",insertable=false,updatable=false)
            
            },
            
            inverseJoinColumns={@javax.persistence.JoinColumn(name="imagen_idimagen",referencedColumnName="idimagen",insertable=false,updatable=false)})
    @javax.persistence.ManyToMany
    private java.util.Collection<Entities.Imagen>imagenCollection;
    
    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.Imagen>getImagenCollection(){
    
        return this.imagenCollection;
    
    }
    
    public void setImagenCollection(java.util.Collection<Entities.Imagen>imagenCollection){
    
        this.imagenCollection=imagenCollection;
    
    }
                
    @Override
    public int hashCode(){
    
    int hash=0;
    
    hash+=this.supplierProductCategoryPK!=null?this.supplierProductCategoryPK.hashCode():0;
    
    return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierProductCategory other = (SupplierProductCategory) obj;
        if (!Objects.equals(this.supplierProductCategoryPK, other.supplierProductCategoryPK)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
    
        return this.supplierProductCategoryPK.toString();
    
    }
    
}
