/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Embeddable
public class SupplierProductCategoryPK {
  
    
    @javax.persistence.Column(name="id_supplier_product_category")
    @javax.persistence.Basic(optional=false)
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    private int idSupplierProductCategory;
    
    public int getIdSupplierProductCategory(){
    
        return this.idSupplierProductCategory;
        
    }
    
    public void setIdSupplierProductCategory(int idSupplierProductCategory){
    
    this.idSupplierProductCategory=idSupplierProductCategory;
    
    }
    
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="supplier_idsupplier")
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    private int supplierIdsupplier;
    
    public int getSupplierIdsupplier(){
    
        return this.supplierIdsupplier;
    
    }
    
    public void setSupplierIdsupplier(int supplierIdsupplier){
    
        this.supplierIdsupplier=supplierIdsupplier;
    
    }
    
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="supplier_loginAdministrador_username")
    @javax.validation.constraints.Size(min=1,max=64)
    @javax.validation.constraints.NotNull
    private String supplierLoginAdministradorUsername;
    
    public String getSupplierLoginAdministradorUsername(){
    
    return this.supplierLoginAdministradorUsername;
    
    }
    
    public void setSupplierLoginAdministradorUsername(String supplierLoginAdministradorUsername){
    
        this.supplierLoginAdministradorUsername=supplierLoginAdministradorUsername;
    
    }
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=this.idSupplierProductCategory;
        
        hash+=this.supplierIdsupplier;
        
        hash+=this.supplierLoginAdministradorUsername!=null?this.supplierLoginAdministradorUsername.hashCode():0;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierProductCategoryPK other = (SupplierProductCategoryPK) obj;
        if (this.idSupplierProductCategory != other.idSupplierProductCategory) {
            return false;
        }
        if (this.supplierIdsupplier != other.supplierIdsupplier) {
            return false;
        }
        if (!Objects.equals(this.supplierLoginAdministradorUsername, other.supplierLoginAdministradorUsername)) {
            return false;
        }
        return true;
    }
    
    
    @Override
    public String toString(){
    
        return this.idSupplierProductCategory+","+this.supplierIdsupplier+","+this.supplierLoginAdministradorUsername;
    
    }
    
}
