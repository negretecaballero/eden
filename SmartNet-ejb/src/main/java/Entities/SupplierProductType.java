/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity

@javax.persistence.Table(name="supplier_product_type")

@javax.xml.bind.annotation.XmlRootElement

public class SupplierProductType implements java.io.Serializable{
    
    private static final long serialVersionUID=1L;
    
    
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.persistence.Id
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="idsupplier_product_type")
    private Integer idSupplierProductType;

     
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Size(min=0,max=45)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="product_type")
    private String productType;
    
    
    
    @javax.persistence.OneToMany(mappedBy="idSupplierProductType",cascade=javax.persistence.CascadeType.ALL)
    private java.util.Collection<Entities.SupplierApplication>supplierApplicationCollection;
    
    public java.util.Collection<Entities.SupplierApplication>getSupplierApplicationCollection(){
    
        return this.supplierApplicationCollection;
    
    }
    
    public void setSupplierApplicationCollection(java.util.Collection<Entities.SupplierApplication>supplierApplicationCollection){
    
        this.supplierApplicationCollection=supplierApplicationCollection;
    
    }


    
    public Integer getIdSupplierProductType(){
    
    return this.idSupplierProductType;
    
    }
    
    public void setIdSupplierProductType(Integer idSupplierProductType){
    
        this.idSupplierProductType=idSupplierProductType;
    
    }

    
    
    public String getProductType(){
    
        return this.productType;
    
    }
    
    public void setProductType(String productType){
    
        this.productType=productType;
        
    }
    
    
    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=this.idSupplierProductType!=null?this.idSupplierProductType.hashCode():0;
        
        return hash;
    
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierProductType other = (SupplierProductType) obj;
        if (!Objects.equals(this.idSupplierProductType, other.idSupplierProductType)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString(){
    
    return "";
    
    }
    
}
