/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.util.Objects;

/**
 *
 * @author luisnegrete
 */

@javax.persistence.Entity
@javax.persistence.Table(name="supplier_type")
@javax.xml.bind.annotation.XmlRootElement
@javax.persistence.NamedQueries({

    
    
})
public class SupplierType implements java.io.Serializable{
    
    private static final long serialVersionUID=1L;
    
    @javax.persistence.GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @javax.persistence.Id
    @javax.validation.constraints.NotNull
    @javax.validation.constraints.Min(java.lang.Integer.MIN_VALUE)
    @javax.validation.constraints.Max(java.lang.Integer.MAX_VALUE)
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="idsupplier_type")
    private Integer idSupplierType;
    
    
    @javax.validation.constraints.NotNull
    @javax.persistence.Basic(optional=false)
    @javax.persistence.Column(name="name")
    @javax.validation.constraints.Size(min=0,max=45)
    private String name;
    
    @javax.persistence.OneToMany(mappedBy="idSupplierType",cascade = javax.persistence.CascadeType.ALL)
    private java.util.Collection<Entities.SupplierApplication>supplierApplicationCollection;
    
    public java.util.Collection<Entities.SupplierApplication>getSupplierApplicationCollection(){
    
        return this.supplierApplicationCollection;
    
    }
    
    public void setSupplierApplicationCollection(java.util.Collection<Entities.SupplierApplication>supplierApplicationCollection){
    
        this.supplierApplicationCollection=supplierApplicationCollection;
    
    }
    
    public Integer getIdSupplierType(){
    
        return this.idSupplierType;
    
    }
    
    public void setIdSupplierType(Integer idSupplierType){
    
        this.idSupplierType=idSupplierType;
    
    }
    
    public String getName(){
    
        return this.name;
        
    }
    
    public void setName(String name){
    
    this.name=name;
    
    }
  

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SupplierType other = (SupplierType) obj;
        if (!Objects.equals(this.idSupplierType, other.idSupplierType)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode(){
    
        int hash=0;
        
        hash+=(idSupplierType !=null ? idSupplierType.hashCode():0);
    
         return hash;
    }

    @Override
    public String toString(){
    
    return "Entities.SupplierType [ idSupplierType = "+this.idSupplierType+"]";
    
    }
}
