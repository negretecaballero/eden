/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "tipo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipo.findAll", query = "SELECT t FROM Tipo t ORDER BY t.nombre ASC"),
    @NamedQuery(name = "Tipo.findByIdtipo", query = "SELECT t FROM Tipo t WHERE t.idtipo = :idtipo"),
    @NamedQuery(name = "Tipo.findByNombre", query = "SELECT t FROM Tipo t WHERE t.nombre = :nombre")})
public class Tipo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipo")
    private Integer idtipo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipo")
    @javax.persistence.OrderBy("name ASC")
    private Collection<Entities.Subtype> subtypeCollection;
    
    
    @javax.persistence.OneToMany(cascade=javax.persistence.CascadeType.ALL,mappedBy="tipoId")
    private java.util.Collection<Entities.News>newsCollection;

    public Tipo() {
    }

    public Tipo(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public Tipo(Integer idtipo, String nombre) {
        this.idtipo = idtipo;
        this.nombre = nombre;
    }

    public Integer getIdtipo() {
        return idtipo;
    }

    public void setIdtipo(Integer idtipo) {
        this.idtipo = idtipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public Collection<Entities.Subtype> getSubtypeCollection() {
        return this.subtypeCollection;
    }

    public void setSubtypeCollection(Collection<Entities.Subtype> subtypeCollection) {
        this.subtypeCollection = subtypeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipo != null ? idtipo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipo)) {
            return false;
        }
        Tipo other = (Tipo) object;
        if ((this.idtipo == null && other.idtipo != null) || (this.idtipo != null && !this.idtipo.equals(other.idtipo))) {
            return false;
        }
        return true;
    }

    
    @javax.xml.bind.annotation.XmlTransient
    public java.util.Collection<Entities.News>getNewsCollection(){
    
    return this.newsCollection;
    
    }
    
    public void setNewsCollection(java.util.Collection<Entities.News>newsCollection){
    
    this.newsCollection=newsCollection;
    
    }
    
    @Override
    public String toString() {
        return "Entities.Tipo[ idtipo=" + idtipo + " ]";
    }
    
}
