/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author luisnegrete
 */
@Entity
@Table(name = "TYPE_ADDRESS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeAddress.findAll", query = "SELECT t FROM TypeAddress t"),
    @NamedQuery(name = "TypeAddress.findByIdTYPEADDRESS", query = "SELECT t FROM TypeAddress t WHERE t.idTYPEADDRESS = :idTYPEADDRESS"),
    @NamedQuery(name = "TypeAddress.findByTypeAddress", query = "SELECT t FROM TypeAddress t WHERE t.typeAddress = :typeAddress")})
public class TypeAddress implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idTYPE_ADDRESS")
    private Integer idTYPEADDRESS;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "TYPE_ADDRESS")
    private String typeAddress;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tYPEADDRESSidTYPEADDRESS")
    private Collection<Direccion> direccionCollection;

    public TypeAddress() {
    }

    public TypeAddress(Integer idTYPEADDRESS) {
        this.idTYPEADDRESS = idTYPEADDRESS;
    }

    public TypeAddress(Integer idTYPEADDRESS, String typeAddress) {
        this.idTYPEADDRESS = idTYPEADDRESS;
        this.typeAddress = typeAddress;
    }

    public Integer getIdTYPEADDRESS() {
        return idTYPEADDRESS;
    }

    public void setIdTYPEADDRESS(Integer idTYPEADDRESS) {
        this.idTYPEADDRESS = idTYPEADDRESS;
    }

    public String getTypeAddress() {
        return typeAddress;
    }

    public void setTypeAddress(String typeAddress) {
        this.typeAddress = typeAddress;
    }

    @XmlTransient
    public Collection<Direccion> getDireccionCollection() {
        return direccionCollection;
    }

    public void setDireccionCollection(Collection<Direccion> direccionCollection) {
        this.direccionCollection = direccionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTYPEADDRESS != null ? idTYPEADDRESS.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeAddress)) {
            return false;
        }
        TypeAddress other = (TypeAddress) object;
        if ((this.idTYPEADDRESS == null && other.idTYPEADDRESS != null) || (this.idTYPEADDRESS != null && !this.idTYPEADDRESS.equals(other.idTYPEADDRESS))) {
            return false;
        }
        return true;
    }

  
    
    
    @Override
    public String toString() {
        return "Entities.TypeAddress[ idTYPEADDRESS=" + idTYPEADDRESS + " ]";
    }
    
}
