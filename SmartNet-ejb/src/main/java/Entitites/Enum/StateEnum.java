/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitites.Enum;

/**
 *
 * @author luisnegrete
 */
public enum StateEnum {
    
    pending("pending"),accepted("accepted");
    
    private final String name;
    
    private StateEnum(String s){
    
        this.name=s;
    
    }
    
     public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    @Override
    public String toString() {
       return this.name;
    }
}
