/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entitites.Enum;

/**
 *
 * @author luisnegrete
 */
public enum SupplierApplicationState {
    
    accepted("accepted"),pending("pending");
    
    private final String name;
    
    private SupplierApplicationState(String s){
    
        name=s;
    
    }
    
     public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    @Override
    public String toString() {
       return this.name;
    }

}
