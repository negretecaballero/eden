/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Iterators;

import java.util.Iterator;

/**
 *
 * @author luisnegrete
 */
public class CircularIterator implements Iterator<Object> {

  
      private int currentPosition;
    private SessionClasses.EdenList first;
    
    public CircularIterator(SessionClasses.EdenList aux){
    
    first=aux;
    
    currentPosition=0;
    
    }
    
    @Override
    public boolean hasNext() {
   
  
    
         
     System.out.print("Current Position :"+currentPosition+" Size: "+size());
     
    return (currentPosition<size());
    
    }
    
    private int size(){
    
   int index=0;     
        
   if(first!=null){
   
   if(first.rnext==null){
   
       return 1;
   
   }
   
   SessionClasses.EdenList aux=first;
   
   while(true){
   
       index++;
       
       
       
       if(aux.rnext==null){
       
       return index;
       
       }
       
       else{
       
       aux=aux.rnext;
       
       }
   
   }
   
   
   
   }
        
    
    return 0;
    }

    @Override
    public Object next() {
        
    System.out.print("You are in next");   
        
    SessionClasses.EdenList el = get(currentPosition);
    
    System.out.print("Next method object value "+el.getObject());
    
    currentPosition = (currentPosition + 1); 
    
    return el.getObject(); 
    
    }
    
    
    private SessionClasses.EdenList get(int index){
    
        int aux=0;
        
        SessionClasses.EdenList auxi=first;
        
        if(auxi!=null){
        
           while(auxi!=null){
           
               if(aux==index){
               
                   System.out.print("Value found");
                   
                   return auxi;
          
               }
               
               aux=aux+1;
               
               auxi=auxi.rnext;
           
           }
        
        }
        
    return null;
    
    }
    
    @Override
    public void remove(){
    
     SessionClasses.EdenList el = get(currentPosition);
     
     el=null;
    
    }
    
}
