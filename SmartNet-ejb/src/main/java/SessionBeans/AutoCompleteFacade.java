/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateful
@javax.ejb.StatefulTimeout(unit=java.util.concurrent.TimeUnit.MINUTES,value=30)
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
public class AutoCompleteFacade implements AutoCompleteFacadeLocal{
  
    java.util.List<String>autoComplete;
    
    
    java.util.List<Object>objectList;
    
    
    @Override
    public java.util.List<Object>getObjectList(){
    
        if(this.objectList==null)
        {
        
            this.objectList=new java.util.ArrayList<Object>();
        
        }
        
        return this.objectList;
    
    }
    
    @Override
    public void setObjectList(java.util.List<Object>objectList){
    
        this.objectList=objectList;
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePasivate(){
    
        System.out.print("prePassivate");
        
    }
    
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("Post Activate");
    
    }
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void destroy(){
        
        System.out.print("Destroy");
        
    }
    
    @Override
    public java.util.List<String>getAutoComplete(){
    
    return this.autoComplete;
    
    }
    
    @Override
    public void setAutoComplete(java.util.List<String>autoComplete){
    
    this.autoComplete=autoComplete;
    
    }
}
