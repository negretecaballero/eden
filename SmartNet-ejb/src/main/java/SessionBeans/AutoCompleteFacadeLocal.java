/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface AutoCompleteFacadeLocal {
    
    java.util.List<String>getAutoComplete();
    
    void setAutoComplete(java.util.List<String>autoComplete);
            
    java.util.List<Object>getObjectList();
    
    void setObjectList(java.util.List<Object>objectList);
}
