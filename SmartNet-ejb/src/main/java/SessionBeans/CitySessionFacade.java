/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateful
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)


public class CitySessionFacade implements CitySessionFacadeLocal {
 
    private java.util.List<Entities.City>cityList;
    
    
    @Override
    public java.util.List<Entities.City>getCityList(){
    
    return cityList;
    
    }
    
    
    @Override
    public void setCityList(java.util.List<Entities.City>cityList){
    
    this.cityList=cityList;
    
    }
    
    
    @javax.ejb.PostActivate
    public void postActivate(){
    
        System.out.print("CitySessionFacade PostActivate");
    
    }
    
    
    @javax.ejb.PrePassivate
    public void prePassivate(){

        System.out.print("CitySessionFacade prePassivate");
    
    }
    
    
    @javax.ejb.Remove
    public void Remove(){
    
        System.out.print("Remove CitySessionFacade");
    
    }
    
}
