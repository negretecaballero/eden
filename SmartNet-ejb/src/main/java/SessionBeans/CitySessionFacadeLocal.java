/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface CitySessionFacadeLocal {
    
    java.util.List<Entities.City>getCityList();
    
    void setCityList(java.util.List<Entities.City>cityList);
    
}
