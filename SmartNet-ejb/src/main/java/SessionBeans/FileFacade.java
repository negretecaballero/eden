/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;



/**
 *
 * @author luisnegrete
 */
@Stateless()
@javax.ejb.StatefulTimeout(unit=java.util.concurrent.TimeUnit.MINUTES,value=30)
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class FileFacade implements FileFacadeLocal {

    String root;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    private  String[] alphabet={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    
    @Override
    public void createFolder(javax.faces.context.FacesContext context){
    
    try{
    
        String folderName=this.findFolderName(context);
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)context.getExternalContext().getContext();
        
        java.io.File file = new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
    
       if(file.mkdir()){
       
           System.out.print("Folder Created");
       
       }
       
       root=folderName;
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public String getRoot(){
    
  
        
        return this.root;
        
       
    
    }
    
    
    @Override
    public void setRoot(String root){
    
        this.root=root;
    
    }
    
    private String getName(){
    
        String result="";
     
        java.util.Random random=new java.util.Random();
        
    for(int i=0;i<6;i++){
    
        
        
    int index=random.nextInt(alphabet.length);
    
    result=result+alphabet[index];
    
    }
    
    return result;
    
    }
            
    private String findFolderName(javax.faces.context.FacesContext context){
        
    String folderName="";
 
    String aux=this.getName();
    
    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)context.getExternalContext().getContext();
    
    java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+aux);
           
    if(!file.exists()){
    
        folderName=aux;
    
    }
    
    if(folderName.equals(""))
    {
    
        System.out.print(aux);
        
        folderName=findFolderName(context);
    
    }
    
    return folderName;
    }
    
    
    @SuppressWarnings("unused")
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
        System.out.print("In prepassivate method");
    
    }
    
    @SuppressWarnings("unused")
    @javax.ejb.PostActivate
    public void postActivate(){
    
        System.out.print("In postactivate method");
    
    }
    
   
    
    @javax.ejb.Remove
    public void Destroy(){
    
        try{
        
        System.out.print("Removing FileFacade");
    
        javax.faces.context.FacesContext facesContext=FacesContext.getCurrentInstance();
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)facesContext.getExternalContext().getContext();
        
        java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.root);
        
        if(file.delete()){
        
        System.out.print(this.root+" deleted");
        
        }
        
        }
        catch(Exception ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    
}