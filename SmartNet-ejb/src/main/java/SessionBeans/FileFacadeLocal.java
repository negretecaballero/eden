/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface FileFacadeLocal {
    
    void createFolder(javax.faces.context.FacesContext context);
    
    String getRoot();
    
    void setRoot(String root);
    
}
