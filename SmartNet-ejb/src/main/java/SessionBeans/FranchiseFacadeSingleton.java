/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Local
public interface FranchiseFacadeSingleton {
    
    java.util.List<Entities.Franquicia>getFranchises();
    
    void setFranchises(java.util.List<Entities.Franquicia>franchises);
    
    
}
