/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Singleton
@javax.ejb.Startup
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class FranchiseFacadeSingletonImplementation implements FranchiseFacadeSingleton{
    
    private java.util.List<Entities.Franquicia>franchises=new java.util.ArrayList<Entities.Franquicia>();
    
    @Override
    public java.util.List<Entities.Franquicia>getFranchises(){
    
    return this.franchises;
    
    }
    
    @Override
    public void setFranchises(java.util.List<Entities.Franquicia>franchises){
    
        this.franchises=franchises;
    
    }
    
  @javax.ejb.PostActivate
  @SuppressWarnings("unused")
  public void postActivate(){
  
      System.out.print("POST ACTIVATE");
  
  }
  
  @javax.ejb.PrePassivate
  @SuppressWarnings("unused")
  public void prePassivate(){
  
      System.out.print("Pre Passivate");
  
  }
  
  @javax.ejb.Remove
  @SuppressWarnings("unused")
  public void remove(){
  
      System.out.print("Remove");
  
  }
  
  
    
   
    
}
