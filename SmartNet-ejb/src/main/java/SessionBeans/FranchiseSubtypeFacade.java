/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Stateful
public class FranchiseSubtypeFacade implements FranchiseSubtypeFacadeLocal{
    
    private java.util.Vector<Entities.Subtype>subtypeArray;
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("Franchise Subtype post activate");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
    System.out.print("Franchise Subtype PrePassivate");
        
    }
    
    @javax.ejb.Remove
    public void Remove(){
    
        System.out.print("Remove");
    
    }
    
    @Override
    public java.util.Vector<Entities.Subtype>getSubtypeArray(){
    
        if(this.subtypeArray==null){
        
            this.subtypeArray=new java.util.Vector<Entities.Subtype>();
        
        }
        
        return this.subtypeArray;
    
    }
    
    @Override
    public void setSubtypeArray(java.util.Vector<Entities.Subtype>subtypeArray){
    
    this.subtypeArray=subtypeArray;
    
    }
    
}
