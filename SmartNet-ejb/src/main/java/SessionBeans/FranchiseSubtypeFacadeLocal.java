/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface FranchiseSubtypeFacadeLocal {
    
    void setSubtypeArray(java.util.Vector<Entities.Subtype>subtypeArray);
    
    java.util.Vector<Entities.Subtype>getSubtypeArray();
    
}
