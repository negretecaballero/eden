/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.util.concurrent.TimeUnit;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

/**
 *
 * @author luisnegrete
 */
@Stateful
@StatefulTimeout(unit=TimeUnit.MINUTES, value=30)
public class InvoiceFacade implements InvoiceFacadeLocal {

   private java.util.List<SessionClasses.Invoice>invoices;
 
   private String flag;
   
   @Override
   public void setFlag(String flag)
   {
   
   this.flag=flag;
   
   }
   
   @Override
   public String getFlag(){
   
       return this.flag;
   
   }
  
   @SuppressWarnings("unused")
   @javax.ejb.PrePassivate
   public void prePassivate(){
   
       System.out.print("In prePassivate method");
   
   }
   
   @SuppressWarnings("unused")
   @javax.ejb.PostActivate
   public void postActivate(){
   
       System.out.print("In postActivate Method");
   
   }
   @Override
   public java.util.List<SessionClasses.Invoice>getInvoices(){
   
       System.out.print("You are in get Invoice");
       
       if(this.invoices==null){
       
            System.out.print("Creating Invoices");
           
           this.invoices=new java.util.ArrayList<SessionClasses.Invoice>();
       
          
       }
   return this.invoices;
   
   }
   
   @Override
   public void setInvoices(java.util.List<SessionClasses.Invoice>invoices){
   
   this.invoices=invoices;
   
   }
   
   @javax.ejb.Remove
   public void destroy(){
   
      //this.invoices=null; 
   
   }
   @Override
   public void test(){
   
       System.out.print("Testing Invoice Bean");
   
   }
}
