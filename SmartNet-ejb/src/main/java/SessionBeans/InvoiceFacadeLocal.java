/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface InvoiceFacadeLocal {
  
    java.util.List<SessionClasses.Invoice>getInvoices();
    
    void setInvoices(java.util.List<SessionClasses.Invoice>invoices);
    
    void test();
    
    void setFlag(String flag);
    
    String getFlag();
    
}
