/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */


@javax.ejb.Singleton


public class MailFacade implements MailFacadeLocal {
    
    
    
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("MailFacade Postactivated");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
        System.out.print("MailFacade PrePassivate");
    
    }
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void Remove(){
    
        System.out.print("MailFacade Removed");
        
    }
    
   
  @Override
  @javax.ejb.Lock(javax.ejb.LockType.WRITE)
  @javax.ejb.AccessTimeout(value=1,unit=java.util.concurrent.TimeUnit.MINUTES)
    public SessionEnum.MailConfirmation sendMessage(String destinatary,String subject,String messageText){
 
        
        try{
        
        SessionClasses.MailManagement mailManagement=new SessionClasses.MailManagement();
         
        switch(mailManagement.sendMail(destinatary, subject, messageText)){
        
            case SUCCESS:{
            
                System.out.print("Mail sent successfully");
                
                return SessionEnum.MailConfirmation.SUCCESS;
            
            }
            case ERROR :{
            
               return SessionEnum.MailConfirmation.ERROR;
            
            }
            
            default:{
            
            return SessionEnum.MailConfirmation.ERROR;
            
            }
        
        }
      
        }
        
        catch(NullPointerException  ex)
        {
            
            
         
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            ex.printStackTrace(System.out);
            
            return SessionEnum.MailConfirmation.ERROR;
        
        }
       
    
    }
    
}
