/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

/**
 *
 * @author luisnegrete
 */

public interface MailFacadeLocal {
    
     SessionEnum.MailConfirmation sendMessage(String destinatary,String subject,String messageText);
    
}
