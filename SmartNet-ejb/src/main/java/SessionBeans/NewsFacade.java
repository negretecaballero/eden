/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import javax.ejb.Stateful;

/**
 *
 * @author luisnegrete
 */
@Stateful
@javax.ejb.StatefulTimeout(unit=java.util.concurrent.TimeUnit.MINUTES,value=30)
public class NewsFacade implements NewsFacadeLocal {

    private boolean initialized;
    
    @Override
    public boolean getInitialized(){
    
        return this.initialized;
    
    }
    
    @Override
    public void setInitialized(boolean initialized){
    
    this.initialized=initialized;
    
    }
    
    @javax.ejb.PostActivate
    public void postActivate(){
    
    System.out.print("NewsFacade PostActivated");
    
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
    System.out.print("NewsFacade PrePassivated");
    
    }
    
    @javax.ejb.Remove
    public void Remove(){
    
        System.out.print("NewsFacade Removed");
    
    }
    
    
    private java.util.List<Entities.News>newsList;
    
    @Override
    public java.util.List<Entities.News>getNewsList(){
    
    if(this.newsList==null){
    
        this.newsList=new java.util.ArrayList<Entities.News>();
    
    }    
        
    return this.newsList;
    
    }
    @Override
    public void setNewsList(java.util.List<Entities.News>newsList){
    
    this.newsList=newsList;
    
    }
    
}
