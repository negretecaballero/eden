/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface NewsFacadeLocal {
    
    java.util.List<Entities.News>getNewsList();
    
        
    void setNewsList(java.util.List<Entities.News>newsList);
    
    boolean getInitialized();
    
    void setInitialized(boolean initialized);
}
