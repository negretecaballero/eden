/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import SessionClasses.Cart;
import SessionClasses.CartInterface;
import java.util.concurrent.TimeUnit;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

/**
 *
 * @author luisnegrete
 */

@Stateful
@StatefulTimeout(unit=TimeUnit.MINUTES, value=30)

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

public class OrderFacade implements OrderFacadeLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
 private CartInterface cart;
 
 
 @SuppressWarnings("unused")
 @PrePassivate
 public void prePassivate(){
 
     System.out.print("In prepassivate method");
 
 }
 
 @SuppressWarnings("unused")
 @PostActivate
 private void postActivate(){
 
     System.out.print("In PostActivate Method");
 
 }
 
 @Override
 public CartInterface getCart(){
 
     if(this.cart==null){
     
         this.cart=new Cart();
     
     }
 return this.cart;
 
 }
 

 @Override
 public void setCart(CartInterface cart){
 
     this.cart=cart;
 
 }
 

 @Override
 public void completePurchase(){
     
    
     this.cart.getCart().clear();
     
      destroy();
 
 
 }
 
 @Remove
 public void destroy(){
 
     System.out.print("Destroying OrderFacade");
 
 }

    
 
 
 
}
