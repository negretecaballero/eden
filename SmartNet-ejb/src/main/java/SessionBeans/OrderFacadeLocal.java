/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import SessionClasses.CartInterface;
import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */

@Local
public interface OrderFacadeLocal {
    
 CartInterface getCart();
 
 void setCart(CartInterface cart);
 
 void completePurchase();

  
 
}
