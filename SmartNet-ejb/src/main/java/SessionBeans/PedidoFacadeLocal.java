/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import Entities.Pedido;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface PedidoFacadeLocal {
   
    void everyMinute();
    
    void updatePedidoMap(List<Pedido>ListPedido);
    
}
