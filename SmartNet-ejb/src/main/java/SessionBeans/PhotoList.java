/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.apache.commons.io.FileUtils;


/*
 * @author luisnegrete
 */

@Stateful
@SessionScoped
@Named(value="photoList")

public class PhotoList implements PhotoListLocal {

    @Override
    public List<String> getPhotolist() {
        return photolist;
    }
    
    
    @Override
    public void addElement(String element){
    this.photolist.add(element);
    }

    @Override
    public void setPhotolist(List<String> photolist) {
        this.photolist = photolist;
    }

   private List <String> photolist;
   
    @PostConstruct
    public void Initialize(){
      
        System.out.print("PhotoList Instantited");
        
        //Create photolist
        if(photolist==null)
            
            {
                this.photolist=new ArrayList();
                
                System.out.print("PhotoList Created");
            }
        
    }
    
    @Remove
    public void removePhotoList(){
        
    System.out.print("PhotoList Removed");
    
    }
    
    @Override
    public void clearList(String folder){
    this.photolist.clear();
    
    try{
        File folders=new File(folder);
        
        if(folders.exists() && folders.isDirectory())
        {
            
        FileUtils.cleanDirectory(new File(folder));
        
        
        }
       
    }
        
        catch(IOException ex){
            
        System.out.print("IOException caught");
        
       Logger.getLogger(PhotoList.class.getName()).log(Level.SEVERE,null,ex);
       
        FacesMessage msg=new FacesMessage("The Photo Cannot Be Deleted");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    
    catch(NullPointerException ex){
        
    Logger.getLogger(PhotoList.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
 
}
