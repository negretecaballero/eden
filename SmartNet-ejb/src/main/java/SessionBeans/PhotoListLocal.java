/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface PhotoListLocal {

    public List<String> getPhotolist();

    public void setPhotolist(List<String> photolist);

   // public StreamedContent photo(String index);
    
    void addElement(String element);

    public void clearList(String folder);
    
}
