/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.ejb.Stateless;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

/**
 *
 * @author luisnegrete
 */
@Stateless
public class QrCode implements QrCodeLocal {

    @Override
    public String ImageToCode(InputStream Image, String ext) {
    
    return "";
    }

    @Override
    public void CodeToImage(String code, String name) {
     ByteArrayOutputStream out = QRCode.from(code).to(ImageType.PNG).stream();
     
     try{
     FileOutputStream fout = new FileOutputStream(new File("/Users/luisnegrete/Copy/ProyectoLuis/guanabara/web/imagesCache/",name+".jpg"));
 
            fout.write(out.toByteArray());
 
            fout.flush();
            fout.close();
     }
     
     catch(FileNotFoundException e){
     
     }
     
     catch(IOException e){}
 
    
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
