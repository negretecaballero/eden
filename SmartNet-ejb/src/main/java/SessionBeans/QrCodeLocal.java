/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.io.InputStream;
import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface QrCodeLocal {
     public String ImageToCode(InputStream Image,String ext);
    
    public void CodeToImage(String Code, String name);
    
}
