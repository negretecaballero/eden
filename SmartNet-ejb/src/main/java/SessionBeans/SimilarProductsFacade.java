/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.StatefulTimeout;

/**
 *
 * @author luisnegrete
 */
@Stateful
@StatefulTimeout(unit=TimeUnit.MINUTES,value=30)
public class SimilarProductsFacade implements SimilarProductsFacadeLocal {

    private java.util.List<Entities.SimilarProducts>productList;
    
    @Override
    public List<Entities.SimilarProducts> getProductList(){
    
    if(this.productList==null){
    
    this.productList=new <Entities.SimilarProducts>ArrayList();
    
    }
    
    return this.productList;
    
    }
    
    @Override
    public void setProductList(List<Entities.SimilarProducts>productList){
    
    this.productList=productList;
    
    }
     
     @Remove
 public void destroy(){
 
     System.out.print("Destroying Similar Products Facade");
 
 }
 
  
 @SuppressWarnings("unused")
 @PrePassivate
 public void prePassivate(){
 
     System.out.print("In prepassivate method");
 
 }
 
 @SuppressWarnings("unused")
 @PostActivate
 private void postActivate(){
 
     System.out.print("In PostActivate Method");
 
 }
    
}
