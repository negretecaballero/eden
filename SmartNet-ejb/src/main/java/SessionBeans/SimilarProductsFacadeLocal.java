/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionBeans;

import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface SimilarProductsFacadeLocal {
   
    void setProductList(List<Entities.SimilarProducts>productList);
    
    List<Entities.SimilarProducts>getProductList();
    
}
