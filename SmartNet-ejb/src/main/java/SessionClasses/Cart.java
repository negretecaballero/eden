/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class Cart implements CartInterface{
    
  private List<CartItem>cart;
  
    public Cart(){
    
        this.cart=new <CartItem>ArrayList();
    
    }
    
    @Override
    public List<CartItem> getCart(){
    
    return this.cart;
    
    }
   
    @Override
    public void setCart(List <CartItem>cart){
    
    this.cart=cart;
    
    }
    
    
}
