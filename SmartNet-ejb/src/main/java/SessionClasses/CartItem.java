/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 *
 * @author luisnegrete
 */
public class CartItem {
    
    private Item item;
    
    
    @Min(1)
    @Max(100)
    private int quantity;
    
    private String name;
    
    
    
    public void setName(String name){
    
    this.name=name;
    
    }
    
    Entities.Sucursal sucursal;
    
    public String getName(){
    
    return this.name;
    
    }
    
    public Item getItem(){
    
        return this.item;
    
    }
    
    public void setItem(Item item){
    
    this.item=item;
    
    }
    
    public int getQuantity(){
    
        
        
    return this.quantity;
    
    }
    
    public void setQuantity(int quantity){
        
    this.quantity=quantity;
    
    }
    
    public Entities.Sucursal getSucursal(){
    
        return this.sucursal;
    
    }
    
    public void setSucursal(Entities.Sucursal sucursal){
    
    this.sucursal=sucursal;
    
    }
    
}
