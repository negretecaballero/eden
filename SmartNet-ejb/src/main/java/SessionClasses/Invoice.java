/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;

/**
 *
 * @author luisnegrete
 */
public interface Invoice {
    
    java.util.List<SessionClasses.InvoiceItem>getItems();
    
    void setItems(java.util.List<SessionClasses.InvoiceItem> items);
    
    double getTotal();
    
    java.util.Date getDate();
    
    void setDate(java.util.Date date);
}
