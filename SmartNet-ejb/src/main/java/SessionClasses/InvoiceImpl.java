/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;

/**
 *
 * @author luisnegrete
 */
public class InvoiceImpl implements Invoice{
    
  private java.util.List<SessionClasses.InvoiceItem>items;
  
  private java.util.Date date;
  
  @Override
  public java.util.Date getDate(){
  
  return this.date;
  
  }
  @Override
  public void setDate(java.util.Date date){
  
      this.date=date;
  
  }
  
  public void InvoiceImpl(){
   
  
  }
  @Override
  public java.util.List<SessionClasses.InvoiceItem>getItems(){
    if(this.items==null){
      
      items=new java.util.ArrayList<SessionClasses.InvoiceItem>();
      
      }
      return this.items;
  
  }
  @Override
  public void setItems(java.util.List<SessionClasses.InvoiceItem>items){
  
      this.items=items;
  
  }
  
  @Override
  public double getTotal(){
  
  double result=0;
  
  for(SessionClasses.InvoiceItem item:this.items){
  
   result=result+(item.getQuantity()*item.getValue());
  
  }
  
  return result;
  }
  
}
