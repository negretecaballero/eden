/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;

/**
 *
 * @author luisnegrete
 */
public interface InvoiceItem {
    
     String getItem();
    
     void setItem(String item);
     
     String getDescription();
     
     void setDescription(String description);
     
     int getQuantity();
     
     void setQuantity(int quantity);
     
     double getValue();
     
     void setValue(double value);
}
