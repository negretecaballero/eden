/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionClasses;

/**
 *
 * @author luisnegrete
 */
public class InvoiceItemImpl implements InvoiceItem{
    
    private String item;
    
    private String description;
    
    private int quantity;
    
    private double value;
    
    @Override
    public String getItem(){
    
        return this.item;
    
    }
    @Override
    public void setItem(String item){
    
        this.item=item;
    
    }
    
    @Override
    public String getDescription(){
    
        return this.description;
    
    }
    
    @Override
    public void setDescription(String description){
    
        this.description=description;
    
    }
    
    @Override
    public int getQuantity(){
    
        return this.quantity;
    
    }
    
    @Override
    public void setQuantity(int quantity){
    
    this.quantity=quantity;
    
    }
    
    @Override
    public double getValue(){
    
        return this.value;
    
    }
    
    @Override
    public void setValue(double value){
    
        this.value=value;
    
    }
}
