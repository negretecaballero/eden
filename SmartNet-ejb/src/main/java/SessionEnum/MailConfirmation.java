/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SessionEnum;

/**
 *
 * @author luisnegrete
 */
public enum MailConfirmation {
 
    SUCCESS("SUCCESS"),ERROR("ERROR");
    
    private final String name;
    
    private MailConfirmation(String n){
    
        this.name = n;
    
    }
    
    public boolean equalsName(String otherName){
    
        return (otherName == null)?false:name.equals(otherName);
    
    }
    
}
