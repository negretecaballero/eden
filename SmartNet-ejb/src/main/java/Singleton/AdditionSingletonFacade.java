/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface AdditionSingletonFacade {
    
    SessionClasses.EdenList<Entities.Addition>getAdditions();
    
    void setAdditions(SessionClasses.EdenList<Entities.Addition>additions);
    
}
