/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Startup

@javax.ejb.Singleton

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)

public class AdditionSingletonFacadeImplementation implements AdditionSingletonFacade{
   
    private SessionClasses.EdenList<Entities.Addition>additions=new SessionClasses.EdenList<Entities.Addition>();
    
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("Post Activate");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
    System.out.print("Pre Passivate");
    
    }
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void remove(){
    
        System.out.print("Remove");
    
    }
    
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public SessionClasses.EdenList<Entities.Addition>getAdditions(){
    
        return additions;
    
    }
    
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    @Override
    public void setAdditions(SessionClasses.EdenList<Entities.Addition>additions){
    
        this.additions=additions;
    
    }
    
}
