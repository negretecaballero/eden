/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.REQUIRED)
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
public class AddressTypeSingletonFacade implements AddressTypeSingletonFacadeLocal{
    
     private java.util.List<String>_type;
    
    @javax.ejb.PostActivate
    public void postActivate(){
    
        System.out.print("Post Activate");
    
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
        System.out.print("Pre Passivate");
    
    }
    
    @javax.ejb.Remove
    public void Destroy(){
    
        System.out.print("Post Activate");
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.List<String>getType(){
    
        if(_type==null){
        
            _type=new java.util.ArrayList<String>();
            
            _type.add("Cl");
            
            _type.add("Cra");
            
            _type.add("Transversal");
            
            _type.add("Diagonal");
        
        }
        
        return _type;
    
    }
    
    @Override
    
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    
    public void setType(java.util.List<String>type){
    
        _type=type;
    
    }
    
}
