/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface CategorySingletonFacade {
    
    public java.util.List<Entities.Categoria>getCategories();
    
    void setCategories(java.util.List<Entities.Categoria>categories);
    
}
