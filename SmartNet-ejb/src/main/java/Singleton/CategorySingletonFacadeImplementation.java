/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
public class CategorySingletonFacadeImplementation implements CategorySingletonFacade {
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("Post Activated");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
        System.out.print("Pre Passivate");
    
    }
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void remove(){
    
        System.out.print("Removed");
    
    }
    
    
    private java.util.List<Entities.Categoria>categories=new java.util.ArrayList<Entities.Categoria>();
    
    @Override
     @Lock(LockType.READ)
    public java.util.List<Entities.Categoria>getCategories(){
    
    return this.categories;
    
    }
    
    @Override
    @Lock(LockType.WRITE)
    @AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setCategories(java.util.List<Entities.Categoria>categories){
    
        this.categories=categories;
    
    }
    
}
