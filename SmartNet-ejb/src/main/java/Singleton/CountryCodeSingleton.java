/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Local
public interface CountryCodeSingleton {
    
    java.util.HashMap<String,String>getCodes();
    
    void setCodes(java.util.HashMap<String,String>codes);
    
    void printAll();
 
}
