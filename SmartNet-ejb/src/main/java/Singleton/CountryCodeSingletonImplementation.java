/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
public class CountryCodeSingletonImplementation implements CountryCodeSingleton{
    
       private java.util.HashMap<String,String>codes;
    
       @Override
       @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.HashMap<String,String>getCodes(){
    
         
            if(codes==null || codes.isEmpty()){
            
                codes=new java.util.HashMap<String,String>();
                
                codes.put("US", "en_US");
                
                codes.put("CO", "es_CO");
            
            }
            
        
        return this.codes;
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setCodes(java.util.HashMap<String,String>codes){
            
        this.codes=codes;
            
    }
    
    @Override
    public void printAll(){
    
        if(codes!=null && !codes.isEmpty()){
        
        for(java.util.Map.Entry<String,String>entry:codes.entrySet()){
        
        System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
        
        }
    
    }
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        try{
        
            if(codes==null || codes.isEmpty()){
            
                codes=new java.util.HashMap<String,String>();
                
                codes.put("US", "en");
                
                codes.put("CO", "es_CO");
            
            }
            
            System.out.print("Country Codes Singleton PostActivated");
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
        System.out.print("Country Code Singleton prepassivated");
    
    }
    
    @javax.ejb.Remove
    public void remove(){
    
        System.out.print("Country Code Singleton Removed");
        
    }
    
    
}
