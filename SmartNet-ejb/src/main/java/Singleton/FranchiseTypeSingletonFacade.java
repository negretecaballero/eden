/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.REQUIRED)
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
public class FranchiseTypeSingletonFacade implements FranchiseTypeSingletonFacadeLocal {
    
    private java.util.Vector<Entities.Tipo>typeArray;
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.Vector<Entities.Tipo>getTypeArray(){
    
        return this.typeArray;
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setTypeArray(java.util.Vector<Entities.Tipo>typeArray){
    
        this.typeArray=typeArray;
    
    }
    
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("PostActivate");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
        System.out.print("Pre Passivate");
    
    }
    
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void remove(){
    
        System.out.print("Removed");
    
    }
    
    
    
    
}
