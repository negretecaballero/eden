/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Local
public interface FranchiseTypeSingletonFacadeLocal {
    
    java.util.Vector<Entities.Tipo>getTypeArray();
    
    void setTypeArray(java.util.Vector<Entities.Tipo>typeArray);
    
}
