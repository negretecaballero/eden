/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Startup

@javax.ejb.Singleton

@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

public class MenuSingletonFacadeImplementation implements MenuSingletonFacade {
    
    
    private java.util.List<Entities.Menu>menuList;
    
    @javax.ejb.PostActivate
    public void postActivate(){
    
    System.out.print("Post activate");
    
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
        System.out.print("PrePassivate");
    
    }
    
    @javax.ejb.Remove
    public void Remove(){
    
    System.out.print("Removed");
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.List<Entities.Menu>getMenuList(){
    
        System.out.print("RETRIEVING MENULIST");
        
        if(this.menuList==null){
        
            this.menuList=new java.util.ArrayList<Entities.Menu>();
        
        }
        
    return this.menuList;
    
    }
    
    @Override
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    public void setMenuList(java.util.List<Entities.Menu>menuList){
    
    this.menuList=menuList;
    
    }
    
}
