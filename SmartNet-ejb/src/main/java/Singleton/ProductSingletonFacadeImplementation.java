/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup

@javax.ejb.Singleton

@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

public class ProductSingletonFacadeImplementation implements ProductSingletonFacade{
    
    
    private SessionClasses.EdenList<Entities.Producto>products;
    
    @Override
    public  SessionClasses.EdenList<Entities.Producto>  getProducts(){
    
    if(products==null){
    
        products=new SessionClasses.EdenList<Entities.Producto>();
    
    }
    
    return products;
    
    }
    
    @Override
    public void setProducts(SessionClasses.EdenList<Entities.Producto>products){
    
        this.products=products;
    
    }
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void poasActivate(){
    
    System.out.print("Post Activated");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
    System.out.print("PrePAssivated");
    
    }
    
    
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void remove(){
    
        System.out.print("Removed");
        
    }
    
    
}
