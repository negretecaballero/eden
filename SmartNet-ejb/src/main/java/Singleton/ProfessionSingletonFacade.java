/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
public class ProfessionSingletonFacade implements ProfessionSingletonFacadeLocal{
    
    private  SessionClasses.EdenList<Entities.Profession>professionCollection;
   
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public SessionClasses.EdenList<Entities.Profession>getProfessionCollection(){
    
        if(this.professionCollection==null){
        
            this.professionCollection=new SessionClasses.EdenList<Entities.Profession>();
        
        }
        
        return this.professionCollection;
    
    }
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setProfessionCollection(SessionClasses.EdenList<Entities.Profession>professionCollection){
    
        this.professionCollection=professionCollection;
    
    }
    
    
    @javax.ejb.PostActivate
    @SuppressWarnings("unused")
    public void postActivate(){
    
        System.out.print("Profession Singleton Post Activated");
    
    }
    
    @javax.ejb.PrePassivate
    @SuppressWarnings("unused")
    public void prePassivate(){
    
        System.out.print("Profession Singleton prepassivated");
    
    }
    
    @javax.ejb.Remove
    @SuppressWarnings("unused")
    public void Remove(){
    
        System.out.print("Profession Singleton Removed");
    
    }
   
}
