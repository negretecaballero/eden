/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Startup
@javax.ejb.Singleton
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)
public class StateSingletonFacade implements StateSingletonFacadeLocal {
 
    private java.util.List<Entities.State>_stateList;
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.List<Entities.State>getStateList(){
    
        return _stateList;
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30, unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setStateList(java.util.List<Entities.State>stateList){
    
        _stateList=stateList;
    
    }
    
    @javax.ejb.PostActivate
    public void postActivate(){
     
        System.out.print("StateSingletonFacade PostActivate");
        
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
    System.out.print("StateSingletonFacade PrePassivate");
    
    }
    
    
    @javax.ejb.Remove
    public void Remove(){
    
        System.out.print("StateSingletonFacade Remove");
    
    }
    
}
