/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Singleton

@javax.ejb.Startup

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)

public class SupplierProductTypeSingletonFacade implements SupplierProductTypeSingletonFacadeLocal {
 
    private java.util.List<Entities.SupplierProductType>supplierProductTypeList;
    
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.List<Entities.SupplierProductType>getSupplierProductTypeList(){
    
        return this.supplierProductTypeList;
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setSupplierProductTypeList(java.util.List<Entities.SupplierProductType>supplierProductTypeList){
    
        this.supplierProductTypeList=supplierProductTypeList;
    
    }
    
    
    @javax.ejb.PostActivate
    public void postActivate(){
    
        System.out.print("SupplierProductTypeSingletonFacade PostActivate");
    
    }
    
    @javax.ejb.PrePassivate
    public void prePassivate(){
    
        System.out.print("SupplierProductTypeSingletoFacade PrePassivates");
    
    }
    
    @javax.ejb.Remove
    public void remove(){
    
        System.out.print("SupplierProductTypeSingletonFacade Remove");
    
    }
    
}
