/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Singleton;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Startup

@javax.ejb.Singleton

@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)

@javax.ejb.ConcurrencyManagement(javax.ejb.ConcurrencyManagementType.CONTAINER)

public class SupplierTypeSingletonFacade implements Singleton.SupplierTypeSingletonFacadeLocal{
    
    private java.util.List<Entities.SupplierType>supplierTypeList;
    
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.READ)
    public java.util.List<Entities.SupplierType>getSupplierTypeList(){
    
        return this.supplierTypeList;
    
    }
    
    @Override
    @javax.ejb.Lock(javax.ejb.LockType.WRITE)
    @javax.ejb.AccessTimeout(value=30,unit=java.util.concurrent.TimeUnit.SECONDS)
    public void setSupplierTypeList(java.util.List<Entities.SupplierType>supplierTypeList){
    
    this.supplierTypeList=supplierTypeList;
    
    }
    
    
    
    
    @javax.ejb.PostActivate
    
    public void postActivate(){
    
        System.out.print("SupplierTypeSingleton Post Activate");
    
    }
    
    @javax.ejb.PrePassivate
    public void prePasivate(){
    
        System.out.print("SupplierTypeSingleton Pre Pasivate");
    
    }
    
    
    @javax.ejb.Remove
    public void Remove(){
    
        System.out.print("SupplierTypeSingleton Remove");
    
    }
    
    
    
}
