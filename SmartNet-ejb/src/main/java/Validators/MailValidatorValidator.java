/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Validators;

import SessionBeans.OrderFacadeLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


/**
 *
 * @author luisnegrete
 */
public class MailValidatorValidator implements ConstraintValidator<MailValidator, String> {
    
    
    
    private Pattern pattern;
	private Matcher matcher;
 
    
   
		 private   String EMAIL_PATTERN;
    
   
    
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        
        System.out.print("Validating Mail");
        
        matcher = pattern.matcher(value);
        
       return matcher.matches();
    }

    @Override
    public void initialize(MailValidator constraintAnnotation) {
        
  
    EMAIL_PATTERN="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
     pattern=Pattern.compile(EMAIL_PATTERN);
    
    }

    private OrderFacadeLocal lookupOrderFacadeLocal() {
        try {
            Context c = new InitialContext();
            return (OrderFacadeLocal) c.lookup("java:comp/env/OrderFacade");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
