/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;

import javax.ejb.Local;

/**
 *
 * @author luisnegrete
 */
@Local
public interface UploadImageLocal {

    String upload();

    public String getExt();

   // public UploadedFile getFile();

    public byte[] getFoto();

    //public void setFile(UploadedFile file);
    
}
