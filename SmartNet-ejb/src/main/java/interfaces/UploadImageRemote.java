/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package interfaces;



/**
 *
 * @author luisnegrete
 */

public interface UploadImageRemote {
  String upload(); 
  //void setFile(UploadedFile file);
  //UploadedFile getFile();
  byte[] getFoto();
  String getExt();
}
