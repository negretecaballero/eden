/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Addition.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SucursalHasAdditionLayout {
    
    Entities.Addition getAddition();
    
    void setAddition(Entities.Addition addition);
    
    String getImage();
    
    void setImage(String image);
    
}
