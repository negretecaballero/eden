/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Addition.Classes;

/**
 *
 * @author luisnegrete
 */
public class SucursalHasAdditionLayoutImplementation implements SucursalHasAdditionLayout{
    
    private Entities.Addition addition;
    
    private String image;
    
    @Override
    public Entities.Addition getAddition(){
    
        return this.addition;
    
    }
    @Override
    public void setAddition(Entities.Addition addition){
    
        this.addition=addition;
    
    }
    @Override
    public String getImage(){
    
       return this.image;
    
    }
    @Override
    public void setImage(String image){
    
        this.image=image;
    
    }
    
}
