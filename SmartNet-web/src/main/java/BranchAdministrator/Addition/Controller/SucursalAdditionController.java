package BranchAdministrator.Addition.Controller;

import ENUM.*;

public interface SucursalAdditionController {

	/**
	 * 
	 * @param viedId
	 */
	void initPreRenderView(String viedId);

	/**
	 * 
	 * @param key
	 */
	void addToSelectedAddition(Entities.AdditionPK key);

	TransactionStatus saveChanges();

	/**
	 * 
	 * @param key
	 */
	void deleteFromSelectedList(Entities.AdditionPK key);

	/**
	 * 
	 * @param key
	 */
	void deleteFromBranch(Entities.AdditionPK key);

}