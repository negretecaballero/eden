package BranchAdministrator.Addition.Controller;

import ENUM.*;


@javax.enterprise.inject.Alternative

public class SucursalAdditionControllerImplementation implements SucursalAdditionController {

    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadBean _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AdditionControllerDelegate _additonalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SucursalControllerDelegate _branchController;
    
	/**
	 * 
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(String viewId) {
	
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                if(sucursalAddition!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                    
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                   switch(viewId){
                   
                       case "/AdminSucursal/add-addition.xhtml":{
                       
                           if(_fileUpload.getActionType()==null || _fileUpload.getActionType().getActionType().equals(viewId)){
                           
                          _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                               
                           _fileUpload.setActionType(new Clases.ActionType(viewId));
                               
                           sucursalAddition.setAdditionList(this.getSucursalHasAdditionLayoutList(viewId,_additonalController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia())));
                           
                           }
                           
                       break;
                       
                       }
                       
                       case "/AdminSucursal/list-addition.xhtml":{
                       
                           if(_fileUpload.getActionType()==null || _fileUpload.getActionType().getActionType().equals(viewId)){
                           
                          _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                               
                           _fileUpload.setActionType(new Clases.ActionType(viewId));
                               
                           sucursalAddition.setUpdateAdditionList(this.getSucursalHasAdditionLayoutList(viewId,(java.util.List<Entities.Addition>)branch.getAdditionCollection()));
                           
                           }
                           
                       break;
                       }
                       
                   
                   
                   } 
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param viewId
	 * @param additionList
	 */
	private java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout> getSucursalHasAdditionLayoutList(String viewId, java.util.List<Entities.Addition> additionList) {
		
           try{
           
               javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
               
                 java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>resultList=new java.util.ArrayList<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>();
              
               
               if(session.getAttribute("selectedOption")instanceof Entities.Sucursal)
               {
                   
                   if(viewId.equals("/AdminSucursal/add-addition.xhtml")){
                   
                   Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                   
                   if(branch.getAdditionCollection()!=null && !branch.getAdditionCollection().isEmpty()){
                   
                   for(Entities.Addition aux:branch.getAdditionCollection()){
                   
                    if(additionList.contains(aux)){
                    
                        additionList.remove(aux);
                    
                    }
                   
                   }
                   
                   }
                   
                   }
                   
                
                for(Entities.Addition addition:additionList)
                {
                
                    BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout aux=new BranchAdministrator.Addition.Classes.SucursalHasAdditionLayoutImplementation();
                    
                    aux.setAddition(addition);
                    
                   
                    if(addition.getImagenCollection()!=null && !addition.getImagenCollection().isEmpty()){
                    
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                        
                        java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+addition.getName());
                        
                        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                        
                        if(!file.exists()){
                        
                            filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator,addition.getName());
                            
                        }
                        else{
                        
                            filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username")+java.io.File.separator+addition.getName());
                        
                        }
                        
                    for(Entities.Imagen image:addition.getImagenCollection()){
                    
                      _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+addition.getName());
                    
                      String imagen=this._fileUpload.getImages().get(this._fileUpload.getImages().size()-1);
                      
                      aux.setImage(imagen);
                      
                      this._fileUpload.getImages().remove(this._fileUpload.getImages().size()-1);
                      
                      this._fileUpload.getPhotoList().remove(this._fileUpload.getPhotoList().size()-1);
                      
                    }
                    
                    }
                    else{
                    
                        aux.setImage("/images/noImage.png");
                    
                    }
                    
                resultList.add(aux);
                }
                
           }
                
                return resultList;
           
           }
           catch(NullPointerException ex){
           
           ex.printStackTrace(System.out);
           
           return null;
           
           }
            
	}

	/**
	 * 
	 * @param key
	 */
	private boolean checkExistenceSelectedAdditons(Entities.AdditionPK key) {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
                
                if(sucursalAddition!=null){
                
                    if(sucursalAddition.getSelectedAddition()!=null && !sucursalAddition.getSelectedAddition().isEmpty()){
                    
                        for(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout aux:sucursalAddition.getSelectedAddition()){
                        
                            if(aux.getAddition().getAdditionPK().equals(key)){
                            
                                return true;
                            
                            }
                        
                        }
                    
                    }
                
                }
                
                return false;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return false;
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	private BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout getAddition(Entities.AdditionPK key) {
		
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
                
                if(sucursalAddition!=null){
                
                    if(sucursalAddition.getAdditionList()!=null && !sucursalAddition.getAdditionList().isEmpty()){
                    
                        for(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout aux:sucursalAddition.getAdditionList()){
                        
                            if(aux.getAddition().getAdditionPK().equals(key)){
                            
                                return aux;
                            
                            }
                        
                        }
                    
                    }
                
                }
                
                return null;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return null;
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void addToSelectedAddition(Entities.AdditionPK key) {
		
            
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
                
                if(sucursalAddition!=null){
                
                    if(!this.checkExistenceSelectedAdditons(key)){
                    
                        if(sucursalAddition.getSelectedAddition()==null){
                        
                            sucursalAddition.setSelectedAddition(new java.util.ArrayList<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>());
                        
                        }
                        
                        sucursalAddition.getSelectedAddition().add(this.getAddition(key));
                    
                        
                        System.out.print("ADDITION ADDED TO SELECTED LIST");
                        
                    }
                    else{
                    
                           System.out.print("ADDITION NOT ADDED TO SELECTED LIST");
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
            
	}

	@Override
	public TransactionStatus saveChanges() {
	
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
                
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
               
                if(sucursalAddition!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
                    if(sucursalAddition.getSelectedAddition()!=null && !sucursalAddition.getSelectedAddition().isEmpty()){
                    
                    if(branch.getAdditionCollection()==null){
                    
                        branch.setAdditionCollection(new java.util.ArrayList<Entities.Addition>());
                    
                    }
                    
                    for(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout aux:sucursalAddition.getSelectedAddition()){
                    
                    branch.getAdditionCollection().add(aux.getAddition());
                    
                    }
                    
                    _branchController.update(branch);
                    
                     return ENUM.TransactionStatus.APPROVED;
                    
                    }
                
                }
            
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void deleteFromSelectedList(Entities.AdditionPK key) {
	
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Addition.View.SucursalAddition sucursalAddition=(BranchAdministrator.Addition.View.SucursalAddition)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalAddition");
            
                if(sucursalAddition!=null && sucursalAddition.getSelectedAddition()!=null && !sucursalAddition.getSelectedAddition().isEmpty()){
                
                    for(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout aux:sucursalAddition.getSelectedAddition()){
                    
                    if(aux.getAddition().getAdditionPK().equals(key)){
                    
                    sucursalAddition.getSelectedAddition().remove(aux);
                    
                    break;
                    
                    }
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
        }

	/**
	 * 
	 * @param key
	 */
        @Override
	public void deleteFromBranch(Entities.AdditionPK key) {
		
            try{
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
                    if(branch.getAdditionCollection()!=null && !branch.getAdditionCollection().isEmpty()){
                    
                        for(Entities.Addition addition:branch.getAdditionCollection()){
                        
                            if(addition.getAdditionPK().equals(key)){
                            
                            branch.getAdditionCollection().remove(addition);
                            
                            break;
                            
                            }
                        
                        }
                        
                        this._branchController.update(branch);
                        
                        this._fileUpload.setActionType(null);
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
	}
        
        

}