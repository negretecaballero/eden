/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Addition.View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class SucursalAddition extends Clases.BaseBacking{

    /**
     * Creates a new instance of SucursalAddition
     */
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private BranchAdministrator.Addition.Controller.SucursalAdditionController _sucursalAdditionController;
    
    public SucursalAddition() {
   
    
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
  //  @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SucursalAdditionControllerImplementation sucursalAdditionController;
    
    private java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>additionList;
    
    private java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>selectedAddition;
    
    private java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>updateAdditionList;
    
    public java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>getUpdateAdditionList(){
    
    return this.updateAdditionList;
    
    }
    
    public void setUpdateAdditionList(java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>updateAdditionList){
    
        this.updateAdditionList=updateAdditionList;
    
    }
    
    public java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>getSelectedAddition(){
    
        return this.selectedAddition;
    
    }
    
    public void setSelectedAddition(java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>selectedAddition){
    
        this.selectedAddition=selectedAddition;
        
    }
    
    public java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>getAdditionList(){
    
        return this.additionList;
    
    }
    
    public void setAdditionList(java.util.List<BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout>additionList){
    
        this.additionList=additionList;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
    
    }
   
    public void setType(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            String id=this.getContext().getViewRoot().getViewId();
            
            
            switch(id){
            
                case "/AdminSucursal/add-addition.xhtml":{
                
                    if(!this.fileUpload.getActionType().getActionType().equals(id)){
                    
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                    
                        this.fileUpload.setActionType(new Clases.ActionType(id));
                    }
                    
//                    this.additionList=this.sucursalAdditionController.getAdditionList();
                    
                    break;
                    
                }
                
                case "/AdminSucursal/list-addition.xhtml":{
                
                    if(!this.fileUpload.getActionType().getActionType().equals(id))
                    {
                    
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                    
                        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                        
                        this.fileUpload.setActionType(new Clases.ActionType(id));
                        
//                        this.updateAdditionList=this.sucursalAdditionController.getAdditionSucursalList();
                    }
                    break;
                
                }
                
            }
        
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(SucursalAddition.class.getName()).log(Level.SEVERE,null,ex);
        
          
            
        }
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            _sucursalAdditionController.initPreRenderView(super.getCurrentView());
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
 
    public void addToSelectedAddition(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout sucursalHasAddition){

        try{
        
            this._sucursalAdditionController.addToSelectedAddition(sucursalHasAddition.getAddition().getAdditionPK());
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
   
        
    }
    
    public void removeSelectedAddition(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout item){
    
        try{
        
            this._sucursalAdditionController.deleteFromSelectedList(item.getAddition().getAdditionPK());
        
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            
          
        
        }
    
    }
    
    public String saveChanges(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(super.getContext(), "bundle");
            
            
            switch(this._sucursalAdditionController.saveChanges()){
            
                case APPROVED:{
                
                return "success";
                
                }
                
                case DISAPPROVED:{
                    
                    javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddAddtion.Error"));
                    
                    msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                    
                    super.getContext().addMessage(null, msg);
                    
                
                return "failure";
                
                }
                
                default:{
                
                      javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddAddtion.Error"));
                    
                    msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                    
                    super.getContext().addMessage(null, msg);
                    
                return "failure";
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        return "failure";
        }
    
    }
    
    
    public void deteleUpdateAdditionList(BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout item){
    
        try{
        
           System.out.print("REMOVING ELEMENT");
           
           this._sucursalAdditionController.deleteFromBranch(item.getAddition().getAdditionPK());
            
        }
        catch(NullPointerException ex){
       
            ex.printStackTrace(System.out);
        
        }
    
    }
}
