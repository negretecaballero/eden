/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Controller;

/**
 *
 * @author luisnegrete
 */
public interface WelcomeSucursalAdminController {
    
    org.primefaces.model.TreeNode getTree();
    
    boolean getCompletion();
    
    double completeCoefficient();
    
    void nodeSelect(String nodeValue);
    
    boolean feasability(Entities.SucursalPK sucursalPK);
    
    org.primefaces.model.map.MapModel getModel();
    
    org.primefaces.model.map.LatLng getCentre();
    
     int zoom();
     
     void initPreRenderView(String viewId);
     
     void redirectBranch(String branchName);
    
}
