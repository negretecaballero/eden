/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Controller;

import javax.el.MethodExpression;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class WelcomeSucursalAdminControllerImplementation implements WelcomeSucursalAdminController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.PedidoControllerInterface pedidoController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SucursalControllerDelegate _sucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @Override
    public org.primefaces.model.TreeNode getTree(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("username")!=null){
        
        if(_loginAdministradorController.find(session.getAttribute("username").toString())!=null){
        
            Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
            
         /*   if(loginAdministrador.getSucursalhasloginAdministradorCollection()!=null && !loginAdministrador.getSucursalhasloginAdministradorCollection().isEmpty()){
            
                org.primefaces.model.TreeNode root=new org.primefaces.model.DefaultTreeNode("root");
                
                java.util.ResourceBundle rb=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                org.primefaces.model.TreeNode node=new org.primefaces.model.DefaultTreeNode(rb.getString("sucursalAdministratorIndex.TreeTitle"),root);
                
                for(Entities.SucursalhasloginAdministrador sucursalHasLoginAdministrador:loginAdministrador.getSucursalhasloginAdministradorCollection()){
                
                if(sucursalHasLoginAdministrador.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador")){
                
                    Entities.Sucursal sucursal=sucursalHasLoginAdministrador.getSucursal();
                    
                    org.primefaces.model.TreeNode aux=new org.primefaces.model.DefaultTreeNode(sucursal.getFranquicia().getNombre()+" "+sucursal.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+sucursal.getDireccion().getPrimerDigito()+" #"+sucursal.getDireccion().getSegundoDigito()+"-"+sucursal.getDireccion().getTercerDigito()+" "+sucursal.getDireccion().getAdicional(),node);
                
                }
                
                }
            
                
                  return root;
            }*/
      
        }
        
        }
        
    return null;
    }
    catch(Exception | StackOverflowError ex){
    
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    return null;
    
    }
    
    }
    
    
    @Override
    public void initPreRenderView(String viewId){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            switch(viewId){
            
                case "/AdminSucursal/welcomeSucursalAdministrador.xhtml":{
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                    
                      if(_fileUploadBean.getActionType()==null || !_fileUploadBean.getActionType().getActionType().equals(viewId)){

                        _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+session.getAttribute("username").toString());
                        
                        _fileUploadBean.setActionType(new Clases.ActionType(viewId));
                    
                    }
                    
                    if(session.getAttribute("username")!=null)
                    {
                    
                      javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                      
                      if(viewRoot.findComponent("form:panelSubmenu")instanceof org.primefaces.component.submenu.UISubmenu){
                      
                          org.primefaces.component.submenu.UISubmenu submenu=(org.primefaces.component.submenu.UISubmenu)viewRoot.findComponent("form:panelSubmenu");
                          
                         if(this.branchOfficeList()!=null && !this.branchOfficeList().isEmpty())
                         {
                             
                             for(String aux:this.branchOfficeList()){
                             
                             org.primefaces.component.menuitem.UIMenuItem menuItem=new org.primefaces.component.menuitem.UIMenuItem();
                             
                             menuItem.setValue(aux);
                             
                             MethodExpression methodExpression = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), "#{welcomeSucursalAdmin.actionListener}", null, new Class[] { ActionEvent.class });
                                
                             menuItem.addActionListener(new MethodExpressionActionListener(methodExpression));

                             
                             submenu.getChildren().add(menuItem);
                             
                             }
                          
                         }
                          
                      
                      }
                    
                    }
                    
                  
                
                break;
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public boolean getCompletion(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null){
            
                Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
                
                if(_loginAdministradorController.accountCompletion(session.getAttribute("username").toString())>=.7){
                
                    return true;
                    
                }
            
            }
            
        return false;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public double completeCoefficient(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("username")!=null){
        
        if(_loginAdministradorController.find(session.getAttribute("username").toString())!=null){
        
            return _loginAdministradorController.accountCompletion(session.getAttribute("username").toString());
        
        }
        
        }
        
    return .0;
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return .0;
    
    }
    
    }
    
    @Override
    public void nodeSelect(String nodeValue){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null){
            
             Entities.LoginAdministrador loginAdministrador=(Entities.LoginAdministrador)_loginAdministradorController.find(session.getAttribute("username").toString());
             
            /* for(Entities.SucursalhasloginAdministrador sucursalHasLoginAdministrador:loginAdministrador.getSucursalhasloginAdministradorCollection()){
             
            if(sucursalHasLoginAdministrador.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador")){
                
                Entities.Sucursal sucursal=sucursalHasLoginAdministrador.getSucursal();
                
                if((sucursal.getFranquicia().getNombre()+" "+sucursal.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+sucursal.getDireccion().getPrimerDigito()+" #"+sucursal.getDireccion().getSegundoDigito()+"-"+sucursal.getDireccion().getTercerDigito()+" "+sucursal.getDireccion().getAdicional()).equals(nodeValue)){
                
                //Sucursal Found
                
                      session.setAttribute("selectedOption", sucursal);
                      
                       NavigationHandler navigationHandler=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            
                       navigationHandler.handleNavigation(javax.faces.context.FacesContext.getCurrentInstance(), null, "selectedSucursal");
                    
                }
                       
                }
             
             
             }*/
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
        @Override
        public boolean feasability(Entities.SucursalPK sucursalPK){
    
        
        System.out.print("Average Per sucursal"+this.pedidoController.findBySucursal(sucursalPK).size());
        
        System.out.print("Average per sucursal Franchise "+this._franchiseController.getAveragePerSucursal(sucursalPK.getFranquiciaIdfranquicia()));
        
        if((this.pedidoController.findBySucursal(sucursalPK).size()>=this._franchiseController.getAveragePerSucursal(sucursalPK.getFranquiciaIdfranquicia()))&& this.pedidoController.findBySucursal(sucursalPK).size()>0 && this._franchiseController.getAveragePerSucursal(sucursalPK.getFranquiciaIdfranquicia())>0){
        
            System.out.print("Returning true");
            
            return true;
        
            
        }
        
        else{
        
        System.out.print("Returning false");
            
        return false;
        
        }
    
    }
        
        private SessionClasses.EdenList<Entities.Sucursal>loginAdministradorSucursales(){
        
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null){
            
                if(_loginAdministradorController.find(session.getAttribute("username").toString())!=null){
                
                Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
                
                /*if(loginAdministrador.getSucursalhasloginAdministradorCollection()!=null && !loginAdministrador.getSucursalhasloginAdministradorCollection().isEmpty()){
                
                SessionClasses.EdenList<Entities.Sucursal>list=new SessionClasses.EdenList<Entities.Sucursal>();
                    
                for(Entities.SucursalhasloginAdministrador sucursalHasLoginAdministrador:loginAdministrador.getSucursalhasloginAdministradorCollection()){
                
                       if(sucursalHasLoginAdministrador.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador")){
                       
                       list.addItem(sucursalHasLoginAdministrador.getSucursal());
                       
                       }
                    
                
                }
                
                return list;
                
                }*/
                
                }
            
            }
        
            return null;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
        
        }
        
        @Override
        public org.primefaces.model.map.LatLng getCentre(){
        
            try{
            
                org.primefaces.model.map.LatLng centre=null;
                
                for(Entities.Sucursal sucursal:this.loginAdministradorSucursales()){
                
                    centre=this._sucursalController.getMarker(sucursal.getSucursalPK()).getLatlng();
                
                }
                
            return centre;
            }
            catch(Exception | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
        
        }
        
        @Override
        public org.primefaces.model.map.MapModel getModel(){
        
        try{
        
              org.primefaces.model.map.MapModel model=new org.primefaces.model.map.DefaultMapModel();
          
          System.out.print("Test Valuue "+Clases.GPSEdenOperations.zoom(800));
            
          
          org.primefaces.model.map.LatLng center;
          
          for(int i=0;i<this.loginAdministradorSucursales().size();i++){
          

          org.primefaces.model.map.Circle circle=new org.primefaces.model.map.Circle(this._sucursalController.getMarker(this.loginAdministradorSucursales().get(i).getSucursalPK()).getLatlng(), (this.distance()/1000)*100);
             
          if(this.feasability(this.loginAdministradorSucursales().get(i).getSucursalPK())){
          
           circle.setStrokeColor("#33FF33");
          circle.setFillColor("#33FF33");
          
          }  else{
          circle.setStrokeColor("#d93c3c");
          circle.setFillColor("#d93c3c");
                 }
          circle.setFillOpacity(0.5);
          
          System.out.print("Id Sucursal "+this.loginAdministradorSucursales().get(i).getSucursalPK().getIdsucursal());
              
          double average=this.pedidoController.averagePerBranch(this.loginAdministradorSucursales().get(i).getSucursalPK());
          
          model.addOverlay(this._sucursalController.getMarker(this.loginAdministradorSucursales().get(i).getSucursalPK()));
          
          model.addOverlay(circle);
          
          
          
          center=this._sucursalController.getMarker(this.loginAdministradorSucursales().get(i).getSucursalPK()).getLatlng();
      
          }

        
         return model;
         
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        }
        
        
        }
        
        
        private double distance(){
        
            try{
            
                double distance=0;
                  int lastIndex=this.loginAdministradorSucursales().size()-1;
          
          
          for(int i=0;i<this.loginAdministradorSucursales().size();i++){
          
                  
          if((i+1)<=lastIndex)
          {
          
              Entities.Direccion address1=this.loginAdministradorSucursales().get(i).getDireccion();
          
              Entities.Direccion address2=this.loginAdministradorSucursales().get(i+1).getDireccion();
              
              if(Clases.GPSEdenOperations.distance(address1.getGpsCoordinatesidgpsCoordinated().getLatitude(), address2.getGpsCoordinatesidgpsCoordinated().getLatitude(), address1.getGpsCoordinatesidgpsCoordinated().getLongitude(), address2.getGpsCoordinatesidgpsCoordinated().getLongitude())>distance){
              
                  distance=Clases.GPSEdenOperations.distance(address1.getGpsCoordinatesidgpsCoordinated().getLatitude(), address2.getGpsCoordinatesidgpsCoordinated().getLatitude(), address1.getGpsCoordinatesidgpsCoordinated().getLongitude(),address2.getGpsCoordinatesidgpsCoordinated().getLongitude());
                  
              }
          }
          
          }
          
                    
                
                
                
            
            
                return distance;
                
            }
            catch(Exception | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return .0;
                
            }
        
        }
        
        @Override
        public int zoom(){
        
        try{
        
            double distance=0;
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("username")!=null){
            
                if(_loginAdministradorController.find(session.getAttribute("username").toString())!=null){
                
                Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
                
              /*  if(loginAdministrador.getSucursalhasloginAdministradorCollection()!=null && !loginAdministrador.getSucursalhasloginAdministradorCollection().isEmpty()){
                
                    SessionClasses.EdenList<Entities.Sucursal>list=new SessionClasses.EdenList<Entities.Sucursal>();
                    
                    for(Entities.SucursalhasloginAdministrador sucursalHasLoginAdministrador:loginAdministrador.getSucursalhasloginAdministradorCollection())
                    {
                   
                        if(sucursalHasLoginAdministrador.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador")){
                        
                        list.addItem(sucursalHasLoginAdministrador.getSucursal());
                        
                        }
                    
                    }
                    
                   int lastIndex=list.size()-1;
          
          
          for(int i=0;i<list.size();i++){
          
                  
          if((i+1)<=lastIndex)
          {
          
              Entities.Direccion address1=list.get(i).getDireccion();
          
              Entities.Direccion address2=list.get(i+1).getDireccion();
              
              if(Clases.GPSEdenOperations.distance(address1.getGpsCoordinatesidgpsCoordinated().getLatitude(), address2.getGpsCoordinatesidgpsCoordinated().getLatitude(), address1.getGpsCoordinatesidgpsCoordinated().getLongitude(), address2.getGpsCoordinatesidgpsCoordinated().getLongitude())>distance){
              
                  distance=Clases.GPSEdenOperations.distance(address1.getGpsCoordinatesidgpsCoordinated().getLatitude(), address2.getGpsCoordinatesidgpsCoordinated().getLatitude(), address1.getGpsCoordinatesidgpsCoordinated().getLongitude(),address2.getGpsCoordinatesidgpsCoordinated().getLongitude());
                  
              }
          }
          
          }
          
                    
                }*/
                
                }
            
            }
            
             distance=(distance/1000);
            
            return Clases.GPSEdenOperations.zoom(distance);
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return 0;
            
        }
        
        }
        
        
        private final SessionClasses.EdenList<String>branchOfficeList(){
        
            try{
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(session.getAttribute("username")!=null){
                
                    if(this._franchiseController.findByUsername(session.getAttribute("username").toString())!=null && !this._franchiseController.findByUsername(session.getAttribute("username").toString()).isEmpty()){
                    
                    SessionClasses.EdenList<String>results=new SessionClasses.EdenList<String>();
                    
                    for(Entities.Franquicia franchise:this._franchiseController.findByUsername(session.getAttribute("username").toString())){
                    
                    if(franchise.getSucursalCollection()!=null && !franchise.getSucursalCollection().isEmpty()){
                    
                    for(Entities.Sucursal branch:franchise.getSucursalCollection()){
                    
                        results.addItem(franchise.getNombre()+" "+branch.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+branch.getDireccion().getPrimerDigito()+" #"+branch.getDireccion().getSegundoDigito()+"-"+branch.getDireccion().getTercerDigito()+" "+branch.getDireccion().getAdicional());
                    
                    }
                    
                    }
                    
                    }
                    
                    return results;
                    
                    }
                
                }
                
                return null;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return null;
            
            }
        
        }
        
        @Override
        public void redirectBranch(String branchName){
        
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null){
            
                if(this._franchiseController.findByUsername(session.getAttribute("username").toString())!=null && !this._franchiseController.findByUsername(session.getAttribute("username").toString()).isEmpty())
                {
                
                    for(Entities.Franquicia franchise:this._franchiseController.findByUsername(session.getAttribute("username").toString()))
                    {
                    
                        if(franchise.getSucursalCollection()!=null && !franchise.getSucursalCollection().isEmpty()){
                        
                            for(Entities.Sucursal branch:franchise.getSucursalCollection()){
                            
                            if(branchName.equals(franchise.getNombre()+" "+branch.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+branch.getDireccion().getPrimerDigito()+" #"+branch.getDireccion().getSegundoDigito()+"-"+branch.getDireccion().getTercerDigito()+" "+branch.getDireccion().getAdicional()))
                            {
                            
                                session.setAttribute("selectedOption", branch);
                            
                                
                                
                            }
                            
                            }
                        
                        
                        }
                    
                    }
                }
                
            }
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
        }
    
}
