/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Home.Controller;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class WelcomePrimefacesController implements WelcomePrimefacesControllerInterface {
    
    private @javax.inject.Inject @javax.enterprise.inject.Default  CDIBeans.PedidoControllerInterface pedidoController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.PedidoStateControllerInterface pedidoStateController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default Controllers.InvoiceControllerInterface invoiceController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.FranchiseControllerInterface franchiseController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default  CDIBeans.FileUploadInterface _fileUpload;


   public WelcomePrimefacesController(){
   
   
   
   }
    
   @Override
   public int getDayInvoices(java.util.Date date){
   
       try{
       
           return this.invoiceController.getByDate(date).size();
       
       }
       catch(Exception ex){
       
           Logger.getLogger(WelcomePrimefacesController.class.getName()).log(Level.SEVERE,null,ex);
       
           throw new javax.faces.FacesException(ex);
           
       }
   
   }
   
   
   @Override
   public void initPreRenderView(String viewId){
   
       try{
       
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           
           switch(viewId){
           
           case"/AdminSucursal/welcomePrimefaces.xhtml":{
       
                if(!_fileUpload.getActionType().getActionType().equals(viewId) && session.getAttribute("username").toString()!=null){
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+session.getAttribute("username").toString());
                    
                }
            
           }
           
           }
       
       }
       catch(NullPointerException ex){
       
       ex.printStackTrace(System.out);
       
       }
   
   }
   
 
   @Override
   public String StatusImage(Entities.SucursalPK sucursalPK){
   
       try{
       String image="";
           if(this.pedidoController.dayAverage(sucursalPK)>this.dayOrders(sucursalPK)){
           
              System.out.print(this.pedidoController.dayAverage(sucursalPK)+" greater than "+this.dayOrders(sucursalPK)); 
               
             image=java.io.File.separator+"images"+java.io.File.separator+"negative.png";  
           
           }
           
           else{
           
               image=java.io.File.separator+"images"+java.io.File.separator+"positive.png";
           
           }
       return image;
       
       }
       catch(Exception ex){
       
           Logger.getLogger(WelcomePrimefacesController.class.getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
   
   }
   
   
   @Override
   public int dayOrders(Entities.SucursalPK sucursalPK){
   
       try{
           
          java.util.Date date=Calendar.getInstance().getTime();
          
          return this.pedidoController.findByDate(date, sucursalPK).size();
       
       }
       catch(Exception ex){
       
           Logger.getLogger(WelcomePrimefacesController.class.getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
   
   }
   
    @Override
    public void loadInvoices(){
    
        try{
            
            this.invoiceController.setFlag("Welcome Primefaces");
            
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
               
          if(session.getAttribute("selectedOption") instanceof Entities.Sucursal){
          
              Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
              
              double a=this.pedidoController.dayAverage(sucursal.getSucursalPK());
              
              System.out.print("Average orders per day "+a);
              
              Entities.PedidoState pedidoState=this.pedidoStateController.findByName("Delivered");
              
              if(pedidoState==null){
              
                  throw new RuntimeException();
              
              }
              
              java.util.List<Entities.Pedido>orders=this.pedidoController.findByState(pedidoState, sucursal.getSucursalPK());
              
              System.out.print("Orders to process "+orders.size());
              
              this.invoiceController.test();
              
            this.invoiceController.reloadInvoices(orders);
              
             
            System.out.print("Invoices at this sucursal "+this.invoiceController.getInvoices().size());
             
          }
          else{
          
              throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
              
          }
          
        }
        catch(Exception ex){
        
            Logger.getLogger(WelcomePrimefacesController.class.getName()).log(Level.SEVERE,null,ex);
        
         
        }
    
    }
    
    
    @Override
    public java.util.List<SessionClasses.Invoice>getByDate(java.util.Date date){
    
    try{
    
    
        
    return    this.invoiceController.getByDate(date);
    
    }
    catch(Exception ex)
    {
    Logger.getLogger(WelcomePrimefacesController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    }
    
}
