/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Home.Controller;

/**
 *
 * @author luisnegrete
 */
public interface WelcomePrimefacesControllerInterface {
    
    void loadInvoices();
    
    java.util.List<SessionClasses.Invoice>getByDate(java.util.Date date);
    
    int dayOrders(Entities.SucursalPK sucursalPK);
    
    int getDayInvoices(java.util.Date date);
    
    String StatusImage(Entities.SucursalPK sucursalPK);
    
    void initPreRenderView(String viewId);
    
}
