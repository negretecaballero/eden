/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Home.View;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class WelcomePrimefacesView extends Clases.BaseBacking{

    /**
     * Creates a new instance of WelcomePrimefacesView
    
    */ 
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SucursalControllerDelegate sucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.LoginAdministradorControllerInterface loginAdministrador;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private BranchAdministrator.Home.Controller.WelcomePrimefacesControllerInterface welcomePrimefacesController;
    
   private java.util.Date maxDate;
    
    public java.util.Date getMaxDate(){
    
    return this.maxDate;
    
    }
    
    public void setMaxDate(java.util.Date maxDate){
    
        this.maxDate=maxDate;
    
    }
    
    
    private java.util.Date invoiceDate;
    
    private double completion;
    
    private String profileImage;
    
    private int dayOrders;
    
    private int dayInvoices;
    
    
    public int getDayInvoices(){
    
       return this.dayInvoices; 
    
    }
    
    public void setDayInvoices(int dayInvoices){
    
        this.dayInvoices=dayInvoices;
    
    }
    
    
    
    public int getDayOrders(){
    
        return this.dayOrders;
    
    }
    
    public void setDayOrders(int dayOrders){
    
        this.dayOrders=dayOrders;
    
    }
    
    public java.util.Date getInvoiceDate(){
    
    return this.invoiceDate;
    
    }
    
    public void setInvoiceDate(java.util.Date invoiceDate){
    
    this.invoiceDate=invoiceDate;
    
    }
    
    public String getProfileImage(){
    
    return this.profileImage;
    
    }
    
    public void setProfileImage(String profileImage){
    
    this.profileImage=profileImage;
    
    }
    
    public double getCompletion(){
    
    return this.completion;
    
    }
    
    public void setCompletion(double completion){
    
    
    this.completion=completion;
    
    }
    
    public WelcomePrimefacesView() {
    }
    
    private java.util.List<String>photoList;
    
    
    public java.util.List<String>getPhotoList(){
    
    return this.photoList;
    
    }
    
    public void setPhotoList(java.util.List<String> photoList)
    {
    
        this.photoList=photoList;
    
    }
    
    private java.util.Date from;
    
    private java.util.Date to;
    
    public java.util.Date getFrom(){
    
        return this.from;
    
    }
    
    public void setFrom(java.util.Date from){
    
        this.from=from;
    
    }
    
    public java.util.Date getTo(){
    
        return this.to;
    
    }
    
    public void setTo(java.util.Date to){
    
    this.to=to;
    
    }
    
    private String orderStatusImage;
    
    public String getOrderStatusImage(){
    
    return this.orderStatusImage;
    
    }
    
    public void setOrderStatusImage(String orderStatusImage){
    
        this.orderStatusImage=orderStatusImage;
    
    }
    
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
    try{
    
        this.welcomePrimefacesController.initPreRenderView(super.getCurrentView());
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIInput fromInput=(javax.faces.component.UIInput)event.getComponent().findComponent("form:from");
            javax.faces.component.UIInput toInput=(javax.faces.component.UIInput)event.getComponent().findComponent("form:to");
            
            if(fromInput.getLocalValue()!=null && toInput.getLocalValue()!=null){
            
            System.out.print("From Input is null");
            
            
            
            System.out.print("From Date "+fromInput.getLocalValue().toString());
            
            
            System.out.print("To Date "+toInput.getLocalValue().toString());
            
            java.util.Date fromDate=(java.util.Date)fromInput.getLocalValue();
            
            System.out.print("From DATE "+fromDate);
            
            java.util.Date toDate=(java.util.Date)toInput.getLocalValue();
            
            System.out.print("FROM DATE "+toDate);
            
            if(fromDate.after(toDate)){
            
                System.out.print("Rendering Response");
                
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage("From date cannot be after To date");
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
                FacesContext.getCurrentInstance().addMessage(null, message);
                
                FacesContext.getCurrentInstance().renderResponse();
            
                org.primefaces.context.RequestContext.getCurrentInstance().update("form:messages");
                
            }
            }
        }
        catch(Exception ex){
        
            Logger.getLogger(WelcomePrimefacesView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
    try{
  
         
         maxDate=Calendar.getInstance().getTime();
       
      
         
         
         this.dayInvoices=this.welcomePrimefacesController.getDayInvoices(maxDate);
        
         
            welcomePrimefacesController.loadInvoices();
         
        if(!this.fileUpload.getClassType().equals(WelcomePrimefacesView.class.getName())){
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
            
        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username"));
            
        this.fileUpload.setClassType(WelcomePrimefacesView.class.getName());
        
        }
        
        
        this.completion=this.loginAdministrador.accountCompletion(getSession().getAttribute("username").toString());
        
        if(getSession().getAttribute("selectedOption")instanceof Entities.Sucursal){
            
            
            
             this.sucursalController.loadImages((Entities.Sucursal)getSession().getAttribute("selectedOption"), getSession().getAttribute("username").toString());
             
             this.welcomePrimefacesController.dayOrders(((Entities.Sucursal)getSession().getAttribute("selectedOption")).getSucursalPK());
             
             this.orderStatusImage=this.welcomePrimefacesController.StatusImage(((Entities.Sucursal)getSession().getAttribute("selectedOption")).getSucursalPK());
             
             photoList=this.fileUpload.getImages();
             
              this.profileImage=this.loginAdministrador.getImageRoot(getSession().getAttribute("username").toString());
       
             if(this.photoList.isEmpty()){
             
            System.out.print("Size is 0");
                 
            this.photoList.add("/images/noImage.png");
                 javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("It is a good practise to add images to your Sucursales");
                 
                 msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
             
                 getContext().addMessage(null, msg);
                 
                org.primefaces.context.RequestContext.getCurrentInstance().update("form:messages");
          
             }
             
             
           System.out.print("Images Loaded");
               
              
       
        }
        
        else{
        
            throw new IllegalArgumentException();
        
        }
    
    }
    catch(IllegalArgumentException ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    public void updateDate(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
            
            System.out.print(this.invoiceDate);
        
            
            System.out.print("List Size "+this.welcomePrimefacesController.getByDate(invoiceDate).size());

            
            javax.faces.application.NavigationHandler navigationHandler=FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
      
            navigationHandler.handleNavigation(FacesContext.getCurrentInstance(), profileImage, "invoicesList");
            
        }
        catch(Exception ex){
        
            Logger.getLogger(WelcomePrimefacesView.class.getName()).log(Level.SEVERE,null,ex);
            
        }
    
    }
    
    public String showOrders(){
    
        try{
        
            System.out.print("Show Orders");
            
            System.out.print("From Date "+this.from);
            
            System.out.print("To Date "+this.to);
            
            
            if(this.from==null || this.to==null){
                           
                
                return "failure";
            
            }
            
            
            
            return "showOrders";
        
        }
        catch(Exception ex){
        
        Logger.getLogger(WelcomePrimefacesView.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
    
        try
        {
        
            String id=this.getContext().getViewRoot().getViewId();
            
            if(this.fileUpload.getActionType()==null || !this.fileUpload.getActionType().getActionType().equals(id)){
            
              
                this.fileUpload.setActionType(new Clases.ActionType(id));
            
            }
            
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            
           
            
        }
    
    }
    
}
