/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Inventory.Controller;

/**
 *
 * @author luisnegrete
 */
public interface InventoryController {
    
    void initPreRenderView(String viewId);
    
    void addToInventoryHasSucursalList();
    
    void deleteAddInventoryList(Entities.InventarioHasSucursalPK key);
    
    ENUM.TransactionStatus saveChanges();
    
    ENUM.TransactionStatus updateInventories();
    
     ENUM.TransactionStatus deleteInventory(Entities.InventarioHasSucursalPK key);
    
}
