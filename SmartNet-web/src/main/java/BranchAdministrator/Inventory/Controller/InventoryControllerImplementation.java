/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Inventory.Controller;

import ENUM.TransactionStatus;
import FrontEndModel.LazySucursalInventarioDataModel;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class InventoryControllerImplementation implements InventoryController {
    
    private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.FileUploadInterface fileUpload;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default  CDIBeans.InventarioDelegate _inventoryController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.SucursalControllerDelegate _branchController;
    
    private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.InventarioHasSucursal _inventarioHasSucursalController;
    
    @Override
    public void initPreRenderView(String viewId){
    
        try{
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                    
            BranchAdministrator.Inventory.View.Inventory inventory=(BranchAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "inventory");
              
            
            switch(viewId){
            
                case "/AdminSucursal/add-inventory.xhtml":{
                    
     
                  if(this.fileUpload.getActionType()==null || !this.fileUpload.getActionType().getActionType().equals(viewId)){
    
                      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
                       this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
    
                       fileUpload.setActionType(new Clases.ActionType(viewId));
                       
                      if(inventory!=null){
                  
                  inventory.setInventarioDataModel(new LazySucursalInventarioDataModel(IteratorUtils.toList(this.getInventoryList().iterator())));
                  
                  inventory.setAddInventoryList(new java.util.ArrayList<Entities.InventarioHasSucursal>());
                  
                  }
                      
                  }
                  
                  
                    
                break;
                
                }
                
                case "/AdminSucursal/list-inventory.xhtml":{
                
                    if(this.fileUpload.getActionType()==null || !this.fileUpload.getActionType().getActionType().equals(viewId)){
    
                      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
                      
                      if(this.getUpdateInventoryList()!=null && !this.getUpdateInventoryList().isEmpty()){
                      
                      inventory.setLazyModel(new LazySucursalInventarioDataModel(IteratorUtils.toList(this.getUpdateInventoryList().iterator())));
                      
                      }
                      
                      else{
                      
                          inventory.setLazyModel(new LazySucursalInventarioDataModel(new java.util.ArrayList<Entities.InventarioHasSucursal>()));
                      
                      }
                      
                       this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
    
                       fileUpload.setActionType(new Clases.ActionType(viewId));

                  }  
                
                break;
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    public SessionClasses.EdenList<Entities.InventarioHasSucursal> getUpdateInventoryList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                if(this._inventarioHasSucursalController.findBySucursal(branch.getSucursalPK())!=null && !_inventarioHasSucursalController.findBySucursal(branch.getSucursalPK()).isEmpty()){
                
                    System.out.print("INVENTORY CONTROLLER PRE RENDER VIEW BRANCH COLLECTION SIZE "+_branchController.find(branch.getSucursalPK()).getInventarioHasSucursalCollection().size());
                    
                    SessionClasses.EdenList<Entities.InventarioHasSucursal>results=new SessionClasses.EdenList<Entities.InventarioHasSucursal>();
                    
                    for(Entities.InventarioHasSucursal inventarioHasSucursal:this._inventarioHasSucursalController.findBySucursal(branch.getSucursalPK())){
                    
                        results.addItem(inventarioHasSucursal);
                    
                    }
                
                    return results;
                    
                }
                
            
            }
            
            return null;
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    private final boolean checkBranchInventoryList(Entities.InventarioHasSucursal inv){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                if(branch.getInventarioHasSucursalCollection()!=null && !branch.getInventarioHasSucursalCollection().isEmpty()){
                
                    for(Entities.InventarioHasSucursal aux:branch.getInventarioHasSucursalCollection()){
                    
                        if(aux.getInventarioHasSucursalPK().equals(inv.getInventarioHasSucursalPK())){
                        
                            return true;
                        
                        }
                    
                    }
                
                }
            
            }
            
        return false;
        
        }
        catch(NullPointerException ex){
        
           ex.printStackTrace(System.out);
           
           return false;
        
        }
    
    }
    
    private final SessionClasses.EdenList<Entities.InventarioHasSucursal>getInventoryList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                if(_inventoryController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia())!=null && !_inventoryController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia()).isEmpty())
                {
                
                SessionClasses.EdenList<Entities.InventarioHasSucursal>results=new SessionClasses.EdenList<Entities.InventarioHasSucursal>();
                
                for(Entities.Inventario inv:_inventoryController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia())){
                
                    Entities.InventarioHasSucursal inventarioHasSucursal=new Entities.InventarioHasSucursal();
                    
                    inventarioHasSucursal.setInventarioHasSucursalPK(new Entities.InventarioHasSucursalPK());
                    
                    inventarioHasSucursal.getInventarioHasSucursalPK().setInventarioFranquiciaIdfranquicia(inv.getInventarioPK().getFranquiciaIdFranquicia());
                    
                    inventarioHasSucursal.getInventarioHasSucursalPK().setInventarioIdinventario(inv.getInventarioPK().getIdinventario());
                    
                    inventarioHasSucursal.getInventarioHasSucursalPK().setSucursalFranquiciaIdfranquicia(inv.getInventarioPK().getFranquiciaIdFranquicia());
                    
                    inventarioHasSucursal.getInventarioHasSucursalPK().setSucursalIdsucursal(branch.getSucursalPK().getIdsucursal());
                    
                    inventarioHasSucursal.setInventario(inv);
                    
                     inventarioHasSucursal.setQuantity(0.0);
                    
                    inventarioHasSucursal.setSucursal(branch);
                    
                    if(!this.checkBranchInventoryList(inventarioHasSucursal)){
                    
                results.addItem(inventarioHasSucursal);
                   
                    }
                    
                }
                return results;
                }
                
                
            }
            
        return null;
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    private final boolean checkExistenceInList(Entities.InventarioHasSucursalPK key,java.util.List<Entities.InventarioHasSucursal>list){
    
        try{
        
            System.out.print("SELECTED ITEM PK "+key);
            
            if(list!=null && !list.isEmpty()){
            
                for(Entities.InventarioHasSucursal inventarioHasSucursal:list){
                
                    if(inventarioHasSucursal.getInventarioHasSucursalPK().equals(key)){
                    
                    return true;
                    
                    }
                
                }
            
            }
            
            return false;
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return false;
        
        }
    
    }
    
    @Override
    public void addToInventoryHasSucursalList(){
    
        try{
        
            
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Inventory.View.Inventory inventory=(BranchAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "inventory");
            
            if(inventory.getSelectedItem()!=null){
                
                System.out.print("SELECTED ITEM "+inventory.getSelectedItem()+" "+inventory.getSelectedItem().getQuantity());
            
            if(inventory.getInventarioDataModel() instanceof LazySucursalInventarioDataModel){
            
                LazySucursalInventarioDataModel model=(LazySucursalInventarioDataModel)inventory.getInventarioDataModel();
                
                if(model.getDatasource()!=null && !model.getDatasource().isEmpty()){
                
                    if(inventory.getAddInventoryList()==null){
                    
                        inventory.setAddInventoryList(new java.util.ArrayList<Entities.InventarioHasSucursal>());
   
                    }
                
                    if(!this.checkExistenceInList(inventory.getSelectedItem().getInventarioHasSucursalPK(), inventory.getAddInventoryList()) && inventory.getSelectedItem().getQuantity()>0){
                    
                        inventory.getAddInventoryList().add(inventory.getSelectedItem());
                    
                    }
                    
                }
            
            }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
        }
    
    
    }
    
    
    @Override
    public void deleteAddInventoryList(Entities.InventarioHasSucursalPK key){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Inventory.View.Inventory inventory=(BranchAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "inventory");
            
            if(inventory!=null){
            
            if(inventory.getAddInventoryList()!=null && !inventory.getAddInventoryList().isEmpty()){
            
            for(Entities.InventarioHasSucursal inventarioHasSucursal:inventory.getAddInventoryList()){
            
            if(inventarioHasSucursal.getInventarioHasSucursalPK().equals(key)){
            
            inventory.getAddInventoryList().remove(inventarioHasSucursal);
            
            break;
            
            }
            
            }
            
            }
            
            }
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
        }
    
    }
    
    @Override
    public ENUM.TransactionStatus saveChanges(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Inventory.View.Inventory inventory=(BranchAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "inventory");
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            
            if(inventory!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                if(branch.getInventarioHasSucursalCollection()==null){
                
                branch.setInventarioHasSucursalCollection(new java.util.ArrayList<Entities.InventarioHasSucursal>());
                
                }
                
                if(inventory.getAddInventoryList()!=null && !inventory.getAddInventoryList().isEmpty()){
                
                    for(Entities.InventarioHasSucursal inventarioHasSucursal:inventory.getAddInventoryList()){
                    
                    branch.getInventarioHasSucursalCollection().add(inventarioHasSucursal);
                    
                    }
                
                     _branchController.update(branch);
                     
            return ENUM.TransactionStatus.APPROVED;
                    
                }
                
               
            }
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
               return ENUM.TransactionStatus.DISAPPROVED;
            
        }
    
    }
    
    @Override
    public ENUM.TransactionStatus updateInventories(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
        
            Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
            
            if(branch.getInventarioHasSucursalCollection()!=null && !branch.getInventarioHasSucursalCollection().isEmpty()){
            
            this._branchController.update(branch);
              
            return ENUM.TransactionStatus.APPROVED;
            
            }
        
        }
        
        return ENUM.TransactionStatus.DISAPPROVED;
    
    }catch(NullPointerException ex){
    
    ex.printStackTrace(System.out);
    
     return ENUM.TransactionStatus.DISAPPROVED;
    
    }
    
    }
    
    @Override
    public ENUM.TransactionStatus deleteInventory(Entities.InventarioHasSucursalPK key){
    
    try{
    
    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
    
    
    if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
    
        Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
        
        if(branch.getInventarioHasSucursalCollection()!=null && !branch.getInventarioHasSucursalCollection().isEmpty()){
        
            for(Entities.InventarioHasSucursal inventarioHasSucursal:branch.getInventarioHasSucursalCollection()){
            
            if(inventarioHasSucursal.getInventarioHasSucursalPK().equals(key)){
            
                branch.getInventarioHasSucursalCollection().remove(inventarioHasSucursal);
                
                   this._inventarioHasSucursalController.delete(key);
                
                    this.fileUpload.setActionType(null);
                   
                  return TransactionStatus.APPROVED;
                   
               
            }
            
            }
        
            java.util.List<Entities.InventarioHasSucursal>inventarioHasSucursalList=new java.util.ArrayList<Entities.InventarioHasSucursal>();
            
            if(branch.getInventarioHasSucursalCollection()!=null && !branch.getInventarioHasSucursalCollection().isEmpty()){
            
                for(Entities.InventarioHasSucursal aux:branch.getInventarioHasSucursalCollection()){
                
                inventarioHasSucursalList.add(aux);
                
                }
            
            }
            
   
           
            
        }
    
    }
    
    
    return TransactionStatus.DISAPPROVED;
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
        
        return TransactionStatus.DISAPPROVED;
    
    }
    
    }
    
}
