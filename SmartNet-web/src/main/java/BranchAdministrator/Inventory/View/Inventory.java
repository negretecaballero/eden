/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Inventory.View;

import Clases.BaseBacking;
import FrontEndModel.LazySucursalInventarioDataModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author luisnegrete
 */
public class Inventory extends BaseBacking {

    /**
     * Creates a new instance of Inventory
     */
    public Inventory() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SucursalInventoryController sucursalInventoryController;
 
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.InventarioHasSucursal inventarioHasSucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private BranchAdministrator.Inventory.Controller.InventoryController _inventoryController;
    
    private java.util.List<Entities.InventarioHasSucursal>addInventoryList;
    
    private Entities.InventarioHasSucursal selectedItem;
    
    private Entities.InventarioHasSucursal updateInventory;
    
    private LazyDataModel <Entities.InventarioHasSucursal>lazyModel;
    
    public LazyDataModel<Entities.InventarioHasSucursal>getLazyModel(){
    
        return this.lazyModel;
    
    }
    
    public void setLazyModel(LazyDataModel<Entities.InventarioHasSucursal>lazyModel){
    
        this.lazyModel=lazyModel;
    
    }
    
    public Entities.InventarioHasSucursal getUpdateInventory(){
    
        return this.updateInventory;
    
    }
    
    public void setUpdateInventory(Entities.InventarioHasSucursal updateInventory){
    
        this.updateInventory=updateInventory;
    
    }
    
    public Entities.InventarioHasSucursal getSelectedItem(){
    
    return this.selectedItem;
    
    }
    
    public void setSelectedItem(Entities.InventarioHasSucursal selectedItem){
    
    this.selectedItem=selectedItem;
    
    }
    
    
    public java.util.List<Entities.InventarioHasSucursal>getAddInventoryList(){
    
        return this.addInventoryList;
    
    }
    
    public void setAddInventoryList(java.util.List<Entities.InventarioHasSucursal>addInventoryList){
    
    this.addInventoryList=addInventoryList;
    
    }
    
    private LazyDataModel<Entities.InventarioHasSucursal>inventarioDataModel;
    
    
    public LazyDataModel getInventarioDataModel(){
    
    return this.inventarioDataModel;
    
    }
    
    public void setInventarioDataModel(LazyDataModel<Entities.InventarioHasSucursal>inventarioDataModel){
    
    
    this.inventarioDataModel=inventarioDataModel;
    
    }
    
    public java.util.List<Entities.InventarioHasSucursal>getInventoryHasSucursalList(){
    
    try{
    
       if(this.getSession().getAttribute("selectedOption") instanceof Entities.Sucursal){
       
           Entities.Sucursal suc=(Entities.Sucursal)this.getSession().getAttribute("selectedOption");
           
           return this.sucursalInventoryController.getInventoryList(suc.getSucursalPK());
       
       }
       else{
       
           throw new IllegalArgumentException("Object of type "+this.getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
       
       }
    
    }
    catch(Exception ex){
    
    Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
    
 return null;
    
    }
    
    }
    
    @javax.annotation.PostConstruct
    
    public void init(){
    
        try{
        
         
            
        }
        catch(Exception ex){
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    public void addToInventoryList(org.primefaces.event.SelectEvent event){
    
     try{   
        /*
         System.out.print("SELECTED ITEM VALUE "+this.selectedItem);
         
    if(this.selectedItem instanceof Entities.InventarioHasSucursal){
    
        if(this.addInventoryList==null){
        
            this.addInventoryList=new java.util.ArrayList<Entities.InventarioHasSucursal>();
        
        }
    
        System.out.print("Selected Inventory "+this.selectedItem.getInventario().getNombre());
        
        
        if(!this.addInventoryList.contains(this.selectedItem))
        {  
            
        this.addInventoryList.add(this.selectedItem);
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(((Entities.InventarioHasSucursal)event.getObject()).getInventario().getNombre()+ " added to the list",javax.faces.application.FacesMessage.FACES_MESSAGES);
        
        this.getContext().addMessage(null, msg);
        
        
        }
        
        else{
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("This Object has Been added already",javax.faces.application.FacesMessage.FACES_MESSAGES);
        
        this.getContext().addMessage(null, msg);
        
        
        }
        
        System.out.print("Add Inventory List has "+this.addInventoryList.size()+" items");
        
    }
    else{
    
        throw new IllegalArgumentException("Object of type "+this.selectedItem.getClass().getName()+" must be of type "+Entities.InventarioHasSucursal.class.getName());
    
    }
    */
         
         System.out.print("SELECTEDITEM "+this.selectedItem.getQuantity());
         
         this._inventoryController.addToInventoryHasSucursalList();
         
     }
     catch(NullPointerException ex){
     
     Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
     
    
     
     }
    
    }
    
    
    public void deleteAddInventoryList(Entities.InventarioHasSucursalPK key){
    
    try{
    
        this._inventoryController.deleteAddInventoryList(key);
    
    }
    catch(NullPointerException ex){
    
     
    
    }
    
    }
    
    public LazyDataModel<Entities.InventarioHasSucursal> getInventarioHasSucursalList(){
    
    try{
    
        LazyDataModel <Entities.InventarioHasSucursal>resultList;
        
        if(this.getSession().getAttribute("selectedOption") instanceof Entities.Sucursal){
        
            Entities.Sucursal sucursal=(Entities.Sucursal)this.getSession().getAttribute("selectedOption");
            
            resultList=new LazySucursalInventarioDataModel(this.inventarioHasSucursalController.findBySucursal(sucursal.getSucursalPK()));
            
            return resultList;
            
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+this.getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
        
        }
    
    }
    catch(Exception ex){
    
        Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
        
       return null;
    
    }
    
    }
    
    public void addInventory(javax.faces.event.ActionEvent event){
    
    try{
    
         java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(), "bundle");
        
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)super.getContext().getExternalContext().getContext();
        
        switch(  this._inventoryController.saveChanges()){
                
            case APPROVED:{
            
               
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddInventory.ChangesButton"));
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null, message);
                
                this.fileUpload.setActionType(null);
                
                break;
            
            }
            
            case DISAPPROVED:{
            
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddInventory.Error"));
               
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                super.getContext().addMessage(null, message);
                
            break;
            
            }
            
            default:{
            
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddInventory.Error"));
               
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                super.getContext().addMessage(null, message);
                
            break;
            
            }
        
        
        }
        
     
        
      
    }
    
    catch(NullPointerException ex){
    
        Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
        
       
    
    }
    
    }
    
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            _inventoryController.initPreRenderView(super.getCurrentView());
        
        }
        catch(NullPointerException ex){
        
        ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event, String type){
    
    try{
    
        if(type.equals("AddInventory")){
    
    if(!this.fileUpload.getActionType().getActionType().equals(type)){
    
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
        
        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
    
    }
    
    }
        
        else if(type.equals("listInventory")){
        
            if(!this.fileUpload.getActionType().getActionType().equals(type)){
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
            
                this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
            }
            
            if(this.getSession().getAttribute("selectedOption")instanceof Entities.Sucursal){
                
            Entities.Sucursal sucursal=(Entities.Sucursal)this.getSession().getAttribute("selectedOption");
                
            this.lazyModel=new LazySucursalInventarioDataModel(this.inventarioHasSucursalController.findBySucursal(sucursal.getSucursalPK()));
           
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+this.getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        
        else if(type.equals("updateInventory")){
        
        if(!this.fileUpload.getActionType().getActionType().equals(type)){
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
        
            this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
            
            this.fileUpload.setActionType(new Clases.ActionType(type));
            
        }
        
        if(this.getSessionAuxiliarComponent() instanceof Entities.InventarioHasSucursal){
        
            this.updateInventory=(Entities.InventarioHasSucursal)this.getSessionAuxiliarComponent();
        
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+this.getSessionAuxiliarComponent().getClass().getCanonicalName()+" must be of type "+Entities.InventarioHasSucursal.class.getName());
        
        }
        
        }
        
    }
    catch(Exception ex){
    
    Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    public void deleteInventory(Entities.InventarioHasSucursal inventoryHasSucursal){
    
    try{
    
        java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(), "bundle");
        
        
        
        switch(this._inventoryController.deleteInventory(inventoryHasSucursal.getInventarioHasSucursalPK())){
        
            case APPROVED:{
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorListSale.Deleted"));
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null, msg);
                
            break;
            }
            case DISAPPROVED:{
            
                  javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorListSale.Error"));
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                super.getContext().addMessage(null, msg);
                
            break;
            }
        
        }
        
      
        
    }
    catch(Exception ex){
    
    Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
    
 
    
    }
    
    }
    
   public void updateSelection(org.primefaces.event.SelectEvent event){
   
       try{
       /*
         if(event.getObject() instanceof Entities.InventarioHasSucursal){
         
             this.updateInventory=(Entities.InventarioHasSucursal)event.getObject();
             
         }
         else{
         
             throw new IllegalArgumentException("Object of type "+event.getObject().getClass().getName()+" must be of type "+Entities.InventarioHasSucursal.class.getName());
         
         }
       */
       }
       catch(Exception ex){
       
           Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
   
   }
    
   
   public void updateInventories(javax.faces.event.AjaxBehaviorEvent event){
   
       try{
       
           java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(), "bundle");
           
           switch(this._inventoryController.updateInventories()){
           
               case APPROVED:{
                   
                   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorUpdateInventory.UpdatedSuccessfully"));
               
                   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                   
                   super.getContext().addMessage(null, msg);
                   
               break;
               
               }
               
               case DISAPPROVED:{
               
                   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorUpdateInventory.UpdatedError"));
                   
                   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                   
                   super.getContext().addMessage(null, msg);
                   
                   break;
               
               }
           
           }
           
       }
       catch(NullPointerException ex){
       
           Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);

       
       }
       
   }
  
   public void quantityChanged(){
   
       try{
       
           System.out.print(this.updateInventory.getQuantity());
       
       }
       catch(Exception ex){
       
           Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
           
      
           
       }
   
   }

	public String saveChanges() {
		// TODO - implement Inventory.saveChanges
		throw new UnsupportedOperationException();
	}
   
 
   
}
