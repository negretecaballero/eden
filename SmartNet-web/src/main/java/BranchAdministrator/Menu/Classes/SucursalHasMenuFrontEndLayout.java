/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Menu.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SucursalHasMenuFrontEndLayout {
    
    Entities.Menu getMenu();
    
    void setMenu(Entities.Menu menu);
    
    String getImage();
    
    void setImage(String image);
    
}
