/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Menu.Classes;

/**
 *
 * @author luisnegrete
 */
public class SucursalHasMenuFrontEndLayoutImplementation implements SucursalHasMenuFrontEndLayout{
    
    private Entities.Menu menu;
    
    private String image;
    
    @Override
    public Entities.Menu getMenu(){
    
        return this.menu;
    
    }
    @Override
    public void setMenu(Entities.Menu menu){
    
        this.menu=menu;
    
    }
    @Override
    public String getImage(){
    
        return this.image;
    
    }
    @Override
    public void setImage(String image){
    
    this.image=image;
    
    }
    
}
