package BranchAdministrator.Menu.Controller;

import Clases.*;
import ENUM.*;

public interface MenuController {

	/**
	 * 
	 * @param viewId
	 */
	void initPreRenderView(String viewId);

	/**
	 * 
	 * @param viewId
	 * @param menuList
	 */
	
     
	/**
	 * 
	 * @param key
	 */
	void addToMenuList();

	/**
	 * 
	 * @param idMenu
	 */
	void removeFromMenuList(int idMenu);

	TransactionStatus saveChanges();

	/**
	 * 
	 * @param idMenu
	 */
	void delete(int idMenu);

	
	

	

}