package BranchAdministrator.Menu.Controller;

import javax.faces.context.FacesContext;
import Clases.*;
import ENUM.*;


@javax.enterprise.inject.Alternative
public class MenuControllerImplementation implements MenuController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;

    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.MenuController _menuController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SucursalControllerDelegate _branchController;
    
	/**
	 * 
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(String viewId) {
	
            try{
                
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalMenu");
                
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal)
                {
                    
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
             switch(viewId){
             
            case "/AdminSucursal/add-menu.xhtml":{
            
                if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId))
                
                {
                
                _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                _fileUpload.setActionType(new Clases.ActionType(viewId));
                
                menu.setMenuList(this.sucursalMenuList(viewId, _menuController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia())));
                
                System.out.print("PRERENDER VIEW ADDMENU");
                
                updateSelectedMenuList();
                
                }
                  
     
                break;
            
            }
            
            case "/AdminSucursal/list-menu.xhtml":{
            
                if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId))
                
                {
                
                _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                _fileUpload.setActionType(new Clases.ActionType(viewId));
                
                menu.setListMenu(this.sucursalMenuList(viewId, (java.util.List<Entities.Menu>)branch.getMenuCollection()));
                
                System.out.print("PRERENDER VIEW ADDMENU");
                
                
                }
                
            break;
            
            }
            
             
             }
             
            }
            
            }
            
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
            }
            
	}


/**
	 * 
	 * @param viewId
	 * @param menuList
	 */
	private final	java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout> sucursalMenuList(String viewId, java.util.List<Entities.Menu> menuList) {

            try{
            
                
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>resultList=new java.util.ArrayList<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>();
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
                               
                if(viewId.equals("/AdminSucursal/add-menu.xhtml")){
                
                if(sucursal.getMenuCollection()!=null && !sucursal.getMenuCollection().isEmpty()){
                
                    for(Entities.Menu aux:sucursal.getMenuCollection()){
                    
                        if(menuList.contains(aux)){
                        
                        menuList.remove(aux);
                        
                        }
                    
                    }
                
                }
                
                }
                
                
                for(Entities.Menu aux:menuList){
                
                  BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout item=new BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayoutImplementation();
                  
                  item.setMenu(aux);
                  
                  
                  
                  if(aux.getImagenCollection()!=null && !aux.getImagenCollection().isEmpty()){
                  
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                      
                      java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+aux.getNombre());
                      
                      Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                      
                      if(file.exists()){
                      
                          filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"demo"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+aux.getNombre());
                      
                      }
                      else{
                      
                          filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, aux.getNombre());
                      
                      }
                      
                      for(Entities.Imagen image:aux.getImagenCollection()){
                      
                       _fileUpload.loadImagesServletContext(image, session.getAttribute("username")+java.io.File.separator+aux.getNombre());
                      
                       String imagePath=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
                       
                       item.setImage(imagePath);
                       
                       _fileUpload.getImages().remove(_fileUpload.getImages().size()-1);
                       
                       _fileUpload.getPhotoList().remove(_fileUpload.getPhotoList().size()-1);
                       
                      }
                      
                  }
                  
                  else{
                  
                      item.setImage(java.io.File.separator+"images"+java.io.File.separator+"noImage.png");
                      
                  }
                
                  resultList.add(item);
                }
                
                  
        
              return resultList;  
            }
          
            return null;
            
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
                
            return null;
            
            }
            
	}
        
        

	
	/**
	 * 
	 * @param key
	 */
	@Override
	public void addToMenuList() {

            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                System.out.print("REQUEST CONTEXT "+javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath());
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalMenu");
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(menu!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                
                for(java.util.Map.Entry<String,String>entry:javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().entrySet())
                
                {
                
                   System.out.print( entry.getKey()+"-"+entry.getValue());
                
                }
                    
                if(request.getParameter("hiddenIdMenu")!=null){
                
                int idMenu=Integer.valueOf(request.getParameter("hiddenIdMenu"));
                
                if(!this.checkExistence(idMenu))
                
                {
                
                    if(this.findInLayoutList(idMenu)!=null){
                    
                        if(menu.getSelectedMenu()==null){
                        
                        menu.setSelectedMenu(new java.util.ArrayList<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>());
                        
                        }
                    
                        menu.getSelectedMenu().add(this.findInLayoutList(idMenu));
                        
                    }
                
                }
                
                }
                    
                }
                
            }
            
            catch(NullPointerException | NumberFormatException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
            
	}

	/**
	 * 
	 * @param idMenu
	 */
	private boolean checkExistence(int idMenu) {

        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalMenu");
            
            if(menu!=null){
            
                if(menu.getSelectedMenu()!=null && !menu.getSelectedMenu().isEmpty()){
                
                    for(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout aux:menu.getSelectedMenu()){
                    
                    if(aux.getMenu().getIdmenu()==idMenu){
                    
                        return true;
                    
                    }
                    
                    }
                
                }
            
            }
               
         return false;   
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return false;
        
        }
        
        }

	/**
	 * 
	 * @param idMenu
	 */
	private BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout findInLayoutList(int idMenu) {
		
            try{
                
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalMenu");
            
                if(menu!=null){
                
                    if(menu.getMenuList()!=null && !menu.getMenuList().isEmpty()){
                    
                    for(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout aux:menu.getMenuList())
                    
                    {
                    
                        if(aux.getMenu().getIdmenu()==idMenu){
                        
                            return aux;
                        
                        }
                    
                    }
                        
                        
                    }
                
                }
                
            return null;
            }
            catch(NullPointerException ex){
            
                
            return null;
            }
            
	}

	private void updateSelectedMenuList() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalMenu");
            
                if(menu!=null){
                
                    if(menu.getSelectedMenu()!=null && !menu.getSelectedMenu().isEmpty()){
                    
                        for(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout aux:menu.getSelectedMenu()){
                        
                       if(this.findInLayoutList(aux.getMenu().getIdmenu())!=null){
                       
                           aux.setImage(this.findInLayoutList(aux.getMenu().getIdmenu()).getImage());
                       
                       }
                        
                        }
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param idMenu
	 */
	@Override
	public void removeFromMenuList(int idMenu) {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalMenu");
            
                if(menu!=null){
                
                    if(menu.getSelectedMenu()!=null && !menu.getSelectedMenu().isEmpty()){
                    
                    for(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout aux:menu.getSelectedMenu()){
                    
                    if(aux.getMenu().getIdmenu()==idMenu){
                    
                        menu.getSelectedMenu().remove(aux);
                        
                        break;
                    
                    }
                    
                    }
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	@Override
        
	public TransactionStatus saveChanges() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Menu.View.Menu menu=(BranchAdministrator.Menu.View.Menu)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalMenu");
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(menu!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
                    if(branch.getMenuCollection()==null){
                    
                        branch.setMenuCollection(new java.util.ArrayList<Entities.Menu>());
                    
                    }
                    
                    if(menu.getSelectedMenu()!=null && !menu.getSelectedMenu().isEmpty()){
                    
                    
                        
                        for(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout aux:menu.getSelectedMenu()){
                        
                            branch.getMenuCollection().add(aux.getMenu());
                        
                        }
                        
                        System.out.print("BRANCH MENU SIZE "+branch.getMenuCollection().size());
                        
                        _branchController.update(branch);
                    
                        return ENUM.TransactionStatus.APPROVED;
                        
                    }
                
                }
               return ENUM.TransactionStatus.DISAPPROVED;  
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            
	}

	/**
	 * 
	 * @param idMenu
	 */
	@Override
	public void delete(int idMenu) {
	
            try{
            
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
              
              if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
              
                  Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                  
                  if(branch.getMenuCollection()!=null && !branch.getMenuCollection().isEmpty()){
                  
                  for(Entities.Menu aux:branch.getMenuCollection()){
                  
                  if(aux.getIdmenu()==idMenu){
                  
                  branch.getMenuCollection().remove(aux);
                  
                  break;
                  
                  }
                  
                  }
                  
                  _branchController.update(branch);
                  
                  _fileUpload.setActionType(null);
                  
                  }
              
              }
                
            
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
	}

	


	

}