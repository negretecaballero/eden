/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Menu.View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class Menu extends Clases.BaseBacking{

    /**
     * Creates a new instance of Menu
     */
    public Menu() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
   // @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SucursalMenuController sucursalMenuController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private BranchAdministrator.Menu.Controller.MenuController _menuController;
    
    
    private java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>menuList;
    
    private java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>selectedMenu;
    
    private java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>listMenu;
    
    public java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>getListMenu(){
    
    return this.listMenu;
    
    }
    
    public void setListMenu(java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>listMenu){
    
        this.listMenu=listMenu;
    
    }
    
    public java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>getSelectedMenu(){
    
    return this.selectedMenu;
    
    }
    
    public void setSelectedMenu(java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>selectedMenu){
    
        this.selectedMenu=selectedMenu;
    
    }
    
    public java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout> getMenuList(){
    
        return this.menuList;
    
    }
    
    public void setMenuList(java.util.List<BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout>menuList){
    
        this.menuList=menuList;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
  
        
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
        
        try{
    
        System.out.print("Root Id"+this.getContext().getViewRoot().getViewId());
    
        String id=this.getContext().getViewRoot().getViewId();
        
        switch (id){
        
            case "/AdminSucursal/add-menu.xhtml":{
                
                if(!this.fileUpload.getActionType().getActionType().equals(id)){
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                
                    this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                    
                    this.fileUpload.setActionType(new Clases.ActionType(id));
                }
                
//                this.menuList=sucursalMenuController.findMenuByFranchise();
                
                break;
            
            }
            
            case "/AdminSucursal/list-menu.xhtml":{
                
                if(!this.fileUpload.getActionType().getActionType().equals(id)){
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                   
                    this.fileUpload.setActionType(new Clases.ActionType(id));
                    
//                    this.listMenu=this.sucursalMenuController.findBySucursal();
                
                    System.out.print("List Menu Size "+this.listMenu.size());
                }
                
              
                
                
                break;}
        
        }
        
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            _menuController.initPreRenderView(super.getCurrentView());
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void addToSelectedMenu(javax.faces.event.ActionEvent event){
    
    try{
    
        this._menuController.addToMenuList();
                
    }
    catch(Exception ex){
    
        Logger.getLogger(Menu.class.getName()).log(Level.SEVERE,null,ex);
        
    
    
    }
    
    }
    
    public void deleteFromList(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout sucursalHasMenu){
    
        try{
        
            _menuController.removeFromMenuList(sucursalHasMenu.getMenu().getIdmenu());
            
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE,null,ex);
            
        
        }
        
        
    }
    
    public String saveChanges(){
    
        try{
            
            switch(_menuController.saveChanges()){
            
                case APPROVED:{
                
                return "success";
                
                }
                case DISAPPROVED:{
                
               return "failure";
                
                }
                
                default:{
                
                return "failure";
               
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
          return "failure";
            
        }
    
    }
 
    public void deleteFromMenuList(BranchAdministrator.Menu.Classes.SucursalHasMenuFrontEndLayout item){
    
        try{
        
            _menuController.delete(item.getMenu().getIdmenu());
            
        }
        catch(NullPointerException ex){
        
          ex.printStackTrace(System.out);
            
        }
    
    }
    
}
