/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SucursalHasProductoFrontEndLayout {
    
    Entities.Producto getProduct();
    
    void setProduct(Entities.Producto product);
    
    String getImage();
    
    void setImage(String image);
    
    @Override
    String toString();
    
}
