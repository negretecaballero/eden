/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.Controller;

import Entities.ProductoPK;
import ENUM.*;

/**
 *
 * @author luisnegrete
 */
public interface ProductController {
    
    void initPreRenderView(String viewId);

	void addToBranchProductList(ProductoPK key);


	void removeBranchProductList(ProductoPK key);

	TransactionStatus saveChanges();

	
	void deleteFromBranch(Entities.ProductoPK key);


	

	

	
	



	
}
