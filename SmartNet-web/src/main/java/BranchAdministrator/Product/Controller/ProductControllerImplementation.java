/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.Controller;

import Interceptors.LoggingInterceptor;
import javax.interceptor.Interceptors;
import ENUM.*;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@Interceptors(LoggingInterceptor.class)
public class ProductControllerImplementation implements ProductController {
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject 
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface _productController;
    
    @javax.inject.Inject 
    @CDIBeans.SucursalControllerQualifier
    private CDIBeans.SucursalControllerDelegate _branchController;
    
    @Override
    public void initPreRenderView(String viewId){
    
    try{
        
          javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      
        if(session.getAttribute("selectedOption") instanceof Entities.Sucursal)
        {
        
            Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
            
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        BranchAdministrator.Product.View.Product product=(BranchAdministrator.Product.View.Product)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"product");
        
        switch(viewId){
        
            case "/AdminSucursal/add-product.xhtml":{
    
                if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"images"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                     product.setProducts(this.getProductsList(_productController.findByFranchise(branch.getFranquicia().getIdfranquicia()),viewId));
                    
                }
                
               
                
            break;
            
            }
            
            case "/AdminSucursal/list-products.xhtml":{
            
                if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
              
                    product.setUpdateLazyModel(this.getProductsList((java.util.List<Entities.Producto>)branch.getProductoCollection(),viewId));

                }
                
                break;
            
            }
        
        }
    
    }
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
   
    
     
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>getProductsList(java.util.List<Entities.Producto>products, String viewId){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption") instanceof Entities.Sucursal){
        
            Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
            
         
            if(viewId.equals("/AdminSucursal/add-product.xhtml"))
                
            {
           for(Entities.Producto product:sucursal.getProductoCollection()){
           
               if(products.contains(product)){
               
                   products.remove(product);
               
               }
           
           }
           
        }
           
           
           java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>results=new java.util.ArrayList<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>();
           
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           for(Entities.Producto product:products){
           
               BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux=new BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayoutImplementation();
               
               aux.setProduct(product);
               
               Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
               
              String imagen="";
              
               if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty())
               {
               for(Entities.Imagen image:product.getImagenCollection()){
               
                  String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+(product.getNombre()+product.getProductoPK().getIdproducto());
               
                  java.io.File file=new java.io.File(path);
                  
                  if(!file.exists()){
                  
                  filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username")+java.io.File.separator, product.getNombre());
                  
                  }
                  
                  else{
                  
                      filesManagement.cleanFolder(path);
                  
                  }
                  
                  _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
               
               String auxi=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
               
             imagen=auxi;
               
               _fileUpload.getImages().remove(_fileUpload.getImages().size()-1);
               
               _fileUpload.getPhotoList().remove(_fileUpload.getPhotoList().size()-1);
               break;
               }
           }
               else{
               
                   imagen="/images/noImage.png";
               
               }
               
               aux.setImage(imagen);
               
               results.add(aux);
           
           }
           
           return results;
           
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Addition.class.getName());
        
        }
    
    }
    catch(NullPointerException | IllegalArgumentException ex){
    
        ex.printStackTrace(System.out);
        
     return null;
    
    }
    
    }

	/**
	 * 
	 * @param key
	 */
    @Override
	public void addToBranchProductList(Entities.ProductoPK key) {

            try{
                
              javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
              BranchAdministrator.Product.View.Product product=(BranchAdministrator.Product.View.Product)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "product");
              
              if(product!=null){
              
              if(product.getSelectedImages()==null){
              
                  product.setSelectedImages(new java.util.ArrayList<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>());
              
              }
              
              if(product.getProducts()!=null && !product.getProducts().isEmpty()){
              
              for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:product.getProducts()){
              
              if(aux.getProduct().getProductoPK().equals(key)){
              
                  product.getSelectedImages().add(aux);
                  
                  break;
              
              }
              
              }
              
              }
              
              }
              
              
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void removeBranchProductList(Entities.ProductoPK key) {
		
            
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Product.View.Product product=(BranchAdministrator.Product.View.Product)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "product");
            
                if(product!=null){
                
                    if(product.getSelectedImages()!=null && !product.getSelectedImages().isEmpty()){
                    
                        for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:product.getSelectedImages()){
                        
                            if(aux.getProduct().getProductoPK().equals(key)){
                            
                                product.getSelectedImages().remove(aux);
                                
                                break;
                            
                            }
                        
                        }
                    
                    }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}


	@Override
	public TransactionStatus saveChanges() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Product.View.Product product=(BranchAdministrator.Product.View.Product)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"product");
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(product!=null && session.getAttribute("selectedOption") instanceof Entities.Sucursal){
                    
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                    if(product.getSelectedImages()!=null && !product.getSelectedImages().isEmpty()){
                    
                        java.util.List<Entities.Producto>results=new java.util.ArrayList<Entities.Producto>();
                        
                        if(branch.getProductoCollection()==null){
                        
                            branch.setProductoCollection(new java.util.ArrayList<Entities.Producto>());
                        
                        }
                        
                        for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:product.getSelectedImages()){
                        
                          branch.getProductoCollection().add(aux.getProduct());
                        
                        }
                               
                         _branchController.update(branch);
                         
                         return ENUM.TransactionStatus.APPROVED;
                        
                    }
                
                 
                    
                }
            
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}



	/**
	 * 
	 * @param key
	 */
	@Override
	public void deleteFromBranch(Entities.ProductoPK key) {
		
            try{
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                if(branch.getProductoCollection()!=null && !branch.getProductoCollection().isEmpty()){
                
                    for(Entities.Producto aux:branch.getProductoCollection()){
                    
                        if(aux.getProductoPK().equals(key)){
                        
                            branch.getProductoCollection().remove(aux);
                            
                            break;
                        
                        }
                    
                    }
                    
                    this._branchController.update(branch);
                    
                    _fileUpload.setActionType(null);
                
                }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	

	
	
	


}
