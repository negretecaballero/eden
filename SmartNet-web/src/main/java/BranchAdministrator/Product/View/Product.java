/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class Product extends Clases.BaseBacking {

    /**
     * Creates a new instance of Product
     */
    public Product() {
    }
   
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SucursalProductController sucursalProductoController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    private java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>products;
    
    private java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> selectedImages;
    
    private java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>updateLazyModel;
    
     @javax.inject.Inject @javax.enterprise.inject.Default private BranchAdministrator.Product.Controller.ProductController _productController;

    
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> getUpdateLazyModel(){
    
        return this.updateLazyModel;
    
    }
    
    public void setUpdateLazyModel(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> updateLazyModel){
    
        this.updateLazyModel=updateLazyModel;
    
    }
    
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>getSelectedImages(){
    
    return this.selectedImages;
    
    }
    
    public void setSelectedImages(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>selectedImages){
    
    this.selectedImages=selectedImages;
    
    }
    
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>getProducts(){
    
    return this.products;
    
    }
    
    public void setProducts(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>products){
    
        this.products=products;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
    try{
    
        _productController.initPreRenderView(super.getCurrentView());
    
    }
    catch(NullPointerException ex){
    
    ex.printStackTrace(System.out);
    
    }
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event,String action){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
            
           switch(action){
           
               case "AddProduct":{
                 
                  if(!this.fileUpload.getActionType().getActionType().equals(action)) {
                  
                  this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("selectedOption"));
                  
                  }
                  
                 // this.products=this.sucursalProductoController.getProductsList();
                   
               break;
               
               }
               
               case "listProducts":{
               
               if(!this.fileUpload.getActionType().getActionType().equals(action)){
               
                   this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                   
                   this.fileUpload.setActionType(new Clases.ActionType(action));
                   
                   
               }
               
//               this.updateLazyModel=this.sucursalProductoController.getSucursalList();
                   
               break;
               
               }
           
           }
        
        }
        catch(NullPointerException ex){
        
        Logger.getLogger(Product.class.getName()).log(Level.SEVERE,null,ex);
        
     
        
        }
    
    }

    
    public void addToList(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout item){
    
      try{
      
          _productController.addToBranchProductList(item.getProduct().getProductoPK());
      
      }
      catch(NullPointerException ex){
      
          ex.printStackTrace(System.out);
      
      }
    
    }
    
    public void deleteFromProductList(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout item){
    
        try{
        
        this._productController.removeBranchProductList(item.getProduct().getProductoPK());
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public String update(){
        
        try
        
        {
            
            java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(), "bundle");
            
        switch(this._productController.saveChanges()){
        
            case APPROVED:{
            
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddProduct.APPROVED"));
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null, msg);
                
                this.fileUpload.setActionType(null);
                
                return "success";
            
            }
            case DISAPPROVED:{
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddProduct.Error"));
            
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null, msg);
                
                 this.fileUpload.setActionType(null);
                
                return "failure";
            
            }
            
            default:{
                
                 javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("sucursalAdministratorAddProduct.Error"));
            
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null, msg);
                
                 this.fileUpload.setActionType(null);
            
              return "failure";
                
            }
            
        }
    }
        
        catch(NullPointerException ex){
            
             
        
          return "failure";
        
        }
        
    }
    
   public void delete(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout item){
   
       try{
       
        this._productController.deleteFromBranch(item.getProduct().getProductoPK());
           
       }
       catch(NullPointerException ex){
       
           Logger.getLogger(Product.class.getName()).log(Level.SEVERE,null,ex);
      
       
       }
   
   }

	/**
	 * 
	 * @param event
	 */
	public void update(javax.faces.event.ActionEvent event) {
		// TODO - implement Product.update
		//throw new UnsupportedOperationException();
	}
}
