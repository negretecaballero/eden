/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Sale.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SucursalHasSaleLayout {
    
    Entities.Sale getSale();
    
    void setSale(Entities.Sale sale);
    
    String getImage();
    
    void setImage(String image);
    
}
