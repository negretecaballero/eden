/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Sale.Classes;

import Entities.Sale;

/**
 *
 * @author luisnegrete
 */
public class SucursalHasSaleLayoutImplementation implements SucursalHasSaleLayout {
    
    private Entities.Sale sale;
    
    private String image;

    @Override
    public Sale getSale() {
  
        
        return this.sale;
    
    }

    @Override
    public void setSale(Sale sale) {
    
        this.sale=sale;
    
    }

    @Override
    public String getImage() {
    
        return this.image;
    
    }

    @Override
    public void setImage(String image) {
    
    this.image=image;
    
    }
    
}
