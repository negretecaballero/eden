package BranchAdministrator.Sale.Controller;

import ENUM.*;

public interface SucursalSaleViewController {

	/**
	 * 
	 * @param viewId
	 */
	void initPreRenderView(String viewId);

	/**
	 * 
	 * @param key
	 */
	void addToSelectedList(Entities.SalePK key);

	TransactionStatus saveChanges();

	/**
	 * 
	 * @param key
	 */
	void removeSelectedList(Entities.SalePK key);

	/**
	 * 
	 * @param key
	 */
	TransactionStatus removeUpdateList(Entities.SalePK key);

}