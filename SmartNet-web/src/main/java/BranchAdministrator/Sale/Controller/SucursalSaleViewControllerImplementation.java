package BranchAdministrator.Sale.Controller;

import CDIBeans.*;
import javax.faces.context.FacesContext;
import ENUM.*;



@javax.enterprise.inject.Alternative
public class SucursalSaleViewControllerImplementation implements SucursalSaleViewController {

    
    
	/**
	 * 
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(String viewId) {
	
            try{
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "sucursalSaleView");
                
                
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal)
                {
                    
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                switch(viewId){
                
                    case "/AdminSucursal/add-sale.xhtml":{
                    
                        if(fileUpload.getActionType()==null || !fileUpload.getActionType().getActionType().equals(viewId)){
                        
                        fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+session.getAttribute("username").toString());
                        
                        fileUpload.setActionType(new Clases.ActionType(viewId));
                        
                        sucursalSaleView.setSaleList(this.branchSaleList(viewId, this._saleController.findByFranchise(branch.getSucursalPK().getFranquiciaIdfranquicia())));
                        
                        this.updateSelectedList();
                        
                        }
                        
                        break;
                    
                    }
                    
                    case "/AdminSucursal/list-sale.xhtml":{
                    
                        if(fileUpload.getActionType()==null || !fileUpload.getActionType().getActionType().equals(viewId))
                        
                        {
                        
                              fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+session.getAttribute("username").toString());
                        
                              fileUpload.setActionType(new Clases.ActionType(viewId));
                              
                              sucursalSaleView.setUpdateSaleList(this.branchSaleList(viewId, (java.util.List<Entities.Sale>)branch.getSaleCollection()));
                        
                        }
                        
                        break;
                    
                    }
                
                }
                
            }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param viewId
	 * @param saleList
	 */
	private final java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout> branchSaleList(String viewId, java.util.List<Entities.Sale> saleList) {
		
            try{
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                
        if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
            java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>resultList=new java.util.ArrayList<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>();
        
            Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
            
            if(viewId.equals("/AdminSucursal/add-sale.xhtml")){
            
                if(branch.getSaleCollection()!=null && !branch.getSaleCollection().isEmpty()){
                
                for(Entities.Sale sale:branch.getSaleCollection()){
                
                    if(saleList.contains(sale)){
                    
                        saleList.remove(sale);
                    
                    }
                
                }
                
                }
            
            }
             
            for(Entities.Sale sale:saleList)
            {
            
            BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux=new BranchAdministrator.Sale.Classes.SucursalHasSaleLayoutImplementation();
            
            aux.setSale(sale);
            
            if(sale.getImagenCollection()!=null && !sale.getImagenCollection().isEmpty()){
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                
                Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                
                if(!file.exists()){
                
                    filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, sale.getName());
                
                }
                else{
                
                    filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                
                }
                
                for(Entities.Imagen image:sale.getImagenCollection()){
                
                    this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                
                    String imagen=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
                    
                    aux.setImage(imagen);
                    
                    this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
                    
                    this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
                    
                }
            }
            
            else{
            
              aux.setImage(("/images/noImage.png"));
                
            }
            
             
            resultList.add(aux);
            }
            
              
              return resultList;
        }
            
            return null;
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return null;
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void addToSelectedList(Entities.SalePK key) {
		
            try{
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
             
            BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
     
      
            
                
                if(!this.checkExistence(key)){
                
                   if(sucursalSaleView!=null){
                       
                       if(sucursalSaleView.getSelectedSale()==null){
                       
                           sucursalSaleView.setSelectedSale(new java.util.ArrayList<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>());
                       
                       }
                   
                       if(sucursalSaleView.getSaleList()!=null && !sucursalSaleView.getSaleList().isEmpty()){
                       
                           for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux:sucursalSaleView.getSaleList()){
                           
                           if(aux.getSale().getSalePK().equals(key)){
                           
                               sucursalSaleView.getSelectedSale().add(aux);
                           
                               break;
                               
                           }
                           
                           }
                       
                       }
                   
                   }
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	private final boolean checkExistence(Entities.SalePK key) {
	
            
            try{
            
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
             
            BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
               
            if(sucursalSaleView!=null && sucursalSaleView.getSelectedSale()!=null && !sucursalSaleView.getSelectedSale().isEmpty()){
            
                for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux:sucursalSaleView.getSelectedSale()){
                
                    if(aux.getSale().getSalePK().equals(key)){
                    
                        return true;
                    
                    }
                
                }
            
            
            }
            
            return false;
            
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            return false;
            
            }
            
	}

	private final void updateSelectedList() {
	
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
                BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
                
                
                if(sucursalSaleView.getSelectedSale()!=null && !sucursalSaleView.getSelectedSale().isEmpty() && sucursalSaleView.getSaleList()!=null && !sucursalSaleView.getSaleList().isEmpty()){
                
                    for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux:sucursalSaleView.getSelectedSale()){
                    
                    for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout auxi:sucursalSaleView.getSaleList()){
                    
                        if(aux.getSale().getSalePK().equals(auxi.getSale().getSalePK())){
                        
                        aux=auxi;
                        
                        break;
                        
                        }
                    
                    }
                    
                    }
                
                }
                
            }
            
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	@Override
	public TransactionStatus saveChanges() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal && sucursalSaleView.getSelectedSale()!=null && !sucursalSaleView.getSelectedSale().isEmpty()){
                
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
                    if(branch.getSaleCollection()==null){
                    
                    branch.setSaleCollection(new java.util.ArrayList<Entities.Sale>());
                    
                    }
                    
                    for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux:sucursalSaleView.getSelectedSale()){
                    
                        branch.getSaleCollection().add(aux.getSale());
                    
                    }
                    
                    this._branchController.update(branch);
                    
                      return ENUM.TransactionStatus.APPROVED;
                
                }
                
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void removeSelectedList(Entities.SalePK key) {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
            
                if(sucursalSaleView.getSelectedSale()!=null && !sucursalSaleView.getSelectedSale().isEmpty()){
                
                for(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout aux:sucursalSaleView.getSelectedSale()){
                
                if(aux.getSale().getSalePK().equals(key)){
                
                sucursalSaleView.getSelectedSale().remove(aux);
                
                break;
                
                }
                
                }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public TransactionStatus removeUpdateList(Entities.SalePK key) {
            
            try{
		
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Sale.View.SucursalSaleView sucursalSaleView=(BranchAdministrator.Sale.View.SucursalSaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"sucursalSaleView");
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(sucursalSaleView!=null && session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
            
            if(sucursalSaleView.getUpdateSaleList()!=null && !sucursalSaleView.getUpdateSaleList().isEmpty()){
            
            for(Entities.Sale aux:branch.getSaleCollection()){
            
            if(aux.getSalePK().equals(key)){
                    
                branch.getSaleCollection().remove(aux);
                
                this._branchController.update(branch);
                
           return ENUM.TransactionStatus.APPROVED;
            
            }
            
            }
            
            }
            
            }
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
            
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}

	@javax.inject.Inject
	private FileUploadInterface fileUpload;
	@javax.inject.Inject
	
	private SaleControllerInterface _saleController;
	@javax.inject.Inject
	
	private SucursalControllerDelegate _branchController;

}