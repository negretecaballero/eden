/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Sale.View;

import java.util.logging.Level;
import java.util.logging.Logger;
import BranchAdministrator.Sale.Controller.*;

/**
 *
 * @author luisnegrete
 */
public class SucursalSaleView extends Clases.BaseBacking {

    /**
     * Creates a new instance of SucursalSaleView
     */
    public SucursalSaleView() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    //@javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SucursalSaleController sucursalSaleController;
    
    @javax.annotation.PostConstruct
    
    public void init(){
    
    }
    
    private java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>saleList;
    
    private java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>selectedSale;
    
    private java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>updateSaleList;
    
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private SucursalSaleViewController _sucursalHasSaleViewController;
    
    public java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>getUpdateSaleList(){
    
        return this.updateSaleList;
    
    }
    
    public void setUpdateSaleList(java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>updateSaleList){
    
     this.updateSaleList=updateSaleList;
    
    }
    
    public java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>getSelectedSale(){
    
        return this.selectedSale;
    
    }
    
    public void setSelectedSale(java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>selectedSale){
    
        this.selectedSale=selectedSale;
    
    }
    
    public java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>getSaleList(){
    
        return this.saleList;
    
    }
    
    public void setSaleList(java.util.List<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>saleList){
    
        this.saleList=saleList;
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            String id=this.getContext().getViewRoot().getViewId();
            
            switch(id){
        
                case "/AdminSucursal/add-sale.xhtml":{
                    
                    if(!this.fileUpload.getActionType().getActionType().equals(id)){
                    
                        if(this.saleList==null){
                        
                            this.saleList=new java.util.ArrayList<BranchAdministrator.Sale.Classes.SucursalHasSaleLayout>();
                        
                        }
                        
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                    
                        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                        
                        this.fileUpload.setActionType(new Clases.ActionType(id));
                    
                        
                        
                    }
                  //  this.saleList=this.sucursalSaleController.getSaleList();
                    break;
                
                }
                
                 case "/AdminSucursal/list-sale.xhtml":{
                
                    if(!this.fileUpload.getActionType().getActionType().equals(id)){
                    
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                    
                        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"resources"+java.io.File.separator+getSession().getAttribute("username").toString());
                        
                        this.fileUpload.setActionType(new Clases.ActionType(id));
                        
                      //  this.updateSaleList=this.sucursalSaleController.getSucursalSaleList();
                        
                        System.out.print("Update Sale List size "+this.updateSaleList.size());
                    }
                    
                    break;
                }
            
            }
            
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            
           
        
        }
    
    }
    
    public void addToSelectedSale(Entities.SalePK key){
    
        try{
            
            System.out.print("ADD TO SALE CLICKED");
            
            this._sucursalHasSaleViewController.addToSelectedList(key);
           
        }catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
          
        }
    
    }
    
    public void deleteSelectedSale(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout sale){
    
    try{
    
        this._sucursalHasSaleViewController.removeSelectedList(sale.getSale().getSalePK());
    
    }
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    
        
    }
    
    }
    
    /**
	 * 
	 * @param action
	 */
	public String saveChanges(){
    
        try{
        
            switch(this._sucursalHasSaleViewController.saveChanges()){
            
               case APPROVED:{
            
                return "success";
            
            }
               case DISAPPROVED:{
               
                   return "failure";
               
               }
               
               default:{
               
               return "failure";
               
               }
            
            
            
            }
            
        }
        catch(NullPointerException ex){
        
          ex.printStackTrace(System.out);
          
          return "failure";
            
        }
    
    }
    
    public void deleteFromUpdateList(BranchAdministrator.Sale.Classes.SucursalHasSaleLayout item){
    
        try{
            
            
            javax.faces.application.NavigationHandler nh=super.getContext().getApplication().getNavigationHandler();
            
            switch( this._sucursalHasSaleViewController.removeUpdateList(item.getSale().getSalePK()))
        
            {
            
                case APPROVED:{
                
                nh.handleNavigation(super.getContext(), null, "success");
                
                break;
                
                }
                
                case DISAPPROVED:{
                
                    nh.handleNavigation(super.getContext(), null, "failure");
                    
                    break;
                
                }
                
                default:{
                
                           nh.handleNavigation(super.getContext(), null, "failure");
                           
                           break;
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
                       
        
        }
    
    }

	

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
	
            try{
            
                this._sucursalHasSaleViewController.initPreRenderView(super.getCurrentView());
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
	}

	
}
