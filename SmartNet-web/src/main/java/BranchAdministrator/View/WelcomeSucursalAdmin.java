/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.View;

import Clases.BaseBacking;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import org.primefaces.event.NodeSelectEvent;

/**
 *
 * @author luisnegrete
 */
public class WelcomeSucursalAdmin extends BaseBacking{
    
    private String username;

     
   private String loginImage;
 

   
   @javax.inject.Inject @javax.enterprise.inject.Default
   private BranchAdministrator.Controller.WelcomeSucursalAdminController _welcomeSucursalAdminController;
   
   public void preRenderView(javax.faces.event.ComponentSystemEvent event){
   
       try{

   _welcomeSucursalAdminController.initPreRenderView(super.getCurrentView());
   
       }
       catch(NullPointerException ex){
       
       ex.printStackTrace(System.out);
       
       }
   
   }
   
   public int getZoom(){
   
   return _welcomeSucursalAdminController.zoom();
   
   }
   

   
   
   public org.primefaces.model.map.LatLng getCenter(){
   
   return this._welcomeSucursalAdminController.getCentre();
   
   }
   
   
   
   public org.primefaces.model.map.MapModel getModel(){
   
   return this._welcomeSucursalAdminController.getModel();
   
   }
   
  
   
   public String getLoginImage(){
   
   return this.loginImage;
   
   }
   
   public void setLoginImage(String loginImage){
   
   this.loginImage=loginImage;
   
   }
   
    public double getCompletedCoefficient(){
    
        return this._welcomeSucursalAdminController.completeCoefficient();
    
    }
    
 
    public boolean getCompletion(){
    
    return this._welcomeSucursalAdminController.getCompletion();
    
    }
    
    
    
    public String getUsername(){
    
    return this.username;
    
    }
    
    public void setUsername(String username){
    
    
    this.username=username;
    
    }
    

    
    public org.primefaces.model.TreeNode getRoot(){
    
    return _welcomeSucursalAdminController.getTree();
    
    }
    
  
    
    
    @PostConstruct
    public void init(){
    
      try{
       
        
       
        
      }
      catch(RuntimeException ex){
      
          Logger.getLogger(WelcomeSucursalAdmin.class.getName()).log(Level.SEVERE,null,ex);
      
      }
        
    
    }
    
   
    
    
    public void nodeSelected(NodeSelectEvent event){
        
        try{
    
       this._welcomeSucursalAdminController.nodeSelect(event.getTreeNode().getData().toString());
       
        }
        catch(RuntimeException ex){
        
        Logger.getLogger(WelcomeSucursalAdmin.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
    }
    
    public void actionListener(javax.faces.event.ActionEvent event){
    
        try{
        
           if(event.getComponent() instanceof org.primefaces.component.menuitem.UIMenuItem){
           
           org.primefaces.component.menuitem.UIMenuItem menuItem=(org.primefaces.component.menuitem.UIMenuItem)event.getComponent();
           
           System.out.print("MENUITEM VALUE "+menuItem.getValue());
           
           System.out.print("Context Path "+this.getContext().getExternalContext().getRequestContextPath());
           
           this._welcomeSucursalAdminController.redirectBranch(menuItem.getValue().toString());
           
          System.out.print("REQUEST CONTEXT "+super.getContext().getExternalContext().getRequestContextPath());
           
          super.getContext().getApplication().getNavigationHandler().handleNavigation(super.getContext(),null,"success");
           
           }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    

    /**
     * Creates a new instance of WelcomeSucursalAdmin
     */
    public WelcomeSucursalAdmin() {
    }
    
}
