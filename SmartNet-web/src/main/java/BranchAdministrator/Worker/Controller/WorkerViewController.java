package BranchAdministrator.Worker.Controller;

import ENUM.*;

public interface WorkerViewController {

	/**
	 * 
	 * @param viewId
	 */
	void initPreRenderView(String viewId);

	/**
	 * 
	 * @param username
	 */
	TransactionStatus addWorker(String username);

	java.util.List<Entities.LoginAdministrador> listWorkers();

	/**
	 * 
	 * @param loginAdministrador
	 */
	void deleteWorker(Entities.LoginAdministrador loginAdministrador);
}