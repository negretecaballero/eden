package BranchAdministrator.Worker.Controller;

import CDIBeans.*;
import ENUM.*;
import javax.transaction.Transactional.TxType;

@javax.enterprise.inject.Alternative
public class WorkerViewControllerImplementation implements WorkerViewController {

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private FileUploadInterface _fileUpload;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private LoginAdministradorControllerInterface _loginAdministrador;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private ProfessionControllerInterface _professionController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private SucursalControllerDelegate _branchController;

	/**
	 * 
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(String viewId) {
	
            try{
            
              final  javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
              final javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
              
               javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
              
               BranchAdministrator.Worker.View.WorkerView workerView=(BranchAdministrator.Worker.View.WorkerView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "workerController");
              
                switch(viewId){
                
                    case "/AdminSucursal/addworker.xhtml":{
                    
                        if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                        
                            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                        
                            _fileUpload.setActionType(new Clases.ActionType(viewId));
                            
                        }
                    
                        break;
                        
                    }
                    
                    case "/AdminSucursal/workerList.xhtml":{
                        

                         if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                        
                            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                        
                            _fileUpload.setActionType(new Clases.ActionType(viewId));
                        
                            if(this.listWorkers()!=null && !this.listWorkers().isEmpty()){
                            
                             workerView.setWorkerList(new FrontEndModel.SucursalWorkerLazyModel(this.listWorkers()));
                            
                            }
                            
                        }
  
                    break;
                    
                    }
                    
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
            
            
	}

	/**
	 * 
	 * @param username
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
	public TransactionStatus addWorker(String username) {
		
            try{
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal)
                {
                
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");    
                    
                Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
                
                appGroupPK.setGroupid("guanabara_worker");
                
                appGroupPK.setLoginAdministradorusername(username);
                
                Entities.AppGroup appGroup=new Entities.AppGroup();
                
                appGroup.setAppGroupPK(appGroupPK);               
                
                Entities.LoginAdministrador loginAdministrador=new Entities.LoginAdministrador();
                
                appGroup.setLoginAdministrador(loginAdministrador);
                
                loginAdministrador.setUsername(username);
                
                loginAdministrador.setPassword(Clases.EdenString.generatePassword(30));
                
                loginAdministrador.setProfessionIdprofession(_professionController.find(1));
                
                this._loginAdministrador.createAccount(loginAdministrador, appGroup);
                
                if(branch.getWorkerCollection()==null || branch.getWorkerCollection().isEmpty()){
                
                    branch.setWorkerCollection(new java.util.ArrayList<Entities.LoginAdministrador>());
                
                }
                
                Entities.LoginAdministrador user=this._loginAdministrador.find(username);
                
                System.out.print(branch.getWorkerCollection().size()+" SIZE");
                
                branch.getWorkerCollection().add(user);
                
                _branchController.update(branch);
                
                return ENUM.TransactionStatus.APPROVED;
                
            }

                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}

	@Override
	public java.util.List<Entities.LoginAdministrador> listWorkers() {
		
            try{
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                    Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                    
                    if(branch.getWorkerCollection()!=null && !branch.getWorkerCollection().isEmpty()){
                    
                        return (java.util.List<Entities.LoginAdministrador>)branch.getWorkerCollection();
                    
                    }
                
                }
                
                return null;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
                return null;
            }
            
	}

	/**
	 * 
	 * @param loginAdministrador
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
	public void deleteWorker(Entities.LoginAdministrador loginAdministrador) {
		
            try{
            

                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
                if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
                
                Entities.Sucursal branch=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                 if(branch.getWorkerCollection()!=null && !branch.getWorkerCollection().isEmpty()){
                 
                     for(Entities.LoginAdministrador aux:branch.getWorkerCollection()){
                     
                         if(aux.getUsername().equals(loginAdministrador.getUsername())){

                              
                             if(loginAdministrador.getSucursalCollection().isEmpty()){
                             
                              Entities.AppGroupPK key=new Entities.AppGroupPK();
                              
                              key.setGroupid("guanabara_worker");
                              
                              key.setLoginAdministradorusername(loginAdministrador.getUsername());
                                 
                              this._loginAdministrador.deleteAccountAppGroup(loginAdministrador.getUsername(), key);
                             
                             }
                             
                             else{
                             
                             branch.getWorkerCollection().remove(aux);
                             
                              this._branchController.update(branch);
                             
                             }
                             

                             this._fileUpload.setActionType(null);
                             
                             
                             break;
                         
                         }
                     
                     }
                 
                 }
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}
}