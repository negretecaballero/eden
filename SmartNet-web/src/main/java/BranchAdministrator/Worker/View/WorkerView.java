/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Worker.View;

import CDIBeans.ConfirmationDelegate;
import Clases.BaseBacking;
import Entities.Sucursal;

import Validators.MailValidator;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import jaxrs.service.AppGroupFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.ProfessionFacadeREST;
import BranchAdministrator.Worker.Controller.*;


/**
 *
 * @author luisnegrete
 */
@RolesAllowed("GuanabaraWorker")
public class WorkerView extends BaseBacking{

    /**
     * Creates a new instance of WorkerController
     */
    
    public WorkerView() {
    }
    
    
    //private SucursalhasloginAdministrador selectedItem;
    
   /* public SucursalhasloginAdministrador getSelectedItem(){
    
        return this.selectedItem;
    
    }
    
    public void setSelectedItem(SucursalhasloginAdministrador selectedItem){
    
    this.selectedItem=selectedItem;
    
    }*/
    
    @Inject private ConfirmationDelegate confirmationController;
    
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
    
    
    @EJB 
    private AppGroupFacadeREST appGroupFacadeREST;
    
    @EJB
    private ProfessionFacadeREST profesionFacadeREST;
    
    /*@EJB
    private SucursalhasloginAdministradorFacadeREST sucursalhasloginAdministradorFacadeREST;*/
    
    @MailValidator
    private String mail;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private WorkerViewController _workerViewController;
	private org.primefaces.model.LazyDataModel<Entities.LoginAdministrador> workerList;
	private Entities.LoginAdministrador selectedLoginAdministrador;
    
   // private List <SucursalhasloginAdministrador> sucursalhasloginAdministradorList;
    
    /*public List<SucursalhasloginAdministrador>getSucursalloginAdministradorList(){
    
    return this.sucursalhasloginAdministradorList;
    }*/
    
   /* public void setSucursalloginAdministradorList(List<SucursalhasloginAdministrador>sucursalhasloginAdministradorList)
    {
    
    this.sucursalhasloginAdministradorList=sucursalhasloginAdministradorList;
    }*/
    
    
    public String getMail(){
    
    return this.mail;
    
    }
    
    public void setMail(String mail){
    
    this.mail=mail;
    
    }
    
    @PostConstruct
    public void init(){


    }
    
    /**
	 * 
	 * @param event
	 */
	public String addWorker(){
    
    try{
        
    switch(this._workerViewController.addWorker(this.mail)){
    
        case APPROVED:{
        
            return "success";
        
        }
        
        case DISAPPROVED:{
        
            return "failure";
        
        }
        
        default:{
        
            return "failure";
        
        }
    
    }
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
        
      return "failure";
        
    }
    
    }
    
   public void deleteWorker(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
      
       this._workerViewController.deleteWorker(selectedLoginAdministrador);
   
   }
   catch(NullPointerException ex){
   
    ex.printStackTrace(System.out);
   
   }
   
   }

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
		
            try{
            
                _workerViewController.initPreRenderView(super.getCurrentView());
            
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
	}

	public void rewSelection(org.primefaces.event.SelectEvent event){
        
        try{
        
            System.out.print("SELECT EVENT");
            
        }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
        }

	public void setWorkerList(org.primefaces.model.LazyDataModel<Entities.LoginAdministrador> workerList) {
		this.workerList = workerList;
	}

	public org.primefaces.model.LazyDataModel<Entities.LoginAdministrador> getWorkerList() {
		return this.workerList;
	}

	public Entities.LoginAdministrador getSelectedLoginAdministrador() {
		return this.selectedLoginAdministrador;
	}

	public void setSelectedLoginAdministrador(Entities.LoginAdministrador selectedLoginAdministrador) {
		this.selectedLoginAdministrador = selectedLoginAdministrador;
	}

	/**
	 * 
	 * @param event
	 */
	public void deleteWorker(ActionEvent event) {
		// TODO - implement WorkerView.deleteWorker
		throw new UnsupportedOperationException();
	}
}
