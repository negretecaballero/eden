/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.AdditionConsumesInventario;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.AdditionConsumesInventarioFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@javax.inject.Named
@AdditionConsumesInventarioControllerQualifier
//@javax.enterprise.inject.Alternative
public class AdditionConsumesInventarioController implements AdditionConsumesInventarioDelegate{
 
    @EJB
    private AdditionConsumesInventarioFacadeREST additionConsumesInventarioFacadeREST;
    
    @Override
    public void create (AdditionConsumesInventario additionConsumesInventario){
        try{
          
            System.out.print("Addition consumes inventario Object "+additionConsumesInventario);
            
            additionConsumesInventarioFacadeREST.create(additionConsumesInventario);
    
        }
        catch(EJBException ex){
        
            Logger.getLogger(AdditionConsumesInventarioController.class.getName()).log(Level.SEVERE, null, ex);
        
            throw new FacesException("Exception Caught");
        }
    }
    
    @Override
    public void remove(Entities.AdditionConsumesInventarioPK additionConsumesInventarioPK){
    
        try{
            
            //additionIdaddition
            //additionSucursalIdsucursal
            //additionSucursalFranquiciaIdfranquicia
            //inventarioIdinventario
            //inventarioSucursalFranquiciaIdfranquicia
            //inventarioSucursalIdsucursal
        
            additionConsumesInventarioFacadeREST.remove(new PathSegmentImpl("bar;additionIdaddition="+additionConsumesInventarioPK.getAdditionIdaddition()+";additionFranquiciaIdFranquicia="+additionConsumesInventarioPK.getAdditionFranquiciaIdFranquicia()+";inventarioIdinventario="+additionConsumesInventarioPK.getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+additionConsumesInventarioPK.getInventarioFranquiciaIdFranquicia()+""));
            
        
        }
        catch(EJBException ex){
        
            Logger.getLogger(AdditionConsumesInventarioController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException("Exception Caugth");
        
        }
    
    }
    
    @Override
    public void edit(Entities.AdditionConsumesInventario additionConsumesInventario){
    
    try
        
    {
            //additionIdaddition
            //additionSucursalIdsucursal
            //additionSucursalFranquiciaIdfranquicia
            //inventarioIdinventario
            //inventarioSucursalFranquiciaIdfranquicia
            //inventarioSucursalIdsucursal
        
        Entities.AdditionConsumesInventarioPK pk=additionConsumesInventario.getAdditionConsumesInventarioPK();
    
        additionConsumesInventarioFacadeREST.edit(new PathSegmentImpl("bar;additionIdaddition="+pk.getAdditionIdaddition()+";additionFranquiciaIdFranquicia="+pk.getAdditionFranquiciaIdFranquicia()+";inventarioIdinventario="+pk.getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+pk.getInventarioFranquiciaIdFranquicia()+""), additionConsumesInventario);
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(AdditionConsumesInventarioController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException("Exception Caugth");
    
    }
    
    }
    
    @Override
    public java.util.List<Entities.AdditionConsumesInventario>findByAddition(Entities.AdditionPK additionPK){
    
        try{
        
            return this.additionConsumesInventarioFacadeREST.findByAddition(new PathSegmentImpl("bar;idaddition="+additionPK.getIdaddition()+";franquiciaIdfranquicia="+additionPK.getFranquiciaIdfranquicia()+""));
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    
    }
    
}
