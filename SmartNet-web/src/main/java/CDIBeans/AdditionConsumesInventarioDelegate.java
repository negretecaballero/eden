/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface AdditionConsumesInventarioDelegate {
    
    void create(Entities.AdditionConsumesInventario additionConsumesInventario);
    
    void remove(Entities.AdditionConsumesInventarioPK additionConsumesInventario);
    
    void edit(Entities.AdditionConsumesInventario additionConsumesInventario);
    
    java.util.List<Entities.AdditionConsumesInventario>findByAddition(Entities.AdditionPK additionPK);
}
