/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Addition;
import Entities.AdditionPK;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Model;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional.TxType;
import jaxrs.service.AdditionFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
//@javax.enterprise.context.Dependent

@CDIBeans.AdditionControllerQualifier

@Dependent
@javax.inject.Named("additionController")

public class AdditionController implements  AdditionControllerDelegate{
    
    @EJB
    private AdditionFacadeREST additionFacadeREST;
    
    @javax.inject.Inject @FileUploadBeanQualifier  private transient CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject
    
    @CDIBeans.AdditionConsumesInventarioControllerQualifier
    
    private CDIBeans.AdditionConsumesInventarioDelegate  _additionConsumesInventarioController;
    
    /**
	 * 
	 * @param idFranchise
	 */
	@Override
	@SuppressWarnings("UseOfObsoleteCollectionType")
    public java.util.Vector<FranchiseAdministrator.Product.Classes.AdditionEden> loadImages(int idFranchise){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
            javax.servlet.http.HttpSession httpSession=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
            
            java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+httpSession.getAttribute("username").toString()+java.io.File.separator+"additions");
            
            if(!file.exists()){
            
                filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+httpSession.getAttribute("username").toString()+java.io.File.separator, "additions");
            
            }
            else{
        
            filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+httpSession.getAttribute("username").toString()+java.io.File.separator+"additions");
            }
            java.util.List<Entities.Addition>results=this.findByFranchise(idFranchise);
            
            java.util.Vector<FranchiseAdministrator.Product.Classes.AdditionEden>additionList=new java.util.Vector<FranchiseAdministrator.Product.Classes.AdditionEden>(results.size());
            
            for(Entities.Addition aux:results){
                
            FranchiseAdministrator.Product.Classes.AdditionEden additionEden=new FranchiseAdministrator.Product.Classes.AdditionEdenImpl();
            
            additionEden.setAdditionPK(aux.getAdditionPK());
            
            if(aux.getImagenCollection()!=null && !aux.getImagenCollection().isEmpty()){
            
                java.io.File sucursalFile=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+httpSession.getAttribute("username").toString()+java.io.File.separator+"additions"+java.io.File.separator+aux.getName());
            
                 if(!sucursalFile.exists()){
            
                filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+httpSession.getAttribute("username").toString()+java.io.File.separator+"additions"+java.io.File.separator, aux.getName());
            
            }
            
                 for(Entities.Imagen image:aux.getImagenCollection()){
                 
                    this.fileUpload.loadImagesServletContext(image, httpSession.getAttribute("username").toString()+java.io.File.separator+"additions"+java.io.File.separator+aux.getName());
                    
                    String cacheImage=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
                    
                     additionEden.setImage(cacheImage);
                    
                    break;
                
                 
                 }
                 
            }
            
            else{
            
                additionEden.setImage("/images/noImage.png");
            
            }
            
            
           
             additionList.add(additionEden);
            }
            
           
            
            this.fileUpload.getPhotoList().clear();
            
            this.fileUpload.getImages().clear();
            
            return additionList;
        
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    /**
	 * 
	 * @param addition
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.Addition addition){
        
        try{
        
       SessionClasses.EdenList<Entities.AdditionConsumesInventario>additionConsumesInventarioList=new SessionClasses.EdenList<Entities.AdditionConsumesInventario>();
            
       if(addition.getAdditionConsumesInventarioCollection()!=null && !addition.getAdditionConsumesInventarioCollection().isEmpty()){
       
           for(Entities.AdditionConsumesInventario additionConsumesInventario:addition.getAdditionConsumesInventarioCollection()){
                
               Entities.AdditionConsumesInventario aux=new Entities.AdditionConsumesInventario();
               
               aux=additionConsumesInventario;
               
               additionConsumesInventarioList.addItem(aux);
               
                   }
       
       }
       
       addition.setAdditionConsumesInventarioCollection(new java.util.ArrayList<Entities.AdditionConsumesInventario>());
       
       additionFacadeREST.create(addition);
       
       if(additionConsumesInventarioList!=null && !additionConsumesInventarioList.isEmpty()){
       
           for(Entities.AdditionConsumesInventario additionConsumesInventario:additionConsumesInventarioList){
           
               additionConsumesInventario.getAdditionConsumesInventarioPK().setAdditionIdaddition(addition.getAdditionPK().getIdaddition());
               
               this._additionConsumesInventarioController.create(additionConsumesInventario);
           
           }
       
       }
       
       
        }
        catch(Exception ex){
        
           
        
        }
    
    }
    
    @Override
    public Entities.Addition find(AdditionPK additionPK){
    
        try{
    
        return additionFacadeREST.find(new PathSegmentImpl("bar;idaddition="+additionPK.getIdaddition()+";franquiciaIdfranquicia="+additionPK.getFranquiciaIdfranquicia()+""));
        
        }
        
        catch(EJBException ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException("Exception Caugth");
        
        }
    
    }
    
    @Override
    public Entities.Addition findByName(String name,int franchiseId){
    
        try{
        
        return  additionFacadeREST.findByname(name, franchiseId);
        
        }
        catch(Exception ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
        return null;
        }
    
    }
    
    
  
    
    @Override
    public java.util.Vector<Entities.Addition>findAll(){
    
        try{
            
            HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
            Entities.Franquicia franchise=((Entities.Franquicia)session.getAttribute("selectedOption"));    
                
            java.util.Vector<Entities.Addition>results=new java.util.Vector<Entities.Addition>(additionFacadeREST.findByFranchise(franchise.getIdfranquicia()).size());
            
            for(Entities.Addition addition:additionFacadeREST.findByFranchise(franchise.getIdfranquicia())){
            
                results.add(addition);
            
            }
            
            return results;
            
            }
            else{
            
               throw new IllegalArgumentException("object " + session.getAttribute("selectedOption") + " is of type " + (session.getAttribute("selectedOption")).getClass().getName() + "; expected type: " + Entities.Franquicia.class.getName());
          
            
            }
            
            
        }
        
        catch(EJBException ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException("Exception Caugth");
        
        }
        catch (IllegalArgumentException ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
        
            return null;
        }
        
    }
    
    /**
	 * 
	 * @param additionPK
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void delete(Entities.AdditionPK additionPK){
    
        try{
  

            additionFacadeREST.remove(new PathSegmentImpl("bar;idaddition="+additionPK.getIdaddition()+";franquiciaIdfranquicia="+additionPK.getFranquiciaIdfranquicia()+""));
        
            
        }
        catch(EJBException ex){
        
            Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException("Exception Caugth");
        
        }
    
    }
    
    /**
	 * 
	 * @param addition
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void edit(Addition addition){
    
        try{
            
         //   javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);

            System.out.print("ADDITION CONSUMES INVENTARIO SIZE "+addition.getAdditionConsumesInventarioCollection().size());
            
            if(this.find(addition.getAdditionPK())!=null){
            
            final Entities.Addition oldAddition=this.find(addition.getAdditionPK());     
                
            //Create New Additions && Update old ones
            
            if(addition.getAdditionConsumesInventarioCollection()!=null && !addition.getAdditionConsumesInventarioCollection().isEmpty()){
            
                for(Entities.AdditionConsumesInventario additionConsumesInventario:addition.getAdditionConsumesInventarioCollection()){
                
                boolean found=false;
                
                  if(oldAddition.getAdditionConsumesInventarioCollection()!=null && !addition.getAdditionConsumesInventarioCollection().isEmpty()){
                  
                      for(Entities.AdditionConsumesInventario aux:oldAddition.getAdditionConsumesInventarioCollection()){
                      
                         if(additionConsumesInventario.getAdditionConsumesInventarioPK().equals(aux.getAdditionConsumesInventarioPK())){
                         
                             found=true;
                         
                         } 
                      
                      }
                      
                      }
                  
                  if(!found){
                  
                      this._additionConsumesInventarioController.create(additionConsumesInventario);
                      
                      System.out.print("Addition "+additionConsumesInventario+" Created");
                  
                  }
                  else{
                  
                      this._additionConsumesInventarioController.edit(additionConsumesInventario);
                      
                         System.out.print("Addition "+additionConsumesInventario+" Updated");
                  
                  }
                  
                  }
 
                
                }
            
            System.out.print("New Addition Consumes Inventario Created");
            
            //Delete AdditionConsumes Inventario
            if(oldAddition.getAdditionConsumesInventarioCollection()!=null && !oldAddition.getAdditionConsumesInventarioCollection().isEmpty()){
            
                for(Entities.AdditionConsumesInventario additionConsumesInventario:oldAddition.getAdditionConsumesInventarioCollection()){
                
                boolean found=false;
                
                if(addition.getAdditionConsumesInventarioCollection()!=null && !addition.getAdditionConsumesInventarioCollection().isEmpty()){
                
                    for(Entities.AdditionConsumesInventario aux:addition.getAdditionConsumesInventarioCollection()){
                    
                        if(additionConsumesInventario.getAdditionConsumesInventarioPK().equals(aux.getAdditionConsumesInventarioPK())){
                        
                            found=true;
                        
                        }
                    
                    }
                
                }
                
                if(!found){
                
                    this._additionConsumesInventarioController.remove(additionConsumesInventario.getAdditionConsumesInventarioPK());
                
                }
                
                }
            
            }
            
            addition.setAdditionConsumesInventarioCollection(new java.util.ArrayList<Entities.AdditionConsumesInventario>());
            
            additionFacadeREST.edit(new PathSegmentImpl("bar;idaddition="+addition.getAdditionPK().getIdaddition()+";franquiciaIdfranquicia="+addition.getAdditionPK().getFranquiciaIdfranquicia()+""), addition);
        
            }
            
            else{
            
                System.out.print("ADDITION PK IS NULL");
            
            }

            }
        
        catch(Exception ex){
        
         if(ex instanceof javax.transaction.RollbackException){
         
             edit(addition);
         
         }
        
        }
    
    }
    
    public java.util.List<Entities.Addition>findBySucursal(Entities.SucursalPK sucursalPK){
    
    try{
    
        return this.additionFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Addition>findByFranchise(int idFranchise){
    
    try{
    
        java.util.Vector<Entities.Addition>results=new java.util.Vector<Entities.Addition>(this.additionFacadeREST.findByFranchise(idFranchise).size());
        
        for(Entities.Addition addition:this.additionFacadeREST.findByFranchise(idFranchise)){
        
        results.add(addition);
        
        }
        
        return results;
    
    }catch(Exception ex){
    
        Logger.getLogger(AdditionController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Addition>getAutoLoadAdditions(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                if(franchise.getAdditionCollection()!=null && !franchise.getAdditionCollection().isEmpty()){
                
                java.util.Vector<Entities.Addition>results=new java.util.Vector<Entities.Addition>(franchise.getAdditionCollection().size());
                
                for(Entities.Addition addition:franchise.getAdditionCollection()){
                
                    results.add(addition);
                
                }
                
                return results;
                }
            
            }
            
            return null;
        }
        catch(Exception ex){
        
            
            
        return null;
        }
    
    }
    
    public String getImage(Entities.AdditionPK additionPK){
    
        try{
        
         if(this.find(additionPK)!=null){
         
             Entities.Addition addition=this.find(additionPK);
         
             if(addition.getImagenCollection()!=null && !addition.getImagenCollection().isEmpty()){
                 
                 javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
                 javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                 
                 Entities.Imagen image=((java.util.List<Entities.Imagen>)addition.getImagenCollection()).get(0);
                 
                 BufferedImage bufferedImage=ImageIO.read(new ByteArrayInputStream(image.getImagen()));
                 
                 String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"additions";
             
                 Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                 
                 java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"additions");
                 
                 if(file.exists()){
                 
                     filesManagement.cleanFolder(path);
                 
                 }
                 else{
                 
                     filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator, "additions");
                 
                 }
                 
                 String ext=image.getExtension().replaceAll(".", "");
                 
                 BufferedImage outputImage=new BufferedImage(500,500,bufferedImage.getType());
                 
                 Graphics2D g2d=outputImage.createGraphics();
                 
                 g2d.drawImage(bufferedImage,0,0,500,500,null);
                 
                 g2d.dispose();
                 
                 ImageIO.write(outputImage, ext, new java.io.File(path,addition.getName()));
                 
             }
             
         }   
            
        return "/images/noImage.png";
        }
        catch(NullPointerException | IllegalArgumentException | java.io.IOException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return "/images/noImage.png";
        
        }
    
    }

	@Override
	public java.util.List<Entities.Addition> getAll() {
		
            return this.additionFacadeREST.findAll();
            
	}
    
}
