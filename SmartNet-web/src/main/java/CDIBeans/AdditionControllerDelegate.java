/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface AdditionControllerDelegate {
    
    void create(Entities.Addition addition);
    
    Entities.Addition findByName(String name,int franchiseId);
    
    java.util.Vector<Entities.Addition>findAll();
    
    void delete(Entities.AdditionPK additionPK);
    
    Entities.Addition find(Entities.AdditionPK additionPK);
    
    void edit(Entities.Addition addition);
    
    java.util.Vector<FranchiseAdministrator.Product.Classes.AdditionEden> loadImages(int idFranchise);
    
    java.util.Vector<Entities.Addition>findByFranchise(int idFranchise);
    
    java.util.Vector<Entities.Addition>getAutoLoadAdditions();
    
    String getImage(Entities.AdditionPK additionPK);

	java.util.List<Entities.Addition> getAll();
}
