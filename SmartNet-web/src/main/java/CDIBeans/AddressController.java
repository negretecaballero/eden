/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import jaxrs.service.DireccionFacadeREST;
import jaxrs.service.TypeAddressFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@AddressControllerQualifier
public class AddressController implements AdressControllerInterface{
    
   
   @EJB private TypeAddressFacadeREST tipoFacadeREST;
   
   @EJB private DireccionFacadeREST addressFacadeREST;
   
   @Inject 
   @CDIBeans.GPSCoordinatesControllerQualifier
   private CDIBeans.GpsCoordinatesControllerInterface gpsCoordinatesController;
   
   @Override
   public java.util.Vector<Entities.TypeAddress>getTipo(){
   
   try{
       
       java.util.Vector<Entities.TypeAddress>results=new java.util.Vector<Entities.TypeAddress>();
       
       for(Entities.TypeAddress typeAddress:tipoFacadeREST.findAll()){
       
       results.add(typeAddress);
       
       }
   
      return results;
      
      
   
   }
   catch(EJBException | NullPointerException ex){
   
       ex.printStackTrace(System.out);
       
       return null;
   
   }
   
   }
   
   /**
	 * 
	 * @param direccion
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
   public void delete(Entities.Direccion direccion){
   
   try{
   
       
   System.out.print("GPS ID TO DELETE "+direccion.getGpsCoordinatesidgpsCoordinated().getIdgpsCoordinated());
   
   if(this.accountsUsingAddress(direccion.getDireccionPK())==1)
   {
   this.gpsCoordinatesController.delete(direccion.getGpsCoordinatesidgpsCoordinated().getIdgpsCoordinated());
   }
   this.addressFacadeREST.remove(new PathSegmentImpl("bar;idDireccion="+direccion.getDireccionPK().getIdDireccion()+";cITYidCITY="+direccion.getDireccionPK().getCITYidCITY()+";cITYSTATEidSTATE="+direccion.getDireccionPK().getCITYSTATEidSTATE()+""));
   
   }
   catch(EJBException | NullPointerException ex){
   
   Logger.getLogger(AddressController.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   }
   
   @Override
   public int accountsUsingAddress(Entities.DireccionPK direccionPK){
   
       try{
       
           Entities.Direccion address=this.find(direccionPK);
           
           int k=0;
           
           if(address.getLoginAdministradorCollection()!=null && !address.getLoginAdministradorCollection().isEmpty())
           {
           k=k+address.getLoginAdministradorCollection().size();
           }
           
           if(address.getSucursalCollection()!=null && !address.getSucursalCollection().isEmpty()){
          k=k+address.getSucursalCollection().size();
           }
          return k;
       
       }
       catch(javax.ejb.EJBException ex){
       
       Logger.getLogger(AddressController.class.getName()).log(Level.SEVERE,null,ex);
       
       return 0;
       
       }
   
   }
    
   @Override
   public Entities.Direccion find(Entities.DireccionPK addressPK){
   
       try{
       
         return this.addressFacadeREST.find(new PathSegmentImpl("bar;idDireccion="+addressPK.getIdDireccion()+";cITYidCITY="+addressPK.getCITYidCITY()+";cITYSTATEidSTATE="+addressPK.getCITYSTATEidSTATE()+""));
       
         
       }
       catch(javax.ejb.EJBException | NullPointerException ex){
       
           Logger.getLogger(AddressController.class.getName()).log(Level.SEVERE,null,ex);
           
         return null;
       
       }
   
   }
   
   /**
	 * 
	 * @param address
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
   public void create(Entities.Direccion address){
   
   try{
   
       this.addressFacadeREST.create(address);
   
   }
   catch(javax.ejb.EJBException | NullPointerException ex){
   
   Logger.getLogger(AddressController.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
   }
   
   
   @Override
     public Entities.Direccion checkExistence(Entities.Direccion address){
    
        try{
            
            System.out.print("looking for address");
        
            System.out.print("Number of addresses "+this.addressFacadeREST.findAll().size());
            
            System.out.print("Address to Find "+address.getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+address.getPrimerDigito()+" #"+address.getSegundoDigito()+"-"+address.getTercerDigito()+" "+address.getAdicional());
            
            for(Entities.Direccion aux:this.addressFacadeREST.findAll()){
            
                System.out.print("Id Direccion "+aux.getDireccionPK().getIdDireccion());
                
                if(aux.getCity().getCityPK().getIdCITY()==address.getCity().getCityPK().getIdCITY() && aux.getCity().getCityPK().getSTATEidSTATE()==address.getCity().getCityPK().getSTATEidSTATE() && aux.getTYPEADDRESSidTYPEADDRESS().getIdTYPEADDRESS().intValue()==address.getTYPEADDRESSidTYPEADDRESS().getIdTYPEADDRESS().intValue() && aux.getPrimerDigito().equals(address.getPrimerDigito()) && aux.getSegundoDigito().equals(address.getSegundoDigito()) && aux.getTercerDigito()==address.getTercerDigito() && aux.getAdicional().equals(address.getAdicional())){
                
                System.out.print("Address Found");
                
                return aux;
               
                }
                
                else{
                
                    System.out.print("Addresses do not match");
                
                }
            
            }
        
            System.out.print("Adress not found");
            return null;
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
        Logger.getLogger(AddressController.class.getName()).log(Level.SEVERE,null,ex);
        
      return null;
        
        }
    
    }
     
   
}
