/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import jaxrs.service.TypeAddressFacadeREST;

/**
 *
 * @author luisnegrete
 */



@javax.enterprise.context.Dependent
@AddressTypeControllerQualifier

public class AddressTypeController  implements AddressTypeControllerInterface{
 
    
    @EJB private TypeAddressFacadeREST typeAddressFacadeAddress;
    
    @Override
    public Entities.TypeAddress findByname(String type){
    
    try{
    
       return this.typeAddressFacadeAddress.findByType(type);
        
    }
    catch(EJBException ex){
    
        Logger.getLogger(AddressTypeController.class.getName()).log(Level.SEVERE,null,ex);
        
        return null;
        
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.TypeAddress>findAll(){
    
    try{
    
        java.util.Vector<Entities.TypeAddress>results=new java.util.Vector<Entities.TypeAddress>(this.typeAddressFacadeAddress.findAll().size());
        
        for(Entities.TypeAddress typeAddress:this.typeAddressFacadeAddress.findAll()){
        
            results.add(typeAddress);
        
        }
        
        
        return results;
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(AddressTypeController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
}
