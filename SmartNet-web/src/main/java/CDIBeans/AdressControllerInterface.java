/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface AdressControllerInterface {
    
    java.util.Vector<Entities.TypeAddress>getTipo();
   
    void delete(Entities.Direccion direccion);
    
    Entities.Direccion find(Entities.DireccionPK addressPK);
    
    void create(Entities.Direccion address);
    
    Entities.Direccion checkExistence(Entities.Direccion address);
    
    int accountsUsingAddress(Entities.DireccionPK direccionPK);
}
