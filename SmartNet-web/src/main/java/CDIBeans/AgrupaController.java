/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import jaxrs.service.AgrupaFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import javax.enterprise.context.Dependent;
/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@AgrupaControllerQualifier
public class AgrupaController implements AgrupaControllerInterface{
 
    @EJB
    private AgrupaFacadeREST agrupaFacadeREST;
    
    @Override
    public java.util.Vector<Entities.Agrupa>findBySucursal(Entities.SucursalPK sucursalPK){
    
    try{
    
        java.util.Vector<Entities.Agrupa>results=new java.util.Vector<Entities.Agrupa>(agrupaFacadeREST.findByIdSucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+"")).size());
        
       
        for(Entities.Agrupa agrupa:agrupaFacadeREST.findByIdSucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+"")))
        {
        
            results.add(agrupa);
        
        }
        
        
      return results; 
        
    }
    catch(Exception ex){
    
        Logger.getLogger(AgrupaController.class.getName()).log(Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Agrupa>findByFranchise(int franchiseId){
    
    try{
    
        java.util.Vector<Entities.Agrupa>results=new java.util.Vector<Entities.Agrupa>(this.agrupaFacadeREST.findByFranchise(franchiseId).size());
        
        System.out.print("AGRUPA SIZE "+this.agrupaFacadeREST.findByFranchise(franchiseId).size()+ " ID FRANCHISE "+franchiseId);
        
        if(this.agrupaFacadeREST.findByFranchise(franchiseId)!=null && !this.agrupaFacadeREST.findByFranchise(franchiseId).isEmpty()){
        
            
            
        for(Entities.Agrupa agrupa:this.agrupaFacadeREST.findByFranchise(franchiseId)){
        
            results.add(agrupa);
        
        }
        
        }
        
    return results;
    
    }
    catch(javax.ejb.EJBException ex){
        
        Logger.getLogger(AgrupaController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    }
    
    }
    
    
    @Override
    public void delete(Entities.AgrupaPK key){
    
    try{
        
        this.agrupaFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";menuIdmenu="+key.getMenuIdmenu()+""));
    
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Agrupa>findByProduct(Entities.ProductoPK key){
    
    try{
        
        java.util.Vector<Entities.Agrupa>results=new java.util.Vector<Entities.Agrupa>(this.agrupaFacadeREST.findByProduct(new PathSegmentImpl("bar;idproducto="+key.getIdproducto()+";categoriaIdcategoria="+key.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+key.getCategoriaFranquiciaIdfranquicia()+"")).size());
    
        for(Entities.Agrupa agrupa:this.agrupaFacadeREST.findByProduct(new PathSegmentImpl("bar;idproducto="+key.getIdproducto()+";categoriaIdcategoria="+key.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+key.getCategoriaFranquiciaIdfranquicia()+"")))
        {
        
            results.add(agrupa);
        
        }
        return results;
        
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    @Override
    public void create(Entities.Agrupa agrupa){
    
        try{
        
            this.agrupaFacadeREST.create(agrupa);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
        
    }
    
    @Override
    public void update(Entities.Agrupa agrupa){
    
        try{
        
            this.agrupaFacadeREST.edit(new PathSegmentImpl("bar;idproducto="+agrupa.getAgrupaPK().getProductoIdproducto()+";categoriaIdcategoria="+agrupa.getAgrupaPK().getProductoCategoriaIdCategoria()+";categoriaFranquiciaIdfranquicia="+agrupa.getAgrupaPK().getProductoCategoriaFranquiciaIdFranquicia()+";menuIdmenu="+agrupa.getAgrupaPK().getMenuIdmenu()+""), agrupa);
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
