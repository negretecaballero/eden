/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface AgrupaControllerInterface {
    
    java.util.Vector<Entities.Agrupa>findBySucursal(Entities.SucursalPK sucursalPK);
    
    java.util.Vector<Entities.Agrupa>findByFranchise(int franchiseId);
    
   java.util.Vector<Entities.Agrupa> findByProduct(Entities.ProductoPK key);
   
   void delete(Entities.AgrupaPK key);
   
   void create(Entities.Agrupa agrupa);
   
   void update(Entities.Agrupa agrupa);
   
}
