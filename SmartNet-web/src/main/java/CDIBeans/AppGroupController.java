/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.AppGroup;
import Entities.AppGroupPK;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.transaction.Transactional.TxType;
import jaxrs.service.AppGroupFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@AppGroupControllerQualifier



public class AppGroupController implements AppGroupControllerDelegate,java.io.Serializable{
   
    @EJB
    private AppGroupFacadeREST appGroupFacadeREST;
    
    /**
	 * 
	 * @param appGroup
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(AppGroup appGroup){
    
        try{
           
        if(this.find(appGroup.getAppGroupPK())==null){
            
        appGroupFacadeREST.create(appGroup);
            
            }
        }
        catch(EJBException | NullPointerException ex){
        
         ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public AppGroup find(AppGroupPK appGroupPK){
    
        try{
        
           return appGroupFacadeREST.find(new PathSegmentImpl("bar;groupid="+appGroupPK.getGroupid()+";loginAdministradorusername="+appGroupPK.getLoginAdministradorusername()+""));
        
        }
        catch(EJBException | NullPointerException ex){
        
            return null;
        
        }
    
    
    }
    
    @Override
    public java.util.Vector<Entities.AppGroup>findByUsername(String username){
    
    try{
    
    java.util.Vector<Entities.AppGroup>results=new java.util.Vector<Entities.AppGroup>(this.appGroupFacadeREST.findByName(username).size());
    
    for(Entities.AppGroup appGroup:this.appGroupFacadeREST.findByName(username)){
    
        results.add(appGroup);
    
    }
        
    return results;
    
    }catch(EJBException | NullPointerException ex){
    
        Logger.getLogger(AppGroupController.class.getName()).log(Level.SEVERE,null,ex);
        
     return null;
    
    }
    
    
    }
    
    /**
	 * 
	 * @param type
	 * @param username
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void delete(String type, String username){
    
        try{
        
            this.appGroupFacadeREST.remove(new PathSegmentImpl("bar;groupid="+type+";loginAdministradorusername="+username+""));
        
        }
        catch(EJBException | NullPointerException ex){
        
            Logger.getLogger(AppGroupController.class.getName()).log(Level.SEVERE,null,ex);
            
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    @Override
    public boolean exists(String username,String appGroupName){
    
        try{
        
            Entities.AppGroupPK key=new Entities.AppGroupPK();
            
            key.setGroupid(appGroupName);
            
            key.setLoginAdministradorusername(username);
            
            return this.find(key)!=null;
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
       
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return false;
        
        }
    
    }
    
}
