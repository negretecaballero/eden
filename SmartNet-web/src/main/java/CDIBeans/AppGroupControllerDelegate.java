/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.AppGroup;

/**
 *
 * @author luisnegrete
 */
public interface AppGroupControllerDelegate {
    
    void create(AppGroup appGroup);
    
    Entities.AppGroup find(Entities.AppGroupPK appGroupPK);
    
    java.util.Vector<Entities.AppGroup>findByUsername(String username);
    
    void delete(String type, String username);
    
    boolean exists(String username,String appGroupName);
}
