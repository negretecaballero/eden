/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface ApplicationController {
    
    void create(Entities.Application application);
    
    Entities.Application find(String mail);
    
    Entities.Application findByNit(String nit);
    
    java.util.Vector<Entities.Application>findAll();
    
    void delete(Entities.ApplicationPK key);
    
    Entities.Application findBYKey(Entities.ApplicationPK key);
    
    void update(Entities.Application application);
    
    java.util.Vector<Entities.Application>findByStatus(Entitites.Enum.ApplicationStatusENUM status);

	/**
	 * 
	 * @param mail
	 */
	java.util.List<Entities.Application> findByMail(String mail);
}
