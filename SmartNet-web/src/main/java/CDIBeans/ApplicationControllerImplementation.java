/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.transaction.Transactional.TxType;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.context.Dependent

@ApplicationControllerQualifier

public class ApplicationControllerImplementation implements ApplicationController{
    
    @javax.ejb.EJB
    private jaxrs.service.ApplicationFacadeREST applicationFacadeREST;

    
    
    /**
	 * 
	 * @param application
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.Application application){
    
        try{
       
            this.applicationFacadeREST.create(application);
            
        }
        catch(Exception ex){
        
        if(ex instanceof javax.transaction.RollbackException){
        
            create(application);
        
        }
            
        }
    
    }
    
    @Override
    public Entities.Application find(String mail){
    
        try{
        
            return this.applicationFacadeREST.find(mail);
            
        }
        catch(javax.ejb.EJBException | NullPointerException |  IllegalArgumentException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public Entities.Application findByNit(String nit){
    
    try{
    
        return this.applicationFacadeREST.findByNit(nit);
        
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
        
        
    
    }
    
      
    }
    
    @Override
    public java.util.Vector<Entities.Application>findAll(){
    
        try{
            
            java.util.Vector<Entities.Application>results=new java.util.Vector<Entities.Application>(this.applicationFacadeREST.findAll().size());
        
            for(Entities.Application application:this.applicationFacadeREST.findAll()){
            
                results.add(application);
            
            }
            
            return results;
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            throw new javax.faces.FacesException(ex);
        }
    
    }
    
    @Override
    
    public void delete(Entities.ApplicationPK key){
    
    try{
    
        this.applicationFacadeREST.remove(new PathSegmentImpl("bar;cedula="+key.getCedula()+";nit="+key.getNit()+""));
    
    }
    catch(javax.ejb.EJBException  | StackOverflowError ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public Entities.Application findBYKey(Entities.ApplicationPK key){
    
    try{
    
        return this.applicationFacadeREST.find(new PathSegmentImpl("bar;cedula="+key.getCedula()+";nit="+key.getNit()+""));
    
    }
    catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void update(Entities.Application application){
    
        try{
        
            applicationFacadeREST.edit(new PathSegmentImpl("bar;cedula="+application.getApplicationPK().getCedula()+";nit="+application.getApplicationPK().getNit()+""),application);
        
        }catch(Exception ex){
    
            ex.printStackTrace(System.out);
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Application>findByStatus(Entitites.Enum.ApplicationStatusENUM status){
    
        try{
        
            java.util.Vector<Entities.Application>result=new java.util.Vector<Entities.Application>();
            
            if(this.applicationFacadeREST.findByStatus(status)!=null && !this.applicationFacadeREST.findByStatus(status).isEmpty())
            {
            
            for(Entities.Application application:this.applicationFacadeREST.findByStatus(status))
            {
            
                result.add(application);
            
            }
            
        }
            return result;
            
        }
        catch(NullPointerException | javax.ejb.EJBException ex){
        
            ex.printStackTrace(System.out);
            
        return null;
        
        }
    
    }

	/**
	 * 
	 * @param mail
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	public java.util.List<Entities.Application> findByMail(String mail) {
		
            try{
        
                return this.applicationFacadeREST.findByMail(mail);
        
            }
            catch(NullPointerException ex){
                    
                ex.printStackTrace(System.out);
                    
                return null;
                    
                    }
            
	}
}
