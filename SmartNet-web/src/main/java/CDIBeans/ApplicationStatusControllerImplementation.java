/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entitites.Enum.ApplicationStatusENUM;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@ApplicationStatusControllerQualifier

public class ApplicationStatusControllerImplementation implements ApplicationStatusController {
    
    @javax.ejb.EJB
    private jaxrs.service.ApplicationStatusFacadeREST _applicationStatusFacadeREST;
    
    @Override
    public Entities.ApplicationStatus findByStatus(Entitites.Enum.ApplicationStatusENUM status){
    
        try{
        
            return _applicationStatusFacadeREST.findByStatus(status);
        
        }
        catch(NullPointerException | javax.ejb.EJBException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
 
    
}
