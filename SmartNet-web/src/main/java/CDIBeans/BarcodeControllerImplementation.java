/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@BarcodeControllerQualifier

public class BarcodeControllerImplementation implements BarcodeController{
    
    @javax.ejb.EJB
    private jaxrs.service.BarcodeFacadeREST barcodeFacadeREST;
    
    @Override
    public Entities.Barcode getBarcodeByCode(String code){
    
        try{
        
            return this.barcodeFacadeREST.findByBarcode(code);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public void createBarcode(Entities.Barcode barcode){
    
        try{
        
            this.barcodeFacadeREST.create(barcode);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
}
