/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@BugControllerQualifier

public class BugControllerImplementation implements BugController {
    
    @javax.ejb.EJB
    private jaxrs.service.BugFacadeREST bugFacadeREST;
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @Override
    public void create(String comments){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("username")!=null){
            
            String username=session.getAttribute("username").toString();
                
            if(_loginAdministradorController.find(username)!=null)
            {
            java.util.Calendar cal=new GregorianCalendar(TimeZone.getTimeZone("America/Bogota"));
            
            Entities.Bug bug=new Entities.Bug();
            
            java.util.Date date=new java.util.Date();
            
            date.setYear(cal.get(java.util.Calendar.YEAR));
            
            date.setMonth(cal.get(java.util.Calendar.MONTH));
            
            date.setDate(cal.get(java.util.Calendar.DAY_OF_MONTH));
            
            date.setHours(cal.get(java.util.Calendar.HOUR_OF_DAY));
            
            date.setMinutes(cal.get(java.util.Calendar.MINUTE));
            
            date.setSeconds(cal.get(java.util.Calendar.SECOND));
            
            SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 
            System.out.print("DATE "+  format.format(cal.getTime()));
            
            bug.setDate(format.parse(format.format(cal.getTime())));
            
            bug.setIdLoginAdministrador(_loginAdministradorController.find(username));
            
            bug.setComments(comments);
            
            System.out.print(bug.getIdLoginAdministrador().getUsername()+" "+bug.getDate()+" "+bug.getComments());
            
            bugFacadeREST.create(bug);
            
            }
            
            }
            
        }
        catch(javax.ejb.EJBException | ParseException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    
}
