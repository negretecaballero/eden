/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.enterprise.inject.Produces;


/**
 *
 * @author luisnegrete
 */
public class CDIProducer {
    
    
    
    @Produces
    @AdditionControllerQualifier
    public CDIBeans.AdditionControllerDelegate additionControllerFactory(){
    
    return new CDIBeans.AdditionController();
    
    }
    
   
    @Produces
    @FileUploadBeanQualifier
    public CDIBeans.FileUploadInterface fileUploadFactory(){
    
    return new CDIBeans.FileUploadBean();
    
    }
    
    @javax.enterprise.inject.Produces
    @AdditionConsumesInventarioControllerQualifier
    public CDIBeans.AdditionConsumesInventarioController additionConsumesInventarioControllerFactory(){

            return new CDIBeans.AdditionConsumesInventarioController();

    }
    
    @javax.enterprise.inject.Produces
    @AddressControllerQualifier
    public CDIBeans.AdressControllerInterface addressControllerFactory(){
    
        return new CDIBeans.AddressController();
    
    }

    @javax.enterprise.inject.Produces
    @AddressTypeControllerQualifier
    public AddressTypeControllerInterface addressTypeControllerFactory(){
    
    return new CDIBeans.AddressTypeController();
    
    }
    
    
    @javax.enterprise.inject.Produces
    @AgrupaControllerQualifier
    public AgrupaControllerInterface agrupaControllerFactory(){
    
        return new CDIBeans.AgrupaController();
    
    }
    
    @javax.enterprise.inject.Produces
    @AppGroupControllerQualifier
    public CDIBeans.AppGroupControllerDelegate appGroupControllerFactory(){
    
        return new CDIBeans.AppGroupController();
    
    }
    
    @javax.enterprise.inject.Produces
    @ApplicationControllerQualifier
    public ApplicationController applicationControllerFactory(){
            
     return new CDIBeans.ApplicationControllerImplementation();        
            
    }
    
    @javax.enterprise.inject.Produces
    @ApplicationStatusControllerQualifier
    public ApplicationStatusController applicationStatusControllerFactory(){
    
        return new CDIBeans.ApplicationStatusControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @BarcodeControllerQualifier
    public BarcodeController barcodeControllerFactory(){
    
        return new CDIBeans.BarcodeControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @BugControllerQualifier
    public BugController bugControllerFactory(){
    
        return new CDIBeans.BugControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @CategoryControllerQualifier
    public CategoryControllerInterface categoryControllerFactory(){
    
    return new CDIBeans.CategoryController();
    
    }
    
    @javax.enterprise.inject.Produces
    @ComponeControllerQualifier
    public ComponeController componeControllerFactory(){
    
        return new CDIBeans.ComponeControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @ConfirmationControllerQualifier
    public ConfirmationDelegate confirmationControllerFactory(){
    
        return new ConfirmationController();
    
    }
    
    @javax.enterprise.inject.Produces
    @DayControllerQualifier
    public DayController dayControllerFactory(){
    
        return new CDIBeans.DayControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableCategoriaQualifier
    public DesplegableCategoriaInterface desplegableCategoriaFactory(){
    
        return new CDIBeans.DesplegableCategoria();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableFranchiseQualifier
    public DesplegableFranchiseInterface desplegableFranchiseFactory(){
    
        return new CDIBeans.DesplegableFranchise();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableInventarioQualifier
    public DesplegableInventarioInterface desplegableInventarioFactory(){
    
    return new CDIBeans.DesplegableInventario();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableMenuQualifier
    public DesplegableMenuInterface desplegableMenuFactory(){
    
        return new CDIBeans.DesplegableMenu();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableProfessionQualifier
    public DesplegableProfessionInterface desplegableProfessionFactory(){
    
        return new CDIBeans.DesplegableProfession();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegableStateQualifier
    public DesplegableStateInterface desplegableStateFactory(){
    
        return new CDIBeans.DesplegableState();
    
    }
   
    @javax.enterprise.inject.Produces
    @FileControllerQualifier
    public FileController fileControllerFactory(){
    
        return new CDIBeans.FileControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @FranchiseControllerQualifier
    public FranchiseControllerInterface franchiseControllerFactory(){
    
        return new CDIBeans.FranchiseController();
    
    }
    
    
    @javax.enterprise.inject.Produces
    @FranchiseTypeControllerQualifier
    public FranchiseTypeControllerInterface franchiseTypeControllerFactory(){
    
        return new CDIBeans.FranchiseTypeController();
    
    }
    
    @javax.enterprise.inject.Produces
    @GPSCoordinatesControllerQualifier
    public GpsCoordinatesControllerInterface gpsCoordinatesControllerFactory(){
    
        return new CDIBeans.GpsCoordinatesController();
    
    }
    
    @javax.enterprise.inject.Produces
    @ImageControllerQualifier
    public ImageControllerDelegate imageControllerFactory(){
    
    return new ImageController();
    
    }
    
    @javax.enterprise.inject.Produces
    @InventarioControllerQualifier
    public InventarioDelegate inventarioControllerFactory(){
    
        return new CDIBeans.InventarioController();
    
    }
    
    @javax.enterprise.inject.Produces
    @InventarioHasSucursalControllerQualifier
    public InventarioHasSucursal inventarioHasSucursalControllerFactory(){
    
        return new CDIBeans.InventarioHasSucursalImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @LastSeenControllerQualifier
    public LastSeenControllerInterface lastSeenControllerFactory(){
    
        return new CDIBeans.LastSeenController();
    
    }
    
    @javax.enterprise.inject.Produces
    @LastSeenProductoControllerQualifier
    public LastSeenProductoController lastSeenProductoControllerFactory(){
    
        return new CDIBeans.LastSeenProductoControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @LoginAdministradorControllerQualifier
    public LoginAdministradorController loginAdministradorControllerFactory(){
    
        return new CDIBeans.LoginAdministradorController();
    
    }
    
    @javax.enterprise.inject.Produces
    @MenuControllerQualifier
    public MenuController menuControllerFactory(){
    
        return new CDIBeans.MenuController();
    
    }
    
    @javax.enterprise.inject.Produces
    @NewsControllerQualifier
    public NewsController newsControllerFactory(){
     
        return new NewsControllerImplementation();
        
       }
    
    
    @javax.enterprise.inject.Produces
    @PartnershipRequestControllerQualifier
    public CDIBeans.PartnershipRequestController partnershipRequestControllerFactory(){
    
        return new CDIBeans.PartnershipRequestControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @PedidoControllerQualifier
    public  PedidoControllerInterface pedidoControllerFactory(){
    
    return new CDIBeans.PedidoController();
    
    }
    
    @javax.enterprise.inject.Produces
    @PedidoStateControllerQualifier
    public PedidoStateControllerInterface pedidoStateControllerFactory(){
    
        return new CDIBeans.PedidoStateController();
    
    }
    
    @javax.enterprise.inject.Produces
    @PhoneControllerQualifier
    public PhoneControllerInterface phoneControllerFacory(){
    
        return new CDIBeans.PhoneController();
    
    }
    
    @javax.enterprise.inject.Produces
    @ProductoControllerQualifier
    public ProductoControllerInterface productoControllerFactory(){
    
    return new CDIBeans.ProductoController();
    
    }
    
    @javax.enterprise.inject.Produces
    @ProductoHasPedidoControllerQualifier
    public ProductoHasPedidoController productoHasPedidoControllerFactory(){
    
    return new CDIBeans.ProductoHasPedidoControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @ProfessionControllerQualifier
    public ProfessionControllerInterface professionControllerFactory(){
    
    return new CDIBeans.ProfessionController();
    
    }
    
    @javax.enterprise.inject.Produces
    @QualificationVariableControllerQualifier
    public QualificationVariableController qualificationVariableControllerFactory(){
    
        return new CDIBeans.QualificationVariableControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @QualificationVariableHasRateControllerQualifier
    public QualificationVariableHasRateController qualificationVariableHasRateControllerFactory(){
    
    return new CDIBeans.QualificationVariableHasRateControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @RateControllerQualifier
    public RateControllerInterface rateControllerFactory(){
    
        return new CDIBeans.RateController();
    
    }
    
    @javax.enterprise.inject.Produces
    @CDIBeans.RatehasPEDIDOControllerQualifier
    public RatehasPEDIDOControllerInterface ratehasPEDIDOControllerFactory(){
    
        return new CDIBeans.RatehasPEDIDOController();
    
    }
    
    @javax.enterprise.inject.Produces
    @CDIBeans.SaleControllerQualifier
    public SaleControllerInterface saleControllerFactory(){
    
        return new CDIBeans.SaleController();
    
    }
    
    @javax.enterprise.inject.Produces
    @CDIBeans.SaleHasMenuControllerQualifier
    public SaleHasMenuControllerInterface saleHasMenuControllerFactory(){
    
    return new CDIBeans.SaleHasMenuController();
    
    }
    
    @javax.enterprise.inject.Produces
    @SaleHasProductoControllerQualifier
    public SaleHasProductoControllerInterface saleHasProductoControllerFactory(){
    
    return new CDIBeans.SaleHasProductoController();
    
    }
    
    @javax.enterprise.inject.Produces
    @SimilarInventarioControllerQualifier
    public SimilarInventarioController similarInventarioControllerFactory(){
    
        return new CDIBeans.SimilarInventarioControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SimilarProductsControllerQualifier
    public SimilarProductsController similarProductsControllerFactory(){
    
        return new CDIBeans.SimilarProductsControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @StateControllerQualifier
    
    public StateController stateControllerFactory(){
    
        return new CDIBeans.StateController();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @SubtypeControllerQualifier
    
    public SubtypeController subtypeControllerFactory(){
    
        return new CDIBeans.SubtypeControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SucursalControllerQualifier
    public SucursalControllerDelegate sucursalControllerFactory()
            {
            
                return new CDIBeans.SucursalController();
            
            }
    
    @javax.enterprise.inject.Produces
    @SucursalHasDayControllerQualifier
    public SucursalHasDayController sucursalHasDayControllerFactory(){
    
        return new CDIBeans.SucursalHasDayControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @SupplierApplicationControllerQualifier
    
    public SupplierApplicationController supplierApplicationControllerFactory(){
    
    return new CDIBeans.SupplierApplicationControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @SupplierControllerQualifier
    
    public SupplierController supplierControllerFactory(){
    
        return new CDIBeans.SupplierControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SupplierProductCategoryControllerQualifier
    public SupplierProductCategoryController supplierProductCategoryControllerFactory(){
    
        return new CDIBeans.SupplierProductCategoryControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SupplierProductTypeControllerQualifier
    public SupplierProductTypeController supplierProductTypeController(){
    
        return new CDIBeans.SupplierProductTypeControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SupplierTypeControllerQualifier
    @javax.enterprise.context.Dependent
    public SupplierTypeController supplierTypeControllerFactory(){
    
        return new CDIBeans.SupplierTypeControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @TipoControllerQualifier
    public TipoController tipoControllerFactory(){
    
        return new CDIBeans.TipoControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @DesplegablesProductoQualifier
    public desplegablesProducto desplegablesProductoFactory(){
    
        return new CDIBeans.desplegablesProducto();
    
    }
    
    
   
}
