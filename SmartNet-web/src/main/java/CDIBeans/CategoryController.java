/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import jaxrs.service.CategoriaFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import Clases.*;
import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */

@CategoryControllerQualifier
@Stereotypes.DependentStereotype
@javax.inject.Named("categoryController")

public class CategoryController extends BaseBacking implements  CategoryControllerInterface{

 @javax.ejb.EJB 
 private CategoriaFacadeREST categoryFacadeRest;
 @javax.inject.Inject 
 @CDIBeans.FileUploadBeanQualifier
 private CDIBeans.FileUploadInterface fileUpload;
 
 @Override
 public Entities.Categoria find(Entities.CategoriaPK categoriaPK){
 
 try{
 
     return this.categoryFacadeRest.find(new PathSegmentImpl("bar;idcategoria="+categoriaPK.getIdcategoria()+";franquiciaIdfranquicia="+categoriaPK.getFranquiciaIdfranquicia()+""));
 
 }
 catch(javax.ejb.EJBException ex){
 
     Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE,null,ex);
     
     throw new javax.faces.FacesException(ex);
 
 }
 
 }
 
 
 @Override
 public java.util.Vector<Entities.Categoria>findAllFranchise(int idFranchise){
 
     try{
     
         java.util.Vector<Entities.Categoria>categoryArray=new java.util.Vector<Entities.Categoria>(this.categoryFacadeRest.findByFranchise(idFranchise).size());
     
         for(Entities.Categoria category:this.categoryFacadeRest.findByFranchise(idFranchise)){
         
             categoryArray.add(category);
         
         }
         
        
         return categoryArray;
         
     }
     catch(javax.ejb.EJBException | StackOverflowError ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         return null;
     
     }
 
 }
 
 @Override
 public void loadImages(String username,Entities.CategoriaPK key){
 
     try{
     
    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
     
    this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
        
    for(Entities.Imagen image:this.find(key).getImagenCollection()){
         
         
             this.fileUpload.loadImagesServletContext(image, username);
         
         }
     
     }
     catch(Exception ex){
     
         Logger.getLogger(CategoryController.class.getName()).log(Level.SEVERE,null,ex);
         
         throw new javax.faces.FacesException(ex);
     
     }
 
 }
 
 
 /**
	 * 
	 * @param key
	 */
	@Override
	@annotations.MethodAnnotations(author="Luis Negrete", date="18/08/2015", comments="Delete Category")
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
 public void delete(Entities.CategoriaPK key){
 
     try{
     
         this.categoryFacadeRest.remove(new PathSegmentImpl("bar;idcategoria="+key.getIdcategoria()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+""));
     
     }
     catch(javax.ejb.EJBException | NullPointerException ex){
     
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
     throw new javax.faces.FacesException(ex);
     
     }
 
 }
    
 /**
	 * 
	 * @param category
	 */
	@Override
	@annotations.MethodAnnotations(author="Luis Negrete", date="19/08/2015", comments="Update Category")
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
     public void update(Entities.Categoria category){
    
    try{
    
       this.categoryFacadeRest.edit(new PathSegmentImpl("bar;idcategoria="+category.getCategoriaPK().getIdcategoria()+";franquiciaIdfranquicia="+category.getCategoriaPK().getFranquiciaIdfranquicia()+""),category);
    
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
     
     
     @Override
     @annotations.MethodAnnotations(author="Luis Negrete",date="28/08/2015",comments="Find Category By Category Converter result")
     public Entities.Categoria findCategoryByConverter(String key){
     
     try{
     
         if(key.split(",").length==2){
         
             Entities.CategoriaPK pk=new Entities.CategoriaPK();
             
             pk.setIdcategoria(new java.lang.Integer(key.split(",")[0]));
             
             pk.setFranquiciaIdfranquicia(new java.lang.Integer(key.split(",")[1]));
             
             return this.find(pk);
             
         }
         else{
         
             throw new IllegalArgumentException("Wrong CategoryPK type argument");
         
         }
     
     }
     catch(javax.ejb.EJBException  | IllegalArgumentException | StackOverflowError ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         throw new javax.faces.FacesException(ex);
     
     
     }
     
     }
     
     
     /**
	 * 
	 * @param category
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
     public void create(Entities.Categoria category){
     
         try{
         
             this.categoryFacadeRest.create(category);
         
         }
         catch(javax.ejb.EJBException | NullPointerException ex){
         
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         }
     
     }
     
     @Override
     public java.util.Vector<Entities.Categoria>getAutoCategories(){
     
         try{
         
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
             
             if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
             
                 Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
             
                 if(franchise.getCategoriaCollection()!=null && !franchise.getCategoriaCollection().isEmpty()){
                 
                     java.util.Vector<Entities.Categoria>results=new java.util.Vector<Entities.Categoria>(franchise.getCategoriaCollection().size());
                     
                 for(Entities.Categoria category:franchise.getCategoriaCollection()){
                 
                     results.add(category);
                 
                 }
                 
                 return results;
                 
                 }
                 
                 
             }
             
         return null;
         }
         catch(Exception ex){
         
         return null;
         }
     
     }

	@Override
	public java.util.List<Entities.Categoria> findAll() {
		
            try{
            
                return this.categoryFacadeRest.findAll();
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
                return null;
            }
            
	}
 
}
