/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface CategoryControllerInterface {
    
  public Entities.Categoria find(Entities.CategoriaPK categoriaPK);
  
  void loadImages(String username,Entities.CategoriaPK key);
  
  void delete(Entities.CategoriaPK key);
  
  void update(Entities.Categoria category);
    
 Entities.Categoria findCategoryByConverter(String key);
 
 void create(Entities.Categoria category);
  
 java.util.Vector<Entities.Categoria>findAllFranchise(int idFranchise);
 
 java.util.Vector<Entities.Categoria>getAutoCategories();

	java.util.List<Entities.Categoria> findAll();
 
}
