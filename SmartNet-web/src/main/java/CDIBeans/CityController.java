/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@CityControllerQualifier
public class CityController implements CityControllerInterface{
    
    
    @javax.ejb.EJB
    private jaxrs.service.CityFacadeREST cityFacadeREST;
    
    @Override
    public Entities.City findByState(String cityName,Entities.State state){
    
        try{
        
                for(Entities.City city:state.getCityCollection()){
                
                
                if(cityName.equals(city.getName())){
                
                
                return city;
                
                }
                
                }
                
                return null;
        
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(CityController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
        
    
    }
    
    @Override
    public Entities.City find(Entities.CityPK key){
    
        try{
        
            return this.cityFacadeREST.find(new PathSegmentImpl("bar;idCITY="+key.getIdCITY()+";sTATEidSTATE="+key.getSTATEidSTATE()+""));
            
        }
        catch(javax.ejb.EJBException ex){
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            throw new javax.faces.FacesException(ex);
            
        
        }
    
    }
    
    @Override
    public Entities.CityPK convertToKey(String key){
    
        try{
            
            System.out.print("STRING TO CONVERT TO CIYY");
        
            if(key.split(",").length==2){
            
                Entities.CityPK cityPK=new Entities.CityPK();
                
                cityPK.setSTATEidSTATE(new java.lang.Integer(key.split(",")[0]));
                
                cityPK.setIdCITY(new java.lang.Integer(key.split(",")[1]));
                
                return cityPK;
                
            }
            
            return null;
        }
        catch(NumberFormatException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public Entities.City findWithinState(Entities.State state,String cityName){
    
    try{
    
        for(Entities.City city:state.getCityCollection()){
        
            if(cityName.equals(city.getName())){
                
                return city;
                
            }
        
        }
        
        return null;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
}
