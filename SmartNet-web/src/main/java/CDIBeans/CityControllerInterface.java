/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface CityControllerInterface {
  
    Entities.City findByState(String cityName, Entities.State state);
    
    Entities.City find(Entities.CityPK key);
    
    Entities.CityPK convertToKey(String key);
    
    Entities.City findWithinState(Entities.State state,String cityName);
}
