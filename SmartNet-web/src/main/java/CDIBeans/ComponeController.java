/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface ComponeController {
    
   java.util.Vector<Entities.Compone>findByProducto(Entities.ProductoPK productPK);
   
   void delete(Entities.ComponePK key);
   
   Entities.Compone find(Entities.ComponePK key);
   
   void create(Entities.Compone compone);
    
}
