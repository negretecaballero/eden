/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@ComponeControllerQualifier
public class ComponeControllerImplementation implements ComponeController {
    
    @javax.ejb.EJB
    private jaxrs.service.ComponeFacadeREST componeFacadeREST;
    
    @Override
    public Entities.Compone find(Entities.ComponePK key){
    
        try{
        
           return this.componeFacadeREST.find(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+key.getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+key.getInventarioFranquiciaIdFranquicia()+""));
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Compone>findByProducto(Entities.ProductoPK productPK){
    
    try{
    
        java.util.Vector<Entities.Compone>results=new java.util.Vector<Entities.Compone>(this.componeFacadeREST.findIdProducto(new PathSegmentImpl("bar;idproducto="+productPK.getIdproducto()+";categoriaIdcategoria="+productPK.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+productPK.getCategoriaFranquiciaIdfranquicia()+"")).size());
        
        
        for(Entities.Compone compone:this.componeFacadeREST.findIdProducto(new PathSegmentImpl("bar;idproducto="+productPK.getIdproducto()+";categoriaIdcategoria="+productPK.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+productPK.getCategoriaFranquiciaIdfranquicia()+""))){
        
        results.add(compone);
        
        }
        
     return results; 
    }
    
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void delete(Entities.ComponePK key){
    
        try{
        
            this.componeFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+key.getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+key.getInventarioFranquiciaIdFranquicia()+""));
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    @Override
    public void create(Entities.Compone compone){
    
        try{
        
            this.componeFacadeREST.create(compone);
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    }
    
    
}
