/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.ConfirmationFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@ConfirmationControllerQualifier
public class ConfirmationController implements ConfirmationDelegate,java.io.Serializable{
  
    @EJB
    private ConfirmationFacadeREST confirmationFacadeREST;
    
    @Override
    public void create(Entities.Confirmation confirmation){
    
        try{
        
           confirmationFacadeREST.create(confirmation);
        
        }
        catch(EJBException ex){
        
            Logger.getLogger(ConfirmationController.class.getName()).log(Level.SEVERE,null,ex);
            
          
        
        }
    
    
    }
    
    
    @Override
    public void confirm(String username){
    
    try{
    
       
   Entities.Confirmation confirmation=confirmationFacadeREST.find(username);
    
   if(confirmation!=null){
   
    confirmation.setConfirmed(true);
       
    this.confirmationFacadeREST.edit(confirmation.getLoginAdministradorusername(), confirmation);
   
   }
   else{
   
       throw new EJBException();
   
   }
    
    }catch(EJBException ex){
    
        Logger.getLogger(ConfirmationController.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public Entities.Confirmation findUsernameConfirmed(String username,boolean confirmed){
    
    try{
    
        return this.confirmationFacadeREST.findByUsernameConfirmed(username, confirmed);
    
    }
    catch(javax.ejb.EJBException | StackOverflowError ex)
    {
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
        
        
    }
    
    @Override
    public void delete(Entities.Confirmation confirmation){
    
        try{
        
            this.confirmationFacadeREST.remove(confirmation);
        
        }
        catch(EJBException ex){
        
        Logger.getLogger(ConfirmationController.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
    
    }

    @Override
    public Entities.Confirmation find(String id){
    
        try{
        
            return this.confirmationFacadeREST.find(id);
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public java.util.List<Entities.Confirmation>findAll(){
    
        try{
        
            return this.confirmationFacadeREST.findAll();
        
        }
        catch(Exception | StackOverflowError ex){
        
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        }
    
    }
    
}
