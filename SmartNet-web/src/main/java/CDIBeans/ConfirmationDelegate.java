/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface ConfirmationDelegate {
    
    void confirm(String username);
    
    void create(Entities.Confirmation confirmation);
    
    void delete(Entities.Confirmation confirmation);
    
    Entities.Confirmation findUsernameConfirmed(String username,boolean confirmed);
    
    Entities.Confirmation find(String id);
    
     java.util.List<Entities.Confirmation>findAll();
}
