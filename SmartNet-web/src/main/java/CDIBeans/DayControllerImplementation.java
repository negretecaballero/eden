/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@DayControllerQualifier
public class DayControllerImplementation implements DayController {
    
    @javax.ejb.EJB
    private jaxrs.service.DayFacadeREST dayFacadeREST;
    
    
    @Override
    public SessionClasses.EdenList<Entities.Day>getAllDays(){
    
        try{
        
            SessionClasses.EdenList<Entities.Day>results=new SessionClasses.EdenList<Entities.Day>();
            
            for(Entities.Day day:this.dayFacadeREST.findAll()){
            
            results.addItem(day);
            
            }
        
            
            return results;
        }
        catch(javax.ejb.EJBException | StackOverflowError ex)
        {
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }

    
    }
    
}
