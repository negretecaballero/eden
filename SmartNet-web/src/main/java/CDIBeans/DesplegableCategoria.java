/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Categoria;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import jaxrs.service.CategoriaFacadeREST;


/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@DesplegableCategoriaQualifier
@Named("desplegablesCategoria")
public class DesplegableCategoria implements DesplegableCategoriaInterface  {
    @EJB
    private CategoriaFacadeREST categoriaFacade;
   private java.util.Vector<Categoria>results;

   
    @Override
    public java.util.Vector<Categoria> getResults() {
        return results;
    }

   
    @Override
    public void setResults(java.util.Vector<Categoria> results) {
        this.results = results;
    }
   
   
   
   @PostConstruct
   public void init(){
   
       System.out.print("Desplegables Categorias PostConstructed");
   
   try{
   HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
   
   if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
   
      Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
      
      results=new java.util.Vector<Entities.Categoria>(categoriaFacade.findByFranchise(franchise.getIdfranquicia()).size());
      
      for(Entities.Categoria category:categoriaFacade.findByFranchise(franchise.getIdfranquicia()))
      {
      
          results.add(category);
      
      }
         
       
   }
   else{
   throw new NullPointerException();
   }
   }
   catch(NullPointerException ex){
   
   }
   
   catch(EJBException ex){
   
   }
   
   
   }
}
