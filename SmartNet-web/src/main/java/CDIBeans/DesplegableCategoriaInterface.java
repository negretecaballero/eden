/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Categoria;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableCategoriaInterface {

    java.util.Vector<Categoria> getResults();

    void setResults(java.util.Vector<Categoria> results);
    
}
