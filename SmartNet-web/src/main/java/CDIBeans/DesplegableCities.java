/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.City;
import Entities.State;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import jaxrs.service.CityFacadeREST;
import jaxrs.service.StateFacadeREST;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@DesplegableCitiesQualifier
public class DesplegableCities implements DesplegableCitiesInterface{
    
    @EJB
    CityFacadeREST cityFacade;
    
    @EJB
    StateFacadeREST stateFacade;
    
   private java.util.Vector<City>resultsCities;
   
   
   @Override
   public void setResultsCities(java.util.Vector<City> resultsCities){
   
       this.resultsCities=resultsCities;
   }
   
   @Override
   public java.util.Vector<City>getResultsCities(){
   
       return this.resultsCities;
   
   }
   
   @PostConstruct
   public void init(){
   try{
       State state=null;
       
       resultsCities=new java.util.Vector<City>();
       
       for(State stat:stateFacade.findAll()){
       
           state=stat;
           
           break;
       
       }
       
       if(state!=null){

         
         for(Entities.City city:state.getCityCollection()){
         
             resultsCities.add(city);
         
         }
         
       }
       
      
       
   }
   catch(EJBException ex){
   
            
   }
   catch(java.lang.ArrayIndexOutOfBoundsException ex){
   
   }
   
   }
   
   @Override
   public void updateResultsCities(State state){
   
   try{
   
       if(state!=null){
       
           resultsCities=new java.util.Vector<Entities.City>();
           
           for(Entities.City city:state.getCityCollection()){
           
           resultsCities.add(city);
           
           }
           
        }
       else{
       
       throw new NullPointerException();
       
       }
   
   }
   catch(NullPointerException ex){
   
   //Logger.getLogger(DesplegableCities.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
   }
    
}
