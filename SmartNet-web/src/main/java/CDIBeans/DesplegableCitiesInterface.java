/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.City;
import Entities.State;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableCitiesInterface {
   
    void setResultsCities(java.util.Vector<City>resultsCities);
    
    java.util.Vector<City>getResultsCities();
    
    void updateResultsCities(State state);
    
}
