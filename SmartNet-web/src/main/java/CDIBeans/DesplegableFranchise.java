/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.BaseBacking;
import Entities.Franquicia;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import jaxrs.service.FranquiciaFacadeREST;


/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@DesplegableFranchiseQualifier
@Named("desplegableFranchise")
public class DesplegableFranchise extends BaseBacking implements DesplegableFranchiseInterface{
    
    
    
    private java.util.Vector <Franquicia>franchises;
    
    
    @Override
    public java.util.Vector<Franquicia>getFranchises(){
    
        return this.franchises;
    
    }
    
    @Override
    public void setFranchises(java.util.Vector<Franquicia>franchises){
    
    this.franchises=franchises;
        
    }
    
    
    @EJB
    private FranquiciaFacadeREST franquiciaFacade;
    
    @PostConstruct
    public void init(){
    
        try{
    
    if(getSession().getAttribute("username")!=null){
    
    this.franchises=new java.util.Vector<Entities.Franquicia>();
    
    for(Entities.Franquicia franchise:franquiciaFacade.findByUsername(String.valueOf(getSession().getAttribute("username")))){
    
    franchises.add(franchise);
    
    }

    
    System.out.print(this.franchises.size());
    
    }
    else{
    
        throw new RuntimeException();
    
    }
        }
        catch(RuntimeException ex){
        
        
        }
    
    
    }
    
    @Override
    
    public void getAll(){
    
    try{
    
        this.franchises=new java.util.Vector<Entities.Franquicia>();
        
        for(Entities.Franquicia franchise:this.franquiciaFacade.findAll())
        {
        
            franchises.add(franchise);
            
        }
      
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(DesplegableFranchise.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    
    
}
