/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Franquicia;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableFranchiseInterface {
    
    
    void setFranchises(java.util.Vector<Franquicia>franchises);
    
    java.util.Vector<Franquicia>getFranchises();
    
    void getAll();
}
