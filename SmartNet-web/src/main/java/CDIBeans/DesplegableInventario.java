/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.BaseBacking;
import Entities.Inventario;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import jaxrs.service.InventarioFacadeREST;



/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@DesplegableInventarioQualifier
@Named("desplegableInventario")
@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})


public class DesplegableInventario extends BaseBacking implements DesplegableInventarioInterface {
    @EJB
    private InventarioFacadeREST inventarioFacade;
    
    private java.util.Vector<Inventario> results;

    
    
    @Override
    public java.util.Vector<Inventario> getResults() {
        return results;
    }

    @Override
    public void setResults(java.util.Vector<Inventario> results) {
        this.results = results;
    }
    
    
    @PostConstruct
    public void init(){
        
        try{
     System.out.print("Desplegable Inventario constructed");
   
        Entities.Franquicia franchise;
        
        if(getSession().getAttribute("selectedOption")instanceof Entities.Franquicia){
        
        franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
        
        results=new java.util.Vector<Entities.Inventario>(inventarioFacade.getByFranchise(franchise.getIdfranquicia()).size());
        
        for(Entities.Inventario inventario:inventarioFacade.getByFranchise(franchise.getIdfranquicia()))
        {
            results.add(inventario);
        
        }
   
        
        
        System.out.print("Results Inventario size "+results.size());
        
        }
        else{
        throw new NullPointerException();
        }
        }
        
        catch(NullPointerException ex){
            
        Logger.getLogger(DesplegableInventario.class.getName()).log(Level.SEVERE,null, ex);
       
        }
   
    

}
   
    @Override
    public void updateResultList(){
    
    
        
        try{
        
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
        
        Entities.Franquicia franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
          
        this.results.clear();
        
        results=new java.util.Vector<Entities.Inventario>();
        
        for(Entities.Inventario inventario:inventarioFacade.getByFranchise(franchise.getIdfranquicia()))
        {
        
            results.add(inventario);
        
        }
               
        }
        
        else{
        
        throw new NullPointerException();
        
                }
        
        
        
        }
        
        catch(EJBException ex){
            
            Logger.getLogger(DesplegableInventario.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
        catch(Exception ex){
        
            Logger.getLogger(DesplegableInventario.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    
    }

 
    
}
