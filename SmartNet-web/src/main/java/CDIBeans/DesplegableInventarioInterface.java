/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Inventario;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableInventarioInterface {

    java.util.Vector<Inventario> getResults();

    void setResults(java.util.Vector<Inventario> results);
    
    void updateResultList();
}
