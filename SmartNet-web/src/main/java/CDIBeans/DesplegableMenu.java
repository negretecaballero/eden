/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Agrupa;
import Entities.Menu;
import Entities.SucursalPK;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.inject.Named;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.AgrupaFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@DesplegableMenuQualifier
@Named("desplegableMenu")


public class DesplegableMenu implements DesplegableMenuInterface{
    
    @EJB
    private AgrupaFacadeREST agrupaFacade;
    
    @javax.inject.Inject
    @CDIBeans.MenuControllerQualifier
    private CDIBeans.MenuControllerInterface menuController;
    
    private java.util.Vector <Menu> menuList;
    
    
    @Override
    public void setMenuList(java.util.Vector <Menu>menuList){
    
    this.menuList=menuList;
    
    }
    
    @Override
    public java.util.Vector<Menu>getMenuList(){
    
        return this.menuList;
    
    }
    
    public DesplegableMenu(){
    
        this.menuList=new java.util.Vector <Menu>();
    
    }
    
    @Override
    public void updateMenuList(int franchiseId){
    
        this.menuList=new java.util.Vector<Entities.Menu>(this.menuController.findByFranchise(franchiseId));
        
        for(Entities.Menu menu:this.menuController.findByFranchise(franchiseId)){
        
            this.menuList.add(menu);
        
        }
        
        
    
    }
}
