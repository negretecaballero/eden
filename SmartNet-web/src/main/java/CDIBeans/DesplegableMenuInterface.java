/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Menu;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableMenuInterface {
    
    void setMenuList(java.util.Vector<Menu>menuList);
    
    java.util.Vector<Menu>getMenuList();
    
    void updateMenuList(int franchiseId);
    
}
