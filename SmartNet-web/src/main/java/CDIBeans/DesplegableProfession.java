/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Profession;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import jaxrs.service.ProfessionFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@DesplegableProfessionQualifier

public class DesplegableProfession implements DesplegableProfessionInterface{
    
    private java.util.Vector<Profession>professions;
    
    @Override
    public java.util.Vector<Profession>getProfessions(){
    
        return this.professions;
    
    }
    
    @Override
    public void setProfessions(java.util.Vector<Profession>professions){
    
        this.professions=professions;
    
    }
    
    
    @EJB
   private ProfessionFacadeREST professionFacadeREST;
    
    @PostConstruct
    public void init(){
    
        this.professions=new java.util.Vector<Entities.Profession>();
        
        for(Entities.Profession profession:professionFacadeREST.findAll()){
        
            this.professions.add(profession);
        
        }
        
     
        
    }
    
}
