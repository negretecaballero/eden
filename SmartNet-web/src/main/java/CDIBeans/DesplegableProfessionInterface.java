/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Profession;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableProfessionInterface {
    
    void setProfessions(java.util.Vector<Profession>professions);
    
   java.util.Vector<Entities.Profession>getProfessions();
    
}
