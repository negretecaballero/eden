/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.State;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import jaxrs.service.StateFacadeREST;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@DesplegableStateQualifier

public class DesplegableState implements DesplegableStateInterface{
    
    private java.util.Vector<State>results;
    
    public java.util.Vector<State>getResults(){
    
        return this.results;
    
    }
    
    public void setResults(java.util.Vector<State>results){
    
        this.results=results;
    
    }
    
    @EJB
    private StateFacadeREST stateFacadeREST;
    
    @PostConstruct
    public void init(){
    
        this.results=new java.util.Vector<Entities.State>(stateFacadeREST.findAll().size());
        
        for(Entities.State state:stateFacadeREST.findAll()){
        
            this.results.add(state);
        
        }
        
   
    }
    
}
