/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.State;

/**
 *
 * @author luisnegrete
 */
public interface DesplegableStateInterface {
    
    void setResults(java.util.Vector<State>results);
    
    java.util.Vector<State>getResults();
    
}
