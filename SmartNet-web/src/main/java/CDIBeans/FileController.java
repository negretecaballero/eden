package CDIBeans;

import Clases.*;

public interface FileController {

	/**
	 * 
	 * @param file
	 * @param path
	 * @param width
	 * @param height
	 */
	void uploadImage(org.primefaces.model.UploadedFile file, String path, int width, int height);

	void Remove();

	/**
	 * 
	 * @param image
	 * @param path
	 */
	void serverDownload(Entities.Imagen image, String path);

	java.util.List<Entities.Imagen> getImageList();

	/**
	 * 
	 * @param imageList
	 */
	void setImageList(java.util.List<Entities.Imagen> imageList);

	java.util.List<String> getPathList();

	/**
	 * 
	 * @param pathList
	 */
	void setPathList(java.util.List<String> pathList);

	ActionTypeInterface getActionType();

	/**
	 * 
	 * @param actiontype
	 */
	void setActionType(ActionTypeInterface actiontype);
}