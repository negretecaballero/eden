package CDIBeans;

import Clases.EdenString;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Entities.Imagen;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import Clases.*;

@FileControllerQualifier
@javax.enterprise.context.Dependent
@javax.inject.Named("fileController")
public class FileControllerImplementation implements FileController {

	
	private java.util.List<Entities.Imagen> imageList;
	private java.util.List<String> pathList;
	private java.util.List<String> name;
	private final String[] alphabet = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
	private ActionTypeInterface actionType;



	/**
	 * 
	 * @param file
	 * @param path
	 * @param width
	 * @param height
	 */
	@Override
	public void uploadImage(org.primefaces.model.UploadedFile file, String path, int width, int height) {
		
            try{
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+path;
                
                 Imagen aux=new Imagen();
        
                 String str=file.getFileName();
                 
                 aux.setExtension(str.substring(str.lastIndexOf('.'), str.length()));
        
                 aux.setImagen(new byte[convertToByteArray(file).length]);
        
                 aux.setImagen(convertToByteArray(file));
        
                 this.imageList.add(aux);
        
                 System.out.print("Tamaño Bits "+convertToByteArray(file).length);
        
        
        System.out.print("Tamaño de la lista "+ this.imageList.size());
        
        System.out.print("File name fileupload "+file.getFileName());
        
       
        String  ext=aux.getExtension().substring(1);
      
     
      
      BufferedImage image=ImageIO.read(new ByteArrayInputStream(aux.getImagen()));
                
      System.out.print("Image read IMAGEIO");
    
           //Created OutputImage
    BufferedImage outputImage=new BufferedImage(width,height,image.getType());
    
     //scales the input image tot he output image
      
       Graphics2D g2d=outputImage.createGraphics();
       
       g2d.drawImage(image,0,0,width,height,null);
       
       g2d.dispose();
      
      ImageIO.write(outputImage, ext,new File(path,file.getFileName()));
       
      System.out.print("File Created");
      
        if(this.pathList.contains("/images/noImage.png")){
      
      this.pathList.remove("/images/noImage.png");
      
      }
      
  this.pathList.add("/resources/demo/images/"+path+File.separator+file.getFileName());
  
  System.out.print("0 position images "+this.pathList.get(0));
  
  System.out.printf("IMAGES SIZE "+this.pathList.size());
  
  if(this.pathList.size()>0){
  
      for(String imageName:this.pathList){
      
          System.out.print(imageName);
      
      }
  
  }
  
                
            }
            catch(NullPointerException | IOException ex)
            {
            
                ex.printStackTrace(System.out);
            
            }       
	}

	/**
	 * 
	 * @param file
	 */
	private final byte[] convertToByteArray(org.primefaces.model.UploadedFile file) {
		
             try{
       
       InputStream inputStream=file.getInputstream();
       byte [] buffer=new byte[8192];
       int bytesRead;
       
       ByteArrayOutputStream output=new ByteArrayOutputStream();
       
       while((bytesRead=inputStream.read(buffer))!=-1){
       output.write(buffer, 0, bytesRead);
       }
       
       return output.toByteArray();
       
   }
   catch(Exception ex){
   Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
   return null;
   }
            
	}

	@Override
	public void Remove() {
		
            try {
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                if(session.getAttribute("username")!=null){
                
                    Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                    
                    filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                    this.pathList.clear();
                    
                    this.imageList.clear();
                    
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}
        
        @javax.annotation.PostConstruct
        public void init(){
        
            try{
            
                if(this.imageList==null){
                
                this.imageList=new java.util.ArrayList<Entities.Imagen>();
                
                
                }
                
                if(this.pathList==null){
                
                this.pathList=new java.util.ArrayList<String>();
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
        
        }

	/**
	 * 
	 * @param image
	 * @param path
	 */
	@Override
	public void serverDownload(Entities.Imagen image, String path) {
		
            try{
             
             
      Imagen aux=new Imagen();  
    
      aux.setImagen(image.getImagen());
  
      aux.setExtension(image.getExtension());
  
      aux.setIdimagen(image.getIdimagen());
        
      
      this.imageList.add(image);
         
      ServletContext servletContext=(ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
         
      System.out.print(servletContext.getRealPath(""));
             
      String  path1=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images";
        
      System.out.print("Path: "+path);
        
      FilesManagementInterface filesManagement=new FilesManagement();

              
      File newfile=new File(path1+File.separator+path);

    
      if(!newfile.exists()){
      
          filesManagement.createFolder(path1+File.separator, path);
      
      }

      
      path1=path1+File.separator+path;
      
      //Number of files in te folder
      
     // int filesFolder=filesManagement.folderFiles(path).length;
      
    String fileName=this.generateName(this.pathList);
      
       if(this.pathList.contains("/images/noImage.png")){
      
      this.pathList.remove("/images/noImage.png");
      
      }
      
      String files=fileName+aux.getExtension();
      String exxt=aux.getExtension().substring(1);
      
      
      
         ImageIO.write(convertImage(500,500,aux.getImagen()), exxt,new File(path1,files));
  
         if(this.name==null){
         
             this.name=new java.util.ArrayList<String>();
         
         }
         
          this.name.add(files);
          
          System.out.print(this.name.size());
          
         this.pathList.add(File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+path+File.separator+files);
         
        
         }
         catch(java.io.IOException | NullPointerException  ex){
         
            ex.printStackTrace(System.out);
         
         }
            
	}

	/**
	 * 
	 * @param nameList
	 */
	private final String generateName(java.util.List<String> nameList) {
		
String name="";

Random random=new Random();

for(int i=0;i<6;i++){

    System.out.print("Alphabet Length "+alphabet.length);
    
    int rand=random.nextInt(((alphabet.length-1)-0)+1)+0;

    System.out.print("Random Number "+rand);
    
    name=name+alphabet[rand];

}

boolean found=false;

for(String aux:nameList){

    System.out.print("Aux value "+aux);
    
    System.out.print("Size pattern "+Clases.EdenString.split(".", aux).length);
    if(EdenString.split(".",aux)[0].equals(name)){
    
        found=true;
        
        break;
    
    }

}

if(found){

    name=generateName(nameList);

}

return name;
	}

	/**
	 * 
	 * @param width
	 * @param height
	 * @param image
	 */
	private final java.awt.image.BufferedImage convertImage(int width, int height, byte[] image) {
	       try{
           
           //ReadInputImage
    BufferedImage buffered=ImageIO.read(new ByteArrayInputStream(image));
    
    System.out.print("you got in convertImage");
           //Created OutputImage
    BufferedImage outputImage=new BufferedImage(width,height,buffered.getType());
    
     //scales the input image tot he output image
      
       Graphics2D g2d=outputImage.createGraphics();
       
       g2d.drawImage(buffered,0,0,width,height,null);
       
       g2d.dispose();
     
      return  outputImage;
   }
   catch(IOException ex)
       
   {
       
   Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
   
   return null;
   
   }
	}

        
      @javax.annotation.PreDestroy
      public void preDestroy(){
      
      try{
      
          this.Remove();
          
      }
      catch(NullPointerException ex){
      
      ex.printStackTrace(System.out);
      
      }
      
      }

	@Override
	public ActionTypeInterface getActionType() {
		return this.actionType;
	}

	/**
	 * 
	 * @param actionType
	 */
	@Override
	public void setActionType(ActionTypeInterface actionType) {
		
            this.actionType=actionType;
            
	}

	@Override()
	public java.util.List<Entities.Imagen> getImageList() {
		return this.imageList;
	}

	/**
	 * 
	 * @param imageList
	 */
	@Override()
	public void setImageList(java.util.List<Entities.Imagen> imageList) {
		this.imageList = imageList;
	}

	@Override()
	public java.util.List<String> getPathList() {
		return this.pathList;
	}

	/**
	 * 
	 * @param pathList
	 */
	@Override()
	public void setPathList(java.util.List<String> pathList) {
		this.pathList = pathList;
	}
        
}