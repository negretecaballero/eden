/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;


import java.util.logging.Logger;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.glxn.qrgen.image.ImageType;
import org.primefaces.model.UploadedFile;
import Clases.ActionTypeInterface;
import Clases.ActionType;
import Clases.BaseBacking;
import Clases.EdenString;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Entities.Imagen;
import java.io.FileInputStream;
import java.util.Random;
import javax.faces.FacesException;
import javax.servlet.ServletContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author luisnegrete
 */

@FileUploadBeanQualifier

@SessionScoped

@Named(value="fileUploadBean")




public class FileUploadBean   implements FileUploadInterface,Serializable{
 
   private static final long serialVersionUID = 7526472295622776147L;

   private String library;
   
   private java.util.List<StreamedContent>streamedList;
   
   
   
   
   @Override
   public String getLibrary()
   {
   
       return this.library;
   
   }   
   
   @Override
   public void setLibrary(String library){
   
       this.library=library;
   
   }
   
   @Override
   public java.util.List<String>getName(){
   
       return this.name;
   
   }
   
   @Override
   public void setName(java.util.List<String>name){
   
   this.name=name;
   
   }
   
   private java.util.List<String>name;
   
   
    public FileUploadBean() {
        this.actionType=new ActionType("");
        this.classType="";
    }
     
    private ActionTypeInterface actionType;
    
    private String classType; 

     @Override
    public String getClassType() {
        return classType;
    }

     @Override
    public void setClassType(String classType) {
        this.classType = classType;
    }
    
    
    @Override
    public ActionTypeInterface getActionType() {
        return actionType;
    }
    
    @Override
    public void setActionType(ActionTypeInterface actionType) {
        this.actionType = actionType;
    }
    
    
    
     private List<Imagen>photoList;
    
     
        private UploadedFile file;
        
  
         
         private List <String> images;

    @Override
    public List<Imagen> getPhotoList() {
        return photoList;
    }

   
    @Override
    public void setPhotoList(List<Imagen> photoList) {
        this.photoList = photoList;
    }

   
    @Override
    public UploadedFile getFile() {
        return file;
    }

   
    @Override
    public void setFile(UploadedFile file) {
        this.file = file;
    }

    @Override
    public List<StreamedContent> getBufferedList() {
        return bufferedList;
    }

    @Override
    public void setBufferedList(List<StreamedContent> bufferedList) {
        this.bufferedList = bufferedList;
    }

   
   
    
    
    @Override
    public List <String> getImages() {
        
        if(this.images==null || this.images.isEmpty()){
        
            System.out.print("Adding no Image to File Upload");
            
            this.images=new java.util.ArrayList<String>();
        
            this.images.add("/images/noImage.png");
        }
        
        return images;
    }

  
    @Override
    public void setImages(List <String> images) {
        this.images = images;
    }
         
         private List<StreamedContent>bufferedList;
    
    @PostConstruct
    public void initialize(){
        
        System.out.print("FileUploadBean Initialized");
        
        this.bufferedList=new java.util.ArrayList<StreamedContent>();
        
         photoList=new <ImageType> ArrayList();

         images=new ArrayList();
         
         this.name=new java.util.ArrayList<String>();
         
         this.streamedList=new java.util.ArrayList<StreamedContent>();
    
    }
    
    public void modifySize(int widtt,int height){
    
    
    
    }
    
    @PreDestroy
    public void preDestroy(){
        
    System.out.print("FileUploadBean Predestroyed");
    
    }
    
    @Override
   public void Remove(String path)
   {
   this.images.clear();
   this.photoList.clear();
   this.file=null;
   
   
   FilesManagementInterface fileManagement=new FilesManagement();
   
   fileManagement.cleanFolder(path);
   
   }
   
   private BufferedImage convertImage(int width, int height, byte [] imagen){
   
       try{
           
           //ReadInputImage
    BufferedImage image=ImageIO.read(new ByteArrayInputStream(imagen));
    
    System.out.print("you got in convertImage");
           //Created OutputImage
    BufferedImage outputImage=new BufferedImage(width,height,image.getType());
    
     //scales the input image tot he output image
      
       Graphics2D g2d=outputImage.createGraphics();
       
       g2d.drawImage(image,0,0,width,height,null);
       
       g2d.dispose();
     
      return  outputImage;
   }
   catch(IOException ex)
       
   {
       
   Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
   return null;
   
   }
       
   }
   
   private  byte []  convertToByteArray(UploadedFile file){
   try{
       
       InputStream inputStream=file.getInputstream();
       byte [] buffer=new byte[8192];
       int bytesRead;
       
       ByteArrayOutputStream output=new ByteArrayOutputStream();
       
       while((bytesRead=inputStream.read(buffer))!=-1){
       output.write(buffer, 0, bytesRead);
       }
       
       return output.toByteArray();
       
   }
   catch(Exception ex){
   Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
   return null;
   }
       
   }
   
        
    @Override
     public void addFiles(UploadedFile f, String path, int width,int heigth){      
     String str;
     
    this.file=f;
        
        if(file!=null){
        
            
        try{
        
        Imagen aux=new Imagen();
        
        str=file.getFileName();
        
        aux.setExtension(str.substring(str.lastIndexOf('.'), str.length()));
        
        
        aux.setImagen(new byte[convertToByteArray(f).length]);
        
     
        
        aux.setImagen(convertToByteArray(f));
        
        this.photoList.add(aux);
        
        System.out.print("Tamaño Bits "+convertToByteArray(f).length);
        
        
        System.out.print("Tamaño de la lista "+ this.photoList.size());
        
        System.out.print("File name fileupload "+f.getFileName());
        
       
        
       
      HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      
      String username=(String) session.getAttribute("username");
      
      
     // System.out.print("Usuario "+user[0]);
      
    
     
      FilesManagementInterface fileManagement=new FilesManagement();
      
      System.out.print(username);
      //Number of files in te folder
      
      int filesFolder=images.size();
      
      if(images.contains("/images/noImage.png")){
      
      this.images.remove("/images/noImage.png");
      
      }
      
      System.out.print("Items in folder "+String.valueOf(filesFolder));
      
      
      String  ext=aux.getExtension().substring(1);
      
     
      
      BufferedImage image=ImageIO.read(new ByteArrayInputStream(aux.getImagen()));
    
      System.out.print("Image read IMAGEIO");
    
           //Created OutputImage
    BufferedImage outputImage=new BufferedImage(width,heigth,image.getType());
    
     //scales the input image tot he output image
      
       Graphics2D g2d=outputImage.createGraphics();
       
       g2d.drawImage(image,0,0,width,heigth,null);
       
       g2d.dispose();
      
      ImageIO.write(outputImage, ext,new File(path,f.getFileName()));
       
      System.out.print("File Created");
      
     // this.listImages.addElement("imagesCache/"+username+"/"+files);
      
      this.images.add( "imagesCache/"+username+"/"+f.getFileName());
      
      if(this.photoList.size()!=this.images.size()){
          
      System.out.print("both lists are not the same");
      
      }
      
     // System.out.print("Objeto agregados  "+this.listImages.getPhotolist().size());   
        
        
      FacesMessage msg = new FacesMessage("Succesful", file.getFileName()+ " is uploaded. "+(double)(aux.getImagen().length)/1000000+" Megabytes" );
                      
       FacesContext.getCurrentInstance().addMessage(null, msg);

        FacesContext.getCurrentInstance().renderResponse();
        
        System.out.println("Uploaded File Name Is : "+file.getFileName()+" :: Uploaded File Size :: "+file.getSize());
    
        ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
        
        System.out.print("Request URI "+((HttpServletRequest) ec.getRequest()).getRequestURI());
        
        ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        
        }
        
        catch(IOException exp){
        
        java.util.logging.Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,exp);
        
        }
        
        catch(NumberFormatException ex){
    
        
        
        }
            
        }
        
            else
        {
         FacesMessage msg = new FacesMessage(FacesMessage.FACES_MESSAGES,"Please select image!!");
         FacesContext.getCurrentInstance().addMessage(null, msg);
        }
 
 }
     
     
    
@Override
public void AddPhoto(Imagen imagen, String path,String path2,int width,int heigth){
    
    try{

  Imagen aux=new Imagen();  
    
  aux.setImagen(imagen.getImagen());
  
  aux.setExtension(imagen.getExtension());
  
  aux.setIdimagen(imagen.getIdimagen());
  
  this.photoList.add(aux);
  
  
      FilesManagementInterface fileManagement=new FilesManagement();
      
      //Number of files in te folder
      
      int filesFolder=fileManagement.folderFiles(path).length;
      
      String files=String.valueOf(filesFolder)+aux.getExtension();
      String exxt=aux.getExtension().substring(1);
   
      //this.library=File.separator+"Users"+File.separator+"luisnegrete"+File.separator+"Copy"+File.separator+"ProyectoLuis"+File.separator+"SmartNet"+File.separator+"SmartNet-web"+File.separator+"target"+File.separator+"SmartNet-web-1.0-SNAPSHOT"+File.separator+"User"+File.separator+"imagesCache"+File.separator+getSession().getAttribute("username");
      
      javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      
      this.library="imagesCache"+File.separator+session.getAttribute("username");
      
      System.out.print("File Name "+files);
      
      this.streamedList.add(new DefaultStreamedContent(new ByteArrayInputStream(imagen.getImagen()),"image/"+aux.getExtension().substring(1)+""));
     
   
   ImageIO.write(convertImage(width,heigth,aux.getImagen()), exxt,new File(path,files));
   
   //HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
  
   String username=(String)session.getAttribute("username");
   
   images.add("imagesCache/"+path2+"/"+files);
   
    this.name.add(files);
    
   System.out.print(aux.getExtension().substring(1));
   
   this.bufferedList.add(new DefaultStreamedContent(new ByteArrayInputStream(aux.getImagen()),"image/svg+xml"));
   
   for(String image:images){
   
   System.out.print("Added image value "+image);
   
   }
   
   System.out.print("Bufered List has "+this.bufferedList.size()+" items");
      
    }
    catch(IOException ex){
        
    Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    catch(NullPointerException ex){
        
    Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
    
    }
     
  
}


@Override
     public void loadImagesServletContext(Entities.Imagen image,String username){
     
         try{
             
             
      Imagen aux=new Imagen();  
    
      aux.setImagen(image.getImagen());
  
      aux.setExtension(image.getExtension());
  
      aux.setIdimagen(image.getIdimagen());
        
      this.photoList.add(image);
         
      javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
      
      ServletContext servletContext=(ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
         
      System.out.print(servletContext.getRealPath(""));
             
      String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images";
        
      System.out.print("Path: "+path);
        
      FilesManagementInterface filesManagement=new FilesManagement();

              
      File newfile=new File(path+File.separator+username);

    
      if(!newfile.exists()){
      
          filesManagement.createFolder(path+File.separator, username);
      
      }

      
      path=path+File.separator+username;
      
      //Number of files in te folder
      
     // int filesFolder=filesManagement.folderFiles(path).length;
      
    String fileName=this.generateName(this.images);
      
       if(images.contains("/images/noImage.png")){
      
      this.images.remove("/images/noImage.png");
      
      }
      
      String files=fileName+aux.getExtension();
      String exxt=aux.getExtension().substring(1);
      
      
      
         ImageIO.write(convertImage(500,500,aux.getImagen()), exxt,new File(path,files));
  
          this.name.add(files);
          
          System.out.print(this.name.size());
          
         images.add(File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+username+File.separator+files);
         
        
         }
         catch(java.io.IOException  ex){
         
             Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
         
         }
         
        
     }





@Override
public void removeImage(String path){
    
    System.out.print("Path to Remove "+path);
    
    File file=new File(path);
    
    System.out.print("Parent path :"+file.getParent());
    
    System.out.print("List size "+this.photoList.size());
    
    if(!path.equals("/images/noImage.png") && this.photoList.size()>0)
    {
        
    System.out.print("Image to Remove "+path);

    int index=0;
    
    for(int i=0;i<this.images.size();i++){
    
    if(path.equals(this.images.get(i))){
    
        index=i;
        
        break;
    
    }
    
    }
    
    this.images.remove(index);
    
    this.photoList.remove(index);
    
    if(!file.getParent().equals("/images/categories")){
    
   FilesManagementInterface filesManagement=new FilesManagement();
   
   HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
   
   javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
   
   String deletePath=servletContext.getRealPath("")+path;
   
   System.out.print("File to Delete "+deletePath);
   
   filesManagement.deleteFile(deletePath);
   
    }
    
    else{
    
        System.out.print("You cannot delete files from this path");
    
    }
}
    
    else{
    
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("You cannot delete this image");
    
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
        
        javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}

    @Override
    public List<StreamedContent> getStreamedList() {
   
        return this.streamedList;
    
    }

    @Override
    public void setStreamedList(List<StreamedContent> streamedList) {

    this.streamedList=streamedList;
    
    }
    
    @Override
    public void deleteImage(String imageName){
    
        System.out.print("Removing "+imageName);
        
        try{
            
         int index=0;
         
         for(int i=0;i<this.images.size();i++){
         
         if(images.get(i).equals(imageName)){
         
             index=i;
             
             break;
         
         }
         
         }
        
         this.images.remove(index);
         
         this.photoList.remove(index);
         
        }
        catch(Exception ex){
        
        Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException("Exception caugth");
        
        }
    
    }
    
 @Override
   public void servletContainer(UploadedFile file,String username,int width, int height){
 
        try{
        
       ServletContext context=(ServletContext) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        String path=context.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+username;
          
        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
          
        Imagen aux=new Imagen();
        
        String str=file.getFileName();
        
        aux.setExtension(str.substring(str.lastIndexOf('.'), str.length()));
        
        
        aux.setImagen(new byte[convertToByteArray(file).length]);
        

        aux.setImagen(convertToByteArray(file));
        
        this.photoList.add(aux);
        
        System.out.print("Tamaño Bits "+convertToByteArray(file).length);
        
        
        System.out.print("Tamaño de la lista "+ this.photoList.size());
        
        System.out.print("File name fileupload "+file.getFileName());
        
       
        String  ext=aux.getExtension().substring(1);
      
     
      
      BufferedImage image=ImageIO.read(new ByteArrayInputStream(aux.getImagen()));
    
      System.out.print("Image read IMAGEIO");
    
           //Created OutputImage
    BufferedImage outputImage=new BufferedImage(width,height,image.getType());
    
     //scales the input image tot he output image
      
       Graphics2D g2d=outputImage.createGraphics();
       
       g2d.drawImage(image,0,0,width,height,null);
       
       g2d.dispose();
      
      ImageIO.write(outputImage, ext,new File(path,file.getFileName()));
       
      System.out.print("File Created");
      
        if(images.contains("/images/noImage.png")){
      
      this.images.remove("/images/noImage.png");
      
      }
      
  this.images.add("/resources/demo/images/"+username+File.separator+file.getFileName());
  
  System.out.print("0 position images "+this.images.get(0));
  
  System.out.printf("IMAGES SIZE "+this.images.size());
  
  if(this.images.size()>0){
  
      for(String imageName:this.images){
      
          System.out.print(imageName);
      
      }
  
  }
  
        }
        catch(Exception ex){
        
        Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
    
    }
   
   
   
   
   @Override
   public void setImageManually(String path,String image,String ext){
   
       try{
       
       if(this.images==null){
       
           this.images=new java.util.ArrayList<String>();
              
       }
   
       if(this.photoList==null){
       
           this.photoList=new java.util.ArrayList<Entities.Imagen>();
       
       }
       
       this.images.add(image);
       
       java.io.File file=new java.io.File(path);
       
       if(file.exists() && file.isFile()){
       
           System.out.print("This file existss");
           
           FileInputStream fileInputStream=null;
 
          byte[] bFile = new byte[(int) file.length()];
          
          fileInputStream = new FileInputStream(file);
	    fileInputStream.read(bFile);
	    fileInputStream.close();
 
	  
       Entities.Imagen imagen=new Entities.Imagen();
       
       
       imagen.setImagen(bFile);
       
       imagen.setExtension("."+ext);
       
       this.photoList.add(imagen);
       
       }
   }
   
   catch(Exception ex){

       Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);

}
}
   
@Override  
public void removeManually(java.util.List<String> nameList){

    try{
        
           if(this.images.contains("/images/noImage.png")){
      
         this.images.remove("/images/noImage.png");
         
      }
        int index=0;
        
        boolean found=false;
        
        for(int i=0;i<nameList.size();i++){
        
            for(int j=0;j<this.images.size();j++){
            
            if(this.images.get(j).equals(nameList.get(i))){
            
                found=true;
                
                index=j;
                
                break;
            
            }
            
            if(found){
            
                break;
            
            }
            
            }
        
        }
    
        if(found){
        
        this.images.remove(index);
        
        this.photoList.remove(index);
        
        }
        
    }
    catch(Exception ex){
    
    
    }

}   

@Override
public void forceLoadImages(String folder){

try{

    java.io.File files=new java.io.File(folder);
    
    java.io.File[] fileArray=files.listFiles();
    
    if(this.photoList==null){
    
        this.photoList=new java.util.ArrayList<Entities.Imagen>();
    
    }
    
    else{
    
        this.photoList.clear();
    
    }
    
    
    if(this.images==null){
    
    this.images=new java.util.ArrayList<String>();
    
    }
    
    else{
    
        this.images.clear();
    
    }
    
    if(files.exists())
    {
        
    javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
    for(int i=0;i<fileArray.length;i++){
        
    if(fileArray[i].isFile()){
        
     if(!this.images.contains(File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+fileArray[i].getName()))   
     {
    System.out.print("File Name "+fileArray[i].getName());
    
    this.images.add(File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+fileArray[i].getName());
    
    FileInputStream fileInputStream=new FileInputStream(fileArray[i]);
    
     byte[] bFile = new byte[(int) fileArray[i].length()];
    
     fileInputStream.read(bFile);
     
     fileInputStream.close();
     
    Entities.Imagen image=new Entities.Imagen();
    
    image.setExtension(this.getExtension(fileArray[i].getName()));
    
    image.setImagen(bFile);
    
    this.photoList.add(image);
    }
    }
    }
    
    }
    else{
    
        throw new javax.faces.FacesException("This folder does not exists");
    
    }

}
catch(IOException ex){

Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);

throw new javax.faces.FacesException(ex);

}

}



private String getExtension(String name){

    System.out.print("File Name Get Extension "+name);
    
    try{
    
    return name.substring(name.lastIndexOf('.'), name.length());
       
    }
    catch(RuntimeException ex){
    
    Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }

}

private String _auxiliar;

@Override
public String getAuxiliar(){

    return _auxiliar;

}

@Override
public void setAuxiliar(String auxiliar){

_auxiliar=auxiliar;

}

private String []alphabet={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

private String generateName(java.util.List<String>nameList){

String name="";

Random random=new Random();

for(int i=0;i<6;i++){

    System.out.print("Alphabet Length "+alphabet.length);
    
    int rand=random.nextInt(((alphabet.length-1)-0)+1)+0;

    System.out.print("Random Number "+rand);
    
    name=name+alphabet[rand];

}

boolean found=false;

for(String aux:nameList){

    System.out.print("Aux value "+aux);
    
    System.out.print("Size pattern "+Clases.EdenString.split(".", aux).length);
    if(EdenString.split(".",aux)[0].equals(name)){
    
        found=true;
        
        break;
    
    }

}

if(found){

    name=generateName(nameList);

}

return name;

}

public void clean(){

    try{
    
       /*javax.servlet.ServletContext context=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        this.Remove(context.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
    */
        this.setActionType(null);
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }

}



}


