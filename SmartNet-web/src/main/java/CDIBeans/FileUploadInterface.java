/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;


import Clases.ActionTypeInterface;
import Entities.Imagen;
import java.util.List;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author luisnegrete
 */
public interface FileUploadInterface {

    void Remove(String path);

    void addFiles(UploadedFile f, String path, int width,int heigth);

    UploadedFile getFile();

    List<String> getImages();

    List<Imagen> getPhotoList();

    List<StreamedContent>getBufferedList();
    
    void setBufferedList(List<StreamedContent>bufferedList);

    void setFile(UploadedFile file);

    void setImages(List<String> images);

    void setPhotoList(List<Imagen> photoList);

      ActionTypeInterface getActionType();
    void setActionType(ActionTypeInterface actionType);
    
    void AddPhoto(Imagen imagen,String path2, String path,int width,int heigth);
    
    String getClassType();
    
    void setClassType(String classType);
    
    void removeImage(String path);
    
    void loadImagesServletContext(Entities.Imagen image,String username);
    
    String getLibrary();
    
    void setLibrary(String library);
    
    java.util.List<String>getName();
    
    void setName(java.util.List<String>name);
    
    java.util.List<StreamedContent>getStreamedList();
    
    void setStreamedList(java.util.List<StreamedContent>streamedList);
    
    void deleteImage(String imageName);
    
    void servletContainer(UploadedFile file,String username,int width, int height);
    
    void setImageManually(String path,String image,String ext);
    
    void removeManually(java.util.List<String> nameList);
    
    void forceLoadImages(String folder);
    
    String getAuxiliar();
    
    void setAuxiliar(String auxiliar);
}
