/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.BaseBacking;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import jaxrs.service.FranquiciaFacadeREST;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@FranchiseControllerQualifier

public class FranchiseController extends BaseBacking implements FranchiseControllerInterface{
  
    
    @EJB
    private FranquiciaFacadeREST franquiciaFacadeREST;
    
    @Inject   
    @CDIBeans.FileUploadBeanQualifier
    private 
    CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject 
    @CDIBeans.PedidoControllerQualifier
    private CDIBeans.PedidoControllerInterface pedidoController;
    
    @javax.inject.Inject
    @CDIBeans.PartnershipRequestControllerQualifier
    private CDIBeans.PartnershipRequestController _partnershipRequestController;
    
    @javax.inject.Inject 
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject
    @CDIBeans.ApplicationControllerQualifier
    private CDIBeans.ApplicationController _applicationController;
    
    @Override
    public String FranchiseLogo(Entities.Franquicia franchise){
    
        try{
        
          javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
            
          Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
          
          java.io.File file=new java.io.File(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username")+File.separator+"logo");
          
          if(!file.exists()){
          
          System.out.print("The file does not exist");
              
          filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString()+File.separator, "logo");
          
          }
          
          else{
          
              System.out.print("Franchise Logo the folder exist already");
          
          }
          
          if(franchise.getImagenIdimagen()!=null){
          
              this.fileUpload.loadImagesServletContext(franchise.getImagenIdimagen(), getSession().getAttribute("username").toString()+File.separator+"logo");
          
              String image=this.fileUpload.getImages().get(0);
              
              this.fileUpload.getImages().clear();
              
              this.fileUpload.getPhotoList().clear();
              
              return image;
          }
          else{
          
              return "/images/delete.jpg";
          
          }
        }
        catch(EJBException ex){
        
            Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
            
          throw new javax.faces.FacesException();
        }
    
    }
    
    @Override
    public java.util.Vector<String>SucursalesPhotos(Entities.Franquicia franchise){
    
    try{
    
        java.util.Vector<String>imageList=new java.util.Vector<String>(franchise.getSucursalCollection().size());
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        java.io.File file=new java.io.File(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString()+File.separator+"sucursales");
        
        if(!file.exists())
        {
        
        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
        
        filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString()+File.separator,"sucursales");
        
        }
        
        
        for(Entities.Sucursal suc:franchise.getSucursalCollection()){
        
        
        if(suc.getImagenCollection()!=null && !suc.getImagenCollection().isEmpty()){
        
        this.fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)suc.getImagenCollection()).get(0), getSession().getAttribute("username").toString()+File.separator+"sucursales");
        
        String image=this.fileUpload.getImages().get(0);
        
        imageList.add(image);
        
        this.fileUpload.getImages().clear();
        
        }
        
        
        }
    return imageList;
    }
    catch(javax.ejb.EJBException | IllegalArgumentException ex){
        
      Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
    
       throw new javax.faces.FacesException(ex); 
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Franquicia>findAll(){
    
    try{
        
        java.util.Vector<Entities.Franquicia>results=new java.util.Vector<Entities.Franquicia>(this.franquiciaFacadeREST.findAll().size());
        
        for(Entities.Franquicia franchise:this.franquiciaFacadeREST.findAll()){
        
            results.add(franchise);
        
        }
        
        return results;
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
    
    }
    
    
    }
    
    @Override
    public Entities.Franquicia findByName(String name){
    
    try{
    
       Entities.Franquicia franchise=null;
       
       for(Entities.Franquicia aux:this.findAll()){
       
       
       if(name.toLowerCase().equals(aux.getNombre().toLowerCase())){
       
           franchise=aux;
           
           break;
       
       }
       
       }
       
       System.out.print("Found Franchise "+franchise);
       
       return franchise;
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
        
   return null;
    
    }
    
    }
    
    @Override
    public java.util.List<Entities.Franquicia>findByUsername(String username){
    
        try{
        
            return this.franquiciaFacadeREST.findByUsername(username);
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    @Override
    public Entities.Franquicia find(int idFranchise){
    
    
    try{
    
       return this.franquiciaFacadeREST.find(idFranchise);
    
    }
    catch(EJBException ex){
    
    Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new FacesException(ex);
    
    }
    
    }
    
    /**
	 * 
	 * @param franchise
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public void create(Entities.Franquicia franchise){
    
        try{
        
            
        this.franquiciaFacadeREST.create(franchise);
              
        }
        catch(EJBException | NullPointerException ex){
        
            Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
            
         ex.printStackTrace(System.out);
        
        }
     
    
    }
    
    @Override
    public String getLogo(Entities.Franquicia franchise){
    
        if(franchise.getImagenIdimagen()!=null){
        
            fileUpload.loadImagesServletContext(franchise.getImagenIdimagen(), getSession().getAttribute("username").toString());
           
            return fileUpload.getImages().get(0);
        }
        else{
        
            return"/images/noImage.png";
            
        }
    
 
    }
    
    /**
	 * 
	 * @param franchise
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void edit(Entities.Franquicia franchise){
    
    try
        
    {
    
        this.franquiciaFacadeREST.edit(franchise.getIdfranquicia(), franchise);
        
        
    }
    catch(EJBException ex){
    
    Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
   @Override
   public double getAveragePerSucursal(int idFranchise){
   
   try{
   
       Entities.Franquicia franchise=this.franquiciaFacadeREST.find(idFranchise);
       
       Integer pedidos;
       
       Integer totalSucursales;
       
       if(franchise!=null && franchise.getSucursalCollection()!=null && !franchise.getSucursalCollection().isEmpty() && this.pedidoController.findByFranchise(idFranchise)!=null && !this.pedidoController.findByFranchise(idFranchise).isEmpty()){
       
          pedidos=this.pedidoController.findByFranchise(idFranchise).size();
          
          totalSucursales=franchise.getSucursalCollection().size();
          
          return pedidos.doubleValue()/totalSucursales.doubleValue();
       
       }
       
       else{
       
           return 0.0;
       
       }
   
   }
   catch(javax.ejb.EJBException ex){
   
       Logger.getLogger(FranchiseController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new javax.faces.FacesException(ex);
   
   }
   
   }
    
   @Override
     public java.util.Vector<Entities.Franquicia>findByTypeName(String name){
    
    try{
    
        java.util.Vector<Entities.Franquicia>results=new java.util.Vector<Entities.Franquicia>(this.franquiciaFacadeREST.findByTypeName(name).size());
        
        for(Entities.Franquicia franchise:this.franquiciaFacadeREST.findByTypeName(name)){
        
            results.add(franchise);
        
        }
        
        return results;
    
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
     
     
     @Override
     public SessionClasses.EdenList<Entities.Producto>getProducts(int idFrancise){
     
         try{
             
             SessionClasses.EdenList<Entities.Producto>result=new SessionClasses.EdenList<Entities.Producto>();
         
            Entities.Franquicia franchise=this.find(idFrancise);
            if(franchise.getCategoriaCollection()!=null && !franchise.getCategoriaCollection().isEmpty())
            {
            for(Entities.Categoria aux:franchise.getCategoriaCollection()){
            
                for(Entities.Producto product:aux.getProductoCollection()){
                
                    result.addItem(product);
                
                }
                

            return result;
            
            }
            
         }
            
            return null;
         
         }
         catch(javax.ejb.EJBException ex){
         
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
             return null;
         }
     
     }
     
     @Override
     public boolean isSupplier(int idFranchise){
             
     try{
     
         if(this.find(idFranchise)!=null){
         
             Entities.Franquicia franchise=this.find(idFranchise);

             if(franchise.getIdSubtype().getIdTipo().getNombre().equals("Supplier")){
             
                 return true;
             
             }
             
         }
        
     
         return false;
     }
     catch(javax.ejb.EJBException | StackOverflowError ex)
     {
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         return false;
     
     }
     }
}
