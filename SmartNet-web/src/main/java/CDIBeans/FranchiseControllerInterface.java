/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseControllerInterface {
    
    java.util.Vector<Entities.Franquicia>findAll();
    
    Entities.Franquicia find(int idFranchise);
    
    Entities.Franquicia findByName(String name);
    
    void create(Entities.Franquicia franchise);
    
    String getLogo(Entities.Franquicia franchise);
    
    void edit(Entities.Franquicia franchise);
    
    String FranchiseLogo(Entities.Franquicia franchise);
    
    java.util.Vector<String>SucursalesPhotos(Entities.Franquicia franchise);
    
    double getAveragePerSucursal(int idFranchise);
    
    java.util.Vector<Entities.Franquicia>findByTypeName(String name);
    
    SessionClasses.EdenList<Entities.Producto>getProducts(int idFrancise);
    
    boolean isSupplier(int idFranchise);
    
    java.util.List<Entities.Franquicia>findByUsername(String username);
}
