/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.TipoFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@FranchiseTypeControllerQualifier
public class FranchiseTypeController implements FranchiseTypeControllerInterface{
    
    
    @EJB private TipoFacadeREST tipoFacadeREST;
    
    @javax.ejb.EJB
    private jaxrs.service.SubtypeFacadeREST subtypeFacadeREST;
   
    
    @Override
    public java.util.Vector<Entities.Tipo>findAll(){
    
    try{
    
        java.util.Vector<Entities.Tipo>results=new java.util.Vector<Entities.Tipo>(tipoFacadeREST.findAll().size());
        
        for(Entities.Tipo tipo:tipoFacadeREST.findAll()){
        
        results.add(tipo);
        
        }
        
      return  results;
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(FranchiseTypeController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
    
    }
    
    }
    
    @Override
    public Entities.Tipo find(int idTipo){
    
        try{
        
            return this.tipoFacadeREST.find(idTipo);
        
        }
        catch(EJBException ex){
        
        Logger.getLogger(FranchiseTypeController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
        
        }
    
    
    }
    
    @Override
    public Entities.Subtype findSubtype(Entities.SubtypePK subtypePK){
    
        try{
        
            
          return this.subtypeFacadeREST.find(new PathSegmentImpl("bar;idSubtype="+subtypePK.getIdSubtype()+";tipoIdtipo="+subtypePK.getTipoIdtipo()+""));
        
        }
        catch(EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
   
    
}
