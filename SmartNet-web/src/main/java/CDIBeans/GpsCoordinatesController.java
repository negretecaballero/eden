/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.transaction.Transactional.TxType;
import jaxrs.service.GpsCoordinatesFacadeREST;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@GPSCoordinatesControllerQualifier

public class GpsCoordinatesController implements GpsCoordinatesControllerInterface{

    @EJB  private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
    
    
    /**
	 * 
	 * @param gpsCoordinates
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.GpsCoordinates gpsCoordinates){
    
    try{
    
        this.gpsCoordinatesFacadeREST.create(gpsCoordinates);
    
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
    
    Logger.getLogger(GpsCoordinatesController.class.getName()).log(Level.SEVERE,null,ex);
    
    
    
    }
    
    }
    
    /**
	 * 
	 * @param id
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void delete(int id){
    
        try{
        
            this.gpsCoordinatesFacadeREST.remove(id);
            
        }
        catch(EJBException | NullPointerException ex){
        
            Logger.getLogger(GpsCoordinatesController.class.getName()).log(Level.SEVERE,null,ex);
            
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public Entities.GpsCoordinates findByCoordinates(double longitude, double latitude){
    
        try{
        
            return this.gpsCoordinatesFacadeREST.findByCoordinates(longitude, latitude);
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            Logger.getLogger(GpsCoordinatesController.class.getName()).log(Level.SEVERE,null,ex);
        
       return null;
            
        }
        
    }
}
