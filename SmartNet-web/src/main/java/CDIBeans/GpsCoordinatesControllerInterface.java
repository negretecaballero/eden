/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface GpsCoordinatesControllerInterface {
    void delete(int id);
    
    Entities.GpsCoordinates findByCoordinates(double longitude, double latitude);
    
    void create(Entities.GpsCoordinates gpsCoordinates);
}
