/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.ImagenFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@ImageControllerQualifier

public class ImageController implements ImageControllerDelegate{
  
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    
    @Override
    public void create(Entities.Imagen image){
    
    try{
    
        imagenFacadeREST.create(image);
    
    }
    catch(EJBException ex){
    
        throw new FacesException("Exception Caugth");
    
    }

        
    
    }
    
    @Override
    public void delete(int idImagen){
    
        try{
        
            imagenFacadeREST.remove(idImagen);
        
        }
        catch(EJBException ex){
        Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException("Exception Caugth");
        }
    
    }
    
    
    @Override
    public Entities.Imagen find(int id){
    
    try{
    
        return this.imagenFacadeREST.find(id);
    
    }
    catch(EJBException ex){
    
    Logger.getLogger(ImageController.class.getName()).log(Level.SEVERE,null,ex);
    
    return null;
    
    }
    
    }
    
}
