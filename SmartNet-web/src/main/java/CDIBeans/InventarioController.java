/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.InventarioPK;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.InventarioFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@InventarioControllerQualifier
@javax.inject.Named("inventoryController")
public class InventarioController implements InventarioDelegate{
  
    @EJB
    private InventarioFacadeREST inventarioFacadeREST;
    
    @Override
    public Entities.Inventario find(InventarioPK inventarioPK){
    
        try{
        
            System.out.print("Inventario ID "+inventarioPK.getIdinventario());
            
            return inventarioFacadeREST.find(new PathSegmentImpl("bar;idinventario="+inventarioPK.getIdinventario()+";franquiciaIdfranquicia="+inventarioPK.getFranquiciaIdFranquicia()+""));
        
        }
        catch(EJBException ex){
            
        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException("Exceptio Caugth");
        
        
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Inventario>findByFranchise(int idFranchise){
    
    try{
    
        java.util.Vector<Entities.Inventario>results=new java.util.Vector<Entities.Inventario>(this.inventarioFacadeREST.getByFranchise(idFranchise).size());
        
        for(Entities.Inventario inventario:this.inventarioFacadeREST.getByFranchise(idFranchise)){
        
            results.add(inventario);
        
        }
        
        return results;
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public Entities.Inventario findByName(String name,int idFranchise){
    
        try{
        
           return  this.inventarioFacadeREST.findByName(name, idFranchise);
            
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public void delete(Entities.InventarioPK key){
    
        try{
        
            this.inventarioFacadeREST.remove(new PathSegmentImpl("bar;idinventario="+key.getIdinventario()+";franquiciaIdfranquicia="+key.getFranquiciaIdFranquicia()+""));
            
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @annotations.MethodAnnotations(author="Luis Negrete",date="22/08/2015",comments="Update Inventory")
    @Override
    public void update(Entities.Inventario inventory){
    
    try{
    
        this.inventarioFacadeREST.edit(new PathSegmentImpl("bar;idinventario="+inventory.getInventarioPK().getIdinventario()+";franquiciaIdfranquicia="+inventory.getInventarioPK().getFranquiciaIdFranquicia()+""),inventory);
    
    }
    
    catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex)
        
    { 
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);  
    }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="26/08/2015",comments="Find Inventory Object given by Inventory Converter")
    public Entities.Inventario findByInventoryConverter(String inventoryId){
    
    try{
        
    if(inventoryId.split(",").length==2){
    
        Entities.InventarioPK key=new Entities.InventarioPK();
        
        key.setFranquiciaIdFranquicia(java.lang.Integer.parseInt(inventoryId.split(",")[0]));
        
        key.setIdinventario(java.lang.Integer.parseInt(inventoryId.split(",")[1]));
        
        return this.find(key);
    
    }
    else{
    
    throw new IllegalArgumentException("Object must be of a structure (number,number)");
    
    }
    
    }
    catch (NullPointerException | NumberFormatException | StackOverflowError | javax.ejb.EJBException ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
   
    
    }
    
    @Override
    public void create(Entities.Inventario inventory){
    
        try{
        
            inventarioFacadeREST.create(inventory);
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Inventario>getAutoInventory(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                if(franchise.getInventarioCollection()!=null && !franchise.getInventarioCollection().isEmpty()){
                
                    java.util.Vector<Entities.Inventario>results=new java.util.Vector<Entities.Inventario>(franchise.getInventarioCollection().size());
                
                    for(Entities.Inventario inventory:franchise.getInventarioCollection()){
                    
                        results.add(inventory);
                    
                    }
                    
                    return results;
                }
            
            }
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        }
    
    }
    
}
