/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface InventarioDelegate {
    
    Entities.Inventario find(Entities.InventarioPK inventarioPK);
    
    java.util.Vector<Entities.Inventario>findByFranchise(int idFranchise);
    
    Entities.Inventario findByName(String name,int idFranchise);
    
    void delete(Entities.InventarioPK key);
    
    void update(Entities.Inventario inventory);
    
    Entities.Inventario findByInventoryConverter(String inventoryId);
    
    void create(Entities.Inventario inventory);
    
    java.util.Vector<Entities.Inventario>getAutoInventory();
    
}
