/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.transaction.Transactional.TxType;
import jaxrs.service.InventarioHasSucursalFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@InventarioHasSucursalControllerQualifier





public class InventarioHasSucursalImplementation implements InventarioHasSucursal{
    
    private @javax.ejb.EJB InventarioHasSucursalFacadeREST inventarioHasSucursalFacadeREST;
    
    
    @Override
    public java.util.Vector<Entities.InventarioHasSucursal>findBySucursal(Entities.SucursalPK key){
    
        try{
        
            java.util.Vector<Entities.InventarioHasSucursal>results=new java.util.Vector<Entities.InventarioHasSucursal>(this.inventarioHasSucursalFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+key.getIdsucursal()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+"")).size());
            
            for(Entities.InventarioHasSucursal inventarioHasSucursal:this.inventarioHasSucursalFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+key.getIdsucursal()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+"")))
            {
            
                results.add(inventarioHasSucursal);
            
            }
          
            return results;
            
        }
        
        catch(javax.ejb.EJBException ex){
        
           Logger.getLogger(InventarioHasSucursalImplementation.class.getName()).log(Level.SEVERE,null,ex); 
        
           throw new javax.faces.FacesException(ex);
           
        }
        
    
    }
    
    public void addInventory(javax.faces.event.ActionEvent event){
    
        try{
        
            System.out.print(event.getComponent().getId());
        
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
            Logger.getLogger(InventarioHasSucursalImplementation.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    /**
	 * 
	 * @param inventarioHasSucursal
	 */
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	@Override
    public void create(Entities.InventarioHasSucursal inventarioHasSucursal){
    
    try{
    
        this.inventarioHasSucursalFacadeREST.create(inventarioHasSucursal);
        
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(InventarioHasSucursalImplementation.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    /**
	 * 
	 * @param key
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void delete(Entities.InventarioHasSucursalPK key){
    
    try{
    
        this.inventarioHasSucursalFacadeREST.remove(new PathSegmentImpl("bar;inventarioIdinventario="+key.getInventarioIdinventario()+";inventarioFranquiciaIdfranquicia="+key.getInventarioFranquiciaIdfranquicia()+";sucursalIdsucursal="+key.getSucursalIdsucursal()+";sucursalFranquiciaIdfranquicia="+key.getSucursalFranquiciaIdfranquicia()+""));
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(InventarioHasSucursalImplementation.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void update(Entities.InventarioHasSucursal inventarioHasSucursal){
    
        try{
        
            this.inventarioHasSucursalFacadeREST.edit(new PathSegmentImpl("bar;inventarioIdinventario="+inventarioHasSucursal.getInventarioHasSucursalPK().getInventarioIdinventario()+";inventarioFranquiciaIdfranquicia="+inventarioHasSucursal.getInventarioHasSucursalPK().getInventarioFranquiciaIdfranquicia()+";sucursalIdsucursal="+inventarioHasSucursal.getInventarioHasSucursalPK().getSucursalIdsucursal()+";sucursalFranquiciaIdfranquicia="+inventarioHasSucursal.getInventarioHasSucursalPK().getSucursalFranquiciaIdfranquicia()+""), inventarioHasSucursal);
        
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(InventarioHasSucursalImplementation.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
