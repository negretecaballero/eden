/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import SessionClasses.EdenLastSeen;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.FacesException;
import javax.inject.Named;
import jaxrs.service.LastSeenMenuFacadeREST;
import jaxrs.service.LastSeenProductoFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@Named("lastSeenController")
@LastSeenControllerQualifier
public class LastSeenController implements LastSeenControllerInterface{
    
    
    @EJB
    private LastSeenMenuFacadeREST lastSeenMenuFacadeREST;
    
    @EJB
    private LastSeenProductoFacadeREST lastSeenProductoFacadeREST;
    
    
    
    @Override
    public void addLastSeenMenu(Entities.Menu menu, Entities.LoginAdministrador loginAdministrador){
    
    try{
    
        Entities.LastSeenMenu lastSeenMenu=new Entities.LastSeenMenu();
        
        Date myDate = new Date();


System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(myDate));

System.out.println("My Date"+myDate);
         
         lastSeenMenu.setDateTime(myDate);
           
         lastSeenMenu.setMenu(menu);
    
         lastSeenMenu.setLoginAdministrador(loginAdministrador);
         
         Entities.LastSeenMenuPK lastSeenMenuPK=new Entities.LastSeenMenuPK();
         
         lastSeenMenuPK.setLoginAdministradorusername(loginAdministrador.getUsername());
         
         lastSeenMenuPK.setMenuIdmenu(menu.getIdmenu());
         
         lastSeenMenu.setLastSeenMenuPK(lastSeenMenuPK);
         
         lastSeenMenuFacadeREST.create(lastSeenMenu);
    }
    catch(EJBException ex){
    
        Logger.getLogger(LastSeenController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException("Exeption Caugth");
        
    }
    
    }
    
    @Override
    public void addLastSeenProducto(Entities.Producto product,Entities.LoginAdministrador loginAdministrador){
    
    try{
    
        if(!this.checkProductCoincidence(product, loginAdministrador)){
        
       Entities.LastSeenProducto lastSeenProducto=new Entities.LastSeenProducto();
       
        Date myDate = new Date();


System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(myDate));

System.out.println("My Date"+myDate);
       
       lastSeenProducto.setDateTime(myDate);
       
       
       lastSeenProducto.setLoginAdministrador(loginAdministrador);
       
       lastSeenProducto.setProducto(product);
       
       Entities.LastSeenProductoPK lastSeenProductoPK=new Entities.LastSeenProductoPK();
       
       lastSeenProductoPK.setLoginAdministradorusername(loginAdministrador.getUsername());
       
      // lastSeenProductoPK.setProductoIdproducto(product.getProductoPK());
       
       lastSeenProducto.setLastSeenProductoPK(lastSeenProductoPK);
       
       lastSeenProductoFacadeREST.create(lastSeenProducto);
        }
        else{
        
        System.out.print("the element is in the list already");
        
        }
    
    }
    catch(EJBException ex){
    
    Logger.getLogger(LastSeenController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new FacesException("Exception Caugth");
    
    }
    
    }
    
   public void removeLastSeenMenu(Entities.LastSeenMenuPK lastSeenMenuPK){
   
   try{
   
       lastSeenMenuFacadeREST.remove(new PathSegmentImpl("bar;loginAdministradorusername="+lastSeenMenuPK.getLoginAdministradorusername()+";menuIdmenu="+lastSeenMenuPK.getMenuIdmenu()+""));
       
   }
   catch(EJBException ex){
   
       Logger.getLogger(LastSeenController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new FacesException ("Exception Caugth");
   
   }
   
   }
   
   public void removeLastSeenProducto(Entities.LastSeenProductoPK lastSeenProductoPK){
   
   try{
   
    this.lastSeenProductoFacadeREST.remove(new PathSegmentImpl("bar;loginAdministradorusername="+lastSeenProductoPK.getLoginAdministradorusername()+";productoIdproducto="+lastSeenProductoPK.getProductoIdproducto()+""));
   
   }
   catch(EJBException ex)
   {
   
       Logger.getLogger(LastSeenController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new FacesException("Exception Cautgh");
   
   }
   
   }
   
   @Override
   public void updateLastSeen(Entities.LoginAdministrador loginAdministrador){
   
   try{
   
        Date today = new Date();


System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today));

System.out.println("My Date"+today); 
       
       if(loginAdministrador.getLastSeenMenuCollection().size()>=5){
       
           Entities.LastSeenMenuPK lastSeenMenuPK=null;
           
           java.util.List<Entities.LastSeenMenu>lastSeenMenuList=(java.util.List)loginAdministrador.getLastSeenMenuCollection();
           
           System.out.print(lastSeenMenuList.size());
           
           for(int i=0;i<(lastSeenMenuList.size());i++){
           
              if(lastSeenMenuList.get(i).getDateTime().before(today)){
              
                  today=lastSeenMenuList.get(i).getDateTime();
                  
                  lastSeenMenuPK=lastSeenMenuList.get(i).getLastSeenMenuPK();
              
              }
          
           }
       
           this.removeLastSeenMenu(lastSeenMenuPK);
       }
       else if(loginAdministrador.getLastSeenProductoCollection().size()>=5){
       
           Entities.LastSeenProductoPK lastSeenProductoPK=null;
           
           java.util.List<Entities.LastSeenProducto>lastSeenProductoList=(java.util.List)loginAdministrador.getLastSeenProductoCollection();
       
           for(int i=0; i<lastSeenProductoList.size();i++){
           
           
           if(lastSeenProductoList.get(i).getDateTime().before(today)){
           
           today=lastSeenProductoList.get(i).getDateTime();
           
           lastSeenProductoPK=lastSeenProductoList.get(i).getLastSeenProductoPK();
           
           }
           
           }
           
           this.removeLastSeenProducto(lastSeenProductoPK);
       
       }
   
   }
   catch(EJBException ex){
   
   Logger.getLogger(LastSeenController.class.getName()).log(Level.SEVERE,null,ex);
   
   throw new FacesException("Exception Caugth");
   
   }
   
   }
   
   
   private boolean checkProductCoincidence(Entities.Producto product,Entities.LoginAdministrador loginAdministrador){
   
   boolean found=false;
   
   for(Entities.LastSeenProducto aux:loginAdministrador.getLastSeenProductoCollection()){
   
   if(aux.getProducto().equals(product)){
   
       found=true;
       
       break;
   
   }
   
   }
   
   return found;
   
   }
   
   
   @Override
   public java.util.List<EdenLastSeen>lastSeenUser(Entities.LoginAdministrador loginAdministrador){
   
       try{
           
           java.util.List<EdenLastSeen>lastSeenList=new java.util.ArrayList<EdenLastSeen>();
           
           for(Entities.LastSeenProducto lastSeenProducto:loginAdministrador.getLastSeenProductoCollection()){
           
             //  lastSeenList.add(lastSeenProducto);
           
           }
           
           for(Entities.LastSeenMenu lastSeenMenu:loginAdministrador.getLastSeenMenuCollection()){
           
               lastSeenList.add(lastSeenMenu);
           
           }
           
           System.out.print(lastSeenList.size());
           
       return lastSeenList;
       }catch(EJBException ex){
       
           return null;
       
       }
               
   
   }
}
