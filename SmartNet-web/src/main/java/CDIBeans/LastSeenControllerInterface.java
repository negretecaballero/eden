/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import SessionClasses.EdenLastSeen;

/**
 *
 * @author luisnegrete
 */
public interface LastSeenControllerInterface {
  
    void addLastSeenMenu(Entities.Menu menu, Entities.LoginAdministrador loginAdministrador);
    
    void addLastSeenProducto (Entities.Producto producto, Entities.LoginAdministrador loginAdministrador);
    
    void updateLastSeen(Entities.LoginAdministrador loginAdministrador);
    
    java.util.List<EdenLastSeen>lastSeenUser(Entities.LoginAdministrador loginAdministrador);
}
