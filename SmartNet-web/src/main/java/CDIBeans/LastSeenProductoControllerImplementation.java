/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@LastSeenProductoControllerQualifier

public class LastSeenProductoControllerImplementation implements LastSeenProductoController {
    
    
    @javax.ejb.EJB
    private jaxrs.service.LastSeenProductoFacadeREST lastSeenProductoFacadeREST;
    
    
    @Override
    public void delete(Entities.LastSeenProductoPK key){
    
        try{
        
            this.lastSeenProductoFacadeREST.remove(new PathSegmentImpl("bar;loginAdministradorusername="+key.getLoginAdministradorusername()+";productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdcategoria="+key.getProductoCategoriaIdcategoria()+";productoCategoriaFranquiciaIdfranquicia="+key.getProductoCategoriaFranquiciaIdfranquicia()+""));
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
