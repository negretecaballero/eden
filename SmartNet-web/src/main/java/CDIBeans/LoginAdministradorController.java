/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.EdenString;
import Entities.LastSeenMenu;
import Entities.LastSeenMenuPK;
import Entities.LastSeenProducto;
import Entities.LastSeenProductoPK;
import Entities.LoginAdministrador;
import SessionClasses.Item;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import jaxrs.service.LastSeenMenuFacadeREST;
import jaxrs.service.LastSeenProductoFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import Clases.*;
import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */

@LoginAdministradorControllerQualifier
@RolesAllowed({"Guanabarauser", "GuanabaraSucAdmin", "GuanabaraFrAdmin", "EdenWorker"})
@Dependent

public class LoginAdministradorController extends BaseBacking implements LoginAdministradorControllerInterface,java.io.Serializable{
    
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
    
    @EJB
    private LastSeenProductoFacadeREST lastSeenProductoFacadeREST;
    
    @EJB
    private LastSeenMenuFacadeREST lastSeenMenuFacadeREST;
    
    @Inject @FileUploadBeanQualifier private 
    transient  FileUploadInterface fileUpload;
    
    @Inject 
    @CDIBeans.AppGroupControllerQualifier 
    private CDIBeans.AppGroupController appGroupController;
    
    @Inject
    @CDIBeans.AddressControllerQualifier
    private CDIBeans.AdressControllerInterface addressController;
    
    @Inject 
    @CDIBeans.ImageControllerQualifier
    private CDIBeans.ImageControllerDelegate imageController;
    
//    @Inject @Default private CDIBeans.SucursalhasloginAdministradorController sucursalHasLoginAdministradorController;
    
    @javax.inject.Inject 
    @CDIBeans.ConfirmationControllerQualifier
    private CDIBeans.ConfirmationDelegate confirmationController;
    
    @javax.inject.Inject
    @CDIEden.MailControllerQualifier
    private CDIEden.MailController _mailController;
	@javax.inject.Inject
	@CDIBeans.ApplicationControllerQualifier
	private ApplicationController _applicationController;
    
   
    /**
	 * 
	 * @param login
	 * @param item
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void AddLastSeen(LoginAdministrador login, Item item)
    {
    
        if(item instanceof Entities.Producto){
        
         if(!checkLastSeenExistence(login,(Entities.Producto)item))   {
         
         LastSeenProducto aux=new LastSeenProducto();
         
         LastSeenProductoPK auxPK=new LastSeenProductoPK();
         
         auxPK.setLoginAdministradorusername(login.getUsername());
         
//         auxPK.setProductoIdproducto(((Entities.Producto)item).getIdproducto());
         
         aux.setLastSeenProductoPK(auxPK);
         
         aux.setProducto((Entities.Producto)item);
         
         aux.setLoginAdministrador(login);
         
            
Date myDate = new Date();


System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(myDate));

System.out.println("My Date"+myDate);
         
         aux.setDateTime(myDate);
       
         lastSeenProductoFacadeREST.create(aux);
         
         }
            
        }
        else if(item instanceof Entities.Menu){
        
         if(!checkLastSeenExistence(login,(Entities.Menu)item))   {
         
             LastSeenMenu aux=new LastSeenMenu();
             
             LastSeenMenuPK auxPK=new LastSeenMenuPK();
             
             auxPK.setLoginAdministradorusername(login.getUsername());
             
             auxPK.setMenuIdmenu(((Entities.Menu)item).getIdmenu());
             
             aux.setLastSeenMenuPK(auxPK);
             
             aux.setLoginAdministrador(login);
             
             aux.setMenu((Entities.Menu)item);
             
             Date myDate = new Date();


System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(myDate));

System.out.println("My Date"+myDate);
         
             
             aux.setDateTime(myDate);
             
             lastSeenMenuFacadeREST.create(aux);
         
         }
        
        }
    
    }
    
    @Override
    public java.util.Vector<Item> listLastSeen(String login){
    
        LoginAdministrador log=loginAdministradorFacadeREST.find(login);
        
        java.util.Vector<Item>listItem=new java.util.Vector<Item>(log.getLastSeenProductoCollection().size()+log.getLastSeenMenuCollection().size());
        
   
        
        for(LastSeenProducto lastSeenProducto:log.getLastSeenProductoCollection()){
             
            
        listItem.add(lastSeenProducto.getProducto());
        
        }
        
        for(LastSeenMenu lastSeenMenu:log.getLastSeenMenuCollection()){
        
            listItem.add(lastSeenMenu.getMenu());
        
        }
        
        return listItem;
    
    }
    
    private boolean checkLastSeenExistence(LoginAdministrador login,Item item){
    
        if(item instanceof Entities.Producto){
        
            Entities.Producto product=(Entities.Producto)item;
            
         for(LastSeenProducto aux:login.getLastSeenProductoCollection()){
         
         if(aux.getProducto().equals(product)){
         
             return true;
             
           
         
         }
         
         }
            
            return false;
        
        }
        else if(item instanceof Entities.Menu){
        
            Entities.Menu menu=(Entities.Menu)item;
            
          for(LastSeenMenu aux:login.getLastSeenMenuCollection()){
          
              if(aux.getMenu().equals(menu)){
              
                  return true;
              
              }
          
          }
            
            return false;
            
        }
    return false;
    }
    
    
    /**
	 * 
	 * @param loginAdministrador
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void update(Entities.LoginAdministrador loginAdministrador){
    
        try{
        
        this.loginAdministradorFacadeREST.edit(loginAdministrador.getUsername(), loginAdministrador);
        
        }
        catch(EJBException ex){
        
            ex.printStackTrace(System.out);
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    /**
	 * 
	 * @param username
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public Entities.LoginAdministrador find(String username){
    
        try{
        
            return loginAdministradorFacadeREST.find(username);
        
        }
        catch(EJBException ex){
        
            return null;
        
        }
    
    
    }
    
    /**
	 * 
	 * @param user
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.LoginAdministrador user){
    
    try{
    
    this.loginAdministradorFacadeREST.create(user);
    
    }
    catch(EJBException ex){
    
    Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);   
    
    }
    
    
    }
    
    //User Role
    
    public void updateLastSeen(Entities.LoginAdministrador loginAdministrador){
    
    
    
    }
    
    @Override
    public double accountCompletion(String username){
    
        int n=0;
        
        Entities.LoginAdministrador user=this.find(username);
        
        if(user!=null){
        
            if(user.getUsername().equals("")){
            
                n++;
            
            }
            
            if(user.getPassword().equals("")){
            
                n++;
            
            }
            
            if(user.getName()!=null){
            
                n++;
            
            }
            
            if(user.getZipCode()!=null){
            
                n++;
            
            }
            
            if(user.getBirthday()!=null){
            
                n++;
            
            }
            
            if(user.getGender()!=null){
            
            n++;
                
            }
            
            if(user.getProfessionIdprofession()!=null){
            
            
                n++;
            
            }
            
            if(user.getImagenIdimagen()!=null){
            
                n++;
            
            }
        
        
        
        System.out.print("N value "+n);
        
        
        Integer number=new Integer(n);
        
        double d=number.doubleValue()*100.0;
        
        return d/800.0;
        
        }
        
        return 0.0;
    
    }
   
    
  @Override
  public String getImageRoot(String username){
 
     String root;
    
     HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
     
     
     Entities.LoginAdministrador loginAdministrador=this.find(username);
     
     System.out.print("LoginAdministradorUsername "+username);
     
     javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
       
     Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
     
     filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator,session.getAttribute("username").toString()+File.separator+"profile");
     
     
     if(loginAdministrador.getImagenIdimagen()!=null && this.imageController.find(loginAdministrador.getImagenIdimagen().getIdimagen())!=null ){
        
         root="";
         
       
         this.fileUpload.loadImagesServletContext(loginAdministrador.getImagenIdimagen(), username+File.separator+"profile");
         
         String image=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
     
         root=image;
         
         this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
         
         this.fileUpload.getPhotoList().remove(this.fileUpload.getImages().size()-1);
         
         
     }
     else{
     
     root=java.io.File.separator+"images"+java.io.File.separator+"NoUserPhoto.jpg";
     
     }
  
     
     return root;
 
 
 }
  
  
  /**
	 * 
	 * @param loginAdministrador
	 */
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
  public void delete(Entities.LoginAdministrador loginAdministrador){
  
  try{
  
      this.loginAdministradorFacadeREST.remove(loginAdministrador.getUsername());
  
  }
  catch(javax.ejb.EJBException ex){
  
      Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
  
  }
  
  }
  
  /**
	 * 
	 * @param username
	 * @param groupid
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
  public void deleteByAppGroupId(String username,String groupid){
  
      try{
          
          System.out.print("Username -"+username+"-"+groupid);
      
          Entities.LoginAdministrador user=this.find(username);
          
          if(user.getAppGroupCollection().size()==1){
          
              
              if(user.getImagenIdimagen()!=null){
          this.imageController.delete(user.getImagenIdimagen().getIdimagen());
              }
       for(Entities.Direccion address:user.getDireccionCollection()){
       
       this.addressController.delete(address);
       
       
       }
       
//       Entities.SucursalhasloginAdministradorPK key=new Entities.SucursalhasloginAdministradorPK();
       
       /*if(user.getSucursalhasloginAdministradorCollection()!=null && !user.getSucursalhasloginAdministradorCollection().isEmpty())
       {
     for(Entities.SucursalhasloginAdministrador aux:user.getSucursalhasloginAdministradorCollection()){
     
          this.sucursalHasLoginAdministradorController.delete(aux.getSucursalhasloginAdministradorPK());
     
     }
          }*/
     
     this.confirmationController.delete(this.loginAdministradorFacadeREST.find(username).getConfirmation());
     
       this.loginAdministradorFacadeREST.remove(username);
       
       
      
          }
          else{
          
              this.appGroupController.delete(groupid, username);
          
          }
      
      }catch(javax.ejb.EJBException ex){
      
          Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
      
      }
  
  }
    
  /**
	 * 
	 * @param user
	 * @param appGroup
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
  public void createAccount(Entities.LoginAdministrador user, Entities.AppGroup appGroup){
  
      try{
          
          if(!this.exists(user.getUsername()))
          {
          String password=user.getPassword();
          
          user.setPassword(EdenString.encription(user.getPassword()));
          
          Entities.Confirmation confirmation=new Entities.Confirmation();
              
          confirmation.setConfirmed(false);
              
          confirmation.setLoginAdministrador(user);
              
          confirmation.setLoginAdministradorusername(user.getUsername());
              
          user.setConfirmation(confirmation);
          
          user.setAppGroupCollection(new java.util.ArrayList<Entities.AppGroup>());
              
          user.getAppGroupCollection().add(appGroup);
          
          this.loginAdministradorFacadeREST.create(user);
          
          //this.appGroupController.create(appGroup);
      /*
              
              Entities.Confirmation confirmation=new Entities.Confirmation();
              
              confirmation.setConfirmed(false);
              
              confirmation.setLoginAdministrador(user);
              
              confirmation.setLoginAdministradorusername(user.getUsername());
              
              this.confirmationController.create(confirmation);
          
              user.setConfirmation(confirmation);
              */

          java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
          
          _mailController.sendMail(user.getUsername(), bundle.getString("RegistrationMail.Subject"), bundle.getString("RegistrationMail.Message")+" "+this.getAbsoluteURL()+"confirmation/confirmation.xhtml?faces-redirect=true&id="+user.getUsername()+"\n"+bundle.getString("RegistrationMail.Username")+": "+user.getUsername()+"\n"+bundle.getString("RegistrationMail.Password")+": "+password);
          
      }
          
          else{
          
              if(!this.containsAppGroup(user, appGroup.getAppGroupPK().getGroupid())){
              
                  System.out.print("APP GROUP TO FIND "+appGroup.getAppGroupPK().getGroupid());
                  
                  user=this.find(user.getUsername());
                  
                  user.getAppGroupCollection().add(appGroup);
                  
                  this.update(user);
              
              }
              else{
              
              System.out.print("THIS USER HAS GOT THIS APPGROUP ALREADY");
              
              }
              
          
          }
          
          
         }
      catch(EJBException | NullPointerException ex){
      
          ex.printStackTrace(System.out);
          
          Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
      
      }
      
      
  }
  
  private final boolean containsAppGroup(Entities.LoginAdministrador user,String groupName){
  
  try{
  
      if(user!=null){
      
          user=this.find(user.getUsername());
          
          if(user.getAppGroupCollection()!=null && !user.getAppGroupCollection().isEmpty()){
          
          for(Entities.AppGroup appGroup:user.getAppGroupCollection()){
          
              if(appGroup.getAppGroupPK().getGroupid().equals(groupName)){
              
                  return true;
              
              }
          
          }
          
          }
      
      }
      
      return false;
  }
  catch(NullPointerException ex){
  
      ex.printStackTrace(System.out);
      
      return false;
  
  }
  
  }
  
  /**
	 * 
	 * @param user
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
  public void edit(Entities.LoginAdministrador user){
  
      try{
      
          this.loginAdministradorFacadeREST.edit(user.getUsername(), user);
      
      }
      catch(javax.ejb.EJBException ex){
      
          Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
      
      }
  
  }
  
  @Override
  public java.util.Vector<Entities.Direccion>findAdrresses(String username){
  
  try{
  
      java.util.Vector<Entities.Direccion>results=new java.util.Vector<Entities.Direccion>(this.loginAdministradorFacadeREST.find(username).getDireccionCollection().size());
      
      for(Entities.Direccion direccion:this.loginAdministradorFacadeREST.find(username).getDireccionCollection()){
      
      results.add(direccion);
      
      }
      
     return results;
      
  }
  catch(javax.ejb.EJBException ex){
  
      Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
  
      return null;
  }
  
  }
  
  @Override
 public java.util.Vector<Entities.Sucursal>getSucursales(String username,String groupid){
 
 try{
 /*
 java.util.List<Entities.SucursalhasloginAdministrador>list=this.sucursalHasLoginAdministradorController.findByUsername(groupid, username);
 
 java.util.Vector<Entities.Sucursal>sucursalList=new java.util.Vector<Entities.Sucursal>(list.size());
 
 for(Entities.SucursalhasloginAdministrador aux:list){
 
     sucursalList.add(aux.getSucursal());
 
 }*/
  return null; 
 
 }
 
 catch(javax.ejb.EJBException ex){
 
     Logger.getLogger(LoginAdministradorController.class.getName()).log(Level.SEVERE,null,ex);
     
     throw new javax.faces.FacesException(ex);
 
 }
 
 }
 
 @Override
 public boolean exists(String loginAdministrador){
 
     try{
     
     return this.loginAdministradorFacadeREST.find(loginAdministrador)!=null;
     
     }
     catch(javax.ejb.EJBException ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         return false;
     
     }
    
 
 }
 
 
 @Override
 public Entities.AppGroup findAppGroup(Entities.LoginAdministrador loginAdministrador,String appGroupName){
 
     try{
     
         if(loginAdministrador.getAppGroupCollection()!=null && !loginAdministrador.getAppGroupCollection().isEmpty()){
         
             for(Entities.AppGroup appGroup:loginAdministrador.getAppGroupCollection()){
             
                 if(appGroup.getAppGroupPK().getGroupid().equals(appGroupName)){
                 
                     return appGroup;
                 
                 }
             
             }
         
         }
     
         return null;
     }
     catch(Exception | StackOverflowError ex){
    
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
     return null;
     
     }
 
 }
 
 @Override
 public int profilesNumber(String username){
 
     try{
     
         if(this.find(username)!=null){
         
             if(this.find(username).getAppGroupCollection()!=null && !this.find(username).getAppGroupCollection().isEmpty()){
             
                 return this.find(username).getAppGroupCollection().size();
                 
             }
         
         }
         
         return 0;
     
     }
     catch(Exception | StackOverflowError ex)
     {
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         return 0;
     
     }
 }
 
 @Override
 public void sendRegistrationMail(String Mail, String password){
 
 try{
 
      java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
          
        _mailController.sendMail(Mail, bundle.getString("RegistrationMail.Subject"), bundle.getString("RegistrationMail.Message")+" "+this.getAbsoluteURL()+"confirmation/confirmation.xhtml?faces-redirect=true&id="+Mail+"\n"+bundle.getString("RegistrationMail.Username")+": "+Mail+"\n"+bundle.getString("RegistrationMail.Password")+": "+password);
        
     
 }
 catch(NullPointerException ex){
 
     ex.printStackTrace(System.out);
 
 }
 
 }

	/**
	 * 
	 * @param username
	 * @param appGroup
	 */
	
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	public void deleteAccountAppGroup(String username, Entities.AppGroupPK appGroup) {
		
            try{
                
                System.out.print("USERNAME "+username);
            
                if(this.find(username)!=null){
                

                Entities.LoginAdministrador loginAdministrador=this.find(username);
                
                   System.out.print("USER EXISTS");
                
                   System.out.print(loginAdministrador.getAppGroupCollection().size());
                   
                if(loginAdministrador.getAppGroupCollection()!=null && !loginAdministrador.getAppGroupCollection().isEmpty()){
                
                    System.out.print("APPGROUP COLLECTION GREATER THAN 0");
                    
                    if(loginAdministrador.getAppGroupCollection().size()>1){
                    
                        System.out.print("DELETING APPGROUP "+username);
                        
                        this.appGroupController.delete(appGroup.getGroupid(), appGroup.getLoginAdministradorusername());
                    
                    }
                    else{
                    
                     //    this.appGroupController.delete(appGroup.getGroupid(), appGroup.getLoginAdministradorusername());
              
                        System.out.print("DELETING ACCOUNT "+username);
                        
                         this.delete(username);
                    
                    }
                
                }
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param username
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)

      
	public void delete(String username) {
		
            try{
            
                System.out.print("USERNAME "+username);
                
                Entities.LoginAdministrador user=this.find(username);
                /*
                if(_applicationController.findByMail(username)!=null && !_applicationController.findByMail(username).isEmpty()){
                
                    for(Entities.Application application:_applicationController.findByMail(username)){
                    
                        _applicationController.delete(application.getApplicationPK());
                    
                    }
                
                }*/
                
                this.loginAdministradorFacadeREST.remove(username);
            
            }
            
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
            }
            
	}

	@Override
	public void logout() {
	
            try{
            
                javax.servlet.http.HttpServletRequest request = (javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
                request.logout();
                
            }
            catch(NullPointerException | javax.servlet.ServletException  ex){
            
                
            
            }
            
        }
 
}
