/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.LoginAdministrador;
import SessionClasses.Item;

/**
 *
 * @author luisnegrete
 */
public interface LoginAdministradorControllerInterface {
    void AddLastSeen(LoginAdministrador login, Item item);
    
    java.util.Vector<Item> listLastSeen(String login);
    
    void update(Entities.LoginAdministrador loginAdministrador);
    
    Entities.LoginAdministrador find(String username);
    
    void create(Entities.LoginAdministrador user);
    
    double accountCompletion(String username);
    
    String getImageRoot(String username);
    
    void deleteByAppGroupId(String username,String groupid);
    
    void createAccount(Entities.LoginAdministrador loginAdministrador, Entities.AppGroup appGroup);
    
    void edit(Entities.LoginAdministrador user);
    
    java.util.Vector<Entities.Direccion>findAdrresses(String username);
    
    java.util.Vector<Entities.Sucursal>getSucursales(String username,String groupid);
    
    boolean exists(String loginAdministrador);
    
    Entities.AppGroup findAppGroup(Entities.LoginAdministrador loginAdministrador,String appGroupName);
    
    int profilesNumber(String username);
    
    void sendRegistrationMail(String Mail, String password);

	/**
	 * 
	 * @param username
	 * @param appGroup
	 */
	void deleteAccountAppGroup(String username, Entities.AppGroupPK appGroup);

	/**
	 * 
	 * @param username
	 */
	void delete(String username);

	void logout();
}
