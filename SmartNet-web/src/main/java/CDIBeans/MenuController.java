/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import jaxrs.service.AgrupaFacadeREST;
import jaxrs.service.MenuFacadeREST;


/**
 *
 * @author luisnegrete
 */


@MenuControllerQualifier
@Dependent
@javax.inject.Named("menuController")
public class MenuController implements  MenuControllerInterface{
   
    @EJB
    private MenuFacadeREST menuFacadeREST;
    
    @EJB
    private AgrupaFacadeREST agrupaFacadeREST;
    
    @javax.inject.Inject
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface productoController;
    
    @Inject
    @CDIBeans.AgrupaControllerQualifier
    private transient AgrupaControllerInterface agrupaController;
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @Override
    public Entities.Menu find(int id){
    
       
        try{
        return menuFacadeREST.find(id);
        }
        catch(EJBException ex){
        
        return null;
        
        }
    
    }
    
    
    @Override
    public java.util.Vector<Entities.Producto>products(int idMenu){
        
        try{
    
     
    Entities.Menu menu=menuFacadeREST.find(idMenu);
    
     java.util.Vector<Entities.Producto>productos=new java.util.Vector<Entities.Producto>(menu.getAgrupaCollection().size());
  
    
    for(Entities.Agrupa agrupa:menu.getAgrupaCollection()){
    
        if(!productos.contains(agrupa.getProducto())){
        
            productos.add(agrupa.getProducto());
        
        }
    
    }
    
     return productos;
     
        }
        
        catch(EJBException ex){
        
       return null;
        
        }
   
    
    }
    
    
    @Override
    public int menuQuantity(int idMenu){
    
    try{
    int minimum=0;
       
    List<Entities.Agrupa>agrupaList=agrupaFacadeREST.findIdMenu(idMenu);
    
    if(agrupaList!=null && !agrupaList.isEmpty()){
    
        Entities.Producto product=agrupaList.get(0).getProducto();
        
        minimum=productoController.productQuantity(product.getProductoPK())/agrupaList.get(0).getCantidadProducto();
    
    }
    
    for(Entities.Agrupa aux:agrupaList){
    
      Entities.Producto product= aux.getProducto();
      
      if(productoController.productQuantity(product.getProductoPK())<minimum){
      
          minimum=productoController.productQuantity(product.getProductoPK())/aux.getCantidadProducto();
          
      }
    
    }
    
    return minimum;
    }
    catch(EJBException ex){
    
    Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE,null,ex);
        return 0; 
    }
   
    }
    
    @Override
    public java.util.Vector<Entities.Menu>findBySucursal(Entities.SucursalPK sucursalPK){
    
        try{
        
            java.util.Vector<Entities.Menu>menuList=new java.util.Vector<Entities.Menu>(this.agrupaController.findBySucursal(sucursalPK).size());
            
            for(Entities.Agrupa agrupa:this.agrupaController.findBySucursal(sucursalPK)){
            if(!menuList.contains(agrupa.getMenu())){
            menuList.add(agrupa.getMenu());
            }
            
            }
            
            return menuList;
            
        }catch(EJBException ex){
        
            Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE,null,ex);
        
            throw new FacesException(ex);
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Menu>findByFranchise(int idFranchise){
    
    try{
    
        java.util.Vector<Entities.Menu>menuList=new java.util.Vector<Entities.Menu>(this.agrupaController.findByFranchise(idFranchise).size());
        
        if(this.agrupaController.findByFranchise(idFranchise)!=null && !this.agrupaController.findByFranchise(idFranchise).isEmpty())
        {
            
        System.out.print("AGRUPA COLLECTION SIZE "+this.agrupaController.findByFranchise(idFranchise).size());
        
        }
        
        for(Entities.Agrupa agrupa:this.agrupaController.findByFranchise(idFranchise)){
        
        if(!menuList.contains(agrupa.getMenu()))
        {
        
            menuList.add(agrupa.getMenu());
        
        }
        }
        
        return menuList;
    
    }
    catch(Exception ex){
    
        Logger.getLogger(MenuController.class.getName()).log(Level.SEVERE,null,ex);
        
       return null;
        
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Menu>findAll(){
    
    java.util.Vector<Entities.Menu>results=new java.util.Vector<Entities.Menu>(this.menuFacadeREST.findAll().size());
        
    for(Entities.Menu menu:this.menuFacadeREST.findAll()){
    
        results.add(menu);
    
    }
    
    return results;
    
    }
    
    
    @Override
    public java.util.List<Clases.MenuLayout>getMenuLayoutCollection(Entities.Franquicia franchise){
    
        try{
        
            java.util.List<Clases.MenuLayout>results=new java.util.ArrayList<Clases.MenuLayout>();
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                      
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(this.findByFranchise(franchise.getIdfranquicia())!=null && !this.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
            
            for(Entities.Menu menu:this.findByFranchise(franchise.getIdfranquicia())){
            
                String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"menu"+menu.getNombre();
            
                java.io.File file=new java.io.File("path");
                
                if(file.exists()){
                
                    filesManagement.cleanFolder(path);
                
                }
                else{
                
                   filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator,"menu"+menu.getNombre());
                
                }
                
                Clases.MenuLayout menuLayout=new Clases.MenuLayoutImplementation();
                
                menuLayout.setMenu(menu);
                
                menuLayout.setImages(new SessionClasses.EdenList<String>());
                
                if(menu.getImagenCollection()!=null && !menu.getImagenCollection().isEmpty()){
                
                    for(Entities.Imagen image:menu.getImagenCollection()){
                    
                        _fileUploadBean.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+"menu"+menu.getNombre());
                    
                        String cacheImage=_fileUploadBean.getImages().get(_fileUploadBean.getImages().size()-1);
                        
                        menuLayout.getImages().addItem(cacheImage);
                        
                        _fileUploadBean.getImages().remove(_fileUploadBean.getImages().size()-1);
                        
                        _fileUploadBean.getPhotoList().remove(_fileUploadBean.getPhotoList().size()-1);
                        
                    }
                
                }
                
                else{
                
                    menuLayout.getImages().addItem("/images/noImage.png");
                    
                }
             
                results.add(menuLayout);
                
            }
             
            return results;
            
            }
           
            return null;
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public void create(Entities.Menu menu){
    
    try{
    
        SessionClasses.EdenList<Entities.Agrupa>agrupaList=null;
        
        //Agrupa Collection
        if(menu.getAgrupaCollection()!=null && !menu.getAgrupaCollection().isEmpty()){
        
            agrupaList=new SessionClasses.EdenList<Entities.Agrupa>();
            
           for(Entities.Agrupa agrupa:menu.getAgrupaCollection()){
           
              Entities.Agrupa aux=new Entities.Agrupa();
               
              aux.setAgrupaPK(new Entities.AgrupaPK());
              
              aux=agrupa;
              
              agrupaList.addItem(aux);
           
           }
        
         }
        
        menu.setAgrupaCollection(new java.util.ArrayList<Entities.Agrupa>());
        
        menuFacadeREST.create(menu);
        
        if(agrupaList!=null && !agrupaList.isEmpty()){
        
            for(Entities.Agrupa agrupa:agrupaList){
            
                agrupa.getAgrupaPK().setMenuIdmenu(menu.getIdmenu());
                
                agrupa.setMenu(menu);
                
            this.agrupaController.create(agrupa);
            
            }
        
        }
        
    }
    catch(Exception | StackOverflowError ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    /**
	 * 
	 * @param idMenu
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
   public void delete(int idMenu){
   
   try{
   
     //Delete Agrupa
       if(this.find(idMenu)!=null){
       
           Entities.Menu menu=this.find(idMenu);
           
           if(menu.getAgrupaCollection()!=null && !menu.getAgrupaCollection().isEmpty()){
           
               for(Entities.Agrupa agrupa:menu.getAgrupaCollection()){
               
                   this.agrupaController.delete(agrupa.getAgrupaPK());
               
               }
           
           }
       
       }
       
       //Delete Menu
       
       menuFacadeREST.remove(idMenu);
       
   
   }
   catch(Exception  ex){
       
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       if(ex instanceof javax.transaction.RollbackException){
       
           delete(idMenu);
       
       }
       
   }
   
   }
   
   @Override
   public void loadImages(Entities.Menu menu){
   
   try{
   
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
      if(menu.getImagenCollection()!=null && !menu.getImagenCollection().isEmpty() && session.getAttribute("username")!=null){
      
          for(Entities.Imagen image:menu.getImagenCollection()){
          
              _fileUploadBean.loadImagesServletContext(image, session.getAttribute("username").toString());
          
          }
      
      }
       
   }
catch(Exception | StackOverflowError ex){

    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

    }
   
   }
    
   /**
	 * 
	 * @param menu
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
   public void update(Entities.Menu menu){
   
       try{
       
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
        if(session.getAttribute("auxiliarComponent")instanceof Entities.Menu){
        
            Entities.Menu sessionMenu=(Entities.Menu)session.getAttribute("auxiliarComponent");
            
            
            //Create or Update agrupa
            for(Entities.Agrupa agrupa:menu.getAgrupaCollection()){
            
            boolean found=false;
            
            for(Entities.Agrupa aux:sessionMenu.getAgrupaCollection()){
            
                if(agrupa.getAgrupaPK().equals(aux.getAgrupaPK())){
                
                    found=true;
                    
                    break;
                
                }
            
            }
            
            if(!found){
            
                System.out.print("CREATING AGRUPA "+agrupa.getAgrupaPK());
                
                agrupaController.create(agrupa);
            
            }
            
            else{
            
                agrupaController.update(agrupa);
            
            }
 
            }
            
            //Delete Agrupa
            for(Entities.Agrupa agrupa:sessionMenu.getAgrupaCollection()){
            
                boolean found=false;
                
                for(Entities.Agrupa aux:menu.getAgrupaCollection()){
                
                    if(agrupa.getAgrupaPK().equals(aux.getAgrupaPK())){
                    
                        found=true;
                        
                        break;
                    
                    }
                
                }
            
                if(!found){
                
                agrupaController.delete(agrupa.getAgrupaPK());
                
                }
                
            }
            
            menu.setAgrupaCollection(new java.util.ArrayList<Entities.Agrupa>());
            
            this.menuFacadeREST.edit(menu.getIdmenu(), menu);
        
        }
        
        
        
        
       }
       catch(Exception ex){
       
           if(ex instanceof javax.transaction.RollbackException){
           
               update(menu);
           
           }
       
       }
   
   }
   
   @Override
   public java.util.List<Entities.Menu>getAutoMenuList(){
   
       try{
       
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
           if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
           
               Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
               
               if(this.findByFranchise(franchise.getIdfranquicia())!=null && !this.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
              
                   return this.findByFranchise(franchise.getIdfranquicia());
               
               }
           
           }
           
           return null;
       }
       catch(Exception ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
           return null;
           
       }
   
   }

	/**
	 * 
	 * @param meal
	 */
	@Override
	public Entities.Franquicia getFranchise(Entities.Menu meal) {
		
            try{
            
                if(meal.getAgrupaCollection()!=null && !meal.getAgrupaCollection().isEmpty()){
                
                    for(Entities.Agrupa agrupa:meal.getAgrupaCollection()){
                    
                        if(agrupa.getProducto().getCategoria().getFranquicia()!=null){
                        
                               return agrupa.getProducto().getCategoria().getFranquicia();
                        
                        }
                        
                     
                    
                    }
                
                }
                
            return null;
            }
            catch(NullPointerException ex){
            
                return null;
            
            }
            
	}
   
}
