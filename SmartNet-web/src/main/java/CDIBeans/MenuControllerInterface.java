/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface MenuControllerInterface {
    
    
    Entities.Menu find(int id);
    
    java.util.Vector<Entities.Producto>products(int idMenu);
    
    int menuQuantity(int idMenu);
    
    java.util.Vector<Entities.Menu>findBySucursal(Entities.SucursalPK sucursalPK);
    
    java.util.Vector<Entities.Menu>findByFranchise(int idFranchise);
    
    java.util.Vector<Entities.Menu>findAll();
    
    java.util.List<Clases.MenuLayout>getMenuLayoutCollection(Entities.Franquicia franchise);
    
    void create(Entities.Menu menu);
    
    void delete(int idMenu);
    
    void loadImages(Entities.Menu menu);
    
    void update(Entities.Menu menu);
    
    java.util.List<Entities.Menu>getAutoMenuList();

	

	/**
	 * 
	 * @param meal
	 */
	Entities.Franquicia getFranchise(Entities.Menu meal);
}
