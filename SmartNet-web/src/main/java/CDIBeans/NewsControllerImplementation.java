/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.Calendar;
import java.util.Random;
import javax.faces.context.FacesContext;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@NewsControllerQualifier

public class NewsControllerImplementation implements NewsController,java.io.Serializable{
    
    @javax.ejb.EJB
    private jaxrs.service.NewsFacadeREST newsFacadeREST;
    
    @javax.ejb.EJB
    private jaxrs.service.NewsTypeFacadeREST newsTypeFacadeREST;
    
    @javax.inject.Inject
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoController tipoController;
    
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="06/09/2015",comments="Create a new pice of news")
    public void create(Entities.News news){
    
        try{
        
            news.setDate(Calendar.getInstance().getTime());
            
           newsFacadeREST.create(news);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    @Override
    @annotations.MethodAnnotations(author="Luis Megrete",date="06/09/2015",comments="List all newstype")
    public java.util.Vector<Entities.NewsType>getNewsTypeList(){
    
    try{
        
        java.util.Vector<Entities.NewsType>results=new java.util.Vector<Entities.NewsType>(this.newsTypeFacadeREST.findAll().size());
    
        for(Entities.NewsType newsType:this.newsTypeFacadeREST.findAll()){
        
            results.add(newsType);
        
        }
        
        return results;
        
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @annotations.MethodAnnotations(author="Luis Negrete",date="09/09/2015",comments="Trim List to limit value")
    private java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>> trimMap(int limit, SessionClasses.EdenList<Entities.News>news){
   
        java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hashMap=new java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>();
     
        java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>selectedItems=new java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>();
        
        
     try{
        
       for(Entities.News aux:news){
  
           
       Clases.EdenDateInterface date=new Clases.EdenDate();

       date.setDay(aux.getDate().getDate());
       
       date.setMonth(aux.getDate().getMonth());
       
       date.setYear(aux.getDate().getYear());
       
       
       if(!hashMap.containsKey(date)){
       
          java.util.List<Entities.News>newsList=new java.util.ArrayList<Entities.News>();
       
          newsList.add(aux);
          
          hashMap.put(date, newsList);
          
       }
       
       else{
       
           hashMap.get(date).add(aux);
       
       }
       
        }
       
       System.out.print("HASH MAP SIZE "+hashMap.size());
       
       for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>> entry:hashMap.entrySet()){
       
           System.out.print("Hash Map Key"+entry.getKey().toString());

           for(Entities.News auxi:entry.getValue()){
           
           System.out.print(entry.getKey() +"----"+auxi.getTitle());
           
           }
           
       }
       
       //Size of the map less or equal to the limit
       if(hashMap.size()<=limit){
       
           
           System.out.print("Size of the MAP "+this.getHashMapNetSize(hashMap));
           
           
           //Call UnderLimitMap
           selectedItems=this.underLimitMap(limit, hashMap);
           
       }
       //Size Of the map greater than limit
       else{
           
           System.out.print("NEWS LARGER THAN TRIM");
       
          for(int i=0;i<limit;i++){
          
          Clases.EdenDateInterface edenDate=this.findDate(selectedItems, hashMap);                 
              
          java.util.List<Entities.News>auxiliarList=hashMap.get(edenDate);
          
          Random random=new Random();
          
          int randomIndex=0;
          
          java.util.List<Entities.News>newsList=new java.util.ArrayList<Entities.News>();

          randomIndex=random.nextInt(((auxiliarList.size()-1)-0)+1)+0;
          
          newsList.add(auxiliarList.get(randomIndex));

          selectedItems.put(edenDate, newsList);
          
          System.out.print("List SelectedItems Map Size "+selectedItems.get(edenDate).size());
          
          }
          
          for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>> entry:selectedItems.entrySet()){
          
              System.out.print("Hash Limited Map "+entry.getKey());
          
          }
           
       }
       
       return selectedItems; 
       
     }
     catch(IllegalArgumentException | StackOverflowError ex){
     
         System.out.print("ParseException Caugth");
         
         return null;
     
     }
        
   
    }
    
    private SessionClasses.EdenList<Entities.News> findByType(String type,int tipo){
    
    try{
    
        SessionClasses.EdenList<Entities.News>results=new SessionClasses.EdenList<Entities.News>();
        
        System.out.print("Type : "+type+" ; Tipo : "+tipo+"");
        
        System.out.print("Total news "+this.newsFacadeREST.findAll().size());
        
        for(Entities.News news:this.newsFacadeREST.findAll()){
    
            if(news.getTipoId()!=null){
            
            if( (news.getNewsTypeId().getType().equals(type) && news.getTipoId().getIdtipo()==tipo)  || (news.getNewsTypeId().getType().equals("Global") && news.getTipoId().getIdtipo()==tipo))
            {
            
            results.addItem(news);
            
            }
            
            }
            else{
            
                if((news.getNewsTypeId().getType().equals(type)) || (news.getNewsTypeId().getType().equals("Global"))){
                
                    results.addItem(news);
                    
                }
            
            }
            
        }
    
        return results;
        
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="",comments="")
    public java.util.List<Entities.News>getOrderedByDate(int trim){
    
    try{
    
       java.util.HashMap<java.util.Date,java.util.List<Entities.News>>news=new java.util.HashMap<java.util.Date,java.util.List<Entities.News>>();
    
      
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
       java.util.List<Entities.News>result=new java.util.ArrayList<Entities.News>();
       
       if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
       
           Entities.Sucursal sucursal =(Entities.Sucursal)session.getAttribute("selectedOption");
           
           Entities.Tipo tipo=sucursal.getFranquicia().getIdSubtype().getIdTipo();
           
           java.util.List<Entities.News>results=(java.util.List<Entities.News>)this.newsFacadeREST.findByType("Branch", new PathSegmentImpl("bar;idtipo="+tipo.getIdtipo()+";nombre="+tipo.getNombre()+""));
           
         
           
           for(Entities.News aux:this.newsFacadeREST.findByType("Branch", null)){
           
               results.add(aux);
           
           }
           
           for(Entities.News aux:this.newsFacadeREST.findByType("Global", null)){
           
               results.add(aux);
           
           }
           
           System.out.print("Results For this Franchise "+results.size());
           
           SessionClasses.EdenList<Entities.News>newsList=new SessionClasses.EdenList<Entities.News>();
           
           for(Entities.News aux:results){

               newsList.addItem(aux);
           
           }
           
           for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>entry:this.trimMap(trim, newsList).entrySet())
           {
           
               if(entry.getValue()!=null && !entry.getValue().isEmpty()){
               
                   result.add(entry.getValue().get(0));
               
               }
           
           }
       }
       else if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
       
           Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
       
           
           SessionClasses.EdenList<Entities.News>results=this.findByType("Franchise",franchise.getIdSubtype().getIdTipo().getIdtipo());
           
            
           System.out.print("Results For this Franchise "+results.size());
           
           java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hash;
           
           hash=this.trimMap(trim, results);
           
           System.out.print("HASH MAP SIZE "+hash.size());
           
          SessionClasses.EdenList<Entities.News>newsList=new SessionClasses.EdenList<Entities.News>();
           
           for(Entities.News aux:results){

               newsList.addItem(aux);
           
           }
           
           for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>entry:hash.entrySet())
           {
           
                for(Entities.News auxi:entry.getValue()){
                
                     result.add(auxi);
                
                }

           }
           
       }
      
       
       
       return result;
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    private Clases.EdenDateInterface findDate(java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>> selectedItems,java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hashMap){
    
        //Find New Date Key in selected Keys
        
       Clases.EdenDateInterface edenDate=null; 
    
       Random rand=new Random();
       
       int randNum=rand.nextInt(((hashMap.size()-1)-0)+1)+0;
       
       System.out.print("Random Number "+randNum);
       
       SessionClasses.EdenList<Clases.EdenDateInterface>dateList=new SessionClasses.EdenList<Clases.EdenDateInterface>();
       
       for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>entry:hashMap.entrySet()){
       
       dateList.addItem(entry.getKey());
       
       }
       
       edenDate=dateList.get(randNum);
       
       if(checkCoincidencaMap(selectedItems,dateList.get(randNum))){
       
           edenDate=findDate(selectedItems,hashMap);
       
       }
       
       System.out.print("Selected Rand "+randNum);
       
       return edenDate;
       
       
    }
    
    
    private final boolean checkCoincidencaMap(java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>> hashMap,Clases.EdenDateInterface date){
    
        for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>entry:hashMap.entrySet()){
        
            {
            
                if(entry.getKey().equals(date)){
                
                return true;
                
                }
            
            }
        }
        
        return false;
        
    }
    
    
    private int getHashMapNetSize(java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hashMap){
    
    int size=0;
    
    for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>> entry:hashMap.entrySet()){
    
        size=size+entry.getValue().size();
    
    }
    
    return size;
    
    }
    
    
    private boolean findInMap(Entities.News news,java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hashMap){
    
        for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>entry:hashMap.entrySet()){
        
            for(Entities.News aux:entry.getValue()){
            
                if(news.equals(aux)){
                
                    return true;
                
                }
            
            }
        
        }
        
    return false;
    }
    
    
    @annotations.MethodAnnotations(author="Luis Negrete",date="20/09/2015",comments="Trim the map")
    private java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>underLimitMap(int trim, java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>> hashMap){
    
        java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>selectedItems=new java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>();
        
        
        if(this.getHashMapNetSize(hashMap)<=trim){
        
            
            
          selectedItems=hashMap;  
        
        }
        else{
        
            int max=hashMap.size()-1;
            
        while(true){
        
            Random rand=new Random();
            
            int hashKey=rand.nextInt((max-0)+1)+0;

            int maxNews= hashMap.get(this.getHashKey(hashKey, hashMap)).size()-1;
            
            int newsKey=rand.nextInt((maxNews-0)+1)+0;
            
            Entities.News news=hashMap.get(this.getHashKey(hashKey, hashMap)).get(newsKey);
            
            if(!this.findInMap(news, selectedItems)){
            
                if(selectedItems.containsKey(this.getHashKey(hashKey, hashMap))){
                
                    java.util.List<Entities.News>newsList=new java.util.ArrayList<Entities.News>();
                
                    newsList.add(news);
                    
                    selectedItems.put(this.getHashKey(hashKey, hashMap), newsList);
                    
                }
                else
                
                {
                
                  selectedItems.get(this.getHashKey(hashKey, hashMap)).add(news);
                
                }
                     
            
            }
        if(this.getHashMapNetSize(selectedItems)==trim){
        
            break;
            
        
        }
        }
        
        }
    
    return selectedItems;
    }
  
    
    private Clases.EdenDateInterface getHashKey(int index,java.util.HashMap<Clases.EdenDateInterface,java.util.List<Entities.News>>hashMap){
    
   int ind=0;
    
    for(java.util.Map.Entry<Clases.EdenDateInterface,java.util.List<Entities.News>>aux:hashMap.entrySet()){
    
    if(ind==index){
    
        return aux.getKey();
    
    }
    
    ind++;
    
    }
    
    return null;
    
    }
}
