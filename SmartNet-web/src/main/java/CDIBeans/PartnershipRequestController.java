/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface PartnershipRequestController {
    
    Entities.PartnershipRequest find(Entities.PartnershipRequestPK key);
    
    void create(Entities.PartnershipRequest partnershipRequest);
    
    java.util.Vector<Entities.PartnershipRequest>findByBuyerState(int idBuyer,Entitites.Enum.StateEnum state);
    
}
