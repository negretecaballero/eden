/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@PartnershipRequestControllerQualifier

public class PartnershipRequestControllerImplementation implements CDIBeans.PartnershipRequestController{
    

    
    @javax.ejb.EJB
    private jaxrs.service.PartnershipRequestFacadeREST _partnershipRequestFacadeREST;
    
    
    @Override
    public Entities.PartnershipRequest find(Entities.PartnershipRequestPK key){
    
        try{
        
            return _partnershipRequestFacadeREST.find(new PathSegmentImpl("bar;idBuyer="+key.getIdBuyer()+";idSupplier="+key.getIdSupplier()+""));
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public void create(Entities.PartnershipRequest partnershipRequest){
    
        try{

            _partnershipRequestFacadeREST.create(partnershipRequest);
        
        }
        catch(javax.ejb.EJBException ex){
        
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
    @Override
    public java.util.Vector<Entities.PartnershipRequest>findByBuyerState(int idBuyer,Entitites.Enum.StateEnum state){
    
        try{
        
            java.util.Vector<Entities.PartnershipRequest>results=new java.util.Vector<Entities.PartnershipRequest>(this._partnershipRequestFacadeREST.findByBuyerState(idBuyer, state).size());
            
            for(Entities.PartnershipRequest partnerShipRequest:this._partnershipRequestFacadeREST.findByBuyerState(idBuyer, state)){
            
                results.add(partnerShipRequest);
            
            }
            
            return results;
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
}
