/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.PedidoWorker;
import Clases.PedidoWorkerInterface;
import SessionClasses.CartItem;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.PedidoFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import org.primefaces.push.EventBusFactory;

/**
 *
 * @author luisnegrete
 */

@PedidoControllerQualifier

@javax.enterprise.context.Dependent

public class PedidoController implements PedidoControllerInterface{
    
    @EJB
    private PedidoFacadeREST pedidoFacadeREST;

    @javax.inject.Inject @CDIBeans.PedidoStateControllerQualifier 
    private CDIBeans.PedidoStateControllerInterface pedidoStateController;
  
    @javax.ejb.EJB
    private jaxrs.service.ProductohasPEDIDOFacadeREST productoHasPedidoFacadeREST;
    
    @javax.ejb.EJB
    private jaxrs.service.MenuhasPEDIDOFacadeREST menuHasPedidoFacadeREST;
    
   @javax.inject.Inject
   @CDIBeans.AddressControllerQualifier
   private transient  CDIBeans.AdressControllerInterface  addressController;
    
    @javax.inject.Inject 
    @CDIBeans.GPSCoordinatesControllerQualifier
    private transient CDIBeans.GpsCoordinatesControllerInterface gpsCoordinatesController;
    
    @javax.inject.Inject @CDIBeans.SucursalControllerQualifier
    private transient CDIBeans.SucursalControllerDelegate _sucursalController;
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private transient CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
    
    @javax.inject.Inject 
    
    private transient Controllers.CartController _cartController;
    
    @javax.inject.Inject
    @CDIBeans.RatehasPEDIDOControllerQualifier
    private transient CDIBeans.RatehasPEDIDOControllerInterface _rateHasPedidoController;
    
    @Override
    public List<PedidoWorkerInterface>loadUserPedidos(Entities.LoginAdministrador loginAdministrador,String stateName){
       
        
    try{
    
        java.util.List<PedidoWorkerInterface> pedidoList=new java.util.ArrayList<Clases.PedidoWorkerInterface>();
    
        for(Entities.Pedido pedido:pedidoFacadeREST.findByUser(loginAdministrador.getUsername(), stateName)){
        
            PedidoWorker aux=new PedidoWorker();
            
            aux.setPedido(pedido);
            
            java.util.List<CartItem>cartItemList=new java.util.ArrayList<CartItem>();
            
            for(Entities.ProductohasPEDIDO productohaspedido: pedido.getProductohasPEDIDOCollection()){
            
                
                
                CartItem item=new CartItem();
                
                item.setItem(productohaspedido.getProducto());
                
                item.setName(productohaspedido.getProducto().getNombre());
                
                item.setQuantity(productohaspedido.getQuantity());
                
                cartItemList.add(item);
            
            }
            
            for(Entities.MenuhasPEDIDO menuhaspedido:pedido.getMenuhasPEDIDOCollection()){
            
                CartItem item=new CartItem();
                
                item.setItem(menuhaspedido.getMenu());
                
                item.setName(menuhaspedido.getMenu().getNombre());
                
                item.setQuantity(menuhaspedido.getQuantity());
                
                cartItemList.add(item);
            
            }
            
            aux.setItemList(cartItemList);
            
            pedidoList.add(aux);
        
        }
        
        return pedidoList;
        
    }
    catch(EJBException ex)
    {
    
        Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
    
    }
    
    }
    
    
    @Override
    public java.util.List<Entities.Pedido>findBySucursal(Entities.SucursalPK sucursalPK){
    
    try{
    
        return this.pedidoFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.List<Entities.Pedido>findByFranchise(int idFranchise){
    
    try{
    
        return this.pedidoFacadeREST.findByIdFranquicia(idFranchise);
    
    }
    catch(javax.ejb.EJBException ex){
    
    Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
   
    @Override
    public java.util.List<Entities.Pedido>findByState(Entities.PedidoState pedidoState,Entities.SucursalPK sucursalPK){
    
        try{
        
            return this.pedidoFacadeREST.findByState(pedidoState.getState(), new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
        
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    
    }
   
    @Override
    public java.util.List<Entities.Pedido>findByDate(java.util.Date date,Entities.SucursalPK sucursalPK){
    
        try{
        
            Clases.EdenDateInterface edenDate=new Clases.EdenDate();
            
            java.util.List<Entities.Pedido>results=new java.util.ArrayList<Entities.Pedido>();
            
            for(Entities.Pedido pedido:this.findBySucursal(sucursalPK)){
            
                if(edenDate.compareSingleDate(pedido.getDate(),date)){
                
                    results.add(pedido);
                
                }
            
            }
        return results;
        }
        catch(Exception ex){
        
            Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
        
    }
    
    @Override
    public java.util.List<Entities.Pedido>filterByState(java.util.List<Entities.Pedido>orders,String orderState){
    
        try{
        
            java.util.List<Entities.Pedido>results=new java.util.ArrayList<Entities.Pedido>();
            
            for(Entities.Pedido order:orders){
            
            if(order.getPedidoStatestateId().getState().equals(orderState)){
            
                results.add(order);
            
            }
            
            }
            
            return results;
            
        }
        catch(Exception ex){
        
            Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public java.util.List<Entities.Pedido>findByInterval(java.util.Date from,java.util.Date to,Entities.SucursalPK sucursalPK){
    
        try{
        
            PathSegmentImpl pathSegment=new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+"");
            
          return this.pedidoFacadeREST.findByDateInterval(from, to,pathSegment);
        
        }
        catch(javax.ejb.EJBException ex){
        
        Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    /*public java.util.List<Entities.Pedido>orderedByState(){
    
    
    }*/
    
    @Override
    public double dayAverage(Entities.SucursalPK sucursalPK){
    
    try{
    
        java.util.List<Entities.Pedido>orders=this.pedidoFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
        
        for(Entities.Pedido order:orders){
        
        System.out.print("Order Date "+order.getDate());
        
        
        }
        
        if(orders.size()>1){
        
            java.util.Date date1=orders.get(0).getDate();
            
            java.util.Date date2=orders.get(orders.size()-1).getDate();
        
            Clases.EdenDateInterface edenDate=new Clases.EdenDate();
            
            int daysDifference =edenDate.daysBetween(date1, date2);
            
            if(daysDifference>0){
            
            return (double)(orders.size()/daysDifference);
            
            }
            else{
            
                return orders.size();
            
            }
            
            
        }
        else{
        
            return orders.size();
        
        }
        
     
    }
    catch(javax.ejb.EJBException ex){
    
    Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void deletePedidoHasProducto(Entities.ProductohasPEDIDOPK key){
    
    try{
    
        this.productoHasPedidoFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getproductoCategoriaFranquiciaIdFranquicia()+";pEDIDOidPEDIDO="+key.getPEDIDOidPEDIDO()+""));
    
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    
    @annotations.MethodAnnotations(author="Luis Negrete", date="23/10/2015",comments="Create order")
    @Override
    public void createOrder(String username,Entities.Direccion direccion,java.util.Date date,String type,String description){
    
        try{
        
            Entities.Pedido order=new Entities.Pedido();
            
            //Create coordinates for order
            this.gpsCoordinatesController.create(direccion.getGpsCoordinatesidgpsCoordinated());
            
            
            //Create address
            this.addressController.create(direccion);
            
            
            //Set Address of order
            order.setDireccion(direccion);
            
            //Set date of order
            order.setDate(Clases.EdenDate.currentDate());
            
            //Set description
            order.setDescription(description);
            
            //Set Type ex:contaentrega
            order.setType(type);
            
            //set Sucursal
            
            
            //setLoginAdministrador
            order.setLoginAdministradorusername(loginAdministradorController.find(username));
            
            //setStatePedido
            order.setPedidoStatestateId(pedidoStateController.findByName("Pending"));
            
            //create order by sucursal
            for(Clases.CartControllerLayout aux:_cartController.getCartBySucursal()){
            
                
            order.setSucursal(aux.getSucursal());
            
            
            
            this.pedidoFacadeREST.create(order);
            
            this._rateHasPedidoController.create((short)6, order);
            
            org.primefaces.push.EventBus bus=EventBusFactory.getDefault().eventBus();
               
            for(Entities.LoginAdministrador auxi:_sucursalController.getWorkers(order.getSucursal())){
            
                System.out.print("Publish in "+java.io.File.separator+order.getSucursal().getFranquicia().getIdfranquicia()+java.io.File.separator+order.getSucursal().getSucursalPK().getIdsucursal()+java.io.File.separator+auxi.getUsername());
                
                bus.publish(java.io.File.separator+order.getSucursal().getFranquicia().getIdfranquicia()+java.io.File.separator+order.getSucursal().getSucursalPK().getIdsucursal(), new javax.faces.application.FacesMessage("Nuevo Pedido ID"+order.getIdPEDIDO()+" Usuario "+order.getLoginAdministradorusername()));
            
            }
            
            }
            
            
            for(Clases.CartControllerLayout aux:_cartController.getCartBySucursal()){
            
                
                
                for(SessionClasses.CartItem item:aux.getCartList()){
                
                    if(item.getItem() instanceof Entities.Producto){
                    
                        Entities.Producto product=(Entities.Producto)item.getItem();
                        
                        Entities.ProductohasPEDIDO productoHasPedido=new Entities.ProductohasPEDIDO();
                        
                        Entities.ProductohasPEDIDOPK productoHasPedidoPK=new Entities.ProductohasPEDIDOPK();
                        
                        productoHasPedidoPK.setPEDIDOidPEDIDO(order.getIdPEDIDO());
                        
                        productoHasPedidoPK.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
                        
                        productoHasPedidoPK.setProductoIdproducto(product.getProductoPK().getIdproducto());
                        
                        productoHasPedidoPK.setproductoCategoriaFranquiciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
                        
                        productoHasPedido.setPedido(order);
                        
                        productoHasPedido.setProducto((Entities.Producto)item.getItem());
                        
                        productoHasPedido.setQuantity(item.getQuantity());
                        
                        productoHasPedido.setProductohasPEDIDOPK(productoHasPedidoPK);
                        
                        this.productoHasPedidoFacadeREST.create(productoHasPedido);
                    
                    }
                    else if(item.getItem() instanceof Entities.Menu){
                    
                        Entities.Menu menu=(Entities.Menu)item.getItem();
                        
                        Entities.MenuhasPEDIDO menuHasPedido=new Entities.MenuhasPEDIDO();
                        
                        Entities.MenuhasPEDIDOPK menuHasPedidoPK=new Entities.MenuhasPEDIDOPK();
                        
                        menuHasPedidoPK.setMenuIdmenu(menu.getIdmenu());
                        
                        menuHasPedidoPK.setPEDIDOidPEDIDO(order.getIdPEDIDO());
                        
                        menuHasPedido.setMenuhasPEDIDOPK(menuHasPedidoPK);
                        
                        menuHasPedido.setMenu(menu);
                        
                        menuHasPedido.setPedido(order);
                        
                        menuHasPedido.setQuantity(item.getQuantity());
                        
                        this.menuHasPedidoFacadeREST.create(menuHasPedido);
                    
                    }
                
                }
                
                
            
            }
            
         
         
            
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    
    @Override
     public Entities.Pedido findEspecificFranchise(int idPedido, int idFranchise){
   
   try{
   
       return this.pedidoFacadeREST.findSpecificFranchise(idPedido, idFranchise);
   
   }
   catch(javax.ejb.EJBException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       return null;
   
   }
   
   }
     
     @Override
     public double averagePerBranch(Entities.SucursalPK sucursalPK){
     
         try{
         
       double average=0;
          
        if((this.findByFranchise(sucursalPK.getFranquiciaIdfranquicia())!=null && !this.findByFranchise(sucursalPK.getFranquiciaIdfranquicia()).isEmpty())&&(this.findBySucursal(sucursalPK)!=null && !this.findBySucursal(sucursalPK).isEmpty())){
        
        System.out.print("Average is not null");    
        
        Integer totalFranchise=this.findByFranchise(sucursalPK.getFranquiciaIdfranquicia()).size();
        
        Integer totalSucursal=this.findBySucursal(sucursalPK).size();
        
        
        average= totalSucursal.doubleValue()/totalFranchise.doubleValue();
        }
        
        System.out.print("Average Value "+average);
        
        return average;
         
         }
         catch(Exception | StackOverflowError ex){
         
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
             
             return .0;
         
         }
     
     }
    
}
