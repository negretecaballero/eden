/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.PedidoWorkerInterface;

/**
 *
 * @author luisnegrete
 */
public interface PedidoControllerInterface {
    
    Entities.Pedido findEspecificFranchise(int idPedido, int idFranchise);
    
    java.util.List<PedidoWorkerInterface>loadUserPedidos(Entities.LoginAdministrador loginAdministrador,String stateName);
    
    java.util.List<Entities.Pedido>findByInterval(java.util.Date from,java.util.Date to,Entities.SucursalPK sucursalpK);
    
    java.util.List<Entities.Pedido>findBySucursal(Entities.SucursalPK sucursalPK);
    
    java.util.List<Entities.Pedido>findByFranchise(int idFranchise);
    
    java.util.List<Entities.Pedido>findByState(Entities.PedidoState state,Entities.SucursalPK sucursalPK);

    java.util.List<Entities.Pedido>findByDate(java.util.Date date,Entities.SucursalPK sucursalPK);

    java.util.List<Entities.Pedido>filterByState(java.util.List<Entities.Pedido>orders,String orderState);

    double dayAverage(Entities.SucursalPK sucursalPK);
    
    void deletePedidoHasProducto(Entities.ProductohasPEDIDOPK key);
    
    void createOrder(String username,Entities.Direccion direccion,java.util.Date date,String type,String description );

    double averagePerBranch(Entities.SucursalPK sucursalPK);

}
