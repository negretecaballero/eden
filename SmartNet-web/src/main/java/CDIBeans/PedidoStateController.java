/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import jaxrs.service.PedidoStateFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@PedidoStateControllerQualifier

public class PedidoStateController implements PedidoStateControllerInterface{
    
    @EJB
    private PedidoStateFacadeREST pedidoStateFacadeREST;
    
    @Override
   public List<Entities.PedidoState>getPedidoStateList(){
   
   return pedidoStateFacadeREST.findAll();
   
   } 
   
   @Override
   public Entities.PedidoState findByName(String name){
   
       try{
       
           return this.pedidoStateFacadeREST.findByState(name);
           
       }
       catch(javax.ejb.EJBException ex){
       
           Logger.getLogger(PedidoStateController.class.getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
           
       }
   
   }
    
  
   
}
