/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface PedidoStateControllerInterface {
    
    List<Entities.PedidoState>getPedidoStateList();
    
    Entities.PedidoState findByName(String name);
}
