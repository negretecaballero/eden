/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import jaxrs.service.PhoneFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@PhoneControllerQualifier
public class PhoneController implements PhoneControllerInterface{
    
    @javax.ejb.EJB
    private PhoneFacadeREST phoneControllerFacade;
    
    
    @Override
    public void create(Entities.Phone phone){
    
        try{
        
           phoneControllerFacade.create(phone);
        
        }
        catch(javax.ejb.EJBException ex){
        
        
        }
    
    }
    
    
    @Override
    public java.util.List<Entities.Phone>findByUsername(String username){
    
        try{
        
            return this.phoneControllerFacade.findByName(username);
        
        }
        catch(javax.ejb.EJBException ex){
        
        Logger.getLogger(PhoneController.class.getName()).log(Level.SEVERE,null,ex);
        
        return null;
        
        }
    
    }
    
    
    @Override
    public boolean findSimilar(String phoneNumber){
    
    
    try{
    System.out.print("Phone to find "+phoneNumber);
    
        if(this.phoneControllerFacade.findByNumber(phoneNumber)!=null){
        
            System.out.print("Phone found");
            
            return true;
        
        }
        
        System.out.print("Phone not found");
        
        return false;
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(PhoneController.class.getName()).log(Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
    }
    
    }
    
    @Override
    public void delete(String phoneNumber){
    
        try{
        
            this.phoneControllerFacade.remove(phoneNumber);
        
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(PhoneController.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void checkPhoneUserList(java.util.List<Entities.Phone> phoneList,String username){
    
        try{
        
        
        }
        catch(Exception ex){
        
        Logger.getLogger(PhoneController.class.getName()).log(Level.SEVERE,null,ex);    
        
        }
    
    
    }
    
    
}
