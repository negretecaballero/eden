/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface PhoneControllerInterface {
   
    void create(Entities.Phone phone);
    
    boolean findSimilar(String phoneNumber);
    
    java.util.List<Entities.Phone>findByUsername(String username);
    
    void delete(String username);
}
