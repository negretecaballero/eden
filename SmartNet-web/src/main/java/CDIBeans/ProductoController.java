/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayout;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import Entities.Compone;
import Entities.Producto;
import Entities.SimilarProducts;
import Entities.SucursalPK;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional.TxType;
import jaxrs.service.ComponeFacadeREST;
import jaxrs.service.ProductoFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@ProductoControllerQualifier
@javax.inject.Named("productoController")
public class ProductoController implements ProductoControllerInterface{
    @EJB private ProductoFacadeREST productoFacadeREST;
    
    
    @EJB private ComponeFacadeREST componeFacadeREST;
    
    @javax.ejb.EJB
    private jaxrs.service.LastSeenProductoFacadeREST lastSeenProductoFacadeREST;
    
    @javax.ejb.EJB
    private jaxrs.service.SimilarProductsFacadeREST similarProductsFacadeREST;
    
    @javax.inject.Inject
    @CDIBeans.ComponeControllerQualifier
    private CDIBeans.ComponeController _componeController;
    
    
    
    @Inject 
    @CDIBeans.FileUploadBeanQualifier
    private FileUploadInterface  fileUpload;
    
  
    @javax.inject.Inject
    @CDIBeans.FranchiseControllerQualifier
    private CDIBeans.FranchiseControllerInterface franchiseController;
    
  
    @javax.inject.Inject 
    @CDIBeans.ImageControllerQualifier
    private CDIBeans.ImageControllerDelegate imageController;
    
    
    @javax.inject.Inject
    @CDIBeans.AgrupaControllerQualifier
    private CDIBeans.AgrupaControllerInterface agrupaController;
    
    @javax.inject.Inject @CDIBeans.ComponeControllerQualifier
    private CDIBeans.ComponeController componeController;
    
    
    
    @javax.inject.Inject
    @CDIBeans.AdditionControllerQualifier
    private CDIBeans.AdditionControllerDelegate additionController;
    
    @javax.inject.Inject 
    @CDIBeans.PedidoControllerQualifier
    private CDIBeans.PedidoControllerInterface pedidoController;
   
    @javax.inject.Inject 
    @CDIBeans.SucursalControllerQualifier
    private CDIBeans.SucursalControllerDelegate sucursalController;
    
    @javax.ejb.EJB private jaxrs.service.BarcodeFacadeREST barcodeFacadeREST;
    
    
    
    @Override
    public int productQuantity(Entities.ProductoPK  productId){
    
    List<Compone>componeList=componeFacadeREST.findIdProducto(new PathSegmentImpl("bar;idproducto="+productId.getIdproducto()+";categoriaIdcategoria="+productId.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+productId.getCategoriaFranquiciaIdfranquicia()+""));
    
    if(componeList!=null && !componeList.isEmpty()){
    
        Compone comp=componeList.get(0);
        
        
        double minimum=0;
      //double minimum=(comp.getInventario().getCantidad())/comp.getCantidadInventario();
        
    for(Compone aux:componeList){
    
      /*  double operation=(aux.getInventario().getCantidad())/aux.getCantidadInventario();
        
        if(minimum<operation){
        
            minimum=operation;
        
        }
      */  
    }
    
    
    return (int)minimum;
    }
    
    else{
    
        return 0;
    
    }
    }
    
    @Override
    public List<SimilarProductsLayoutInterface>findSimilar(Entities.ProductoPK idProducto){
    try{
        List<SimilarProductsLayoutInterface>similarList=new <SimilarProductsLayout>ArrayList();
        
        Entities.Producto product=productoFacadeREST.find(idProducto);
       
        HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(!product.getSimilarProductsCollection().isEmpty() || !product.getSimilarProductsCollection1().isEmpty()){
        
         System.out.print("Similar Products is not empty");   
            
          FilesManagementInterface filesManagement=new FilesManagement();
       
         
          File file=new File("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/"+"SimilarProducts");
       
        if(file.exists()){
        if(file.isDirectory()){
            filesManagement.cleanFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/"+"SimilarProducts");
        }
        }
         
        
        filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/", "SimilarProducts");
        
       
        
        }
        
        System.out.print("Selected Find Similar "+product.getNombre());
        
        for(SimilarProducts aux:product.getSimilarProductsCollection()){
            
            SimilarProductsLayoutInterface productLayout=new SimilarProductsLayout();
        
        System.out.print("Similar size "+product.getSimilarProductsCollection().size());
        
        Entities.Producto productSimilar=aux.getProducto1();
        
        productLayout.setProduct(productSimilar);
        
        productLayout.setType("similar");
        
        productLayout.setPrice(aux.getPriceDiference());
        
        
        System.out.print("Similar Product name "+productSimilar.getNombre());
        
             
        FilesManagementInterface filesManagement=new FilesManagement();
       
        
        
        filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/"+"SimilarProducts/",productSimilar.getNombre());
        
        for(Entities.Imagen image:productSimilar.getImagenCollection()){
        
            String path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/SimilarProducts/"+productSimilar.getNombre();
            
            File file=new File(path);
            
            if(file.exists())
            {
            
                System.out.print("This path Exists");
                
            fileUpload.AddPhoto(image, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/SimilarProducts/"+productSimilar.getNombre(), session.getAttribute("username")+"/"+product.getNombre()+"/SimilarProducts/"+productSimilar.getNombre(), 500, 500);
        
            productLayout.getProductImages().add(fileUpload.getImages().get(fileUpload.getImages().size()-1));
            }
        }
        
        fileUpload.getImages().clear();
        
        fileUpload.getPhotoList().clear();
        
         similarList.add(productLayout);
       
        }
        
        for(SimilarProducts aux:product.getSimilarProductsCollection1()){
        
        System.out.print("Similar 1 size "+product.getSimilarProductsCollection1().size());
        
        Entities.Producto productSimilar=aux.getProducto();
        
        SimilarProductsLayoutInterface productLayout=new SimilarProductsLayout();
        
        productLayout.setPrice(aux.getPriceDiference()*-1);
        
        productLayout.setProduct(productSimilar);
        
        productLayout.setType("similar1");
        
        
        System.out.print("Similar 1 Product name "+productSimilar.getNombre());
       
        FilesManagementInterface filesManagement=new FilesManagement();
        
        filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/"+"SimilarProducts/",productSimilar.getNombre());
        
        
        
          for(Entities.Imagen image:productSimilar.getImagenCollection()){
        
            fileUpload.AddPhoto(image, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre()+"/"+"SimilarProducts"+"/"+productSimilar.getNombre(), session.getAttribute("username")+"/"+product.getNombre()+"/SimilarProducts"+"/"+productSimilar.getNombre(), 500, 500);
        
            productLayout.getProductImages().add(fileUpload.getImages().get(fileUpload.getImages().size()-1));
            
        }
        
        fileUpload.getImages().clear();
        
        fileUpload.getPhotoList().clear();
        
         similarList.add(productLayout);
        
        
        }
        
        return similarList;
    }
    catch(EJBException ex){
    
        return null;
    
    }
    
    }
    
    @Override
    public java.util.List<String> getImages(Entities.ProductoPK idProducto){
    
        try{
            
           java.util.List<String>images=new java.util.ArrayList<String>();
        
           Entities.Producto product=this.find(idProducto);                   
          
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           
           java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products");
           
           if(!file.exists()){
           
               Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
               
               filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, "products");
           
           }
           
           java.io.File file2=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
           
           if(!file2.exists()){
           
             Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
            
             filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator, product.getNombre());
           
           }
           
       
            
           if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
           
              for(Entities.Imagen photo:product.getImagenCollection()){
              
                   this.fileUpload.loadImagesServletContext(photo, session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
         
                   images.add(this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1));
              }  
           }
           
           else{
           
               images.add("/images/noImage.png");
           
           }
       
           System.out.print("After Product Controller sizes "+this.fileUpload.getPhotoList().size()+"-"+this.fileUpload.getImages().size());
          
        return images;
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE,null,ex);
        
            throw new javax.faces.FacesException(ex);
        }
        
        catch(NullPointerException ex){
        
            Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    @Override
    public java.util.List<String>getImageCollection(Entities.ProductoPK key){
    
    if(this.find(key)!=null){
        
        java.util.List<String>results=new java.util.ArrayList<String>();
    
        Entities.Producto product=this.find(key);
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
       
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
        
        String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre();
        
        java.io.File file=new java.io.File(path);
        
         if(file.exists()){
          
              filesManagement.cleanFolder(path);
              
          }
          else{
              
              filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, product.getNombre());
          
          }
        
        if(product.getImagenCollection()!=null  && !product.getImagenCollection().isEmpty()){
        
         
             for(Entities.Imagen image:product.getImagenCollection()){
             
                 this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());

                 String cache=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
                 
                 results.add(cache);
                 
                 this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
                 
                 this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
                 
             }
          
         
            
        }
        else{
        
            results.add(java.io.File.separator+"images"+java.io.File.separator+"noImage.png");
            
        }
        
        return results;
    }    
        
        
    return null;
    
    }
    
    @Override
    public void deleteSimilarProducts(Entities.SimilarProductsPK key){
    
    try{
    
        this.similarProductsFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+key.getProductoIdproducto1()+";productoCategoriaIdCategoria1="+key.getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+key.getProductoCategoriaFranquiciaIdFranquicia1()+""));
    
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void deleteLastSeenProducto(Entities.LastSeenProductoPK key){
    
    try{
    
        this.lastSeenProductoFacadeREST.remove(new PathSegmentImpl("bar;loginAdministradorusername="+key.getLoginAdministradorusername()+";productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdcategoria="+key.getProductoCategoriaIdcategoria()+";productoCategoriaFranquiciaIdfranquicia="+key.getProductoCategoriaFranquiciaIdfranquicia()+""));
    
    }
    catch(javax.ejb.EJBException | NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
   
    
    /**
	 * 
	 * @param key
	 */
	@annotations.MethodAnnotations(author="luis", date="18/08/2015", comments="Delete Product for Franchise")
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void deleteFromFranchise(Entities.ProductoPK key){  
        
        try{
        
           Entities.Producto product=this.find(key);
           
           //Delete Image
          
           for(Entities.Imagen image:product.getImagenCollection()){
           
             this.imageController.delete(image.getIdimagen());
           
           }
           
           //Delete Agrupa
           
           for(Entities.Agrupa agrupa:product.getAgrupaCollection()){
           
           this.agrupaController.delete(agrupa.getAgrupaPK());
           
           }
           
           
           //Compone
           
           for(Entities.Compone compone:product.getComponeCollection()){
           
               this.componeController.delete(compone.getComponePK());
           
           }
           
           //Sale
           /*
           for(Entities.SaleHasProducto saleHasProducto:product.getSaleHasProductoCollection()){
           
           this.saleController.deleteSaleHasProducto(saleHasProducto.getSaleHasProductoPK());
           
           }*/
           
           //LastSeenProducto
           
           for(Entities.LastSeenProducto lastSeenProducto:product.getLastSeenProductoCollection()){
           
               this.deleteLastSeenProducto(lastSeenProducto.getLastSeenProductoPK());
           
           }
           
           //Addition
           
           for(Entities.Addition addition:product.getAdditionCollection()){
           
           addition.getProductoCollection().remove(product);
           
           this.additionController.edit(addition);
           
           }
           
           //Similarproducts
           for(Entities.SimilarProducts similarProducts:product.getSimilarProductsCollection()){
           
           this.deleteSimilarProducts(similarProducts.getSimilarProductsPK());
           
           }
           
           for(Entities.SimilarProducts similarProducts:product.getSimilarProductsCollection1()){
           
           this.deleteSimilarProducts(similarProducts.getSimilarProductsPK());
           
           }
           
           //Producto has pedido
           
           for(Entities.ProductohasPEDIDO productoHasPedido:product.getProductohasPEDIDOCollection()){
           
           this.pedidoController.deletePedidoHasProducto(productoHasPedido.getProductohasPEDIDOPK());
           
           
           }
           
           //ProductoHasSucursal
           
           for(Entities.Sucursal sucursal:product.getSucursalCollection()){
           
              sucursal.getProductoCollection().remove(product);
              
              this.sucursalController.updateSucursalHasProduct(sucursal);
           
           }
           
           this.productoFacadeREST.remove(new PathSegmentImpl("bar;idproducto="+key.getIdproducto()+";categoriaIdcategoria="+key.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+key.getCategoriaFranquiciaIdfranquicia()+""));
        
        }
        catch(Exception ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
         if(ex instanceof javax.transaction.TransactionRolledbackException){
 
            this.deleteFromFranchise(key);
             
   }
        
        }
    
    }
    public void deleteFromBranchOffice(){
    
    
    
    }
    
    
   @Override
   
    public Entities.Producto find(Entities.ProductoPK idProduct){
    
    try{
    
        return productoFacadeREST.find(new PathSegmentImpl("bar;idproducto="+idProduct.getIdproducto()+";categoriaIdcategoria="+idProduct.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+idProduct.getCategoriaFranquiciaIdfranquicia()+""));
    
    }
    catch(EJBException ex){
    
        return null;
    
    }
    
    }
    
    @Override
    public List<Entities.Producto>findBySucursal(SucursalPK sucursalPK){
    
    try{
    
       return productoFacadeREST.findBySucursal(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
    
    }
    catch(EJBException ex){
    
        Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE,null,ex);
    
        throw new FacesException("Exception Caugth");
    }
    
    }
    
 @Override
    public java.util.List<Entities.Producto>findByFranchise(int idFranchise){
    
    try{
    
       java.util.List<Entities.Producto>products=new java.util.ArrayList<Entities.Producto>();
       
      products=this.productoFacadeREST.findByFranchise(idFranchise);
    
       return products;
    }
    catch(Exception ex){
    
        Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void createProduct(Entities.Producto product){
    
        try{
        
            //Verify Existence Barcode
            
            if(product.getBarcode().getBarcode()!=null && !product.getBarcode().getBarcode().equals("")){
            
                if(!product.getBarcode().getBarcode().equals("")){
                
                    if(barcodeFacadeREST.findByBarcode(product.getBarcode().getBarcode())==null){
                    
                    barcodeFacadeREST.create(product.getBarcode());
                    
                    }
                    
                    product.setBarcode(barcodeFacadeREST.findByBarcode(product.getBarcode().getBarcode()));
                
                }
            
            }else{
            
            product.setBarcode(null);
            
            }
            SessionClasses.EdenList<Entities.Compone>componeList=new SessionClasses.EdenList<Entities.Compone>();
            
            //Add Additional info for compone
            
            for(Entities.Compone compone:product.getComponeCollection()){
            
      
            compone.getComponePK().setProductoCategoriaFranquiciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            compone.getComponePK().setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
            
            compone.getComponePK().setProductoIdproducto(product.getProductoPK().getIdproducto());
            
            componeList.addItem(compone);
            
            }
            
            //Create Images
            
            for(Entities.Imagen image:this.fileUpload.getPhotoList()){
            
            this.imageController.create(image);
            
            }
            
            //Create Similar Products List
            
            SessionClasses.EdenList<Entities.SimilarProducts>similarProducts=new SessionClasses.EdenList<Entities.SimilarProducts>();
            
            if(product.getSimilarProductsCollection()!=null && !product.getSimilarProductsCollection().isEmpty()){
            
            for(Entities.SimilarProducts similar:product.getSimilarProductsCollection()){
            
            similarProducts.addItem(similar);
            
            }
            }
            
            //Create Product
            
            product.getProductoPK().setCategoriaFranquiciaIdfranquicia(product.getCategoria().getCategoriaPK().getFranquiciaIdfranquicia());

            product.getProductoPK().setCategoriaIdcategoria(product.getCategoria().getCategoriaPK().getIdcategoria());
            
        
            product.setComponeCollection(new java.util.ArrayList<Entities.Compone>());
            
            product.setSimilarProductsCollection(new java.util.ArrayList<Entities.SimilarProducts>());
            
            product.setImagenCollection(this.fileUpload.getPhotoList());
            
            this.productoFacadeREST.create(product);
            
            
            
            //Create Compone List
            
            for(Entities.Compone compone:componeList){        
                
            compone.getComponePK().setProductoIdproducto(product.getProductoPK().getIdproducto());
             
            compone.setProducto(product);
         
            compone.getComponePK().setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
            
            System.out.print("Compone ITEMPK Value: "+compone.getComponePK().toString());
            
            this.componeFacadeREST.create(compone);

            }
            
            //Create SimilarProducts
            
            for(Entities.SimilarProducts aux:similarProducts){
            
            Entities.SimilarProductsPK key=new Entities.SimilarProductsPK();
            
            key.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
            
            key.setProductoCategoriaFranquciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            key.setProductoIdproducto(product.getProductoPK().getIdproducto());
            
            key.setProductoIdproducto1(aux.getProducto1().getProductoPK().getIdproducto());
            
            key.setProductoCategoriaIdCategoria1(aux.getProducto1().getProductoPK().getCategoriaIdcategoria());
            
            key.setProductoCategoriaFranquiciaIdFranquicia1(aux.getProducto1().getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            aux.setSimilarProductsPK(key);
            
            aux.setProducto(product);
            
            this.similarProductsFacadeREST.create(aux);
            
            }
        
            
        }
        
        catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        
        }
    
    }
    
    @Override
    public Entities.Producto getProductByConverter(String key){
    
    try{
    
        if(key.split(",").length==3){
        
            Entities.ProductoPK productPK=new Entities.ProductoPK();
            
            productPK.setIdproducto(new java.lang.Integer(key.split(",")[0]));
            
            productPK.setCategoriaIdcategoria(new java.lang.Integer(key.split(",")[1]));
        
            productPK.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(key.split(",")[2]));
            
            return this.find(productPK);
            
        }
        else{
        
            throw new IllegalArgumentException("Wrong productPK type");
        
        }
    
    }
    catch(StackOverflowError | IllegalArgumentException  ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="27/08/2015",comments="Find Product by its name")
    public Entities.Producto findByName(Entities.CategoriaPK category,String name){
    
    try{
    
        
        return this.productoFacadeREST.findByName(name, new PathSegmentImpl("bar;idcategoria="+category.getIdcategoria()+";franquiciaIdfranquicia="+category.getFranquiciaIdfranquicia()+""));
    
    }
    catch(javax.ejb.EJBException | StackOverflowError | NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void editProduct(Entities.Producto product){
    
        try{
        
            this.productoFacadeREST.edit(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+""), product);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="31/08/2015",comments="Update Product")
    public void updateProduct(Entities.Producto product){
    
    try{
    
        //Delete Images
        
        for(Entities.Imagen image:product.getImagenCollection()){
        
        boolean found=false;
        
        for(Entities.Imagen aux:this.fileUpload.getPhotoList()){
        
            if(aux.equals(image)){
            
            found=true;
            
            }
        
        }
        
        if(!found){
        
        this.imageController.delete(image.getIdimagen());
        
        }
        
        }
        
        //Create New Images
        
        for(Entities.Imagen image:this.fileUpload.getPhotoList()){
        
            boolean found=false;
            
            for(Entities.Imagen aux:this.fileUpload.getPhotoList()){
            
            if(image.equals(aux)){
            
                found=true;
                
            }
            
            }
        
            if(!found){
            
                this.imageController.create(image);
            
            }
            
        }
        
        
        //Delete Compone
        
        for(Entities.Compone compone:componeFacadeREST.findIdProducto(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+""))){
        
        boolean found=false;
        
        for(Entities.Compone aux:product.getComponeCollection()){
        
        if(aux.equals(compone)){
        
            found=true;
        
        }
        
        }
        
        if(!found){
        
        this.componeFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+compone.getComponePK().getProductoIdproducto()+";productoCategoriaIdCategoria="+compone.getComponePK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+compone.getComponePK().getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+compone.getComponePK().getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+compone.getComponePK().getInventarioFranquiciaIdFranquicia()+""));
        
        }
        
        }
        
        
    //Create Compone
       /* 
        for(Entities.Compone compone:product.getComponeCollection()){
        
        
        if(componeFacadeREST.find(new PathSegmentImpl("bar;productoIdproducto="+compone.getComponePK().getProductoIdproducto()+";productoCategoriaIdCategoria="+compone.getComponePK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+compone.getComponePK().getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+compone.getComponePK().getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+compone.getComponePK().getInventarioFranquiciaIdFranquicia()+""))==null){
        
            System.out.print("Compone PK to add "+compone.getComponePK().toString());
            
            System.out.print("Compone Product "+compone.getProducto().toString());
            
            System.out.print("Compone Inventory "+compone.getInventario().toString());
            
            componeFacadeREST.create(compone);
        
        }
        
        }*/
        
        
        //Delete Similar Products
        
        for(Entities.SimilarProducts similar:similarProductsFacadeREST.findByFirstProduct(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+""))){
        
            boolean found=false;
            
            for(Entities.SimilarProducts aux:product.getSimilarProductsCollection()){
            
            
                if(aux.getProducto1().equals(similar.getProducto1())){
                
                found=true;
                
                }
            
            }
            
            if(!found){
            
                this.similarProductsFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+similar.getSimilarProductsPK().getProductoIdproducto()+";productoCategoriaIdCategoria="+similar.getSimilarProductsPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+similar.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+similar.getSimilarProductsPK().getProductoIdproducto1()+";productoCategoriaIdCategoria1="+similar.getSimilarProductsPK().getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+similar.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia1()+""));
            
            }
        
        }
        
        
        //Create new Similar Products
        /*
        for(Entities.SimilarProducts similar:product.getSimilarProductsCollection()){
        
            boolean found=false;
            
            for(Entities.SimilarProducts aux:this.similarProductsFacadeREST.findByFirstProduct(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+"")))
            {
            
                if(similar.getProducto1().equals(aux.getProducto1())){
                
                 found=true;
                
                }
            
            }
            
            
            if(!found){
            
                this.similarProductsFacadeREST.create(similar);
            
            }
        }*/
        
        
        //updateProduct
        
        product.setImagenCollection(this.fileUpload.getPhotoList());
        
        this.productoFacadeREST.edit(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+""), product);
        
    
    }
    catch(javax.ejb.EJBException | IllegalArgumentException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
   
    public SessionClasses.EdenList<Entities.Inventario>getInventoryList(Entities.Producto product){
    
    try{
    
    SessionClasses.EdenList<Entities.Inventario>inventoryList=new SessionClasses.EdenList<Entities.Inventario>();
    
    for(Entities.Compone compone:product.getComponeCollection()){
    
        if(inventoryList.constains(compone.getInventario()))
        {
        inventoryList.addItem(compone.getInventario());
        }
        
    }
    
    return inventoryList;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    @Override
    public boolean validInventory(Entities.Producto product){
    
        try{
        
            if(product.getComponeCollection()==null || product.getComponeCollection().isEmpty()){
            
                return false;
            
            }
            else{
            
            for(Entities.Compone compone:product.getComponeCollection()){
            
            if(compone.getInventario().getHabilitado()==1){
            
                return true;
            
            }
            
            }
            
            }
            
        return false;
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public java.util.List<Entities.Producto>findAll(){
    
        return this.productoFacadeREST.findAll();
    
    }
    
    private Entities.InventarioHasSucursal findInvetory(java.util.List<Entities.InventarioHasSucursal>collection,Entities.Inventario inventory){
    
        try{
        
            for(Entities.InventarioHasSucursal inv:collection){
            
            if(inv.getInventario().getInventarioPK().equals(inventory)){
            
                return inv;
            
            }
            
            }
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public boolean hasInventory(Entities.Producto product,Entities.Sucursal sucursal,int quantity){
    
    try{
    
        for(Entities.Inventario inventory: this.getInventoryList(product))
        {
        
            if(inventory.getHabilitado()==1){
            
                Entities.ComponePK key=new Entities.ComponePK();
                
                key.setProductoIdproducto(product.getProductoPK().getIdproducto());
                
                key.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
                
                key.setProductoCategoriaFranquiciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
                
                key.setInventarioIdinventario(inventory.getInventarioPK().getIdinventario());
                
                key.setInventarioFranquiciaIdFranquicia(inventory.getInventarioPK().getFranquiciaIdFranquicia());
                
                double inventoryQuantity=this._componeController.find(key).getCantidadInventario();
                
                inventoryQuantity=inventoryQuantity*quantity;
                
                Entities.InventarioHasSucursal inv=this.findInvetory((java.util.List<Entities.InventarioHasSucursal>)sucursal.getInventarioHasSucursalCollection(), inventory);
                
                if(inv.getQuantity()<inventoryQuantity){
                
                    return false;
                
                }
                
            }
        
        }
    return true;
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
    
    }
    
    
    }
    
    @Override
    public Entities.Producto findByName(int idFranchise,String name){
    
    try{
    
        if(this.findByFranchise(idFranchise)!=null && !this.findByFranchise(idFranchise).isEmpty()){
        
            java.util.Vector<Entities.Producto>products=new java.util.Vector<Entities.Producto>(this.findByFranchise(idFranchise).size());
        
            for(Entities.Producto product:products){
            
                if(product.getNombre().toLowerCase().equals(name.toLowerCase())){
                
                    return product;
                
                }
            
            }
            
        }
        
        return null;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Producto>getFranchiseProducts(){
    
        try{
        
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
           if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
           
               Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
               
               java.util.Vector<Entities.Producto>results=new java.util.Vector<Entities.Producto>(this.findByFranchise(franchise.getIdfranquicia()).size());
           
               for(Entities.Producto product:this.findByFranchise(franchise.getIdfranquicia())){
               
                   results.add(product);
               
               }
              
          
        }
             return null;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    
    @Override
    public java.util.Vector<Entities.Producto>getAutoFranchiseProducts(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                if(this.findByFranchise(franchise.getIdfranquicia())!=null  && !this.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
                
                    java.util.Vector<Entities.Producto>results=new java.util.Vector<Entities.Producto>(this.findByFranchise(franchise.getIdfranquicia()));
                
                    for(Entities.Producto product:this.findByFranchise(franchise.getIdfranquicia())){
                    
                        results.add(product);
                    
                    }
                    
                    return results;
                    
                }
            
            }
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }

    @Override
    public Producto findByBarcode(String barcode) {
      
        try{
        
            if(this.productoFacadeREST.findByBarcode(barcode)!=null && !this.productoFacadeREST.findByBarcode(barcode).isEmpty()){
            
            int min=0;
            
            int max=this.productoFacadeREST.findByBarcode(barcode).size()-1;
            
            Random rand=new Random();
            
            int index=rand.nextInt((max-min)+1)+min;
            
            return this.productoFacadeREST.findByBarcode(barcode).get(index);
            
            }
          
            return null;
        }
        catch(Exception | StackOverflowError ex){
        
        return null;
        }
        
    }
    
    
    
}
