/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface ProductoControllerInterface {
    int productQuantity(Entities.ProductoPK productId);
    
    List<SimilarProductsLayoutInterface>findSimilar(Entities.ProductoPK idProducto);
    
    Entities.Producto find(Entities.ProductoPK idProduct);
    
    java.util.List<Entities.Producto>findBySucursal(Entities.SucursalPK sucursalPK);
    
    java.util.List<String>getImages(Entities.ProductoPK idProducto);
    
    java.util.List<Entities.Producto>findByFranchise(int idFranchise);
    
    void deleteFromFranchise(Entities.ProductoPK key);
    
    void deleteSimilarProducts(Entities.SimilarProductsPK key);
    
    void deleteLastSeenProducto(Entities.LastSeenProductoPK key);
    
    void createProduct(Entities.Producto product);
    
    Entities.Producto getProductByConverter(String key);
    
    Entities.Producto findByName(Entities.CategoriaPK category,String name);
    
    void updateProduct(Entities.Producto product);
    
    boolean validInventory(Entities.Producto product);
    
    java.util.List<Entities.Producto>findAll();
    
    java.util.List<String>getImageCollection(Entities.ProductoPK key);
    
    boolean hasInventory(Entities.Producto product,Entities.Sucursal sucursal,int quantity);
    
    Entities.Producto findByName(int idFranchise,String name);

    Entities.Producto findByBarcode(String barcode);
    
    java.util.Vector<Entities.Producto>getAutoFranchiseProducts();

    java.util.Vector<Entities.Producto>getFranchiseProducts();
    
    void editProduct(Entities.Producto product);

}
