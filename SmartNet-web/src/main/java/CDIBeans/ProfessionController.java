/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.ProfessionFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@ProfessionControllerQualifier
public class ProfessionController implements ProfessionControllerInterface{
    
    @EJB private ProfessionFacadeREST professionFacadeREST;
    
    @Override
    public Entities.Profession find(int idProfession){
    
    try{
    
        return this.professionFacadeREST.find(idProfession);
    
    }
    catch(EJBException | NullPointerException ex){
    
        Logger.getLogger(ProfessionController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
    
    }
    
    
    
    }

	@Override
	public java.util.List<Entities.Profession> findAll() {
		
            try{
            
                return this.professionFacadeREST.findAll();
            
            }
            catch(NullPointerException | javax.ejb.EJBException ex){
            
                ex.printStackTrace(System.out);
                
                return null;
            
            }
            
	}
    
}
