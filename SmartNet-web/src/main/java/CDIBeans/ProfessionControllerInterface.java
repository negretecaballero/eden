/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import EdenFinalUser.UpdateAccount.Controller.*;

/**
 *
 * @author luisnegrete
 */
public interface ProfessionControllerInterface {
    
    Entities.Profession find(int idProfession);

	java.util.List<Entities.Profession> findAll();
    
}
