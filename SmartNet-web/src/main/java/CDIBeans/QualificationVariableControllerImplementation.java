/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@QualificationVariableControllerQualifier
public class QualificationVariableControllerImplementation implements QualificationVariableController {
    
    @javax.ejb.EJB
    private jaxrs.service.QualificationVariableFacadeREST _qualificationVariableFacadeREST;
    
    @Override
    public SessionClasses.EdenList<Entities.QualificationVariable>findAll(){
    
        try{
        
            SessionClasses.EdenList<Entities.QualificationVariable>results=new SessionClasses.EdenList<Entities.QualificationVariable>();
            
            for(Entities.QualificationVariable qualificationVariable:_qualificationVariableFacadeREST.findAll()){
            
                results.addItem(qualificationVariable);
            
            }
            
            return results;
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
