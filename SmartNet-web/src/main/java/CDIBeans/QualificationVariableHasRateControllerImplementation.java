/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@QualificationVariableHasRateControllerQualifier
public class QualificationVariableHasRateControllerImplementation implements QualificationVariableHasRateController{
    
    @javax.ejb.EJB
    private jaxrs.service.QualificationVariableHasRateFacadeREST _qualificationVariableHasRateFacadeREST;
    
    @Override
    public Entities.QualificationVariableHasRate find(Entities.QualificationVariableHasRatePK key){
    
        try{
        
            return _qualificationVariableHasRateFacadeREST.find(new PathSegmentImpl("bar;qualificationVariableIdqualificationVariable="+key.getQualificationVariableIdqualificationVariable()+";rateIdrate="+key.getRateIdrate()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+""));
        
        }
        
        catch(javax.ejb.EJBException ex){
        
            return null;
        
        }
    
    }
    
    
    @Override
    public java.util.List<Entities.QualificationVariableHasRate>findByFranchise(Integer idFranchise){
    
    try{
    
        System.out.print("ID FRANCHISE FIND IN BEAN "+idFranchise);
        
        return _qualificationVariableHasRateFacadeREST.findByFranchise(idFranchise);
    
    }
    
    catch(javax.ejb.EJBException ex){
    
        return null;
    
    }
    
    }
    
}
