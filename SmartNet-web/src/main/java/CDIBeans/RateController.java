/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.FacesException;
import jaxrs.service.RateFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@RateControllerQualifier

public class RateController implements RateControllerInterface{
    
    @EJB
    private RateFacadeREST rateFacadeREST;
    
    @Override
    public Entities.Rate find(int idRate){
    
        try{
        
            return rateFacadeREST.find(idRate);
            
        
        }catch(EJBException ex){
        
            Logger.getLogger(RateController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException(ex);
        
        }
    
    }
    
    @Override
    public Entities.Rate findByStars(short stars){
    
        try{
        
            return this.rateFacadeREST.findByStars(stars);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
}
