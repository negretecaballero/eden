/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import SessionClasses.CartItem;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import javax.inject.Inject;
import jaxrs.service.RateFacadeREST;
import jaxrs.service.RatehasPEDIDOFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@RatehasPEDIDOControllerQualifier
public class RatehasPEDIDOController implements RatehasPEDIDOControllerInterface{
    
     @EJB
    private RatehasPEDIDOFacadeREST ratehasPEDIDOFacadeREST;
     
     @EJB
     private RateFacadeREST rateFacadeREST;
     
  
    
    
     @Override
     public void create(short rateID,Entities.Pedido pedido){
     
         try{
         
            
             Entities.RatehasPEDIDOPK key=new Entities.RatehasPEDIDOPK();
             
             key.setRateIdrate(rateID);
             
           
             Entities.RatehasPEDIDO ratehasPEDIDO=new Entities.RatehasPEDIDO();
             
             ratehasPEDIDO.setRateHasPedidoPK(key);
             
                ratehasPEDIDO.setPedido(pedido);
             
                ratehasPEDIDO.setRateIdrate(rateFacadeREST.find(rateID));
                
                ratehasPEDIDO.setState(false);
                
                ratehasPEDIDOFacadeREST.create(ratehasPEDIDO);
         
         }
         catch(EJBException | NullPointerException ex){
         
             ex.printStackTrace(System.out);
         
         }
     
     }
     
     @Override
     public int shipmentsToRate(Entities.LoginAdministrador loginAdministrador){
     
         int itemsToRate=0;
     
         for(Entities.Pedido pedido:loginAdministrador.getPedidoCollection()){
         
             if(pedido.getPedidoStatestateId().getState().equals("Delivered")){
             
                 Entities.RatehasPEDIDO ratehaspedido=this.ratehasPEDIDOFacadeREST.findBySucursal(pedido.getIdPEDIDO());
                 
                System.out.print(ratehaspedido.getState());
                
                if(!ratehaspedido.getState()){
                
                    itemsToRate++;
                
                }
             
             }
         
         }
         
         return itemsToRate;
     }
     
     @Override
     public java.util.List<Clases.UserRateInterface>unratedPedidos(Entities.LoginAdministrador loginAdministrador){
     
     try{
         
         java.util.List<Clases.UserRateInterface>pedidoList=new java.util.ArrayList<Clases.UserRateInterface>();
     
         for(Entities.Pedido aux:loginAdministrador.getPedidoCollection()){
         
         if(aux.getPedidoStatestateId().getStateId()==3){
         
         if(!ratehasPEDIDOFacadeREST.findByIdPedido(aux.getIdPEDIDO()).getState()){
         
             
             
             Clases.UserRateInterface userRate=new Clases.UserRate();
             
             
             
             userRate.setRate(ratehasPEDIDOFacadeREST.findByIdPedido(aux.getIdPEDIDO()).getRateIdrate().getStars());
             
             Clases.PedidoWorkerInterface auxi=new Clases.PedidoWorker();
             
             auxi.setPedido(aux);
             
          for(Entities.ProductohasPEDIDO products:aux.getProductohasPEDIDOCollection()){
          
              CartItem item=new CartItem();
              
              item.setItem(products.getProducto());
              
              item.setName(products.getProducto().getNombre());
              
              item.setQuantity(products.getQuantity());
          
              auxi.getItemList().add(item);
          }
          
          for(Entities.MenuhasPEDIDO menus: aux.getMenuhasPEDIDOCollection()){
          
          CartItem item=new CartItem();
          
          
          item.setItem(menus.getMenu());
          
          item.setName(menus.getMenu().getNombre());
          
          item.setQuantity(menus.getQuantity());
          
          auxi.getItemList().add(item);
          }
          
          userRate.setPedido(auxi);
          
             
             pedidoList.add(userRate);
         
         }
         
         }
         
         }
        
         return pedidoList;
     
     }catch(EJBException ex){
     
         Logger.getLogger(RatehasPEDIDOController.class.getName()).log(Level.SEVERE,null,ex);
         
         throw new FacesException(ex);
     
     }
     
     }
     
     
     @Override
     public void updateRate(int idPedido, short stars){
     
     try{
     
         Entities.Rate rate=rateFacadeREST.findByStars(stars);
         
         Entities.RatehasPEDIDO ratehasPedido=ratehasPEDIDOFacadeREST.findByIdPedido(idPedido);
         
         if(!ratehasPedido.getState()){
         
         ratehasPedido.setState(true);
         
         }
         
        ratehasPedido.setRateIdrate(rate);
   
        //ratehasPEDIDOFacadeREST.edit(, ratehasPedido);
        
         
     }
     catch(EJBException ex){
     
         Logger.getLogger(RatehasPEDIDOController.class.getName()).log(Level.SEVERE,null,ex);
         
         throw new FacesException("Exception Caugth");
     
     }
     
     }
    
}
