/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface RatehasPEDIDOControllerInterface {
    
    
   java.util.List<Clases.UserRateInterface>unratedPedidos(Entities.LoginAdministrador loginAdministrador);
    
    int shipmentsToRate(Entities.LoginAdministrador loginAdministrador);
    
    void create(short rateID,Entities.Pedido pedido);
    
    void updateRate(int idPedido, short stars);
}
