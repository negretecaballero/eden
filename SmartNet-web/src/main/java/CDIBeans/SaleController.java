/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import jaxrs.service.SaleFacadeREST;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

@javax.enterprise.context.Dependent

@SaleControllerQualifier

public class SaleController implements SaleControllerInterface {
    
   @EJB private SaleFacadeREST saleFacadeREST;
   
   @javax.ejb.EJB
   private jaxrs.service.SaleHasProductoFacadeREST saleHasProductoFacadeREST;
    
   @Inject
   @CDIBeans.ImageControllerQualifier
   private ImageControllerDelegate imageController;
   
   @Inject
   @CDIBeans.FileUploadBeanQualifier
   private FileUploadInterface fileUpload;
   
   @javax.inject.Inject
   @CDIBeans.SaleHasProductoControllerQualifier
   private CDIBeans.SaleHasProductoControllerInterface _saleHasProductoController;
   
   @javax.inject.Inject
   @CDIBeans.SaleHasMenuControllerQualifier
   private CDIBeans.SaleHasMenuControllerInterface _saleHasMenuController;
   
   /**
	 * 
	 * @param sale
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
public void create(Entities.Sale sale){

    try{
    
        java.util.List<Entities.SaleHasProducto>saleHasProductoList=new java.util.ArrayList<Entities.SaleHasProducto>();
        
        java.util.List<Entities.SaleHasMenu>saleHasMenuList=new java.util.ArrayList<Entities.SaleHasMenu>();
        
        if(sale.getSaleHasMenuCollection()!=null && !sale.getSaleHasMenuCollection().isEmpty()){
        
            System.out.print("SALE HAS MENU BEFORE CREATE BEAN "+sale.getSaleHasMenuCollection().size());
            
        for(Entities.SaleHasMenu saleHasMenu:sale.getSaleHasMenuCollection()){
        
            saleHasMenuList.add(saleHasMenu);
        
        }
        
        }
        
        if(sale.getSaleHasProductoCollection()!=null && !sale.getSaleHasProductoCollection().isEmpty()){
        
            for(Entities.SaleHasProducto saleHasProducto:sale.getSaleHasProductoCollection()){
            
                saleHasProductoList.add(saleHasProducto);
            
            }
        
        }
        
       
        
        
        sale.setSaleHasProductoCollection(new java.util.ArrayList<Entities.SaleHasProducto>());
        
        sale.setSaleHasMenuCollection(new java.util.ArrayList<Entities.SaleHasMenu>());
        
        System.out.print("SALE iMAGES LIST SIZE "+sale.getImagenCollection().size());
        
        this.saleFacadeREST.create(sale);
        
           
        if(saleHasProductoList!=null && !saleHasProductoList.isEmpty()){
        
            for(Entities.SaleHasProducto saleHasProducto:saleHasProductoList){
            
                saleHasProducto.setSale(sale);
                
                saleHasProducto.getSaleHasProductoPK().setSaleIdsale(sale.getSalePK().getIdsale());
                
                _saleHasProductoController.create(saleHasProducto);
            
            }
        
        }
        
        if(saleHasMenuList!=null && !saleHasMenuList.isEmpty()){
        
            System.out.print("SALE HAS MENU SIZE "+saleHasMenuList.size());
            
            for(Entities.SaleHasMenu saleHasMenu:saleHasMenuList){
            
                saleHasMenu.setSale(sale);
                
                saleHasMenu.getSaleHasMenuPK().setSaleIdsale(sale.getSalePK().getIdsale());
                
                _saleHasMenuController.create(saleHasMenu);
            
            }
        
        }
    }
    catch(Exception ex){
    
        if(ex instanceof javax.transaction.RollbackException)
        {
            
            this.create(sale);
        
        }
    }

}
  
@Override
public java.util.List<Entities.Sale>findByFranchise(int idFranchise){

try{

    return this.saleFacadeREST.findByFranchise(idFranchise);

}
catch(javax.ejb.EJBException ex){

    throw new javax.faces.FacesException(ex);

}

}
  
/**
	 * 
	 * @param salePK
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
public void delete(Entities.SalePK salePK){

try{

    this.saleFacadeREST.remove(new PathSegmentImpl("bar;idsale="+salePK.getIdsale()+";franquiciaIdfranquicia="+salePK.getFranquiciaIdfranquicia()+""));

}
catch(Exception ex){

   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

}

}

/**
	 * 
	 * @param sale
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
public void update(Entities.Sale sale){

    try{
        
        
        java.util.List<Entities.SaleHasProducto>saleHasProductoList=new java.util.ArrayList<Entities.SaleHasProducto>();
 
        java.util.List<Entities.SaleHasMenu>saleHasMenuList=new java.util.ArrayList<Entities.SaleHasMenu>();
        
        if(sale.getSaleHasProductoCollection()!=null && !sale.getSaleHasProductoCollection().isEmpty()){
        
            for(Entities.SaleHasProducto saleHasProducto:sale.getSaleHasProductoCollection()){
            
                saleHasProductoList.add(saleHasProducto);
            
            }
        
        }
        
        if(sale.getSaleHasMenuCollection()!=null && !sale.getSaleHasMenuCollection().isEmpty()){
        
        for(Entities.SaleHasMenu saleHasMenu:sale.getSaleHasMenuCollection()){
        
            saleHasMenuList.add(saleHasMenu);
        
        }
        
        }
        
        
        if(saleHasProductoList!=null && !saleHasProductoList.isEmpty()){
        //Create SaleHasProducto
        for(Entities.SaleHasProducto aux:saleHasProductoList){
        
        boolean found=false;
        
        for(Entities.SaleHasProducto auxi:this.find(sale.getSalePK()).getSaleHasProductoCollection())
        {
        
            if(aux.getSaleHasProductoPK().equals(auxi.getSaleHasProductoPK())){
            
                found=true;
            
            }
        
        }
        
        if(!found){
        
        System.out.print("SaleHasProducto to create PK "+aux.getSaleHasProductoPK());    
            
        this._saleHasProductoController.create(aux);
        
        }
        else{
        
            this._saleHasProductoController.update(aux);
        
        }
        
        }
        }
    
        //Delete SaleHasProducto
        if(this.find(sale.getSalePK()).getSaleHasProductoCollection()!=null && !this.find(sale.getSalePK()).getSaleHasProductoCollection().isEmpty())
        {
        for(Entities.SaleHasProducto aux:this.find(sale.getSalePK()).getSaleHasProductoCollection()){
        
        boolean found=false;
        
        for(Entities.SaleHasProducto auxi:saleHasProductoList){
        
            if(aux.getSaleHasProductoPK().equals(auxi.getSaleHasProductoPK())){
            
                found=true;
            
            }
        
        }
        
        if(!found){
        
        this._saleHasProductoController.delete(aux.getSaleHasProductoPK());
        
        }
        
        }
    }
        
        //Create SaleHasPMenu
        
        if(saleHasMenuList!=null && !saleHasMenuList.isEmpty()){
        
            for(Entities.SaleHasMenu saleHasMenu:saleHasMenuList){
            
                boolean found=false;
                
                for(Entities.SaleHasMenu aux:sale.getSaleHasMenuCollection()){
                
                    if(saleHasMenu.getSaleHasMenuPK().equals(aux.getSaleHasMenuPK())){
                    
                        found =true;
                    
                    }
                
                }
                
                if(!found){
                
                    _saleHasMenuController.create(saleHasMenu);
                    
                }
                else{
                
                    _saleHasMenuController.update(saleHasMenu);
                
                }
                
            }
        
        }
        
        
        //Delete SaleHasMenu
        
        if(this.find(sale.getSalePK()).getSaleHasMenuCollection()!=null && !this.find(sale.getSalePK()).getSaleHasMenuCollection().isEmpty()){
        
            for(Entities.SaleHasMenu saleHasMenu:this.find(sale.getSalePK()).getSaleHasMenuCollection()){
            
                boolean found=false;
            
                for(Entities.SaleHasMenu aux:saleHasMenuList){
                
                    if(saleHasMenu.getSaleHasMenuPK().equals(aux.getSaleHasMenuPK())){
                    
                        found=true;
                    
                    }
                
                }
               
                if(!found){
                
                    _saleHasMenuController.delete(saleHasMenu.getSaleHasMenuPK());
                
                }
                
            }
        
        }
      
        
        sale.setSaleHasProductoCollection(new java.util.ArrayList<Entities.SaleHasProducto>());
        
        sale.setSaleHasMenuCollection(new java.util.ArrayList<Entities.SaleHasMenu>());
        
        this.saleFacadeREST.edit(new PathSegmentImpl("bar;idsale="+sale.getSalePK().getIdsale()+";franquiciaIdfranquicia="+sale.getSalePK().getIdsale()+""),sale);
        
        
    
    }
    catch(Exception ex){
    
     if(ex instanceof javax.transaction.RollbackException){
     
         
         this.update(sale);
     
     }
    
    }

}

@Override
public Entities.Sale find(Entities.SalePK key){

try{

    return this.saleFacadeREST.find(new PathSegmentImpl("bar;idsale="+key.getIdsale()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+""));

}
catch(NullPointerException | IllegalArgumentException ex){

    Logger.getLogger(SaleController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);

}

}

@Override
public void deleteSaleHasProducto(Entities.SaleHasProductoPK key){

try{

    this.saleHasProductoFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";saleIdsale="+key.getSaleIdsale()+""));

}
catch(javax.ejb.EJBException | NullPointerException ex){

    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);

}


}

@Override
public void loadImages(Entities.Sale sale, String path){

    try{
    
        if(sale.getImagenCollection()!=null && !sale.getImagenCollection().isEmpty()){
        
            for(Entities.Imagen image:sale.getImagenCollection()){
            
                fileUpload.loadImagesServletContext(image, path);
            
            }
        
        }
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }

}




}
