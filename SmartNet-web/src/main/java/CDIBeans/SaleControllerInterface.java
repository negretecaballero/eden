/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SaleControllerInterface {
    
    void create(Entities.Sale sale);
    
    java.util.List<Entities.Sale>findByFranchise(int idFranchise);
    
    void delete(Entities.SalePK salePK);
    
    void update(Entities.Sale sale);
    
    Entities.Sale find(Entities.SalePK key);
    
    void deleteSaleHasProducto(Entities.SaleHasProductoPK key);
    
    void loadImages(Entities.Sale sale, String path);
}
 