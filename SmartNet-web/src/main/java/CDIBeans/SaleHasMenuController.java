/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import jaxrs.service.SaleHasMenuFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

@javax.enterprise.context.Dependent
@SaleHasMenuControllerQualifier
public class SaleHasMenuController implements SaleHasMenuControllerInterface{
   
    @EJB 
    private SaleHasMenuFacadeREST saleHasMenuFacadeREST;
    
    @Inject 
    @CDIBeans.MenuControllerQualifier
    private MenuControllerInterface menuController;
    
    /**
	 * 
	 * @param saleHasMenu
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public void create(Entities.SaleHasMenu saleHasMenu){
    
        try{
            
            System.out.print("SALE HAS MENU TO CREATE "+saleHasMenu.getSaleHasMenuPK());
            
            this.saleHasMenuFacadeREST.create(saleHasMenu);
        
        }catch(Exception ex){
        
            if(ex instanceof javax.transaction.RollbackException){
            
                this.create(saleHasMenu);
            
            }
        
        }
      
    
    }
    
    
    @Override
    public Entities.SaleHasMenu find(Entities.SaleHasMenuPK key){
    
    try{
    
        return this.saleHasMenuFacadeREST.find(new PathSegmentImpl("bar;menuIdmenu="+key.getMenuIdmenu()+";saleIdsale="+key.getSaleIdsale()+";saleFranquiciaIdfranquicia="+key.getSaleFranquiciaIdfranquicia()+""));
    
    }
    catch(IllegalArgumentException | NullPointerException ex){
    
        Logger.getLogger(SaleHasMenuController.class.getName()).log(Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
        
    }
    
    }
    
    
    /**
	 * 
	 * @param saleHasMenu
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public void update(Entities.SaleHasMenu saleHasMenu){
    
        try{
        
            this.saleHasMenuFacadeREST.edit(new PathSegmentImpl("bar;menuIdmenu="+saleHasMenu.getSaleHasMenuPK().getMenuIdmenu()+";saleIdsale="+saleHasMenu.getSaleHasMenuPK().getSaleIdsale()+";saleFranquiciaIdfranquicia="+saleHasMenu.getSaleHasMenuPK().getSaleFranquiciaIdfranquicia()+""),saleHasMenu);
            
        }
        catch(Exception ex){
        
            if(ex instanceof javax.transaction.RollbackException){
            
                this.update(saleHasMenu);
            
            }
        
        }
    
    }
    
    @Override
    public void delete(Entities.SaleHasMenuPK key){
    
        try{
        
            this.saleHasMenuFacadeREST.remove(new PathSegmentImpl("bar;menuIdmenu="+key.getMenuIdmenu()+";saleIdsale="+key.getSaleIdsale()+";saleFranquiciaIdfranquicia="+key.getSaleFranquiciaIdfranquicia()+""));
        
        }
        catch(javax.ejb.EJBException | NullPointerException | IllegalArgumentException ex){
        
            Logger.getLogger(SaleHasMenuController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.SaleHasMenu>findBySale(Entities.SalePK salePK){
    
        try{
        
           return new SessionClasses.EdenList<Entities.SaleHasMenu>(this.saleHasMenuFacadeREST.findBySale(new PathSegmentImpl("bar;idsale="+salePK.getIdsale()+";franquiciaIdfranquicia="+salePK.getFranquiciaIdfranquicia()+"")));
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
