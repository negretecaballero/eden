/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SaleHasMenuControllerInterface {
    
    void create(Entities.SaleHasMenu saleHasMenu);
    
    Entities.SaleHasMenu find(Entities.SaleHasMenuPK key);
    
    void update(Entities.SaleHasMenu saleHasMenu);
    
    void delete(Entities.SaleHasMenuPK key);
    
    SessionClasses.EdenList<Entities.SaleHasMenu>findBySale(Entities.SalePK salePK);
    
}
