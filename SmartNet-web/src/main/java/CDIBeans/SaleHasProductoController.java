/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.transaction.Transactional.TxType;
import jaxrs.service.SaleHasProductoFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@SaleHasProductoControllerQualifier

public class SaleHasProductoController implements SaleHasProductoControllerInterface{
  
    @EJB private SaleHasProductoFacadeREST saleHasProductoFacadeREST;

	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public void create(Entities.SaleHasProducto saleHasProducto){
    
 try{
        
    this.saleHasProductoFacadeREST.create(saleHasProducto);
    
 }
 catch(Exception ex){
 
     if(ex instanceof javax.transaction.RollbackException){
     
         this.create(saleHasProducto);
     
     }
 
 }
    
    }
    
    @Override
    public Entities.SaleHasProducto find(Entities.SaleHasProductoPK key){
    
    try{
    
        return this.saleHasProductoFacadeREST.find(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";saleIdsale="+key.getSaleIdsale()+";saleFranquiciaIdfranquicia="+key.getSaleFranquiciaIdfranquicia()+""));
    
    }
    catch(IllegalArgumentException | NullPointerException ex){
    
        Logger.getLogger(SaleHasProductoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    /**
	 * 
	 * @param saleHasProducto
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public void update(Entities.SaleHasProducto saleHasProducto){
    
        try{
        
             this.saleHasProductoFacadeREST.edit(new PathSegmentImpl("bar;productoIdproducto="+saleHasProducto.getSaleHasProductoPK().getProductoIdproducto()+";productoCategoriaIdCategoria="+saleHasProducto.getSaleHasProductoPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+saleHasProducto.getSaleHasProductoPK().getProductoCategoriaFranquiciaIdFranquicia()+";saleIdsale="+saleHasProducto.getSaleHasProductoPK().getSaleIdsale()+""),saleHasProducto);
        
        }
        catch(Exception ex){
        
          if(ex instanceof javax.transaction.RollbackException){
          
              this.update(saleHasProducto);
          
          }
        
        }
    
    
    }
    
    @Override
    public void delete(Entities.SaleHasProductoPK key){
    
    try{
    
        this.saleHasProductoFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";saleIdsale="+key.getSaleIdsale()+";saleFranquiciaIdfranquicia="+key.getSaleFranquiciaIdfranquicia()+""));
    
    }
    catch(IllegalArgumentException | NullPointerException ex){
    
        Logger.getLogger(SaleHasProductoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
    }
    
    
    }
    
    /**
	 * 
	 * @param idFranchise
	 */
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	@Override
    public SessionClasses.EdenList<Entities.SaleHasProducto>findByFranchise(int idFranchise){
    
        try{
        
            return new SessionClasses.EdenList<Entities.SaleHasProducto>(this.saleHasProductoFacadeREST.findByFranchise(idFranchise));
            
        }
        catch(Exception ex){
        
           if(ex instanceof javax.transaction.RollbackException){
           
             return  this.findByFranchise(idFranchise);
           
           } 
        return null;
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.SaleHasProducto>findBySale(Entities.SalePK key){
    
        try{
        
            return new SessionClasses.EdenList<Entities.SaleHasProducto>(this.saleHasProductoFacadeREST.findBySale(new PathSegmentImpl("bar;idsale="+key.getIdsale()+";franquiciaIdfranquicia="+key.getFranquiciaIdfranquicia()+"")));
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
