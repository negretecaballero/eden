/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SaleHasProductoControllerInterface {
    
    void create(Entities.SaleHasProducto saleHasProducto);
    
    Entities.SaleHasProducto find(Entities.SaleHasProductoPK key);
    
    void update(Entities.SaleHasProducto saleHasProducto);
    
    void delete(Entities.SaleHasProductoPK key);
    
    SessionClasses.EdenList<Entities.SaleHasProducto>findByFranchise(int idFranchise);
    
    SessionClasses.EdenList<Entities.SaleHasProducto>findBySale(Entities.SalePK key);
    
}
