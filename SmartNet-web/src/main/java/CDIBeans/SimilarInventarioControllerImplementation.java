/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SimilarInventarioControllerQualifier

public class SimilarInventarioControllerImplementation implements SimilarInventarioController{
    
    @javax.ejb.EJB
    private jaxrs.service.SimilarInventarioFacadeREST _similarInventarioFacadeREST;
    
    @Override
    public void create(Entities.SimilarInventario similar){
    
        try{
        
            _similarInventarioFacadeREST.create(similar);
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void update(Entities.SimilarInventario similarInventory){
    
        try{
        
            _similarInventarioFacadeREST.edit(new PathSegmentImpl("bar;inventarioIdinventario="+similarInventory.getSimilarInventarioPK().getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+similarInventory.getSimilarInventarioPK().getInventarioFranquiciaIdFranquicia()+";inventarioIdinventario1="+similarInventory.getSimilarInventarioPK().getInventarioIdinventario1()+";inventarioFranquiciaIdFranquicia1="+similarInventory.getSimilarInventarioPK().getInventarioFranquicioaIdFranquicia1()+""), similarInventory);
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void remove(Entities.SimilarInventarioPK key){
    
        try{
        
            System.out.print("KEY TO REMOVE CONTROLLER "+key);
            
            _similarInventarioFacadeREST.remove(new PathSegmentImpl("bar;inventarioIdinventario="+key.getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+key.getInventarioFranquiciaIdFranquicia()+";inventarioIdinventario1="+key.getInventarioIdinventario1()+";inventarioFranquiciaIdFranquicia1="+key.getInventarioFranquicioaIdFranquicia1()+""));
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
}
