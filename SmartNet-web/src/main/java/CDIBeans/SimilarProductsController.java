/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SimilarProductsController {
    
    void delete(Entities.SimilarProductsPK key);
    
    void edit(Entities.SimilarProducts aux);
    
    void create(Entities.SimilarProducts similarProduct);
    
}
