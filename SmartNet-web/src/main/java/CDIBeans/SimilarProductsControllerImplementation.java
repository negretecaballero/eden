/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.ws.rs.core.PathSegment;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@SimilarProductsControllerQualifier
public class SimilarProductsControllerImplementation implements SimilarProductsController {
    
    @javax.ejb.EJB
    private jaxrs.service.SimilarProductsFacadeREST similarProductsFacadeREST;
    
    
    @Override
    public void delete(Entities.SimilarProductsPK key){
    
        try{
        
            this.similarProductsFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+key.getProductoIdproducto()+";productoCategoriaIdCategoria="+key.getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+key.getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+key.getProductoIdproducto1()+";productoCategoriaIdCategoria1="+key.getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+key.getProductoCategoriaFranquiciaIdFranquicia1()+""));
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    @Override
    public void edit(Entities.SimilarProducts aux){
    try{
        
        PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+aux.getSimilarProductsPK().getProductoIdproducto()+";productodCategoriaIdCategoria="+aux.getSimilarProductsPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+aux.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+aux.getSimilarProductsPK().getProductoIdproducto1()+";productoCategoriaIdCategoria1="+aux.getSimilarProductsPK().getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+aux.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia1()+"");
           
        similarProductsFacadeREST.edit(ps,aux);
        
        this.similarProductsFacadeREST.edit(null, aux);
        
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    }
    
    @Override
   public void create(Entities.SimilarProducts similarProduct){
   
       try{
       
           this.similarProductsFacadeREST.create(similarProduct);
       
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       }
   
   }
    
}
