/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.faces.FacesException;
import jaxrs.service.StateFacadeREST;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@StateControllerQualifier

public class StateController implements StateControllerInterface{
   
    @EJB
    private StateFacadeREST stateFacadeREST;
    
    
  @Override  
  public Entities.State findByName(String name){
  
      try{
      
          return stateFacadeREST.findByName(name);
          
      }
      catch(EJBException ex){
      
         Logger.getLogger(StateController.class.getName()).log(Level.SEVERE,null,ex);
         
         throw new FacesException("Exception Caugth");
      
      }
      
  }

  @Override
 public java.util.List<Entities.State>findAll(){
 
     try{
     
   
      java.util.List<Entities.State>results=this.stateFacadeREST.findAll();
      
     
      return results; 

     }
     catch(Exception ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
         throw new javax.faces.FacesException(ex);
     }
 
 }
 
 @Override
 public Entities.State find(int stateId){
 try{
         
     return this.stateFacadeREST.find(stateId);
 }
 catch(javax.ejb.EJBException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
 
     throw new javax.faces.FacesException(ex);
     
 }
 }
    
}
