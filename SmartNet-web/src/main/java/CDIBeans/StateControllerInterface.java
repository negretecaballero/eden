/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface StateControllerInterface {

    Entities.State findByName(String name);
   
    java.util.List<Entities.State>findAll();
    
    Entities.State find(int stateId);
}
