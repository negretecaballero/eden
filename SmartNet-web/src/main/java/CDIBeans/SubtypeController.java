/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SubtypeController {
 
         Entities.Subtype find(Entities.SubtypePK key);

	/**
	 * 
	 * @param typeName
	 * @param subtypeName
	 */
	Entities.Subtype findByTypeSubtypeName(String typeName, String subtypeName);

	java.util.List<Entities.Subtype> findAll();
         
}
