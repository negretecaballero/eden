/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.SessionScoped

@SubtypeControllerQualifier

public class SubtypeControllerImplementation implements SubtypeController,java.io.Serializable{
    
   @javax.ejb.EJB
   private jaxrs.service.SubtypeFacadeREST subtypeFacadeREST;
   
   @Override
   public Entities.Subtype find(Entities.SubtypePK key){
   
       try{
       
           return this.subtypeFacadeREST.find(new PathSegmentImpl("bar;idSubtype="+key.getIdSubtype()+";tipoIdtipo="+key.getTipoIdtipo()+""));
       
       }
       catch(javax.ejb.EJBException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
       
   }

	/**
	 * 
	 * @param typeName
	 * @param subtypeName
     * @return 
	 */
	@Override
	public Entities.Subtype findByTypeSubtypeName(String typeName, String subtypeName) {
		
            try{
            
                return this.subtypeFacadeREST.findSubtypeByNames(subtypeName, typeName);
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                return null;
            
            }
            
	}

	@Override
	public java.util.List<Entities.Subtype> findAll() {
		
            try{
            
                return this.subtypeFacadeREST.findAll();
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return null;
            
            }
            
	}
   
    
}
