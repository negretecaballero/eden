/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Clases.BaseBacking;
import Clases.EdenDateInterface;
import Entities.SucursalPK;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import jaxrs.service.SucursalFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@SucursalControllerQualifier

public class SucursalController extends BaseBacking implements SucursalControllerDelegate {
    
    @EJB 
    private SucursalFacadeREST sucursalFacadeREST;
    
    @Inject
    @CDIBeans.FileUploadBeanQualifier
    private transient CDIBeans.FileUploadInterface fileUpload;
    
    @Inject
    @CDIBeans.ImageControllerQualifier
    private CDIBeans.ImageController imageController;
    
    @Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
    
    @Inject
    @CDIBeans.ProfessionControllerQualifier
    private CDIBeans.ProfessionControllerInterface professionController;
    
//    @javax.inject.Inject @javax.enterprise.inject.Default private  CDIBeans.SucursalhasloginAdministradorControllerDelegate sucursasHasLoginAdministradorController;
    
    @javax.inject.Inject
    @CDIBeans.AppGroupControllerQualifier
    private CDIBeans.AppGroupControllerDelegate appGroupController;
    
    @javax.inject.Inject
    @CDIBeans.GPSCoordinatesControllerQualifier
    private CDIBeans.GpsCoordinatesControllerInterface gpsCoordinatesController;
    
    @javax.inject.Inject 
    @CDIBeans.AddressControllerQualifier
    private CDIBeans.AdressControllerInterface addressController;
    
    @javax.inject.Inject
    @CDIBeans.SucursalHasDayControllerQualifier
    private CDIBeans.SucursalHasDayControllerImplementation _sucursalHasDayController;
    
     
    @Override
    public Entities.Sucursal find(SucursalPK sucursalPK){
    
        try{
        
            return sucursalFacadeREST.find(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
            
            
        }
        catch(EJBException ex){
        
            return null;
        
        }
    
    }
    
    @Override
    public boolean isOpen(Entities.SucursalPK sucursalPK){
    
        try{
            
           
            
        boolean open=false;
        Entities.Sucursal sucursal=sucursalFacadeREST.find(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
        
        java.util.Date date=new java.util.Date();
        
      
        
       boolean found=false;
       
       java.util.Calendar calendar=new GregorianCalendar(TimeZone.getTimeZone("America/Bogota"));
       
       int day=calendar.get(Calendar.DAY_OF_WEEK);
       
         System.out.print("Day Date "+day);
       
       for(Entities.SucursalHasDay aux:sucursal.getSucursalHasDayCollection()){
       
           if(aux.getDayId().getIdDay()==day){
           
                 double currentTime=Clases.EdenDate.convertToDecimalTime(date.getHours(), date.getMinutes());
               
                 if(currentTime>=Clases.EdenDate.convertToDecimalTime(aux.getOpenTime().getHours(), aux.getOpenTime().getMinutes()) && currentTime<=(Clases.EdenDate.convertToDecimalTime(aux.getCloseTime().getHours(),aux.getCloseTime().getMinutes()))){
                 
                     return true;
                 
                 }
           }
       
       }
      
       
        return false;
        }
        catch(EJBException ex){
        
            Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new FacesException(ex);
        
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.LoginAdministrador>getWorkers(Entities.Sucursal sucursal){
    
    try{
    
        SessionClasses.EdenList<Entities.LoginAdministrador>results=new SessionClasses.EdenList<Entities.LoginAdministrador>();
        
       /* for(Entities.SucursalhasloginAdministrador aux:sucursal.getSucursalhasloginAdministradorCollection()){
        
            if(aux.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_worker")){
            
                results.addItem(aux.getLoginAdministrador());
            
            }
        
        }*/
       return results; 
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
        return null;
        
    }
    
    }
    
 private boolean checkHours(EdenDateInterface current,EdenDateInterface open,EdenDateInterface close){
    
 System.out.print("Current Minutes "+current.getMinutes());
    

System.out.print("Opening minutes "+open.getMinutes());

System.out.print("Closing Minutes "+close.getMinutes());

double openingDivision=open.getMinutes()/60.0;

System.out.print(openingDivision+open.getHour());

 double openCoefficient=openingDivision+open.getHour();
 
 double closingDivision=close.getMinutes()/60.0;
 
 double closeCoefficient=close.getHour()+closingDivision;
 
 
 double currentDivision=current.getMinutes()/60.0;
 
 double currentCoeffient=current.getHour()+currentDivision;
 
 System.out.print("open coefficient "+openCoefficient);
 
 System.out.print("Closed Coefficient "+closeCoefficient);
 
 System.out.print("Current Coefficient "+currentCoeffient);
 
 if(currentCoeffient>=openCoefficient && currentCoeffient<=closeCoefficient)
 {
 return false;
 }
 
 return true;   
    
    }
    
   
    @Override
     public void loadImages(Entities.Sucursal sucursal,String username){
  
  try{
  
   javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
      
    this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
      
   // System.out.print("This sucursal has "+sucursal.getAdditionCollection().size()+" images");
    
    for(Entities.Imagen image:sucursal.getImagenCollection()){
    
        
    
    this.fileUpload.loadImagesServletContext(image, username);
    
    
    }
     
  
  }
  catch(Exception ex){
  
      Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
  
  }
  
  }
    
     
     @Override
     public String getSucursalAdministrator(Entities.SucursalPK sucursalPK)
     {
     
         try{
         
             String username="";
             
             Entities.Sucursal sucursal=this.find(sucursalPK);
             
           /*  for(Entities.SucursalhasloginAdministrador aux:sucursal.getSucursalhasloginAdministradorCollection())
             {
                 
                 System.out.print("App Group "+aux.getAppGroup().getAppGroupPK().getGroupid());
             
             if(aux.getAppGroup().getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador") && !aux.getLoginAdministrador().getUsername().equals(getSession().getAttribute("username").toString())){
             
                 username=aux.getLoginAdministrador().getUsername();
                 
                 break;
             
             }
             
             }*/
             return username;
         
         }
         catch(EJBException ex){
         
             Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
             
             
             
             throw new javax.ejb.EJBException("Exception Caugth");
         
         }
     
     }   
     
     @Override
     public void delete(Entities.SucursalPK sucursalPK){
     
     try{
     
     String username=this.getSucursalAdministrator(sucursalPK);
     
     Entities.LoginAdministrador user=this.loginAdministradorController.find(username);
     
     System.out.print("Delete username "+username);
     
   
     
     if(this.find(sucursalPK).getDireccion()!=null){
     
     Entities.Direccion address=this.find(sucursalPK).getDireccion();
     
    
     
     this.addressController.delete(address);
     
     System.out.print("Address Deleted");
     
     }
     
     this.sucursalFacadeREST.remove(new PathSegmentImpl("bar;idsucursal="+sucursalPK.getIdsucursal()+";franquiciaIdfranquicia="+sucursalPK.getFranquiciaIdfranquicia()+""));
  
     if(this.sucursalAdministradorNumber(user)<2){
     
     this.loginAdministradorController.deleteByAppGroupId(username, "guanabara_sucursal_administrador");
     
     }
     }
     catch(javax.ejb.EJBException ex){
     
         Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
       
     }
     
     }
     
     @Override
     public void editSucursal(Entities.Sucursal sucursal,String sucursalAdministrator){
     
         try{
         
             System.out.print("Username edit Sucursal "+sucursalAdministrator);
             
             for(Entities.Imagen image:sucursal.getImagenCollection()){
             
             boolean found=false;
             
             for(Entities.Imagen innerImage:this.fileUpload.getPhotoList()){
             
             
             if(image.getIdimagen()==innerImage.getIdimagen()){
             
                 found=true;
             
                 break;
             }
             
             }
             if(!found){
             this.imageController.delete(image.getIdimagen());
             }
             
             
             }
             
             for(Entities.Imagen image:this.fileUpload.getPhotoList()){
             
               if(image.getIdimagen()==null || image.getIdimagen()==0)
             {
        
            this.imageController.create(image);
            
             }
             
             }
             
             sucursal.setImagenCollection(this.fileUpload.getPhotoList());
            
             
             
             if(!this.getSucursalAdministrator(sucursal.getSucursalPK()).equals(sucursalAdministrator)){
             
                 
               if(this.sucursalAdministradorNumber(this.loginAdministradorController.find(this.getSucursalAdministrator(sucursal.getSucursalPK())))<2)
               {
                this.loginAdministradorController.deleteByAppGroupId(this.getSucursalAdministrator(sucursal.getSucursalPK()), "guanabara_sucursal_administrador");
                
               }
               
               else{
               
//               Entities.SucursalhasloginAdministradorPK key=new Entities.SucursalhasloginAdministradorPK();
               
//               key.setAPPGROUPgroupid("guanabara_sucursal_administrador");
               
  //             key.setAPPGROUPloginAdministradorusername(this.getSucursalAdministrator(sucursal.getSucursalPK()));
               
              // key.setLoginAdministradorusername(this.getSucursalAdministrator(sucursal.getSucursalPK()));
                  
               //key.setSucursalFranquiciaIdfranquicia(sucursal.getSucursalPK().getFranquiciaIdfranquicia());
               
               //key.setSucursalIdsucursal(sucursal.getSucursalPK().getIdsucursal());
               
               
              // this.sucursasHasLoginAdministradorController.delete(key);
               
               }
                
             
               //Create a New Account
                if(this.loginAdministradorController.find(sucursalAdministrator)==null){
                
                Entities.LoginAdministrador user=new Entities.LoginAdministrador();
                
                user.setUsername(sucursalAdministrator);
                
                user.setPassword(Clases.DataGeneration.passwordGeneration(30));
                
                user.setProfessionIdprofession(this.professionController.find(1));
                
                Entities.AppGroup appGroup=new Entities.AppGroup();
                
                Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
                
                appGroupPK.setGroupid("guanabara_sucursal_administrador");
                
                appGroupPK.setLoginAdministradorusername(sucursalAdministrator);
                
                appGroup.setAppGroupPK(appGroupPK);
                
                appGroup.setLoginAdministrador(user);
                
                this.loginAdministradorController.createAccount(user, appGroup);
                
                }
                
                //The account exists. create a new AppGroup
                else{
                
                Entities.AppGroup appGroup=new Entities.AppGroup();
                
                Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
                
                appGroupPK.setGroupid("guanabara_sucursal_administrador");
                
                appGroupPK.setLoginAdministradorusername(sucursalAdministrator);
                
                appGroup.setAppGroupPK(appGroupPK);
                
                appGroup.setLoginAdministrador(this.loginAdministradorController.find(sucursalAdministrator));
                
                if(this.appGroupController.find(appGroupPK)==null){
                
                this.appGroupController.create(appGroup);
                
                }
                }
                
                
//                Entities.SucursalhasloginAdministrador sucursalHasLoginAdministrador=new Entities.SucursalhasloginAdministrador();
                
  //              Entities.SucursalhasloginAdministradorPK sucursalHasLoginAdministradorPK=new Entities.SucursalhasloginAdministradorPK();
                
//                sucursalHasLoginAdministradorPK.setAPPGROUPgroupid("guanabara_sucursal_administrador");
                
  //              sucursalHasLoginAdministradorPK.setAPPGROUPloginAdministradorusername(sucursalAdministrator);
                
              /*  sucursalHasLoginAdministradorPK.setLoginAdministradorusername(sucursalAdministrator);
                
                sucursalHasLoginAdministradorPK.setSucursalFranquiciaIdfranquicia(sucursal.getSucursalPK().getFranquiciaIdfranquicia());
                
                sucursalHasLoginAdministradorPK.setSucursalIdsucursal(sucursal.getSucursalPK().getIdsucursal());
                
                sucursalHasLoginAdministrador.setSucursalhasloginAdministradorPK(sucursalHasLoginAdministradorPK);
                
                sucursalHasLoginAdministrador.setSucursal(sucursal);
                
                Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
                
                appGroupPK.setGroupid("guanabara_sucursal_administrador");
                
                appGroupPK.setLoginAdministradorusername(sucursalAdministrator);
                
                sucursalHasLoginAdministrador.setAppGroup(this.appGroupController.find(appGroupPK));
                
                sucursalHasLoginAdministrador.setLoginAdministrador(this.loginAdministradorController.find(sucursalAdministrator));
                
                this.sucursasHasLoginAdministradorController.create(sucursalHasLoginAdministrador);*/
                
             }
             
               
             
             this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""), sucursal);
         
         }
         catch(javax.ejb.EJBException ex){
         
         
         }
     
     }
     
     private int sucursalAdministradorNumber(Entities.LoginAdministrador user){
     
     
    int n=0;
    
    
  /*  for(Entities.SucursalhasloginAdministrador aux:user.getSucursalhasloginAdministradorCollection()){
    
   if(aux.getSucursalhasloginAdministradorPK().getAPPGROUPgroupid().equals("guanabara_sucursal_administrador")){
    
    n++;
    
    }
    
    }*/
    
    return n;
     
     }
     
     
     private java.util.List<Clases.IndexCategoryPOJOInterface>indexList;
     
     @Override
     public java.util.List<Clases.IndexCategoryPOJOInterface>getIndexList(){
     
     return this.indexList;
     
     }
     
     @Override
     public void setIndexList(java.util.List<Clases.IndexCategoryPOJOInterface>indexList){
     
         this.indexList=indexList;
     
     }
     
     
     @Override
     public void loadSucursalCategoriesPictures(Entities.SucursalPK sucursalPK,String username){
     
     try{
     
         if(this.indexList==null){
         
             this.indexList=new java.util.ArrayList<Clases.IndexCategoryPOJOInterface>();
         
         }
         
         else{
         
             this.indexList.clear();
         
         }
         
         Entities.Sucursal sucursal=this.find(sucursalPK);
         
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
         
         this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+username);
         /*
         for(Entities.Categoria category:sucursal.getCategoriaCollection()){
         
         for(Entities.Imagen image:category.getImagenCollection()){
         
         this.fileUpload.loadImagesServletContext(image, username);
         
         Clases.IndexCategoryPOJOInterface aux=new Clases.IndexCategoryPOJO();
         
         aux.setKey(category.getCategoriaPK());
         
         aux.setImageRoot(this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1));
         
         this.indexList.add(aux);
         }
         
         }*/
         
     }
     catch(javax.ejb.EJBException ex){
     
         Logger.getAnonymousLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
     
     }
     
     }
     
  
    @Override
   public void loadProductImages(String username,Entities.SucursalPK sucursalPK){
    
    try{
        
    Entities.Sucursal suc=this.find(sucursalPK);
    /*
    for(Entities.Categoria categoria:suc.getCategoriaCollection()){
    
        for(Entities.Producto product:categoria.getProductoCollection()){
        
            for(Entities.Imagen image:product.getImagenCollection()){
            
                this.fileUpload.loadImagesServletContext(image, username);
            
            }
        
        }
    
    }*/
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(ProductoController.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
   
   
   @Override
   public org.primefaces.model.map.Marker getMarker(Entities.SucursalPK sucursalPK){
   
       try{
           
           Entities.Sucursal suc=this.find(sucursalPK);
           
           Entities.Direccion address=suc.getDireccion();
           
           org.primefaces.model.map.LatLng latitudeLongotude=new org.primefaces.model.map.LatLng(address.getGpsCoordinatesidgpsCoordinated().getLatitude(),address.getGpsCoordinatesidgpsCoordinated().getLongitude());
       
           org.primefaces.model.map.Marker marker=new org.primefaces.model.map.Marker(latitudeLongotude,suc.getFranquicia().getNombre());
                   
                   
           return marker;        
       }
       catch(Exception ex){
       
       Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new javax.faces.FacesException(ex);
       
       }
   
   }
   
   @Override
   public void updateSucursalHasProduct(Entities.Sucursal sucursal){
   
   try{
   
       this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""), sucursal);
       
   }
   catch(javax.ejb.EJBException ex){
   
       Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new javax.faces.FacesException(ex);
   
   }
   
   }
 
   @Override
   public void updateMenu(Entities.Sucursal sucursal){
   
   try{
   
       this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""), sucursal);
       
   }catch(Exception ex){
   
       Logger.getLogger(SucursalController.class.getName()).log(Level.SEVERE,null,ex);
       
       throw new javax.faces.FacesException(ex);
   
   }
   
   }
   
   @Override
   public void updateAddition(Entities.Sucursal sucursal){
   
       try{
       
           this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""),sucursal);
           
       }
       catch(javax.ejb.EJBException ex){
       
           Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
   
   }
   @Override
   public void updateSale(Entities.Sucursal sucursal){
   
       try{
       
           this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""), sucursal);
           
       }
       catch(javax.ejb.EJBException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
   
   }
   
   /**
	 * 
	 * @param sucursal
	 */
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	@Override
   public void createSucursal(Entities.Sucursal sucursal){
   
       try{
           
           java.util.List<Entities.SucursalHasDay>sucursalHasDayList=null;
           
           if(sucursal.getSucursalHasDayCollection()!=null && !sucursal.getSucursalHasDayCollection().isEmpty()){
               
               sucursalHasDayList=new java.util.ArrayList<Entities.SucursalHasDay>();
           
           for(Entities.SucursalHasDay sucursalHasDay:sucursal.getSucursalHasDayCollection()){
           
               Entities.SucursalHasDay aux=new Entities.SucursalHasDay();
               
               aux.setSucursalHasDayPK(new Entities.SucursalHasDayPK());
               
               aux=sucursalHasDay;
               
               sucursalHasDayList.add(aux);
           
           }
           
           }
           
           sucursal.setSucursalHasDayCollection(null);
       
           this.sucursalFacadeREST.create(sucursal);
           
           if(sucursalHasDayList!=null && !sucursalHasDayList.isEmpty()){
           
               for(Entities.SucursalHasDay sucursalHasDay:sucursalHasDayList){
               
                   
                   sucursalHasDay.setSucursalId(sucursal);
                   
                   sucursalHasDay.getSucursalHasDayPK().setSucursalIdSucursal(sucursal.getSucursalPK().getIdsucursal());
                   
                  this._sucursalHasDayController.create(sucursalHasDay);
               
               }
           
           }
       
           System.out.print("SUCURSAL CREATED");
           
       }
       catch(NullPointerException ex){
       
           ex.printStackTrace(System.out);
       
       }
   
   }
   
   
   /**
	 * 
	 * @param branch
	 */
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
	@Override
   public void update(Entities.Sucursal branch){
   
       try{
       
           this.sucursalFacadeREST.edit(new PathSegmentImpl("bar;idsucursal="+branch.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+branch.getSucursalPK().getFranquiciaIdfranquicia()+""), branch);
       
       }
       catch(Exception ex){
       
           if(ex instanceof javax.transaction.RollbackException){
           
               update(branch);
           
           }
       
       }
   
   }

	/**
	 * 
	 * @param key
	 */
	@Override
	public java.util.List<Entities.LoginAdministrador> findWorkers(Entities.SucursalPK key) {
	
            try{
            
                if(this.find(key)!=null){
                
                    Entities.Sucursal branch=this.find(key);
                    
                    if(branch.getLoginAdministradorCollection()!=null && !branch.getLoginAdministradorCollection().isEmpty()){
                    
                        return (java.util.List<Entities.LoginAdministrador>) branch.getLoginAdministradorCollection();
                      
                    }
                
                }
            
                
                return null;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            return null;
            
            }
            
	}

	/**
	 * 
	 * @param key
	 */
	public java.util.List<Entities.LoginAdministrador> findManagers(Entities.SucursalPK key) {
		// TODO - implement SucursalController.findManagers
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param key
	 * @param userCoordinates
	 */
	@Override()
	public boolean isInRange(Entities.SucursalPK key, Clases.EdenGpsCoordinates userCoordinates) {
	
            try{
                
                if(this.find(key)!=null){
                
                    Entities.Sucursal branch = this.find(key);
                
                    if( branch.getDireccion()!=null && branch.getDireccion().getGpsCoordinatesidgpsCoordinated()!=null){
                    
                    double distance = (Clases.GPSEdenOperations.distance(userCoordinates.getLatitude(), branch.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude(), userCoordinates.getLongitude(), branch.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude()))/1000;
                    
                    if(branch.getAlcance() >= distance){
                    
                        return true;
                    
                    }
                    
                    }
                    
                
                }
            
                return false;
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                return false;
            
            }
            
	}
   
}
