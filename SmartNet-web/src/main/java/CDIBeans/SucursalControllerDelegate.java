/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.SucursalPK;

/**
 *
 * @author luisnegrete
 */
public interface SucursalControllerDelegate {
 
    Entities.Sucursal find(SucursalPK sucursalPK);
    
    boolean isOpen(Entities.SucursalPK sucursalPK);
    
    void loadImages(Entities.Sucursal sucursal,String username);
    
    String getSucursalAdministrator(Entities.SucursalPK sucursalPK);
    
    void  editSucursal(Entities.Sucursal sucursal,String sucursalAdministrator);
    
    void delete(Entities.SucursalPK sucursalPK);
    
    void loadSucursalCategoriesPictures(Entities.SucursalPK sucursalPK, String username);
    
   void loadProductImages(String username,Entities.SucursalPK sucursalPK);
   
   void setIndexList(java.util.List<Clases.IndexCategoryPOJOInterface>indexList);
   
   java.util.List<Clases.IndexCategoryPOJOInterface>getIndexList();
   
   org.primefaces.model.map.Marker getMarker(Entities.SucursalPK sucursalPK);
   
   void updateSucursalHasProduct(Entities.Sucursal sucursal);
   
   void updateMenu(Entities.Sucursal sucursal);
   
   void updateAddition(Entities.Sucursal sucursal);
   
   void updateSale(Entities.Sucursal sucursal);
   
   SessionClasses.EdenList<Entities.LoginAdministrador>getWorkers(Entities.Sucursal sucursal);
   
   void createSucursal(Entities.Sucursal sucursal);
   
   void update(Entities.Sucursal branch);

	/**
	 * 
	 * @param key
	 */
	java.util.List<Entities.LoginAdministrador> findWorkers(Entities.SucursalPK key);

	/**
	 * 
	 * @param key
	 */
	java.util.List<Entities.LoginAdministrador> findManagers(Entities.SucursalPK key);

	/**
	 * 
	 * @param key
	 * @param userCoordinates
	 */
	boolean isInRange(Entities.SucursalPK key, Clases.EdenGpsCoordinates userCoordinates);
   
}
