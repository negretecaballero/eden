/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SucursalHasDayControllerQualifier

public class SucursalHasDayControllerImplementation implements SucursalHasDayController{
    
    @javax.ejb.EJB
    private jaxrs.service.SucursalHasDayFacadeREST sucursalHasDayFacadeREST;
    
    
    /**
	 * 
	 * @param sucursalHasDay
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.SucursalHasDay sucursalHasDay){
    
        try{
        
            this.sucursalHasDayFacadeREST.create(sucursalHasDay);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
