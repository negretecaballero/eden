/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SupplierApplicationController {
    
    Entities.SupplierApplication find(String nit);
    
    void create(Entities.SupplierApplication application);
    
    java.util.List<Entities.SupplierApplication>findByState(Entitites.Enum.SupplierApplicationState applicationState);
    
    void update(Entities.SupplierApplication supplierApplication);
    
    void delete(String nit);
    
    SessionClasses.EdenList<Entities.SupplierApplication>getSupplierApplicationUsernameState(String mail, Entitites.Enum.SupplierApplicationState state);
    
}
