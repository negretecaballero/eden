/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SupplierApplicationControllerQualifier

public class SupplierApplicationControllerImplementation implements SupplierApplicationController {
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierApplicationFacadeREST _supplierApplicationFacadeREST;
    
    
    @Override
    public Entities.SupplierApplication find(String nit){
    
        try{
        
            if(_supplierApplicationFacadeREST.find(nit)!=null){
            
            return _supplierApplicationFacadeREST.find(nit);
        
            }
            return null;
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    /**
	 * 
	 * @param application
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void create(Entities.SupplierApplication application){
    
        try{
        
            this._supplierApplicationFacadeREST.create(application);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    /**
	 * 
	 * @param supplierApplication
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void update(Entities.SupplierApplication supplierApplication){
    
        try{
        
            _supplierApplicationFacadeREST.edit(supplierApplication.getNit(), supplierApplication);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    @Override
    public java.util.List<Entities.SupplierApplication>findByState(Entitites.Enum.SupplierApplicationState applicationState){
    
        try{
        
            return this._supplierApplicationFacadeREST.findByState(applicationState);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    /**
	 * 
	 * @param nit
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void delete(String nit){
    
        try{
        
            _supplierApplicationFacadeREST.remove(nit);
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.SupplierApplication>getSupplierApplicationUsernameState(String mail, Entitites.Enum.SupplierApplicationState state){
    
    try{
    
        return this._supplierApplicationFacadeREST.findByUsernameApplicationState(mail, state);
        
    }
    
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
}
