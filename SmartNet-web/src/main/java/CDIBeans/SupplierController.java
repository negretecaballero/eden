/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */



public interface SupplierController {
    
    Entities.Supplier find(Entities.SupplierPK key);
    
    void create(Entities.Supplier supplier);
    
    java.util.Vector<Entities.Supplier>getByUsername(String username);
    
    Entities.Supplier findByNit(String nit);
    
}
