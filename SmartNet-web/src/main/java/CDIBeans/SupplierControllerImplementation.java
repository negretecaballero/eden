/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entitites.Enum.SupplierApplicationState;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SupplierControllerQualifier

public class SupplierControllerImplementation implements SupplierController{
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierFacadeREST _supplierFacadeREST;
    
    @javax.inject.Inject
    @CDIBeans.SupplierApplicationControllerQualifier
    private CDIBeans.SupplierApplicationController _supplierApplicationController;
    
    
    @Override
    public Entities.Supplier find(Entities.SupplierPK key){
    
    try{
    
    return    _supplierFacadeREST.find(new PathSegmentImpl("bar;idSupplier="+key.getIdSupplier()+";loginAdministratorUsername="+key.getLoginAdministradorUsername()+""));
    
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="01/12/2015",comments="Persist Supplier")
    public void create(Entities.Supplier supplier){
    
    try{
    
        _supplierFacadeREST.create(supplier);
    
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Supplier>getByUsername(String username){
    
        try{
        
            if(_supplierApplicationController.getSupplierApplicationUsernameState(username, Entitites.Enum.SupplierApplicationState.accepted)!=null && _supplierApplicationController.getSupplierApplicationUsernameState(username, Entitites.Enum.SupplierApplicationState.accepted).size()>0 ){
            
                java.util.Vector<Entities.Supplier>result=new java.util.Vector<Entities.Supplier>(_supplierApplicationController.getSupplierApplicationUsernameState(username, Entitites.Enum.SupplierApplicationState.accepted).size());
            
                for(Entities.SupplierApplication supplierApplication:_supplierApplicationController.getSupplierApplicationUsernameState(username, Entitites.Enum.SupplierApplicationState.accepted)){
                
                if(supplierApplication.getIdSupplier()!=null){
                
                    result.add(supplierApplication.getIdSupplier());
                
                }
                
                }
              return result;  
            }
            else{
            
                return null;
                
            }
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    
    @Override
    public Entities.Supplier findByNit(String nit){
    
        try{
        
            if(_supplierApplicationController.find(nit)!=null){
            
                Entities.SupplierApplication supplierApplication=_supplierApplicationController.find(nit);
                
                if(supplierApplication.getIdSupplier()!=null && supplierApplication.getApplicationState()==Entitites.Enum.SupplierApplicationState.accepted){
                
                    return supplierApplication.getIdSupplier();
                
                }
            
            }
            
        return null;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    
}
