/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SupplierProductCategoryController {
    
    Entities.SupplierProductCategory find(Entities.SupplierProductCategoryPK key);
    
    void create(Entities.SupplierProductCategory supplierProductCategory);
    
    void delete(Entities.SupplierProductCategoryPK key);
    
}
