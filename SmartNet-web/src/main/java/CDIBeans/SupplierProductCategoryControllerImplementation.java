/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SupplierProductCategoryControllerQualifier

public class SupplierProductCategoryControllerImplementation implements SupplierProductCategoryController {
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierProductCategoryFacadeREST _supplierProductCategoryFacadeREST;
    
    @Override
    public Entities.SupplierProductCategory find(Entities.SupplierProductCategoryPK key){
    
        try{
        
            return this._supplierProductCategoryFacadeREST.find(new PathSegmentImpl("bar;idSupplierProductCategory="+key.getIdSupplierProductCategory()+";supplierIdsupplier="+key.getSupplierIdsupplier()+";supplierLoginAdministradorUsername="+key.getSupplierLoginAdministradorUsername()+""));
        
        }
        
        catch(javax.ejb.EJBException | StackOverflowError ex){
    
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
    }
    
    }
    
    @Override
    public void create(Entities.SupplierProductCategory supplierProductCategory){
    
        try{
        
            _supplierProductCategoryFacadeREST.create(supplierProductCategory);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    @Override
    public void delete(Entities.SupplierProductCategoryPK key){
    
        try{
        
            this._supplierProductCategoryFacadeREST.remove(new PathSegmentImpl("bar;idSupplierProductCategory="+key.getIdSupplierProductCategory()+";supplierIdsupplier="+key.getSupplierIdsupplier()+";supplierLoginAdministradorUsername="+key.getSupplierLoginAdministradorUsername()+""));
        
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
}
