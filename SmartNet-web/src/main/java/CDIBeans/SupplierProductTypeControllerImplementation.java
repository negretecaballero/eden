/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SupplierProductTypeControllerQualifier

public class SupplierProductTypeControllerImplementation implements SupplierProductTypeController{
    
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierProductTypeFacadeREST _supplierProductTypeFacadeREST;
    
    
    @Override
    public Entities.SupplierProductType find(int idSupplierProductType){
    
        try{
        
            return _supplierProductTypeFacadeREST.find(idSupplierProductType);
        
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return null;
            
        }
        
    
    }
    
}
