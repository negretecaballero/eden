/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface SupplierTypeController {
    
    java.util.List<Entities.SupplierType>findAll();
    
    Entities.SupplierType find(int id);
    
}
