/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
@javax.inject.Named("supplierTypeController")
@SupplierTypeControllerQualifier
public class SupplierTypeControllerImplementation implements SupplierTypeController{
    
 @javax.ejb.EJB
 private jaxrs.service.SupplierTypeFacadeREST supplierTypeFacadeREST;
 
 @Override
 public java.util.List<Entities.SupplierType>findAll(){
 
     try{
     
         return supplierTypeFacadeREST.findAll();
     
     }
     catch(javax.ejb.EJBException ex){
     
         return null;
     
     }
 
 }
 
 @Override
 public Entities.SupplierType find(int id){
 
 try{
 
     return this.supplierTypeFacadeREST.find(id);
 
 }
 
 catch(javax.ejb.EJBException ex)
 {
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
     return null;
     
 }
 
 }
    
}
