/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
public interface TipoController {
    
    SessionClasses.EdenList<Entities.Tipo>getTipoList();
    
    Entities.Tipo find(int id);
    
    Entities.Tipo findByName(String name);
    
    java.util.Vector<Entities.Tipo>findAll();
}
