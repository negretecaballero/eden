/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@TipoControllerQualifier

public class TipoControllerImplementation implements TipoController{
    
    @javax.ejb.EJB
    private jaxrs.service.TipoFacadeREST tipoFacadeREST;
    
    @Override
    public SessionClasses.EdenList<Entities.Tipo>getTipoList(){
    
    SessionClasses.EdenList<Entities.Tipo>results=new SessionClasses.EdenList<Entities.Tipo>();
    
    java.util.List<Entities.Tipo>list=tipoFacadeREST.findAll();
    
    java.util.Comparator<Entities.Tipo>comparator=new Clases.TipoComparator();
    
    java.util.Collections.sort(list, comparator);
    
    for(Entities.Tipo aux:list)
    {
        results.addItem(aux);
    
    }
 
    
    return results;
    
    }
    
    @Override
    public Entities.Tipo find(int id){
    
        try{
        
            return this.tipoFacadeREST.find(id);
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    @Override
    public Entities.Tipo findByName(String name){
    
    try{
    
        return this.tipoFacadeREST.findByName(name);
        
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
    }
    
    }
    
    @Override
    public java.util.Vector<Entities.Tipo>findAll(){
    
    try{
    
        java.util.Vector<Entities.Tipo>results=new java.util.Vector<Entities.Tipo>();
        
        for(Entities.Tipo tipo:this.tipoFacadeREST.findAll()){
        
            results.add(tipo);
        
        }
        
        return results;
    
    }
    catch(javax.ejb.EJBException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    
    }
    
    
}
