/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Producto;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.CategoriaFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@DesplegablesProductoQualifier
@Named("desplegablesproducto")
@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})
public class desplegablesProducto implements desplegablesProductoInterface{
    

 
    private List<Producto>productsList;
    
    
    @EJB
    CategoriaFacadeREST categoriaFacade;
    
    @Override
    public void setProductsList(List<Producto>productsList){
    
        this.productsList=productsList;
    
    }
    
    @Override
    public List<Producto> getProductsList(){
    
    return this.productsList;
    
    }
    
    @PostConstruct
    public void init(){
    
        System.out.print("DesplegableProducto Postconstructed");
        
    Categoria result=null;
    
    Entities.Franquicia franchise=null;
    
    HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
    if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
    
       franchise=((Entities.Franquicia)session.getAttribute("selectedOption"));
    
    }
    
    
    for(Categoria categoria:categoriaFacade.findByFranchise(franchise.getIdfranquicia()))
    {
    
    result=categoria;
    
    break;
        
    }
    
    if(result!=null){
    
    UpdateProductsList(result.getCategoriaPK());
    
    }
    
    }
    
    public void resetList(){
    
    this.productsList.clear();
    
    Categoria result=null;
    
    FacesContext fc=FacesContext.getCurrentInstance();
    
    HttpSession session=(HttpSession)fc.getExternalContext().getSession(true);
    
    
    Entities.Franquicia franchise=null;
    
    if(session.getAttribute("selectedOption") instanceof Entities.Franquicia)
    {
    
     franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
    
    }
    
    
    for(Categoria categoria:categoriaFacade.findByFranchise(franchise.getIdfranquicia()))
    {
    
    result=categoria;
    
    break;
        
    }
    
    if(result!=null){
    
    UpdateProductsList(result.getCategoriaPK());
    
    }
    
    }
    
    
    @Override
    public void UpdateProductsList(CategoriaPK categoriaPK){
    
    if(this.productsList!=null){
    this.productsList.clear();
    }    
    
    PathSegment ps=new PathSegmentImpl("bar;idcategoria="+categoriaPK.getIdcategoria()+";franquiciaIdfranquicia="+categoriaPK.getFranquiciaIdfranquicia()+"");
    
    Categoria categoria=categoriaFacade.find(ps);
    
    this.productsList=(List)categoria.getProductoCollection();
        
       
    }
    
    @Override
    public void removeDesplegablesProducto(Producto product){
        
    for(int i=0;i<this.productsList.size();i++)
    {
          
        if(this.productsList.get(i).equals(product)){
        
        productsList.remove(i);
            
        }
        
    }
        
        
    }
    
}
