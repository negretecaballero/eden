/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIBeans;

import Entities.CategoriaPK;
import Entities.Producto;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface desplegablesProductoInterface {
    
    void setProductsList(List<Producto>productsList);
    
    List<Producto> getProductsList();
    
    void UpdateProductsList(CategoriaPK categoriaPK);
    
    void removeDesplegablesProducto(Producto product);
    
    void resetList();
}
