/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.context.Dependent

@AgregarProductoControllerQualifier

public class AgregarProductoController extends Clases.BaseBacking implements AgregarProductoControllerInterface{
    
     @javax.inject.Inject 
     
     @CDIBeans.ProductoControllerQualifier
     
     private  CDIBeans.ProductoControllerInterface productController;
     
     @javax.ejb.EJB
     private jaxrs.service.BarcodeFacadeREST barcodeFacadeREST;
     
    
    @javax.inject.Inject 
    
    @CDIBeans.FileUploadBeanQualifier
    
    private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject
    
    @CDIBeans.AdditionControllerQualifier
    
    private CDIBeans.AdditionControllerDelegate additionController;
    
    public AgregarProductoController(){
        
     
     

    
    }
    
    @Override
    public void loadProductsImages(Entities.ProductoPK idProducto){
    
        try{
        
            
           Entities.Producto product=this.productController.find(idProducto);
        
           
           if(product!=null){
            
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
           java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString()+java.io.File.separator+"products");
      
           if(!file.exists() || !file.isDirectory())
           {
           
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString()+java.io.File.separator, "products");
           
           }
           
           java.io.File file2=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
           
           if(!file2.exists() || !file2.isDirectory())
           {
           
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator,product.getNombre());
           
           }
           
           if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
               this.fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)product.getImagenCollection()).get(0), getSession().getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
           
           }
           
           }
         this.fileUpload.getImages().clear();
         
         this.fileUpload.getPhotoList().clear();
        }
        catch(Exception ex){
        
        Logger.getLogger(AgregarProductoController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden> initAdditions(int idFranchise){
    
    try{
    
       return this.additionController.loadImages(idFranchise);
    
    }
    catch(Exception ex){
    
        Logger.getLogger(AgregarProductoController.class.getName()).log(Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
    }
    
    }
    
    @Override
    public boolean checkAdditionExistence(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>list,Entities.AdditionPK additionPK){
    
        try{
        
            for(FranchiseAdministrator.Product.Classes.AdditionEden addition:list){
            
                if(addition.getAdditionPK().equals(additionPK)){
                
                    return true;
                
                }
            
            }
        return false;
        }
        catch(Exception ex){
        
            Logger.getLogger(AgregarProductoController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    
    @Override
    public void createBarcode(String barcode){
    
        try{
            
            Entities.Barcode item=new Entities.Barcode();
        
            item.setBarcode(barcode);
            
            this.barcodeFacadeREST.create(item);
        
        }
        catch(NumberFormatException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public Entities.Barcode findBarcode(String barcode){
    
    try{
    
        return this.barcodeFacadeREST.findByBarcode(barcode);
    
    }
    catch (javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);        
    
    }
    
    }
    
    
    
    @Override
    public Entities.Producto findProductByBarcode(String barcode){
    
        try{
        
            if(this.barcodeFacadeREST.findByBarcode(barcode)!=null){
            
                System.out.print("Barcode is not null in database");
                
                Entities.Barcode code=this.barcodeFacadeREST.findByBarcode(barcode);
                
                if(code!=null){
                
                if(code.getProductoCollection()!=null && !code.getProductoCollection().isEmpty())
                {
                
                    return ((java.util.List<Entities.Producto>)code.getProductoCollection()).get(0);
                
                }
                }
            }
        
            return null;
        }
        catch(javax.ejb.EJBException | StackOverflowError ex)
        {
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    }
    
    @Override
    public void loadImagesBarcode(String barcode){
    
        try{
        
            if(this.findProductByBarcode(barcode)!=null){
            
                Entities.Producto product=this.findProductByBarcode(barcode);
                
                for(Entities.Imagen image:product.getImagenCollection()){
                
                   this.fileUpload.loadImagesServletContext(image, this.getSession().getAttribute("username").toString());
                    
                
                }
                
                SessionClasses.EdenList<Entities.Imagen>imageList=new SessionClasses.EdenList<Entities.Imagen>();
                
                for(Entities.Imagen image:this.fileUpload.getPhotoList()){
                
                    Entities.Imagen imagen=new Entities.Imagen();
                    
                    imagen.setImagen(image.getImagen());
                    
                    imagen.setExtension(image.getExtension());
                
                    
                    imageList.addItem(imagen);
                    
                }
                
                this.fileUpload.getPhotoList().clear();
                
                for(Entities.Imagen image:imageList){
                
                this.fileUpload.getPhotoList().add(image);
                
                }
              System.out.print("Images in PhotoList "+this.fileUpload.getPhotoList().size());  
            
            }
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
}
