/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface AgregarProductoControllerInterface {
    
    void loadProductsImages(Entities.ProductoPK idProducto);
    
    java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>initAdditions(int idFranchise);
    
   boolean checkAdditionExistence(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>list,Entities.AdditionPK additionPK);
 
   void createBarcode(String barcode);
   
   Entities.Barcode findBarcode(String barcode);
   
   Entities.Producto findProductByBarcode(String barcode);
   
   void loadImagesBarcode(String barcode);
   
}
