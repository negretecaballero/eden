/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface AutoCompleteController {
    
    void updateAutoCompleteList(String search, ENUM.Role role);
    
    java.util.List<Object>getObjectAutoComplete();

	/**
	 * 
	 * @param search
	 * @param role
	 */
	java.util.List<String> getAutoComplete(String search, ENUM.Role role);
    
}
