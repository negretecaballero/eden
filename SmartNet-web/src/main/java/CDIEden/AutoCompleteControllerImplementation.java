/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import Clases.EdenString;
import java.io.*;

/**
 *
 * @author luisnegrete
 */


@AutoCompleteControllerQualifier
@Stereotypes.SessionStereotype
@javax.inject.Named("autocompleteController")
@javax.enterprise.context.SessionScoped
public class AutoCompleteControllerImplementation implements java.io.Serializable, AutoCompleteController{
  
 @javax.ejb.EJB
 private SessionBeans.AutoCompleteFacadeLocal autoCompleteFacade;
 
 
 @javax.inject.Inject 
 @CDIBeans.FranchiseControllerQualifier
 private transient CDIBeans.FranchiseControllerInterface _franchiseController;
 
 @javax.inject.Inject
 @CDIBeans.ProductoControllerQualifier
 private transient CDIBeans.ProductoControllerInterface _productController;
 
 @javax.inject.Inject 
 @CDIBeans.MenuControllerQualifier
 private transient CDIBeans.MenuControllerInterface _menuController;
 
 private java.util.List<Object>resultList;
 
 @Override
 public java.util.List<Object>getObjectAutoComplete(){
 
 return this.autoCompleteFacade.getObjectList();
 
 }
 
 /**
	 * 
	 * @param search
	 * @param role
	 */
	@Override
 public java.util.List<String>getAutoComplete(String search, ENUM.Role role){
 
     /*if(autoCompleteFacade.getAutoComplete()==null){
     
         autoCompleteFacade.setAutoComplete(new java.util.ArrayList<String>());

     }*/
     
     System.out.print("AUTOCOMPLETE GET METHOD VALUE "+search);
     
     this.updateAutoCompleteList(search, role);
     
 return autoCompleteFacade.getAutoComplete();
 
 }
 
 private boolean contains(java.util.List<String>list,String name){
     
     System.out.print("ITEMS IN LIST "+list.size());
 
     for(String aux:list){
     
         if(aux.equals(name)){
         
             System.out.print("THERE IS A SIMILAR NAME");
             
             return true;
         
         }
     
     }
     
     System.out.print("THERE IS NO SIMILAR NAME");
     
     return false;
 
 }
    
 
 @Override
 public void updateAutoCompleteList(String search, ENUM.Role role){
 
 try{
 
     if( this.autoCompleteFacade.getAutoComplete()!=null){
     
     this.autoCompleteFacade.getAutoComplete().clear();
     
     }
     
     else{
     
         this.autoCompleteFacade.setAutoComplete(new java.util.ArrayList<String>());
     
     }
     
     if( this.getObjectAutoComplete()!=null){
     
     this.getObjectAutoComplete().clear();
     
     }
     
    switch(role){
    
        case Franchise:{
        
            System.out.print("You are updating a franchise");
            
             for(Entities.Franquicia aux:this._franchiseController.findByTypeName("Supplier"))
            {
            
            if(EdenString.similarity(aux.getNombre().toLowerCase(), search.toLowerCase())>=.25){
            
                if(!this.contains(this.autoCompleteFacade.getAutoComplete(), aux.getNombre()))
                        
                {
                            
                this.autoCompleteFacade.getAutoComplete().add(aux.getNombre());
                
                this.getObjectAutoComplete().add(aux);
                
                }
            }
            
            }
    
            
        break;
        
        }
        
        case User:{
        
           System.out.print("YOU ARE IN USER");
            
            for(Entities.Franquicia franchise:_franchiseController.findAll()){
            
                if(EdenString.similarity(franchise.getNombre().toLowerCase(), search.toLowerCase())>=.25)
                {
                
                 if(!this.autoCompleteFacade.getAutoComplete().contains(franchise.getNombre())){   
                    
                this.autoCompleteFacade.getAutoComplete().add(franchise.getNombre());
                
                this.getObjectAutoComplete().add(franchise);
                
                 }
                    
                }
            }
            
            
            //Methods for second version
            
            /*
            
            for(Entities.Producto product:_productController.findAll()){
            
                if(EdenString.similarity(product.getNombre().toLowerCase(), search.toLowerCase())>=.25){
                
                    if(!this.getAutoComplete().contains(product.getNombre())){
                    
                    this.getAutoComplete().add(product.getNombre());
                    
                    this.getObjectAutoComplete().add(product);
                
                    }
                }
            
            }
            
            for(Entities.Menu menu:_menuController.findAll()){
            
            if(EdenString.similarity(menu.getNombre().toLowerCase(), search.toLowerCase())>=.25){
            
            if(!this.getAutoComplete().contains(menu.getNombre()))    
            { 
            this.getAutoComplete().add(menu.getNombre());
            
            this.getObjectAutoComplete().add(menu);
            }
            }
                
            }
            */
            break;
            
        }
        
        case Supplier:{
            
            System.out.print("THIS IS A SUPPLIER");
            
            for(Entities.Franquicia franchise:this._franchiseController.findAll()){
            
            if(!franchise.getIdSubtype().getIdTipo().getNombre().equals("Supplier") && !this.autoCompleteFacade.getAutoComplete().contains(franchise.getNombre())){
            
                this.autoCompleteFacade.getAutoComplete().add(franchise.getNombre());
                
                this.getObjectAutoComplete().add(franchise);
            
            }
            
            }
        
            break;
           
        }
    
    }
 
 }
 catch(javax.ejb.EJBException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
 
 }
 
 }
 
}
