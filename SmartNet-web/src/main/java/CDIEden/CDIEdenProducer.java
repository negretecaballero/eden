/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public class CDIEdenProducer {
   
    
    @javax.enterprise.inject.Produces
    @AgregarProductoControllerQualifier
    public AgregarProductoController agregarProductoControllerFactory(){
    
        return new CDIEden.AgregarProductoController();
    
    }
    
    @javax.enterprise.inject.Produces
    @AutoCompleteControllerQualifier
    public AutoCompleteController autoCompleteControllerFactory(){
    
    return new CDIEden.AutoCompleteControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @CRUDSUCViewControllerQualifier
    
    public CRUDSUCViewController cRUDSUCViewControllerFactory(){
    
        return new CDIEden.CRUDSUCViewControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @CompleteRegistrationControllerQualifier
    
    public CompleteRegistrationControllerInterface completeRegistrationControllerFactory(){
    
    return new CDIEden.CompleteRegistrationController();
    
    }
    
    @javax.enterprise.inject.Produces
    @EdenRedirectionQualifier
    public EdenRedirection edenRedirectionFactory(){
    
        return new CDIEden.EdenRedirectionImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @EdenWorkerControllerQualifier
    public EdenWorkerControllerInterface edenWorkerControllerProducer(){
    
        return new CDIEden.EdenWorkerController();
    
    }
    
    @javax.enterprise.inject.Produces
    @EdenWorkerNoteQualifier
    public EdenWorkerNote edenWorkerNoteFactory(){
    
        return new CDIEden.EdenWorkerNoteImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
   
    @FileControllerUpdatedQualifier
    
    public FileController fileControllerFactory(){
    
        return new CDIEden.FileControllerImplementation();
    
    }
    
    
    @javax.enterprise.inject.Produces
    
    @FlowRegistrationControllerQualifier
    
    @javax.enterprise.context.SessionScoped
    
    public FlowRegistrationController flowRegistrationControllerFactory(){
    
        return new CDIEden.FlowRegistrationControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @FranchiseAdministratorInventoryControllerQualifier
    public FranchiseAdministratorInventoryController franchiseAdministratorInventoryControlleFactory(){
    
        return new CDIEden.FranchiseAdministratorInventoryControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @FranchiseAdministratorProductControllerQualifier
    public FranchiseAdministratorProductController franchiseAdministratorProductControllerFactory(){
    
        return new CDIEden.FranchiseAdministratorProductControllerImplementation();
        
    }
    
    @javax.enterprise.inject.Produces
    
    @FranchiseRegistrationControllerQualifier
    
    public FranchiseRegistrationController franchiseRegistrationControllerFactory(){
    
        return new CDIEden.FranchiseRegistrationControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @IndexCategoriesViewControllerQualifier
    public IndexCategoriesViewControllerInterface indexCategoriesControllerFactory(){
    
        return new CDIEden.IndexCategoriesViewController();
    
    }
    
    @javax.enterprise.inject.Produces
    @InvoiceControllerQualifier
    public InvoiceControllerInterface invoiceControllerFactory(){
    
        return new CDIEden.InvoiceController();
    
    }
    
    @javax.enterprise.inject.Produces
    @MailControllerQualifier
    public MailController mailControllerFactory(){
    
        return new CDIEden.MailControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @OrdersControllerQualifier
    public OrdersControllerInterface ordersControllerFactory(){
    
        return new CDIEden.OrdersController();
    
    }
    
    @javax.enterprise.inject.Produces
    
    @SucursalInventoryControllerQualifier
    
    public SucursalInventoryController sucursalInventoryControllerFactory(){
    
        return new CDIEden.SucursalInventoryControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @SucursalProductControllerQualifier
    public SucursalProductController sucursalProductControllerFactory(){
    
        return new CDIEden.SucursalProductControllerImplementation();
    
    }
    
    @javax.enterprise.inject.Produces
    @UserPedidoControllerQualifier
    public UserPedidoController userPedidoControllerFactory(){
    
        return new CDIEden.UserPedidoControllerImplementation();
    
    }
    
@javax.enterprise.inject.Produces
@FranchiseAdministratorCategoryControllerQualifier
public franchiseAdministratorCategoryController franchiseAdministradorCategoryControllerFactory(){

    return new CDIEden.franchiseAdministratorCategoryControllerImplementation();

}
    
    
    
}
