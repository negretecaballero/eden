/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface CRUDSUCViewController {
    
    java.util.List<Entities.Day>getDays();
    
    java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getSucursalHasdayList();
    
    void createSucursalOpenList(Entities.Sucursal sucursal, java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>sucursalHasDateLayout);
}
