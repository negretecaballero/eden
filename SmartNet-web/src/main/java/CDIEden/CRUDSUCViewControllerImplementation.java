/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@CRUDSUCViewControllerQualifier

public class CRUDSUCViewControllerImplementation implements CRUDSUCViewController {
    
    @javax.inject.Inject
    @CDIBeans.DayControllerQualifier
    private CDIBeans.DayController _dayController;
    
    @javax.inject.Inject
    @CDIBeans.SucursalHasDayControllerQualifier
    private CDIBeans.SucursalHasDayController _sucursalHasDayController;
    
    
    @Override
    public java.util.List<Entities.Day>getDays(){
    
    try{
    
        return (java.util.List<Entities.Day>)IteratorUtils.toList(_dayController.getAllDays().iterator());
    
    }
    catch(RuntimeException ex){
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getSucursalHasdayList(){
    
    try{
    
        java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>results=new java.util.ArrayList<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>();
        
        for(Entities.Day day:this.getDays()){
        
        FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout aux=new FranchiseAdministrator.Branch.Classes.SucursalHasDateLayoutImplementation();
        
        aux.setDay(day);
        
        aux.setOpenTime(new Clases.EdenDate());
        
        aux.setCloseTime(new Clases.EdenDate());
        
        results.add(aux);
        
        }
        
        return results;
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    @Override
    public void createSucursalOpenList(Entities.Sucursal sucursal, java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>sucursalHasDateLayout){
    
    try{
    
        if(sucursalHasDateLayout!=null && !sucursalHasDateLayout.isEmpty())
        {
        for(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout aux:sucursalHasDateLayout)
        {
        
            Entities.SucursalHasDay sucursalHasDay=new Entities.SucursalHasDay();
            
            sucursalHasDay.setSucursalHasDayPK(new Entities.SucursalHasDayPK());
            
            sucursalHasDay.getSucursalHasDayPK().setDayIdday(aux.getDay().getIdDay());
            
            sucursalHasDay.getSucursalHasDayPK().setSucursalFranquiciaIdfranquicia(sucursal.getSucursalPK().getFranquiciaIdfranquicia());
            
            sucursalHasDay.getSucursalHasDayPK().setSucursalIdSucursal(sucursal.getSucursalPK().getIdsucursal());
            
            sucursalHasDay.setSucursalId(sucursal);
            
            sucursalHasDay.setDayId(aux.getDay());
            
            java.util.Date openTime=new java.util.Date();
            
            openTime.setHours(aux.getOpenTime().getHour());
            
            openTime.setMinutes(aux.getOpenTime().getMinutes());
            
            openTime.setSeconds(0);
            
            sucursalHasDay.setOpenTime(openTime);
            
            java.util.Date closeTime=new java.util.Date();
            
            closeTime.setHours(aux.getCloseTime().getHour());
            
            closeTime.setMinutes(aux.getCloseTime().getMinutes());
            
            closeTime.setSeconds(0);
            
            sucursalHasDay.setCloseTime(closeTime);
        
            _sucursalHasDayController.create(sucursalHasDay);
            
        }
        
    }
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
        
    }
    
    }
    
    
}
