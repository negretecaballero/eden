/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@CompleteRegistrationControllerQualifier

public class CompleteRegistrationController implements CompleteRegistrationControllerInterface{
    
    @javax.inject.Inject @CDIBeans.LoginAdministradorControllerQualifier private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
  
    @javax.inject.Inject @CDIBeans.ProfessionControllerQualifier private CDIBeans.ProfessionControllerInterface professionController;
    
    @javax.inject.Inject @CDIBeans.PhoneControllerQualifier private CDIBeans.PhoneControllerInterface phoneController;
    
    @javax.inject.Inject @CDIBeans.AddressControllerQualifier private CDIBeans.AdressControllerInterface addressController;
    
    @javax.inject.Inject @CDIBeans.ImageControllerQualifier private CDIBeans.ImageControllerDelegate imagenController;
   
    @javax.inject.Inject @CDIBeans.GPSCoordinatesControllerQualifier private CDIBeans.GpsCoordinatesControllerInterface gpsController;
    
    @Override
    public void completeAccount(Entities.LoginAdministrador user, Entities.Direccion address,  Entities.Imagen profileImage){
    
        try{
        
          
              if(profileImage!=null){
            
                
            if(user.getImagenIdimagen()!=null){
            
             System.out.print("Image to Delete "+user.getImagenIdimagen().getIdimagen());   
                
             int idImagen=user.getImagenIdimagen().getIdimagen();
             
             user.setImagenIdimagen(null);
             
             this.loginAdministradorController.edit(user);
             
             this.imagenController.delete(idImagen);
            
            }    

            System.out.print("Image Id "+profileImage.getIdimagen());
            
            if(profileImage.getIdimagen()==null)
            {
           
            user.setImagenIdimagen(profileImage);
             
            }
            
            
            }
            
            
            user.setProfessionIdprofession(this.professionController.find(user.getProfessionIdprofession().getIdprofession()));
       
            
            System.out.print("Phone Collection Sixe "+user.getPhoneCollection().size());
            
            if(user.getPhoneCollection()!=null && !user.getPhoneCollection().isEmpty())
            
            {
                
            java.util.List<Entities.Phone>phoneList=new java.util.ArrayList<Entities.Phone>();
            
            for(Entities.Phone aux:user.getPhoneCollection()){
            
            phoneList.add(aux);
            
            }
            
            
            for(Entities.Phone aux:phoneList){   
                
            String phoneNumber=aux.getPhoneNumber();
                
            if(phoneNumber!=null && !phoneNumber.equals("") && !phoneController.findSimilar(phoneNumber)){
            
            Entities.Phone phone=new Entities.Phone();
            
            phone.setPhoneNumber(phoneNumber);
            
            phone.setLoginAdministradorusername(user);
            
            this.phoneController.create(phone);
            
            user.getPhoneCollection().add(phone);
            
            }
            
            else{
            
                System.out.print("There was a similiar phone number found");
            
            }
            } 
            
            java.util.List<Entities.Phone>phoneUserList=new java.util.ArrayList<Entities.Phone>();
            
            
            System.out.print("Phone found by username "+this.phoneController.findByUsername(user.getUsername()).size());
            
            for(Entities.Phone aux:this.phoneController.findByUsername(user.getUsername())){
            
                phoneUserList.add(aux);
            
            }
            
            for(Entities.Phone aux:phoneUserList){
            
                boolean found=false;
                
                for(Entities.Phone phone:user.getPhoneCollection()){
                
                if(aux.getPhoneNumber().equals(phone.getPhoneNumber())){
                
                    found=true;
                
                }
                
                }
                
                if(!found){
                
                    this.phoneController.delete(aux.getPhoneNumber());
                
                }
            
            }
            
        }
            
            
            if(address!=null)
            
            {
                
            if(!this.gpsExistence((java.util.List)user.getDireccionCollection(), address)){
            
             System.out.print("Ready to next step address");
            
             if(this.addressController.checkExistence(address)==null){   
              
             System.out.print("Ready to create address");
                 
             this.gpsController.create(address.getGpsCoordinatesidgpsCoordinated());
              
             this.addressController.create(address);
            
             System.out.print("New address created");
             
             }
             
             else{
             
             address=this.addressController.checkExistence(address);
                 
             System.out.print("There was found an address like this in the database");
             
             }
            
            user.getDireccionCollection().add(address);  
            
            
            
            }
            
            else{
            
                System.out.print("This Address in linked to this user Already");
            
            }
            
            
        }
            
           
            
            else{
            
                System.out.print("Address is null");
            
            }
            
             for(Entities.Direccion add:this.loginAdministradorController.findAdrresses(user.getUsername())){
            
                boolean found=false;
                
            for(Entities.Direccion auxi:user.getDireccionCollection()){
            
                if(add.equals(auxi)){
                
                    found=true;
                    
                    break;
                
                }
            
            }
            
            if(!found){
            
                System.out.print("ID ADDRESS TO DELETE "+add.getDireccionPK().getIdDireccion());
                
                this.addressController.delete(add);
            
            }
            
            }
            
            this.loginAdministradorController.edit(user);
            
            
            
        }catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(CompleteRegistrationController.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public boolean gpsExistence(java.util.List<Entities.Direccion>addressList,Entities.Direccion address){
    
        try{
       
            System.out.print("Adresses associated to this user "+addressList.size());
            
            for(Entities.Direccion aux:addressList){
            
            if(aux.getCity().equals(address.getCity()) && aux.getTYPEADDRESSidTYPEADDRESS().equals(address.getTYPEADDRESSidTYPEADDRESS()) && aux.getPrimerDigito().equals(address.getPrimerDigito()) && aux.getSegundoDigito().equals(address.getSegundoDigito()) && aux.getTercerDigito()==address.getTercerDigito() && aux.getAdicional().equals(address.getAdicional())){ 
            
                return true;
                
                
            }
    
        }
            
            return false;
        }
        catch(javax.ejb.EJBException ex){
        
        Logger.getLogger(CompleteRegistrationController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }

}
