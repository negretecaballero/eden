/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import Clases.ActionType;
import Clases.ActionTypeInterface;
import EdenFinalUser.Classes.ResultsType;
import EdenFinalUser.Classes.ResultsTypeInterface;
import Entities.Categoria;
import Entities.Imagen;
import Entities.Menu;
import Entities.Producto;
import Entities.Sucursal;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.PathSegment;
import managedBeans.commandlinks;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import Clases.*;

/**
 *
 * @author luisnegrete
 */

@javax.inject.Named("edenRedirection")
@javax.enterprise.context.RequestScoped
@EdenRedirectionQualifier
public class EdenRedirectionImplementation extends BaseBacking implements EdenRedirection {
   
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @Override
    public String redirect(Object o){
    
        if(o instanceof Entities.Producto){
        
        this.setSessionAuxiliarComponent((Entities.Producto)o);
            
        return "success";
        
        }
        
        if(o instanceof Entities.Sale){
        
         this.setSessionAuxiliarComponent((Entities.Sale)o);
        
         return "success";
         
        }
        
        
      if(o instanceof Entities.Inventario){
      
          this.setAuxiliarComponent((Entities.Inventario)o);
      
      return "success";
      }
        
        
        return "failure";
    
    }
    
     private String auxiliar="auxiliarComponent";
     
     @javax.ejb.EJB
     private jaxrs.service.SucursalFacadeREST sucursalFacadeREST;
     
     @javax.inject.Inject 
     @CDIBeans.FileUploadBeanQualifier
     private transient CDIBeans.FileUploadBean fileUpload;
     
    @Override
    public String setAuxiliarComponent(Object obj){

        try{
            

            
            if(obj instanceof Categoria){
            
          javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                
          this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
          
            Categoria cat=(Categoria)obj;
            
            
            System.out.print("Category Name "+cat.getNombre());
            
            getSession().setAttribute(auxiliar, cat);
            
            
            
            for(Imagen image:cat.getImagenCollection()){
            
                fileUpload.loadImagesServletContext(image,getSession().getAttribute("username").toString());
                
            }
            
            return "success";
            
        }
        
          else  if(obj instanceof Entities.Addition){
            
                super.getSession().setAttribute(auxiliar, (Entities.Addition)obj);
                
                return "success";
            
            }
        
        
       else if(obj instanceof Producto){
                
            Producto product=(Producto)obj;
            
            System.out.print("You have selected a product "+product.getNombre());
            
            
            getSession().setAttribute(auxiliar, product);
                 
            this.setAuxiliarComponent(product);
  
            ActionTypeInterface action=new ActionType("editProduct");
            
                    
            this.fileUpload.setActionType(action);
            
            
            System.out.print(product.getNombre()+" has "+product.getImagenCollection().size()+" images");
    
            
            return "success";
        
        }
        
     else   if(obj instanceof Entities.Sale){
        
         System.out.print("SALE TYPE OBJECT");
         
         this.getSession().setAttribute("auxiliarComponent",obj);
         
        
            
            return "success";
        
        }
        
  else      if(obj instanceof Entities.SupplierProductCategory){
        
       System.out.print("OBJECT TYPE SUPPLIERCATEGORY");
            
        this.setAuxiliarComponent(obj);
        
        return "success";
        
        }
        
      else  if(obj instanceof Entities.Inventario){
        
        setSessionAuxiliarComponent(obj);
        
        return "success";
        
        }
        
        
       else if(obj instanceof Entities.Addition){
        
            setSessionAuxiliarComponent(obj);
                
            return "success";
        
        }
        
      else  if(obj instanceof Menu){
        
            Menu menu=(Menu)obj;
            
            setSessionAuxiliarComponent(menu);
            
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
           
            ActionTypeInterface actionType=new ActionType("updateMenu");
            
            this.fileUpload.setActionType(actionType);
            
            for(Imagen image:menu.getImagenCollection()){
            
            this.fileUpload.loadImagesServletContext(image, getSession().getAttribute("username").toString());
            
            }
            
           return "success"; 
        
        }
        
      else  if(obj instanceof ResultsTypeInterface){
        
            System.out.print("OBJ is instance of ResultsType");
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
            
            String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString();
            
            this.fileUpload.Remove(path);
            
            ResultsTypeInterface resultsType=(ResultsType)obj;
            
            System.out.print(resultsType);
            
            PathSegment ps=new PathSegmentImpl("bar;idsucursal="+resultsType.getKey().getIdsucursal()+";franquiciaIdfranquicia="+resultsType.getKey().getFranquiciaIdfranquicia()+"");
            
            Sucursal suc=sucursalFacadeREST.find(ps);
            
            System.out.print("Sucursal EDENREDIRECTION "+suc);
            
            for(Imagen image:suc.getImagenCollection()){
            
            
            this.fileUpload.loadImagesServletContext(image,getSession().getAttribute("username").toString());
            
            
            
            
            }
            
            setSessionAuxiliarComponent(obj);
            
            
        return "sucursal?faces-redirect=true";
        }
        
      else  if(obj instanceof Entities.InventarioHasSucursal){
        
            this.setSessionAuxiliarComponent(obj);
            
            return "success";
        
        }
        
       else if(obj instanceof Entities.Menu){
        
            this.setAuxiliarComponent(obj);
            
            return "success";
        
        }
        
        return "failure";
        
    }catch(StackOverflowError ex){
        
        Logger.getLogger(commandlinks.class.getName()).log(Level.SEVERE,null,ex);
        
        
        
        return "failure";

}
        

}
    
    @Override
    public void HomeRedirection(String username){
    
        try{
        
            if(_loginAdministradorController.profilesNumber(username)==1){
            
            
            
            String view=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId();
            
            System.out.printf("REDIRECTION ARRAY SIZE "+Clases.EdenString.split("/", view).length);
            
            if(Clases.EdenString.split("/", view)!=null && Clases.EdenString.split("/", view).length>0){
            
                
                for(String aux:Clases.EdenString.split("/", view)){
                
                    System.out.print("ARRAY LOOP "+aux);
                
                }
                
                System.out.print("REDIRECTION POSITION "+Clases.EdenString.split("/", view)[0]);
                
                String pattern=Clases.EdenString.split("/", view)[1];
                
                switch (pattern){
                
                    case "User":{
                        
                        javax.faces.context.ExternalContext ec=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                        
                        ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"User"+java.io.File.separator+"welcomeuser.xhtml?faces-redirect=true");
                        
                        break;
                    }
                
                }
            
            }
        
            }
            
            else if(_loginAdministradorController.profilesNumber(username)>1){
            
                javax.faces.context.ExternalContext ec=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                
                ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"selection.xhtml?faces-redirect=true");
            
            }

            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
}
