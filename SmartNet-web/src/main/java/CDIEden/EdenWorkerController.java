/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import CDIBeans.AppGroupControllerDelegate;
import CDIBeans.LoginAdministradorControllerInterface;
import Clases.DataGeneration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@EdenWorkerControllerQualifier

public class EdenWorkerController implements EdenWorkerControllerInterface{
   
   @Inject 
   @CDIBeans.AppGroupControllerQualifier
   private AppGroupControllerDelegate appGroupController;
   
   @Inject 
   @CDIBeans.LoginAdministradorControllerQualifier
   private LoginAdministradorControllerInterface loginAdministradorController;
   
   @Inject  
   @CDIBeans.FranchiseControllerQualifier
   private  CDIBeans.FranchiseControllerInterface franchiseController;
   
   @Inject 
   @CDIBeans.ProfessionControllerQualifier
   private CDIBeans.ProfessionControllerInterface professionController;
   
   @Inject
   @CDIBeans.ImageControllerQualifier
   private CDIBeans.ImageControllerDelegate imageController;
   
   @Inject
   @CDIBeans.FileUploadBeanQualifier
   private CDIBeans.FileUploadInterface fileUpload;

   @javax.inject.Inject 
   @CDIBeans.FranchiseTypeControllerQualifier
   private CDIBeans.FranchiseTypeControllerInterface franchisteType;
   
   @javax.annotation.PostConstruct
   public void init(){
   
    
   
   }
   
   
   @Override
    public boolean checkExistence(String Name){
    
      
             
            if(this.franchiseController.findByName(Name)!=null){
            
                return true;
                
            }
            
            else{
                
                System.out.print("Null Value");
            
        return false;
            }
        
        
        
    
    }
    
    @Override
    public void createSucursalAdministrador(Entities.Franquicia franchise,String username){
          
        try{
              
            System.out.print("Images Size "+this.fileUpload.getPhotoList().size());
            
            if(this.fileUpload.getPhotoList()!=null && !this.fileUpload.getPhotoList().isEmpty()){
            
             Entities.Imagen image=this.fileUpload.getPhotoList().get(0);
                
             this.imageController.create(image);
             
             franchise.setImagenIdimagen(image);
             
             System.out.print("Image is not null, created");
            
            }
            
         
            
           if(!this.usernameExists(username)) {
           
           Entities.LoginAdministrador user=new Entities.LoginAdministrador();
           
           Entities.Confirmation confirmation=new Entities.Confirmation();
           
           
           user.setUsername(username);
           
           user.setPassword(DataGeneration.passwordGeneration(30));
           
           
           
           user.setProfessionIdprofession(this.professionController.find(1));
           
           confirmation.setConfirmed(false);
           
           confirmation.setLoginAdministrador(user);
           
           confirmation.setLoginAdministradorusername(username);
           
           user.setConfirmation(confirmation);
           
           
           this.loginAdministradorController.create(user);
           
           Entities.AppGroup appGroup=new Entities.AppGroup();
           
           Entities.AppGroupPK appGroupPK= new Entities.AppGroupPK();
           
           appGroupPK.setGroupid("guanabara_franquicia_administrador");
           
           appGroupPK.setLoginAdministradorusername(username);
           
           
           
           appGroup.setAppGroupPK(appGroupPK);
           
           appGroup.setLoginAdministrador(user);
           
           this.appGroupController.create(appGroup);
           
           SessionClasses.MailManagement mailManagement=new SessionClasses.MailManagement();
           
           
           javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
           
           String url=String.valueOf(request.getRequestURL());
           
           String uri=request.getRequestURI();
           
           String finalUri=url.replaceAll(uri, "");
           
           System.out.print("Requested Uri "+finalUri);
           
       
           
           mailManagement.sendMail(username, "Welcome to Eden", "Please follow this link to activate your account "+finalUri+java.io.File.separator+"SmartNet-web/confirmation/confirmation.xhtml?faces-redirect=true&id="+username+" your password is : "+user.getPassword()+" remember to change it");
        
           }
           
      
           
           Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
           
           appGroupPK.setGroupid("guanabara_franquicia_administrador");
           
           appGroupPK.setLoginAdministradorusername(username);
               
               
           if(this.appGroupController.find(appGroupPK)==null){
           
               Entities.AppGroup appGroup=new Entities.AppGroup();
               
               appGroup.setAppGroupPK(appGroupPK);

               appGroup.setLoginAdministrador(this.loginAdministradorController.find(username));
               
               this.appGroupController.create(appGroup);
           
           }
           
            
           
           franchise.setLoginAdministradorusername(this.loginAdministradorController.find(username));
           
           
           
           this.franchiseController.create(franchise);
           
           
          
            
    
    }catch(EJBException ex){
        
        Logger.getLogger(EdenWorkerController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new FacesException(ex);
    
    }
    
    
    }
    
    private boolean usernameExists(String username){
    
    
        if(this.loginAdministradorController.find(username)!=null){
        
        return true;
        
        }
        else{
        
        return false;
        
        }
    
    }
  
    
private java.util.List<Entities.Tipo>typeList;
   
private java.util.List<Entities.Subtype>subtypeList;
   
@Override
public java.util.List<Entities.Tipo>getTypeList(){

return this.typeList;

}

@Override
public void setTypeList(java.util.List<Entities.Tipo>typeList){

this.typeList=typeList;

}


@Override
public java.util.List<Entities.Subtype>getSubtypeList(){

    return this.subtypeList;

}

@Override
public void setSubtypeList(java.util.List<Entities.Subtype>subtypeList){

    this.subtypeList=subtypeList;

}

@Override
public void updateSubtypeList(int idType){

try{

    this.subtypeList=(java.util.List<Entities.Subtype>)this.franchisteType.find(idType).getSubtypeCollection();

}

catch(javax.ejb.EJBException ex){

java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

throw new javax.faces.FacesException(ex);


}

}



@Override
 public void initLists(){
 
     try{
         
         if(this.franchisteType.findAll()!= null && !this.franchisteType.findAll().isEmpty()){
         
             if(this.typeList==null){
             
                 this.typeList=this.franchisteType.findAll();
             
             }
             
             this.subtypeList=(java.util.List<Entities.Subtype>)this.typeList.get(0).getSubtypeCollection();
         
         }
     
     }
     catch(Exception ex){
    
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         throw new javax.faces.FacesException(ex);
         
     }
 
 }  
    
    
}
