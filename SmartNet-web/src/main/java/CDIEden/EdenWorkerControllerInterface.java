/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface EdenWorkerControllerInterface {
    boolean checkExistence(String Name);
    
    void createSucursalAdministrador(Entities.Franquicia franchise,String username);
    
    void initLists();
    
    java.util.List<Entities.Tipo>getTypeList();
    
    void setTypeList(java.util.List<Entities.Tipo>typeList);
    
    java.util.List<Entities.Subtype>getSubtypeList();
    
    void setSubtypeList(java.util.List<Entities.Subtype>subtypeList);
    
    void updateSubtypeList(int idType);
}
