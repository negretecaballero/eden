/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */



public interface EdenWorkerNote {
  
    java.util.List<Entities.NewsType>getNewsTypeList();
    
    java.util.List<Entities.Tipo> getFieldList();
    
    void createNote(Entities.News news);
    
}
