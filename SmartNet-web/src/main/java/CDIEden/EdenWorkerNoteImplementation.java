/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@EdenWorkerNoteQualifier

public class EdenWorkerNoteImplementation implements EdenWorkerNote{
    
    @javax.inject.Inject 
    @CDIBeans.NewsControllerQualifier
    private CDIBeans.NewsController newsController;
    
    @javax.inject.Inject
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoControllerImplementation tipoController;
    
    
    @javax.ejb.EJB
    private jaxrs.service.NewsTypeFacadeREST newsTypeFacadeREST;
            
    
    @Override
    public java.util.List<Entities.NewsType>getNewsTypeList(){
    

        java.util.List<Entities.NewsType>results=((java.util.List<Entities.NewsType>)this.newsController.getNewsTypeList());
        
        System.out.print("NewsTypeList size "+this.newsController.getNewsTypeList().size());
        
        return results;
    
    }
    
    @Override
    public java.util.List<Entities.Tipo> getFieldList()
    {
    
        return (java.util.ArrayList<Entities.Tipo>)IteratorUtils.toList(this.tipoController.getTipoList().iterator());
    
    }
    
    @Override
    public void createNote(Entities.News news){
    
        try{
        
            if(news.getTipoId()!=null){
            
                if(news.getNewsTypeId().getId()>0){
                
                    System.out.print("Tipo Id Tipo value "+news.getTipoId().getIdtipo());
                    
                    news.setTipoId(this.tipoController.find(news.getTipoId().getIdtipo()));
                
                }
                else{
                
                    news.setTipoId(null);
                
                }
            
            }
            
            
            news.setNewsTypeId(this.newsTypeFacadeREST.find(news.getNewsTypeId().getId()));
            
            this.newsController.create(news);
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex); 
        
        }
    
    }
    
}
