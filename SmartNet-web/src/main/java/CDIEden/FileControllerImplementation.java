/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import CDIBeans.FileControllerQualifier;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */



@javax.enterprise.context.SessionScoped

@FileControllerUpdatedQualifier

public class FileControllerImplementation implements FileController,java.io.Serializable{
    
    @javax.ejb.EJB
    private SessionBeans.FileFacadeLocal fileFacade;
    
    
    @Override
    public void createFolder(){
    
        if(this.fileFacade.getRoot()==null || !this.fileFacade.getRoot().equals("")){
    this.fileFacade.createFolder(FacesContext.getCurrentInstance());
        }
        
        else{
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("There is a file with this name");
        
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
        
        FacesContext.getCurrentInstance().addMessage(null,msg);
        
        }
        
    }
    
    @Override
    public String getFolderName(){
    
    try{
    
        return this.fileFacade.getRoot();
    
    }
    catch(RuntimeException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    
}
