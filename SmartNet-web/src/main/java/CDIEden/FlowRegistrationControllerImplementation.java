/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@javax.inject.Named

@FlowRegistrationControllerQualifier

public class FlowRegistrationControllerImplementation implements FlowRegistrationController, java.io.Serializable {
    
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
    
    
    
    @Override
    public boolean userExists(String username){
    
    try{
    
        
        if(this.loginAdministradorController.find(username)!=null){
        
            return true;
        
        }
        
        return false;
    
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
}
