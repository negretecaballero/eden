/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */

@javax.inject.Qualifier

@java.lang.annotation.Retention(java.lang.annotation.RetentionPolicy.RUNTIME)

@java.lang.annotation.Target({java.lang.annotation.ElementType.PARAMETER,java.lang.annotation.ElementType.TYPE,java.lang.annotation.ElementType.FIELD,java.lang.annotation.ElementType.METHOD})

public @interface FranchiseAdministratorCategoryControllerQualifier {
    
}
