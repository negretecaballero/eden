/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseAdministratorInventoryController {
    
    java.util.List<Entities.Inventario>getInventoryList();
    
    void deleteInventory(Entities.InventarioPK key);
    
    void initiateInventoryList();
    
    void updateInventory(Entities.Inventario inventory);
    
    boolean checkUpdateInventoryName(Entities.Franquicia franchise,String name,Entities.Inventario inventario);
    
}
