/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import CDIBeans.InventarioControllerQualifier;
import javax.faces.context.FacesContext;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.context.Dependent

@FranchiseAdministratorInventoryControllerQualifier

public class FranchiseAdministratorInventoryControllerImplementation implements FranchiseAdministratorInventoryController {
    
    @javax.inject.Inject @CDIBeans.InventarioControllerQualifier
    private CDIBeans.InventarioDelegate inventoryController;
    
    SessionClasses.EdenList <Entities.Inventario>inventoryList;
    
    
    @Override
    public void initiateInventoryList(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
        if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
        
              Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
          
            
            if(this.inventoryList==null || (this.inventoryList.size()!=franchise.getInventarioCollection().size())){
            
            this.inventoryList=null;
            
            if(inventoryList==null){
            
                this.inventoryList=new SessionClasses.EdenList<Entities.Inventario>();
            
            }
            
            
            
            
            for(Entities.Inventario inventory:franchise.getInventarioCollection()){
            
            inventoryList.addItem(inventory);
            
            }
            
            }
            
           
            
          
        
        }
        else{
        
        throw new IllegalArgumentException("Object of type "+session.getAttribute("username").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
        
    }
    catch(IllegalArgumentException | javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
    }
    
    }
    
    
    @Override
    public java.util.List<Entities.Inventario>getInventoryList(){
    
        if(this.inventoryList!=null){
        
        return IteratorUtils.toList(this.inventoryList.iterator());
        }
        return null;
    }
    
    @Override
    public void deleteInventory(Entities.InventarioPK key){
    
        try{
        
            this.inventoryList.remove(this.inventoryController.find(key));
            
            System.out.print("List Size after deletion "+this.inventoryList.size());
            
            
            this.inventoryController.delete(key);
            
       
        
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
         
        }
    
    
    }
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="22/08/2015",comments="this method check pr anothe rinventory with this name")
    public boolean checkUpdateInventoryName(Entities.Franquicia franchise,String name,Entities.Inventario inventario){
    
        try{
        
      
          
          for(Entities.Inventario inventory:franchise.getInventarioCollection()){
          
          System.out.print("Inventory name "+inventory.getNombre()+" InventoryPK "+inventory.getInventarioPK());    
          
          System.out.print("Name to evaluate "+name);
          
          if(inventory.getNombre().toLowerCase().equals(name.toLowerCase()) && !inventory.getInventarioPK().equals(inventario.getInventarioPK())){
          
          System.out.print("Inventory coincidence found");   
              
          return false;
          
          }
          
          }
          
         
          
          return true;
            
        }
        catch(StackOverflowError | javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    @Override
    public void updateInventory(Entities.Inventario inventory){
    
        try{
        
        this.inventoryController.update(inventory);
        
        }
        
        catch(StackOverflowError | javax.ejb.EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
