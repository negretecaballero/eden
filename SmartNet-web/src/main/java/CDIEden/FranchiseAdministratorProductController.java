/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseAdministratorProductController {
    
    
    java.util.List<Entities.Categoria>getCategoryList();
    
    java.util.List<Entities.Inventario>getInventoryList();
    
    Entities.Inventario findInventoryByConverter(String key);
    
    boolean checkComponeExistenceByInventory(Entities.Producto product,Entities.InventarioPK key);
    
    java.util.List<Entities.Producto>productList();
    
    void createBusinessProduct(Entities.Producto product);
    
    Entities.Producto getByConverter(String key);
    
    boolean checkNameCoincidence(String name,String key);
    
    java.util.List<Layouts.EdenLayout<Entities.Producto>>getProductsFranchiseList();
    
    void initFranchiseList();
    
    void loadImagesUpdateProduct(Entities.Producto product);
    
    boolean findSimilarCoincidence(java.util.List<Entities.SimilarProducts>similarList,Entities.Producto aux);
    
    void deleteSimilarProduct(Entities.Producto product,Entities.Producto aux);
    
    void updateProduct(Entities.Producto product);
    
}
