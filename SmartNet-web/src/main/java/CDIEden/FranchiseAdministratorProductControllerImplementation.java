/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import javax.faces.context.FacesContext;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.context.Dependent

@FranchiseAdministratorProductControllerQualifier

public class FranchiseAdministratorProductControllerImplementation implements FranchiseAdministratorProductController {
 
    @javax.inject.Inject
    @CDIBeans.InventarioControllerQualifier
    private CDIBeans.InventarioDelegate inventoryController;
    
    @javax.inject.Inject 
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface productController;
    
    @javax.inject.Inject 
    @CDIBeans.CategoryControllerQualifier
    private CDIBeans.CategoryControllerInterface categoryController;
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject
    @CDIBeans.BarcodeControllerQualifier
    private CDIBeans.BarcodeController _barcodeController;
    
    
    
    @Override
     public java.util.List<Entities.Categoria>getCategoryList(){
    
    try{
    
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
       if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
       
       Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
       
       SessionClasses.EdenList<Entities.Categoria>results=new SessionClasses.EdenList<Entities.Categoria>();
       
       for(Entities.Categoria category:franchise.getCategoriaCollection()){
       
       results.addItem(category);
       
       }
       
         return IteratorUtils.toList(results.iterator());
       
       }
       
       else{
       
       throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
       
       }
    
    }
    catch(javax.ejb.EJBException | IllegalArgumentException | NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
     
     
     @Override
     @annotations.MethodAnnotations(author="Luis Negrete",date="24/08/2015",comments="Get Inventory List by franchise")
    public java.util.List<Entities.Inventario>getInventoryList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            SessionClasses.EdenList<Entities.Inventario>results=new SessionClasses.EdenList<Entities.Inventario>();
            
            for(Entities.Inventario inventory:franchise.getInventarioCollection()){
            
            results.addItem(inventory);
                
            }
            
            
            return IteratorUtils.toList(results.iterator());
            
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
            
            }
          
        }
        catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex){
      
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
   @Override
    public Entities.Inventario findInventoryByConverter(String key){
    
    try{
    
        return this.inventoryController.findByInventoryConverter(key);
    
    }
    catch(Exception ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public boolean checkComponeExistenceByInventory(Entities.Producto product,Entities.InventarioPK key){
    
        for(Entities.Compone compone:product.getComponeCollection()){
        
        if(compone.getInventario().getInventarioPK().equals(key)){
        
        return true;
        
        }
        
        }
        
        return false;
       
    }
    
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="26/08/2015",comments="Products by franchise")
    public java.util.List<Entities.Producto>productList(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
        if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
            SessionClasses.EdenList<Entities.Producto>results=new SessionClasses.EdenList<Entities.Producto>();
            
            for(Entities.Categoria category:franchise.getCategoriaCollection()){
            
               for(Entities.Producto product:category.getProductoCollection()){
               
               results.addItem(product);
               
               }
            
            }
            
        return IteratorUtils.toList(results.iterator());
        }
        
        else{
        
        throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
        
    
    }
    catch(StackOverflowError | NullPointerException | IllegalArgumentException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    
    public void createBusinessProduct(Entities.Producto product){
    
    try{
        
        product.setCategoria(this.categoryController.find(product.getCategoria().getCategoriaPK()));
    
        System.out.print("Category NAme to Add "+product.getCategoria().getNombre());
        
        product.getProductoPK().setCategoriaFranquiciaIdfranquicia(product.getCategoria().getCategoriaPK().getFranquiciaIdfranquicia());
        
        product.getProductoPK().setCategoriaIdcategoria(product.getProductoPK().getCategoriaIdcategoria());
        
        if(product.getBarcode().getBarcode()!=null && !product.getBarcode().getBarcode().equals("")){
    
            if(_barcodeController.getBarcodeByCode(product.getBarcode().getBarcode())!=null){
            
            product.setBarcode(_barcodeController.getBarcodeByCode(product.getBarcode().getBarcode()));
            
            }
            else{
            
            Entities.Barcode barcode=product.getBarcode();
            
            _barcodeController.createBarcode(barcode);
            
            product.setBarcode(barcode);
            
            }
    
         }
        
        this.productController.createProduct(product);
        
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    
    }
    
    }
    
    @Override
    public Entities.Producto getByConverter(String key){
    
    try{
    
        return this.productController.getProductByConverter(key);
    
    }
    catch(Exception ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    
    }
    
    private Entities.CategoriaPK getCategoryPKByConverter(String key){
    
        try{
        
            if(key.split(",").length==2){
            
                Entities.CategoriaPK pk=new Entities.CategoriaPK();
                
                pk.setIdcategoria(new java.lang.Integer(key.split(",")[1]));
            
                pk.setFranquiciaIdfranquicia(new java.lang.Integer(key.split(",")[0]));
                
                return pk;
                
            }
            
            else{
            
                throw new IllegalArgumentException("Wrong data converter type");
            
            }
        
        }
        catch(IllegalArgumentException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="27/08/2015",comments="Check is the name of this products exists")
    public boolean checkNameCoincidence(String name,String key){
    
    try{
    
        System.out.print("Product to Evaluate "+name+" categoryPK "+this.getCategoryPKByConverter(key));
        
        if((this.productController.findByName(this.getCategoryPKByConverter(key), name))!=null){
        
            System.out.print("Object Found different from null");
            
            return true;
        
        }
        
        return false;
    
    }
    
    catch(Exception ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
        
    
    }
    
    
    }
    
    
    private java.util.List<Layouts.EdenLayout<Entities.Producto>>_productsFranchiseList;
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="28/08/2015",comments="Return list with products by franchise")
    public java.util.List<Layouts.EdenLayout<Entities.Producto>>getProductsFranchiseList(){
    
    return _productsFranchiseList;
    
    }
  
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="28/08/2015",comments="Initialize Products List")
    public void initFranchiseList(){
    
    try{
    
        _productsFranchiseList=new java.util.ArrayList<Layouts.EdenLayout<Entities.Producto>>();
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            for(Entities.Producto product:this.productController.findByFranchise(franchise.getIdfranquicia())){
            
            
                Layouts.EdenLayout<Entities.Producto> aux=new Layouts.EdenLayoutImplementation<Entities.Producto>();
            
                aux.setObject(product);
            
                if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                
                   Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                   
                    
                   javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                   
                   java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
                
                   if(file.exists()){
                   
                       this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
                   
                   }
                   else{
                   
                   filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, product.getNombre());
                   
                   }
                   
                 this.fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)product.getImagenCollection()).get(0), session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
                  
                 String image=this.fileUpload.getImages().get(this.fileUpload.getPhotoList().size()-1);
                 
                 aux.setImage(image);
                 
                 this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
                 
                 this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
                 
                }
                else{
                
                    aux.setImage("/images/noImage.png");
                
                }
                _productsFranchiseList.add(aux); 
            }
            
            
           
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
    
    }
    catch(IllegalArgumentException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void loadImagesUpdateProduct(Entities.Producto product){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        for(Entities.Imagen image:product.getImagenCollection()){
        
            if(!this.fileUpload.getPhotoList().contains(image)){
            
        this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString());

            }
        }
    
    }
    catch(Exception ex){
    
      java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
      
      throw new javax.faces.FacesException(ex);
    
    }
    
    }
  
    @Override
    public boolean findSimilarCoincidence(java.util.List<Entities.SimilarProducts>similarList,Entities.Producto aux){
    
        
        for(Entities.SimilarProducts item:similarList){
        
            if(item.getProducto1().equals(aux)){
            
                return true;
            
            }
        
        }
        
    return false;
    
    }
    
    @Override
  
    public void deleteSimilarProduct(Entities.Producto product,Entities.Producto aux){
    
        for(Entities.SimilarProducts item:product.getSimilarProductsCollection()){
        
        
        if(item.getProducto1().equals(aux)){
        
            product.getSimilarProductsCollection().remove(item);
            
            System.out.print("Product to Remove from List "+item.getProducto1().getNombre());
            
            break;
            
        }
        
        }
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="31/08/2015",comments="Update Product")
    public void updateProduct(Entities.Producto product){
    
    try{
   
        if(product.getBarcode().getBarcode()!=null && !product.getBarcode().getBarcode().equals("")){
        
        if(_barcodeController.getBarcodeByCode(product.getBarcode().getBarcode())!=null)
        {
        
          product.setBarcode(_barcodeController.getBarcodeByCode(product.getBarcode().getBarcode()));  
        
        }
        else{
        
            _barcodeController.createBarcode(product.getBarcode());
            
            product.setBarcode(_barcodeController.getBarcodeByCode(product.getBarcode().getBarcode()));
        
        }
        }
    
        else{
        
            product.setBarcode(null);
        
        }
        
        this.productController.updateProduct(product);
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
}
