/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseRegistrationController {
    
    void initStateList();
    
    java.util.List<Entities.State>getStateList();
    
    void setStateList(java.util.List<Entities.State>stateList);
    
    java.util.List<Entities.City>getCityList();
    
    void setCityList(java.util.List<Entities.City>cityList);
    
    void updateCities(int stateId);
    
    void saveChanges(Entities.Application application);
    
    boolean nitExists(String nit);
    
    SessionClasses.EdenList<Entities.Tipo> tipoList();
    
    SessionClasses.EdenList<Entities.Subtype>getSubtypeList(int idTipo);
    
}
