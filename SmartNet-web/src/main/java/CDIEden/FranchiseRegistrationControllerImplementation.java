/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@FranchiseRegistrationControllerQualifier

public class FranchiseRegistrationControllerImplementation implements FranchiseRegistrationController {
    
    @javax.inject.Inject
    @CDIBeans.StateControllerQualifier
    private CDIBeans.StateController stateController;
    
    @javax.inject.Inject
    @CDIBeans.CityControllerQualifier
    private CDIBeans.CityControllerInterface cityController;
    
    @javax.inject.Inject
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoControllerImplementation tipoController;
    
    @javax.inject.Inject
    @CDIBeans.SupplierTypeControllerQualifier
    private CDIBeans.SupplierTypeController _supplierTypeController;
    
    private java.util.List<Entities.State>stateList;
    
    private java.util.List<Entities.City>cityList;
    
    @javax.inject.Inject
    @CDIBeans.ApplicationControllerQualifier
    private CDIBeans.ApplicationController applicationController;
    
    @javax.inject.Inject
    @CDIBeans.SubtypeControllerQualifier
    private CDIBeans.SubtypeController subtypeController;
    
    @Override
    public void initStateList(){
    
      this.stateList=this.stateController.findAll();
    
      if(this.stateList!=null && !this.stateList.isEmpty()){
      
          this.cityList=(java.util.List<Entities.City>)this.stateList.get(0).getCityCollection();
      
      }else{
      
          this.cityList=null;
      
      }
      
    }
    
    
    @Override
    public java.util.List<Entities.State>getStateList(){
    
    return this.stateList;
    
    }
    
    @Override
    public void setStateList(java.util.List<Entities.State>stateList){
    
        this.stateList=stateList;
    
    }
    
    @Override
    public java.util.List<Entities.City>getCityList(){
    
        return this.cityList;
    
    }
    
    @Override
    public void setCityList(java.util.List<Entities.City>cityList){
    
        this.cityList=cityList;
    
    }
    @Override
    public void updateCities(int stateId){
    
        try{
        
           this.cityList=(java.util.List<Entities.City>)this.stateController.find(stateId).getCityCollection();
            
        }catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
        
    }
    
    @Override
    public void saveChanges(Entities.Application application){
    
      try{
      
        application.setCity(this.cityController.find(application.getCity().getCityPK()));  
          
        application.setIdSubtype(this.subtypeController.find(application.getIdSubtype().getSubtypePK()));
        
        
        System.out.print("TIPO "+application.getIdSubtype().getIdTipo().getNombre());
        
        
        System.out.print("Application Object City "+application.getCity().getName());
        
        System.out.print("Applicaton Object Subtype "+application.getIdSubtype().toString());
        
        
        this.applicationController.create(application);
      
      }
      
      catch(javax.ejb.EJBException ex){
      
          java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
      
          throw new javax.faces.FacesException(ex);
          
      }
    
    }
    
    @Override
    public boolean nitExists(String nit){
    
        try{
        
            if(this.applicationController.findByNit(nit)!=null){
            
                return true;
                
            }
            
            return false;
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.Tipo> tipoList(){
    
        return this.tipoController.getTipoList();
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.Subtype>getSubtypeList(int idTipo){
    
        try{
        
            SessionClasses.EdenList<Entities.Subtype>results=new SessionClasses.EdenList<Entities.Subtype>();
            
            for(Entities.Subtype subtype:this.tipoController.find(idTipo).getSubtypeCollection()){
            
                results.addItem(subtype);
            
            }
            
            return results;
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
