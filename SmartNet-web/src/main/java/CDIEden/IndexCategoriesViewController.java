/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@IndexCategoriesViewControllerQualifier

public class IndexCategoriesViewController extends Clases.BaseBacking implements IndexCategoriesViewControllerInterface{
   
  private  java.util.List<Clases.IndexCategoryPOJOInterface>indexCategoryList;
  
  @javax.inject.Inject
  @CDIBeans.SucursalControllerQualifier
  private CDIBeans.SucursalControllerDelegate sucursalController;
  
  @javax.inject.Inject 
  @CDIBeans.FileUploadBeanQualifier
  private CDIBeans.FileUploadInterface fileUpload;
  
  
  @Override
  public void init(Entities.Sucursal sucursal){
  
  try{
      
      if(this.indexCategoryList==null){
      
          this.indexCategoryList=new java.util.ArrayList<Clases.IndexCategoryPOJOInterface>();
      
      }
      
      this.sucursalController.loadSucursalCategoriesPictures(sucursal.getSucursalPK(), getSession().getAttribute("username").toString());
      
      this.indexCategoryList=this.sucursalController.getIndexList();
      
      System.out.print("Index Categry List size "+this.indexCategoryList.size());
  
  }
  catch(Exception ex){
  
      Logger.getLogger(IndexCategoriesViewController.class.getName()).log(Level.SEVERE,null,ex);
  
  }
  
  }
  
  
  @Override
  public java.util.List<Clases.IndexCategoryPOJOInterface>getIndexCategoryList(){

      return this.indexCategoryList;

}
  
  @Override
  public void setIndexCategoryList(java.util.List<Clases.IndexCategoryPOJOInterface>indexCategoryList){
  
  this.indexCategoryList=indexCategoryList;
  
  }
  
  
}
