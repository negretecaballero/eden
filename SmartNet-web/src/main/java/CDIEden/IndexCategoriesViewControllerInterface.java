/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;


/**
 *
 * @author luisnegrete
 */

public interface IndexCategoriesViewControllerInterface {

void init(Entities.Sucursal sucursal);

void setIndexCategoryList(java.util.List<Clases.IndexCategoryPOJOInterface>indexCategoryList);
    
java.util.List<Clases.IndexCategoryPOJOInterface>getIndexCategoryList();
}
