/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@InvoiceControllerQualifier

public class InvoiceController implements InvoiceControllerInterface{
  
    @javax.inject.Inject @javax.enterprise.inject.Default private Controllers.InvoiceControllerInterface invoiceController;
    
    @Override
    public java.util.List<SessionClasses.Invoice>getInvoices(){
          
    return this.invoiceController.getInvoices();
    
    }
    
    @Override
    public String getFlag(){
    
        return this.invoiceController.getFlag();
    
    }
}
