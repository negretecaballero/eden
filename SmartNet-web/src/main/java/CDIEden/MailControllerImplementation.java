/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.SessionScoped
@MailControllerQualifier
public class MailControllerImplementation implements MailController,java.io.Serializable{
 
 @javax.ejb.EJB
 private SessionBeans.MailFacadeLocal _mailFacade;
    
    @Override
   public void sendMail(String destinatary,String subject,String messageText){
   
       _mailFacade.sendMessage(destinatary, subject, messageText);
   
   }

   
}
