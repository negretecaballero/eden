/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
@OrdersControllerQualifier
public class OrdersController implements OrdersControllerInterface {

    @javax.inject.Inject
    @CDIBeans.PedidoControllerQualifier
    private CDIBeans.PedidoControllerInterface pedidoController;
    
      @Override
      public java.util.Date getFromDate(){
    
    try{
    
        javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        String from=request.getParameter("from");
        
        SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
        
        java.util.Date date = formatter.parse(from);
        
        return date;
    }
    catch(Exception ex){
    
        Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public java.util.Date getToDate(){
    
        try{
        
         javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
         String to=request.getParameter("to");
         
         SimpleDateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
        
        java.util.Date date = formatter.parse(to);
       
         
         return date;
         
        }
        catch(Exception ex){
        
            Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public java.util.List<Entities.Pedido>getOrders(java.util.Date from,java.util.Date to,Entities.SucursalPK sucursalPK){
    
    try{
    
       return this.pedidoController.findByInterval(from, to,sucursalPK);
    
    }
    catch(Exception ex){
    
    Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    
    }
}
