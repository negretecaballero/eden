/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */


public interface OrdersControllerInterface {
    
 java.util.Date getFromDate();
    
 java.util.Date getToDate();
 
 java.util.List<Entities.Pedido>getOrders(java.util.Date from, java.util.Date to,Entities.SucursalPK sucursalPK);
 
}
