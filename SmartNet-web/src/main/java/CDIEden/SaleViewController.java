/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */



public interface SaleViewController {
    
  java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>sales();
   
  void delete(Entities.SalePK salePK);
  
  void updateSaleImages();
  
  void createBusinessSale(Entities.Sale sale);
  
  java.util.List<Layouts.EdenLayout<Entities.Sale>>getBusinessSaleList();
  
   void updateBusinessSale(Entities.Sale sale);
}
