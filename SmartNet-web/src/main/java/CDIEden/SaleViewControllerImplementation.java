/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent

@SaleViewControllerQualifier

public class SaleViewControllerImplementation extends Clases.BaseBacking implements SaleViewController{
 
    @javax.inject.Inject
    @CDIBeans.SaleControllerQualifier
    private CDIBeans.SaleControllerInterface saleController;
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject
    @CDIBeans.ImageControllerQualifier
    private CDIBeans.ImageControllerDelegate imageController;
    
    @javax.inject.Inject
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface productController;
            
    @javax.ejb.EJB
    private jaxrs.service.SaleHasProductoFacadeREST saleHasProductoFacadeREST;
    
    @Override
    public java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>sales(){
    
    try{
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
        
        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
        
        java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>sales=new java.util.ArrayList<FranchiseAdministrator.Sale.Classes.SaleViewLayout>();
    
        if(this.getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
            
          for(Entities.Sale aux:this.saleController.findByFranchise(franchise.getIdfranquicia())){
          
           FranchiseAdministrator.Sale.Classes.SaleViewLayout layout=new FranchiseAdministrator.Sale.Classes.SaleViewLayoutImplementation();
          
           layout.setSale(aux);
           
           java.util.List<String>imageList=new java.util.ArrayList<String>();
           
           if(aux.getImagenCollection()!=null && !aux.getImagenCollection().isEmpty())
           {
           for(Entities.Imagen image:aux.getImagenCollection()){
           
               this.fileUpload.loadImagesServletContext(image, this.getSession().getAttribute("username").toString());
           
               String auxi=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
               
               imageList.add(auxi);
               
               int removeItem=this.fileUpload.getImages().size()-1;
               
               this.fileUpload.getImages().remove(removeItem);
               
               this.fileUpload.getPhotoList().remove(removeItem);
               
              
           }
           }
           
           else{
           
               imageList.add("/images/noImage.png");
           
           }
           
           layout.setImages(imageList);
           
           sales.add(layout);
          }
            return sales;
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+this.getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sale.class.getName());
        
        }
        
    }
    catch(IllegalArgumentException | EJBException |NullPointerException ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void delete(Entities.SalePK salePK){
    
    try{
    
        this.saleController.delete(salePK);
    
    }
    catch(javax.ejb.EJBException ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void updateSaleImages(){
    
    try{
    
     if(this.getSessionAuxiliarComponent() instanceof Entities.Sale){
     
       Entities.Sale sale=(Entities.Sale)this.getSessionAuxiliarComponent();
       if(sale.getImagenCollection()!=null && !sale.getImagenCollection().isEmpty())
       {
       for(Entities.Imagen image:sale.getImagenCollection()){
       
       this.fileUpload.loadImagesServletContext(image, this.getSession().getAttribute("username").toString());
       
       }
     }
       else{
       
           this.fileUpload.getImages().add("/images/noImage.png");
       
       }
         
     }
     else{
     
         throw new RuntimeException("Object of type "+this.getSessionAuxiliarComponent().getClass().getName()+" must be of type "+Entities.Sale.class.getName());
         
     }
    
    }
    catch(IllegalArgumentException | NullPointerException ex){
    
        Logger.getLogger(SaleViewControllerImplementation.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void createBusinessSale(Entities.Sale sale){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia)
            
            {
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                sale.setFranquicia(franchise);
                
                if(sale.getSalePK()==null){
                
                sale.setSalePK(new Entities.SalePK());
                
                }
                
                sale.getSalePK().setFranquiciaIdfranquicia(franchise.getIdfranquicia());
            
            //Create Images
            
            for(Entities.Imagen image:this.fileUpload.getPhotoList()){
            
               this.imageController.create(image);
            
            }
            
            //Create EdenSaleList
            
            SessionClasses.EdenList<Entities.SaleHasProducto>saleProductList=new SessionClasses.EdenList<Entities.SaleHasProducto>();
            
            for(Entities.SaleHasProducto aux:sale.getSaleHasProductoCollection()){
            
            aux.setProducto(this.productController.find(aux.getProducto().getProductoPK()));
            
            aux.getSaleHasProductoPK().setProductoIdproducto(aux.getProducto().getProductoPK().getIdproducto());
            
            aux.getSaleHasProductoPK().setProductoCategoriaIdCategoria(aux.getProducto().getProductoPK().getCategoriaIdcategoria());
            
            aux.getSaleHasProductoPK().setProductoCategoriaFranquiciaIdFranquicia(aux.getProducto().getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            aux.getSaleHasProductoPK().setSaleFranquiciaIdfranquicia(sale.getSalePK().getFranquiciaIdfranquicia());
            
            aux.setSale(sale);
            
            saleProductList.addItem(aux);
            
            }
            
            sale.setSaleHasMenuCollection(null);
            
            sale.setSaleHasProductoCollection(null);
            
            
            //Create Sale
            
            sale.setImagenCollection(this.fileUpload.getPhotoList());
            
            this.saleController.create(sale);
            
            
            //Create SaleHasProducto
            
            sale=this.saleController.find(sale.getSalePK());
            
            for(Entities.SaleHasProducto saleHasProducto:saleProductList){
            
            saleHasProducto.setSale(sale);
            
            saleHasProducto.getSaleHasProductoPK().setSaleIdsale(sale.getSalePK().getIdsale());
                
            this.saleHasProductoFacadeREST.create(saleHasProducto);
            
            }
        }
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="3/09/2015",comments="Get Sales by franchise")
            
    public java.util.List<Layouts.EdenLayout<Entities.Sale>>getBusinessSaleList(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            SessionClasses.EdenList<Layouts.EdenLayout<Entities.Sale>>results=new SessionClasses.EdenList<Layouts.EdenLayout<Entities.Sale>>();
            
            for(Entities.Sale sale:franchise.getSaleCollection()){
            
                Layouts.EdenLayout<Entities.Sale>layout=new Layouts.EdenLayoutImplementation<Entities.Sale>();
                
                layout.setObject(sale);
                
                if(sale.getImagenCollection()!=null && !sale.getImagenCollection().isEmpty()){
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
   
                    java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                    
                    Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                    
                    if(file.exists()){
                    
                        filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                    
                    }
                    else{
                    
                        filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username"+java.io.File.separator),sale.getName());
                    
                    }
                    
                   this.fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)sale.getImagenCollection()).get(0), session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                    
                   String image=this.fileUpload.getImages().get(0);
                   
                   layout.setImage(image);
                   
                   this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
                    
                   this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
                   
                }
                else{
                
                    layout.setImage("/images/noImage.png");
                
                }
                
                results.addItem(layout);
            }
        
            
            System.out.print("Sale List Size "+results.size());
            
            
            return IteratorUtils.toList(results.iterator());
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
    
    }
    catch(NullPointerException | StackOverflowError | IllegalArgumentException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);    
    }
    
    }
    
    
    public void deteleSale(Entities.SalePK sale){
    
    try{
    
        this.saleController.delete(sale);
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    @Override
    public void updateBusinessSale(Entities.Sale sale){
    
        try{
        //Add All Values to SaleHasProducto
            for(Entities.SaleHasProducto aux:sale.getSaleHasProductoCollection()){
            
             System.out.print("SaleHasProducto ProductoPK "+aux.getProducto().getProductoPK());   
                
            aux.setProducto(this.productController.find(aux.getProducto().getProductoPK()));
            
            aux.getSaleHasProductoPK().setProductoCategoriaFranquiciaIdFranquicia(aux.getProducto().getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            aux.getSaleHasProductoPK().setProductoCategoriaIdCategoria(aux.getProducto().getProductoPK().getCategoriaIdcategoria());
            
            aux.getSaleHasProductoPK().setProductoIdproducto(aux.getProducto().getProductoPK().getIdproducto());
            
            aux.getSaleHasProductoPK().setSaleIdsale(sale.getSalePK().getIdsale());
            
            aux.getSaleHasProductoPK().setSaleFranquiciaIdfranquicia(sale.getSalePK().getFranquiciaIdfranquicia());
            
            }
            
            System.out.print("SALEPK "+sale.getSalePK());
            
            this.saleController.update(sale);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
