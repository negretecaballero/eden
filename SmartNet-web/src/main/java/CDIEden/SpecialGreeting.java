/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import Interceptors.LoggingInterceptor;
import javax.interceptor.Interceptors;

/**
 *
 * @author luisnegrete
 */




@Special
@javax.enterprise.context.Dependent
@Interceptors(LoggingInterceptor.class)
public class SpecialGreeting implements Greeting{
    
  
    public String getSpecialGreeting(String name){
    
        System.out.print("SPECIAL GREETING NAME "+name);
        
        java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
        
    switch(Clases.EdenDate.greeting()){
    
        case MORNING:{
        
            return name+" "+bundle.getString("Eden.Greeting.Morning");
        
        }
        
        case AFTERNOON:{
        
            return name+" "+bundle.getString("Eden.Greeting.Afternoon");
             
        }
        
        case EVENING:{
        
            return name+" "+bundle.getString("Eden.Greeting.Evening");
        
        }
        
        case NIGTH:{
        
            return name+" "+bundle.getString("Eden.Greeting.Nigth");
        
        }
        
    
    }
    
    return null;
    
    }
    
}
