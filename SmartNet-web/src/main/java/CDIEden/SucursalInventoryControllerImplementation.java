/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SucursalInventoryControllerQualifier

public class SucursalInventoryControllerImplementation implements SucursalInventoryController{
    
    
    @javax.inject.Inject 
    @CDIBeans.InventarioHasSucursalControllerQualifier
    private CDIBeans.InventarioHasSucursal inventarioHasSucursalController;
    
    @javax.inject.Inject 
    @CDIBeans.InventarioControllerQualifier
    private CDIBeans.InventarioDelegate inventarioController;
    
    @javax.inject.Inject
    @CDIBeans.SucursalControllerQualifier
    private CDIBeans.SucursalControllerDelegate sucursalController;
    
    
    @Override
   public java.util.List<Entities.InventarioHasSucursal>getInventoryList(Entities.SucursalPK sucursalKey){
   
   try{
   java.util.List<Entities.InventarioHasSucursal>resultList=new java.util.ArrayList<Entities.InventarioHasSucursal>();
       
   
   for(Entities.Inventario inventory:this.inventarioController.findByFranchise(sucursalKey.getFranquiciaIdfranquicia()))
       {
       
           boolean found=false;
           
           for(Entities.InventarioHasSucursal inventarioHasSucursal:this.inventarioHasSucursalController.findBySucursal(sucursalKey)){
           
           if(inventory.getInventarioPK().equals(inventarioHasSucursal.getInventario().getInventarioPK())){
           
               found=true;
           
           }
           
           }
           
           if(!found){
           
               Entities.InventarioHasSucursal aux=new Entities.InventarioHasSucursal();
               
               Entities.InventarioHasSucursalPK auxPK=new Entities.InventarioHasSucursalPK();
               
               auxPK.setInventarioFranquiciaIdfranquicia(inventory.getInventarioPK().getFranquiciaIdFranquicia());
               
               auxPK.setInventarioIdinventario(inventory.getInventarioPK().getIdinventario());
               
               auxPK.setSucursalFranquiciaIdfranquicia(sucursalKey.getFranquiciaIdfranquicia());
               
               auxPK.setSucursalIdsucursal(sucursalKey.getIdsucursal());
               
               aux.setInventarioHasSucursalPK(auxPK);
               
               aux.setSucursal(this.sucursalController.find(sucursalKey));
               
               aux.setQuantity(0.0);
               
               aux.setInventario(inventory);
                              
               System.out.print("Row Key "+auxPK.getInventarioFranquiciaIdfranquicia()+","+auxPK.getInventarioIdinventario()+","+auxPK.getSucursalFranquiciaIdfranquicia()+","+auxPK.getSucursalIdsucursal());
               
             resultList.add(aux);
               
           
           }
       
       }
       
       
       return resultList;
   }
   
   catch(javax.ejb.EJBException ex){
   
   Logger.getLogger(SucursalInventoryControllerImplementation.class.getName()).log(Level.SEVERE,null,ex);
   
   throw new javax.faces.FacesException(ex);
   
   }
   
   }
    
}
