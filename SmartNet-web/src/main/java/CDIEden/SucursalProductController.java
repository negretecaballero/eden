/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout;
import java.util.*;

/**
 *
 * @author luisnegrete
 */


public interface SucursalProductController {
   
      java.util.List<SucursalHasProductoFrontEndLayout>getAttribute();
    
      boolean contains(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>list,BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout item);
  
      void saveChanges(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>list);
      
      java.util.List<SucursalHasProductoFrontEndLayout>getAttribute2();
      
      void delete(Entities.ProductoPK key);

	Collection<SucursalHasProductoFrontEndLayout> getProductsList();

	Collection<SucursalHasProductoFrontEndLayout> getSucursalList();
      
}
