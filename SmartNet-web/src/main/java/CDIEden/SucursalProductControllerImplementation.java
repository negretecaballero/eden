/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@SucursalProductControllerQualifier

public class SucursalProductControllerImplementation implements SucursalProductController {
    
    @javax.inject.Inject 
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface productController;
    
    @javax.inject.Inject 
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject 
    @CDIBeans.SucursalControllerQualifier 
    private CDIBeans.SucursalControllerDelegate sucursalController;
    
    
    @Override
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>getProductsList(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption") instanceof Entities.Sucursal){
        
            Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
            
           java.util.List<Entities.Producto>products=this.productController.findByFranchise(sucursal.getFranquicia().getIdfranquicia());
        
           for(Entities.Producto product:sucursal.getProductoCollection()){
           
               if(products.contains(product)){
               
                   products.remove(product);
               
               }
           
           }
           
           java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>results=new java.util.ArrayList<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>();
           
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           for(Entities.Producto product:products){
           
               BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux=new BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayoutImplementation();
               
               aux.setProduct(product);
               
               Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
               
              String imagen="";
               if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty())
               {
               for(Entities.Imagen image:product.getImagenCollection()){
               
                  String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre();
               
                  java.io.File file=new java.io.File(path);
                  
                  if(!file.exists()){
                  
                  filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username")+java.io.File.separator, product.getNombre());
                  
                  }
                  
                  else{
                  
                      filesManagement.cleanFolder(path);
                  
                  }
                  
                  this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
               
               String auxi=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
               
             imagen=auxi;
               
               this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
               
               this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
               break;
               }
           }
               else{
               
                   imagen="/images/noImage.png";
               
               }
               
               aux.setImage(imagen);
               
               results.add(aux);
           
           }
           
           return results;
           
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Addition.class.getName());
        
        }
    
    }
    catch(NullPointerException | IllegalArgumentException ex){
    
        Logger.getLogger(SucursalProductControllerImplementation.class.getName()).log(Level.SEVERE,null,ex);
        
     return null;
    
    }
    
    }
    
    
    @Override
    public boolean contains(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>list,BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout item){
    
    
    for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:list){
    
        if(aux.getProduct().getProductoPK().equals(item.getProduct().getProductoPK())){
        
            return true;
        
        }
    
    }
    return false;
    
    }
    
    
    /**
	 * 
	 * @param list
	 */
	@javax.annotation.security.RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})
	@Override
    public void saveChanges(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>list){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
                
                java.util.List<Entities.Producto>products=new java.util.ArrayList<Entities.Producto>();
                
                for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout product:list){
                
                  products.add(product.getProduct());  
                
                }
                
                sucursal.setProductoCollection(products);
                
                this.sucursalController.updateSucursalHasProduct(sucursal);
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Products Added Successfully");
                
                FacesContext.getCurrentInstance().addMessage(null, msg);
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        
        catch(javax.ejb.EJBException | NullPointerException | IllegalArgumentException ex){
        
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
    @Override
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>getSucursalList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
              Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
              
              java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>resultList=new java.util.ArrayList<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>();
              
              for(Entities.Producto product:sucursal.getProductoCollection()){
              
                  BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux=new BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayoutImplementation();
                  
                  aux.setProduct(product);
                  
                  javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                  
                  Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                  
                  java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
              
                  if(file.exists()){
                  
                    filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
                  
                  }
                  else{
                  
                      filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, product.getNombre());
                  
                  }
                  
                  if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                  
                      for(Entities.Imagen image:product.getImagenCollection()){
                       
                          this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
                     
                          String imagePath=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
                          
                          aux.setImage(imagePath);
                          
                          this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
                          
                          this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
                          
                          break;
                      }
                  
                  }
                  else{
                  
                      aux.setImage("/images/noImage.png");
                  
                  }
                  
                  resultList.add(aux);
              }
              
              return resultList;
                
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        catch(Exception ex){
        
            Logger.getLogger(SucursalProductControllerImplementation.class.getName()).log(Level.SEVERE,null,ex);
        
            throw new javax.faces.FacesException(ex);
        }
    
    }
    
    @Override
    public void delete(Entities.ProductoPK key){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Sucursal){
            
                Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
                
              java.util.List<Entities.Producto>products=(java.util.List<Entities.Producto>)sucursal.getProductoCollection();
                
              for(Entities.Producto aux:products){
              
                  if(aux.getProductoPK().equals(key)){
                  
                      products.remove(aux);
                      
                      break;
                  
                  }
              
              }
              
              sucursal.setProductoCollection(products);
              
              this.sucursalController.updateSucursalHasProduct(sucursal);
              
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
                
            }
            
        }
        catch(javax.ejb.EJBException ex){
        
            Logger.getLogger(SucursalProductControllerImplementation.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }

    @Override
    public List<SucursalHasProductoFrontEndLayout> getAttribute() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<SucursalHasProductoFrontEndLayout> getAttribute2() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

 
}
