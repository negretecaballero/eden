/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import ENUM.Day;
import ENUM.OrderState;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@UserPedidoControllerQualifier

public class UserPedidoControllerImplementation implements UserPedidoController {
    
    private ENUM.OrderState orderStateENUM;
   
    @javax.inject.Inject 
    @CDIBeans.PedidoControllerQualifier
    private CDIBeans.PedidoControllerInterface _pedidoController;
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministrador;
    
    @Override
    public SessionClasses.EdenList<Clases.PedidoWorkerInterface>findOrdersByUser(String username){
        {
        
           try{
           
               SessionClasses.EdenList<Clases.PedidoWorkerInterface>results=new SessionClasses.EdenList<Clases.PedidoWorkerInterface>();
               
               Entities.LoginAdministrador loginAdministrador=_loginAdministrador.find(username);
               
               if(_loginAdministrador!=null){
               
                   for(Entities.Pedido aux:loginAdministrador.getPedidoCollection()){
                   
                     Clases.PedidoWorkerInterface item=new Clases.PedidoWorker();
                     
                     item.setPedido(aux);
                     
                     item.setItemList(new java.util.ArrayList<SessionClasses.CartItem>());
                     
                    
                     
                     this.setOrderState(aux.getPedidoStatestateId());
                     
                     item.setImage(this.stateToImage());
                     
                     //Check Products in order
                     if(aux.getProductohasPEDIDOCollection()!=null && !aux.getProductohasPEDIDOCollection().isEmpty()){
                     
                       for(Entities.ProductohasPEDIDO orderHasProduct:aux.getProductohasPEDIDOCollection()){
                       
                           SessionClasses.CartItem cartItem=new SessionClasses.CartItem();
                           
                           cartItem.setName(orderHasProduct.getProducto().getNombre());
                           
                           cartItem.setItem(orderHasProduct.getProducto());
                           
                           cartItem.setQuantity(orderHasProduct.getQuantity());
                           
                           item.getItemList().add(cartItem);
                       
                       } 
                         
                     }
                     
                     //Check Menu in order
                     if(aux.getMenuhasPEDIDOCollection()!=null && !aux.getMenuhasPEDIDOCollection().isEmpty()){
                     
                         for(Entities.MenuhasPEDIDO orderHasMenu:aux.getMenuhasPEDIDOCollection()){
                         
                         SessionClasses.CartItem cartItem=new SessionClasses.CartItem();
                         
                         cartItem.setItem(orderHasMenu.getMenu());
                         
                         cartItem.setName(orderHasMenu.getMenu().getNombre());
                         
                         cartItem.setQuantity(orderHasMenu.getQuantity());
                         
                         item.getItemList().add(cartItem);
                         
                         }
                     
                     }
                   
                     results.addItem(item);
                   }
                  
                   
                   return results;
               }
               else{
               
                   throw new Exception();
               
               }
               
           }
           catch(Exception ex){
           
               java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
               
               return null;
               
           }
        
        }
    
    }
    
    private String stateToImage(){
    
        try{
        
            
            
            switch(orderStateENUM){
            
                case Pending :{
                
                    System.out.print("Your order is pending");
                    
                    return "/images/pending.jpg";
     
                }
                
                case Processing:{
                
                    System.out.print("Order is processing");
                    
                   return "/images/processing.png";
                
                }
                
                case Delivered:{
                
                    System.out.print("Order is Delievred");
                    
                    return"/images/checked.png";
                
                }
            
            }
        return "";
        }
        
        catch(NullPointerException ex){
        
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           return "";
            
        }
    
    }
    
    
    private void setOrderState(Entities.PedidoState orderState){
    
        switch(orderState.getState()){
        
            case("Pending"):{
            
                orderStateENUM=ENUM.OrderState.Pending;
                
                break;
            
            }
            
            case("Processing"):{
            
                orderStateENUM=ENUM.OrderState.Processing;
            
             
            }
            
            case("Delivered"):{
            
                orderStateENUM=ENUM.OrderState.Delivered;
            
            }
        
        }
    
    }
}
