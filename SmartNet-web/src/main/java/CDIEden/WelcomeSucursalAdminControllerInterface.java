/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface WelcomeSucursalAdminControllerInterface {
   
    public double AveragePerSucursal(Entities.SucursalPK sucursalPK);
    
    public boolean feasability(Entities.SucursalPK sucursalPK);
}
