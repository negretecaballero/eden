/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

/**
 *
 * @author luisnegrete
 */
public interface franchiseAdministratorCategoryController {
    
    SessionClasses.EdenList<FranchiseAdministrator.Category.Classes.CategoryLayout>getCategories();
    
    void delete(Entities.CategoriaPK key);
    
    void updateBusinessCategory(Entities.Categoria category);
}
