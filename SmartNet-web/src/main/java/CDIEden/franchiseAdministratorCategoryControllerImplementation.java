/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CDIEden;

import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent

@FranchiseAdministratorCategoryControllerQualifier

public class franchiseAdministratorCategoryControllerImplementation implements franchiseAdministratorCategoryController {
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject
    @CDIBeans.ProductoControllerQualifier
    private CDIBeans.ProductoControllerInterface productController;
    
    @javax.inject.Inject
    @CDIBeans.CategoryControllerQualifier
    private CDIBeans.CategoryControllerInterface categoryController;
    
    @javax.inject.Inject
    @CDIBeans.ImageControllerQualifier
    private CDIBeans.ImageControllerDelegate imageController;
    
    @javax.inject.Inject 
    @CDIBeans.FranchiseControllerQualifier
    private CDIBeans.FranchiseControllerInterface franchiseController;
    
    @Override
    public SessionClasses.EdenList<FranchiseAdministrator.Category.Classes.CategoryLayout>getCategories(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
            SessionClasses.EdenList<Entities.Categoria>result=new SessionClasses.EdenList<Entities.Categoria>();
            
          Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
          
          franchise=this.franchiseController.find(franchise.getIdfranquicia());
          
        SessionClasses.EdenList<FranchiseAdministrator.Category.Classes.CategoryLayout> results=new SessionClasses.EdenList<FranchiseAdministrator.Category.Classes.CategoryLayout>();
          
         
        for(Entities.Categoria aux:franchise.getCategoriaCollection()){
        
        FranchiseAdministrator.Category.Classes.CategoryLayout layout=new FranchiseAdministrator.Category.Classes.CategoryLayoutImplementation();
        
        layout.setCategory(aux);
        
      
        
        if(aux.getImagenCollection()!=null && !aux.getImagenCollection().isEmpty()){
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
         
        java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+aux.getNombre());
        
        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
        
        if(file.exists()){
        
            filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+aux.getNombre());
            
        }
        else{
        
            filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, aux.getNombre());
        
        }
        
        for(Entities.Imagen image:aux.getImagenCollection()){
        
        this.fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+aux.getNombre());
        
        String photo=this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1);
        
        layout.setImage(photo);
        
        this.fileUpload.getImages().remove(this.fileUpload.getImages().size()-1);
        
        this.fileUpload.getPhotoList().remove(this.fileUpload.getPhotoList().size()-1);
        
        break;
        
        }
        
        }
        else{
        
            layout.setImage("/images/noImage.png");
        
        }
        
        results.addItem(layout);
        
        }
        
     return results;
        
        }
        
        
        else{
        
            throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
        
    }
    catch(javax.ejb.EJBException | IllegalArgumentException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
        
    }
    
    
    }
    
    @Override
    @annotations.MethodAnnotations(author="Luis Negrete",date="18/08/2015",comments="Delete a Category from a franchise")
    public void delete(Entities.CategoriaPK key){
    
        try{
        
            Entities.Categoria category=this.categoryController.find(key);
        
            
            for(Entities.Imagen image:category.getImagenCollection()){
            
                this.imageController.delete(image.getIdimagen());
            
            }
            
            for(Entities.Producto product:category.getProductoCollection()){
            
                this.productController.deleteFromFranchise(product.getProductoPK());
            
            }
            
            
            this.categoryController.delete(key);
            
                 
        }
        catch(javax.ejb.EJBException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
        finally{
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Category deleted succesfully");

            FacesContext.getCurrentInstance().addMessage(null,msg);
            
        }
    
    }
    
    @Override
    public void updateBusinessCategory(Entities.Categoria category){
    
        try{
        
            
            
            
          for(Entities.Imagen image:category.getImagenCollection()){
            boolean exists=false;
          
          
              for(Entities.Imagen photo:this.fileUpload.getPhotoList()){
              
                  if(image.equals(photo)){
                  
                      exists=true;
                  
                  }
              
              }
          
              if(!exists){
              
                  this.imageController.delete(image.getIdimagen());
              
              }
              
          }
          
          
          for(Entities.Imagen image:this.fileUpload.getPhotoList()){
          
            boolean exists=false;
          
            for(Entities.Imagen photo:category.getImagenCollection()){
            
                if(image.equals(photo)){
                
                    exists=true;
                
                }
            
            }
            
            if(!exists){
            
                this.imageController.create(image);
            
            }
          
          }
        
          category.setImagenCollection(this.fileUpload.getPhotoList());
          
          this.categoryController.update(category);
          
        }
        catch(javax.ejb.EJBException | NullPointerException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        
        throw new javax.faces.FacesException(ex);
        
        
        }
    
    }
    
}
