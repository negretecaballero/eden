package CategoryProfile.Classes;

import Clases.ViewLayout.*;

public interface CategoryProfileLayout {

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	java.util.Collection<Entities.Categoria> getCategories();

	/**
	 * 
	 * @param categories
	 */
	void setCategories(java.util.Collection<Entities.Categoria> categories);

	java.util.Collection<Layout> getViewLayout();

	/**
	 * 
	 * @param viewLayout
	 */
	void setViewLayout(java.util.Collection<Layout> viewLayout);
}