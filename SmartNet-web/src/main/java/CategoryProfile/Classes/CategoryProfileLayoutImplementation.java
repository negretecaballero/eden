package CategoryProfile.Classes;

import java.util.*;
import Clases.ViewLayout.*;

public class CategoryProfileLayoutImplementation implements CategoryProfileLayout {

	private String image;
	private Collection<Entities.Categoria> categories = null;
	private Collection<Layout> viewLayout = null;

	@Override
	public String getImage() {
		return this.image;
	}

	/**
	 * 
	 * @param image
	 */
	@Override
	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public java.util.Collection<Entities.Categoria> getCategories() {
		return this.categories;
	}

	/**
	 * 
	 * @param categories
	 */
	@Override
	public void setCategories(java.util.Collection<Entities.Categoria> categories) {
		this.categories = categories;
	}

	@Override
	public java.util.Collection<Layout> getViewLayout() {
		
            return this.viewLayout;
            
	}

	/**
	 * 
	 * @param viewLayout
	 */
	@Override
	public void setViewLayout(java.util.Collection<Layout> viewLayout) {
		
            this.viewLayout = viewLayout;
            
	}

	
}