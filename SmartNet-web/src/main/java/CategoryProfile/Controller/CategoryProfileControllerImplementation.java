package CategoryProfile.Controller;

import CDIBeans.*;

@javax.enterprise.inject.Alternative
public class CategoryProfileControllerImplementation implements CategoryProfileController {

	@Override
	public void init() {
	
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                CategoryProfile.View.CategoryProfileView categoryProfileView = (CategoryProfile.View.CategoryProfileView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "categoryProfileView");
                
                
                //Has not been initialized
                if(!categoryProfileView.getInit()){
                
                    System.out.print("Initializing Category Profile View");
                    
                    if(categoryProfileView.getCategoryProfileLayout() == null){
                    
                        categoryProfileView.setCategoryProfileLayout(new CategoryProfile.Classes.CategoryProfileLayoutImplementation());
                    
                    }
                    
                    CategoryProfile.Classes.CategoryProfileLayout categoryProfileLayout = new CategoryProfile.Classes.CategoryProfileLayoutImplementation();
                    
                    categoryProfileView.setCategoryPK(this.getKey());
                    
                    //Clear images
                    
                    this.fileController.Remove();
                    
                    //Set Image
                    
                    if(categoryProfileView.getCategoryPK().getFranquiciaIdfranquicia() > 0){
                    
                        Entities.Franquicia franchise = this.franchiseController.find(categoryProfileView.getCategoryPK().getFranquiciaIdfranquicia());
                    
                        if(franchise.getImagenIdimagen() != null){
                            
                            //Session
                            
                            javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        
                            //ServletContext
                            
                            javax.servlet.ServletContext servletContext = (javax.servlet.ServletContext) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                            
                            Clases.FilesManagementInterface filesManagement = new Clases.FilesManagement();
                            
                            String franchiseName = franchise.getNombre().trim();
                            
                            filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, franchiseName);
                        
                            
                            //DownloadImage
                            
                       
                            
                            fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+franchiseName);
                            
                            String image = fileController.getPathList().get(fileController.getPathList().size()-1);
                            
                            categoryProfileLayout.setImage(image);
                            
                            fileController.getPathList().remove(fileController.getPathList().size()-1);
                            
                            
                        }
                        
                        else{
                        
                           //Set No Image
                            
                            categoryProfileLayout.setImage("/images/noImage.png");
                        
                        }
                        
                    }
                    
                    categoryProfileView.setCategoryProfileLayout(categoryProfileLayout);
                    
                    if(categoryProfileView.getCategoryPK() != null && categoryProfileView.getCategoryPK().getFranquiciaIdfranquicia()>0 && categoryProfileView.getCategoryPK().getIdcategoria()>0)
                    {
                    
                        //Initialized menu
                        
                        categoryProfileView.setMenuModel(new org.primefaces.model.menu.DefaultMenuModel());
                        
                        if(this.franchiseController.find(categoryProfileView.getCategoryPK().getFranquiciaIdfranquicia())!=null){
                        
                            Entities.Franquicia franchise = this.franchiseController.find(categoryProfileView.getCategoryPK().getFranquiciaIdfranquicia());
                            
                            org.primefaces.model.menu.DefaultSubMenu submenu = new org.primefaces.model.menu.DefaultSubMenu();
                            
                            java.util.ResourceBundle bundle = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                            
                            submenu.setLabel(bundle.getString("EdenFinalUser.category.categories"));
                            
                            if(franchise.getCategoriaCollection()!=null && !franchise.getCategoriaCollection().isEmpty()){
                            
                                for(Entities.Categoria category : franchise.getCategoriaCollection()){
                                
                                    if(!category.getCategoriaPK().equals(categoryProfileView.getCategoryPK())){
                                    
                                        org.primefaces.model.menu.DefaultMenuItem item = new org.primefaces.model.menu.DefaultMenuItem();
                                        
                                        org.primefaces.component.link.Link link = new org.primefaces.component.link.Link();
                                        
                                        item.setValue(category.getNombre());
                                   
                                        javax.faces.context.ExternalContext ec = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                                        
                                        javax.servlet.http.HttpServletRequest request = (javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                                        
                                        String rawPath = request.getRequestURL().toString().replace(ec.getRequestServletPath(), "");
                                        
                                        System.out.print("RAW PATH "+rawPath);
                                        
                                        System.out.print("PATH "+request.getRequestURL().toString()+ec.getRequestServletPath());
                                        
                                        item.setUrl(rawPath+ec.getRequestServletPath()+"?faces-redirect=true&idFranchise="+category.getFranquicia().getIdfranquicia()+"&categoryId="+category.getCategoriaPK().getIdcategoria());
                                        
                                        submenu.addElement(item);
                                        
                                        
 
                                    }
                                
                                }
                                
                                
                            
                            }
                            
                               categoryProfileView.getMenuModel().addElement(submenu);
                        
                        }
                        
                        //Show west layout unit
                        
                        javax.faces.component.UIViewRoot viewRoot = javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                        
                        if(viewRoot.findComponent("layout")instanceof org.primefaces.component.layout.Layout){
                        
                            System.out.print("LAYOUT FOUND");
                            
                            org.primefaces.component.layout.Layout layout = (org.primefaces.component.layout.Layout)viewRoot.findComponent("layout");
                            
                            
                            
                            for(javax.faces.component.UIComponent component : layout.getChildren()){
                            
                                System.out.print("ID "+component.getClientId());
                                
                                if(component.getClientId().equals("west") && component instanceof org.primefaces.component.layout.LayoutUnit){
                                
                                    org.primefaces.component.layout.LayoutUnit unit= (org.primefaces.component.layout.LayoutUnit)component;
                                    
                                    unit.setVisible(true);
                                
                                }
                            
                            }
                            
                        
                        }
                        
                    }
                    
                    //Initialize Products
                    
                    if(categoryProfileView.getCategoryPK()!=null){
                    
                        Entities.Categoria category = this.categoryController.find(categoryProfileView.getCategoryPK());
                    
                        if(category.getProductoCollection()!=null && !category.getProductoCollection().isEmpty()){
                        
                            java.util.List<Clases.ViewLayout.Layout<Entities.ProductoPK>>list = new java.util.ArrayList<Clases.ViewLayout.Layout<Entities.ProductoPK>>();
                            
                            for(Entities.Producto product : category.getProductoCollection()){
                            
                                Clases.ViewLayout.ProductoLayoutImplementation aux = new Clases.ViewLayout.ProductoLayoutImplementation();
                            
                                aux.setName(product.getNombre());
                                
                                aux.setKey(product.getProductoPK());
                                
                                //Set Image
                                
                                if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                                
                                    
                                     //Session
                            
                            javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        
                            //ServletContext
                            
                            javax.servlet.ServletContext servletContext = (javax.servlet.ServletContext) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                          
                                    
                                    Clases.FilesManagementInterface filesManagement = new Clases.FilesManagement();
                                    
                                    java.io.File file = new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"resources"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getCategoria().getFranquicia().getNombre().trim()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre().trim());
                                
                                    if(file.exists()){
                                    
                                        filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"resources"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getCategoria().getFranquicia().getNombre().trim()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre().trim());
                                    
                                    }
                                    else{
                                    
                                        filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"resources"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getCategoria().getFranquicia().getNombre().trim()+java.io.File.separator+"Product"+java.io.File.separator, product.getNombre().trim());
                                    
                                    }
                                    
                                    
                                    this.fileController.serverDownload(((java.util.List<Entities.Imagen>)product.getImagenCollection()).get(Clases.EdenNumber.random(0, product.getImagenCollection().size()-1)),session.getAttribute("username").toString()+java.io.File.separator+product.getCategoria().getFranquicia().getNombre()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre());
                                    
                                    String image = this.fileController.getPathList().get(this.fileController.getPathList().size()-1);
                                    
                                    aux.setPrice(product.getPrecio());
                                    
                                    aux.setImage(image);
                                    
                                    this.fileController.getPathList().remove(this.fileController.getPathList().size()-1);
                                    
                                    this.fileController.getImageList().remove(this.fileController.getImageList().size()-1);
                                    
                                    
                                }
                                else{
                                
                                    aux.setImage("/images/noImage.png");
                                
                                }
                                
                                list.add(aux);
                                
                            }
                            
                            categoryProfileView.getCategoryProfileLayout().setViewLayout((java.util.Collection) list);
                        
                        }
                        
                    }
                    
                    
                }
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	private final Entities.CategoriaPK getKey() {
	
            try{
                
                java.util.Map<String,String> requestMap = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                
                if(requestMap != null){
                
                    if(requestMap.get("categoryId")!=null && requestMap.get("idFranchise") != null){
                    
                        Entities.CategoriaPK key = new Entities.CategoriaPK();
                        
                        key.setIdcategoria(new java.lang.Integer(requestMap.get("categoryId")));
                        
                        key.setFranquiciaIdfranquicia(new java.lang.Integer(requestMap.get("idFranchise")));
                        
                        return key;
                        
                    }
                
                }
            
                return null;
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
            return null;
            }
            
	}

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private FileController fileController;
	@javax.inject.Inject
	private FranchiseController franchiseController;
	@javax.inject.Inject
	private CategoryControllerInterface categoryController;
}