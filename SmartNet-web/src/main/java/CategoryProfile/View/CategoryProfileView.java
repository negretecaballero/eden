package CategoryProfile.View;

import CategoryProfile.Classes.*;
import CategoryProfile.Controller.*;


public class CategoryProfileView {

	private boolean init = false;
	private CategoryProfileLayout categoryProfileLayout = null;
	private Entities.CategoriaPK categoryPK;
	@javax.inject.Inject
	private CategoryProfileController categoryProfileController;
	private org.primefaces.model.menu.MenuModel menuModel;

	public boolean getInit() {
		return this.init;
	}

	/**
	 * 
	 * @param init
	 */
	public void setInit(boolean init) {
		this.init = init;
	}

	public CategoryProfileLayout getCategoryProfileLayout() {
		return this.categoryProfileLayout;
	}

	public void setCategoryProfileLayout(CategoryProfileLayout categoryProfileLayout) {
		this.categoryProfileLayout = categoryProfileLayout;
	}

	public Entities.CategoriaPK getCategoryPK() {
		return this.categoryPK;
	}

	public void setCategoryPK(Entities.CategoriaPK categoryPK) {
		this.categoryPK = categoryPK;
	}

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
	
            try{
            
                this.categoryProfileController.init();
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	public org.primefaces.model.menu.MenuModel getMenuModel() {
		return this.menuModel;
	}

	public void setMenuModel(org.primefaces.model.menu.MenuModel menuModel) {
		this.menuModel = menuModel;
	}
    
    
    
}