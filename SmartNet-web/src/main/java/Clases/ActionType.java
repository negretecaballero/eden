/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class ActionType implements ActionTypeInterface {

    
    
    public ActionType(String actionType) {
        this.actionType = actionType;
        
    }
    
    private String actionType;

    @Override
    public String getActionType() {
        return actionType;
    }

    @Override
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

   
    
    
}
