/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;

/**
 *
 * @author luisnegrete
 */
public interface ActionTypeInterface extends Serializable{

    String getActionType();

    void setActionType(String actionType);
    
}
