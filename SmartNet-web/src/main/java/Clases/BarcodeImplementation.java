/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;


/**
 *
 * @author luisnegrete
 */
public class BarcodeImplementation implements Barcode {
    
   // private BarCodeReader  _reader;
    
    
    @Override
    public String getCode(String path, String FileName){
    /*
        if(_reader==null){
        
            _reader=new BarCodeReader(path+java.io.File.separator+FileName,com.aspose.barcoderecognition.BarCodeReadType.Code39Standard);
        
            _reader.setImageBinarizationHints(RecognitionHints.ImageBinarization.MedianSmoothing);
            
            _reader.setMedianSmoothingWindowSize(4);
            
        }
        
        while(_reader.read()){
        
            System.out.print("COde read "+_reader.getCodeText());
        
        }
        
        return _reader.getCodeText();*/
        
        try{
        
            java.io.InputStream inputStream=new java.io.FileInputStream(path+java.io.File.separator+FileName);
            
            BufferedImage bufferedImage=ImageIO.read(inputStream);
            
            LuminanceSource source=new BufferedImageLuminanceSource(bufferedImage);
            
            
            
            BinaryBitmap bitMap=new BinaryBitmap(new HybridBinarizer(source));
        
            
            
            Reader reader=new MultiFormatReader();
            
       Map<DecodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<DecodeHintType, ErrorCorrectionLevel>();
    
       hintMap.put(DecodeHintType.TRY_HARDER, ErrorCorrectionLevel.L);
            
           Result result=reader.decode(bitMap,hintMap);
           
           return result.getText();
            
        }
        catch(java.io.IOException | com.google.zxing.NotFoundException | com.google.zxing.ChecksumException | com.google.zxing.FormatException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
           javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(null,"Try to scan again");
        
           msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
           
           FacesContext.getCurrentInstance().addMessage(null,msg);
        
           return "";
           
        } 
        
        finally{
            
            System.out.print("There is no barcode to scan");
    
        }
    
    }
    
}
