/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



/**
 *
 * @author luisnegrete
 */
public abstract class BaseBacking{

    
    protected FacesContext getContext() {
   
    return FacesContext.getCurrentInstance();
    
    }
    
    

    protected Map getRequestMap(){
      
    return getContext().getExternalContext().getRequestParameterMap();
    }
    
    protected HttpSession getSession(){
    return (HttpSession) getContext().getExternalContext().getSession(false);
    }
    
    protected Object evaluateEL(String elExpression,Class beanClazz){
        
    return getContext().getApplication().evaluateExpressionGet(getContext(), elExpression, beanClazz);
   
    }
    
    protected HttpServletRequest getRequest(){
    return (HttpServletRequest) getContext().getExternalContext().getRequest();
    }
    
    protected Object getSessionAuxiliarComponent(){
    
        return getSession().getAttribute("auxiliarComponent");
    
    }
    
    protected void setSessionAuxiliarComponent(Object obj){
    
    getSession().setAttribute("auxiliarComponent",obj);
        
    }
    
    protected String getCurrentView(){
    
        return this.getContext().getViewRoot().getViewId();
    
    }
    
    protected javax.servlet.ServletContext getServletContext(){
    
    return (javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
    
    }
    
    protected String getAbsoluteURL(){
    
        try{
        
            String url=this.getRequest().getRequestURL().toString();
            
            url=url.replaceAll(this.getContext().getViewRoot().getViewId(), "");
            
            url=url+"/";
            
            return url;
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return "";
        
        }
    
    }
    
    
}
