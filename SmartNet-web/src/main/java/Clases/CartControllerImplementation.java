/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class CartControllerImplementation implements CartControllerLayout {
    
    Entities.Sucursal sucursal;
    
    java.util.List<SessionClasses.CartItem>cartList;
    
    
    @Override
    public Entities.Sucursal getSucursal(){
    
        return this.sucursal;
    
    }
    
    
    @Override
    public void setSucursal(Entities.Sucursal sucursal){
    
        this.sucursal=sucursal;
    
    }
    
    
    @Override
    public java.util.List<SessionClasses.CartItem>getCartList(){
    
    
        if(this.cartList==null){
        
            this.cartList=new java.util.ArrayList<SessionClasses.CartItem>();
        
        }
        
        return this.cartList;
    
    }
    
    @Override
    public void setCartList(java.util.List<SessionClasses.CartItem>cartList){
    
    this.cartList=cartList;
    
    }
    
    
}
