/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface CartControllerLayout {
   
    Entities.Sucursal getSucursal();
    
    void setSucursal(Entities.Sucursal sucursal);
    
    java.util.List<SessionClasses.CartItem>getCartList();
    
    void setCartList(java.util.List<SessionClasses.CartItem>cartList);
    
}
