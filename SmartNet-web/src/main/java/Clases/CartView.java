/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import SessionClasses.CartItem;

/**
 *
 * @author luisnegrete
 */
public class CartView implements CartViewInterface{
    
    private CartItem cartItem;
    
    private String franchiseName;
    
    private double price;
    
    @Override
    public void setCartItem(CartItem cartItem){
    
        this.cartItem=cartItem;
    
    }
    @Override
    public CartItem getCartItem(){
    
    return this.cartItem;
    
    }
    @Override
    public void setFranchiseName(String franchiseName){
    
    this.franchiseName=franchiseName;
    
    }
    @Override
    public String getFranchiseName(){
    
        return this.franchiseName;
    
    }
    
    @Override
    public void setPrice(double price){
    
        this.price=price;
    
    }
    @Override
    public double getPrice(){
    
    return this.price;
    
    }
    
    public CartView(){
    
        
    
    }
    
}
