/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import SessionClasses.CartItem;

/**
 *
 * @author luisnegrete
 */
public interface CartViewInterface {
    
    void setPrice(double price);
    
    double getPrice();
    
    void setFranchiseName(String franchiseName);
    
    String getFranchiseName();
    
    void setCartItem(CartItem cartItem);
    
    CartItem getCartItem();
    
}
