/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Producto;
import SessionClasses.Item;
import java.util.List;
import javax.faces.event.ActionEvent;

/**
 *
 * @author luisnegrete
 */
public class CartViewUpdated implements CartViewUpdatedInterface{
    
    private String name;
    
    private double price;
    
    private List<CartViewInterface>cartList;
    
    @Override
    public void setPrice(double price){
    
    this.price=price;
    
    }
    
    @Override
    public double getPrice(){
        
        this.price=0;
    
        for(CartViewInterface viewInterface:this.cartList){
        
      if(viewInterface.getCartItem().getItem() instanceof Entities.Producto){
      
      
      Producto producto=(Producto)viewInterface.getCartItem().getItem();
      
      this.price=this.price+(producto.getPrecio()*viewInterface.getCartItem().getQuantity());
      
      }
      
       if(viewInterface.getCartItem().getItem() instanceof Entities.Menu){
      
      
      Entities.Menu menu=(Entities.Menu)viewInterface.getCartItem().getItem();
      
      this.price=this.price+(menu.getPrecio()*viewInterface.getCartItem().getQuantity());
      
      }
        
        }
        
    return this.price;
    
    }
    
    
   
    
    
    @Override
    public String getName(){
    
    return this.name;
    
    }
    
    @Override
    public void setName(String name){
    
    this.name=name;
    
    }
    
    @Override
    public  List<CartViewInterface>getCartList(){
    
        return this.cartList;
    
    }
    
    @Override
    public void setCartList(List<CartViewInterface>cartList){
    
    this.cartList=cartList;
    
    }
    
    
}
