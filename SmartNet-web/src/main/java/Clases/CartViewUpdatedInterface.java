/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface CartViewUpdatedInterface {
    
    double getPrice();
    
    void setPrice(double price);
    
    String getName();
    
    void setName(String name);
    
    List<CartViewInterface>getCartList();
    
    void setCartList(List<CartViewInterface>cartList);
    
  
    
}
