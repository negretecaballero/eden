/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Categoria;



/**
 *
 * @author luisnegrete
 */
public class CategoriaUpdated  {
  
    private Categoria categoria;
    
    private String concatKey;

   
    public Categoria getCategoria() {
        return categoria;
    }

   
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

   
    public String getConcatKey() {
        return concatKey;
    }

   
    public void setConcatKey(String concatKey) {
        this.concatKey = concatKey;
    }
    
    
    
}
