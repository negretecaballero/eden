/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Categoria;


/**
 *
 * @author luisnegrete
 */
public interface CategoriaUpdatedInterface {

    Categoria getCategoria();

    String getConcatKey();

    void setCategoria(Categoria categoria);

    void setConcatKey(String concatKey);
    
}
