/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class ComponeItems {

    private int idInv;
    private int idPro;

    public int getIdInv() {
        return idInv;
    }

    public void setIdInv(int idInv) {
        this.idInv = idInv;
    }

    public int getIdPro() {
        return idPro;
    }

    public void setIdPro(int idPro) {
        this.idPro = idPro;
    }

    public String getInv() {
        return Inv;
    }

    public void setInv(String Inv) {
        this.Inv = Inv;
    }

    public double getCant() {
        return Cant;
    }

    public void setCant(double Cant) {
        this.Cant = Cant;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    private String Inv;
    private boolean enabled;
    private double Cant;
}
