/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Random;

/**
 *
 * @author luisnegrete
 */
public class DataGeneration {
    
    public static String passwordGeneration(int size){
    
        
        String returnment="";
        
        String password[]={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"};
        
        for (int i=0;i<size;i++){
        
        int index=randInt(0,password.length-1);
        
        returnment=returnment+password[index];
        
        }
        
        return returnment;
        
    }
    
    private static int randInt(int min, int max) {

    // NOTE: Usually this should be a field rather than a method
    // variable so that it is not re-seeded every call.
    Random rand = new Random();

    // nextInt is normally exclusive of the top value,
    // so add 1 to make it inclusive
    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
}
    
}
