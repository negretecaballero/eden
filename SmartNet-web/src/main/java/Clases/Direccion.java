/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Clases;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;



/**
 *
 * @author luisnegrete
 */
public class Direccion {
    @Size(min = 1, max = 3)
    private String primerDigito;

    public String getPrimerDigito() {
        return primerDigito;
    }

    public void setPrimerDigito(String primerDigito) {
        this.primerDigito = primerDigito;
    }

    public String getSegundoDigito() {
        return segundoDigito;
    }

    public void setSegundoDigito(String segundoDigito) {
        this.segundoDigito = segundoDigito;
    }

    public int getTercerDigito() {
        return tercerDigito;
    }

    public void setTercerDigito(int tercerDigito) {
        this.tercerDigito = tercerDigito;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

  
    
    private String segundoDigito;
    
    @Min(1)
	@Max(100)
    private int tercerDigito;
    
    private String tipo;

    public String getAdicional_descripcion() {
        return adicional_descripcion;
    }

    public void setAdicional_descripcion(String adicional_descripcion) {
        
        this.adicional_descripcion = adicional_descripcion;
        
    }
    
    private String adicional_descripcion;

    public Direccion() {
       
    }
    
    @Override
    public String toString(){
     
        return "Direccion [ tipo = "+this.tipo+", primerDigito = "+this.primerDigito+", segundoDigito = "+this.segundoDigito+", segundoDigito = "+this.segundoDigito+", tercerDigito = "+this.tercerDigito+", adicional_description = "+this.adicional_descripcion+" ]";
    
    }
    
}
