package Clases;

public interface EdenCookie {

	/**
	 * 
	 * @param name
	 * @param value
	 * @param expiry
	 */
	void addCookie(String name, String value, int expiry);

	javax.servlet.http.Cookie[] listCookies();

	/**
	 * 
	 * @param name
	 */
	javax.servlet.http.Cookie findCookie(String name);

}