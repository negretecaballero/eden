package Clases;

public class EdenCookieImplementation implements EdenCookie {

	/**
	 * 
	 * @param name
	 * @param value
	 * @param expiry
	 */
	@Override
	public void addCookie(String name, String value, int expiry) {
	
            try{
            
                javax.faces.context.FacesContext context=javax.faces.context.FacesContext.getCurrentInstance();
                
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)context.getExternalContext().getRequest();
                
                javax.servlet.http.Cookie cookie=null;
                
                javax.servlet.http.Cookie[]cookies=request.getCookies();
                
               
                
       for (int i = 0; i < cookies.length; i++) {
                        
            System.out.print("COOKIE NAME "+cookies[i].getName());
                        
            if (cookies[i].getName().equals(name)) {
                cookie = cookies[i];
                break;
            }
        }
                
                    
    if (cookie != null) {
        cookie.setValue(value);
    } else {
        cookie = new javax.servlet.http.Cookie(name, value);
        cookie.setPath(request.getContextPath());
    }

    cookie.setMaxAge(expiry);
             
    javax.servlet.http.HttpServletResponse response=(javax.servlet.http.HttpServletResponse)context.getExternalContext().getResponse();
            
    response.addCookie(cookie);
    
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	@Override
	public javax.servlet.http.Cookie[] listCookies() {
		
            try{
            
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                
                if(request.getCookies()!=null && request.getCookies().length>0)
                
                {
                for(javax.servlet.http.Cookie cookie:request.getCookies()){
                
                    System.out.print("COOKIE: "+cookie.getName()+" Value: "+cookie.getValue());
                
                }
                
            }
                
                return request.getCookies();
                
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
                
            return null;
            
            }
            
            
	}

	/**
	 * 
	 * @param name
	 */
	@Override
	public javax.servlet.http.Cookie findCookie(String name) {
		
            try{
            
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                
                if(request.getCookies()!=null && request.getCookies().length>0){
                
                    for(javax.servlet.http.Cookie cookie:request.getCookies()){
                    
                        if(cookie.getName().equals(name)){
                        
                            return cookie;
                        
                        }
                    
                    }
                
                }
                
                return null;
            
            }
            catch(NullPointerException ex){
            
                return null;
            
            }
            
            
	}

}