/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;


/**
 *
 * @author luisnegrete
 */
public class EdenDate implements EdenDateInterface{
    
    @Min(0)
	@Max(23)
    private int hour;
    
    @Min(0)
	@Max(59)
    private int minutes;
    
    @javax.validation.constraints.Min(1)
	@javax.validation.constraints.Max(31)
    private int _day;
    
    
    @javax.validation.constraints.Min(1)
	@javax.validation.constraints.Max(12)
    private int _month;
    
    
    
    private int _year;
    
    @Override
    public int getDay(){
    
        return _day;
    
    }
    @Override
    public void setDay(int day){
    
        _day=day;
    
    }
    @Override
    public int getMonth(){
    
        return _month;
    
    }
    @Override
    public void setMonth(int month){
    
        _month=month;
    
    }
    @Override
    public int getYear(){

    return _year;

    }
    @Override
    public void setYear(int year){
    
    
        _year=year;
    
    }
    
    @Override
    public int getHour(){
    
        return this.hour;
        
    }
    
    @Override
    public int getMinutes(){
    
        return this.minutes;
    
    }
    
    @Override
    public void setHour(int hour){
    
    this.hour=hour;
    
    }
    
    @Override
    public void setMinutes(int minutes){
    
        this.minutes=minutes;
    
    }
    
  
    public static  ENUM.PartsOfDay greeting(){

    Calendar c=Calendar.getInstance();
    
    System.out.print("Date "+c.getTime());

    int hour =c.getTime().getHours();
   
    if(hour>5 && hour<12){
    
       return ENUM.PartsOfDay.MORNING;
       
    
    }
    
    else if(hour>=12 && hour<17){
    
      return ENUM.PartsOfDay.AFTERNOON;
    
    }
    
    else if(hour>=17 && hour <21){
    
       return ENUM.PartsOfDay.EVENING;
    
    }
    
    else if(hour>=21 || hour<=5){
    
        return ENUM.PartsOfDay.NIGTH;
    
    }
    
    else{
    
        return null;
    
    }
    
    }
   
    @Override
    public boolean compareSingleDate(java.util.Date date1, java.util.Date date2){
    
        if(date1.getDay()==date2.getDay() && date1.getMonth()==date2.getMonth() && date1.getYear()==date2.getYear()){
        
            return true;
        
        }
    return false;
    }
    
    
    @Override
    public boolean dateOrder(Clases.EdenDateInterface date1, Clases.EdenDateInterface date2){
    
          double netOpenTime=date1.getHour()+Double.valueOf((double)(date1.getMinutes()/60.0));
           
           System.out.print("Open Time "+date1.getHour()+":"+date1.getMinutes());
           
           System.out.print("Net Open Time "+netOpenTime);
           
           double netCloseTime=date2.getHour()+Double.valueOf((double)(date2.getMinutes()/60.0));
           
           System.out.print("Close Time "+date2.getHour()+":"+date2.getMinutes());
           
           System.out.print("Net Close Time "+netCloseTime);
    
           if(netCloseTime<netOpenTime){
           
           return false;
           
           }
           
           return true;
    }
    
    @Override
    public int daysBetween(java.util.Date date1, java.util.Date date2){
  
 
    
     long diff = date2.getTime() - date1.getTime();
    
     
     return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    
   
   
    
    }
    
    @Override
    public boolean equals(Object object){
    
        if(!(object instanceof Clases.EdenDate)){
        
        return false;
        
        }
        
        Clases.EdenDateInterface date=(Clases.EdenDate)object;
    
        if((this.hour!=date.getHour()) || (this.minutes!=date.getMinutes()) || (this._day!=date.getDay()) || (this._month!=date.getMonth()) || (this._year!=date.getYear())){
        
            return false;
        
        }
        
        return true;
    }
    
    @Override
    public String toString(){
    
       return "Month "+this._month+" Day "+this._day+" Year "+this._year+" Hour "+this.hour+" Minutes "+this.minutes+""; 
    
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + this.hour;
        hash = 37 * hash + this.minutes;
        hash = 37 * hash + this._day;
        hash = 37 * hash + this._month;
        hash = 37 * hash + this._year;
        return hash;
    }
    
    public static double convertToDecimalTime(int hour, int minutes){
    
    try{
    
        double min=(double)minutes/60;
        
        return ((double)hour)+min;
        
    
    }catch(NumberFormatException ex){
    
        System.out.print("Illegal Number");
        
    return 0.0;
    
    }
    
    
    
    }
    
    
 public static java.util.Date currentDate(){
    
  java.util.Calendar calendar=Calendar.getInstance();
  
  return calendar.getTime();
   
}
   
 public static  String currentDate(String format){
 
     java.util.Calendar cal=new java.util.GregorianCalendar(TimeZone.getTimeZone("America/Bogota"));
 
     java.util.Date date=new java.util.Date();
     
     date.setYear(cal.get(java.util.Calendar.YEAR));
     
     date.setMonth(cal.get(java.util.Calendar.MONTH));
     
     date.setDate(cal.get(java.util.Calendar.DAY_OF_MONTH));
     
     SimpleDateFormat dateFormat=new SimpleDateFormat(format);
     
     return dateFormat.format(date);
     
 }
 
}
