/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface EdenDateInterface {
    
    int getHour();
    
    int getMinutes();
    
    int getDay();
    
    void setDay(int day);
    
    int getMonth();
    
    void setMonth(int month);
    
    int getYear();
    
    void setYear(int year);
    
    void setHour(int hour);
    
    void setMinutes(int minutes);
    
   boolean compareSingleDate(java.util.Date date1, java.util.Date date2);
    
   int daysBetween(java.util.Date date1, java.util.Date date2);
   
   boolean dateOrder(Clases.EdenDateInterface date1, Clases.EdenDateInterface date2);
}
