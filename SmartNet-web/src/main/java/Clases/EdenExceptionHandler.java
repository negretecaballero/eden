/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisnegrete
 */
public class EdenExceptionHandler {
    
    public static void handleException(String className,Exception ex){
    
        Logger.getLogger(className).log(Level.SEVERE,null,ex);
        
    }
    
}
