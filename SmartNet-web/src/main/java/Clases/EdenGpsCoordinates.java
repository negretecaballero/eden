/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface EdenGpsCoordinates {
    
 double getLongitude();
 
 void setLongitude(double longitude);
 
 double getLatitude();
 
 void setLatitude(double latitude);   
    
}
