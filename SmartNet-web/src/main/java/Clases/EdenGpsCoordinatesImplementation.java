/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class EdenGpsCoordinatesImplementation implements EdenGpsCoordinates {
    
    double longitude;
    
    @Override
    public double getLongitude(){
    
        return this.longitude;
    
    }
    
    @Override
    public void setLongitude(double longitude){
    
        this.longitude=longitude;
    
    }
    
    double latitude;
    
    @Override
    public double getLatitude(){
    
        return this.latitude;
    
    }
    
    @Override
    public void setLatitude(double latitude){
    
        this.latitude=latitude;
    
    }
    
    @Override
    public String toString(){
    
        return "EdenGpsCoordinates [ latitude = "+this.latitude+"; longitude = "+this.longitude+" ]";
    
    }
    
}
