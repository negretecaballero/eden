package Clases;

public interface EdenOpeningTimes {

	String getDay();

	/**
	 * 
	 * @param day
	 */
	void setDay(String day);

	EdenTime getOpeningTime();

	/**
	 * 
	 * @param openingTime
	 */
	void setOpeningTime(EdenTime openingTime);

	EdenTime getClosingTime();

	/**
	 * 
	 * @param closingTime
	 */
	void setClosingTime(EdenTime closingTime);

}