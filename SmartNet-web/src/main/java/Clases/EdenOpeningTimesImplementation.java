package Clases;

public class EdenOpeningTimesImplementation implements EdenOpeningTimes {

	private String day;
	private EdenTime openingTime = null;
	private EdenTime closingTime = null;

        @Override
	public EdenTime getOpeningTime() {
		return this.openingTime;
	}

        @Override
	public void setOpeningTime(EdenTime openingTime) {
		this.openingTime = openingTime;
	}

        @Override
	public EdenTime getClosingTime() {
		return this.closingTime;
	}

        @Override
	public void setClosingTime(EdenTime closingTime) {
		this.closingTime = closingTime;
	}

	@Override
	public String getDay() {
		return this.day;
	}

	/**
	 * 
	 * @param day
	 */
	@Override
	public void setDay(String day) {
		this.day = day;
	}

}