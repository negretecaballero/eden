/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.Random;

/**
 *
 * @author luisnegrete
 */
public class EdenString {
    
    private static final String[] completeAlphabet={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"};
   
  public static double similarity(String s1, String s2) {
      
  
      String longer = s1, shorter = s2;
  if (s1.length() < s2.length()) { // longer should always have greater length
    longer = s2; shorter = s1;
  }
  int longerLength = longer.length();
  if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
  return (longerLength - EditDistance.getEditDistance(longer, shorter)) / (double) longerLength;
}
       
  
  public static double AccariedSimilatiry(String string1, String string2,int pos1, int pos2,double carried){

      
       if(pos1==(string1.split(" ").length)){

             
           return 0.0;
           
      }
      

        double similarity;
        
        if(pos2==(string2.split(" ").length-1)){
            
          
     String value1=string1.split(" ")[pos1].toLowerCase();
     
     
     String value2=string2.split(" ")[pos2].toLowerCase();
     
     
     if(pos2>0){
     
         if(Clases.EdenString.similarity(value1, value2)>Clases.EdenString.similarity(value1, string2.split(" ")[pos2-1].toLowerCase())){
         
               similarity=Clases.EdenString.similarity(value1, value2)+AccariedSimilatiry(string1,string2,pos1+1,0,0);
         
         }
         else{
         
             return AccariedSimilatiry(string1,string2,pos1+1,0,0);
         
         }
         
     
     }
     else{
     
          similarity=Clases.EdenString.similarity(value1, value2)+AccariedSimilatiry(string1,string2,pos1+1,0,0);
     
     }
     
           
      
      }
        else
        {
         
     String value1=string1.split(" ")[pos1].toLowerCase();

     String value2=string2.split(" ")[pos2].toLowerCase();

     
     if(pos2>0){
     
     if(Clases.EdenString.similarity(value1, value2)>Clases.EdenString.similarity(value1, string2.split(" ")[pos2-1].toLowerCase())){
     
       similarity=Clases.EdenString.similarity(value1, value2)+AccariedSimilatiry(string1,string2,pos1,(pos2+1),Clases.EdenString.similarity(value1, value2));
     
     }
     
     else{
     
        return AccariedSimilatiry(string1,string2,pos1,(pos2+1),carried);
     
     }
     
     }
     else{
     
         similarity=Clases.EdenString.similarity(value1, value2)+AccariedSimilatiry(string1,string2,pos1,(pos2+1),Clases.EdenString.similarity(value1, value2));
     
     }
     
    
  }
  return similarity;
      
 
  }
  

  
       
       private static final String[] letters={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
       
       
       public static String []split(String pattern,String string){
       
           String []result=new String[calculateLength(pattern,string)];
       
           int index=0;
           
           String aux=string;
           
           for(int i=0;i<result.length;i++){
           
               if(result[i]==null){
               
                   result[i]="";
               
               }
               
            for(int j=index;j<aux.length();j++){
            
                if(!String.valueOf(aux.charAt(j)).equals(pattern)){
                
                    result[i]=result[i]+String.valueOf(aux.charAt(j));
                    
                }
                else{
                
                    index=j+1;
                
                    break;
                }
            
            }
           
           }
           
           for(int i=0;i<result.length;i++){
           
           System.out.print("Position i "+result[i]);
           
           }
           
           return result;
       }
       
       
       private static int calculateLength(String pattern,String string){
       
           int index=0;
           
           for(int i=0;i<string.length();i++){
           
           if(String.valueOf(string.charAt(i)).equals(pattern)){
            
               index=index+1;
               
           }
               
           
           }
           
           return index+1;
       
       }
    
       
       public static String generateString(int index,String j){
       
          
           
           Random rand=new Random();
           
           int max=letters.length-1;
           
           int randomNum=rand.nextInt((max-0)+1)+0;
           
           
           
           System.out.print("J value "+j);
         

         if(j.length()<index){

             
         return generateString(index,j+letters[randomNum]);  
         
  
         }  
          
         return j;
       
       }
       
       
       public static String generatePassword(int length){
       
       Random rand=new Random();
       
       int randomNum=rand.nextInt(((completeAlphabet.length-1)-0)+1)+0;
           

       if(length==0){
       
           return"";
       
       }
       
       String result=completeAlphabet[randomNum]+generatePassword(length-1);
     
       return result;
       }
       
       
       public static String encription(String string){
       
           try{
           
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            String text = string;
            md.update(text.getBytes("UTF-8")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String output = bigInt.toString(16);
              
            
           return output;
           }
           catch(Exception | StackOverflowError ex){
           
              return null;
               
           }
       
       }
       
       public static String getPublicIpAddress() {
    String res = null;
    try {
        String localhost = InetAddress.getLocalHost().getHostAddress();
        Enumeration<NetworkInterface> e = NetworkInterface.getNetworkInterfaces();
        while (e.hasMoreElements()) {
            NetworkInterface ni = (NetworkInterface) e.nextElement();
            if(ni.isLoopback())
                continue;
            if(ni.isPointToPoint())
                continue;
            Enumeration<InetAddress> addresses = ni.getInetAddresses();
            while(addresses.hasMoreElements()) {
                InetAddress address = (InetAddress) addresses.nextElement();
                if(address instanceof Inet4Address) {
                    String ip = address.getHostAddress();
                    if(!ip.equals(localhost))
                        System.out.println((res = ip));
                }
            }
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return res;
}
       
}
