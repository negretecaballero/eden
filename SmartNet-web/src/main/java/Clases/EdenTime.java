package Clases;

public interface EdenTime {

	int getHour();

	/**
	 * 
	 * @param hour
	 */
	void setHour(int hour);

	int getMinutes();

	/**
	 * 
	 * @param minutes
	 */
	void setMinutes(int minutes);

	int getSeconds();

	/**
	 * 
	 * @param seconds
	 */
	void setSeconds(int seconds);

}