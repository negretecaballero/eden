package Clases;

public class EdenTimeImplementation implements EdenTime {

	private int hour;
	private int minutes;
	private int seconds;

	@Override
	public int getHour() {
		return this.hour;
	}

	/**
	 * 
	 * @param hour
	 */
	@Override
	public void setHour(int hour) {
		this.hour = hour;
	}

	@Override
	public int getMinutes() {
		return this.minutes;
	}

	/**
	 * 
	 * @param minutes
	 */
	@Override
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	@Override
	public int getSeconds() {
		return this.seconds;
	}

	/**
	 * 
	 * @param seconds
	 */
	@Override
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

}