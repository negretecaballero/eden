/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import javax.xml.xpath.XPathFactory;
import java.io.FileInputStream;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author luisnegrete
 */
public class EdenXML {
    
    public static void exampleParse(){
    
        try{
        
          DocumentBuilderFactory builderFactory=DocumentBuilderFactory.newInstance();
          
          DocumentBuilder builder = null;
        
          builder = builderFactory.newDocumentBuilder();
          
          Document document = builder.parse(new FileInputStream("/Users/luisnegrete/Documents/example.xml"));
          
          XPath xpath=XPathFactory.newInstance().newXPath();
          
          String expression="/bookstore/book[price>35.00]";
          
             //read a string value
          String book = xpath.compile(expression).evaluate(document,XPathConstants.STRING).toString();
          
             
          //read a nodelist using xpath
          NodeList nodeList = (NodeList) xpath.compile(expression).evaluate(document, XPathConstants.NODESET);
          
          System.out.print("BOOK SIZE "+nodeList.getLength());
          
          for(int i = 0; i < nodeList.getLength(); i++){
          
              System.out.print(nodeList.item(i).getNodeValue()); 
          
          }
          
        }
        catch(NullPointerException | ParserConfigurationException | SAXException | IOException | XPathExpressionException ex){
        
            ex.printStackTrace(System.out);
            
            
        
        }
    
    }
    
}
