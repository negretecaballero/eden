/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.File;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;




/**
 *
 * @author luisnegrete
 */
public class FilesManagement implements FilesManagementInterface,Serializable  {
    
   
    @Override
    public void createFolder(String path,String fileName){
    try{
        /*
        System.out.print("Creating Folder "+path+fileName);
        
        System.out.print("FileName: "+fileName);
      
        
        File file=new File(path+fileName);
        
        String auxi=path+fileName;
        
        String []split=EdenString.split("/", auxi);
        
        if(split.length==0)
        {
        if(!file.exists())
        {
            
        file.mkdir();
        
        }
        
        }
        
        else{
        
            String Path="";
            
          
            
            for(int i=1;i<split.length;i++){
            
                System.out.print("Splash "+i+" "+split[i]);
                
                Path=Path+java.io.File.separator+split[i];
            
                System.out.print("Slash Path to create "+Path);
                
                file=new File(Path);
            
                if(!file.exists()){
                
                    file.mkdir();
                
                }
                
            }
            
            
        
        }
        */
        
        java.io.File file=new java.io.File(path+java.io.File.separator+fileName);
        
        if(!file.exists()){
        
            file.mkdirs();
        
        }
        
        
        }
    catch(Exception ex){
        
      Logger.getLogger(FilesManagement.class.getName()).log(Level.SEVERE,null,ex);
 
    }
    }
    
    @Override
    public File[] getFiles(String path){
    
    try{
    
        File file=new File(path);
        
        if(file.exists() && file.isDirectory()){
        
           return file.listFiles();
            
        }
       
    return null;
    }
    catch(Exception ex){
    
        Logger.getLogger(FilesManagement.class.getName()).log(Level.SEVERE,null,ex);
    
        throw new javax.faces.FacesException(ex);
    }
    
    }
    
    @Override
    public void deleteFolder(String path){
    try{
        
    File file=new File(path);
    delete(file);
    
        System.out.print("Folder Deleted");
        
    
    
    }
    catch(Exception ex){
    Logger.getLogger(FilesManagement.class.getName()).log(Level.SEVERE,null,ex);
  }
    
    }
    
    @Override
    public void deleteFile(String path){
    
        try{
        
            File file=new File(path);
            
           
        delete(file);
            
        }
        catch(Exception ex){
        Logger.getLogger(FilesManagement.class.getName()).log(Level.SEVERE,null,ex);
        }
        
    }
    
    @Override
    public void cleanFolder(String path){
    
        try{
            
            File folder=new File(path);
            
            if(folder.isDirectory()){
                
            FileUtils.cleanDirectory(folder);
            
            }
            
        }
        
        catch(Exception ex){
            
        Logger.getLogger(FilesManagement.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
        
    }
    
    @Override
    public  File [] folderFiles(String path){
    
    
    try{
    
        
        File folder=new File(path);
        if(folder.isDirectory()){
        File [] files=folder.listFiles();
        return files;
        }
        
       return null; 
    }
    
    catch(Exception ex){
          
        Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
 
        return null;
    }
    
        
    }
    
    private void delete(File file){
    
    try{
    
        if(file.isDirectory()){
 
            System.out.print("File is a Directory");
            
    		//directory is empty, then delete it
    		if(file.list().length==0){
 
    		   file.delete();
    		   System.out.println("Directory is deleted : " + file.getAbsolutePath());
 
    		}else{
 
    		   //list all the directory contents
                  
        	   File files[] = file.listFiles();
 
                   System.out.print("File has "+file.length()+" Elements");
                   
        	   for (File temp : files) {
        	      //construct the file structure
        	     
        	      //recursive delete
        	     delete(temp);
        	   }
 
        	   //check the directory again, if empty then delete it
        	   if(file.list().length==0){
           	     file.delete();
        	     System.out.println("Directory is deleted : " 
                                                  + file.getAbsolutePath());
        	   }
    		}
 
    	}else{
    		//if file, then delete it
            System.out.print("File is not a Directory");
    		file.delete();
    		System.out.println("File is deleted : " + file.getAbsolutePath());
    	}
    
    
    }
    catch(Exception ex){
    
    }
    
    }
    
    
    @Override
    public String generateFolderName(String path){
    
        java.io.File file=new java.io.File(path);
        
        String fileName;
        
        fileName=Clases.EdenString.generateString(6, "");
       
       for(java.io.File aux:file.listFiles()){
       
       if(file.getName().equals(fileName)){
       
           generateFolderName(path);
       
       }
       
       }
        
     
       System.out.print("File Name Files Management "+fileName);
       
    return fileName;
    }
    
    
    public static SessionClasses.EdenList<String>getFileNameList(String folderName){
    
        try{
        
            java.io.File file=new java.io.File(folderName);
            
            if(file.isDirectory()){
            
               
            
            }
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
        return null;
        }
    
    }
    
}
