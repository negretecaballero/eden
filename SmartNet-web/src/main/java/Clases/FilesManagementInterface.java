/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.File;

/**
 *
 * @author luisnegrete
 */
public interface FilesManagementInterface {

    void createFolder(String path,String folderName);
    void deleteFolder(String path);
    void cleanFolder(String path);
    File [] folderFiles(String path);
    void deleteFile(String path);
    File[]getFiles(String path);
    String generateFolderName(String path);
    
}
