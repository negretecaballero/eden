/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseLayout {

    String getImage();
    
    void setImage(String image);
    
    Entities.Franquicia getFranchise();
    
    void setFranchise(Entities.Franquicia franchise);
    
}
