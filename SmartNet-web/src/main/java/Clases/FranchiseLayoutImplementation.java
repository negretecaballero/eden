/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class FranchiseLayoutImplementation implements FranchiseLayout{
    
    private String image;
    
    private Entities.Franquicia franchise;
    
    
    @Override
    public String getImage(){
    
        return this.image;
    
    }
    
    @Override
    public void setImage(String image){
    
    this.image=image;
    
    }
    
    @Override
    public Entities.Franquicia getFranchise(){
    
    return this.franchise;
    
    }
    
    @Override
    public void setFranchise(Entities.Franquicia franchise){
    
        this.franchise=franchise;
    
    }
    
    
}
