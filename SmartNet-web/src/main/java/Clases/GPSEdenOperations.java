/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import com.maxmind.geoip.Location;
import com.maxmind.geoip.LookupService;

import java.io.IOException;
import java.net.URL;


import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author luisnegrete
 */
public class GPSEdenOperations {
  
    public static int zoom(double distance){
    
    double x=distance;    
        
    double result=15;
    
    if(distance>0){
    
        result=((((x-650)*(x-6731))/((1-650)*(1-6731)))*15)+((((x-1)*(x-6731))/((650-1)*(650-6731)))*6)+((((x-1)*(x-650))/((6731-1)*(6731-650)))*1);
    
    }
    
    return (int)result;
    }
    
   public static double distance(double latitude1,double latitude2, double longitude1,double longitude2){
   
       return Clases.gpsOperations.calculateDistance(latitude1, latitude2, longitude1, longitude2);
   
   
   } 
   
   
   @annotations.MethodAnnotations(author="Luis Negrete",date="01/02/2016",comments="Address = Colombia,Bogota,Bogota,Diagonal+16+#+11b+-+16")
   public static Clases.EdenGpsCoordinates getCoordinates(String address){
   
   try{
   
       Clases.EdenGpsCoordinates coordinates=new Clases.EdenGpsCoordinatesImplementation();
       
       if(address!=null && !address.equals("")){
           
           URL myUrl=new URL("http://maps.googleapis.com/maps/api/geocode/xml?address="+address+"&sensor=false");
           
           DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
           
           DocumentBuilder builder=factory.newDocumentBuilder();
           
           Document doc=builder.parse(myUrl.openStream());
           
           XPathFactory xPathFactory=XPathFactory.newInstance();
           
           XPath xpath=xPathFactory.newXPath();
           
           javax.xml.xpath.XPathExpression expr=xpath.compile("/GeocodeResponse/result/geometry/location/lat");
         
           String latitude=expr.evaluate(doc,XPathConstants.STRING).toString();
           
           coordinates.setLatitude(new java.lang.Double(latitude));
           
           expr=xpath.compile("/GeocodeResponse/result/geometry/location/lng");
       
           String longitude=expr.evaluate(doc,XPathConstants.STRING).toString();
           
           coordinates.setLongitude(new java.lang.Double(longitude));
           
       }
       
       return coordinates;
   
   }
   catch(NullPointerException | NumberFormatException | javax.xml.xpath.XPathExpressionException  | SAXException | java.io.IOException | ParserConfigurationException ex){
   
       ex.printStackTrace(System.out);
       
   return null;
   }    
   
   }
   
   public static String locateIP(String ip,String path){
   
        try {
            
         
            path=path+java.io.File.separator+"GeoLiteCity.dat";
            
            System.out.print("IP "+ip);
            
           path= path.replace("\"", "");
               
           System.out.print("PATH "+path);
                 
            ip=ip.replace("\"", "");
            LookupService cl = new LookupService(path,LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
            
         Location location = cl.getLocation(ip);
            
            
        
             return  location.countryCode;
            
        } catch (IOException | NullPointerException ex) {
            Logger.getLogger(GPSEdenOperations.class.getName()).log(Level.SEVERE, null, ex);
            
            return "";
        }
   
   }
   
}
