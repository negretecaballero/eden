/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import SessionClasses.Key;

/**
 *
 * @author luisnegrete
 */
public class IndexCategoryPOJO implements IndexCategoryPOJOInterface{
    
    private String imageRoot;
    
    private Key key;
    
    
    @Override
    public String getImageRoot(){
    
    return this.imageRoot;
    
    }
    
    @Override
    public void setImageRoot(String imageRoot){
    
        this.imageRoot=imageRoot;
    
    }
    
    @Override
    public Key getKey(){
    
    return this.key;
    
    }
    
    @Override
    public void setKey(Key key){
    
    this.key=key;
    
    }
}
