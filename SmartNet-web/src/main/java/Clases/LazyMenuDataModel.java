/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class LazyMenuDataModel extends LazyDataModel<Entities.Menu>{
    
    private List<Entities.Menu>datasource;
    
    public LazyMenuDataModel(List<Entities.Menu>datasource){
    
        this.datasource=datasource;
    
    }
    
    @Override
    public Entities.Menu getRowData(String rowKey){
    
        for(Entities.Menu menu:datasource){
        
              if(menu.getIdmenu().equals(rowKey))
                return menu; 
        
        }
    
        return null;
    }
    
     @Override
    public Object getRowKey(Entities.Menu menu) {
        
        return menu;
    }
    
    
        @Override
    public List<Entities.Menu> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Entities.Menu> data = new ArrayList<Entities.Menu>();
 
        //filter
        for(Entities.Menu menu : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(menu.getClass().getField(filterProperty).get(menu));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(menu);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new LazySorterMenu(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
}
