/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class LazySorterMenu implements Comparator<Entities.Menu>{
     private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorterMenu(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.Menu menu1, Entities.Menu menu2) {
        try {
            Object value1 = Entities.Menu.class.getField(this.sortField).get(menu1);
            Object value2 = Entities.Menu.class.getField(this.sortField).get(menu2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
