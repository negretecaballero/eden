package Clases.Maps;

public interface BranchMarker<T> {

	double getLatitude();

	/**
	 * 
	 * @param latitude
	 */
	void setLatitude(double latitude);

	double getLongitude();

	/**
	 * 
	 * @param longitude
	 */
	void setLongitude(double longitude);

	String getAddress();

	/**
	 * 
	 * @param address
	 */
	void setAddress(String address);

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	T getObject();

	/**
	 * 
	 * @param object
	 */
	void setObject(T object);

}