package Clases.Maps;

public class BranchMarkerImplementation<T> implements BranchMarker<T> {

	private double latitude;
	private double longitude;
	private String address;
	private String image;
	private T object;

	@Override
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * 
	 * @param latitude
	 */
	@Override
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Override
	public double getLongitude() {
		return this.longitude;
	}

	/**
	 * 
	 * @param longitude
	 */
	@Override
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String getAddress() {
		return this.address;
	}

	/**
	 * 
	 * @param address
	 */
	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String getImage() {
		return this.image;
	}

	/**
	 * 
	 * @param image
	 */
	@Override
	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public T getObject() {
		return this.object;
	}

	/**
	 * 
	 * @param object
	 */
	@Override
	public void setObject(T object) {
		this.object = object;
	}

}