package Clases.Maps;

import Clases.*;

public interface InfoWindow {

	String getTitle();

	/**
	 * 
	 * @param title
	 */
	void setTitle(String title);

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	String getOpen();

	/**
	 * 
	 * @param open
	 */
	void setOpen(String open);

	EdenTime getEdenTime();

	/**
	 * 
	 * @param edenTime
	 */
	void setEdenTime(EdenTime edenTime);

	java.util.Collection<EdenOpeningTimes> getEdenOpeningTimeses();

	/**
	 * 
	 * @param edenOpeningTimeses
	 */
	void setEdenOpeningTimeses(java.util.Collection<EdenOpeningTimes> edenOpeningTimeses);

	boolean getInRange();

	/**
	 * 
	 * @param inRange
	 */
	void setInRange(boolean inRange);

}