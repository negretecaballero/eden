package Clases.Maps;

import Clases.*;
import java.util.*;

public class InfoWindowImplementation implements InfoWindow {

	private String title;
	private String image;
	private String open;
	private EdenTime edenTime = null;
	private Collection<EdenOpeningTimes> edenOpeningTimeses = null;
	private boolean inRange;

	@Override
	public String getTitle() {
		return this.title;
	}

	/**
	 * 
	 * @param title
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getImage() {
		return this.image;
	}

	/**
	 * 
	 * @param image
	 */
	@Override
	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String getOpen() {
		return this.open;
	}

	/**
	 * 
	 * @param open
	 */
	@Override
	public void setOpen(String open) {
		this.open = open;
	}

	
        @Override
        public EdenTime getEdenTime() {
		return this.edenTime;
	}

        @Override
	public void setEdenTime(EdenTime edenTime) {
		this.edenTime = edenTime;
	}

        @Override
	public Collection<EdenOpeningTimes> getEdenOpeningTimeses() {
		return this.edenOpeningTimeses;
	}

        @Override
	public void setEdenOpeningTimeses(Collection<EdenOpeningTimes> edenOpeningTimeses) {
		this.edenOpeningTimeses = edenOpeningTimeses;
	}

	@Override()
	public boolean getInRange() {
		return this.inRange;
	}

	/**
	 * 
	 * @param inRange
	 */
	@Override()
	public void setInRange(boolean inRange) {
		this.inRange = inRange;
	}

}