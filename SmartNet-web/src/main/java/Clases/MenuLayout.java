/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface MenuLayout {
    
    Entities.Menu getMenu();
    
    void setMenu(Entities.Menu menu);
    
    SessionClasses.EdenList<String>getImages();
    
    void setImages(SessionClasses.EdenList<String> images);
    
}
