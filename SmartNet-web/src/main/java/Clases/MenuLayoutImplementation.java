/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class MenuLayoutImplementation implements MenuLayout {
    
    private Entities.Menu _menu;
    
    private SessionClasses.EdenList<String>_images;
    
    public MenuLayoutImplementation(){
    
    
    }
    
    public MenuLayoutImplementation(Entities.Menu menu){
    
        _menu=menu;
    
    }
    
    @Override
    public Entities.Menu getMenu(){
    
        return _menu;
    
    }
    
    @Override
    public void setMenu(Entities.Menu menu){
    
        _menu=menu;
    
    }
    
    @Override
    public SessionClasses.EdenList<String>getImages(){
    
        return _images;
    
    }
    
    @Override
    public void setImages(SessionClasses.EdenList<String>images){
    
        _images=images;
    
    }
    
}
