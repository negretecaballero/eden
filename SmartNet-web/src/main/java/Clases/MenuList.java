/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Menu;

/**
 *
 * @author luisnegrete
 */
public class MenuList implements MenuListInterface{
    
    private Menu menu;
    
    private int quantity;
    
    
    @Override
    public Menu getMenu(){
    
    return this.menu;
    
    }
    
    @Override
    public void setMenu(Menu menu){
    
    this.menu=menu;
    
    }
    
    @Override
    public int getQuantity(){
    
    return this.quantity;
    
    }
    
    @Override
    public void setQuantity(int quantity){
    
    this.quantity=quantity;
    
    }
    
}
