/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Menu;

/**
 *
 * @author luisnegrete
 */
public interface MenuListInterface {
    
    Menu getMenu();
    
    void setMenu(Menu menu);
    
    int getQuantity();
    
    void setQuantity(int quantity);
    
}
