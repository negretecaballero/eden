/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class MenuResultsViewType implements MenuResultsViewTypeInterface{
    
    private Entities.Menu menu;
    
    private List<String>images;
    
    private int menuQuantity;
    
    public MenuResultsViewType(){
    
        this.images=new <String>ArrayList();
    
    }
    @Override
    public Entities.Menu getMenu(){
    
        return this.menu;
    
    }
    
    @Override
    public List<String>getImages(){
    
        return this.images;
    
    }
    
    @Override
    public void setMenu(Entities.Menu menu){
    
        this.menu=menu;
    
    }
    
    @Override
    public void setImages(List<String>images){
    
        this.images=images;
    
    }
    
    @Override
    public int getMenuQuantity(){
    
        return this.menuQuantity;
    
    }
    
    @Override
    public void setMenuQuantity(int menuQuantity){
    
        this.menuQuantity=menuQuantity;
    
    }
    
}
