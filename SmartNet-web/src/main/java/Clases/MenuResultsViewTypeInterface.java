/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface MenuResultsViewTypeInterface {
    
    Entities.Menu getMenu();
    
    java.util.List<String>getImages();
    
    void setMenu(Entities.Menu menu);
    
    void setImages(java.util.List<String> images);
    
    int getMenuQuantity();
    
    void setMenuQuantity(int menuQuantity);
}
