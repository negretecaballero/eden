/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class MenuUpdateLayout implements MenuUpdateLayoutInterface {
    
   private Entities.ProductoPK productKey;
    
   private Entities.Agrupa agrupa;
   
   
   @Override
  public Entities.ProductoPK getProductKey(){
   
   return this.productKey;
   
   }
  
  @Override
  public void setProductKey(Entities.ProductoPK productKey){
  
  this.productKey=productKey;
  
  }
    
  @Override
  public Entities.Agrupa getAgrupa(){
  
  return this.agrupa;
  
  }
  
  @Override
  public void setAgrupa(Entities.Agrupa agrupa){
  
  this.agrupa=agrupa;
  
  
}


}