/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface MenuUpdateLayoutInterface {
    
    Entities.ProductoPK getProductKey();
    
    void setProductKey(Entities.ProductoPK productKey);
    
    Entities.Agrupa getAgrupa();
    
    void setAgrupa(Entities.Agrupa agrupa);
    
}
