/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class Message {
    
    private String text;
    
    private String user;
    
    private boolean updateList;
    
    public Message(String text){
    
        this.text=text;
    
    }
    
    public Message(String text,boolean updateList){
    
    this.text=text;
    
    this.updateList=updateList;
    
    }
    
    public Message(){
    
    
    }
    
    public Message(String user, String text,boolean updateList){
    
    this.user=user;
    
    this.text=text;
    
    this.updateList=updateList;
    
    }
    
    public String getText(){
    
        return this.text;
    
    }
    
    public void setText(String text){
    
        this.text=text;
    
    }
    
    public String getUser(){
    
        return this.user;
    
    }
    
    public void setUser(String user){
    
        this.user=user;
    
    }
    
    public boolean isUpdateList(){
    
    return this.updateList;
    
    }
    
    public void setUpdateList(boolean updateList){
    
        this.updateList=updateList;
        
    }
    
}
