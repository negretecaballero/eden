/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Pedido;
import SessionClasses.CartItem;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class PedidoWorker implements PedidoWorkerInterface {
    
    private Pedido pedido;
    
    private String image;
    
    List<CartItem>itemList;
    
    @Override
    public Pedido getPedido(){
    
      
        
        if(this.pedido==null){
        
            this.pedido=new Pedido();
        
        }
      return this.pedido;
    }
    
    @Override
    public void setPedido(Pedido pedido){
    
    
    this.pedido=pedido;
    
    }
    
    @Override
    public List<CartItem>getItemList(){
    
        if(this.itemList==null){
        
        this.itemList=new <CartItem>ArrayList();
        
        }
    
        return this.itemList;
    }
    
    @Override
    public void setItemList(List<CartItem>itemList){
        
    this.itemList=itemList;
    
    }
    
    @Override
    public String getImage(){
    
        return this.image;
    
    }
    
    @Override
    public void setImage(String image){
    
        this.image=image;
        
    }
    
}
