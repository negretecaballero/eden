/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Pedido;
import SessionClasses.CartItem;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface PedidoWorkerInterface {
    
    void setPedido(Pedido pedido);
    
    Pedido getPedido();
    
    void setItemList(List<CartItem>itemList);
    
    List<CartItem>getItemList();
    
    String getImage();
    
    void setImage(String image);
    
}
