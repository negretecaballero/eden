/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface ProductoLayout {
    
    java.util.List<String> getImages();
    
    void setImages(java.util.List<String>_images);
    
    Entities.Producto getProduct();
    
    void setProduct(Entities.Producto product);
    
}
