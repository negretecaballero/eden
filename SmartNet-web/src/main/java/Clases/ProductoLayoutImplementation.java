/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class ProductoLayoutImplementation implements ProductoLayout {
    
    private java.util.List<String> _images;
    
    private Entities.Producto _product;
    
    
    @Override
    public java.util.List<String> getImages(){
    
        return _images;
    
    }
    
    @Override
    public void setImages(java.util.List<String> images){
    
    _images=images;
    
    }
    
    @Override
    public Entities.Producto getProduct(){
    
    return _product;
    
    }
    
    @Override
    public void setProduct(Entities.Producto product){
    
        _product=product;
    
    }
    
}
