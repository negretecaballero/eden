/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Producto;

/**
 *
 * @author luisnegrete
 */
public class ProductsList implements ProductsListInterface{
    
   private Entities.Producto product;
    
   private int quantity;
   
   @Override
   public void setProduct(Producto product){
   
       this.product=product;
   
   }
   
   @Override
   public Producto getProduct(){
   
       return this.product;
   
   }
   
   @Override
   public void setQuantity(int quantity){
   
   this.quantity=quantity;
   
   }
   
   @Override
   public int getQuantity(){
   
   return this.quantity;
   
   }
}
