/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Producto;

/**
 *
 * @author luisnegrete
 */
public interface ProductsListInterface {
    
    
    void setProduct(Producto product);
    
    
    Producto getProduct();
    
    
    void setQuantity(int quantity);
    
    int getQuantity();
            
    
    
}
