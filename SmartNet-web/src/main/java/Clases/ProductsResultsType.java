/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayout;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class ProductsResultsType implements ProductsResultsTypeInterface{
   
    
    private Entities.Producto product;
    
    private List<String>images;
    
    private int productQuantity;
    
    @Override
    public int getProductQuantity(){
    
        return this.productQuantity;
    
    }
    @Override
    public void setProductQuantity(int productQuantity){
    
        this.productQuantity=productQuantity;
    
    }
    
    @Override
    public List<String>getImages(){

return this.images;

}
    
    @Override
    public void setImages(List<String>images){
    
    this.images=images;
    
    }
    
    @Override
    public Entities.Producto getProduct(){
    
        return this.product;
    
    }
    
    @Override
    public void setProduct(Entities.Producto product){
    
    this.product=product;
    
    }
    
    public ProductsResultsType(){
    
    
    this.images=new <String>ArrayList();
    
    this.similar=new <SimilarProductsLayout>ArrayList();
    
    }
    
    private List<SimilarProductsLayoutInterface>similar;
    
    public List<SimilarProductsLayoutInterface>getSimilar(){
    
        return this.similar;
    
    }
    
    
    public void setSimilar(List<SimilarProductsLayoutInterface>similar){
    
        this.similar=similar;
    
    }
}
