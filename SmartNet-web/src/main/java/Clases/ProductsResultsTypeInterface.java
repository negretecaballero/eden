/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface ProductsResultsTypeInterface {
    
    void setProduct(Entities.Producto product);
    void setImages(List<String>images);
    
    Entities.Producto getProduct();
    List<String>getImages();
    
    int getProductQuantity();
    
    void setProductQuantity(int productQuantity);
}
