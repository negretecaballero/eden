package Clases;

/**
 * @author luisnegrete
 */
public interface SucursalHasAdditionLayout {

	Entities.Addition getAddition();

	void setAddition(Entities.Addition addition);

	String getImage();

	void setImage(String image);

}