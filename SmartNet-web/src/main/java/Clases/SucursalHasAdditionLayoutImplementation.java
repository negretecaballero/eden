package Clases;

/**
 * @author luisnegrete
 */
public class SucursalHasAdditionLayoutImplementation implements SucursalHasAdditionLayout {

	private Entities.Addition addition;
	private String image;

	public Entities.Addition getAddition() {
		return this.addition;
	}

	public void setAddition(Entities.Addition addition) {
		this.addition = addition;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}