package Clases;

/**
 * @author luisnegrete
 */
public interface SucursalHasMenuFrontEndLayout {

	Entities.Menu getMenu();

	void setMenu(Entities.Menu menu);

	String getImage();

	void setImage(String image);

}