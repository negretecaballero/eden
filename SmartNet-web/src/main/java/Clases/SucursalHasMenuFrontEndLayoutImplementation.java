package Clases;

/**
 * @author luisnegrete
 */
public class SucursalHasMenuFrontEndLayoutImplementation implements SucursalHasMenuFrontEndLayout {

	private Entities.Menu menu;
	private String image;

	public Entities.Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Entities.Menu menu) {
		this.menu = menu;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}