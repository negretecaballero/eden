/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import Entities.Tipo;

/**
 *
 * @author luisnegrete
 */
public class TipoComparator implements java.util.Comparator<Entities.Tipo>{

    @Override
    public int compare(Tipo o1, Tipo o2) {
  if(o1.getNombre().equals(o2.getNombre())){
  
      return 0;
  
  }
  
  else if(o1.getNombre().compareTo(o2.getNombre())<0){
  
      return -1;
  
  }
    return 1; 
    
    }
}
