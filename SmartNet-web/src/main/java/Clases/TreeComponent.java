/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class TreeComponent implements TreeComponentInterface{
    
    private String value;
    
    private int id;

    @Override
    public String getValue() {
        return value;
    }
    
    
    @Override
    public void setValue(String value) {
        this.value = value;
    }
    
    

    @Override
    public int getId() {
        return id;
    }

    
    @Override
    public void setId(int id) {
        this.id = id;
    }
    
    
}
