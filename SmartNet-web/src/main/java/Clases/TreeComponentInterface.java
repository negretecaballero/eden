/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface TreeComponentInterface {
  
    public String getValue();
    
    void setValue(String value);
    
    public int getId();
    
    void setId(int id);
    
}
