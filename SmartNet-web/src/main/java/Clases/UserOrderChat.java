/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class UserOrderChat {
    
    private String _title;
    
    private String _content;
    
    private String _user;
    
    public String getTitle(){
    
        return _title;
    
    }
    
    public void setTitle(String title){
    
    _title=title;
    
    }
    
    public String getContent()
    {
    
        return _content;
    
    }  
    
    public void setContent(String content){
     
    _content=content;
    
    }
    
    
    public String getUser(){
    
    return _user;
    
    }
    
    public void setUser(String user){
    
    _user=user;
    
    }
    
    
}
