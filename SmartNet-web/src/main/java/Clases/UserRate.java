/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class UserRate implements UserRateInterface{
    
    private short rate;
    
    private Clases.PedidoWorkerInterface pedido;
    
    @Override
    public Clases.PedidoWorkerInterface getPedido(){
    
        return this.pedido;
    
    }
    
    @Override
    public void setPedido(Clases.PedidoWorkerInterface pedido){
    
        this.pedido=pedido;
    
    }
    
    @Override
    public short getRate(){
    
        return this.rate;
    
    }
    
    @Override
    public void setRate(short rate){
    
        this.rate=rate;
    
    }
    
    
}
