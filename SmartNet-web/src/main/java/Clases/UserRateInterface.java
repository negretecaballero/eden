/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public interface UserRateInterface {
   
    short getRate();
    
    void setRate(short pedido);
    
    Clases.PedidoWorkerInterface getPedido();
    
    void setPedido(Clases.PedidoWorkerInterface pedido);
    
}
