package Clases.ViewLayout;

public interface AddressViewLayout {

	java.util.Collection<Entities.State> getStateCollection();

	/**
	 * 
	 * @param stateCollection
	 */
	void setStateCollection(java.util.Collection<Entities.State> stateCollection);

	/**
	 * 
	 * @param cityCollection
	 */
	void setCityCollection(java.util.Collection<Entities.City> cityCollection);

	java.util.Collection<Entities.City> getCityCollection();

	Entities.Direccion getAddress();

	/**
	 * 
	 * @param address
	 */
	void setAddress(Entities.Direccion address);

	/**
	 * 
	 * @param state
	 */
	void updateCities(Entities.State state);

}