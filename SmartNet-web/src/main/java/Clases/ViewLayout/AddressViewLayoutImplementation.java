package Clases.ViewLayout;

import java.util.*;

public class AddressViewLayoutImplementation implements AddressViewLayout {

	private Collection<Entities.State> stateCollection = null;
	private Entities.Direccion address;
	private Collection<Entities.City> cityCollection = null;

	@Override
	public java.util.Collection<Entities.State> getStateCollection() {
		return this.stateCollection;
	}

	/**
	 * 
	 * @param stateCollection
	 */
	@Override
	public void setStateCollection(java.util.Collection<Entities.State> stateCollection) {
		this.stateCollection = stateCollection;
	}

	@Override
	public java.util.Collection<Entities.City> getCityCollection() {
		
            return this.cityCollection;
            
	}

	/**
	 * 
	 * @param cityCollection
	 */
	@Override
	public void setCityCollection(java.util.Collection<Entities.City> cityCollection) {
	
            this.cityCollection = cityCollection;
            
	}

	@Override
	public Entities.Direccion getAddress() {
		return this.address;
	}

	/**
	 * 
	 * @param address
	 */
	@Override
	public void setAddress(Entities.Direccion address) {
		this.address = address;
	}

	/**
	 * 
	 * @param state
	 */
	@Override
	public void updateCities(Entities.State state) {
		
            this.cityCollection = state.getCityCollection();
            
	}

}