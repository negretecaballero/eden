package Clases.ViewLayout;

public interface CategoryViewLayout<T> {

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	T getKey();

	/**
	 * 
	 * @param key
	 */
	void setKey(T key);

	String getName();

	/**
	 * 
	 * @param name
	 */
	void setName(String name);

}