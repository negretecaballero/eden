package Clases.ViewLayout;

public abstract class Layout<T> {

	private String image;
	private T key;
	private String name;

	protected String getImage() {
		return this.image;
	}

	/**
	 * 
	 * @param image
	 */
	protected void setImage(String image) {
		this.image = image;
	}

	protected T getKey() {
		return this.key;
	}

	/**
	 * 
	 * @param key
	 */
	protected void setKey(T key) {
		this.key = key;
	}

	protected String getName() {
		return this.name;
	}

	/**
	 * 
	 * @param name
	 */
	protected void setName(String name) {
		this.name = name;
	}

}