package Clases.ViewLayout;

public class MealViewLayoutImplementation<T> extends Layout<T> implements MealViewLayout<T> {

	@Override
	public String getImage() {
		
            return super.getImage();
            
	}

	/**
	 * 
	 * @param image
	 */
	@Override
	public void setImage(String image) {
		super.setImage(image);
	}

	@Override
	public T getKey() {
		return super.getKey();
	}

	/**
	 * 
	 * @param key
	 */
	@Override
	public void setKey(T key) {
		
            super.setKey(key);
            
	}

	@Override
	public String getName() {
		
            return super.getName();
            
	}

	/**
	 * 
	 * @param name
	 */
	@Override
	public void setName(String name) {
		
            super.setName(name);
            
	}

}