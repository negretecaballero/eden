package Clases.ViewLayout;

public interface ProductoLayout {

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	Entities.ProductoPK getKey();

	/**
	 * 
	 * @param key
	 */
	void setKey(Entities.ProductoPK key);

	String getName();

	/**
	 * 
	 * @param name
	 */
	void setName(String name);

	double getPrice();

	/**
	 * 
	 * @param price
	 */
	void setPrice(double price);
}