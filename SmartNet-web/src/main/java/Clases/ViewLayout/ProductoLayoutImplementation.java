package Clases.ViewLayout;

public class ProductoLayoutImplementation extends Layout<Entities.ProductoPK> implements ProductoLayout {

	@Override
	public String getImage() {
	
            return super.getImage();
        
        }

	/**
	 * 
	 * @param image
	 */
	@Override
	public void setImage(String image) {
		
            super.setImage(image);
            
	}

	@Override
	public Entities.ProductoPK getKey() {
	
        
        return super.getKey();
        
        }

	/**
	 * 
	 * @param key
	 */
	@Override
	public void setKey(Entities.ProductoPK key) {
	
        super.setKey(key);
        
        }

	@Override
	public String getName() {
	
        return super.getName();
        
        }

	/**
	 * 
	 * @param name
	 */
	@Override
	public void setName(String name) {
	
        super.setName(name);
        
        }

	@Override
	public double getPrice() {
		return this.price;
	}

	/**
	 * 
	 * @param price
	 */
	@Override
	public void setPrice(double price) {
		this.price = price;
	}

	private double price;



}