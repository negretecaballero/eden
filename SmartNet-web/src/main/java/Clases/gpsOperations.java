/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author luisnegrete
 */
public class gpsOperations {
    
    
    public static double calculateDistance(double lat1,double lat2,double lon1,double lon2){
        
        
    
    double r=6371000;//Meters Earth Ratio
    
    double latRad1=Math.toRadians(lat1);
    
    double latRad2=Math.toRadians(lat2);
    
    double varLat=Math.toRadians(lat2-lat1);
    
    double lonVar=Math.toRadians(lon2-lon1);
    
    double a=Math.sin(varLat/2)*Math.sin(varLat/2)+Math.cos(latRad1)*Math.sin(lonVar/2)*Math.sin(lonVar/2);
    
    double c=2*Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    
    return r*c;
    }
    
}
