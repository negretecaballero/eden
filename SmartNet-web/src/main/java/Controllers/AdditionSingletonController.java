package Controllers;

public interface AdditionSingletonController {

	void update();

	SessionClasses.EdenList<Entities.Addition> getAdditions();
}