package Controllers;

import CDIBeans.*;

@javax.enterprise.context.ApplicationScoped
@javax.faces.bean.ManagedBean(eager=true)
@javax.inject.Named
@javax.enterprise.inject.Alternative

public class AdditionSingletonControllerImplementation implements AdditionSingletonController {

	@Override
	public void update() {
		
            try{
            
            if(_additionController.getAll()!=null && !_additionController.getAll().isEmpty()){
            
                SessionClasses.EdenList<Entities.Addition>additions=new SessionClasses.EdenList<Entities.Addition>();
                
                for(Entities.Addition addition:_additionController.getAll()){
                
                    additions.addItem(addition);
                
                }
                
                _additionSingletonFacade.setAdditions(additions);
            
            }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
	}

	@Override
	public SessionClasses.EdenList<Entities.Addition> getAdditions() {
		
            try{
            
             if(_additionSingletonFacade.getAdditions()==null || _additionSingletonFacade.getAdditions().isEmpty()){
                
                
                   update();
                
                }
            
            return _additionSingletonFacade.getAdditions();
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
            
	}

	@javax.annotation.PostConstruct()
	public void init() {
		// TODO - implement AdditionSingletonControllerImplementation.init
		throw new UnsupportedOperationException();
	}

	/*@javax.annotation.PostConstruct
	public void init() {
		
            try{
            
                if(_additionSingletonFacade.getAdditions()==null || _additionSingletonFacade.getAdditions().isEmpty()){
                
                
                    update();
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}*/

	@javax.ejb.EJB
	private Singleton.AdditionSingletonFacade _additionSingletonFacade;
      
        @CDIBeans.AdditionControllerQualifier
	@javax.inject.Inject
	private AdditionControllerDelegate _additionController;
}