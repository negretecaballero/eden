/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import java.io.*;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.SessionScoped
@javax.inject.Named("AddressTypeController")
@javax.ejb.TransactionAttribute(javax.ejb.TransactionAttributeType.NOT_SUPPORTED)
public class AddressTypeControllerImplementation implements java.io.Serializable, AddressTypeController{
    
    @javax.ejb.EJB
    private Singleton.AddressTypeSingletonFacadeLocal addressTypeFacade;
    
    @Override
    @javax.transaction.Transactional
    public java.util.List<String>getType(){
    
        return addressTypeFacade.getType();
    
    }
    
    
}
