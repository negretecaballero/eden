/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import CDIBeans.MenuControllerInterface;
import CDIBeans.ProductoControllerInterface;
import Entities.Menu;
import Entities.Producto;
import SessionBeans.OrderFacadeLocal;
import SessionClasses.CartInterface;
import SessionClasses.CartItem;
import SessionClasses.Item;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import java.io.*;

/**
 *
 * @author luisnegrete
 */
@javax.inject.Named("cartController")
@javax.enterprise.context.SessionScoped

public class CartController implements java.io.Serializable {
    
    @EJB 
    private OrderFacadeLocal orderFacade;

    @Inject
    
    @CDIBeans.ProductoControllerQualifier
    
    private transient ProductoControllerInterface productoController;
    
    @Inject
    
    @CDIBeans.MenuControllerQualifier
    
    private transient MenuControllerInterface menuController;
    
    /**
     * Creates a new instance of CartController
     */
    public CartController() {
    }
    
    public void addToCart(Item item, int quantity,Entities.Sucursal sucursal){
        
        System.out.print("Adding Item to Cart");
        
        System.out.print("Quantity "+quantity);
        
        System.out.print("Item to add "+item.getClass().getName());
        
        if(getCart()==null){
        
            System.out.print("Get cart is null");
        
        }
        
        if(getCart()!=null){
            
            
            
            System.out.print("Get Cart is not null");
            
        if(!findSimilar(item)){
        
        CartItem cartItem=new CartItem();
        
        cartItem.setItem(item);

        cartItem.setQuantity(quantity);
        
        System.out.print("Item added to cart");
        
        cartItem.setSucursal(sucursal);
        
        if(item instanceof Producto){
        
            if((productoController.productQuantity(((Entities.Producto)item).getProductoPK())>=quantity)||!this.productoController.validInventory((Entities.Producto)item))
            {
            
            cartItem.setName(((Producto)item).getNombre());
            
            
            
             getCart().getCart().add(cartItem);
             
             
             
            }
            else{
            
                FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"There is not enough inventory fot this Product");
                
                FacesContext fc=FacesContext.getCurrentInstance();
                
                fc.addMessage(null,msg);
                
                
            
            }
        
        }
        
        else if(item instanceof Menu){
        
           if(menuController.menuQuantity(((Entities.Menu)item).getIdmenu())>=quantity){
           cartItem.setName(((Menu)item).getNombre());
           getCart().getCart().add(cartItem);
               
           
           }
           
           else{
           
               FacesMessage msg=new FacesMessage("There is not enough invenotry for this menu");
               
               msg.setSeverity(FacesMessage.SEVERITY_ERROR);
               
               FacesContext fc=FacesContext.getCurrentInstance();
               
               fc.addMessage(null,msg);
           
           }
            
            
           
        }
        
       
        
        System.out.print(getCart().getCart().size());
        
        System.out.print(quantity);
            }
        
        }
        
        else{
        
        System.out.print("Cart Is null");
        
        }
    
  
    
    }
    
    public void removeFromCart(ActionEvent event, CartItem item){
    
        int index=0;
        
        for(int i=0;i<getCart().getCart().size();i++){
        
            if(getCart().getCart().get(i).equals(item)){
            
            index=i;
            
            }
        
        }
    
  getCart().getCart().remove(index);
    
    }
    
    public double getPrice(){
    
    double price=0;
    
    for(CartItem item:getCart().getCart()){
    
        if(item.getItem() instanceof Producto){
        
        price=price+(((Producto)item.getItem()).getPrecio()*item.getQuantity());
        
        }
        
        else if(item.getItem() instanceof Menu){
        
        price=price+(((Menu)item.getItem()).getPrecio()*item.getQuantity());
        }
    
    }
    
    return price;
    
    }
    
    public boolean findSimilar(Item item){
    
    if(getCart().getCart()!=null){
    
    
    for(CartItem cartItem:getCart().getCart()){
    
        if(cartItem.getItem().equals(item)){
        
            return true;
        
        }
    
    }
    
    }
    return false;
    }
    
    
    public CartInterface getCart(){
    
        return orderFacade.getCart();
    
    }
    
    public void completePurchase(){
    
    orderFacade.completePurchase();
    
    }
    
    
    private Clases.CartControllerLayout checkExistence(java.util.List<Clases.CartControllerLayout>list,Entities.Sucursal sucursal){
    
        if(list!=null && !list.isEmpty()){
        
        for(Clases.CartControllerLayout aux:list){
        
            if(aux.getSucursal().equals(sucursal)){
            
                return aux;
            
            }
        
        }
        
        }
        
    return null;
    
    }
    
    
    
    public java.util.List<Clases.CartControllerLayout>getCartBySucursal(){
    
        try{
            
            java.util.List<Clases.CartControllerLayout>result=new java.util.ArrayList<Clases.CartControllerLayout>();
        
            for(SessionClasses.CartItem item: this.getCart().getCart()){
            
           if(checkExistence(result,item.getSucursal())!=null){
           
               checkExistence(result,item.getSucursal()).getCartList().add(item);
           
           }
           else{
           
               Clases.CartControllerLayout aux=new Clases.CartControllerImplementation();
               
               aux.getCartList().add(item);
               
               aux.setSucursal(item.getSucursal());
               
               result.add(aux);
               
           }
            
            }
        return result;
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
}
