package Controllers;

public interface CategorySingletonController {

	java.util.List<Entities.Categoria> getCategories();
}