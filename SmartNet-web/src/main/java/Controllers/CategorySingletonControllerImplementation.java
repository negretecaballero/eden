package Controllers;

import CDIBeans.*;

@javax.enterprise.context.ApplicationScoped
@javax.faces.bean.ManagedBean(eager=true)
public class CategorySingletonControllerImplementation implements CategorySingletonController {

	@javax.ejb.EJB
	private Singleton.CategorySingletonFacade _categorySingletonFacade;
	@javax.inject.Inject
	@CDIBeans.CategoryControllerQualifier
	private CategoryControllerInterface _categoryController;

	@javax.annotation.PostConstruct
	public void init() {
		
           initialize();
            
            
	}

	@Override
	public java.util.List<Entities.Categoria> getCategories() {
            
            if(_categorySingletonFacade.getCategories()==null || _categorySingletonFacade.getCategories().isEmpty()){
            
                initialize();
            
            }
            
		return _categorySingletonFacade.getCategories();
	}

	private void initialize() {
		
            if(this._categorySingletonFacade.getCategories()==null || this._categorySingletonFacade.getCategories().isEmpty()){
            
                 try{
            
                for(Entities.Categoria category:_categoryController.findAll()){
                
                    
                    
                _categorySingletonFacade.getCategories().add(category);
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
            }
            
	}
        
        
        
        
}