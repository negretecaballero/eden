/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
public interface CitySessionController {
    
    void initList(Entities.State state);
    
    java.util.List<Entities.City> getCityList();

	Entities.City getCity();

	/**
	 * 
	 * @param state
	 */
	java.util.List<Entities.City> getCities(Entities.State state);
    
}
