/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import javax.transaction.Transactional.TxType;
import java.io.*;

/**
 *
 * @author luisnegrete
 */

//@javax.enterprise.inject.Alternative
@javax.enterprise.inject.Alternative
@javax.transaction.Transactional(javax.transaction.Transactional.TxType.REQUIRED)
@javax.enterprise.context.SessionScoped
@javax.inject.Named("citySessionController")

public class CitySessionControllerImplementation implements java.io.Serializable, CitySessionController{
    
    @javax.ejb.EJB
    private SessionBeans.CitySessionFacadeLocal _citySessionFacade;
	@javax.inject.Inject
	private StateSingletonControllerImplementation stateController;
    
    @Override
    public void initList(Entities.State state){

        java.util.List<Entities.City>cities=(java.util.List<Entities.City>)state.getCityCollection();

        _citySessionFacade.setCityList(cities);
    
    }
    
    
    @Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public java.util.List<Entities.City> getCityList(){
    
        try{
            
            java.util.List<Entities.City>cities=_citySessionFacade.getCityList();
            
          
          
            return cities;
            
        }
        catch(Exception ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }
    
    }

	@Override
	public Entities.City getCity() {
		
            try{
            
               if(this.stateController.getStateList() != null && !this.stateController.getStateList().isEmpty()){
               
                  if(this.stateController.getStateList().get(0).getCityCollection()!=null && !this.stateController.getStateList().get(0).getCityCollection().isEmpty()){
                  
                      return((java.util.List<Entities.City>)this.stateController.getStateList().get(0).getCityCollection()).get(0);
                      
                    }
               
               }
             
                return null;
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
                return null;
                
            }
            
            
	}

	/**
	 * 
	 * @param state
         * @return 
	 */
	@Override
	public java.util.List<Entities.City> getCities(Entities.State state) {
		
            try{
            
                if(state!= null && !state.getCityCollection().isEmpty() && state.getCityCollection()!=null){
                
                    return (java.util.List)state.getCityCollection();
                   
                }
            
                
                return null;
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
            
	}
    
}
