package Controllers;

@javax.enterprise.context.ApplicationScoped
@javax.faces.bean.ManagedBean(eager=true)
@javax.enterprise.inject.Alternative
public class Class implements AdditionSingletonController {

	@javax.ejb.EJB
	private Singleton.AdditionSingletonFacade additionSingletonFacade;
	@javax.inject.Inject
	private CDIBeans.AdditionController additionController;

	@javax.annotation.PostConstruct
	public void init() {
		
            try{
            
                if(additionSingletonFacade.getAdditions()==null || additionSingletonFacade.getAdditions().isEmpty()){
                
                   update();
                
                }
                
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	@Override
	public void update() {
	
            try{
            
                if(additionController.findAll()!=null && !additionController.findAll().isEmpty()){
                
                SessionClasses.EdenList<Entities.Addition>additions=new SessionClasses.EdenList<Entities.Addition>();
                
                for(Entities.Addition addition:additions){
                
                additions.addItem(addition);
                
                }
                
                additionSingletonFacade.setAdditions(additions);
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	@Override
	public SessionClasses.EdenList<Entities.Addition> getAdditions() {
		return this.additionSingletonFacade.getAdditions();
	}
}