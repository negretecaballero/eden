/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.SessionScoped
public class CountryCodesSingletonControllerImplementation implements CountryCodesSingletonController, java.io.Serializable {
    
    @javax.ejb.EJB
    private Singleton.CountryCodeSingleton _countryCodeSingleton;
    
    @Override
    public java.util.HashMap<String,String>getCodeMap(){
    
    return _countryCodeSingleton.getCodes();
    
    }
    
    @Override
    public void printAll(){
    
        _countryCodeSingleton.printAll();
        
    }
    
}
