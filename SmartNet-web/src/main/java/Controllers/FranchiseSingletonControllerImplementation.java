/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.ApplicationScoped

@javax.faces.bean.ManagedBean(eager=true)

//@javax.enterprise.inject.Alternative

public class FranchiseSingletonControllerImplementation implements FranchiseSingletonController{
    
    @javax.ejb.EJB
    private SessionBeans.FranchiseFacadeSingleton _franchiseFacadeSingleton;
    
    @javax.inject.Inject 
    @CDIBeans.FranchiseControllerQualifier
    private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
    try{
    
        System.out.print("Initializing Franchise Singleton Controller");
        
        this.update();
        
        
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    public void update(){
    
    try{
    
        if(this._franchiseFacadeSingleton.getFranchises()==null || this._franchiseFacadeSingleton.getFranchises().size()!=this._franchiseController.findAll().size())
        {
        
            this._franchiseFacadeSingleton.setFranchises(new java.util.ArrayList<Entities.Franquicia>());
        
            if(_franchiseController.findAll()!=null && !_franchiseController.findAll().isEmpty()){
            
                for(Entities.Franquicia franchise:_franchiseController.findAll()){
                
                    this._franchiseFacadeSingleton.getFranchises().add(franchise);
                
                }
            
                System.out.print("FRANCHISES ADDED "+this._franchiseFacadeSingleton.getFranchises().size());
                
            }
            
        }
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    @Override
    public java.util.List<Entities.Franquicia>getFranchises(){
    
    return this._franchiseFacadeSingleton.getFranchises();
    
    }
    
}
