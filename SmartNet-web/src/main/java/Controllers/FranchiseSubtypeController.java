/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseSubtypeController {
    
    void updateSubtypeArray(Entities.Tipo type);
    
    java.util.Vector<Entities.Subtype>getSubtypeArray();
    
}
