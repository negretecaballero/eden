/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Entities.Subtype;
import java.util.Vector;
import java.io.*;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.SessionScoped
@javax.inject.Named("franchiseSubtypeController")
public class FranchiseSubtypeControllerImplentation implements java.io.Serializable, FranchiseSubtypeController {

    @javax.ejb.EJB
    private SessionBeans.FranchiseSubtypeFacadeLocal franchiseSubtypeFacade;
	/*@javax.inject.Inject
	@CDIBeans.SubtypeControllerQualifier
	private CDIBeans.SubtypeControllerImplementation attribute;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private CDIBeans.SubtypeControllerImplementation attribute2;*/
	@javax.inject.Inject
	@CDIBeans.SubtypeControllerQualifier
	private CDIBeans.SubtypeControllerImplementation _subtypeController;
	
    
    @Override
    public void updateSubtypeArray(Entities.Tipo type){
    
        try{
            
            System.out.print("Type to find "+type.getNombre());
        
            if(type.getSubtypeCollection()!=null && !type.getSubtypeCollection().isEmpty()){
            
                
                if(franchiseSubtypeFacade.getSubtypeArray()!=null && !franchiseSubtypeFacade.getSubtypeArray().isEmpty()){
              
                    franchiseSubtypeFacade.getSubtypeArray().clear();
              
                }
                java.util.Vector<Entities.Subtype>results=new java.util.Vector<Entities.Subtype>(type.getSubtypeCollection().size());
                
                
                for(Entities.Subtype subtype:type.getSubtypeCollection()){
                
                    results.add(subtype);
                
                }
                
                this.franchiseSubtypeFacade.setSubtypeArray(results);
                
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }

    @Override
    @javax.transaction.Transactional
    public Vector<Subtype> getSubtypeArray() {
        
        if(this.franchiseSubtypeFacade.getSubtypeArray()==null || this.franchiseSubtypeFacade.getSubtypeArray().isEmpty()){
        
                java.util.Vector<Entities.Subtype>array=new java.util.Vector<Entities.Subtype>(this._subtypeController.findAll().size());
             
        
                for(Entities.Subtype subtype:this._subtypeController.findAll()){
                
                array.add(subtype);
                
                }
                
                this.franchiseSubtypeFacade.setSubtypeArray(array);
        
        }
     
        return this.franchiseSubtypeFacade.getSubtypeArray();
    
    }
    
}
