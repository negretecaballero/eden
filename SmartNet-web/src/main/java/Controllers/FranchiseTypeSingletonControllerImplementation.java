/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.ApplicationScoped
@javax.inject.Named("franchiseTypeSingletonController")
//@javax.enterprise.inject.Alternative
public class FranchiseTypeSingletonControllerImplementation implements FranchiseTypeSingletonController{
    
    @javax.ejb.EJB
    private Singleton.FranchiseTypeSingletonFacadeLocal _franchiseTypeSingletonFacade;
    
    @javax.inject.Inject  
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoController _typeController;
    
    @Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public java.util.Vector<Entities.Tipo>getFranchiseTypeArray(){
    
        if(_franchiseTypeSingletonFacade.getTypeArray()==null || _franchiseTypeSingletonFacade.getTypeArray().size()!=_typeController.findAll().size()){
        
            _franchiseTypeSingletonFacade.setTypeArray(_typeController.findAll());
        
        }
        
        return _franchiseTypeSingletonFacade.getTypeArray();
    
    }
    
}
