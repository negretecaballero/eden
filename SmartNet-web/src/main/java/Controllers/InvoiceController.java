/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import SessionBeans.InvoiceFacadeLocal;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.SessionScoped

public class InvoiceController implements Serializable,InvoiceControllerInterface{

    /**
     * Creates a new instance of InvoiceController
     */
    public InvoiceController() {
    }
    
 
    
    @EJB
    private InvoiceFacadeLocal invoiceFacade;
    @Override
    public void localTesting()
    {
    
        System.out.print("Local testing");
    
    }   
    
    @Override
       public void test(){
    
    this.invoiceFacade.test();
    
    }
       
       @Override
       public void setFlag(String flag){
       
       this.invoiceFacade.setFlag(flag);
       
       }
       @Override
       public String getFlag(){
       
       return this.invoiceFacade.getFlag();
       
       }
    
     @Override
    public java.util.List<SessionClasses.Invoice>getInvoices(){
    
    return invoiceFacade.getInvoices();
    
    }
    
    @Override
    public void setInvoiceList(java.util.List<SessionClasses.Invoice>invoices){
    
    this.invoiceFacade.equals(invoices);
    
    }
    
    @Override
    public void addToInvoices(SessionClasses.Invoice invoice){
    
        this.getInvoices().add(invoice);
    
    }
    
   @Override
    public void reloadInvoices(java.util.List<Entities.Pedido>orders){
    
        try{
        
            System.out.print("Reloading Invoices");
             this.invoiceFacade.test();
            
            if(this.getInvoices()!=null && !this.getInvoices().isEmpty())
            {
                
                this.getInvoices().clear();
            
            }
       for(Entities.Pedido pedido:orders){
       
        System.out.print("Order Status "+pedido.getPedidoStatestateId().getState());
        
        System.out.print("Order Date "+pedido.getDate());
           
       SessionClasses.Invoice invoice=new SessionClasses.InvoiceImpl();
       
       if(pedido.getProductohasPEDIDOCollection()!=null && !pedido.getProductohasPEDIDOCollection().isEmpty())
       {
       
           for(Entities.ProductohasPEDIDO productoHasPedido:pedido.getProductohasPEDIDOCollection()){
           
               SessionClasses.InvoiceItem invoiceItem=new SessionClasses.InvoiceItemImpl();
               
               invoiceItem.setDescription(productoHasPedido.getProducto().getNombre());
           
               invoiceItem.setQuantity(productoHasPedido.getQuantity());
               
               invoiceItem.setValue(productoHasPedido.getProducto().getPrecio());
               
               
               invoice.getItems().add(invoiceItem);
           }
       
       }
       
       if(pedido.getMenuhasPEDIDOCollection()!=null && !pedido.getMenuhasPEDIDOCollection().isEmpty())
       {
       for(Entities.MenuhasPEDIDO menuHasPedido:pedido.getMenuhasPEDIDOCollection()){
       
       SessionClasses.InvoiceItem item=new SessionClasses.InvoiceItemImpl();
       
       item.setDescription(menuHasPedido.getMenu().getNombre());
       
       item.setQuantity(menuHasPedido.getQuantity());
       
       item.setValue(menuHasPedido.getMenu().getPrecio());
       
       invoice.getItems().add(item);
       
       }
       }  
       
       invoice.setDate(pedido.getDate());
       
        this.getInvoices().add(invoice);
       }
   
        }
        catch(Exception ex){
        
            Logger.getLogger(InvoiceController.class.getName()).log(Level.SEVERE,null,ex);
            
        }
    
    }
    @Override
    public java.util.List<SessionClasses.Invoice>getByDate(java.util.Date date){
    
    try{
    
        Clases.EdenDateInterface edenDate=new Clases.EdenDate();
        
        java.util.List<SessionClasses.Invoice>resultInvoices=new java.util.ArrayList<SessionClasses.Invoice>();
    
        System.out.print("Invoices to check "+this.getInvoices().size());
        
        for(SessionClasses.Invoice invoice:this.getInvoices()){
        
          
            
          if(edenDate.compareSingleDate(date, invoice.getDate()))  
          {
              System.out.print("Invoice date "+invoice.getDate());
              
              for(SessionClasses.InvoiceItem invoiceItem: invoice.getItems()){
              
                     System.out.print(invoiceItem.getQuantity()+"-"+invoiceItem.getDescription()+"-"+invoiceItem.getValue());
            
              
              }
              
              resultInvoices.add(invoice);
          
          }
        }
      
        System.out.print("Result Invoices "+resultInvoices);
        
        this.invoiceFacade.setInvoices(resultInvoices);
        
      return resultInvoices;  
    }
    catch(Exception ex)
    {
    
        Logger.getLogger(InvoiceController.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }    
    }
    
   
    
}
