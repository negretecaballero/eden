/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
public interface InvoiceControllerInterface {
    
    void test();
    
    void localTesting();
    
    java.util.List<SessionClasses.Invoice>getInvoices();
    
    void addToInvoices(SessionClasses.Invoice invoice);
    
    void reloadInvoices(java.util.List<Entities.Pedido>orders);
    
    java.util.List<SessionClasses.Invoice>getByDate(java.util.Date date);
    
    void setInvoiceList(java.util.List<SessionClasses.Invoice>invoices);
    
    void setFlag(String flag);
    
    String getFlag();
}
