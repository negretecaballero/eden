/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
public interface MailSingletonController {

	/**
	 * 
	 * @param destinatary
	 * @param subject
	 * @param text
	 */
	boolean sendMail(String destinatary, String subject, String text);
    
   // SessionEnum.MailConfirmation sendMail()
    
}
