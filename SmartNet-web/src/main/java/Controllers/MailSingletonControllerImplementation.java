package Controllers;

@javax.enterprise.context.ApplicationScoped

public class MailSingletonControllerImplementation implements MailSingletonController,java.io.Serializable {

	/**
	 * 
	 * @param destinatary
	 * @param subject
	 * @param text
     * @return 
	 */
	@javax.transaction.Transactional(javax.transaction.Transactional.TxType.REQUIRES_NEW)
	@Override
	public boolean sendMail(String destinatary, String subject, String text) {
	
            try{
            
                switch(this.mailFacade.sendMessage(destinatary, subject, text)){
                
                    case SUCCESS:{
                    
                        return true;
                    
                    }
                    
                    case ERROR:{
                    
                        return false;
                    
                    }
                    
                    default:{
                    
                        return false;
                    
                    }
                
                }
                
               
            
            }
            catch(NullPointerException ex){
            
                return false;
            
            }
            
	}

	@javax.ejb.EJB
	private SessionBeans.MailFacadeLocal mailFacade;

}