package Controllers;
@javax.enterprise.context.ApplicationScoped
@javax.faces.bean.ManagedBean(eager=true)
public class MenuSingletonControllerImplementation implements MenuSingletonController {

	@javax.ejb.EJB
	private Singleton.MenuSingletonFacade menuSingletonFacade;
	@javax.ejb.EJB
	private jaxrs.service.MenuFacadeREST menuFacadeREST;

	public MenuSingletonControllerImplementation() {
		
        
            
	}
        
        @javax.annotation.PostConstruct
        public void init(){
        
            try{
            
                if(this.menuSingletonFacade.getMenuList().isEmpty()){
                
                    update();
                    
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
        
        }

	@Override public
	void update() {
		 try{
            
              if(this.menuFacadeREST.findAll()!=null && !this.menuFacadeREST.findAll().isEmpty()){
              
                
              
                  this.menuSingletonFacade.setMenuList(this.menuFacadeREST.findAll());
              
              }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
	}

	@Override
	public java.util.List<Entities.Menu> getMenuList() {
	
            return this.menuSingletonFacade.getMenuList();
            
	}
}