/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
public interface NewsController {
    
    java.util.List<Entities.News>getNews();
    
    void addPieceNews(Entities.News news);
    
    void init(int trim);
    
}
