/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import javax.inject.Named;
import java.io.*;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.SessionScoped
@Named("newsControllerImpl")
public class NewsControllerImpl implements java.io.Serializable, NewsController{
    
    @javax.inject.Inject 
    @CDIBeans.NewsControllerQualifier
    private transient CDIBeans.NewsController _newsController;
    
    @javax.ejb.EJB
    private SessionBeans.NewsFacadeLocal newsFacade;
    
    @Override
    public java.util.List<Entities.News>getNews(){
    
      return  this.newsFacade.getNewsList();
    
    }
    
    @Override
    public void addPieceNews(Entities.News news){
    
        this.newsFacade.getNewsList().add(news);
    
    }
    
    @Override
    public void init(int trim){
        
        if(!this.newsFacade.getInitialized())
        {
        for(Entities.News news:_newsController.getOrderedByDate(trim)){
                
            this.addPieceNews(news);
            
        }
        }
        
        this.newsFacade.setInitialized(true);
    }
    
}
