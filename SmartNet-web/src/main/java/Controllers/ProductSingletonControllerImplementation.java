package Controllers;

@javax.enterprise.context.ApplicationScoped
@javax.faces.bean.ManagedBean(eager=true)
//@javax.enterprise.inject.Alternative
public class ProductSingletonControllerImplementation implements ProductSingletonController {

	@javax.annotation.PostConstruct
	public void init() {
		
            try{
            
                if(productSingleton.getProducts()==null || productSingleton.getProducts().isEmpty()){
                
                    if(productController.findAll()!=null && !productController.findAll().isEmpty()){
                    
                        for(Entities.Producto product:productController.findAll()){
                        
                            productSingleton.getProducts().addItem(product);
                        
                        }
                    
                    }
                
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	@Override
	public SessionClasses.EdenList<Entities.Producto> getProducts() {
		
            return this.productSingleton.getProducts();
            
	}

	@javax.ejb.EJB
	private Singleton.ProductSingletonFacade productSingleton;
	@javax.inject.Inject
        @CDIBeans.ProductoControllerQualifier
	private CDIBeans.ProductoControllerInterface productController;
}