/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@Stereotypes.ApplicationStereotype
@javax.inject.Named("professionSingletonController")
public class ProfessionSingletonControllerImpementation implements ProfessionSingletonController{
    
   @javax.ejb.EJB
   private Singleton.ProfessionSingletonFacadeLocal _professionSingletonFacade;
   
   @javax.ejb.EJB
   private jaxrs.service.ProfessionFacadeREST _professionFacadeREST;
   
   @Override
   public java.util.List<Entities.Profession>getProfessions(){
   
       initProfessions();
          
       java.util.List<Entities.Profession>list = IteratorUtils.toList(this._professionSingletonFacade.getProfessionCollection().iterator());
       
       if(list!=null && !list.isEmpty()){
       
           for(Entities.Profession profession : list){
           
               System.out.print("Profession "+profession.getProfession());
           
           }
       
       }
       
       return IteratorUtils.toList(this._professionSingletonFacade.getProfessionCollection().iterator());

   
   }
   
   @Override
   public void initProfessions(){
   
       try{
       
           if(this._professionSingletonFacade.getProfessionCollection().size()!=_professionFacadeREST.findAll().size()){
           
               for(Entities.Profession profession:_professionFacadeREST.findAll()){
               
               if(!_professionSingletonFacade.getProfessionCollection().constains(profession)){
               
                  _professionSingletonFacade.getProfessionCollection().addItem(profession);
               
               }
               
               }
           
           }
       }
       catch(javax.ejb.EJBException | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           
       
       }
   
   }
    
}
