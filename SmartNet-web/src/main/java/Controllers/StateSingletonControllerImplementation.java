/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
@javax.faces.bean.ManagedBean(eager=true)
@javax.enterprise.context.ApplicationScoped
@javax.inject.Named("stateSingletonController")
public class StateSingletonControllerImplementation implements StateSingletonController{
    
    @javax.ejb.EJB
    private Singleton.StateSingletonFacadeLocal _stateSingletonFacade;
    
    @javax.ejb.EJB
    private jaxrs.service.StateFacadeREST _stateFacadeREST;
    
    @javax.inject.Inject 
    @CDIBeans.StateControllerQualifier
    private CDIBeans.StateControllerInterface _stateController;
    
    
    @Override
    public java.util.List<Entities.State>getStateList(){
    
        if(_stateSingletonFacade.getStateList()==null || _stateSingletonFacade.getStateList().isEmpty() || (_stateSingletonFacade.getStateList().size()!=_stateFacadeREST.findAll().size())){
        
            _stateSingletonFacade.setStateList(_stateController.findAll());
        
        }
        
        return _stateSingletonFacade.getStateList();
    
    }
    
}
