/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.ApplicationScoped
@javax.inject.Named("supplierProductTypeSingletonController")

public class SupplierProductTypeSingletonControllerImplementation implements SupplierProductTypeSingletonController{
    
    @javax.ejb.EJB
    private Singleton.SupplierProductTypeSingletonFacadeLocal _supplierProductTypeSingletonFacade;
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierProductTypeFacadeREST supplierProductTypeFacadeREST;
    
    
    @Override
    
    public java.util.List<Entities.SupplierProductType>getSupplierProductTypeList(){
    
        if(this._supplierProductTypeSingletonFacade.getSupplierProductTypeList()==null || this._supplierProductTypeSingletonFacade.getSupplierProductTypeList().isEmpty() || (this.supplierProductTypeFacadeREST.findAll().size()!=this._supplierProductTypeSingletonFacade.getSupplierProductTypeList().size())){
        
            this._supplierProductTypeSingletonFacade.setSupplierProductTypeList(this.supplierProductTypeFacadeREST.findAll());
        
        }
        
        return _supplierProductTypeSingletonFacade.getSupplierProductTypeList();
    
    }
    
}
