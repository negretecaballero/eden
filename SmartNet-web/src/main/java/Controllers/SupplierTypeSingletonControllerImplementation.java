/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.ApplicationScoped
@javax.inject.Named("supplierTypeSingletonController")

public class SupplierTypeSingletonControllerImplementation implements SupplierTypeSingletonController {

    @javax.ejb.EJB
    private Singleton.SupplierTypeSingletonFacadeLocal _supplierTypeSingletonFacade;
    
    @javax.ejb.EJB
    private jaxrs.service.SupplierTypeFacadeREST _supplierTypeFacadeREST;
    
    @Override
    public java.util.List<Entities.SupplierType>getSupplierTypeList(){
    
        if(_supplierTypeSingletonFacade.getSupplierTypeList()==null || _supplierTypeSingletonFacade.getSupplierTypeList().isEmpty() || (_supplierTypeSingletonFacade.getSupplierTypeList().size()!=this._supplierTypeFacadeREST.findAll().size())){
                
              _supplierTypeSingletonFacade.setSupplierTypeList(this._supplierTypeFacadeREST.findAll());
        
        }
        
    return _supplierTypeSingletonFacade.getSupplierTypeList();
    
    }
  
    
}
