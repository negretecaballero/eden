/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author luisnegrete
 */

@FacesConverter("additionPKConverter")

public class AdditionPKConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
    
    System.out.print("AdditionPK get as object "+string);
    
    String [] keyItem=string.split(",");
    
    if(keyItem.length==2){
    
        Entities.AdditionPK additionPK=new  Entities.AdditionPK();
    
        additionPK.setIdaddition(new java.lang.Integer(keyItem[0]));
        
        additionPK.setFranquiciaIdfranquicia(new java.lang.Integer(keyItem[1]));
        
         
        System.out.print("Get As Object return "+additionPK);
        
        return additionPK;
        
    }
    
    else{
    
        return string;
    
    }
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    
    System.out.print("Addition PK get as stirng "+o);
    
    if(o instanceof Entities.AdditionPK){
    
        Entities.AdditionPK additionPK=(Entities.AdditionPK)o;
        
        return additionPK.getIdaddition() +","+additionPK.getFranquiciaIdfranquicia();
    
    }
    
    else if(o.toString().split(",").length==2){
    
    return o.toString();
    
    }
    else{
    
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Exception Caugth");
        
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
        
        throw new ConverterException(msg);
    
    }
    
    }
    
}
