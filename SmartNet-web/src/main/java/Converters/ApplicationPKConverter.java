/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.*;

/**
 *
 * @author luisnegrete
 */
@javax.faces.convert.FacesConverter("ApplicationConverter")
public class ApplicationPKConverter implements javax.faces.convert.Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
  
        if(string.split(",").length==2)
        {
        
            Entities.ApplicationPK key=new Entities.ApplicationPK();
            
            key.setCedula(new java.lang.Long(string.split(",")[0]));
            
            key.setNit(string.split(",")[1]);
        
            return key;
        }
        else{
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Converter Exception");
        
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            throw new javax.faces.convert.ConverterException(msg);
            
        }
    }
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
       
        if(o instanceof Entities.ApplicationPK){
        
            Entities.ApplicationPK key=(Entities.ApplicationPK)o;
            
            return key.getCedula()+","+key.getNit();
        
        }
        else if(o.toString().split(",").length==2){
        
            return o.toString();
        
        }
    
        else{
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Converter Exception");
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            throw new javax.faces.convert.ConverterException(msg);
        
        }
    }
    
}
