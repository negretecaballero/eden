/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import Entities.CategoriaPK;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author luisnegrete
 */


@FacesConverter("CategoriaConverter")

public class CategoriaPKConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        
    System.out.print("You are in");
           
    System.out.print("CategoriaPK converter Get As Object "+string);
        
    System.out.print("Before split");
   
    CategoriaPK categoriaPK=new CategoriaPK();
      
    String [] values=string.split(",");
    
    System.out.print("After split");
    
    System.out.print("Converter Array Size "+values.length);
    
    if(values.length==2){
    
    categoriaPK.setFranquiciaIdfranquicia(Integer.parseInt(values[0]));
    
    categoriaPK.setIdcategoria(Integer.parseInt(values[1]));
    
    System.out.print("CategoriaPK converter Get As Object get As Object "+categoriaPK);
     
    return categoriaPK;
   
    }
    
    else{
    
    javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Converter Exception Caugth");
        
    throw new javax.faces.convert.ConverterException(msg);
    
    }
    
    
   
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        
         try{
             
             System.out.print("You are in");
        
        System.out.print("Categoria Raw Object Value getAsString "+o);
        
        if(o instanceof Entities.CategoriaPK){
             
        CategoriaPK categoriaPK=(CategoriaPK)o;
        
        System.out.print("Categoria Obj Value "+categoriaPK);
        
        String result=categoriaPK.getFranquiciaIdfranquicia()+","+categoriaPK.getIdcategoria();
    
        System.out.print("CategoriaPK converter Get As String "+result);
        
         return result;
        
        }
        else
        {
        
            System.out.print("Object to String "+o.toString());
            
            return o.toString();
        
        }
           
       
        
    }
         
    catch(Exception ex){
    
        
        FacesMessage msg=new FacesMessage(null,"Invalid CategoriaPK Object");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        throw new ConverterException(msg);
    
    }
    
    
    }
    
}
