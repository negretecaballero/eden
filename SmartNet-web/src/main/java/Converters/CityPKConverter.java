/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import Entities.CityPK;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author luisnegrete
 */
@FacesConverter("CityPKConverter")
public class CityPKConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
    
        String []results=string.split(",");
        
        if(results.length==2){
        
          CityPK citypk=new CityPK(); 
          
          citypk.setSTATEidSTATE(Integer.parseInt(results[0]));
          
          citypk.setIdCITY(Integer.parseInt(results[1]));
          
          return citypk;
        
        }
        
        else{
        
        FacesMessage msg=new FacesMessage("You are Hacking Bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        throw new ConverterException(msg);
        
       
        }
            
            
            
    
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
      //  throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
        System.out.print("Object Value: "+o.toString());
        
    if(o instanceof CityPK){
    
        
    CityPK citypk=(CityPK)o;
        
    String result=citypk.getSTATEidSTATE()+","+citypk.getIdCITY();
            
    return result;
    
    }
    
    else if(o.toString().split(",").length==2){
    
        System.out.print("OBJECT TO STRING VALUE "+o.toString());
        
         return o.toString();
    
    }
    
    else{
    
    FacesMessage msg=new FacesMessage("You are HAcking");
    
    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
    
    throw new ConverterException(msg);
   
    }
    
    }
    
}
