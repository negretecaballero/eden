/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.*;

/**
 *
 * @author luisnegrete
 */

@javax.faces.convert.FacesConverter("DateConverter")
public class DateConverter implements javax.faces.convert.Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
  
        if(string.split("-").length==3){
        
            String []splits=string.split("-");
            
            java.util.Date date=new java.util.Date();
            
            date.setDate(Integer.parseInt(splits[0]));
            
            date.setMonth(Integer.parseInt(splits[1]));
            
            date.setYear(Integer.parseInt(splits[2]));
            
            return date;
        
        }
        else{
        
            javax.faces.application.FacesMessage msg= new javax.faces.application.FacesMessage("Wrong Date Type");
        
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            throw new javax.faces.convert.ConverterException(msg);
            
        }
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
  
    if(o instanceof java.util.Date){
    
       java.util.Date date=(java.util.Date)o;
       
       return date.getDate()+"-"+date.getMonth()+"-"+date.getYear();
    
    }
    else{
    
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("This is not a Date Object");
        
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
        
        throw new javax.faces.convert.ConverterException(msg);
    
    }
    
    }
    
}
