/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.*;

/**
 *
 * @author luisnegrete
 */
@javax.faces.convert.FacesConverter("ProductPKConverter")
public class ProductPKConverter implements javax.faces.convert.Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
  
    try{
    
        String[]splits=string.split(",");
        
        System.out.print("Product get as string");
        
        if(splits.length==3){
        
            Entities.ProductoPK productPK=new Entities.ProductoPK();
            
            productPK.setIdproducto(new java.lang.Integer(splits[0]));
            
            productPK.setCategoriaIdcategoria(new java.lang.Integer(splits[1]));
            
            productPK.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(splits[2]));
            
            System.out.print("Returning "+productPK);
            
            return productPK;
        
        }
        else{
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("there is an error with the input");
            
            throw new javax.faces.convert.ConverterException(msg);
        
        }
        
    }
    catch(NumberFormatException | ConverterException ex){
    
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Converter Exception Caugth");
        
        throw new javax.faces.convert.ConverterException(msg);
    
    }
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
    
    try{
         System.out.print("Producto Converter get As String "+o);
        if(o instanceof Entities.ProductoPK){
        
       
            
           Entities.ProductoPK productPK=(Entities.ProductoPK)o;
           
           return productPK.getIdproducto()+","+productPK.getCategoriaIdcategoria()+","+productPK.getCategoriaFranquiciaIdfranquicia();
            
        }
        else{
        
           
            return o.toString();
        
        }
    
    }
    catch(IllegalArgumentException | javax.faces.convert.ConverterException | NullPointerException ex){
    
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Exception Caugth");
        
        throw new javax.faces.convert.ConverterException(msg);
        
    }
    
    }
    
}
