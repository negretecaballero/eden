/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author luisnegrete
 */
@FacesConverter("ProfessionConverter")
public class ProfessionConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
  
        System.out.print("Profession Converter Get As Object "+string);
        
        
        return string;
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        
         System.out.print("Profession Converter Get As String "+o);
   
    return o.toString();
    
    }
    
}
