/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.*;

/**
 *
 * @author luisnegrete
 */

@javax.faces.convert.FacesConverter("SubtypePKConverter")

public class SubtypePKConverter implements javax.faces.convert.Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
   
        try{
        
            System.out.print("SubtypePK converter");
            
            System.out.print("Get as obhect string to split "+string);
            
            String[] split=string.split(",");
            
            if(split.length==2){
            
                Entities.SubtypePK subtypePK=new Entities.SubtypePK();
                
                System.out.print("Position 0 "+split[0]);
                
                subtypePK.setIdSubtype(new java.lang.Integer(split[0]));
                
                subtypePK.setTipoIdtipo(new java.lang.Integer(split[1]));
                
                System.out.print("Subtype Converter get as object "+subtypePK);
                
                return subtypePK;
                
            }
            else{
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage();
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                throw new javax.faces.convert.ConverterException(msg);
            
            }
        
        }
        catch(NumberFormatException | javax.faces.convert.ConverterException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
   try
        {
        if(o instanceof Entities.SubtypePK){
        
            Entities.SubtypePK subtypePK=(Entities.SubtypePK)o;
            
            System.out.print("Subtype get as string "+subtypePK.getIdSubtype()+","+subtypePK.getTipoIdtipo());
            
            return (subtypePK.getIdSubtype()+","+subtypePK.getTipoIdtipo());
        
        }
        else if(o.toString().split(",").length==2){
        
            System.out.print("Subtype get as string "+o.toString());
            
            return o.toString();
        
        }
        
        else{
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Wrong data type");
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            throw new javax.faces.convert.ConverterException(msg);
        
        }
        
    }
        catch(javax.faces.convert.ConverterException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
    
}
