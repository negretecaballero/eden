/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Converters;

import Entities.InventarioPK;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author luisnegrete
 */

@FacesConverter("inventarioConverter")
public class inventarioPKConverter implements Converter{

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
 
  try{
  
  InventarioPK inventarioPK=new InventarioPK();
  
  
  System.out.print("Inventario Converter Get As Object "+string);
      
      String [] results=string.split(",");
      
      if(results.length==2){
      
      inventarioPK.setFranquiciaIdFranquicia(Integer.parseInt(results[0]));
      
      inventarioPK.setIdinventario(Integer.parseInt(results[1]));
      
      }
      
      else{
      
     throw new javax.faces.convert.ConverterException();
          
      }
      
      
      
     
  
  return inventarioPK;
  }
  
  catch(Exception ex){
  
      FacesMessage msg=new FacesMessage("The Field cannot be Converted to InventarioPK");
      
      msg.setSeverity(FacesMessage.SEVERITY_ERROR);
      
      throw new ConverterException(msg);
  
  }
   
    
    
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        
        try{
            
            if(o instanceof InventarioPK)
            {
    System.out.print("Inventario Converter GetAsString "+o);
    
    InventarioPK inventarioPK=(InventarioPK)o;
    
    
    
    String result=inventarioPK.getFranquiciaIdFranquicia()+","+inventarioPK.getIdinventario();
    
    System.out.print("InventarioPK "+result);
    
    
    return result;
    
        }
            else if(o.toString().split(",").length==2){
            
                return o.toString();
            
            }
            
            else{
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Converter Exception Caugth");
            
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                throw new javax.faces.convert.ConverterException(msg);
                
            }
    
        }
        
        catch(Exception ex){
        
            FacesMessage msg=new FacesMessage("Converter Exception caugth");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            throw new ConverterException(msg);
        
        }
    
    
    
    
    }
    
}
