/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecoderEncoder;

import Clases.Message;

/**
 *
 * @author luisnegrete
 */
public class MessageDecoder implements org.primefaces.push.Decoder<String,Clases.Message>{

    @Override
    public Message decode(String u) {
  
    String [] userAndMessage=u.split(":");
    
    if(userAndMessage.length>=2){
    
       Clases.Message msg=new Clases.Message();
       
       msg.setUser(userAndMessage[0]);
       
       msg.setText(userAndMessage[1]);
       
       return msg;
    
    }
    
    else{
    
        return new Clases.Message(u);
    
    }
    
    }
    
}
