/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecoderEncoder;

import Clases.Message;

/**
 *
 * @author luisnegrete
 */
public class MessageEncoder implements org.primefaces.push.Encoder<Clases.Message,String>{

    @Override
    public String encode(Message u) {
   
    return new org.primefaces.json.JSONObject(u).toString();
    
    }
    
}
