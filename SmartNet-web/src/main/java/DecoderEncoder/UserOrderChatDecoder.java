/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecoderEncoder;

import Clases.UserOrderChat;

/**
 *
 * @author luisnegrete
 */
public class UserOrderChatDecoder implements org.primefaces.push.Decoder<String,Clases.UserOrderChat>{

    @Override
    public UserOrderChat decode(String u) {
  
        if(u.split(":").length>=3){
        
        Clases.UserOrderChat aux=new Clases.UserOrderChat();
        
        aux.setUser(u.split(":")[2]);
        
        aux.setContent(u.split(":")[1]);
        
        aux.setTitle(u.split(":")[0]);
        
        return aux;
        
        }
        
    return null;
    
    }
    
}
