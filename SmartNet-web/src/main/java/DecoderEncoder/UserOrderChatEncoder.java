/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DecoderEncoder;

import Clases.UserOrderChat;

/**
 *
 * @author luisnegrete
 */
public class UserOrderChatEncoder implements org.primefaces.push.Encoder<Clases.UserOrderChat,String>{

    @Override
    public String encode(UserOrderChat u) {
  
    return new org.primefaces.json.JSONObject(u).toString();
    
    
    }
    
}
