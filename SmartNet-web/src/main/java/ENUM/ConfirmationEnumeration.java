/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENUM;

/**
 *
 * @author luisnegrete
 */
public enum ConfirmationEnumeration {
  
    CONFIRMED("CONFIRMED"),UNCONFIRMED("UNCONFIRMED");
    
    private final String name;
    
     private ConfirmationEnumeration(String s){

      name=s;

    }
     
      public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    @Override
    public String toString() {
       return this.name;
    }
}
