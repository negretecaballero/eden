/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENUM;

/**
 *
 * @author luisnegrete
 */
public enum MailConfirmationEnum {
   
    SUCCESS("SUCCESS"),ERROR("ERROR");
    
    private final String name;
    
    private MailConfirmationEnum(String s){

   this.name = s;
    
    }
    
    @Override
    public String toString(){
    
        return this.name;
    
    }
    
    
}
