/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENUM;

/**
 *
 * @author luisnegrete
 */
public enum OrderState {
    Pending ("Pending") ,Processing ("Processing"), Delivered("Delivered");
    
     private final String name;       

    private OrderState(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    @Override
    public String toString() {
       return this.name;
    }
}
