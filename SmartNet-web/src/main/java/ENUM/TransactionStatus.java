/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ENUM;

/**
 *
 * @author luisnegrete
 */
public enum TransactionStatus {
    
    APPROVED("APPROVED"), DISAPPROVED("DISAPPROVED");
    
    private final String name;
    
    private TransactionStatus(String s){
    
        this.name=s;
        
    }
    
}
