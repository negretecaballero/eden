/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eden;

/**
 *
 * @author luisnegrete
 */
public class EdenWorkerNoteView extends Clases.BaseBacking{

    /**
     * Creates a new instance of EdenWorkerNoteView
     */
    public EdenWorkerNoteView() {
    }
    
    private boolean _anyField;
    
    public boolean getAnyField(){
    
        return _anyField;
    
    }
    
    public void setAnyField(boolean anyField){
    
    _anyField=anyField;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
       
    
    }
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIEden.EdenWorkerNote _edenWorkerNoteController;
            
    public java.util.List<Entities.NewsType>getNewsTypeList(){
    
        System.out.print("NewstypeList Front End Size "+_edenWorkerNoteController.getNewsTypeList().size());
        
        return _edenWorkerNoteController.getNewsTypeList();
    
    }
    
    public java.util.List<Entities.Tipo>getFieldList(){
    
    return _edenWorkerNoteController.getFieldList();
    
    }
    
    private Entities.News _news;
    
    public Entities.News getNews(){
    
        return _news;
    
    }
    
    public void setNews(Entities.News news){
    
        _news=news;
    
    }
    
    
    public void preRenderView(){
    
    try{
    
    String view=this.getContext().getViewRoot().getViewId();
    
    switch(view){
    
        case "/EdenWorker/add-note.xhtml":{
        
            if(!this.fileUploadBean.getActionType().getActionType().equals(view))
            {
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
            
            this.fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
            
            this.fileUploadBean.setActionType(new Clases.ActionType(view));
            
            }
            
             if(_news==null){
        
            _news=new Entities.News();
            
            _news.setNewsTypeId(new Entities.NewsType());
            
            _news.setTipoId(new Entities.Tipo());
        
        }
        
        if(_news!=null){
        
        if(_news.getTipoId()==null || _news.getNewsTypeId()!=null){
        
            _news.setTipoId(new Entities.Tipo());
            
            _news.setNewsTypeId(new Entities.NewsType());
        
        }
        
        }
            
        break;
        
        }
    
    }
    
    }
    catch(IllegalArgumentException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    public void changeAnySate(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
            System.out.print("Boolean Value "+_anyField);
        
            if(_anyField){
            
            _news.getTipoId().setIdtipo(0);
            }
             
            
        
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
            
        }
    
    }
    
    
    public void saveNote(javax.faces.event.ActionEvent event){
    
        try{
        
            if(_anyField)
           
            {
            
                _news.setTipoId(null);
            
            }
            
            this._edenWorkerNoteController.createNote(_news);
            
            _anyField=false;
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Note Crated Successfully");
        
            this.getContext().addMessage(null,msg);
            
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
}
