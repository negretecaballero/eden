/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eden.controller;

/**
 *
 * @author luisnegrete
 */
public interface EdenWorkerApplicationConfirmationController {
 
    void createSupplier(Entities.SupplierApplication supplierApplication);
    
    Entities.SupplierApplication getSupplierApplicationObject();
    
}
