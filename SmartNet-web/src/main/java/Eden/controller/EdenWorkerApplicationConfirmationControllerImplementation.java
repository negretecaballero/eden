/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Eden.controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class EdenWorkerApplicationConfirmationControllerImplementation implements EdenWorkerApplicationConfirmationController{
 
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
 
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.AppGroupControllerDelegate _appGroupController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierController _supplierController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierApplicationController _supplierApplicationController;
    
    
    private EdenWorker.Create.View.EdenWorkerView edenWorkerView;
    
    @Override
    public Entities.SupplierApplication getSupplierApplicationObject(){
    
    try{
     
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("auxiliarComponent") instanceof Entities.SupplierApplication){
        
            return (Entities.SupplierApplication)session.getAttribute("auxiliarComponent");
        
        }
    return null;
    }
    
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    private EdenWorker.Create.View.EdenWorkerView getEdenWorkerViewBean(){
    
    try{
    
       javax.faces.context.FacesContext fc=javax.faces.context.FacesContext.getCurrentInstance();
       
       return (EdenWorker.Create.View.EdenWorkerView)fc.getApplication().getELResolver().getValue(fc.getELContext(), null, "edenWorkerView");
    }
    catch(Exception ex){
    
        return null;
    
    }
    
    }
    
    
 
    @Override
    public void createSupplier(Entities.SupplierApplication supplierApplication){
    
    try{
    
       System.out.print("Eden Worker View supplierApplication"+this.getEdenWorkerViewBean());
        
        Entities.LoginAdministrador loginAdministrador;
        
        System.out.print("MAIL TO CREATE "+supplierApplication.getMail());
        
        if(!_loginAdministradorController.exists(supplierApplication.getMail())){
        
          System.out.print("THIS USER DOES NOT EXIST"); 
            
          loginAdministrador=new Entities.LoginAdministrador();
          
          loginAdministrador.setUsername(supplierApplication.getMail());
          
          loginAdministrador.setPassword(Clases.EdenString.generatePassword(30));
        
          Entities.AppGroup appGroup=new Entities.AppGroup();
          
          Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
          
          appGroupPK.setGroupid("guanabara_supplier");
          
          appGroupPK.setLoginAdministradorusername(loginAdministrador.getUsername());
          
          appGroup.setAppGroupPK(appGroupPK);
          
          appGroup.setLoginAdministrador(loginAdministrador);
          
          _loginAdministradorController.createAccount(loginAdministrador, appGroup);
          
        }
        
        else{
       
            loginAdministrador=_loginAdministradorController.find(supplierApplication.getMail());
            
            if(_loginAdministradorController.findAppGroup(loginAdministrador, "guanabara_supplier")==null){
            
                System.out.print("THIS APP GROUP DOES NOT EXIST");
                
                Entities.AppGroup appGroup=new Entities.AppGroup();
                
                Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
                
                appGroupPK.setGroupid("guanabara_supplier");
                
                appGroupPK.setLoginAdministradorusername(loginAdministrador.getUsername());
                
                appGroup.setAppGroupPK(appGroupPK);
                
                appGroup.setLoginAdministrador(loginAdministrador);
                
                _appGroupController.create(appGroup);
            
            }
             
        
        }
        
      Entities.Supplier supplier=new Entities.Supplier();
      
      supplier.setIdLoginAdministrador(loginAdministrador);
      
      supplier.setLogo(supplierApplication.getLogo());
      
      supplier.setName(supplierApplication.getName());
      
      supplier.setIdSupplierApplication(supplierApplication);
      
      Entities.SupplierPK supplierPK=new Entities.SupplierPK();
      
      supplierPK.setLoginAdministradorUsername(loginAdministrador.getUsername());
      
      supplier.setSupplierPK(supplierPK);
      
      _supplierController.create(supplier);
      
      if(supplierApplication.getApplicationState()==Entitites.Enum.SupplierApplicationState.pending){
      
          supplierApplication.setApplicationState(Entitites.Enum.SupplierApplicationState.accepted);
          
          _supplierApplicationController.update(supplierApplication);
      
      }
      
      
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
}
