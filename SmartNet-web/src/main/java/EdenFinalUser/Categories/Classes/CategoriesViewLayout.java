package EdenFinalUser.Categories.Classes;

public interface CategoriesViewLayout {

	Entities.Franquicia getFranchise();

	/**
	 * 
	 * @param franchise
	 */
	void setFranchise(Entities.Franquicia franchise);

	String getLogo();

	/**
	 * 
	 * @param logo
	 */
	void setLogo(String logo);
}