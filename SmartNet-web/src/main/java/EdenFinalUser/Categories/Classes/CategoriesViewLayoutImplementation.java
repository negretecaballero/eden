package EdenFinalUser.Categories.Classes;

public class CategoriesViewLayoutImplementation implements CategoriesViewLayout {

	private Entities.Franquicia franchise;
	private String logo;

	@Override()
	public Entities.Franquicia getFranchise() {
		return this.franchise;
	}

	/**
	 * 
	 * @param franchise
	 */
	@Override()
	public void setFranchise(Entities.Franquicia franchise) {
		this.franchise = franchise;
	}

	@Override()
	public String getLogo() {
		return this.logo;
	}

	/**
	 * 
	 * @param logo
	 */
	@Override()
	public void setLogo(String logo) {
		this.logo = logo;
	}

	


}