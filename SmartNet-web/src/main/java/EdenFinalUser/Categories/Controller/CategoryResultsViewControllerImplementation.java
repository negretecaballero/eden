package EdenFinalUser.Categories.Controller;

import CDIBeans.*;

@javax.enterprise.inject.Alternative
public class CategoryResultsViewControllerImplementation implements CategoryResultsViewController {

	/**
	 * 
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(String viewId) {
		
            try{
            
                switch(viewId){
                
                    case "/User/category/result.xhtml":{
                        
                        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                        
                        EdenFinalUser.Categories.View.CategoryResultsView categoryResultsView=(EdenFinalUser.Categories.View.CategoryResultsView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "categoryResultsView");
                      
                       javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                      
                         this._fileController.Remove();
                     
                       
                       
                       if(servletRequest.getParameter("category")!=null && servletRequest.getParameter("subcategory")!=null && categoryResultsView!=null){
                       
                       Entities.Subtype subtype=this._subtypeController.findByTypeSubtypeName(servletRequest.getParameter("category"), servletRequest.getParameter("subcategory"));
                       
                       if(subtype!=null && subtype.getFranquiciaCollection()!=null && !subtype.getFranquiciaCollection().isEmpty()){
                       
                           
                           
                           
                           if(subtype.getFranquiciaCollection()!=null && !subtype.getFranquiciaCollection().isEmpty())
                           {
                               
                               categoryResultsView.setFranchiseList(new java.util.ArrayList<EdenFinalUser.Categories.Classes.CategoriesViewLayout>());
                           
                               javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                               
                           for(Entities.Franquicia franchise:subtype.getFranquiciaCollection()){
                           
                               EdenFinalUser.Categories.Classes.CategoriesViewLayout aux=new EdenFinalUser.Categories.Classes.CategoriesViewLayoutImplementation();
                               
                               aux.setFranchise(franchise);
                               
                               
                               
                               if(franchise.getImagenIdimagen()!=null){
                               
                                  Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                               
                               java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre());
                               
                               if(file.exists()){
                               
                                   filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre());
                               
                               }
                               
                               else{
                               
                                   filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator,franchise.getNombre());
                               
                               } 
                                   
                               this._fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre());
                               
                               String imageName=this._fileController.getPathList().get(this._fileController.getPathList().size()-1);
                               
                               aux.setLogo(imageName);
                               
                               this._fileController.getPathList().remove(this._fileController.getPathList().size()-1);
                               
                               this._fileController.getImageList().remove(this._fileController.getImageList().size()-1);
                               
                               }
                               
                               else{
                               
                                   aux.setLogo(("/images/noImage.png"));
                               
                               }
                               
                               categoryResultsView.getFranchiseList().add(aux);
                           
                           }
                           
                           }
                       }
                       
                       }
                        
                       
                        break;
                    
                    }
                
                }
            
            }
            catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
            }
            
	}

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private SubtypeController _subtypeController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private FileController _fileController;



}