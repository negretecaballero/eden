package EdenFinalUser.Categories.View;

import EdenFinalUser.Categories.Controller.*;
import EdenFinalUser.Categories.Classes.*;

public class CategoryResultsView extends Clases.BaseBacking{

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
	
            try{
                
                System.out.print("VIEW ID "+super.getCurrentView());
            
                _categoryResultsViewController.initPreRenderView(super.getCurrentView());
            
            }catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	public java.util.List<CategoriesViewLayout> getFranchiseList() {
		return this.franchiseList;
	}

	public void setFranchiseList(java.util.List<CategoriesViewLayout> franchiseList) {
		this.franchiseList = franchiseList;
	}

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private CategoryResultsViewController _categoryResultsViewController;
	private java.util.List<CategoriesViewLayout> franchiseList;
        
       
}