/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

/**
 *
 * @author luisnegrete
 */
public interface ProductViewLayout {
    
    String getImage();
    
    void setImage(String image);
    
    Entities.Producto getProduct();
    
    void setProduct(Entities.Producto product);
    
}
