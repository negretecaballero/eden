/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

/**
 *
 * @author luisnegrete
 */
public class ProductViewLayoutImplementation implements ProductViewLayout {
    
    private String image;
    
    private Entities.Producto product;
    
    
    @Override
    public Entities.Producto getProduct(){
    
        return this.product;
    
    }
    
    @Override
    public void setProduct(Entities.Producto product){
    
        this.product=product;
    
    }
    
    @Override
    public String getImage(){
    
    return this.image;
    
    }
    
    @Override
    public void setImage(String image){
    
        this.image=image;
    
    }
    
}
