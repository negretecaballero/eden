/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

import Entities.SucursalPK;

/**
 *
 * @author luisnegrete
 */
public class ResultsType implements ResultsTypeInterface  {
    
    private SucursalPK key;
    
    private String name;
    
    private String imageLocation;
    
    private float distance;
    
    @Override
    public float getDistance(){
    
        return this.distance;
        
    }
    
    @Override
    public void setDistance(float distance){
    
      this.distance=distance;
    
    }
    
    @Override
    public void setImageLocation(String imageLocation){
    
    this.imageLocation=imageLocation;
    
    }
    
    @Override
    public String getImageLocation(){
    
    return this.imageLocation;
    
    }
    
    @Override
    public void setKey(SucursalPK key){
    
    this.key=key;
    
    }
    
    @Override
    public SucursalPK getKey(){
    
    return this.key;
    
    }
    
    @Override
    public void setName(String name){
    
    
    this.name=name;
    
    
    }
    
    @Override
    public String getName(){
    
    return this.name;
    
    }
    
    
    public ResultsType(){
    
    key=new SucursalPK();
    
    }
    
    boolean range;
    
    @Override
    public boolean getRange(){
            
        return this.range;
        
    }
    
    @Override
    public void setRange(boolean range){
    
        this.range=range;
    
    }
    
}
