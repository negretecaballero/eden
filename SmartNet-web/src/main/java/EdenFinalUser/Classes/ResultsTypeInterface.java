/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

import Entities.SucursalPK;

/**
 *
 * @author luisnegrete
 */
public interface ResultsTypeInterface {
    
    void setName(String name);
    String getName();
    
    void setKey(SucursalPK key);
    SucursalPK getKey();
    
    void setImageLocation(String imageLocation);
    String getImageLocation();
    
    float getDistance();
    
    void setDistance(float distance);
    
    boolean getRange();
    
    void setRange(boolean range);
    
}
