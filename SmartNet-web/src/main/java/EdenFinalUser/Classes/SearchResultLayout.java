/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SearchResultLayout {
    
    Entities.Franquicia getFranchise();
    
    void setFranchise(Entities.Franquicia franchise);
    
    String getLogo();
    
    void setLogo(String logo);
    
}
