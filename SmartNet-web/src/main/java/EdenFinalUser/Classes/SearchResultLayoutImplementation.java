/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Classes;

/**
 *
 * @author luisnegrete
 */
public class SearchResultLayoutImplementation implements SearchResultLayout {
    
    private Entities.Franquicia _franchise;
    
    public String _logo;
    
    @Override
    public Entities.Franquicia getFranchise(){
    
        return _franchise;
    
    }
    
    @Override
    public void setFranchise(Entities.Franquicia franchise){
    
        _franchise=franchise;
    
    }
    
    @Override
    public String getLogo(){
    
        return _logo;
    
    }
    @Override
    public void setLogo(String logo){
    
    _logo=logo;
    
    }
    
    
    
    
}
