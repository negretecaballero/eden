/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;


/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Stereotype
@javax.enterprise.context.Dependent
@Retention(RUNTIME)
@Target({TYPE})

public @interface EdenFinalUserStereotype {
    
}
