/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

/**
 *
 * @author luisnegrete
 */
public interface EdenFinalUserSucursalController {
    
     void addSucursalMarker(org.primefaces.model.map.MapModel model);
    
     double getLatitude();
     
     double getLongitude();
     
     java.util.Vector<String>sucursalImages();
     
     java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>getProductsViewLayout();
     
     boolean open();
     
     boolean range();
     
     void addCartMessage();
     
     void addToCart(Entities.Producto product);
     
}
