/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

import java.util.Random;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative

public class EdenFinalUserSucursalControllerImplementation implements EdenFinalUser.Home.Controller.EdenFinalUserSucursalController{
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SucursalControllerDelegate _sucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private Controllers.CartController _cartController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.ProductoControllerInterface _productoController;
    
    @Override
    public void addSucursalMarker(org.primefaces.model.map.MapModel model){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface ){
            
            EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
                     
                Entities.Sucursal sucursal=_sucursalController.find(resultsType.getKey());
                
            if(model==null){
            
                model=new org.primefaces.model.map.DefaultMapModel();
            
            }
            
            org.primefaces.model.map.Marker marker=new org.primefaces.model.map.Marker(new org.primefaces.model.map.LatLng(sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude(), sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude()), sucursal.getFranquicia().getNombre());
            
            System.out.print("MARKER NAME "+marker.getTitle());
            
            model.addOverlay(marker);
            }
            else{
                    
                    
            }
            
        
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public double getLatitude(){
    
        try{
        
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
              EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
               
                Entities.Sucursal sucursal=_sucursalController.find(resultsType.getKey());
      
                return sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude();
                
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return 0.0;
        
        }
    
    }
    
    
    @Override
    public double getLongitude(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
                    EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
                     
                Entities.Sucursal sucursal=_sucursalController.find(resultsType.getKey());
      
                return sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude();
                
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return 0.0;
        
        }
    
    }
    
    
    @Override
    public java.util.Vector<String>sucursalImages(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
  
                EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
                
                System.out.print("KEY "+resultsType.getKey());
                
                Entities.Sucursal sucursal=_sucursalController.find(resultsType.getKey());
      
                
                 java.util.Vector<String>results;
                
               if(sucursal.getImagenCollection()!=null && !sucursal.getImagenCollection().isEmpty()){
                   
                   javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
               
                   String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"sucursal";
                        
                   java.io.File file=new java.io.File(path);
                   
                   Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                   
                   if(file.exists()){
                   
                       filesManagement.cleanFolder(path);
                   
                   }
                   else{
                   
                       filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, "sucursal");
                   
                   }
                   
                   
                   
                   results=new java.util.Vector<String>(sucursal.getImagenCollection().size());
               
               for(Entities.Imagen image:sucursal.getImagenCollection()){
               
               _fileUploadBean.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+"sucursal");
               
               String imagen=_fileUploadBean.getImages().get(_fileUploadBean.getImages().size()-1);
               
               _fileUploadBean.getImages().remove(_fileUploadBean.getImages().size()-1);
               
               _fileUploadBean.getPhotoList().remove(_fileUploadBean.getPhotoList().size()-1);
               
               results.add(imagen);
               
               }
                   
                   
                   
               }
               else{
               
                  results =new java.util.Vector<String>(1);
                    
                    results.add("/images/noImage.png");
               
               }
               
            return results;
            }
            else{
            
            throw new IllegalArgumentException("Object must be of type "+Entities.Sucursal.class.getName());
            
            }
        
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
          return  null;
        
        }
    
    }
    
    @Override
    public java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>getProductsViewLayout(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
                EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
                 
                Entities.Sucursal sucursal=_sucursalController.find(resultsType.getKey());
      
              
                java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>results=new java.util.Vector<>(sucursal.getProductoCollection().size());
                
                Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                
                javax.servlet.ServletContext sc=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                String path=sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products";
                
                java.io.File file=new java.io.File(path);
                
                if(file.exists()){
                
                    filesManagement.cleanFolder(path);
                
                }
                else{
                
                    filesManagement.createFolder(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, "products");
                
                }
                
                for(Entities.Producto product:sucursal.getProductoCollection()){
                
                EdenFinalUser.Classes.ProductViewLayout aux=new EdenFinalUser.Classes.ProductViewLayoutImplementation();
                
                aux.setProduct(product);
                
                if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                
                    Random rand=new Random();
                    
                    int max=product.getImagenCollection().size()-1;
                    
                    int min=0;
                    
                    int randomNum=rand.nextInt(((max-min)+1)+min);
                    
                    Entities.Imagen imagen=((java.util.List<Entities.Imagen>)product.getImagenCollection()).get(randomNum);
                    
                    this._fileUploadBean.loadImagesServletContext(imagen, session.getAttribute("username").toString()+java.io.File.separator+"products");
                
                    String logo=this._fileUploadBean.getImages().get(this._fileUploadBean.getImages().size()-1);
                    
                    this._fileUploadBean.getImages().remove(this._fileUploadBean.getImages().size()-1);
                    
                    this._fileUploadBean.getPhotoList().remove(this._fileUploadBean.getPhotoList().size()-1);
                    
                    aux.setImage(logo);
                    
                }
                else{
                
                    aux.setImage("/images/noImage.png");
                
                }
                
                results.add(aux);
                }
                
                return results;
                
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+Entities.Sucursal.class.getName());
            
            }
            
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        }
    
    }
    
    @Override
    public boolean open(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
                 EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
              
             return   _sucursalController.isOpen(resultsType.getKey());
            
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+EdenFinalUser.Classes.ResultsTypeInterface.class);
            
            }
            
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return false;
        }
        
    }
    
    @Override
    public boolean range(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
            EdenFinalUser.Classes.ResultsTypeInterface resultsType=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
            
           return resultsType.getRange();
            
            }
            
            else{
            
                throw new IllegalArgumentException("Object must be of type "+EdenFinalUser.Classes.ResultsTypeInterface.class.getName());
            
            }
            
        }
        catch(IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public void addCartMessage(){
    
        try{
        
            java.util.ResourceBundle rb=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
            if(!this.open())
            {
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(rb.getString("userSucursalProfile.ClosedMessage"));
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
            
          
            
            }
            else if(!this.range()){
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(rb.getString("userSucursalProfile.OutofRange"));
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
            
           
            
            }
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void addToCart(Entities.Producto product){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")!=null && session.getAttribute("auxiliarComponent")instanceof EdenFinalUser.Classes.ResultsTypeInterface){
            
                EdenFinalUser.Classes.ResultsTypeInterface resultsTypeInterface=(EdenFinalUser.Classes.ResultsTypeInterface)session.getAttribute("auxiliarComponent");
                
                if(_productoController.hasInventory(product, _sucursalController.find(resultsTypeInterface.getKey()), 1))
                {
                
                    this._cartController.addToCart(product, 1, _sucursalController.find(resultsTypeInterface.getKey()));
                
                }
                
                else{
                
                    java.util.ResourceBundle resourceBundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                    javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(resourceBundle.getString("outofInventory"));
                   
                    msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                    
                    javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                    
                    javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
                    
                }
            
            }
        }
        catch(IllegalArgumentException | NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
