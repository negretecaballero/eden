/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

import Clases.gpsOperations;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class FranchiseSearchResultsControllerImplementation implements FranchiseSearchResultsController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @Override
    public java.util.Vector<EdenFinalUser.Classes.ResultsTypeInterface>getResutls(String franchiseName, double latitude, double longitude){
    
        try{
            
            System.out.print("Name "+franchiseName+" Latitude: "+latitude+" Longitude "+longitude);
            
            if(this._franchiseController.findByName(franchiseName)!=null)
            {
                
                Entities.Franquicia franchise=this._franchiseController.findByName(franchiseName);
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
            _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
            java.util.Vector<EdenFinalUser.Classes.ResultsTypeInterface>results=new java.util.Vector<EdenFinalUser.Classes.ResultsTypeInterface>();
        
            for(Entities.Sucursal sucursal:franchise.getSucursalCollection()){
            
                EdenFinalUser.Classes.ResultsTypeInterface aux=new EdenFinalUser.Classes.ResultsType();
                
                aux.setDistance((float)gpsOperations.calculateDistance(latitude, sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude(), longitude, sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude())/1000);
            
                if(aux.getDistance()<=sucursal.getAlcance()){
                
                    aux.setRange(true);
                    
                }
                else{
                
                   aux.setRange(false);
                    
                }
                
                aux.setKey(sucursal.getSucursalPK());
                
                aux.setName(franchise.getNombre());
                
                if(sucursal.getImagenCollection()!=null && !sucursal.getImagenCollection().isEmpty()){
                
                    
                    SessionClasses.EdenList<Entities.Imagen>images=new SessionClasses.EdenList<Entities.Imagen>((java.util.List<Entities.Imagen>)sucursal.getImagenCollection());
                    
                   _fileUploadBean.loadImagesServletContext(images.get(0),session.getAttribute("username").toString());
                   
                   String image=_fileUploadBean.getImages().get(_fileUploadBean.getImages().size()-1);
                   
                   _fileUploadBean.getPhotoList().remove(_fileUploadBean.getPhotoList().size()-1);
                   
                   _fileUploadBean.getImages().remove(_fileUploadBean.getImages().size()-1);
                   
                   aux.setImageLocation(image);
                
                }
                else{
                
                aux.setImageLocation("/images/noImage.png");
                
                }
                
                results.add(aux);
            }
            
            return results;
            
        }
        else{
                
                return null;
                }
        
            
        }
        catch(Exception | StackOverflowError ex){
        
            return null;
        
        }
    
    }
    
}
