/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

/**
 *
 * @author luisnegrete
 */
public interface WelcomeUserController {
    
    java.util.Vector<Entities.Tipo>tipoArray();
    
    boolean validLatitudeLongitude();
    
    double getLatitudeValue();
    
    double getLongitudeValue();

	/**
	 * 
	 * @param view
	 */
	void initPreRenderView(String view);

	void logout();
    
}
