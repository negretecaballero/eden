/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.Controller;

import CDIBeans.*;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
public class WelcomeUserControllerImplementation implements WelcomeUserController {
    
    @javax.inject.Inject
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoController _tipoController;
	
    @javax.inject.Inject
	
    @CDIBeans.LoginAdministradorControllerQualifier
	
    private LoginAdministradorController _loginAdministradorController;
	@javax.inject.Inject
	@CDIBeans.FranchiseControllerQualifier
	private FranchiseController _franchiseController;
	@javax.inject.Inject
	@CDIBeans.ProductoControllerQualifier
	private ProductoControllerInterface _productController;
    
    @Override
    public java.util.Vector<Entities.Tipo>tipoArray(){
    
        try
        {
        
            if(_tipoController.findAll()!=null  && !_tipoController.findAll().isEmpty()){
            
                java.util.Vector<Entities.Tipo>results=new java.util.Vector<Entities.Tipo>();
               
                for(Entities.Tipo tipo:_tipoController.findAll()){
                
                    results.add(tipo);
                
                }
                
                return results;
                
            }
            
            return null;
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            return null;
        
        }
    
    }
    
    @Override
    public boolean validLatitudeLongitude(){
    
        try{
        
            javax.faces.context.FacesContext fc=javax.faces.context.FacesContext.getCurrentInstance();
            
            for(java.util.Map.Entry<String,String>entry:fc.getExternalContext().getRequestParameterMap().entrySet()){
            
            System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)fc.getExternalContext().getRequest();
            
            if(servletRequest.getParameter("form:latitude")!=null && servletRequest.getParameter("form:longitude")!=null && !servletRequest.getParameter("form:latitude").equals("0.0") && !servletRequest.getParameter("form:longitude").equals("0.0")){
            
                return true;
            
            }
            
            return false;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public double getLatitudeValue(){
    
        try{
        
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
         
            return Double.valueOf(servletRequest.getParameter("form:latitude"));
         
        }
        catch(NumberFormatException | NullPointerException | StackOverflowError ex)
        {
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return 0.0;
        
        }
    }
    
    @Override
    public double getLongitudeValue(){
    
        try{
        
        javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        
        return Double.valueOf(servletRequest.getParameter("form:longitude"));
        
        }
        
         catch(NumberFormatException | NullPointerException | StackOverflowError ex)
        {
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return 0.0;
        
        }
    
    }

	/**
	 * 
	 * @param view
	 */
	@Override
	public void initPreRenderView(String view) {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.Home.View.welcomeUser welcomeUser=(EdenFinalUser.Home.View.welcomeUser)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "welcomeUser");
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                 
                switch(view){
                
                    case "/User/welcomeuser.xhtml":{
                        
                         javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
            
            if(viewRoot.findComponent("west")!=null && viewRoot.findComponent("west") instanceof org.primefaces.component.layout.LayoutUnit){
            
                System.out.print("LAYOUT UNIT INSTANCE");
                
                org.primefaces.component.layout.LayoutUnit layoutUnit=(org.primefaces.component.layout.LayoutUnit)viewRoot.findComponent("west");
                
                layoutUnit.setVisible(true);
            
            }
                    
                        welcomeUser.setMenuModel(new org.primefaces.model.menu.DefaultMenuModel());
                        
                         for(Entities.Tipo tipo:this.tipoArray()){
        
            org.primefaces.model.menu.DefaultSubMenu submenu=new org.primefaces.model.menu.DefaultSubMenu(tipo.getNombre());
        
            for(Entities.Subtype subtype:tipo.getSubtypeCollection()){
            
                org.primefaces.model.menu.DefaultMenuItem item=new org.primefaces.model.menu.DefaultMenuItem(subtype.getName());
               
                item.setUrl("category/result.xhtml?category="+subtype.getIdTipo().getNombre()+"&subcategory="+subtype.getName()+"");
                //javax.el.MethodExpression expression=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), "#{welcomeUser.actionEvent}", null, new Class[]{javax.faces.event.ActionEvent.class});
                
               
                
                submenu.addElement(item);
         
            }
            
            submenu.setExpanded(false);
            
            
            
            welcomeUser.getMenuModel().addElement(submenu);
               }
                      
                         welcomeUser.setAccountCompletion(String.valueOf(this._loginAdministradorController.accountCompletion(session.getAttribute("username").toString())*100));
       
                         welcomeUser.setTest("Hey You");
                         
                         welcomeUser.setResults("Hey you");
                         
                         welcomeUser.setRootImage(this._loginAdministradorController.getImageRoot(session.getAttribute("username").toString()));
                         
                          if(this._loginAdministradorController.accountCompletion(session.getAttribute("username").toString())<0.5){
   
       welcomeUser.setIsComplete("red");
   
   }
   else{
   
      welcomeUser.setIsComplete("white");
   
   }
                          
     if(welcomeUser.getCompleteList()==null){
     
         welcomeUser.setCompleteList(new java.util.ArrayList<String>());
     
     }
             
    if(_franchiseController.findAll()!=null && !_franchiseController.findAll().isEmpty()){
    
        for(Entities.Franquicia franchise:_franchiseController.findAll()){
        
        welcomeUser.getCompleteList().add(franchise.getNombre());
        
        }
        
    
        
      
    
    }
    
    if(_productController.findAll()!=null && !_productController.findAll().isEmpty()){
    
           for(Entities.Producto product:this._productController.findAll()){
       
           welcomeUser.getCompleteList().add(product.getNombre());
       
       }
    
    }
                     
    if(viewRoot.findComponent("form:search")instanceof org.primefaces.component.autocomplete.AutoComplete){
    
        System.out.print("AUTOCOMPLETE INSTANCE");
        
        org.primefaces.component.autocomplete.AutoComplete autoComplete=(org.primefaces.component.autocomplete.AutoComplete)viewRoot.findComponent("form:search");
        
        System.out.print("AutoComplete Size "+autoComplete.getSize());
        
        if(viewRoot.findComponent("center-header-panel")instanceof org.primefaces.component.panel.Panel){
        
            org.primefaces.component.panel.Panel panel=(org.primefaces.component.panel.Panel)viewRoot.findComponent("center-header-panel");
            
            //System.out.print("Panel Size "+panel.)
        
        }
        
    
    }
    
                                 
                    }
                
                }
            
            }
            catch(NullPointerException ex){
            
                System.out.print("EXCEPTION CAUGHT");
                
                ex.printStackTrace(System.out);
            
            }
            
	}

	@Override
	public void logout() {
	
            try{
                
                javax.faces.application.NavigationHandler nh = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
                
                this._loginAdministradorController.logout();
                
                javax.faces.context.ExternalContext ec = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                
                String url = ec.getRequestContextPath()+"login.xhtml?faces-redircet=true";
                
                System.out.print("URL "+url);
                
                ec.redirect("/login.xhtml?faces-redircet=true");
            
            }
            catch(NullPointerException | java.io.IOException ex){
            
            
            }
            
	}
    
    
    
}
