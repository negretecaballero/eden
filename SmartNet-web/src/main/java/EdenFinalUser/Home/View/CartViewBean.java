/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import Clases.CartView;
import Clases.CartViewInterface;
import Clases.CartViewUpdated;
import Clases.CartViewUpdatedInterface;
import Entities.Agrupa;
import Entities.Producto;
import SessionClasses.CartItem;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

/**
 *
 * @author luisnegrete
 */


public class CartViewBean {

    /**
     * Creates a new instance of CartViewBean
     */
    
  
    
    private String test;
    
    private List<CartViewUpdatedInterface>cart;
    
    @javax.inject.Inject
    @javax.enterprise.inject.Default
    private transient Controllers.CartController cartController;
 
    private String operation;
    
    public String getOperation(){
    
        return this.operation;
    
    }
    
    public void setOperation(String operation){
    
        this.operation=operation;
    
    }
    
    
    public void setCart(List<CartViewUpdatedInterface>cart){
    
    this.cart=cart;
    
    }
    
    
    public List<CartViewUpdatedInterface>getCart(){
    
    return this.cart;
    
    }
    
    public void setTest(String test){
    
    this.test=test;
    
    }
    
    public String getTest(){
    
    return this.test;
    
    }
    
    
    
    public CartViewBean() {
        
    }
    
    
    @PostConstruct
    public void init(){
        
  
    
      
      this.cart=new <CartViewUpdated>ArrayList();
      
      
      //Loop along the cart ArrayList
      for(CartItem cartItem:cartController.getCart().getCart()){
          
          System.out.print(cartItem.getName());
      
      CartViewInterface aux=new CartView();
      
      
      
      aux.setCartItem(cartItem);
      
      CartViewUpdatedInterface cartViewUpdated=null;
      
      
      //Check if cartItem is a Producto instance
      if(cartItem.getItem() instanceof Producto){
      
          Producto product=(Producto)cartItem.getItem();

          String name=product.getCategoria().getFranquicia().getNombre();
          
          boolean found=false;
          
       
        for(CartViewUpdatedInterface cartView:this.cart){
        
            //Franchise found, add CartViewItem to List
        if(cartView.getName().equals(name)){
        
            System.out.print("Found");
            
            found=true;
            
            cartViewUpdated=cartView;
            
            aux.setFranchiseName(product.getCategoria().getFranquicia().getNombre());
          
            aux.setPrice(product.getPrecio()*cartItem.getQuantity());
            
            aux.setPrice(cartItem.getQuantity()*product.getPrecio());
            
            cartViewUpdated.getCartList().add(aux);
     
            break;
        
        }
        
        //Franchise NAme not found, create a new object
        if(!found)
        {
            
         System.out.print("not found");
            
        cartViewUpdated=new CartViewUpdated();
        
        cartViewUpdated.setName(name);
        
        System.out.print(cartViewUpdated.getName());
        
        
        List<CartViewInterface> list=new <CartView> ArrayList();
        
        cartViewUpdated.setCartList(list);
        
        aux.setFranchiseName(product.getCategoria().getFranquicia().getNombre());
          
        aux.setPrice(product.getPrecio()*cartItem.getQuantity());
        
         aux.setPrice(cartItem.getQuantity()*product.getPrecio());
      cartViewUpdated.getCartList().add(aux);
      this.cart.add(cartViewUpdated);
      
      break;
        
        }
        
        
        }
          
             if(this.cart.size()==0){
          
          cartViewUpdated=new CartViewUpdated();
          
         List<CartViewInterface> list=new <CartView> ArrayList();
        
         cartViewUpdated.setCartList(list);
           
        cartViewUpdated.setName(name);
          
         aux.setFranchiseName(product.getCategoria().getFranquicia().getNombre());
          
         aux.setPrice(product.getPrecio()*cartItem.getQuantity());
        
         aux.setPrice(cartItem.getQuantity()*product.getPrecio());
         
         cartViewUpdated.getCartList().add(aux);
         
         this.cart.add(cartViewUpdated);
          
          }
          
          
          
          
      
      }
      
      else if(cartItem.getItem() instanceof Entities.Menu){
          
        
      
          Entities.Menu menu=(Entities.Menu)cartItem.getItem();
          
          
            List<Agrupa>agrupaList = (List)menu.getAgrupaCollection();
            
            String name="";
            for(Agrupa agrupa: agrupaList){
            
               aux.setFranchiseName(agrupa.getProducto().getCategoria().getFranquicia().getNombre());
               name=agrupa.getProducto().getCategoria().getFranquicia().getNombre();
               break;
            
            }
            
          
            
                boolean found=false;
            for(CartViewUpdatedInterface cartView:this.getCart()){
        
        if(cartView.getName().equals(name)){
        
            found=true;
            
            cartViewUpdated=cartView;
            
      aux.setPrice(cartItem.getQuantity()*menu.getPrecio());
      cartViewUpdated.getCartList().add(aux);
      
            
            break;
        
        }
        
        if(!found)
        {
            
        cartViewUpdated=new CartViewUpdated();
        
        cartViewUpdated.setName(name);
        
        List<CartViewInterface> list=new <CartView> ArrayList();
        
        cartViewUpdated.setCartList(list);
        
        aux.setPrice(cartItem.getQuantity()*menu.getPrecio());
      cartViewUpdated.getCartList().add(aux);
      this.cart.add(cartViewUpdated);
      break;
        }
        
        
        }
            
          
           if(this.cart.size()==0){
          
          cartViewUpdated=new CartViewUpdated();
          
         List<CartViewInterface> list=new <CartView> ArrayList();
        
         cartViewUpdated.setCartList(list);
           
          cartViewUpdated.setName(name);
          
            
        aux.setPrice(cartItem.getQuantity()*menu.getPrecio());
      cartViewUpdated.getCartList().add(aux);
      this.cart.add(cartViewUpdated);
          
          }
          
          
      }
      
      
      
      
      }
      
      
      for(CartViewUpdatedInterface cartView:this.cart){
      
     
      
      for(CartViewInterface cartviewInterface:cartView.getCartList()){
      
       System.out.print(cartView.getName()+"-"+cartviewInterface.getPrice()+"-"+cartviewInterface.getCartItem().getName());
      
      }
      
      
      }
      
        
    }
    
    
     public void removeItem(ActionEvent event,CartItem item){
    
        for(CartViewUpdatedInterface cartView:this.cart){
        
       for(CartViewInterface cartViewInterface:cartView.getCartList()){
       
       if(cartViewInterface.getCartItem().equals(item)){
       
       cartView.getCartList().remove(cartViewInterface);
       
       break;
       
       }
       
       }
        
        }
        
        this.cartController.removeFromCart(event, item);
        
        }
    
     public String pedidoRedirect(String operation){
     
       this.operation=operation;  
     
       
       return("Pedido");
     }
     
    }
    

