/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import CDIBeans.DesplegableCitiesInterface;
import CDIBeans.DesplegableStateInterface;
import CDIBeans.StateControllerInterface;
import Clases.BaseBacking;
import Clases.Direccion;
import Entities.City;
import Entities.CityPK;
import Entities.State;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.CityFacadeREST;
import jaxrs.service.DireccionFacadeREST;
import jaxrs.service.GpsCoordinatesFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.MenuhasPEDIDOFacadeREST;
import jaxrs.service.PedidoFacadeREST;
import jaxrs.service.PedidoStateFacadeREST;
import jaxrs.service.ProductohasPEDIDOFacadeREST;
import jaxrs.service.StateFacadeREST;
import jaxrs.service.TypeAddressFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;


/**
 *
 * @author luisnegrete
 */
@RolesAllowed("GuanabaraUser")

public class Pedido extends BaseBacking{
    
@Inject private DesplegableStateInterface desplegableState;

@Inject private DesplegableCitiesInterface desplegableCities;

@EJB
private StateFacadeREST stateFacadeREST;

@EJB
private LoginAdministradorFacadeREST loginAdministradorFacadeREST;

@EJB
private CityFacadeREST cityFacadeREST;

@EJB
private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;

@EJB
private TypeAddressFacadeREST typeAddressFacadeREST;


@EJB
private DireccionFacadeREST direccionFacadeREST;

@EJB
private PedidoStateFacadeREST pedidoStateFacadeREST;

@EJB
private MenuhasPEDIDOFacadeREST menuhasPEDIDOFacadeREST;

@EJB
private ProductohasPEDIDOFacadeREST productohasPEDIDOFacadeREST;

@EJB
private PedidoFacadeREST pedidoFacadeREST;

@Inject @javax.enterprise.inject.Default
private StateControllerInterface stateController;

@javax.inject.Inject @javax.enterprise.inject.Default
private CDIBeans.CityControllerInterface cityController;

@javax.inject.Inject @javax.enterprise.inject.Default
private CDIBeans.AddressTypeControllerInterface addressTypeController;

@javax.inject.Inject @javax.enterprise.inject.Default
private transient CDIBeans.PedidoControllerInterface pedidoController;

@javax.inject.Inject @javax.enterprise.inject.Default
private transient Controllers.CartController _cartController;

private String hiddenState;

private String hiddenCity;

private final static String CHANNEL = "/pedidos";

public String getHiddenState(){

return this.hiddenState;

}

public void setHiddenState(String hiddenState){

    this.hiddenState=hiddenState;

}

public String getHiddenCity(){

    return this.hiddenCity;

}

public void setHiddenCity(String hiddenCity){

this.hiddenCity=hiddenCity;
    
}

public DesplegableCitiesInterface getDesplegableCities(){

return this.desplegableCities;

}

public void setDesplegableCities(DesplegableCitiesInterface desplegableCities){

this.desplegableCities=desplegableCities;

}


public DesplegableStateInterface getDesplegableState(){


    return this.desplegableState;

}

public void setDesplegableState(DesplegableStateInterface desplegableState){

this.desplegableState=desplegableState;

}
    
private CityPK cityPK;

private String stateId;
    
private Direccion address;
    
private String id;


public String getId(){

    return this.id;

}

public void setId(String id){

    this.id=id;

}

public String getStateId(){


    return this.stateId;
}



public void setStateId(String stateId){

this.stateId=stateId;

}


public void setCityPK(CityPK cityPK){
    
this.cityPK=cityPK;

}

public CityPK getCityPK(){

    return this.cityPK;

}

public Direccion getAddress(){

return this.address;

}

public void setAddress(Direccion address){

this.address=address;

}


private String test;

public void setTest(String test){

    this.test=test;

}

public String getTest(){


return this.test;

}



    public Pedido() {
    }
   
    
    @PostConstruct
    public void init(){
        
      
        
        this.address=new Direccion();
        
           DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
           //get current date time with Date()
	   Date date = new Date();
	   System.out.println(dateFormat.format(date));
 
	   //get current date time with Calendar()
	   Calendar cal = Calendar.getInstance();
	   System.out.println(dateFormat.format(cal.getTime()));
    
    this.test="Hi Smart";
    
    Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
    
    for(Map.Entry<String,String>entry:requestMap.entrySet()){
    
    System.out.print(entry.getKey()+"-"+entry.getValue());
    
    }
    
   
    
    HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
    
    this.id=request.getParameter("id");
    
     System.out.print("Id"+this.id);
    
    }
    
     public void cityChanged(AjaxBehaviorEvent event){
    
           System.out.print("Changing city");
        
        try{
            
            
            
            System.out.print("City Changed");
            
            System.out.print("Component "+event.getComponent().getClientId());
        
            if(event.getComponent().getClientId().equals("form:city")){
            
                PathSegment ps=new PathSegmentImpl("bar;idCITY="+this.cityPK.getIdCITY()+";sTATEidSTATE="+this.cityPK.getSTATEidSTATE()+"");
            
            this.hiddenCity=cityFacadeREST.find(ps).getName();
            
            
            System.out.print("New City "+this.hiddenCity);
            
            }
            else
            {
            
                throw new RuntimeException();
            
            }
        
        }
        catch(RuntimeException ex){
        
        FacesMessage msg=new FacesMessage("You are Hacking Bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    public void stateChanged(AjaxBehaviorEvent event){
    
    try{
    
        System.out.print(event.getComponent().getClientId());
        
        if(event.getComponent().getClientId().equals("form:state")){
            
            System.out.print("hiddenState "+this.hiddenState);
        
            System.out.print("State Id "+this.stateId);
            
            this.desplegableCities.updateResultsCities(stateController.findByName(this.stateId));
            
            Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
            
            for(Map.Entry<String,String> entry:requestMap.entrySet()){
            
            System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
            HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
           
            String cityName=request.getParameter("HiddenCity");
            
           
            
            System.out.print(cityName);
            
            State state=stateController.findByName(this.stateId);
            
             this.hiddenState=state.getName();
             
            if(this.hiddenCity==null){ 
            
                System.out.print("Hidden City was null");
                
            for(City cities:state.getCityCollection()){
            
                if(cities.getName().equals(cityName)){
                
                this.cityPK=cities.getCityPK();
                this.hiddenCity=cities.getName();
                
                break;
                }
            
            }
            
            }
            
            else{
            
            this.hiddenCity=((List<City>)state.getCityCollection()).get(0).getName();
            
            }
            
          
            
          
            
            
        
        }
        else{
        
            throw new RuntimeException();
        
        }
    
    }
    catch(RuntimeException ex){
    
        FacesMessage msg=new FacesMessage("You are hacking bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void order(javax.faces.event.AjaxBehaviorEvent event, String type){
    
    try{
         
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);
        
        for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
        
            System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
        
       
       
     String username=session.getAttribute("username").toString();
     
     Entities.GpsCoordinates gps=new Entities.GpsCoordinates();
     
     
        
     Entities.Direccion direccion=new Entities.Direccion();
     
     javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
     
         for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
     
         System.out.print(entry.getKey()+"-"+entry.getValue());
     
     }
     
     
     gps.setLatitude(Double.valueOf(servletRequest.getParameter("latitudeValue")));
     
     gps.setLongitude(Double.valueOf(servletRequest.getParameter("longitudeValue")));
     
     Entities.DireccionPK direccionKEY=new Entities.DireccionPK();
     
     direccionKEY.setCITYSTATEidSTATE(this.cityPK.getSTATEidSTATE());

     direccionKEY.setCITYidCITY(this.cityPK.getIdCITY());
     
     direccion.setDireccionPK(direccionKEY);
     
     direccion.setGpsCoordinatesidgpsCoordinated(gps);
     
     direccion.setCity(this.cityController.find(cityPK));
     
     direccion.setPrimerDigito(servletRequest.getParameter("form:primerDigito"));
     
     direccion.setSegundoDigito(servletRequest.getParameter("form:segundoDigito"));
     
     direccion.setTercerDigito(Short.valueOf(servletRequest.getParameter("form:tercerDigito")));
     
     direccion.setAdicional(servletRequest.getParameter("form:description"));
     
     direccion.setTYPEADDRESSidTYPEADDRESS(this.addressTypeController.findByname(this.address.getTipo()));
     
 
     System.out.print("Cart List Size "+_cartController.getCartBySucursal().size());
     
     
     
      
     for(Clases.CartControllerLayout aux:_cartController.getCartBySucursal()){
     
         System.out.print(aux.getSucursal().getFranquicia().getNombre()+" items in this franchise "+aux.getCartList().size());
     
     }
     
     this.pedidoController.createOrder(username, direccion, Clases.EdenDate.currentDate(), type, "");

     if(type.equals("contraentrega")){
     
     javax.faces.application.NavigationHandler nh=getContext().getApplication().getNavigationHandler();
        
     nh.handleNavigation(getContext(), null, "success");
     }
     
    }
    
    
    catch(EJBException ex){
    
        FacesMessage msg=new FacesMessage("Error Placing Order");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(null,msg);
        
        getContext().renderResponse();
        
        Logger.getLogger(Pedido.class.getName()).log(Level.SEVERE,null,ex);
        
     
    
    
    }
    
    /*catch(javax.servlet.ServletException ex){
    
        throw new javax.faces.FacesException(ex);
    
    }*/
    
    catch(IllegalArgumentException  ex){
    
      java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    
    
    
    
    }
   
    
}
