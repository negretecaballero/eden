/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import CDIBeans.FileUploadInterface;
import CDIBeans.LastSeenControllerInterface;
import CDIBeans.ProductoControllerInterface;
import Clases.BaseBacking;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Clases.ProductsResultsType;
import Clases.ProductsResultsTypeInterface;
import SessionClasses.CartItem;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import jaxrs.service.ProductoFacadeREST;

/**
 *
 * @author luisnegrete
 */

@javax.annotation.security.RolesAllowed("Guanabarauser")
public class ProductsResults extends BaseBacking{
 
  private  List<ProductsResultsTypeInterface>results;
  
  @javax.inject.Inject
  @javax.enterprise.inject.Default
  private transient Controllers.CartController cartController;
  
  @Inject
  private ProductoControllerInterface productoController;
  
  private int selectedProduct;
  
  public int getSelectedProduct(){
  
  return this.selectedProduct;
  
  }
  
  @EJB
  private ProductoFacadeREST productoFacadeREST;
  
  @Inject 
          
  private FileUploadInterface fileUpload;
  
  @Inject
  
  private ProductoControllerInterface productoControllerInterface;
  
  @Inject private LastSeenControllerInterface lastSeenController;
  
  public List<ProductsResultsTypeInterface> getResults(){

      return results;

}
  
  public void setResults(List<ProductsResultsTypeInterface>results){
  this.results=results;
  }
  
  public void preRenderView(javax.faces.event.ComponentSystemEvent event){
  
      try{
      
          javax.faces.component.UIViewRoot viewRoot=(javax.faces.component.UIViewRoot)this.getContext().getViewRoot();
          
          if(viewRoot.findComponent("west") instanceof org.primefaces.component.layout.LayoutUnit){
          
              org.primefaces.component.layout.LayoutUnit unit=(org.primefaces.component.layout.LayoutUnit)viewRoot.findComponent("west");
              
              unit.setVisible(true);
              
          }
          
      }
      catch(StackOverflowError ex){
      
          java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
      
      }
  
  }
  
  @PostConstruct
  public void init(){
  
      
  try{
  
      System.out.print("Selected Product init value "+this.selectedProduct);
      
      System.out.print("Instance AuxiliarComponent "+this.getSessionAuxiliarComponent());
      
      if(getSessionAuxiliarComponent() instanceof Entities.Sucursal){
      
          results= new <ProductsResultsType>ArrayList();
          
          Entities.Sucursal sucursal=(Entities.Sucursal)getSessionAuxiliarComponent();
          
         List<Entities.Producto>products=(java.util.List<Entities.Producto>)sucursal.getProductoCollection();
          
        System.out.print("This Sucursal has "+products.size()+" products");
         
         
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
        
         
         this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString());
         
        FilesManagementInterface filesManagement=new FilesManagement();
         
         for(Entities.Producto product:products){
         
             filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+File.separator+"images"+File.separator, getSession().getAttribute("username").toString());
             
             filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username")+File.separator, product.getNombre());
         
             ProductsResultsTypeInterface aux=new ProductsResultsType();
             
             aux.setProduct(product);
             
//             aux.setProductQuantity(productoController.productQuantity(product.getIdproducto()));
             
             
             System.out.print(product.getNombre());
             
            
             List <String>imagenes=new <String>ArrayList();
           
             for(Entities.Imagen image:product.getImagenCollection()){
             
                 String path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+getSession().getAttribute("username")+"/"+product.getNombre();
                 
                 fileUpload.loadImagesServletContext(image,getSession().getAttribute("username")+"/"+product.getNombre());
             
                 imagenes.add(fileUpload.getImages().get(fileUpload.getImages().size()-1));
                 
             }
             
             if(product.getImagenCollection().isEmpty()){
             
             
                 imagenes.add("/images/noImage.png");
             
             }
             
               
            
         
             aux.setImages(imagenes);
             
             System.out.print("File Upload size "+fileUpload.getImages().size());
             
             System.out.print("Products Results Type List size "+aux.getImages().size());
             
             this.fileUpload.getImages().clear();
             
             System.out.print("New FileUpload Size "+fileUpload.getImages().size());
             
             System.out.print("New ProductsResultsType size "+aux.getImages().size());
             
             for(String string:aux.getImages()){
             
             System.out.print("Image Location " +string);
             
             }
             
              this.results.add(aux);
             
         }
         
      
      }
      
    
  
  }
  catch(RuntimeException ex){
  
  Logger.getLogger(ProductsResults.class.getName()).log(Level.SEVERE,null,ex);
  
  }
  
  }
  
  public void addItemToCart(SessionClasses.Item item, int quantity){
  
     try{
     
         
         System.out.print("Item Name "+item.getClass().getName());
         
           if(getSessionAuxiliarComponent() instanceof Entities.Sucursal){
         
               Entities.Sucursal sucursal=(Entities.Sucursal)getSessionAuxiliarComponent();
               
         cartController.addToCart(item, quantity,sucursal);
         
           }
         
     }
     catch(javax.faces.event.AbortProcessingException | NullPointerException ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
     }
     
     
  
  }
    
  public String selectProducto(Entities.Producto product){
  
      try{
      
          System.out.print("You have Selected "+product.getNombre());
          
 // this.selectedProduct=product.getProductoPK();
  
  
  
   return "success";
      }
      catch(RuntimeException ex){
      
          return "failure";
      
      }
 
  }
  
  public void checkInventory(CartItem item){
  
      
      
     
      
       int quantity=Integer.parseInt(getRequest().getParameter("form2:table:0:quantity"));
       
       System.out.print("Quantity: "+quantity);
       
       if(item.getItem() instanceof Entities.Producto)
       
       {
           if(productoController.validInventory((Entities.Producto)item.getItem())){
           
       if(productoController.productQuantity(((Entities.Producto)item.getItem()).getProductoPK())<quantity){
       
           item.setQuantity(1);
           
           FacesMessage msg=new FacesMessage("There is not enough inventory for this product quantity");
           
           msg.setSeverity(FacesMessage.SEVERITY_ERROR);
           
           getContext().addMessage(null,msg);
       
       }
           }
       
  }
       
      
  }
  
}
