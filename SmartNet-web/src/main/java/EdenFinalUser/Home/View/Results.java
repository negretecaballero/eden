/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import CDIBeans.FileUploadInterface;
import CDIBeans.SucursalControllerDelegate;
import Clases.BaseBacking;
import EdenFinalUser.Classes.ResultsType;
import EdenFinalUser.Classes.ResultsTypeInterface;
import Clases.gpsOperations;
import Entities.Franquicia;
import Entities.GpsCoordinates;
import Entities.Imagen;
import Entities.Sucursal;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import jaxrs.service.FranquiciaFacadeREST;
import managedBeans.loginBean;

/**
 *
 * @author luisnegrete
 */
public class Results extends BaseBacking{
    

private int size;
   
private java.util.Vector<ResultsTypeInterface>results;
    

@Inject private SucursalControllerDelegate sucursalController;

@javax.inject.Inject @javax.enterprise.inject.Default private EdenFinalUser.Home.Controller.FranchiseSearchResultsController _franchiseResultController;




public SucursalControllerDelegate getSucursalController(){

    return this.sucursalController;

}

public void setSucursalController(SucursalControllerDelegate sucursalController){

this.sucursalController=sucursalController;

}

 
public void setSize(int size){

this.size=size;

}

public int getSize(){

return this.size;

}


public void setResults(java.util.Vector<ResultsTypeInterface>results){


this.results=results;

}

public java.util.Vector<ResultsTypeInterface>getResults(){

return this.results;

}

    /**
     * Creates a new instance of Results
     */
    public Results() {
        
 
      
    }
    
    @PostConstruct
    public void init(){
        
     
        
    loadComponents();
    
    }
    
    
    
    private void loadComponents(){
        
    try{
      
        javax.servlet.http.HttpServletRequest params=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
        
      System.out.print("Latitude "+params.getParameter("latitude"));
      
      double latitude=Double.parseDouble(params.getParameter("latitude"));
      
      double longitude=Double.parseDouble(params.getParameter("longitude"));
      
      
      String name=params.getParameter("search_input");
      
    
      this.results=this._franchiseResultController.getResutls(name, latitude, longitude);
      
      
      System.out.print("Sucursales Size "+this.results.size());
    this.size=this.results.size();
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    private int idSucursal;
    
    public int getIdSucursal(){
    
    return this.idSucursal;
    
    }
    
    public void setIdSucursal(int idSucursal){
    
        this.idSucursal=idSucursal;
    
    }
    
    private int idFranchise;
    
    public int getIdFranchise(){
    
        return this.idFranchise;
    
    }
    
    public void setIdFranchise(int idFranchise){
    
        this.idFranchise=idFranchise;
    
    }
    
    public String redirectProgrammDelivery(Entities.SucursalPK id){
    
        try{
            
            System.out.print(id.getIdsucursal());
            
            System.out.print(id.getFranquiciaIdfranquicia());
        
            this.idSucursal=id.getIdsucursal();
            
            this.idFranchise=id.getFranquiciaIdfranquicia();
            
            
          
            
            return "ProgramDelivery";
        
        }
        catch(Exception ex){
        
            return "failure";
        
        }
    
    }
}
