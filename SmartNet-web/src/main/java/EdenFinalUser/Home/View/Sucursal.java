/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import CDIBeans.FileUploadInterface;
import Clases.BaseBacking;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.MapModel;

/**
 *
 * @author luisnegrete
 */
public class Sucursal extends BaseBacking{
    
    @Inject private FileUploadInterface fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenFinalUser.Home.Controller.EdenFinalUserSucursalController _sucursalController;
    
    
    private MapModel model=new DefaultMapModel();
    
    private boolean isOpen;
    
    public boolean getIsOpen(){
    
        this.isOpen=this._sucursalController.open();
        
        return this.isOpen;
    
    }
    
    public void setIsOpen(boolean isOpen){
    
        
        
        this.isOpen=isOpen;
    
    }
    

    public MapModel getModel() {
        return model;
    }

    public void setModel(MapModel model) {
        this.model = model;
    }
    
    private java.util.Vector<String>_sucursalImages;
    
    public java.util.Vector<String>getSucursalImages(){
    
        _sucursalImages=this._sucursalController.sucursalImages();
        
        return _sucursalImages;
    
    }
    
    public void setSucursalImages(java.util.Vector<String>sucursalImages){
    
        _sucursalImages=sucursalImages;
    
    }
    
    
    private java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>products;
    
    public java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>getProducts(){
    
        this.products=this._sucursalController.getProductsViewLayout();
        
        return this.products;
    
    }
    
    public void setProducts(java.util.Vector<EdenFinalUser.Classes.ProductViewLayout>products){
    
        this.products=products;
    
    }

    public Sucursal() {
    }
    
    private double longitude;
    
    private double latitude;
    
    private String infoWindowImage;
    
    private String sucursalName;

    
    public void setInfoWindowImage(String infoWindowImage){
    
        this.infoWindowImage=infoWindowImage;
    
    }
    
    public String getInfoWindowImage(){
    
        return this.infoWindowImage;
    
    }
    
    public void setSucursalName(String sucursalName){
    
    this.sucursalName=sucursalName;
    
    }
    
    public String getSucursalName(){
    
    
    return this.sucursalName;
    
    }
    
    public double getLatitude(){
    
        return this.latitude;
    
    }
    
    public void setLatitude(double latitude){
    
    this.latitude=latitude;
    
    }
    
    public double getLongitude(){
    
    return this.longitude;
    
    }
    
    public void setLongitude(double longitude){
    
    this.longitude=longitude;
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIViewRoot viewRoot=this.getContext().getViewRoot();
            
            if(viewRoot.findComponent("center") instanceof org.primefaces.component.layout.LayoutUnit){
            
                System.out.print("LAYOUT UNIT INSTANCE");
                
                org.primefaces.component.layout.LayoutUnit unit=(org.primefaces.component.layout.LayoutUnit)viewRoot.findComponent("center");
                
                
                
                for(java.util.Map.Entry<String,Object>entry:unit.getAttributes().entrySet()){
                
                    System.out.print(entry.getKey()+"-"+entry.getValue());
                
                }
            
            }
       
        }
        catch(StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @PostConstruct
    public void init(){
 
      try{
 
          javax.servlet.ServletContext sc=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
          
          this.fileUpload.Remove(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
          
          _sucursalController.addSucursalMarker(model);
          
          this.latitude=_sucursalController.getLatitude();
          
          this.longitude=_sucursalController.getLongitude();
          
      }
      catch(Exception | StackOverflowError ex){
      
          java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
      
      }
        
        
        }
 
            
    
    public void addToCart(EdenFinalUser.Classes.ProductViewLayout productViewLayout){
    
    try{
        
        System.out.print("Add TO CART METHOD");
        
        
        System.out.print("ProductViewLayout "+productViewLayout.getProduct().getNombre());
    
        if(this._sucursalController.open() && this._sucursalController.range()){
        
            this._sucursalController.addToCart(productViewLayout.getProduct());
        
        }
        
        else{
        
            System.out.print("ADD TO CART MESSAGE");
      
            _sucursalController.addCartMessage();
           
            this.getContext().renderResponse();
        
        }
        
    }
    
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    

    
}
