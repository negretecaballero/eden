
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Home.View;

import CDIBeans.DesplegableFranchiseInterface;
import CDIBeans.FileUploadInterface;
import Clases.BaseBacking;
import Clases.EditDistance;
import Clases.UpdatedTreeNode;
import ENUM.Role;



import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.inject.Inject;
import jaxrs.service.AppGroupFacadeREST;
import jaxrs.service.ProductoFacadeREST;
import jaxrs.service.TipoFacadeREST;
import managedBeans.loginBean;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.TreeNode;

/**
 *
 * @author luisnegrete
 */
public class welcomeUser extends BaseBacking{

    /**
     * Creates a new instance of welcomeUser
     */
    
    
    
   
    
    @EJB
    private TipoFacadeREST typeFacadeREST;
    
    @EJB
    private AppGroupFacadeREST appGroupFacadeREST;
    
   
    
    

    
    private String results;
    
    private TreeNode root;
    
    private UpdatedTreeNode selectedNode;
    
    private double latitude;
    
    private double longitude;

    
    private String accountCompletion;
    
    @javax.inject.Inject
    
    private EdenFinalUser.Home.Controller.WelcomeUserController welcomeUserController;
    
    private org.primefaces.model.menu.MenuModel menuModel;
    
    public org.primefaces.model.menu.MenuModel getMenuModel(){
    
    return this.menuModel;
    
    }
    
    public void setMenuModel(org.primefaces.model.menu.MenuModel menuModel){
    
        this.menuModel=menuModel;
    
    }
    
    @javax.inject.Inject
    @CDIEden.AutoCompleteControllerQualifier
    private transient CDIEden.AutoCompleteController _autoCompleteController;
    
    public String getAccountCompletion(){
    
    return this.accountCompletion;
    
    }
    
    public java.util.Vector<Entities.Tipo>getTipoArray(){
    
        return welcomeUserController.tipoArray();
    
    }
    
    public void setAccountCompletion(String accountCompletion){
    
    
    this.accountCompletion=accountCompletion;
    
    }
    
    public UpdatedTreeNode getSelectedNode(){
    
    return this.selectedNode;
    
    }
    
    public void setSelectedNode(UpdatedTreeNode selectedNode){
    
    this.selectedNode=selectedNode;
    
    }
    
    public double getLatitude(){
    
        return this.latitude;
    
    }
    
    
    public double getLongitude(){
    
        return this.longitude;
    
    }
    
    public void setLatitude(double latitude){
    
    
    this.latitude=latitude;
    
    }
    
    public void setLongitude(double longitude){
    
        this.longitude=longitude;
    
    }
    
    public TreeNode getRoot(){
    
        return this.root;
    
    }
    
    public void setRoot(TreeNode root){
    
    this.root=root;
    
    }
    
    public String getResults(){
    
    return this.results;
    
    }
    
    
    public void setResults(String results){
    
        this.results=results;
    
    }
    
    private String test;
    
    public String getTest(){
    
        return this.test;
    
    }
    
    public void setTest(String test){
    
    this.test=test;
    
    }
    
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
            
           this.welcomeUserController.initPreRenderView(this.getCurrentView());
        
        }
        catch(NullPointerException | StackOverflowError ex){
        
          ex.printStackTrace(System.out);
        
        }
    
    }
    
  
    public void init(){
        
   
        
    
        /*
        this.deplegableFranchise.getAll();
        
        
        
        for(Franquicia franchise:this.deplegableFranchise.getFranchises()){
            
        this.completeList.add(franchise.getNombre());
        
        }
        
        for(Entities.Producto producto:productoFacadeREST.findAll()){
        
        this.completeList.add(producto.getNombre());
        
        
        }
        
        System.out.print("Complete List size "+this.completeList.size());
    this.results="";
    /*
    root=new DefaultTreeNode("Categories",null);
    
    TreeNode food=new DefaultTreeNode("Food",root);
    
    for(Tipo type:typeFacadeREST.findAll()){
    
    TreeNode aux=new DefaultTreeNode(type.getNombre(),food);
    
    }
    
    
    
        
javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
    System.out.print("You are in welcomeuser");
    
   

    
     String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+this.getSession().getAttribute("username").toString();
     
     this.fileUpload.Remove(path);
   
    
   /* for(EdenLastSeen edenLastSeen:lastSeenController.lastSeenUser(loginbean.getLog())){
    
        if(edenLastSeen instanceof Entities.LastSeenProducto){
        
            Entities.LastSeenProducto lastSeenProducto=(Entities.LastSeenProducto)edenLastSeen;
           
            
                System.out.print("Authorization path "+path);
             
            for(Entities.Imagen image:lastSeenProducto.getProducto().getImagenCollection()){
            
                   
                this.fileUpload.loadImagesServletContext(image, loginbean.getLog().getUsername());
                
            }
        
        }
        
        else if(edenLastSeen instanceof Entities.LastSeenMenu){
        
        for(Entities.Imagen image: ((Entities.LastSeenMenu)edenLastSeen).getMenu().getImagenCollection())
        {
        
            this.fileUpload.loadImagesServletContext(image,loginbean.getLog().getUsername());
        
        }
        }
    
    }
    
    //loginBean login=(loginBean)getSession().getAttribute("loginBean");
    
     
   // fileUpload.Remove("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+getSession().getAttribute("username"));
    
    /*
    for(Item aux:loginAdministradorController.listLastSeen(getSession().getAttribute("username").toString()))
    {
    
       if(aux instanceof Entities.Producto){
       
           Entities.Producto product=(Entities.Producto)aux;
           
           System.out.print("Number of images producto "+product.getImagenCollection().size());
           
           for(Entities.Imagen image:product.getImagenCollection()){
            
         //    fileUpload.AddPhoto(image,"/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+getSession().getAttribute("username")+"",getSession().getAttribute("username").toString(),500,500);
           
        //     this.fileUpload.loadImagesServletContext(image);
    
           }
       
       }
       
       else if(aux instanceof Entities.Menu){
       
           Entities.Menu menu=(Entities.Menu)aux;
           
             System.out.print("Number of images Menu "+menu.getImagenCollection().size());
         
           
           for(Entities.Imagen image:((Entities.Menu)aux).getImagenCollection()){
           
            //          fileUpload.AddPhoto(image,"/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+getSession().getAttribute("username")+"",getSession().getAttribute("username").toString(),500,500);
     
                           }
       
       }
    
    }
    
    java.util.List<String>stringList=new java.util.ArrayList<String>();
    
    stringList.add("form2:graphicImage");
    
    stringList.add("form2:imageSwitch");
    
    
    
    
    RequestContext.getCurrentInstance().update("form2:imageSwitch");
    
    */
   
   
  
   
  
    
    }
    
    private String isComplete;
    
    public String getIsComplete(){
    
    return this.isComplete;
    
    }
    
    public void setIsComplete(String isComplete){
    
    this.isComplete=isComplete;
    
    }
    
    private String rootImage;
    
    public String getRootImage(){
    
    return this.rootImage;
    
    }
    
    public void setRootImage(String rootImage){
    
    this.rootImage=rootImage;
    
    }
    
    
    
    
    public List <String> autoCompleteMethod(String query){
    
        this.search=query;
        
        System.out.print("QUERY VALUE "+query);
        
      //_autoCompleteController.updateAutoCompleteList(query, ENUM.Role.User);
      
    return _autoCompleteController.getAutoComplete(query, Role.User);
    
    
    }
    
    @Inject 
    @CDIBeans.DesplegableFranchiseQualifier
    private DesplegableFranchiseInterface  deplegableFranchise;
            
           
    
    private List <String>completeList;
    public welcomeUser() {
    }
    
    private String search;
	@EJB()
	private ProductoFacadeREST productoFacadeREST;
	@Inject()
        @CDIBeans.FileUploadBeanQualifier
	private FileUploadInterface fileUpload;
    
    
    
    public void setSearch(String search){
    
    this.search=search;
    
    
    }
    
    public String getSearch(){
    
    return this.search;
    
    }
    
    private double similarity(String s1, String s2) {
  String longer = s1, shorter = s2;
  if (s1.length() < s2.length()) { // longer should always have greater length
    longer = s2; shorter = s1;
  }
  int longerLength = longer.length();
  if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
  return (longerLength - EditDistance.getEditDistance(longer, shorter)) / (double) longerLength;
}

    public String executeSearch(){
     try{
         
     
      
          System.out.print("RETURNED TRUE");
          
           return "results";
      
    
     
     }
     catch(Exception | StackOverflowError ex){
         
     return "welcomeuser";
     
     }
    }
    
    public void ajaxMethod(SelectEvent event){
      
        try{
        
            if(this.welcomeUserController.validLatitudeLongitude())
            {
            String selection=event.getObject().toString();
            
            System.out.print("SELECTION "+selection);
            
            this.longitude=this.welcomeUserController.getLongitudeValue();
            
            this.latitude=this.welcomeUserController.getLatitudeValue();
            
            this.search=selection;
            
            javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
            
            nh.handleNavigation(this.getContext(),null,"success");
            
            }
            
            else{
            
                java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("geolocation.WaitforLocation"));
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                this.getContext().addMessage(null, msg);
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
      
    }
    
 public void onNodeSelect(NodeSelectEvent event){
     
     System.out.print(event.getComponent().getClientId());
     
     if(event.getComponent().getClientId().equals("formTree:tree"))
     
     {
         
  int id=0;
 
 System.out.print("Node Selected");
 
 System.out.print(event.getTreeNode().getData().toString());
 
 id=(typeFacadeREST.findByName(event.getTreeNode().getData().toString())).getIdtipo();
 
 
 
 ExternalContext ex=getContext().getExternalContext();
 
 NavigationHandler nh = getContext().getApplication().getNavigationHandler();
 
 //String url = ex.encodeActionURL(getContext().getApplication().getViewHandler().getActionURL(getContext(), "/User/category.xhtml?filterData=name="+"laala"));
 
 nh.handleNavigation(getContext(), null, "/User/category.xhtml?faces-redirect=true&includeViewParams=true&id="+id+"");
 
 
 
    try{
        
 //ex.redirect(url);
 
    }
    
    catch(EJBException exx){
    
        Logger.getLogger(welcomeUser.class.getName()).log(Level.SEVERE,null,exx);
        
        FacesMessage msg=new FacesMessage("Exception Caugth");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
    
    }
    
   
    
 }
    
    else{
            
            
            }
 
 }
 
 public boolean load(){
 
     Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
     
     
     for(Map.Entry<String,String>entry:requestMap.entrySet()){
     
     
     System.out.print(entry.getKey()+"-"+entry.getValue());
     
     }
     
    
     
     Map<String,Object>sessionMap=getContext().getExternalContext().getSessionMap();
     
     for(Map.Entry<String,Object>entry:sessionMap.entrySet()){
     
     
     System.out.print(entry.getKey()+"-"+entry.getValue());
     
     
     
     
     }
     
     loginBean login=(loginBean)sessionMap.get("loginBean");
     
     System.out.print("Login Confirmation "+login.getConfirmation());
     
 
     
     
    
    
     
 return login.getLog().getConfirmation().getConfirmed();
 }
    
 public String deleteAccount(){
 
      loginBean log=(loginBean)getSession().getAttribute("loginBean");
     
     if(appGroupFacadeREST.usernameCounter(getSession().getAttribute("username").toString())>1){
     
         
         
         appGroupFacadeREST.remove(new PathSegmentImpl("bar;groupid=guanabara_user;loginAdministradorusername="+getSession().getAttribute("username")+""));
         
     
     }
     else if(appGroupFacadeREST.usernameCounter(getSession().getAttribute("username").toString())==1){
     
         System.out.print("Item to delete "+log.getLog().getUsername());
         
         
     
         log.deleteLogin(log.getLog());
        
         
     }
      
       
    
          
 return  log.cerrarSesion();
     
 }

	public java.util.List<String> getCompleteList() {
		return this.completeList;
	}

	public void setCompleteList(java.util.List<String> completeList) {
		this.completeList = completeList;
	}

	/**
	 * 
	 * @param event
	 */
	public void actionEvent(javax.faces.event.ActionEvent event) {
	
        try{
        
            System.out.print("ACTION EVENT");
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
        }

	/**
	 * 
	 * @param event
	 */
	public void logout(javax.faces.event.AjaxBehaviorEvent event) {
		
            try{
            
                this.welcomeUserController.logout();
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}
 

 
}
