package EdenFinalUser.Search.Results.Classes;

public interface ResultsLayout<T> {

	T getItem();

	/**
	 * 
	 * @param item
	 */
	void setItem(T item);

	String getName();

	/**
	 * 
	 * @param name
	 */
	void setName(String name);

	String getImage();

	/**
	 * 
	 * @param image
	 */
	void setImage(String image);

	double getSimilarity();

	/**
	 * 
	 * @param similarity
	 */
	void setSimilarity(double similarity);

	ResultsType getResultsType();

	/**
	 * 
	 * @param resultsType
	 */
	void setResultsType(ResultsType resultsType);

	String getLink();

	/**
	 * 
	 * @param link
	 */
	void setLink(String link);
}