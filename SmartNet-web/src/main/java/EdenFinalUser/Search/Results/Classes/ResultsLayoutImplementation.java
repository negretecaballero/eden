package EdenFinalUser.Search.Results.Classes;

public class ResultsLayoutImplementation<T> implements ResultsLayout<T> {

	private T item;
	private String name;
	private String image;
	private double similarity;
	private ResultsType attribute;
	private String link;
	private ResultsType attribute2;
	private ResultsType attribute3;
	private ResultsType _resultsType;

        @Override
	public T getItem() {
		return this.item;
	}
        @Override
	public void setItem(T item) {
		this.item = item;
	}
        @Override
	public String getName() {
		return this.name;
	}
        @Override
	public void setName(String name) {
		this.name = name;
	}
        @Override
	public String getImage() {
		return this.image;
	}
        @Override
	public void setImage(String image) {
		this.image = image;
	}

	public ResultsLayoutImplementation() {
		// TODO - implement ResultsLayoutImplementation.ResultsLayoutImplementation
		//throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * @param image
	 */
	public ResultsLayoutImplementation(String image) {
		// TODO - implement ResultsLayoutImplementation.ResultsLayoutImplementation
		//throw new UnsupportedOperationException();
	}

	@Override
	public double getSimilarity() {
		return this.similarity;
	}

	/**
	 * 
	 * @param similarity
	 */
	@Override
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	@Override
	public ResultsType getResultsType() {
		return _resultsType;
	}

	/**
	 * 
	 * @param resultsType
	 */
	@Override
	public void setResultsType(ResultsType resultsType) {
		
            _resultsType=resultsType;
            
	}

	@Override
	public String getLink() {
		
            return this.link;
            
	}

	/**
	 * 
	 * @param link
	 */
	@Override
	public void setLink(String link) {
		
            this.link=link;
            
	}

}