package EdenFinalUser.Search.Results.Classes;

public enum ResultsType {
    
    Franchise,
	Type,
	Category,
	Product,
	Meal,
	Addition
    
}