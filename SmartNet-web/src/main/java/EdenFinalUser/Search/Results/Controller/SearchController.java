/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Search.Results.Controller;

/**
 *
 * @author luisnegrete
 */
public interface SearchController {
    
    java.util.Vector<EdenFinalUser.Classes.SearchResultLayout>franchiseResults();

	/**
	 * 
	 * @param bundle
	 * @param viewId
	 */
	void initPreRenderView(java.util.ResourceBundle bundle, String viewId);

	void postConstruct();

	/**
	 * 
	 * @param bundle
	 */
	void filter(java.util.ResourceBundle bundle);

	void search();
    
  
    
}
