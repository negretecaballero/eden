/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Search.Results.Controller;

import Clases.EdenString;
import EdenFinalUser.Search.Results.Classes.ResultsType;
import Stereotypes.DependentStereotype;
import EdenFinalUser.Search.Results.Classes.*;
import Controllers.*;
import CDIBeans.*;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative

public class SearchControllerImplementation implements SearchController {
    
    @javax.inject.Inject private CDIEden.AutoCompleteController _autoCompleteController;
    
    @javax.inject.Inject private CDIBeans.FileUploadInterface _fileUploadBean;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private Controllers.FranchiseSingletonController _franchiseController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private CDIBeans.FileController _fileController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private Controllers.FranchiseTypeSingletonController typeController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private Controllers.FranchiseSubtypeController subtypeController;
	@javax.inject.Inject
	private CategorySingletonController _categoryController;
	@javax.inject.Inject
	private ProductSingletonControllerImplementation _productSingletonController;
	@javax.inject.Inject
	private MenuSingletonController _menuSingletonController;
	@javax.inject.Inject
	private MenuController _menuController;
	@javax.inject.Inject
	private AdditionSingletonController _additionSingletonController;
    
    
    @Override
    public java.util.Vector<EdenFinalUser.Classes.SearchResultLayout>franchiseResults(){
    
        try{
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username"));
            
            java.util.Vector results=new java.util.Vector<Entities.Franquicia>(_autoCompleteController.getObjectAutoComplete().size());
            
            for(Object aux:this._autoCompleteController.getObjectAutoComplete()){
            
            if(aux instanceof Entities.Franquicia){
            
               EdenFinalUser.Classes.SearchResultLayout auxi=new EdenFinalUser.Classes.SearchResultLayoutImplementation();
               
               Entities.Franquicia franchise=(Entities.Franquicia)aux;
               
               auxi.setFranchise((Entities.Franquicia)aux);
               
               if(franchise.getImagenIdimagen()!=null){
               
                   _fileUploadBean.loadImagesServletContext(franchise.getImagenIdimagen(), session.getAttribute("username").toString());
                   
                   String path=_fileUploadBean.getImages().get(_fileUploadBean.getImages().size()-1);
                   
                   auxi.setLogo(path);

                   _fileUploadBean.getImages().remove(_fileUploadBean.getImages().size()-1);
                   
                   _fileUploadBean.getPhotoList().remove(_fileUploadBean.getPhotoList().size()-1);
                   
                   System.out.print("LOGO IS NOT NULL "+auxi.getLogo());
               
               }
               else{
               
                   auxi.setLogo("/images/noImage.png");
                   
                   System.out.print("AUXI IS NULL");
               
               }
               
               results.add(auxi);
            
            }
            
            }
            
            return results;
        
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }

	/**
	 * 
	 * @param bundle
	 * @param viewId
	 */
	@Override
	public void initPreRenderView(java.util.ResourceBundle bundle, String viewId) {
		try{
                
                       javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                    
                    EdenFinalUser.Search.Results.View.SearchView searchView=(EdenFinalUser.Search.Results.View.SearchView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenFinalUserSearchView");

       
                 javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                            
      
                    
                    if(!searchView.isInitialized())
                    {     
                      
                         if(viewRoot.findComponent("west-form:franchises") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                            
                            System.out.print("SELECTBOOLEANCHECKBOX FOUND");
                            
                            org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox box=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:franchises");
                            
                            box.setSelected(true);
                            
                            }
                         
                          if(viewRoot.findComponent("layout")instanceof org.primefaces.component.layout.Layout){
                            
                     
                               org.primefaces.component.layout.Layout layout=(org.primefaces.component.layout.Layout)viewRoot.findComponent("layout");
                               
                               if(layout.getChildren()!=null && !layout.getChildren().isEmpty()){
                               
                               for(javax.faces.component.UIComponent component:layout.getChildren()){
                               
                                 
                               if(component.getClientId().equals("west") && component instanceof org.primefaces.component.layout.LayoutUnit){
                               
                                org.primefaces.component.layout.LayoutUnit unit=(org.primefaces.component.layout.LayoutUnit)component;
                                
                                unit.setVisible(true);
                               
                               }
                               
                               }
                               
                               }
                            
                            }
                        
                          this._fileController.Remove();
                
                          searchView.setSearch(this.searchValue());
                        
                           searchView.getResultList().clear();
                           
                           //java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout> list=this.addFilterToList(bundle,ResultsType.Franchise, searchView.getSearch());
                      
                           searchView.setResultList(this.addFilterToList(bundle,ResultsType.Franchise, searchView.getSearch()));
                           
                         
                           
                           searchView.setInitialized(true);
                           
                    }
                    
                    
                      if(searchView.getResultList()==null || searchView.getResultList().size()==0 || searchView.getResultList().isEmpty()){
                           
                               searchView.setEmpty(true);
                           
                           }
                      else{
                      
                          searchView.setEmpty(false);
                      
                      }
                    
                }
                
                catch(NullPointerException ex){
                
                    ex.printStackTrace(System.out);
                    
                }
	}

	private final String searchValue() {
		
            try{
            
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                
                
                if(request.getParameter("search")!=null){
                
                System.out.print("SEARCH "+request.getParameter("search"));
                
                return request.getParameter("search");
                
                }
                
                return "";
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
                return "";
            
            }
            
	}

	@Override
	public void postConstruct() {
		
            try{
            
                this._fileController.Remove();
                
      
                
                 javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                            
                            if(viewRoot.findComponent("west-form:franchises") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                            
                            System.out.print("SELECTBOOLEANCHECKBOX FOUND");
                            
                            org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox box=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:franchises");
                            
                            box.setSelected(true);
                            
                            }
                            
                            if(viewRoot.findComponent("layout")instanceof org.primefaces.component.layout.Layout){
                            
                     
                               org.primefaces.component.layout.Layout layout=(org.primefaces.component.layout.Layout)viewRoot.findComponent("layout");
                               
                               if(layout.getChildren()!=null && !layout.getChildren().isEmpty()){
                               
                               for(javax.faces.component.UIComponent component:layout.getChildren()){
                               
                                 
                               if(component.getClientId().equals("west") && component instanceof org.primefaces.component.layout.LayoutUnit){
                               
                                org.primefaces.component.layout.LayoutUnit unit=(org.primefaces.component.layout.LayoutUnit)component;
                                
                                unit.setVisible(true);
                               
                               }
                               
                               }
                               
                               }
                            
                            }
                            
                            
                             
                             
                            
                             
                           

            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param bundle
	 * @param type
	 * @param search
	 */
	private java.util.List<ResultsLayout> addFilterToList(java.util.ResourceBundle bundle, ResultsType type, String search) {
		
            try{
            
            
                System.out.print("SEARCH VALUE ADD FILTER "+search);
                
                
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                           
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                      
                java.util.Comparator< EdenFinalUser.Search.Results.Classes.ResultsLayout> comparator=new comparator.ResultsTypeComparator();
                    
                
                switch(type){
                
                    
                    
                    case Franchise:{
                
                         
                          
                             
                              java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout> list=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                        
                          for(Entities.Franquicia franchise:_franchiseController.getFranchises()){
                              
                                  //Similarity Coincidence 
                              if(Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), franchise.getNombre().toLowerCase(), 0, 0,0)>0.9 && !this.contains(list, franchise))
                              
                              {
                          
                          EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Franquicia>result=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Franquicia>();
                          
                          result.setItem(franchise);
                          
                          result.setName(franchise.getNombre());
                          
                          result.setResultsType(ResultsType.Franchise);
                          
                          result.setSimilarity(Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), franchise.getNombre().toLowerCase(), 0, 0,0));
  
                          result.setLink(bundle.getString("EdenFinalUser.Search.Results.ResultsPanel.GotoStore"));
                          
                          if(franchise.getImagenIdimagen()!=null){
                          
                              
                             Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                             
                              
                             java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                          
                             if(file.exists()){
                             
                                 filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                             
                             }
                             else{
                             
                                 filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator, franchise.getNombre());
                             
                             }
                             
                             _fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                             
                             String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                             
                             result.setImage(image);
                             
                             _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                             
                             _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                             
                          }
                          else{
                          
                          result.setImage("/images/noImage.png");
                          
                          }
                          
                              list.add(result);
                          
                              }
                              

                          }
                          
                          
                         if(list!=null && !list.isEmpty())
                         {
                            try{
                            
                            list.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                         }
                         
                         
                          java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout> typeList=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                          
                          for(Entities.Tipo tipo:this.typeController.getFranchiseTypeArray()){
                          
                              
                              //Similarity coefficient
                              if(EdenString.AccariedSimilatiry(search.toLowerCase(), tipo.getNombre().toLowerCase(), 0, 0,0)>0.9)
                              {
                              
                                  for(Entities.Subtype subtype:tipo.getSubtypeCollection()){
                                  
                                      for(Entities.Franquicia franchise:subtype.getFranquiciaCollection()){
                                      
                                          if(!this.contains(list, franchise)){
                                          
                                              EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Franquicia>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Franquicia>();
                                          
                                              item.setName(franchise.getNombre());
                                              
                                              item.setLink(bundle.getString("EdenFinalUser.Search.Results.ResultsPanel.GotoStore"));
                        
                                              item.setSimilarity(EdenString.AccariedSimilatiry(search.toLowerCase(), tipo.getNombre().toLowerCase(), 0, 0,0));
                                          
                                              item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Type);
                                              
                                              item.setItem(franchise);
                                              
                                              if(franchise.getImagenIdimagen()==null){
                                              
                                                  item.setImage("/images/noImage.png");
                                              
                                              }
                                              else{
                                              
                                                  java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                              
                                                  
                                                  Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                                                  
                                                  if(file.exists())
                                                  {
                                                      
                                                  filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                                  
                                                  }
                                                  
                                                  else{
                                                  
                                                      filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator, franchise.getNombre());
                                                  
                                                  }
                                                  
                                                  _fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                             
                                              String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                                              
                                              item.setImage(image);
                                              
                                              _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                                              
                                              _fileController.getImageList().remove( _fileController.getImageList().size()-1);
                                              
                                              }
                                              
                                              typeList.add(item);
                                          
                                          }
                                      
                                      }
                                  
                                  }
                              
                              }
                              
                          }
                          
                          
                           
                            try{
                            
                            typeList.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                          
                           if(typeList!=null && !typeList.isEmpty()){
                           
                            for(EdenFinalUser.Search.Results.Classes.ResultsLayout item:typeList){
                            
                                list.add(item);
                            
                            }
                           
                           }
                           
                           java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>subtypeList=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                           
                           for(Entities.Subtype subtype:this.subtypeController.getSubtypeArray()){
                           
                           if(EdenString.AccariedSimilatiry(search.toLowerCase(), subtype.getName().toLowerCase(), 0, 0,0)>0.9)
                           {
                           
                               for(Entities.Franquicia franchise:subtype.getFranquiciaCollection()){
                               
                                  if(!this.contains(list, franchise)){
                                  
                                      EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Franquicia>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Franquicia>();
                                  
                                        item.setName(franchise.getNombre());
                                              
                                              item.setLink(bundle.getString("EdenFinalUser.Search.Results.ResultsPanel.GotoStore"));
                        
                                              item.setSimilarity(EdenString.AccariedSimilatiry(search.toLowerCase(), subtype.getName().toLowerCase(), 0, 0,0));
                                          
                                              item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Type);
                                              
                                              item.setItem(franchise);
                                              
                                              if(franchise.getImagenIdimagen()==null){
                                              
                                                  item.setImage("/images/noImage.png");
                                              
                                              }
                                              else{
                                  
                                                  java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                  
                                                  Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                                                  
                                                  if(file.exists()){
                                                  
                                                     filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                                  
                                                  }
                                                  
                                                  else{
                                                  
                                                      filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator,franchise.getNombre());
                                                  
                                                  }
                                                  
                                                  _fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+"Franchises"+java.io.File.separator+franchise.getNombre());
                                                 
                                                  
                                                  String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                                                  
                                                  item.setImage(image);
                                                  
                                                  _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                                                  
                                                  _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                                                  
                                              }
                                              
                                      subtypeList.add(item);
                                  } 
                               
                               }
                           
                           }
                           
                           }
                           
                           
                             try{
                            
                            subtypeList.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                          
                           
                           if(subtypeList!=null && !subtypeList.isEmpty()){
                           
                             for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:subtypeList){
                             
                                 list.add(aux);
                             
                             }
                           
                           }
                           
                           java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>categoryList=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                          
                        for(Entities.Categoria category:this._categoryController.getCategories()){
                        
                            System.out.print("Category Name "+category.getNombre()+" similarity "+Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), category.getNombre().toLowerCase(), 0, 0, 0));
                            
                            if(category.getFranquicia()!=null){
                            
                                System.out.print(category.getNombre()+" is not null");
                            
                            }
                            
                            else{
                            
                                System.out.print(category.getNombre()+" is null");
                            
                            }
                            
                            
                            if(!this.contains(list, category.getFranquicia()) && (Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), category.getNombre().toLowerCase(), 0, 0, 0)>.9) && category.getFranquicia()!=null){
                            
                                EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Franquicia>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Franquicia>();
                           
                                item.setItem(category.getFranquicia());
                                
                                item.setName(category.getFranquicia().getNombre());
                                
                                item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Category);
                                
                                item.setLink(bundle.getString("EdenFinalUser.Search.Results.ResultsPanel.GotoStore"));
                                
                                item.setSimilarity(Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), category.getNombre().toLowerCase(), 0, 0, 0));
                                
                                if(category.getImagenCollection()!=null && !category.getImagenCollection().isEmpty()){
                                
                                   java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)category.getImagenCollection();
                                   
                                   java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre()+category.getFranquicia().getNombre());
                                    
                                   Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                                   
                                   if(file.exists()){
                                   
                                       filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre()+category.getFranquicia().getNombre());
                                   
                                   }
                                   else{
                                   
                                   filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Category"+java.io.File.separator, category.getNombre()+category.getFranquicia().getNombre());
                                   
                                   }
                                   
                                   
                                   _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, images.size())), session.getAttribute("username").toString()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre()+category.getFranquicia().getNombre());
                                
                                   String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                                   
                                   item.setImage(image);
                                   
                                   _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                                   
                                   _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                                   
                                }
                                else{
                                
                                   item.setImage("/images/noImage.png");
                                
                                }
                                
                                categoryList.add(item);
                                
                            }
                        
                        } 
                        
                        
                         
                          try{
                            
                            categoryList.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                           
                           if(categoryList!=null && !categoryList.isEmpty()){
                           
                             for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:categoryList){
                             
                                 list.add(aux);
                             
                             }
                           
                           }
                           
                    return list;
                     
                    }
                    
                    
                    
                    case Product:{
                    
                       System.out.print("YOU ARE LOOKING FOR A PRODUCT");
                                                
                       java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>list=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                        
                        if(this._productSingletonController.getProducts()!=null && !this._productSingletonController.getProducts().isEmpty())
                        {
                        
                            for(Entities.Producto product:this._productSingletonController.getProducts()){
                            
                            if(!this.contains(list, product) && (Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), product.getNombre().toLowerCase(), 0, 0, 0)>.9)){
                            
                                EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Producto>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Producto>();
                            
                                item.setName(product.getNombre());
                                
                                item.setItem(product);
                                
                                item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Product);
                                
                                item.setSimilarity(Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), product.getNombre().toLowerCase(), 0, 0, 0));
                               
                                if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                                
                                    Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                                    
                                    java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre());
                                
                                    if(!file.exists()){
                                    
                                    filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator,"Product"+java.io.File.separator+product.getNombre());
                                    
                                    }
                                    else{
                                    
                                    filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre());
                                    
                                    }
                                    
                                    java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)product.getImagenCollection();
                                    
                                    _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, (images.size()-1))), session.getAttribute("username").toString()+java.io.File.separator+"Product"+java.io.File.separator+product.getNombre());
                                    
                                    String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                                    
                                    item.setImage(image);
                                    
                                    _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                                    
                                    _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                                    
                                    
                                }
                                
                                else{
                                
                                    item.setImage("/images/noImage.png");
                                
                                }
                                
                                item.setLink(bundle.getString("EdenFinalUser.categories.gotoproduct"));
                               
                                
                                list.add(item);
                                
                            }
                            
                            }
                            
                        }
                        
                        if(list!=null && !list.isEmpty()){
                        
                         try{
                            
                            list.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                        
                        }
                        
                       
                        
                        return list;
                        
                    }
                    
                    case Meal:{
                    
                     java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>list=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                    
                     if(_menuSingletonController.getMenuList()!=null && !_menuSingletonController.getMenuList().isEmpty()){
                     
                         for(Entities.Menu menu:_menuSingletonController.getMenuList()){
                         
                         System.out.print("Menu Name "+menu.getNombre());
                       
                         if(this._menuController.getFranchise(menu)!=null && !this.contains(list, menu) && (Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), menu.getNombre().toLowerCase(), 0, 0, 0)>.9)){
                         
                         EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Menu>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Menu>();
                         
                         item.setName(menu.getNombre());
                         
                         item.setSimilarity(Clases.EdenString.AccariedSimilatiry(search.toLowerCase(), menu.getNombre().toLowerCase(), 0, 0, 0));
                         
                         item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Meal);
                         
                         item.setLink(bundle.getString("EdenFinalUser.categories.gotomenu"));
                         
                         item.setItem(menu);
                         
                         if(menu.getImagenCollection()!=null && !menu.getImagenCollection().isEmpty()){
                         
                             java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+this._menuController.getFranchise(menu).getNombre()+java.io.File.separator+"Meal"+java.io.File.separator+menu.getNombre());
                         
                             Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                             
                             if(file.exists()){
                             
                                 filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+this._menuController.getFranchise(menu).getNombre()+java.io.File.separator+"Meal"+java.io.File.separator+menu.getNombre());
                             
                             }
                             else{
                             
                                 filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+this._menuController.getFranchise(menu).getNombre()+java.io.File.separator+"Meal"+java.io.File.separator, menu.getNombre());
                             
                             }
                             
                             java.util.List<Entities.Imagen>imageList=(java.util.List<Entities.Imagen>)menu.getImagenCollection();
                             
                             _fileController.serverDownload(imageList.get(Clases.EdenNumber.random(0, imageList.size()-1)), session.getAttribute("username").toString()+java.io.File.separator+_menuController.getFranchise(menu).getNombre()+java.io.File.separator+menu.getNombre());
                             
                             String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                             
                             item.setImage(image);
                             
                             _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                             
                             _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                             
                         }
                         else{
                         
                         item.setImage("/images/noImage.png");
                         
                         }
                         
                         list.add(item);
                         
                         }
                          
                         
                         }
                     
                     }
                     
                     try{
                            
                            list.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                     return list;
                     
                    }
                    
                    
                    case Addition:{
                    
                        java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>list=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                    
                        System.out.print("ADDITIONS SIZE "+this._additionSingletonController.getAdditions().size());
                        
                        if(this._additionSingletonController.getAdditions()!=null && !this._additionSingletonController.getAdditions().isEmpty())
                        {
                        
                            for(Entities.Addition addition:this._additionSingletonController.getAdditions()){
                            
                                System.out.print("ADDITION NAME "+addition.getName());
                                
                                if((Clases.EdenString.AccariedSimilatiry(addition.getName().toLowerCase(), search.toLowerCase(), 0, 0, 0)>.9)){
                                
                                    for(Entities.Producto product:addition.getProductoCollection()){
                                    
                                        if(!this.contains(list, product)){
                                        
                                            EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Producto>item=new EdenFinalUser.Search.Results.Classes.ResultsLayoutImplementation<Entities.Producto>();
                                        
                                            item.setItem(product);
                                            
                                            item.setName(product.getNombre());
                                            
                                            item.setSimilarity(Clases.EdenString.AccariedSimilatiry(addition.getName().toLowerCase(), search.toLowerCase(), 0, 0, 0));
                                            
                                            item.setResultsType(EdenFinalUser.Search.Results.Classes.ResultsType.Addition);
                                            
                                            item.setLink(bundle.getString("EdenFinalUser.categories.gotoproduct"));
                                            
                                            if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                                            
                                                java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+addition.getFranquicia().getNombre()+java.io.File.separator+addition.getName()+java.io.File.separator+product.getNombre());
                                            
                                                Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                                                
                                                if(!file.exists()){
                                                
                                                  filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+addition.getFranquicia().getNombre()+java.io.File.separator+addition.getName()+java.io.File.separator, product.getNombre());
                                          
                                                }
                                                                                            
                                                java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)product.getImagenCollection();
                                                
                                                _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, images.size()-1)), session.getAttribute("username").toString()+java.io.File.separator+addition.getFranquicia().getNombre()+java.io.File.separator+addition.getName()+java.io.File.separator+product.getNombre());
                                                
                                                String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                                                
                                                item.setImage(image);
                                                
                                                _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                                                
                                                _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                                                
                                            }
                                            else{
                                            
                                                item.setImage("/images/noImage.png");
                                            
                                            }
                                            
                                            list.add(item);
                                            
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        }
                        
                        if(list!=null && !list.isEmpty()){
                        
                            try{
                            
                            list.sort(comparator);
                            
                            }
                            catch(java.lang.NoSuchMethodError ex){
                            
                                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                            
                            }
                        
                        }
                        
                        return list;
                        
                    }
                    
                    default:{
                    
                        return null;
                    
                    }
                
                
                }
            
            }
            catch(NullPointerException  ex){
            
                ex.printStackTrace(System.out);
            
                return null;
            }
            
	}

	/**
	 * 
	 * @param list
	 * @param item
	 */
	public boolean contains(java.util.List<ResultsLayout> list, Object item) {
	
            try{
            
                
                if(list!=null && !list.isEmpty()){
                
                for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:list){
                
                    if(aux.getItem().equals(item)){
                    
                    return true;
                    
                    }
                
                }
                
                }
                
                return false;
            
            }
            
            catch(NullPointerException ex){
            
                return false;
            
            }
            
	}

	/**
	 * 
	 * @param bundle
	 */
	@Override
	public void filter(java.util.ResourceBundle bundle) {
	
            try{
            
                _fileController.Remove();
                
                
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                
                java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout> results=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
            
                EdenFinalUser.Search.Results.View.SearchView searchView=(EdenFinalUser.Search.Results.View.SearchView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenFinalUserSearchView");

              searchView.getResultList().clear();
                
                if(viewRoot.findComponent("west-form:franchises") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox franchises=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:franchises");                   
                                  
                
                if(franchises.isSelected()){
                
                         
                 for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle,EdenFinalUser.Search.Results.Classes.ResultsType.Franchise,searchView.getSearch()))
                 {
                 
                     results.add(aux);
                 
                 }
                 
                 
                }
                
                
                
               
                
                }
                
                
              
                
                if(viewRoot.findComponent("west-form:product") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox product=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:product");
                    
                    if(product.isSelected()){
                    
                         for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle,ResultsType.Product, searchView.getSearch()))
                         {
                
                              results.add(aux);
                    
                          }
                    
                    }
                
                }
                
                if(viewRoot.findComponent("west-form:meal") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox mealBox=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:meal");
                    
                    if(mealBox.isSelected()){
                    
                    for(EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Menu>aux:this.addFilterToList(bundle, ResultsType.Meal, searchView.getSearch()))
                    {
                    
                        results.add(aux);
                    
                    }
                    
                    
                    }
                
                }
                
                
                if(viewRoot.findComponent("west-form:addition")instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox additionBox=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:addition");
                
                    
                    if(additionBox.isSelected()){
                    
                    for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle, ResultsType.Addition, searchView.getSearch()))
                    {
                    
                        results.add(aux);
                        
                    }
                    
                    
                    }
                    
                }
                
                  searchView.setResultList(results);
                
                  if(searchView.getResultList()==null || searchView.getResultList().size()==0 || searchView.getResultList().isEmpty()){
                  
                      searchView.setEmpty(true);
                  
                  }
                  else{
                  
                      searchView.setEmpty(false);
                  
                  }
                  
                  this.trimResultsList();
               
                 // this.sendJSONResponse();
                    
            }
            catch(NullPointerException | java.lang.NoSuchMethodError ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	private void  sendJSONResponse() {
            
            javax.faces.context.FacesContext context=javax.faces.context.FacesContext.getCurrentInstance();
            
try{
            /*
                javax.servlet.http.HttpServletResponse response=(javax.servlet.http.HttpServletResponse)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getResponse();
            
                 response.setContentType("text/html;charset=UTF-8");
                 
                 try(PrintWriter out = response.getWriter()){
                 
                      response.setContentType("text/html");
                      response.setCharacterEncoding("utf-8");
                      
                      JsonObject value = Json.createObjectBuilder().add("id", "Locale").add("Value", "Luis").build();
                      
                        out.print(value.toString());
                 
                 } 
                 
                 
                 catch (IOException ex) {
            Logger.getLogger(SearchControllerImplementation.class.getName()).log(Level.SEVERE, null, ex);
                    
                    
        }
                
            
            
*/
            
            javax.faces.context.ExternalContext ex=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
            
           // ex.dispatch("/ResultsServlet");
            
}
catch(NullPointerException  ex){

    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    ex.printStackTrace(System.out);

}
            
finally{

 context.responseComplete();   

}



            
	}

	@Override
	public void search() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.Search.Results.View.SearchView searchView=(EdenFinalUser.Search.Results.View.SearchView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenFinalUserSearchView");
            
                java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>results=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                
                if(searchView!=null && searchView.getSearch()!=null){
                
                System.out.print("SEARCH VALUE "+searchView.getSearch());
                
                _fileController.Remove();
                
                javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
              searchView.getResultList().clear();
                
                if(viewRoot.findComponent("west-form:franchises") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox franchises=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:franchises");                   
                                  
                
                if(franchises.isSelected()){
                
                         
                 for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle,EdenFinalUser.Search.Results.Classes.ResultsType.Franchise,searchView.getSearch()))
                 {
                 
                     results.add(aux);
                 
                 }
                 
                 
                }
                
                
                
               
                
                }
                
                
              
                
                if(viewRoot.findComponent("west-form:product") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox product=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:product");
                    
                    if(product.isSelected()){
                    
                         for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle,ResultsType.Product, searchView.getSearch()))
                         {
                
                              results.add(aux);
                    
                          }
                    
                    }
                
                }
                
                if(viewRoot.findComponent("west-form:meal") instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox mealBox=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:meal");
                    
                    if(mealBox.isSelected()){
                    
                    for(EdenFinalUser.Search.Results.Classes.ResultsLayout<Entities.Menu>aux:this.addFilterToList(bundle, ResultsType.Meal, searchView.getSearch()))
                    {
                    
                        results.add(aux);
                    
                    }
                    
                    
                    }
                
                }
                
                
                if(viewRoot.findComponent("west-form:addition")instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
                
                    org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox additionBox=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox)viewRoot.findComponent("west-form:addition");
                
                    
                    if(additionBox.isSelected()){
                    
                    for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:this.addFilterToList(bundle, ResultsType.Addition, searchView.getSearch()))
                    {
                    
                        results.add(aux);
                        
                    }
                    
                    
                    }
                    
                }
                
                  searchView.setResultList(results);
                
                  if(searchView.getResultList()==null || searchView.getResultList().size()==0 || searchView.getResultList().isEmpty()){
                  
                      searchView.setEmpty(true);
                  
                  }
                  else{
                  
                      searchView.setEmpty(false);
                  
                  }
               
                  this.trimResultsList();
                
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace();
            
            }
            
	}

	private void trimResultsList() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
                EdenFinalUser.Search.Results.View.SearchView searchView=(EdenFinalUser.Search.Results.View.SearchView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenFinalUserSearchView");
                
                if(searchView.getResultList()!=null && !searchView.getResultList().isEmpty()){
                
                    java.util.List<EdenFinalUser.Search.Results.Classes.ResultsLayout>backedResults=new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
                
                    backedResults=searchView.getResultList();
                    
                    for(EdenFinalUser.Search.Results.Classes.ResultsLayout item:searchView.getResultList()){
                    
                        int cont=0;
                        
                        for(EdenFinalUser.Search.Results.Classes.ResultsLayout aux:backedResults){
                        
                            if(item.getItem().equals(aux.getItem())){
                            
                                if(cont>0){
                                
                                   backedResults.remove(aux);
                                   
                                   break;
                                
                                }
                                cont++;
                            
                            }
                        
                        }
                    
                    }
                    
                    searchView.setResultList(backedResults);
                    
                }
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}
}
