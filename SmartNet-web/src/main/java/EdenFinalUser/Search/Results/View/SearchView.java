/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenFinalUser.Search.Results.View;

import EdenFinalUser.Search.Results.Classes.ResultsLayout;
import CDIEden.*;
import ENUM.Role;

/**
 *
 * @author luisnegrete
 */
public class SearchView extends Clases.BaseBacking{
    
    @javax.inject.Inject
	@javax.enterprise.inject.Default private EdenFinalUser.Search.Results.Controller.SearchController _searchController;
    
    private java.util.Vector<EdenFinalUser.Classes.SearchResultLayout>results;
	private java.util.List<ResultsLayout> resultList = new java.util.ArrayList<EdenFinalUser.Search.Results.Classes.ResultsLayout>();
	private boolean initialized = false;
	private String search;
	private boolean empty;
	@javax.inject.Inject
	private AutoCompleteControllerImplementation _autoCompleteController;
    
		
    
    
    
public java.util.List<EdenFinalUser.Classes.SearchResultLayout>getResults(){

    return this.results;

}

public void setResults(java.util.Vector<EdenFinalUser.Classes.SearchResultLayout>results){

    this.results=results;

}
    
    
    //PreRenderView event
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
           
       // this.results=this._searchController.franchiseResults();
            
           
            
         this._searchController.initPreRenderView(this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle"),this.getCurrentView());
            
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }

	
	public java.util.List<ResultsLayout> getResultList() {
		return this.resultList;
	}

	public void setResultList(java.util.List<ResultsLayout> resultList) {
		this.resultList = resultList;
	}

	public boolean isInitialized() {
		return this.initialized;
	}

	public void setInitialized(boolean initialized) {
		this.initialized = initialized;
	}

	/**
	 * 
	 * @param event
	 */
	public void filter(javax.faces.event.AjaxBehaviorEvent event) {
	
            try{
            
                this._searchController.filter(this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle"));
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	public String getSearch() {
		return this.search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * 
	 * @param ajaxEvent
	 */
	public void checkEvent(javax.faces.event.AjaxBehaviorEvent ajaxEvent) {
		
            
            try{
            
                System.out.print(ajaxEvent.getComponent().getClientId());
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
            
	}

	public boolean getIsEmpty() {
		return this.empty;
	}

	/**
	 * 
	 * @param empty
	 */
	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public boolean getEmpty() {
		return this.empty;
	}

	/**
	 * 
	 * @param query
	 */
	public java.util.List<String> autoComplete(String query) {
		try{
                
                    this.search=query;
                    
                    return this._autoCompleteController.getAutoComplete(query, Role.User);
                    
                }
                catch(NullPointerException ex){
                
                    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                    return null;
                }
	}

	/**
	 * 
	 * @param event
	 */
	public void autoCompleteSelect(javax.faces.event.AjaxBehaviorEvent event) {
		
            try{
            
                System.out.print("SEARCH VALUE "+this.search);
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param event
	 */
	public void searchOperation(javax.faces.event.AjaxBehaviorEvent event) {
		
            try{

                
                this._searchController.search();
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}
    
}
