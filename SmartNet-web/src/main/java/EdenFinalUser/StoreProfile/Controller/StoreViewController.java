package EdenFinalUser.StoreProfile.Controller;

public interface StoreViewController {

	void initPreRenderView();

	boolean checkRestaurant();

	/**
	 * 
	 * @param marker
	 */
	void updateInfoWindow(org.primefaces.model.map.Marker marker);
}