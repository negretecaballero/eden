package EdenFinalUser.StoreProfile.Controller;

import Clases.ViewLayout.*;
import org.apache.commons.collections.IteratorUtils;
import CDIBeans.*;
import Clases.Maps.*;



@javax.enterprise.inject.Alternative
@javax.enterprise.context.Dependent
@javax.inject.Named("storeViewController")

public class StoreViewControllerImplementation implements StoreViewController {

	@Override
	public void initPreRenderView() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.StoreProfile.View.StoreView storeView=(EdenFinalUser.StoreProfile.View.StoreView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "storeView");
            
                if(!storeView.getInit()){
                    
                    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    Entities.Franquicia franchise=this.getStore();
                    
                    this._fileController.Remove();
                    
                    storeView.setFranchise(franchise);
                    
                    java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"logo");
                
                    Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                    
                    if(!file.exists()){
                    
                        filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator, "logo");
                    
                    }
                    else{
                    
                        filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"logo");
                    
                    }
                    
                    if(franchise.getImagenIdimagen()!=null){
                    
                        _fileController.serverDownload(franchise.getImagenIdimagen(), session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"logo");
                    
                        String logo=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                        
                        storeView.setLogo(logo);
                        
                        _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                        
                        _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                        
                        
                    }
                    else{
                    
                    storeView.setLogo("/images/noImage.png");
                        
                    }
                    
                    
                    //init categories
                    
                    
                    java.util.List<Clases.ViewLayout.CategoryViewLayout>layout=null;
                    
                    if(IteratorUtils.toList(this.initCategories(storeView.getFranchise()).iterator())!=null){
                    
                        layout=IteratorUtils.toList(this.initCategories(storeView.getFranchise()).iterator());
                        
                        storeView.setCategoryViewLayouts(layout);
                    
                    }
                    
                    //Init Products
                    
                    if(this.initProducts(storeView.getFranchise())!=null && !this.initProducts(storeView.getFranchise()).isEmpty()){
                    
                    storeView.setProductViewLayout(IteratorUtils.toList(this.initProducts(storeView.getFranchise()).iterator()));
                    
                    }
                    
                    //Init meals
                    if(this.checkRestaurant()){
                    
                    if(this.initMeals(storeView.getFranchise())!=null && !this.initMeals(storeView.getFranchise()).isEmpty()){    
                        
                    storeView.setMealLayout(IteratorUtils.toList(this.initMeals(storeView.getFranchise()).iterator()));
                    
                    storeView.setRestaurant(true);
                    
                    }
                    
                    }
                    
                    //Init branches
                    
                    storeView.setBranchMarkerCollection(IteratorUtils.toList(this.initBranchMarkers(storeView.getFranchise()).iterator()));
                    
                    if(storeView.getBranchMarkerCollection()!=null && !storeView.getBranchMarkerCollection().isEmpty()){
                    
                     
                        storeView.setMapModel(new org.primefaces.model.map.DefaultMapModel());
                        
                        for(Clases.Maps.BranchMarker marker: storeView.getBranchMarkerCollection()){
                        
                            storeView.getMapModel().addOverlay(new org.primefaces.model.map.Marker(new org.primefaces.model.map.LatLng(marker.getLatitude(), marker.getLongitude())));
                        
                        }
                        
                    
                    }
                    
                    
                    
                    //Initialized store view
                    storeView.setInit(true);
                
                }
                
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	public Entities.Franquicia getStore() {
	
           try{
           
               javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
               
               if(request!=null && request.getParameter("id")!=null){
               
               int id=java.lang.Integer.valueOf(request.getParameter("id"));
               
               return this._franchiseController.find(id);
               
               }
               
              return null;
           
           }
           catch(NullPointerException | NumberFormatException ex){
           
           ex.printStackTrace(System.out);
           
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           return null;
           
           }
            
	}

	/**
	 * 
	 * @param franchise
	 */
	private SessionClasses.EdenList<CategoryViewLayout> initCategories(Entities.Franquicia franchise) {
		
            try{
            
                if(franchise!=null){
                
                    SessionClasses.EdenList<Clases.ViewLayout.CategoryViewLayout>result=new SessionClasses.EdenList<Clases.ViewLayout.CategoryViewLayout>();
                    
                    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    for(Entities.Categoria category:franchise.getCategoriaCollection()){
                    
                    Clases.ViewLayout.CategoryViewLayout aux=new Clases.ViewLayout.CategoryViewLayoutImplementation();
                    
                    aux.setName(category.getNombre());
                    
                    if(category.getImagenCollection()!=null && !category.getImagenCollection().isEmpty()){
                    
                        java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)category.getImagenCollection();
                        
                        java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre());
                    
                        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                        
                        if(!file.exists()){
                        
                            filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Category"+java.io.File.separator, category.getNombre());
                        
                        }
                        
                        else{
                        
                            filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre());
                        
                        }
                        
                        _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, (images.size()-1))), session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Category"+java.io.File.separator+category.getNombre());
                       
                       
                        String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                        
                        aux.setImage(image);
                        
                        _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                        
                        _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                        
                    }
                    else{
                    
                    aux.setImage("/images/noImage.png");
                    
                    }
                    
                    aux.setKey(category.getCategoriaPK());
                    
                    result.addItem(aux);
                    
                    }
                    
                    
                    return result;
                    
                }
                return null;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
                return null;
            }
            
	}

	/**
	 * 
	 * @param franchise
	 */
	private SessionClasses.EdenList<ProductViewLayout> initProducts(Entities.Franquicia franchise) {
		
            try{
            
                if(this._franchiseController.getProducts(franchise.getIdfranquicia())!=null && !this._franchiseController.getProducts(franchise.getIdfranquicia()).isEmpty()){
                
                    System.out.print("Products Is not Empty");
                    
                    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    SessionClasses.EdenList<ProductViewLayout>results=new SessionClasses.EdenList<ProductViewLayout>();
                
                    for(Entities.Producto product:this._franchiseController.getProducts(franchise.getIdfranquicia())){
                    
                        Clases.ViewLayout.ProductViewLayout<Entities.ProductoPK>aux=new Clases.ViewLayout.ProductViewLayoutImplementation<Entities.ProductoPK>();
                    
                        aux.setName(product.getNombre());
                        
                        aux.setKey(product.getProductoPK());
                        
                        if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                        
                            java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)product.getImagenCollection();
                            
                            java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+"Product"+java.io.File.separator+product.getNombre());
                            
                            Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                            
                            if(!file.exists()){
                            
                                filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Product"+java.io.File.separator+java.io.File.separator, product.getNombre());
                            
                            }
                            else{
                            
                                filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+"Product"+java.io.File.separator+product.getNombre());
                            
                            }
                            
                            
                            _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, images.size()-1)), session.getAttribute("username").toString()+java.io.File.separator+"Product"+product.getNombre());
                            
                            String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                            
                            aux.setImage(image);
                            
                            _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                            
                            _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                            
                        }
                        
                        else{
                        
                            aux.setImage("/images/noImage.png");
                        
                        }
                        
                        results.addItem(aux);
                        
                    }
                    
                    return results;
                }
                
                return null;
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
            
	}

	/**
	 * 
	 *
	 */
	@Override
	public boolean checkRestaurant() {
		
            try{
            
                javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.StoreProfile.View.StoreView storeView=(EdenFinalUser.StoreProfile.View.StoreView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "storeView");
                
                if(storeView.getFranchise()!=null){
                
                    if(storeView.getFranchise().getIdSubtype().getIdTipo().getNombre().equals("Restaurante")){
                    
                        return true;
                    
                    }
                
                }
                
            return false;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return false;
            
            }
            
	}

	/**
	 * 
	 * @param franchise
	 */
	private SessionClasses.EdenList<MealViewLayout> initMeals(Entities.Franquicia franchise) {
		
            try{
            
               
                if(franchise!=null){
                    
                    SessionClasses.EdenList<Clases.ViewLayout.MealViewLayout>results=new SessionClasses.EdenList<Clases.ViewLayout.MealViewLayout>();
                
                if(_mealController.findByFranchise(franchise.getIdfranquicia())!=null && !_mealController.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
                
                    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    for(Entities.Menu meal:_mealController.findByFranchise(franchise.getIdfranquicia())){
                    
                        Clases.ViewLayout.MealViewLayout<Entities.Menu>aux=new Clases.ViewLayout.MealViewLayoutImplementation<Entities.Menu>();
                    
                        aux.setKey(meal);
                        
                        aux.setName(meal.getNombre());
                        
                        if(meal.getImagenCollection()!=null && !meal.getImagenCollection().isEmpty()){
                        
                            java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)meal.getImagenCollection();
                            
                            java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Meal"+java.io.File.separator+meal.getNombre());
                        
                            Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                            
                            if(!file.exists()){
                            
                                filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"Meal"+java.io.File.separator+java.io.File.separator, meal.getNombre());
                            
                            }
                            else{
                            
                                filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Meal"+java.io.File.separator+meal.getNombre());
                            
                            }
                            
                            
                            _fileController.serverDownload(images.get(Clases.EdenNumber.random(0, images.size()-1)), session.getAttribute("username").toString());
                            
                            String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                            
                            aux.setImage(image);
                            
                            _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                            
                            _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                            
                        }
                        else{
                        
                            aux.setImage("/images/noImage.png");
                        
                        }
                        
                        results.addItem(aux);
                    }
                
                    return results;
                    
                }
                
                }
            
                return null;
                
                
            }
            catch(NullPointerException ex){
            
                return null;
            
            }
            
	}

	/**
	 * 
	 * @param franchise
	 */
        @annotations.MethodAnnotations(author="Luis Negrete",date="08/04/2016") 
        private SessionClasses.EdenList<BranchMarker> initBranchMarkers(Entities.Franquicia franchise) {
		
            try{
            
                if(franchise!=null){
                
                    if(franchise.getSucursalCollection()!=null && !franchise.getSucursalCollection().isEmpty()){
                    
                        SessionClasses.EdenList<Clases.Maps.BranchMarker>results=new SessionClasses.EdenList<Clases.Maps.BranchMarker>();
                    
                        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                        
                        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        
                        
                        
                        for(Entities.Sucursal branch:franchise.getSucursalCollection()){
                                
                           Clases.Maps.BranchMarker<Entities.Sucursal> marker=new Clases.Maps.BranchMarkerImplementation<Entities.Sucursal>();
                           
                           if(branch.getDireccion()!=null){
                           
                               if(branch.getDireccion().getGpsCoordinatesidgpsCoordinated()!=null){
                               
                                   marker.setLatitude(branch.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude());
                                   
                                   marker.setLongitude(branch.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude());
                               
                               }
                               
                               marker.setAddress(branch.getDireccion().getCity().getName()+","+branch.getDireccion().getCity().getState().getName()+" "+branch.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+branch.getDireccion().getPrimerDigito()+" #"+branch.getDireccion().getSegundoDigito()+"-"+branch.getDireccion().getTercerDigito()+","+branch.getDireccion().getAdicional());
                           
                           }
                           
                          if(branch.getImagenCollection()!=null && !branch.getImagenCollection().isEmpty()){
                          
                              java.util.List<Entities.Imagen>images=(java.util.List<Entities.Imagen>)branch.getImagenCollection();
                              
                              java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Branch"+java.io.File.separator+branch.getSucursalPK().toString());
                          
                              Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                              
                              if(file.exists()){
                              
                                  filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Branch"+java.io.File.separator+branch.getSucursalPK().toString());
                              
                              }
                              else{
                              
                                  filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Branch"+java.io.File.separator, branch.getSucursalPK().toString());
                              
                              }
                              
                              _fileController.serverDownload(images.get(Clases.EdenNumber.random(0,images.size()-1)),session.getAttribute("username").toString()+java.io.File.separator+franchise.getNombre()+java.io.File.separator+"Branch"+java.io.File.separator+branch.getSucursalPK().toString());
                              
                              
                              String image=_fileController.getPathList().get(_fileController.getPathList().size()-1);
                              
                              marker.setImage(image);
                              
                         
                              
                              _fileController.getPathList().remove(_fileController.getPathList().size()-1);
                              
                              _fileController.getImageList().remove(_fileController.getImageList().size()-1);
                              
                          }
                          else{
                          
                              marker.setImage("/images/noImage.png");
                          
                          }
                          
                            marker.setObject(branch);
                           
                          results.addItem(marker);
                          
                        }
                      
                        return results;
                        
                    }
                
                }
                
                return null;
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
            
	}

	/**
	 * 
	 * @param marker
	 */
	@Override
	public void updateInfoWindow(org.primefaces.model.map.Marker marker) {
		
            try{
            
                if(marker!=null){
                
                    javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                    
                    EdenFinalUser.StoreProfile.View.StoreView storeView=(EdenFinalUser.StoreProfile.View.StoreView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "storeView");
                    
                    System.out.print("MARKER IS NOT NULL "+storeView.getBranchMarkerCollection().size());
                    
                    if(storeView.getBranchMarkerCollection()!=null && !storeView.getBranchMarkerCollection().isEmpty()){
                    
                        System.out.print("MARKER LIST IS NOT EMPTY "+marker.getLatlng().getLat()+"-"+marker.getLatlng().getLng());
                        
              
                            storeView.setInfoWindow(null);
                   
                            
                            for(Clases.Maps.BranchMarker aux:storeView.getBranchMarkerCollection()){
                            
                                System.out.print(marker.getLatlng().getLat()+"-"+marker.getLatlng().getLng());
                                
                                if(aux.getLatitude()==marker.getLatlng().getLat() && aux.getLongitude()==marker.getLatlng().getLng()){
                                
                                    System.out.print("Marker found");
                                    
                                    Clases.Maps.InfoWindow infoWindow=new Clases.Maps.InfoWindowImplementation();
                                    
                                    infoWindow.setTitle(aux.getAddress());
                                    
                                    infoWindow.setImage(aux.getImage());
                                    
                                
                                    
                                    
                                    //Opening Times 
                                    if(aux.getObject() instanceof Entities.Sucursal){
                                    
                                        Entities.Sucursal branch=(Entities.Sucursal)aux.getObject();
                                        
                                        if(this._branchController.isOpen(branch.getSucursalPK())){
                                        
                                            //Is Open
                                        
                                            infoWindow.setOpen("/images/open.png");
                                            
                                        }
                                        else{
                                        
                                            //Is Closed
                                        
                                            infoWindow.setOpen("/images/closed.png");
                                            
                                        }
                                    
                                        if(branch.getSucursalHasDayCollection()!=null && !branch.getSucursalHasDayCollection().isEmpty()){
                                        
                                            java.util.List<Clases.EdenOpeningTimes>list=new java.util.ArrayList<Clases.EdenOpeningTimes>();
                                            
                                            for(Entities.SucursalHasDay day: branch.getSucursalHasDayCollection()){
                                            
                                               Clases.EdenOpeningTimes item = new Clases.EdenOpeningTimesImplementation();
                                               
                                               item.setDay(day.getDayId().getDayName());
                                               
                                               Clases.EdenTime open = new Clases.EdenTimeImplementation();
                                               
                                               open.setHour(day.getOpenTime().getHours());
                                               
                                               open.setMinutes(day.getOpenTime().getMinutes());
                                               
                                               open.setSeconds(day.getOpenTime().getSeconds());
                                               
                                               item.setOpeningTime(open);
                                               
                                               Clases.EdenTime closing = new Clases.EdenTimeImplementation();
                                               
                                               closing.setHour(day.getCloseTime().getHours());
                                               
                                               closing.setMinutes(day.getCloseTime().getMinutes());
                                               
                                               closing.setSeconds(day.getCloseTime().getSeconds());
                                               
                                               item.setClosingTime(closing);
                                               
                                               list.add(item);
                                            
                                            }
                                        
                                            infoWindow.setEdenOpeningTimeses(list);
                                            
                                        }
                                        
                                       //Range
                                        
                                        java.util.Map<String,String> map = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
                                        
                                        for(java.util.Map.Entry<String,String> entry : map.entrySet()){
                                        
                                            System.out.print(entry.getKey()+"-"+entry.getValue());
                                            
                                        
                                        }
                                        
                                        
                                        //In Range
                                        if(map.get("latitude")!=null && map.get("longitude")!=null){
                                        
                                          Clases.EdenGpsCoordinates coordinates = new Clases.EdenGpsCoordinatesImplementation();
                                          
                                          coordinates.setLatitude(new java.lang.Double(map.get("latitude")));
                                          
                                          coordinates.setLongitude(new java.lang.Double(map.get("longitude")));
                                        
                                          infoWindow.setInRange(this._branchController.isInRange(branch.getSucursalPK(), coordinates));
                                          
                                        }
                                        
                                    }
                                    
                                     
                                    
                                    
                                    storeView.setInfoWindow(infoWindow);
                                    
                                    break;
                                
                                }
                                
                            
                            }
                            
                        
                    
                    }
                
                }
            
            }
            catch(NullPointerException | NumberFormatException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	@javax.inject.Inject
	private CDIBeans.FranchiseControllerInterface _franchiseController;
	@javax.inject.Inject
	private CDIBeans.FileController _fileController;
	@javax.inject.Inject
	private MenuController _mealController;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private CDIBeans.SucursalControllerDelegate _branchController;
}