package EdenFinalUser.StoreProfile.View;

import EdenFinalUser.StoreProfile.Controller.*;
import java.util.*;
import Clases.ViewLayout.*;
import Clases.Maps.*;

public class StoreView {

	
	@javax.inject.Inject
	private StoreViewController storeViewController;
	private String logo;
	private Entities.Franquicia franchise;
	private boolean init = false;

	private Collection<CategoryViewLayout> categoryViewLayouts = null;
	private Collection<ProductViewLayout> productViewLayout = null;
	private Collection<MealViewLayoutImplementation> mealLayout = null;
	private Collection<BranchMarker> branchMarkerCollection = null;
	private boolean restaurant = false;
	private org.primefaces.model.map.MapModel mapModel;
	private InfoWindow infoWindow = null;
	
	

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
	
            try{
            
                this.storeViewController.initPreRenderView();
                
                org.primefaces.context.RequestContext.getCurrentInstance().update(":form:map");
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	public String getLogo() {
		return this.logo;
	}

	/**
	 * 
	 * @param logo
	 */
	public void setLogo(String logo) {

            this.logo=logo;
            
	}

	public Entities.Franquicia getFranchise() {
		return this.franchise;
	}

	/**
	 * 
	 * @param franchise
	 */
	public void setFranchise(Entities.Franquicia franchise) {
		this.franchise = franchise;
	}

	public boolean getInit() {
		return this.init;
	}

	/**
	 * 
	 * @param init
	 */
	public void setInit(boolean init) {
		this.init = init;
	}

	public Collection<CategoryViewLayout> getCategoryViewLayouts() {
		return this.categoryViewLayouts;
	}

	public void setCategoryViewLayouts(Collection<CategoryViewLayout> categoryViewLayouts) {
		this.categoryViewLayouts = categoryViewLayouts;
	}

	public Collection<ProductViewLayout> getProductViewLayout() {
		return this.productViewLayout;
	}

	public void setProductViewLayout(Collection<ProductViewLayout> productViewLayout) {
		this.productViewLayout = productViewLayout;
	}

	public Collection<MealViewLayoutImplementation> getMealLayout() {
		return this.mealLayout;
	}

	public void setMealLayout(Collection<MealViewLayoutImplementation> mealLayout) {
		this.mealLayout = mealLayout;
	}

	public Collection<BranchMarker> getBranchMarkerCollection() {
		return this.branchMarkerCollection;
	}

	public void setBranchMarkerCollection(Collection<BranchMarker> branchMarkerCollection) {
		this.branchMarkerCollection = branchMarkerCollection;
	}

	public boolean getRestaurant() {
		return this.restaurant;
	}

	/**
	 * 
	 * @param restaurant
	 */
	public void setRestaurant(boolean restaurant) {
		this.restaurant = restaurant;
	}

	public org.primefaces.model.map.MapModel getMapModel() {
		return this.mapModel;
	}

	/**
	 * 
	 * @param mapModel
	 */
	public void setMapModel(org.primefaces.model.map.MapModel mapModel) {
		this.mapModel = mapModel;
	}

	/**
	 * 
	 * @param event
	 */
	public void markerSelection(org.primefaces.event.map.OverlaySelectEvent event) {
	
            
            try{
            
               if(event.getOverlay() instanceof org.primefaces.model.map.Marker){
               
                   System.out.print("This is a marker");
                   
                   this.storeViewController.updateInfoWindow((org.primefaces.model.map.Marker)event.getOverlay());
                   
                   System.out.print("Info Window Title "+this.infoWindow.getTitle());
               
               }
            
            }catch(NullPointerException | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                ex.printStackTrace(System.out);
            
            }
            
	}

	public InfoWindow getInfoWindow() {
		return this.infoWindow;
	}

	/**
	 * 
	 * @param infoWindow
	 */
	public void setInfoWindow(InfoWindow infoWindow) {
		this.infoWindow = infoWindow;
	}

	
}