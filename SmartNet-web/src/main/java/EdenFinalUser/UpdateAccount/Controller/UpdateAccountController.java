package EdenFinalUser.UpdateAccount.Controller;

import Clases.ViewLayout.*;

public interface UpdateAccountController {

	void preRenderView();

	/**
	 * 
	 * @param oldPassword
	 */
	boolean oldPasswordConfirmation(String oldPassword);

	/**
	 * 
	 * @param loginAdministrador
	 */
	void updateGeneralInfo(Entities.LoginAdministrador loginAdministrador);

	/**
	 * 
	 * @param user
	 */
	void updatePassword(Entities.LoginAdministrador user);

	/**
	 * 
	 * @param address
	 */
	void removeAddress(AddressViewLayout address);

	ENUM.TransactionStatus updateAddress();

	/**
	 * 
	 * @param stateId
	 * @param address
	 */
	void stateChanged(int stateId, AddressViewLayout address);

	void addAddress();
}