package EdenFinalUser.UpdateAccount.Controller;

import CDIBeans.*;
import CDIBeans.AddressTypeController;
import Controllers.*;
import Clases.ViewLayout.*;
import SessionClasses.EdenList;
import org.apache.commons.collections.IteratorUtils;

@javax.enterprise.context.Dependent
@javax.transaction.Transactional(javax.transaction.Transactional.TxType.REQUIRED)
public class UpdateAccountControllerImplementation implements UpdateAccountController {

	@javax.inject.Inject
	private transient Controllers.ProfessionSingletonController professionSingletonController;
	@javax.inject.Inject
        @CDIBeans.LoginAdministradorControllerQualifier
	private LoginAdministradorControllerInterface loginAdministratorController;
	@javax.inject.Inject
        @CDIBeans.ProfessionControllerQualifier
	private ProfessionControllerInterface professionController;
	@javax.inject.Inject
        
	private StateSingletonController stateSingletonController;
	@javax.inject.Inject
	private transient CitySessionController citySessionController;
	@javax.inject.Inject
        @CDIBeans.StateControllerQualifier
	private StateControllerInterface stateController;
	@javax.inject.Inject
	@CDIBeans.CityControllerQualifier
	private CityController cityController;
	@javax.inject.Inject
	@CDIBeans.AddressTypeControllerQualifier
	private AddressTypeController addressTypeController;

	@Override
	public void preRenderView() {
		
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
            
                if(!updateAccountView.isInit()){
                
                    javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    
                    if(session.getAttribute("username")!=null){
                    
                        if(this.loginAdministratorController.find(session.getAttribute("username").toString()) != null){
                        
                            Entities.LoginAdministrador user = this.loginAdministratorController.find(session.getAttribute("username").toString());
                        
                            System.out.print("Username "+user.getName());
                            
                            updateAccountView.setUser(user);
                            
                            
                            
                        }
                    
                    }
                    
                    updateAccountView.setProfessions(professionSingletonController.getProfessions());                   
                     
                    updateAccountView.setInit(true);
                
                    if(updateAccountView.getAddressViewLayoutCollection() == null || updateAccountView.getAddressViewLayoutCollection().isEmpty()){
                    
                        if(updateAccountView.getUser().getDireccionCollection() != null && !updateAccountView.getUser().getDireccionCollection().isEmpty())
                        {
                            java.util.List<Clases.ViewLayout.AddressViewLayout> list = new java.util.ArrayList<Clases.ViewLayout.AddressViewLayout>();
                        
                            
                            for(Entities.Direccion address : updateAccountView.getUser().getDireccionCollection()){
                            
                            //Create Address Layout View
                                
                                Clases.ViewLayout.AddressViewLayout layoutItem = new Clases.ViewLayout.AddressViewLayoutImplementation();
                                
                                layoutItem.setAddress(address);
                                
                                layoutItem.setStateCollection(this.stateSingletonController.getStateList());
                                
                                layoutItem.setCityCollection(address.getCity().getState().getCityCollection());
                            
                                list.add(layoutItem);
                                
                            }
                        
                            System.out.print("ADDRESS SIZE "+list.size());
                            
                            updateAccountView.setAddressViewLayoutCollection(list);
                            
                        }
                        
                    }
                    
                    
                }
               
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	/**
	 * 
	 * @param oldPassword
     * @return 
	 */
	@Override
	public boolean oldPasswordConfirmation(String oldPassword) {
		
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
                
                if(updateAccountView.getUser().getPassword().equals(Clases.EdenString.encription(oldPassword))){
                
                    return true;
                    
                }
                
            return false;
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
            return false;    
            
            }
            
	}

	/**
	 * 
	 * @param loginAdministrador
	 */
	@Override
	public void updateGeneralInfo(Entities.LoginAdministrador loginAdministrador) {
		
            try{
                
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
            
                loginAdministrador.setProfessionIdprofession(this.professionController.find(loginAdministrador.getProfessionIdprofession().getIdprofession()));
                
                System.out.print("UPDATING ACCOUNT "+loginAdministrador.getName()+", Date: "+loginAdministrador.getBirthday().toString());

                this.loginAdministratorController.update(loginAdministrador);
                
                javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                java.util.ResourceBundle rb = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                session.setAttribute("severity", "info");
                
                session.setAttribute("message",rb.getString("EdenFinalUser.UpdateInfo.UpdateButton.Msg"));
    
                session.setAttribute("title", rb.getString("EdenFinalUser.UpdateInfo.UpdateButton.Title"));
                
            }
            catch(NullPointerException ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
                ex.printStackTrace(System.out);
                
            }
            
	}

	/**
	 * 
	 * @param user
	 */
	@Override
	@annotations.MethodAnnotations(author="Luis Negrete", date="18/06/2016", comments="Update Password method")
        
	public void updatePassword(Entities.LoginAdministrador user) {
	
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView) resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
                
                System.out.print("OLD PASSWORD "+updateAccountView.getOldPassword());
                
                System.out.print("NEW PASSWORD "+updateAccountView.getNewPassword());
                
                java.util.ResourceBundle rb = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                if(updateAccountView.getNewPassword() != null && !updateAccountView.getNewPassword().equals("") && updateAccountView.getOldPassword()!=null && !updateAccountView.getOldPassword().equals(""))
                {                    
                    String password = Clases.EdenString.encription(updateAccountView.getOldPassword());
                    
                    System.out.print("ENCRIPTED PASSWORD FORM "+password+" UNCRIPTED PASSWORD "+updateAccountView.getOldPassword());
                    
                    System.out.print("USER PASSWORD DATABASE "+user.getPassword());
                    
                    if(password.equals(user.getPassword())){
                    
                       System.out.print("CHANGING PASSWORD");
                        
                       user.setPassword(Clases.EdenString.encription(updateAccountView.getNewPassword()));
                        
                       this.loginAdministratorController.update(user);
                     
                       javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        
                       session.setAttribute("title",rb.getString("EdenFinalUser.UpdateInfo.UpdateButton.Title"));
                    
                       session.setAttribute("severity", "info");
                       
                       session.setAttribute("message", rb.getString("EdenFinalUser.UpdateInfo.UpdateButton.PasswordChangedMessage"));
                       
                       System.out.print("PASSWORD CHANGED");
                       
                    }
                    else{
                        
                    
                        System.out.print("ERROR CHANGING PASSWORD NOT EQUAL");
                        
                        javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                        
                        session.setAttribute("severity", "warn");
                        
                        session.setAttribute("title", rb.getString("EdenFinalUser.UpdateInfo.UpdatePassword.ErrorTitle"));

                        session.setAttribute("message", rb.getString("EdenFinalUser.UpdateInfo.UpdatePassword.ErrorMessage"));
                        
                    }
                
                }
                
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
        }

	/**
	 * 
	 * @param address
	 */
	@Override
	public void removeAddress(AddressViewLayout address) {
		
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView) resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
            
                if(updateAccountView != null){
                
                    if(updateAccountView.getAddressViewLayoutCollection()!=null && !updateAccountView.getAddressViewLayoutCollection().isEmpty())
                    {
                    
                        updateAccountView.getAddressViewLayoutCollection().remove(address);
                    
                    }
                }
                
            }
            catch(NullPointerException ex){
            
               ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	@Override
	public ENUM.TransactionStatus updateAddress() {
		
            try{
            
                javax.el.ELResolver resolver = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
            
                if(updateAccountView != null){
                    
                    if(updateAccountView.getAddressViewLayoutCollection() != null && !updateAccountView.getAddressViewLayoutCollection().isEmpty())
                    {
                        EdenList<Entities.Direccion> addressCollection = new EdenList();
                    
                        for(Clases.ViewLayout.AddressViewLayout aux: updateAccountView.getAddressViewLayoutCollection()){
                        
                            System.out.print("IS CHANGING CITY ? "+this.isNewCity(aux.getAddress(),  updateAccountView.getUser()));
                                                        
                            updateAccountView.getUser().getDireccionCollection().clear();

                            this.loginAdministratorController.update(updateAccountView.getUser());
                            
                            aux.getAddress().setCity(this.cityController.find(aux.getAddress().getCity().getCityPK()));
                            
                            aux.getAddress().getDireccionPK().setCITYidCITY(aux.getAddress().getCity().getCityPK().getIdCITY());
                            
                            aux.getAddress().getDireccionPK().setCITYSTATEidSTATE(aux.getAddress().getCity().getCityPK().getSTATEidSTATE());
                            
                            aux.getAddress().setTYPEADDRESSidTYPEADDRESS(this.addressTypeController.findByname(aux.getAddress().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()));
                        
                            System.out.print("ADDRESSPK "+aux.getAddress().getDireccionPK());
                            
                            System.out.print("ADDRESS INFORMATION TYPE "+aux.getAddress().getTYPEADDRESSidTYPEADDRESS().toString()+" "+aux.getAddress().getDireccionPK()+" First Digit "+aux.getAddress().getPrimerDigito()+" Second Digit "+aux.getAddress().getSegundoDigito()+" Third Digit: "+aux.getAddress().getTercerDigito()+" Additional "+aux.getAddress().getAdicional()+ "Latitude "+aux.getAddress().getGpsCoordinatesidgpsCoordinated().getLatitude()+" Longitude "+aux.getAddress().getGpsCoordinatesidgpsCoordinated().getLongitude()+" City "+aux.getAddress().getCity().toString()+" CityPK "+aux.getAddress().getCity().getCityPK().toString());

                            addressCollection.addItem(aux.getAddress());
                            
                        }
                        
                        updateAccountView.getUser().setDireccionCollection(IteratorUtils.toList(addressCollection.iterator()));
                        
                        for(Entities.Direccion address:updateAccountView.getUser().getDireccionCollection()){
                        
                            System.out.print("UPDATING ADDRESS PK "+address.getDireccionPK().toString());
                        
                        
                        }
                        
                        this.loginAdministratorController.update(updateAccountView.getUser());
                        
                        System.out.print("ADDRESS UPDATED");
                        
                    }
                    
                    return ENUM.TransactionStatus.APPROVED;
                    
                  }
                
                return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}

	/**
	 * 
	 * @param stateId
	 * @param address
	 */
	@Override
	@annotations.MethodAnnotations(author="Luis Negrete", date="18/06/2016", comments="State changed")
	public void stateChanged(int stateId, AddressViewLayout address) {
	
            try{
           
                if(this.stateController.find(stateId) != null){
                
                    address.updateCities(this.stateController.find(stateId));
                
                }
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	/**
	 * 
	 * @param address
	 * @param user
	 */
	private boolean isNewCity(Entities.Direccion address, Entities.LoginAdministrador user) {
	
            
            try{
            
                if(user.getDireccionCollection() != null && !user.getDireccionCollection().isEmpty()){
                
                    for(Entities.Direccion userAddress: user.getDireccionCollection()){
                    
                        if(userAddress.getDireccionPK().equals(address.getDireccionPK()))
                        {
                        
                            return true;
                        
                        }
                    }
                
                }
            
                return false;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
                return false;
                
            }
            
        }

	@Override
	public void addAddress() {
		
            try{
            
                javax.el.ELResolver er = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
                
                EdenFinalUser.UpdateAccount.View.UpdateAccountView updateAccountView = (EdenFinalUser.UpdateAccount.View.UpdateAccountView)er.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "updateAccountView");
            
                if(updateAccountView.getAddressViewLayoutCollection() != null){
                
                AddressViewLayout addressViewLayout = new AddressViewLayoutImplementation();
                
                
                Entities.Direccion address = new Entities.Direccion();
                
                Entities.DireccionPK addressPK = new Entities.DireccionPK();
                
                Entities.City city = this.citySessionController.getCity();
                
                System.out.print("SELECTED CITY "+city.getCityPK().toString());
                
                addressPK.setCITYSTATEidSTATE(city.getCityPK().getSTATEidSTATE());
                
                addressPK.setCITYidCITY(city.getCityPK().getIdCITY());
                
                address.setDireccionPK(addressPK);
                
                address.setCity(city);
                
                address.setPrimerDigito("0");
                
                address.setSegundoDigito("0");
                
                address.setTercerDigito((short)0);
                
                System.out.print("THERE ARE "+stateSingletonController.getStateList().size()+" states");
              
                address.setTYPEADDRESSidTYPEADDRESS(this.addressTypeController.findAll().get(0));
                
                addressViewLayout.setAddress(address);
                
                addressViewLayout.getAddress().setDireccionPK(new Entities.DireccionPK());
                
                addressViewLayout.setCityCollection(this.citySessionController.getCities(this.stateSingletonController.getStateList().get(0)));
                
                
                addressViewLayout.setStateCollection(this.stateSingletonController.getStateList());
                
                updateAccountView.getAddressViewLayoutCollection().add(addressViewLayout);
                
                }
                else{
                
                updateAccountView.setAddressViewLayoutCollection(new java.util.ArrayList<AddressViewLayout>());
                
                AddressViewLayout addressViewLayout = new AddressViewLayoutImplementation();
                
                
                Entities.Direccion address = new Entities.Direccion();
                
                Entities.DireccionPK addressPK = new Entities.DireccionPK();
                
                Entities.City city = this.citySessionController.getCity();
                
                System.out.print("SELECTED CITY "+city.getCityPK().toString());
                
                addressPK.setCITYSTATEidSTATE(city.getCityPK().getSTATEidSTATE());
                
                addressPK.setCITYidCITY(city.getCityPK().getIdCITY());
                
                address.setDireccionPK(addressPK);
                
                address.setCity(city);
                
                address.setPrimerDigito("0");
                
                address.setSegundoDigito("0");
                
                address.setTercerDigito((short)0);
              
                address.setTYPEADDRESSidTYPEADDRESS(this.addressTypeController.findAll().get(0));
                
                addressViewLayout.setAddress(address);
                
                addressViewLayout.getAddress().setDireccionPK(new Entities.DireccionPK());
                
                addressViewLayout.setCityCollection(this.citySessionController.getCities(this.stateSingletonController.getStateList().get(0)));
                
                addressViewLayout.setStateCollection(this.stateSingletonController.getStateList());
                
                System.out.print("THERE ARE "+stateSingletonController.getStateList().size()+" states");
                
                updateAccountView.getAddressViewLayoutCollection().add(addressViewLayout);
                    
                }
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                ex.printStackTrace(System.out);
            
            }
            
	}
        
        
   

}