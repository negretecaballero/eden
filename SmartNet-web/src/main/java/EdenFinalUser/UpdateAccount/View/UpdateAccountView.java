package EdenFinalUser.UpdateAccount.View;

import java.text.DateFormat;
import org.springframework.beans.factory.annotation.Autowired;



@javax.faces.bean.ViewScoped
@javax.faces.bean.ManagedBean(name="updateAccountView")
public class UpdateAccountView  {

	private java.util.List<Entities.Profession> professions;
	private boolean init = false;
        
	@javax.inject.Inject
        
	private EdenFinalUser.UpdateAccount.Controller.UpdateAccountController updateAccountController;

        @javax.validation.constraints.Size(min=6,max=128)
        @javax.validation.constraints.NotNull
        private String oldPassword="";
        
        private String severity;
        
        private java.util.Collection<Clases.ViewLayout.AddressViewLayout>addressViewLayoutCollection = null;
        
        public java.util.Collection<Clases.ViewLayout.AddressViewLayout>getAddressViewLayoutCollection(){
        
            return this.addressViewLayoutCollection;
        
        }
        
        public void setAddressViewLayoutCollection(java.util.Collection<Clases.ViewLayout.AddressViewLayout> addressViewLayoutCollection){
        
            this.addressViewLayoutCollection = addressViewLayoutCollection;
        
        }
        
        public String getSeverity(){
        
            return this.severity;
        
        }
        
        public void setSeverity(String severity){
        
            this.severity =  severity;
        
        }
        
        
        //@javax.validation.constraints.Size(min=6,max=128)
        private String newPassword="";
        
        
        
        public String getNewPassword(){
        
            return this.newPassword;
        
        }
        
        public void setNewPassword(String newPassword){
        
            this.newPassword =  newPassword;
        
        }
        
        private Entities.LoginAdministrador user;
        
        public String getOldPassword(){
        
            return this.oldPassword;
        
        }
        
        public void setOldPassword(String oldPassword){
        
            this.oldPassword = oldPassword;
        
        }
        
        public Entities.LoginAdministrador getUser(){
        
        return this.user;
        
        }
        
        public void setUser(Entities.LoginAdministrador user){
        
            this.user = user;
        
        }
        
	public java.util.List<Entities.Profession> getProfessions() {
		return this.professions;
	}

	/**
	 * 
	 * @param professions
	 */
	public void setProfessions(java.util.List<Entities.Profession> professions) {
		this.professions = professions;
	}

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
		
            try{
            
                this.updateAccountController.preRenderView();
            
                System.out.print("PRE RENDER VIEW");
                
                /*javax.faces.application.FacesMessage msg = new javax.faces.application.FacesMessage("Testing");
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);*/
                
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
            
	}

	public boolean isInit() {
		return this.init;
	}

	public void setInit(boolean init) {
		this.init = init;
	}
        
        public void checkOldPassword(javax.faces.event.AjaxBehaviorEvent event){
        
            try{
            
                if(!this.updateAccountController.oldPasswordConfirmation(oldPassword)){
                
                    
                
                }
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
        
        }


        public void updateGeneralInformation(javax.faces.event.AjaxBehaviorEvent event){
        
            try{
            
                
                this.updateAccountController.updateGeneralInfo(user);
                
                //java.util.ResourceBundle rb = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                //javax.faces.application.FacesMessage msg = new javax.faces.application.FacesMessage("HELLO");
                
                //msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                //System.out.print("LOGIN ADMINISTRATOR UPDATED");
                
                //javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                
                //org.primefaces.context.RequestContext.getCurrentInstance().update("messageForm:growl");
                
               // javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
             
            
            }
            catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
        
        }
        
        
        public void dateSelection(javax.faces.event.AjaxBehaviorEvent event){
        
        try{
            
            System.out.print("DATE CHANGED");
            
            
            if(event instanceof org.primefaces.event.SelectEvent){
            
            org.primefaces.event.SelectEvent select = (org.primefaces.event.SelectEvent)event;
  
            DateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy");
            
            System.out.print("Date before parse "+select.getObject().toString());
            
//            java.util.Date date = new java.util.Date(select.getObject().toString());
            
            System.out.print("DATE "+this.user.getBirthday());
        
            
            }
        
        }
        catch(NullPointerException  ex){
        
            ex.printStackTrace(System.out);
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
        }
        
           public void updatePassword(javax.faces.event.ActionEvent  event){
        
            try{
                
                System.out.print("UPDATING PASSWORD");
                
                
                this.updateAccountController.updatePassword(user);
                
                System.out.print("Password Updated View");
                
                
            }catch(NullPointerException | StackOverflowError ex){
            
                ex.printStackTrace(System.out);
                
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
        
        }
           
           public void deleteAddress(Clases.ViewLayout.AddressViewLayout address){
           
               try{
               
                   this.updateAccountController.removeAddress(address);
                   
               }
               catch(NullPointerException ex){
               
                   ex.printStackTrace(System.out);
                   
                   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
               
               }
           
           }
           
           public void stateChanged(Clases.ViewLayout.AddressViewLayout address){
           
           try{
           
               //System.out.print("State "+address.getAddress().getDireccionPK().getCITYSTATEidSTATE());
           
               this.updateAccountController.stateChanged(address.getAddress().getDireccionPK().getCITYSTATEidSTATE(), address);
               
           }
           catch(NullPointerException | StackOverflowError ex){
           
               ex.printStackTrace(System.out);
               
               java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           }
               
           
           }
           
           public void updateAddress(javax.faces.event.ActionEvent event){
           
               try{
  
                   System.out.print("UPDATING ADDRESS");
                   
                   switch(this.updateAccountController.updateAddress()){
                   
                       case APPROVED:{
                       
                           java.util.ResourceBundle bundle = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                           
                           javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                           
                           session.setAttribute("title",bundle.getString("EdenFinalUser.UpdateInfo.UpdateButton.Title"));
                    
                           session.setAttribute("severity", "info");
                       
                           session.setAttribute("message", bundle.getString("EdenFinalUser.AddressUpdatedSuccess"));
                      
                           
                       };
                       
                       case DISAPPROVED:{

                           
                           java.util.ResourceBundle bundle = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
    
                           javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                       
                           session.setAttribute("title",bundle.getString("EdenFinalUser.UpdateInfo.UpdateButton.Title"));
                    
                           session.setAttribute("severity", "info");
                       
                           session.setAttribute("message", bundle.getString("EdenFinalUser.AddressUpdatedError"));
                           
                       };
                       
                       default:{
                       
                           java.util.ResourceBundle bundle = javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
    
                           javax.servlet.http.HttpSession session = (javax.servlet.http.HttpSession) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                       
                           session.setAttribute("title",bundle.getString("EdenFinalUser.UpdateInfo.UpdateButton.Title"));
                    
                           session.setAttribute("severity", "info");
                       
                           session.setAttribute("message", bundle.getString("EdenFinalUser.AddressUpdatedError"));
               
                       
                       };
                   
                   }
                   
               
               }
               catch(NullPointerException ex){
               
               ex.printStackTrace(System.out);
               
               java.util.logging.Logger.getLogger(this.getClass().getName()).log(null);
               
               }
           
           }
           
             public void addAddress(javax.faces.event.AjaxBehaviorEvent event){
     
         try{
         
             System.out.print("ADDIDNG ADDRESS");
             
             this.updateAccountController.addAddress();
         
         }
         catch(NullPointerException | StackOverflowError ex){
         
             ex.printStackTrace(System.out);
             
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         }
     
     }
        
}