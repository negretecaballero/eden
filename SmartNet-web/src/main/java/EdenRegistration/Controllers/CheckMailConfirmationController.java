/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.Controllers;

/**
 *
 * @author luisnegrete
 */
public interface CheckMailConfirmationController {
    
    void resendMail(String destinatary);
    
     String getMessageURL();
    
}
