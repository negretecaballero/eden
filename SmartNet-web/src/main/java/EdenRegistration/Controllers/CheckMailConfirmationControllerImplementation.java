/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.Controllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
public class CheckMailConfirmationControllerImplementation implements CheckMailConfirmationController {
  
    @javax.inject.Inject 
    @CDIEden.MailControllerQualifier
    private CDIEden.MailController _mailController;
    
    @Override
    public void resendMail(String destinatary){
    
        try{
        
            javax.faces.context.ExternalContext ec=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
            
            javax.servlet.http.HttpServletRequest request= (javax.servlet.http.HttpServletRequest) javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            
            System.out.print(this.getMessageURL()+" Message URL");
            
            System.out.print(ec.getRequestContextPath()+java.io.File.separator+"confirmation.xhtml?faces-redirect=true&id="+destinatary);
            
            _mailController.sendMail(destinatary, "Welcome to Eden", "please follow this link to confirm your account "+this.getMessageURL()+"confirmation/confirmation.xhtml?faces-redirect=true&id="+destinatary+"");
   
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public String getMessageURL(){
    
    try{
    
        javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        String url=request.getRequestURL().toString();
        
        url=url.replaceAll(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId(), "");
        
        url=url+"/";
        
        return url;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return "";
    
    }
    
    }
    
}
