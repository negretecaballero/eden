/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.Controllers;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class UserConfirmationControllerImplementation implements UserConfirmationController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.ConfirmationDelegate _confirmationController;
    
    @Override
    public String getUsername(){
    
        try{
        
            javax.faces.context.FacesContext context=javax.faces.context.FacesContext.getCurrentInstance();
            
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)context.getExternalContext().getRequest();
            
            return servletRequest.getParameter("id");
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return null;
        }
    
    }
    
    @Override
    public ENUM.ConfirmationEnumeration confirm(String username){
    try
        {
            System.out.print("THIS USERNAME "+username);
            
               
            System.out.print(this._confirmationController.findAll().size());
            
            
            
        if(this._confirmationController.findUsernameConfirmed(username,false)!=null){
        
            
            System.out.print("CONFIRMING ACCOUNT");
            
            System.out.print("CONFIRM ACCOUNT BOOLEAN "+this._confirmationController.find(username).getConfirmed());
            
            this._confirmationController.confirm(username);
            
            return ENUM.ConfirmationEnumeration.CONFIRMED;
        }
        else{
        
            
            System.out.print("This account does not exist");
            return ENUM.ConfirmationEnumeration.UNCONFIRMED;
        
        }
        
    }
    
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return ENUM.ConfirmationEnumeration.UNCONFIRMED;
    
    }
    
    }
    
}
