/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.Controllers;

import org.primefaces.event.CaptureEvent;

/**
 *
 * @author luisnegrete
 */
public interface UserRegistrationController {
    
    boolean checkExistence(String username);
    
    boolean matchMail(String mail,String mailConfirmation);
    
    void updateCities(String state);
    
    void initCitiesList();
    
    String getFirstCity();
    
    String getRequestStateName();
    
    String getCityName(Entities.CityPK key);
    
    Entities.Direccion requestAddress(String name);
    
    Entities.CityPK getCityPK();
    
    void oncapture(CaptureEvent event,String fileName, Entities.Imagen image,String folderName);
    
     String getRandomImageName();
     
     String genarateFolderName();
     
     String gerFileName(String folderName);
     
     Entities.Imagen getImage(String folderName);
     
     void handleUpload(String folderName, org.primefaces.event.FileUploadEvent event);
     
    void register(Entities.LoginAdministrador loginAdministrador,Entities.Direccion address,String folderName,String phoneNumber,int idProfession);
    
    String updateCity();
    
}
