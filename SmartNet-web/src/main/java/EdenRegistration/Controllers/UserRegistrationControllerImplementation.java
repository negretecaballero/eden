/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.Controllers;

import CDIBeans.FileUploadBean;
import Clases.EdenString;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathFactory;
import javax.imageio.stream.FileImageOutputStream;
import javax.transaction.Transactional.TxType;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;



import org.primefaces.model.UploadedFile;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
public class UserRegistrationControllerImplementation implements UserRegistrationController{
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject
    
    private Controllers.CitySessionController _citySessionController;
    
    @javax.inject.Inject 
    @CDIBeans.StateControllerQualifier
    private CDIBeans.StateControllerInterface _stateController;
    
    @javax.inject.Inject

    private Controllers.StateSingletonController _stateSingletonController;
    
    @javax.inject.Inject 
    @CDIBeans.CityControllerQualifier
    private CDIBeans.CityControllerInterface _cityController;
    
    @javax.inject.Inject
    @CDIBeans.AddressTypeControllerQualifier
    private CDIBeans.AddressTypeControllerInterface _tipoController;
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private CDIBeans.FileUploadBean _fileUploadBean;
    
    @javax.inject.Inject
    @CDIBeans.AddressControllerQualifier
    private CDIBeans.AdressControllerInterface _addressController;
    
    @javax.inject.Inject
    @CDIBeans.ProfessionControllerQualifier
    private CDIBeans.ProfessionControllerInterface _professionController;
    
    
    
    @Override
    public boolean checkExistence(String username){
    
        try{
        
            if(_loginAdministradorController.find(username)!=null){
            
                return true;
            
            }
            
                    return false;
        }
        catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
        
    
    }
    
    @Override
    public void initCitiesList(){
    
        try{
        
            if(this._stateSingletonController.getStateList()!=null && !this._stateSingletonController.getStateList().isEmpty()){
            
                this._citySessionController.initList(this._stateSingletonController.getStateList().get(0));
            
            }
        
        }
        catch(NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
   
    
    @Override
    public void updateCities(String state){
    
    try{
    
        _citySessionController.initList(_stateController.findByName(state));
    
    }
    catch(NullPointerException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public boolean matchMail(String mail,String mailConfirmation){
    
        try{
        
            if(mail.equals(mailConfirmation)){
            
                return true;
            
            }
            
            return false;
        }
        
        catch(NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public String getFirstCity(){
    
        try{
        
            if(this._citySessionController.getCityList()!=null && !this._citySessionController.getCityList().isEmpty()){
            
            return this._citySessionController.getCityList().get(0).getName();
            
            }
            
            return "";
            
        }
        catch(NullPointerException | ArrayIndexOutOfBoundsException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return "";
        
        }
    
    }
    
    @Override
    public String getCityName(Entities.CityPK key){
    
    try{
    
        if(this._cityController.find(key)!=null){
        
            return this._cityController.find(key).getName();
        
        }
        
        return "";
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
      return "";  
    
    }
    
    }
    
    @Override
    public String getRequestStateName(){
    
    try{
    
        javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        if(servletRequest.getParameter("form:state_input")!=null && !servletRequest.getParameter("form:state_input").equals("")){
        
            return servletRequest.getParameter("form:state_input");
        
        }
        
    return "";
    }
    catch(NullPointerException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return"";
    
    }
    
    }
    
    @Override
    public Entities.CityPK getCityPK(){
    
        try{
        
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            if(servletRequest.getParameter("form:cities_input")!=null && !servletRequest.getParameter("form:cities_input").equals("")){
            
                return  _cityController.convertToKey(servletRequest.getParameter("form:cities_input"));
            
            }
            
            return null;
        
        }
    catch(NullPointerException | StackOverflowError ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    return null;
    }
    
    }
    
    @Override
    public Entities.Direccion requestAddress(String name){
    
        try{
        
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
            
            Entities.Direccion address;
            
            for(java.util.Map.Entry<String,String>entry:javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().entrySet()){
            
                System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
            System.out.print(name);
            
            if((servletRequest.getParameter("form:cities_input")!=null)  && (servletRequest.getParameter("form:select_input")!=null) && (servletRequest.getParameter("form:primerDigito")!=null && !servletRequest.getParameter("form:primerDigito").equals("")) && (servletRequest.getParameter("form:segundoDigito")!=null && !servletRequest.getParameter("form:segundoDigito").equals("")) && (servletRequest.getParameter("form:tercerDigito")!=null && !servletRequest.getParameter("form:tercerDigito").equals("")) && (servletRequest.getParameter("form:adicional")!=null) ){
            
              Entities.GpsCoordinates gps=new Entities.GpsCoordinates();
              
             Entities.State estado=this._stateController.findByName(name);
                
              Entities.CityPK cityPK=this._cityController.findWithinState(estado, servletRequest.getParameter("form:cities_input")).getCityPK();
                
              address=new Entities.Direccion();
              
              Entities.DireccionPK addressPK=new Entities.DireccionPK();
              
              System.out.print("CITYPK "+cityPK);
              
              addressPK.setCITYSTATEidSTATE(cityPK.getSTATEidSTATE());
              
              addressPK.setCITYidCITY(cityPK.getIdCITY());
              
              address.setDireccionPK(addressPK);
              
              address.setCity(this._cityController.find(cityPK));
              
              address.setTYPEADDRESSidTYPEADDRESS(_tipoController.findByname(servletRequest.getParameter("form:select_input")));
              
              address.setPrimerDigito(servletRequest.getParameter("form:primerDigito"));
              
              address.setSegundoDigito(servletRequest.getParameter("form:segundoDigito"));
              
              address.setTercerDigito(new java.lang.Short(servletRequest.getParameter("form:tercerDigito")));
              
              address.setAdicional(servletRequest.getParameter("form:adicional"));
              
              //Geocode Via XML
              
              String stringUrl="Colombia,"+address.getCity().getState().getName()+","+address.getCity().getName()+","+address.getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+"+"+address.getPrimerDigito()+"+#+"+address.getSegundoDigito()+"+-+"+address.getTercerDigito();
              
              System.out.print(stringUrl);
              
              URL myUrl=new URL("http://maps.googleapis.com/maps/api/geocode/xml?address="+stringUrl+"&sensor=false");
              
              DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
              
              DocumentBuilder builder=factory.newDocumentBuilder();
              
              Document doc=builder.parse(myUrl.openStream());
              
              XPathFactory xPathFactory=XPathFactory.newInstance();
              
              XPath xpath=xPathFactory.newXPath();
              
              javax.xml.xpath.XPathExpression expr=xpath.compile("/GeocodeResponse/result/geometry/location/lat");
              
              String latitude=expr.evaluate(doc,XPathConstants.STRING).toString();
              
              expr=xpath.compile("/GeocodeResponse/result/geometry/location/lng");
              
              String longitude=expr.evaluate(doc,XPathConstants.STRING).toString();
              
              System.out.print("LATITUDE "+latitude+" Longitude "+longitude);
              
              if(latitude!=null && !latitude.equals("") && longitude!=null && !longitude.equals("")){
              
                  gps.setLatitude(java.lang.Double.valueOf(latitude));
                  
                  gps.setLongitude(java.lang.Double.valueOf(longitude));
              
              }
              
              address.setGpsCoordinatesidgpsCoordinated(gps);
            
              
              
              System.out.print("GPS COORDINATES "+address.getGpsCoordinatesidgpsCoordinated().getLatitude()+"-"+address.getGpsCoordinatesidgpsCoordinated().getLongitude());
              
              return address;  
                
            }
            else{
            
                System.out.print("There is Some null value");
            
            }
            
            return null;
        }
        catch(NullPointerException | XPathExpressionException | IOException | SAXException | ParserConfigurationException  | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        } 
        
    
    }
    
    
    @Override
        public String getRandomImageName() {
        int i = (int) (Math.random() * 10000000);
         
        return String.valueOf(i);
    }
        
        @Override
        public String genarateFolderName(){
        try{
        String folderName;
 
        Clases.FilesManagementInterface fileManagement=new Clases.FilesManagement();
        
        javax.servlet.ServletContext sContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        folderName=fileManagement.generateFolderName(sContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images");
        
        fileManagement.cleanFolder(sContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images");
        
        System.out.print("Folder Name "+folderName);
        
        fileManagement.createFolder(sContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator, folderName);
        
        return folderName;
        }
        
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return "";
        }
        
        }
        
        @Override
        public String gerFileName(String folderName){
        
            try{
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
                
                if(file.exists())
                {
                if(file.listFiles()!=null && file.listFiles().length>0){
                
                    System.out.print("FILES IN THIS FOLDER "+file.listFiles().length);
                    
                   return java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName+java.io.File.separator+file.listFiles()[0].getName();
                
                }
                
            }
                
            return"/images/noImage.png";
            }
            catch(Exception ex){
            
                return "images/noImage.png";
            
            }
        
        }
        
            private  byte []  convertToByteArray(UploadedFile file){
   try{
       
       InputStream inputStream=file.getInputstream();
       byte [] buffer=new byte[8192];
       int bytesRead;
       
       ByteArrayOutputStream output=new ByteArrayOutputStream();
       
       while((bytesRead=inputStream.read(buffer))!=-1){
       output.write(buffer, 0, bytesRead);
       }
       
       return output.toByteArray();
       
   }
   catch(Exception ex){
   Logger.getLogger(FileUploadBean.class.getName()).log(Level.SEVERE,null,ex);
   return null;
   }
}
            @Override
            public void handleUpload(String folderName, org.primefaces.event.FileUploadEvent event){
            
                try{
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();

                    
                    _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
                    
                    _fileUploadBean.servletContainer(event.getFile(), folderName, 500, 500);
                
                }
                catch(Exception | StackOverflowError ex){
                
                    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                }
            
            }
   
            @Override
        public Entities.Imagen getImage(String folderName){
        
            try{
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
                
                if(file!=null && file.listFiles()!=null && file.listFiles().length>0){
                    
                    System.out.print("THIS FOLDER HAS "+file.listFiles().length+" FILES");
                
                    java.io.File imagen=file.listFiles()[0];
                    
                    Path path=Paths.get(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName+java.io.File.separator+imagen.getName());
                    
                    byte[] bFile = Files.readAllBytes(path);
                                     
                   System.out.print("IMAGE NAME "+imagen.getName());
                  
                 
                  System.out.print("IMAGE SPLIT . SIZE "+EdenString.split(".", imagen.getName()).length);
                  
                  if(EdenString.split(".", imagen.getName()).length==2){
                  
                      Entities.Imagen result=new Entities.Imagen();
                      
                      result.setImagen(bFile);                                     
                      
                      result.setExtension("."+Clases.EdenString.split(".", imagen.getName())[1]);
                      
                      
                      
                     return result;
                  
                  }
                  else{
                  
                      return null;
                      
                  }
                
                }
                
                return null;
            }
            catch(NullPointerException | java.io.IOException  | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return null;
            
            }
        
        }
        
        @Override
        public String updateCity(){
        
            try{
            
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
                
                String city=request.getParameter("city");
                
                String state=request.getParameter("form:state_input");
                
                if(state!=null && !state.equals(""))
                {
                
                    Entities.State estado=this._stateController.findByName(state);
                    
                    return this.getCityName(estado.getCityCollection(), city);
                    
                }
                
                
                return null;
                
            }
            catch(Exception | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                return "";
            
            }
        
        }
        
        private String getCityName(java.util.Collection<Entities.City>cities,String name){
        
            for(Entities.City city:cities){
            
            if(EdenString.similarity(city.getName(), name)>0.9){
            
                return city.getName();
            
            }
            
            }
            
            return null;
        
        }
        
        @Override
   public void oncapture(org.primefaces.event.CaptureEvent event,String fileName, Entities.Imagen image,String folderName){
   
       try{
       
           System.out.print("Capturing");
           
           System.out.print("FolderName "+folderName);
           
           String random=getRandomImageName();
           
           javax.faces.context.FacesContext  fc=javax.faces.context.FacesContext.getCurrentInstance();
          
          javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)fc.getExternalContext().getContext();
       
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
           
           if(file.exists()){
           
           filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
           }
           
           _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
           
          fileName=java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName+java.io.File.separator+random+".jpeg";
           
          System.out.print("FileName "+fileName);
          
          byte[]data=event.getData();
          
          System.out.print("Data Extracted");
           
 
          
         if(image==null){
         
             image=new Entities.Imagen();
         
         }
         
         image.setImagen(data);
         
         image.setExtension(".jpeg");
          
          
          
          System.out.print("Real Path "+servletContext.getRealPath(""));
          
        String newFileName = servletContext.getRealPath("") + java.io.File.separator + "resources" + java.io.File.separator + "demo" +java.io.File.separator + "images" + java.io.File.separator +folderName+ java.io.File.separator + random + ".jpeg";
        
       
        
        
        System.out.print(newFileName);
        
       
        
        FileImageOutputStream imageOutput;
        
        imageOutput=new FileImageOutputStream(new java.io.File(newFileName));
        
        imageOutput.write(data,0,data.length);
        
        imageOutput.close();
  
        
       
       }
       catch(Exception ex){
   
           
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
   
   
   
   
   
   /**
	 * 
	 * @param loginAdministrador
	 * @param address
	 * @param folderName
	 * @param phoneNumber
	 * @param idProfession
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
   public void register(Entities.LoginAdministrador loginAdministrador,Entities.Direccion address,String folderName,String phoneNumber,int idProfession){
   
       try{
       
           java.util.List<Entities.Direccion>direccionCollection=new java.util.ArrayList<Entities.Direccion>();
           
           if(address!=null){
           
               //_addressController.create(address);
               
               direccionCollection.add(address);
           
           }
           
           loginAdministrador.setDireccionCollection(direccionCollection);
           
           loginAdministrador.setImagenIdimagen(this.getImage(folderName));
           
           Entities.Phone phone=new Entities.Phone();
           
           if(phoneNumber.length()>0){
               
           phone.setPhoneNumber(phoneNumber);
           
           phone.setLoginAdministradorusername(loginAdministrador);
           
           System.out.print("PHONE NUMBER "+phone.getPhoneNumber());
           
           java.util.List<Entities.Phone>phoneList=new java.util.ArrayList<Entities.Phone>();
           
           phoneList.add(phone);
           
           loginAdministrador.setPhoneCollection(phoneList);
           
           }
           
           loginAdministrador.setProfessionIdprofession(_professionController.find(idProfession));
           
           Entities.AppGroup appGroup=new Entities.AppGroup();
           
           appGroup.setLoginAdministrador(loginAdministrador);
           
           Entities.AppGroupPK appGroupPK=new Entities.AppGroupPK();
           
           appGroupPK.setGroupid("guanabara_user");
           
           appGroupPK.setLoginAdministradorusername(loginAdministrador.getUsername());
           
           appGroup.setAppGroupPK(appGroupPK);
           
          // Entities.Confirmation confirmation=new Entities.Confirmation();
           
          // confirmation.setConfirmed(false);
           
           //confirmation.setLoginAdministrador(loginAdministrador);
           
           //confirmation.setLoginAdministradorusername(loginAdministrador.getUsername());
           
           _loginAdministradorController.createAccount(loginAdministrador, appGroup);
                   
                          
       }
       catch(IllegalArgumentException | NullPointerException | StackOverflowError ex){
       
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
   
   
    
}
