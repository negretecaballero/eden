/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.View;

/**
 *
 * @author luisnegrete
 */
public class CheckMailConfirmationView extends Clases.BaseBacking {
    
    @javax.inject.Inject @javax.enterprise.context.Dependent private EdenRegistration.Controllers.CheckMailConfirmationController _checkConfirmationController;
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            System.out.print(this.getRequest().getUserPrincipal().getName());
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public String sendMail(){
    
        try{
        
            _checkConfirmationController.resendMail(this.getRequest().getUserPrincipal().getName());
            
            return "success";
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return "failure";
            
        }
    
    }
    
    
    
}
