/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.View;

import Clases.BaseBacking;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;


/**
 *
 * @author luisnegrete
 */
public class ConfirmationView extends BaseBacking{

    /**
     * Creates a new instance of ConfirmationView
     */
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenRegistration.Controllers.UserConfirmationController _confirmationController;
    
    private String username;
    
   
    
    public String getUsername(){
    
        return this.username;
    
    }
    
    public void setUsername(String username){
    
        this.username=username;
    
    }
    
   
    public ConfirmationView() {
    }
    
    
    
    @PostConstruct
    public void init(){
    
    this.username=_confirmationController.getUsername();
     
    System.out.print(this.username);
    
    }
    
    public void confirmUser(javax.faces.event.ActionEvent event){
   
       try{
       
           System.out.print("CONFIRMING ACCOUNT");
           
          switch(_confirmationController.confirm(this.username)){
 
              case CONFIRMED:{
              
                  javax.faces.context.ExternalContext ec=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                  
                  System.out.print(ec.getRequestContextPath()+" PATH CONTEXT");
                  
                  ec.redirect(ec.getRequestContextPath()+"/login.xhtml");
                  
                  System.out.print("USER CONFIRMEd");
                  
              }
              
              case UNCONFIRMED:{
              
                  javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Error Confirming Account");
                  
                  msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                  
                  this.getContext().addMessage(null, msg);
                  
              }
          
          }
     
           
       
       }
       catch(Exception ex){
       
           throw new FacesException("Exception Caugth");
       
       }
    
    }
    
}
