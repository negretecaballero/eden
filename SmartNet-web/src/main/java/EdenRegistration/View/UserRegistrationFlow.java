/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenRegistration.View;

import CDIBeans.DesplegableCitiesInterface;
import CDIBeans.DesplegableProfessionInterface;
import CDIBeans.ImageControllerDelegate;
import CDIBeans.LoginAdministradorControllerInterface;
import CDIBeans.StateControllerInterface;
import CDIEden.FlowRegistrationControllerQualifier;
import Clases.BaseBacking;
import Clases.Direccion;
import SessionClasses.MailManagement;
import Entities.City;

import Entities.DireccionPK;
import Entities.GpsCoordinates;
import Entities.LoginAdministrador;
import Entities.Phone;
import Entities.Profession;
import Entities.State;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.flow.FlowScoped;
import javax.inject.Inject;
import javax.inject.Named;
import jaxrs.service.AppGroupFacadeREST;
import jaxrs.service.ConfirmationFacadeREST;
import jaxrs.service.DireccionFacadeREST;
import jaxrs.service.GpsCoordinatesFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.PhoneFacadeREST;
import jaxrs.service.ProfessionFacadeREST;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author luisnegrete
 */
@Named("flowRegistrationScope")
@FlowScoped("registration")
public class UserRegistrationFlow extends BaseBacking implements java.io.Serializable{
    
    private Entities.Direccion address;

   /* @Inject
    transient private DesplegableCitiesInterface desplegableCities;

    @EJB
    private DireccionFacadeREST direccionFacadeREST;
    
    @EJB 
    private LoginAdministradorFacadeREST userFacadeREST;
    
  
    @EJB
    private ConfirmationFacadeREST confirmationFacadeREST;
    
    @EJB
    ProfessionFacadeREST professionFacadeREST;
    
    @EJB
    private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
    
    @EJB
    private PhoneFacadeREST phoneFacadeREST;
    
    @EJB
    private AppGroupFacadeREST appGroupFacadeREST;*/
    
    @Inject
    @CDIBeans.ImageControllerQualifier
    transient private ImageControllerDelegate imageControllerDelegate;
    
    @javax.inject.Inject
    
    private transient EdenRegistration.Controllers.UserRegistrationController _userRegistrationController;
    
    @javax.inject.Inject
    @CDIBeans.AddressTypeControllerQualifier
    transient private CDIBeans.AddressTypeControllerInterface _addressTypeController;
    
    private boolean _checkBox;
    
    public boolean getCheckBox(){
    
        return _checkBox;
    
    }
    
    public void setCheckBox(boolean checkBox){
    
        _checkBox=checkBox;
    
    }
    
    private boolean _uploadBox;
    
    public boolean getUploadBox(){
    
        return _uploadBox;
    
    }
    
    public void setUploadBox(boolean uploadBox){
    
    _uploadBox=uploadBox;
    
    }
    
    
    @javax.inject.Inject 
    @CDIBeans.ApplicationControllerQualifier
    transient private CDIBeans.ApplicationController applicationController;
    
    private static final long serialVersionUID = 1L;


    
    private LoginAdministrador user;
    
    private List<Profession>professions;
    
    private int idprofession;
    
    private String phone;
    
    private List<State>states;
    
    private String stateId;
    

    
    private List<City>cities;
    
    private Direccion direccion;
    
    private String state;
    
    private String city;
    
    private String fileName;
    
    private boolean showCapturePanel;
    
    private Entities.Application application;
    
    private String folderName;
    
    private double _longitude;
    
    private double _latitude;
    
    @Inject 
    @CDIBeans.LoginAdministradorControllerQualifier
    transient private LoginAdministradorControllerInterface loginAdministradorController;
    
    @Inject 
    @CDIBeans.StateControllerQualifier
    transient private StateControllerInterface stateController;
    
    public Entities.Application getApplication(){
    
        return this.application;
    
    }
    
    public void setApplication(Entities.Application application){
    
        this.application=application;
    
    }
    
    public boolean getShowCapturePanel(){

    return this.showCapturePanel;

}
    
    public void setShowCapturePanel(boolean showCapturePanel){
    
        this.showCapturePanel=showCapturePanel;
    
    }
    
    
    
    public void setFileName(String fileName){
    
        this.fileName=fileName;
    
    }
    public String getFileName(){
    
    return this.fileName;
    
    }
    
    public String getProfileImage(){
    
        return _userRegistrationController.gerFileName(this.folderName);
    
    }
    
    private Entities.Imagen image;
    
    public String getState(){
    
       return this.state;
    
    }
    
    public void setState(String state){
    
    this.state=state;
    
    }
    
    public String getCity(){
    
        return this.city;
    
    }
    
    public void setCity(String city){
    
    this.city=city;
    
    }
    
    public Direccion getDireccion(){
    
        if(this.direccion==null){
        
        this.direccion=new Clases.Direccion();
        
        }
    
    return this.direccion;
    
    }
    
    public void setDireccion(Direccion direccion){
    
    this.direccion=direccion;
    
    }
    
    public List<City> getCities(){
    
    return this.cities;
    
    }
    
    public void setCities(List<City>cities){
    
    this.cities=cities;
    
    }
  
    
    public void setStates(List<State>states){
    
    this.states=states;
    
    }
    
    public List<State>getStates(){
    
    return this.states;
    
    }
    
    public void setStateId(String stateId){
    
        this.stateId=stateId;
    
    }
    
    public String getStateId(){
    
    return this.stateId;
    
    }
    
    public void setPhone(String phone){
    
    this.phone=phone;
    
    }
    
    public String getPhone(){
    
        return this.phone;
    
    }
    
    public int getIdprofession(){
    
      return this.idprofession;  
    
    }
    
    public void setIdprofession(int idprofession){
    
        this.idprofession=idprofession;
    
    }
    
    private String _firstDigit;
    
    private String _secondDigit;
    
    private int _thirdDigit;
    
    private String _description;
    
    private String _type;
    
    public String getFirstDigit(){
    
        return _firstDigit;
    
    }
    
    public void setFirstDigit(String firstDigit){
        
        _firstDigit=firstDigit;
        
    }
    
    public String getSecondDigit(){
    
        return _secondDigit;
    
    }
    
    public void setSecondDigit(String secondDigit){
    
        _secondDigit=secondDigit;
    
    }
    
    public int getThirdDigit(){
    
       return _thirdDigit;
    
    }
    
    public void setThirdDigit(int thirdDigit){
    
        _thirdDigit=thirdDigit;
    
    }
    
    public String getDescription(){
    
        return _description;
    
    }
    
    public void setDescription(String description){
    
        _description=description;
    
    }
    
    public String getType(){
    
        return _type;
    
    }
    
    public void setType(String type){
    
    _type=type;
    
    }
    
    
    /*@Inject
    transient private DesplegableProfessionInterface desplegableProfession;*/
    
    @javax.inject.Inject 
    
    @FlowRegistrationControllerQualifier
    private CDIEden.FlowRegistrationController flowRegistrationController;
	@Inject()
        @CDIBeans.DesplegableCitiesQualifier
	private transient DesplegableCitiesInterface desplegableCities;
	@EJB()
	private DireccionFacadeREST direccionFacadeREST;
	@EJB()
	private LoginAdministradorFacadeREST userFacadeREST;
	@EJB()
	private ConfirmationFacadeREST confirmationFacadeREST;
	@EJB()
	ProfessionFacadeREST professionFacadeREST;
	@EJB()
	private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
	@EJB()
	private PhoneFacadeREST phoneFacadeREST;
	@EJB()
	private AppGroupFacadeREST appGroupFacadeREST;
	@Inject()
        @CDIBeans.DesplegableProfessionQualifier
	private transient DesplegableProfessionInterface desplegableProfession;
    
    
    /**
     * Creates a new instance of FlowRegistrationScope
     */
    
    @PostConstruct
    public void init(){

        this.user=new LoginAdministrador();
   
         
        if(this.folderName==null || this.folderName.equals("")){
      
            this.folderName=_userRegistrationController.genarateFolderName();
      
           System.out.print("FOLDERNAME "+this.folderName);
            
        }
        
       //this.stateId=states.get(0).getName();
       
        //System.out.print("Init State Id "+stateId);
              
        //this.cityPK=cities.get(0).getCityPK();
        
       this._userRegistrationController.initCitiesList();
      
    }
    
    public List<Profession>getProfessions(){
    
        return this.professions;
    
    }
    
    public void setProfessions(List<Profession>professions){
    
    this.professions=professions;
        
    }
    
    
    public void setUser(LoginAdministrador user){
    
    this.user=user;
    
    }
    
    public LoginAdministrador getUser(){
    
    return this.user;
    
    }
    
    
    public UserRegistrationFlow() {
        
        try{
        
        this.city="";
        
        this._checkBox=true;
        
        this._uploadBox=false;
        
          
        this.fileName=java.io.File.separator+"images"+java.io.File.separator+"noImage.png";
        
        System.out.print("Initializing FlowRegistration");
       
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    }
   
 
   
 
   
   
   public void postValidateComponents(ComponentSystemEvent event){
   
     
   if(this.getCurrentView().equals("/registration/registration.xhtml")){
       
       UIComponent component=event.getComponent();
   
   System.out.print("You are in Registration");
   
   
   if(((javax.faces.component.UIInput)component.findComponent(":form:mail")).getLocalValue()!=null && ((javax.faces.component.UIInput)component.findComponent("mailConfirmation")).getLocalValue()!=null)
   
   {
   
   String mail=((UIInput)component.findComponent("form:mail")).getLocalValue().toString();
   
   String mailConfirmation=((UIInput)component.findComponent("form:mailConfirmation")).getLocalValue().toString();
   
   
   
   
   
   if(!_userRegistrationController.matchMail(mail, mailConfirmation)){
   
   FacesContext context=FacesContext.getCurrentInstance();
  
       FacesMessage msg=new FacesMessage("Mails are not the same");
       
       msg.setSeverity(FacesMessage.SEVERITY_ERROR);
       
       context.addMessage(((UIInput)component.findComponent("mail")).getLocalValue().toString(),msg);
 
   
   context.renderResponse();
   
   
   }
   
   if(_userRegistrationController.checkExistence(mail)){
   
       javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("There is already an user registered with this mail");
       
       msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
       
       
       
       this.getContext().addMessage(event.getComponent().getClientId(), msg);
       
       this.getContext().renderResponse();
       
   
   }
   

   }
   
   
   }
   
   if(this.getCurrentView().equals("/registration/final.xhtml")){
   
   UIComponent component=event.getComponent();
   
   UIInput phone=(UIInput)component.findComponent("phone");
   
   UIInput phoneConfirmation=(UIInput)component.findComponent("phoneConfirmation");
   
   if(!phone.getLocalValue().equals(phoneConfirmation.getLocalValue())){
   
   FacesMessage msg=new FacesMessage("Phone numbers are not the same");
   
   msg.setSeverity(FacesMessage.SEVERITY_ERROR);
   
   getContext().addMessage(phone.getClientId(), msg);
   
   getContext().renderResponse();
   
   
   }
   
   }
   
   }
   
   public void stateChanged(AjaxBehaviorEvent event){

     _userRegistrationController.updateCities(_userRegistrationController.getRequestStateName());
       
    
       
     if(this._userRegistrationController.updateCity()!=null && !this._userRegistrationController.updateCity().equals("")){
         
          this.city=this._userRegistrationController.updateCity();
     
     }
   
   }
    
   public void cityChanged(AjaxBehaviorEvent event){
   
  // this.city=_userRegistrationController.getCityName(_userRegistrationController.getCityPK());
   
   }
   
   public String Register(){
   
   try{
       
       System.out.print("Registering User");
       
       Map<String,String>request=getContext().getExternalContext().getRequestParameterMap();
       
       for(Map.Entry<String,String>entry:request.entrySet()){
       
       System.out.print(entry.getValue()+"-"+entry.getKey());
       
       }
   
      DireccionPK direccionPK=new DireccionPK();
      
      direccionPK.setCITYSTATEidSTATE(stateController.findByName(this.stateId).getIdSTATE());
      
//      direccionPK.setCITYidCITY(this.cityPK.getIdCITY());
       
      Entities.Direccion address=new Entities.Direccion();
      
      address.setDireccionPK(direccionPK);
        
      System.out.print("RAW TIPE "+this.direccion.getTipo());
      
      Entities.TypeAddress tipo=_addressTypeController.findByname(this.direccion.getTipo());
      
     System.out.print("TIPO: "+tipo.getTypeAddress());
      
      address.setTYPEADDRESSidTYPEADDRESS(tipo);
      
      address.setPrimerDigito(this.direccion.getPrimerDigito());
      
      address.setSegundoDigito(this.direccion.getSegundoDigito());
      
      address.setTercerDigito((short)this.direccion.getTercerDigito());
      
      address.setAdicional(this.direccion.getAdicional_descripcion());
     

      GpsCoordinates coordinates=new GpsCoordinates();
      
      coordinates.setLatitude(_latitude);
      
      coordinates.setLongitude(_longitude);
     
      address.setGpsCoordinatesidgpsCoordinated(coordinates);
      
      System.out.print("Longitude "+_longitude+" Latitude "+_latitude);
      
      System.out.print("Address to Create "+this.direccion.getPrimerDigito()+"-"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito());
      
      
      //direccionFacadeREST.create(address);
      
      if(this.image!=null){
      
          imageControllerDelegate.create(this.image);
      
      }
      
      List <Entities.Direccion>dir=new <Entities.Direccion>ArrayList();
      
      dir.add(address);
      
      this.user.setDireccionCollection(dir);
      
//      this.user.setProfessionIdprofession(professionFacadeREST.find(this.idprofession));
      
      this.user.setImagenIdimagen(image);
      
      Phone phones=new Phone();
      
     /* if(userFacadeREST.find(this.user.getUsername())!=null){
      
      if(this.phone.length()>0){
          
       phones.setLoginAdministradorusername(userFacadeREST.find(this.user.getUsername()));
       
      }
       
      this.loginAdministradorController.update(this.user);
      
      }
     
     
     
      userFacadeREST.create(this.user);*/
      
      MailManagement mailManagement=new MailManagement();
     
      javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
      
      String urlPath=String.valueOf(servletRequest.getRequestURL()).replaceAll(servletRequest.getRequestURI(), "");
      
      System.out.print("URL PATH "+urlPath);
      
      mailManagement.sendMail(this.user.getUsername(),"Mail Confirmation","Mr. "+this.user.getName()+" Your password is "+this.user.getPassword()+"\nwe Invite you to confirm your account, please go to this link "+urlPath+"/SmartNet-web/confirmation/confirmation.xhtml?username="+user.getUsername()+"&type=user");
      
       
      
      System.out.print("Mail sent to "+this.user.getUsername());
     
      
      if(this.phone.length()>0){
      phones.setLoginAdministradorusername(this.user);
      }
      
      
      if(this.phone.length()>0){
          
     // phones.setPhoneNumber(this.phone);
      
      
      //this.phoneFacadeREST.create(phones);
      
      }
      
     /* if(appGroupFacadeREST.findByUsername(new PathSegmentImpl("bar;groupid=guanabara_user;loginAdministradorusername="+user.getUsername()+""))==null)
      {
          
          System.out.print("Adding App Group");
          
    AppGroup appgroup=new AppGroup();
    
    AppGroupPK appgroupPK=new AppGroupPK();
    
    appgroupPK.setGroupid("guanabara_user");
    
    appgroupPK.setLoginAdministradorusername(this.user.getUsername());
    
    appgroup.setAppGroupPK(appgroupPK);
    
    //this.appGroupFacadeREST.create(appgroup);
      
     
     Confirmation confirmation=new Confirmation();
     
     confirmation.setLoginAdministrador(user);
     
     confirmation.setLoginAdministradorusername(user.getUsername());
     
     confirmation.setConfirmed(false);
     
    // confirmationFacadeREST.create(confirmation);
     
     //MailManagement mailManagement= new MailManagement();
     
     //javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
     
     //String urlPath=String.valueOf(servletRequest.getRequestURL()).replaceAll(servletRequest.getRequestURI(), "");
      
     
    // mailManagement.sendMail(this.user.getUsername(),"Mail Confirmation","Mr. "+this.user.getName()+" Your password is "+this.user.getPassword()+"\nwe Invite you to confirm your account, please go to this link "+urlPath+"/SmartNet-web/confirmation/confirmation.xhtml?username="+user.getUsername()+"&type=user");
      
      
      
   }
      
      else{
      
          System.out.print("This user Exists Already");
          
          FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"This user exists already");
          
          getContext().addMessage(null,msg);
      
      }*/
      
      
       
   return "success";
   
   }
   catch(EJBException ex){
   
   Logger.getLogger(UserRegistrationFlow.class.getName()).log(Level.SEVERE,null,ex);
       
   return "failure";
   }
   
   
   }
   
   public String Registration(){
   
       try{
       
           _userRegistrationController.register(this.user, this.address,this.folderName,this.phone,this.idprofession);
       
           return "success";
       }
       catch(Exception | StackOverflowError ex)
       {
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           return "failure";
       
       }
       
   }
   
   public void capture(org.primefaces.event.CaptureEvent event){
   
   try{
   
       System.out.print("FOLDERNAME "+this.folderName);
       
       _userRegistrationController.oncapture(event, this.fileName, this.image,this.folderName);
   
   }
   catch(Exception ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
   }
   
   }
   

    public void booleanClicked(AjaxBehaviorEvent event){
    
        System.out.print("CheckBox clicked");
        
        if(event.getComponent() instanceof org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox){
        
        org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox selectBoolean=(org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox) event.getComponent();
        
        this.showCapturePanel=selectBoolean.isSelected();
     
          }
    
    }
    
    public void handleFileUpload(FileUploadEvent event){
    
        try{
        
            
            
            
            _userRegistrationController.handleUpload(this.folderName, event);
            
        }

        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    }
    

     
   
    

    public void preRenderView(String view){
    
        try{
        
        javax.faces.component.UIViewRoot viewRoot=(javax.faces.component.UIViewRoot)javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
       
        System.out.print("View Id "+this.getContext().getViewRoot().getViewId());
            
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);  
        
        }
    
    }
    
    public void selectBooleanCheckbox(javax.faces.event.AjaxBehaviorEvent event){
    
    try{
    
        System.out.print("ChekBox Value"+this._checkBox);
        
        System.out.printf("UploadCheckBox "+this._uploadBox);
        
        System.out.print("Client ID "+event.getComponent().getClientId());
        
        
     if(event.getComponent().getClientId().equals("form:tabView:uploadCheck")){
     
         if(this._uploadBox){
         
             this._checkBox=false;
         
         }
         else{
         
             this._checkBox=true;
         
         }
     
     }
     
     else if(event.getComponent().getClientId().equals("form:tabView:checkBox")){
     
         if(this._checkBox){
         
             this._uploadBox=false;
         
         }
         else {
         
         this._uploadBox=true;
         }
     
     }
        
    }
    catch(javax.ejb.EJBException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    public String redirect(){
        
        try{
    
        System.out.print("REDIRECT FLOW REGISTRATION "+this.getContext().getViewRoot().getViewId());
        
        switch(this.getContext().getViewRoot().getViewId()){
        
            case "/registration/address.xhtml":{
            
                this.address=_userRegistrationController.requestAddress(this.stateId);
                
                System.out.print("ADRESS "+address);
  
               return "complete";
               
                
            }
            
            
        
        }
        
        return "";
        
        }
        catch(NullPointerException ex){
        
       javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Try Again Please");
        
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
        
        this.getContext().addMessage(null,msg);
        
        
        
        return "";
            
        }
        
        
    
    }
     
}