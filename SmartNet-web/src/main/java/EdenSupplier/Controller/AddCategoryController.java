/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.Controller;

/**
 *
 * @author luisnegrete
 */


public interface AddCategoryController {
    
    void uploadPicture(org.primefaces.event.FileUploadEvent fileUploadEvent);
    
    boolean checkExistence(String name);
    
    void createCategory(String name);
    
}
