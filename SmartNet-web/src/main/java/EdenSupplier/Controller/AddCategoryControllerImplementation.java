/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.Controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class AddCategoryControllerImplementation implements AddCategoryController {
    
    @javax.inject.Inject private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @javax.inject.Inject  private CDIBeans.SupplierProductCategoryController _supplierProductCategoryController;
    
    @javax.inject.Inject  private CDIBeans.ImageControllerDelegate _imageController;
    
    @Override
    public void uploadPicture(org.primefaces.event.FileUploadEvent fileUploadEvent){
    
    try{
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
     _fileUploadBean.servletContainer(fileUploadEvent.getFile(), session.getAttribute("username").toString(), 500, 500);
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    @Override
    public boolean checkExistence(String name){
    
        try{
        
          javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
          
          if(session.getAttribute("selectedOption")instanceof Entities.Supplier){
          
              Entities.Supplier supplier=(Entities.Supplier)session.getAttribute("selectedOption");
              
              if(supplier.getSupplierProductCategoryCollection()!=null && !supplier.getSupplierProductCategoryCollection().isEmpty()){
              
              for(Entities.SupplierProductCategory supplierProductCategory:supplier.getSupplierProductCategoryCollection()){
              
                  if(supplierProductCategory.getName().equals(name)){
                  
                  return true;
                  
                  }
              
              }
              
              }
          
          }
            
         return false;
            
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
            
        }
    
    }
    
    @Override
    public void createCategory(String name){
    
        try{
        
             
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           
           if(session.getAttribute("selectedOption")instanceof Entities.Supplier){
           
               Entities.Supplier supplier=(Entities.Supplier)session.getAttribute("selectedOption");
               
               
               /*SupplierProductCategoryPK*/
               Entities.SupplierProductCategoryPK supplierProductCategoryPK=new Entities.SupplierProductCategoryPK();
       
               supplierProductCategoryPK.setSupplierIdsupplier(supplier.getSupplierPK().getIdSupplier());
               
               supplierProductCategoryPK.setSupplierLoginAdministradorUsername(supplier.getSupplierPK().getLoginAdministradorUsername());
               
               
               /*SupplierProductCategory*/
           
               Entities.SupplierProductCategory supplierProductCategory=new Entities.SupplierProductCategory();
               
               supplierProductCategory.setSupplierProductCategoryPK(supplierProductCategoryPK);
               
               supplierProductCategory.setName(name);
               
               supplierProductCategory.setIdSupplier(supplier);
               
               /*Create Images*/
               
               if(this._fileUploadBean.getPhotoList()!=null && !this._fileUploadBean.getPhotoList().isEmpty()){
               
                   for(Entities.Imagen image:this._fileUploadBean.getPhotoList())
                   {
                   
                       this._imageController.create(image);
                   
                   }
               }
               
               supplierProductCategory.setImagenCollection(this._fileUploadBean.getPhotoList());
               
               this._supplierProductCategoryController.create(supplierProductCategory);
               
               javax.servlet.ServletContext sc=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
               
               this._fileUploadBean.Remove(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
           }
           else{
           
               throw new IllegalArgumentException("Object must be of type "+Entities.Supplier.class.getName());
           
           }
            
            
        }
        catch(javax.ejb.EJBException | IllegalArgumentException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
