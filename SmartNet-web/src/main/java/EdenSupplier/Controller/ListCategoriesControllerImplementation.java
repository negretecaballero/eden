/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.Controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class ListCategoriesControllerImplementation implements ListCategoriesController{
   
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierProductCategoryController _supplierProductCategoryProductController;
   
    @Override
    public org.primefaces.model.LazyDataModel<Entities.SupplierProductCategory>getSupplierProductCategoryLazyModel(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Supplier){
            
                Entities.Supplier supplier=(Entities.Supplier)session.getAttribute("selectedOption");
            
                
                System.out.print("Categories Product at this Supplier "+supplier.getSupplierProductCategoryCollection().size());
                
                if(supplier.getSupplierProductCategoryCollection()!=null && !supplier.getSupplierProductCategoryCollection().isEmpty()){
                
                    return new FrontEndModel.SupplierProductCategoryLazyModel((java.util.List<Entities.SupplierProductCategory>)supplier.getSupplierProductCategoryCollection());
                
                     
                    
                }
              
                return null;
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+Entities.SupplierProductCategory.class.getName());
            
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public void deleteSupplierCategpryProduct(Entities.SupplierProductCategoryPK key){
    
        try{
        
            this._supplierProductCategoryProductController.delete(key);
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
