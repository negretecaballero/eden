/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.Controller;

/**
 *
 * @author luisnegrete
 */
public interface SupplierIndexController {
    
    java.util.Vector<Entities.Supplier>getSuppliers();
    
    Entities.Supplier getByNit(String nit);
    
}
