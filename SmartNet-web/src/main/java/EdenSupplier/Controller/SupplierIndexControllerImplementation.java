/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.Controller;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.inject.Alternative
public class SupplierIndexControllerImplementation implements SupplierIndexController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierController _supplierController;
    
    @Override
    public java.util.Vector<Entities.Supplier>getSuppliers(){
    
        try{
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("username")!=null){
            
                return this._supplierController.getByUsername(session.getAttribute("username").toString());
            
            }
            else{
            
                return null;
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public Entities.Supplier getByNit(String nit){
    
        try{
        
            return _supplierController.findByNit(nit);
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
