
package EdenSupplier.View;

import Clases.BaseBacking;

/**
 *
 * @author luisnegrete
 */
public class AddCategory extends BaseBacking{
    
    private Entities.SupplierProductCategory _supplierProductCategory;
    
    private String name;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenSupplier.Controller.AddCategoryController _addCategoryController;
    
    public String getName(){
    
        return this.name;
    
    }
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
        
            /*Create SupplierCategory Product Object*/
            
            if(_supplierProductCategory==null){
                    
                _supplierProductCategory= new Entities.SupplierProductCategory();
                
                    
                    }
            
            if(!_fileUploadBean.getClassType().equals(this.getClass().getName())){
            
                javax.servlet.ServletContext sc=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                
                
                _fileUploadBean.Remove(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                
                
                _fileUploadBean.setClassType(this.getClass().getName());
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                
                }
    
    }
    
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            if(event.getComponent().findComponent("form:name")!=null && event.getComponent().findComponent("form:name")instanceof javax.faces.component.UIInput){
            
                System.out.print("Component instance of UIINput");
                
                javax.faces.component.UIInput input=(javax.faces.component.UIInput)event.getComponent().findComponent("form:name");
                
                if(input.getLocalValue()!=null){
                
                    String name=input.getLocalValue().toString();
                
                }
            
            }
        
        }catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    public void uploadImage(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            _addCategoryController.uploadPicture(event);
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public Entities.SupplierProductCategory getSupplierProductCategory(){
    
        return _supplierProductCategory;
    
    }
    
    public void setSupplierProductCategory(Entities.SupplierProductCategory supplierProductCategory){
    
    _supplierProductCategory=supplierProductCategory;
    
    }
    
    public void removePicture(javax.faces.event.ActionEvent event){
    
        try{
        
          if(this.getRequest().getParameter("hiddenImage")!=null){
          
              this._fileUploadBean.removeImage(this.getRequest().getParameter("hiddenImage"));
          
          }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
 
    public void createCategory(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
            
            System.out.print("CREATING CATEGORY "+this._supplierProductCategory.getName());
        
            if(!this._addCategoryController.checkExistence(this.name)  && this.name!=null && !this.name.equals("")){
            
                this._addCategoryController.createCategory(this.name);
            
                javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
                
                nh.handleNavigation(this.getContext(), null, "success");
                
            }
            else{
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Error");
               
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                
                this.getContext().addMessage(null,msg);
                
                this.getContext().renderResponse();
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
         
        
        }
    
    }
    
}
