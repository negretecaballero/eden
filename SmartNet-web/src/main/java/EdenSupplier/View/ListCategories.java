/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.View;

/**
 *
 * @author luisnegrete
 */
public class ListCategories extends Clases.BaseBacking{
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenSupplier.Controller.ListCategoriesController _listCategoriesController;
    
    private Entities.SupplierProductCategory selection;
    
    public Entities.SupplierProductCategory getSelection(){
    
        return this.selection;
    
    }
    
    public void setSelection(Entities.SupplierProductCategory selection){
    
    this.selection=selection;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
         
         
        
        }
        catch(Exception | StackOverflowError  ex)
        {
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            
        
        }
    }
    
    public org.primefaces.model.LazyDataModel<Entities.SupplierProductCategory>getLazyModel(){
    
        try{
        
            return _listCategoriesController.getSupplierProductCategoryLazyModel();
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    public void redirect(Object supplierProductCategory){
    
    try{
        
        System.out.print("SUPPLIERPRODUCTCATEGORY "+supplierProductCategory);
        
        
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void selectCategory(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
  
            
            if(this.selection!=null)
            {

            System.out.print(this.selection);
                
               
              System.out.print("REDIRECTING");
              
              javax.faces.context.ExternalContext ec=this.getContext().getExternalContext();
              
              ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"Supplier"+java.io.File.separator+"category"+java.io.File.separator+"edit"+java.io.File.separator+"edit.xhtml?faces-redirect=true&ProductCategort="+selection.getSupplierProductCategoryPK().getIdSupplierProductCategory()+"&Supplier="+selection.getSupplierProductCategoryPK().getSupplierIdsupplier()+"&Username="+selection.getSupplierProductCategoryPK().getSupplierLoginAdministradorUsername()+"");
              
              
            }
                     
        }
        
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    public void deleteCategory(javax.faces.event.AjaxBehaviorEvent event){
    
    try{
    
  if(this.selection!=null){
  
      System.out.print("ITEM TO DELETE "+this.selection.getSupplierProductCategoryPK());
      
      this._listCategoriesController.deleteSupplierCategpryProduct(this.selection.getSupplierProductCategoryPK());
  
  }
    
    }
    catch(Exception | StackOverflowError ex)
    {
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    }
    
}
