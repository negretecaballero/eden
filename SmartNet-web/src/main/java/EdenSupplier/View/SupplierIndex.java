/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenSupplier.View;

/**
 *
 * @author luisnegrete
 */
public class SupplierIndex extends Clases.BaseBacking{
    
    private org.primefaces.model.DefaultTreeNode root;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenSupplier.Controller.SupplierIndexController _supplierIndexController;
    
    public org.primefaces.model.DefaultTreeNode getRoot(){
    
    return this.root;
    
    }
    
    public void setRoot(org.primefaces.model.DefaultTreeNode root){
    
        this.root=root;
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
            
            System.out.print("SUPPLIERS "+this._supplierIndexController.getSuppliers().size());
        
            root=new org.primefaces.model.DefaultTreeNode();
            
            java.util.ResourceBundle rb=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
            
            org.primefaces.model.DefaultTreeNode suppliers=new org.primefaces.model.DefaultTreeNode(rb.getString("edenSupplierIndex.TreeNode"),root);
            
            for(Entities.Supplier supplier:this._supplierIndexController.getSuppliers()){
            
                org.primefaces.model.DefaultTreeNode supplierRoot=new org.primefaces.model.DefaultTreeNode(supplier.getName()+","+supplier.getIdSupplierApplication().getNit(),suppliers);
            
 
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    public void supplierSelection(org.primefaces.event.NodeSelectEvent event){
    
    try{
    
  org.primefaces.model.TreeNode node=event.getTreeNode();
  
  System.out.print("DATA "+node.getData().toString());
  
  String data=node.getData().toString();
  
  if(data.split(",").length==2){
  
      String [] array=data.split(",");
      
      String nit=array[1];
      
      System.out.print("NIT "+nit);
      
      if(this._supplierIndexController.getByNit(nit)!=null){
      
          this.getSession().setAttribute("selectedOption",this._supplierIndexController.getByNit(nit));
          
          System.out.print("Auxiliar Component Set");
          
          javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
          
          nh.handleNavigation(this.getContext(), null, "success");
      
      }
  
  }
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
}
