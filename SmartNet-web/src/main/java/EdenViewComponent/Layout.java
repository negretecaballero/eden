/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenViewComponent;

import java.util.Arrays;
import java.util.Collections;
import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.UINamingContainer;
import javax.faces.component.behavior.ClientBehaviorHolder;

import org.primefaces.component.api.Widget;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.ResizeEvent;
import org.primefaces.extensions.event.OpenEvent;
import org.primefaces.util.Constants;
import static org.primefaces.util.Constants.RequestParams.PARTIAL_BEHAVIOR_EVENT_PARAM;

/**
 *
 * @author luisnegrete
 */

@ResourceDependencies({

    @ResourceDependency(library="primefaces",name="jquery/jquery.js"),
        
    @ResourceDependency(library="primefaces",name="primefaces.js"),
    
    @ResourceDependency(library="css",name="EdenLayout.css"),
    
    @ResourceDependency(library="javascript",name="EdenLayout.js"),
    
    @ResourceDependency(library="javascript",name="jquery.layout.js")

})

public class Layout extends javax.faces.component.UIComponentBase implements Widget,ClientBehaviorHolder{

    public static final String COMPONENT_FAMILY = "org.primefaces.cookbook.component";
    
    private static final String DEFAULT_RENDERER = "org.primefaces.cookbook.component.LayoutRenderer";
    
    public static final String POSITION_SEPARATOR = "_";
    
    public static final String STYLE_CLASS_PANE = "ui-widget-content ui-corner-all";
    
    public static final String STYLE_CLASS_PANE_WITH_SUBPANES = "ui-corner-all ui-layout-pane-withsubpanes";
    
    public static final String STYLE_CLASS_PANE_HEADER = "ui-widget-header ui-corner-top ui-layout-pane-header";
    
    public static final String STYLE_CLASS_PANE_CONTENT = "ui-layout-pane-content";
    
    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(Layout.class.getName());
    
    private static final java.util.Collection<String> EVENT_NAMES = Collections.unmodifiableCollection(Arrays.asList("open","close","resize"));
    
    @Override
    public java.util.Collection<String>getEventNames(){
    
        return EVENT_NAMES;
    
    }
    
    private boolean isSelfRequest(javax.faces.context.FacesContext context){
    
        return this.getClientId(context).equals(context.getExternalContext().getRequestParameterMap().get(Constants.RequestParams.PARTIAL_SOURCE_PARAM));
    
    }
    
    @Override
    public void processDecodes(javax.faces.context.FacesContext fc){
    
       if(isSelfRequest(fc)){
       
           this.decode(fc);
           
       }
       else{
           
           super.processDecodes(fc);
       
       }
    
    }
    
    @Override
    public void processValidators(javax.faces.context.FacesContext fc){
    
        if(!isSelfRequest(fc)){
        
            super.processValidators(fc);
        
        }
    
    }
    
    @Override
    public void processUpdates(javax.faces.context.FacesContext fc){
    
        if(isSelfRequest(fc)){
        
            super.processUpdates(fc);
        
        }
        
    
    }
    
    @Override
    public void queueEvent(javax.faces.event.FacesEvent event){
    
        javax.faces.context.FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
        
        java.util.Map<String,String> params = context.getExternalContext().getRequestParameterMap();
        
        String eventName;
        
        eventName = params.get(PARTIAL_BEHAVIOR_EVENT_PARAM);
        
        String clientId = this.getClientId(context);
        
        if(isSelfRequest(context)){
        
            javax.faces.event.AjaxBehaviorEvent behaviorEvent = (javax.faces.event.AjaxBehaviorEvent) event;
            
            LayoutPane pane = getLayoutPane(this,params.get(clientId+"_pane"));
        
            if(pane == null){
            
                LOG.warning("LayoutPane by request parameter '"+params.get(clientId+"_pane"+"' was not found"));
            
                return;
                
            }
            
            if("open".equals(eventName)){
            
                OpenEvent openEvent = new OpenEvent(pane, behaviorEvent.getBehavior());
                
                openEvent.setPhaseId(behaviorEvent.getPhaseId());
                
                super.queueEvent(openEvent);
                
                return;
            
            }
            
            else if("close".equals(eventName)){
            
                CloseEvent closeEvent = new CloseEvent(pane, behaviorEvent.getBehavior());
            
                closeEvent.setPhaseId(behaviorEvent.getPhaseId());
                
                super.queueEvent(closeEvent);
             
                return;
                
            }
            
            else if("resize".equals(eventName)){
            
                double width = Double.valueOf(params.get(clientId+"_width"));
                
                double height = Double.valueOf(params.get(clientId+"_height"));
                
                ResizeEvent resizeEvent = new ResizeEvent(pane,behaviorEvent.getBehavior(),(int)width,(int)height);
            
                event.setPhaseId(behaviorEvent.getPhaseId());
                
                super.queueEvent(resizeEvent);
                
                return;
                
            }
            
        }
    
        super.queueEvent(event);
        
    }
    
    public LayoutPane getLayoutPane(javax.faces.component.UIComponent component, String combinedPosition){
    
        for(javax.faces.component.UIComponent child:component.getChildren()){
        
            if(child instanceof LayoutPane){
            
                if(((LayoutPane)child).getCombinedPosition().equals(combinedPosition)){
                
                    return (LayoutPane)child;
                    
                }
                else{
                
                    LayoutPane pane = getLayoutPane(child,combinedPosition);
                    
                    if(pane != null){
                    
                        return pane;
                    
                    }
                
                }
            
            }
        
        }
    
        return null;
        
    }
    
    
    
    protected enum PropertyKeys{
    
        widgetVar,
        fullPage,
        options,
        style,
        styleClass
    
    }
    
    public Layout(){
    
        setRendererType(DEFAULT_RENDERER);
    
    }
    
    @Override
    public String getFamily() {
  
        return COMPONENT_FAMILY;
    
    }
    
    public String getWidgetVar(){
    
        return (String) getStateHelper().eval(PropertyKeys.widgetVar,null);
    
    }
    
    public void setWidgetVar(String widgetVar){
    
        getStateHelper().put(PropertyKeys.widgetVar, widgetVar);
    
    }
    
    public boolean isFullPage(){
    
        return (Boolean) getStateHelper().eval(PropertyKeys.fullPage,true);
    
    }
    
    public void setFullPage(boolean fullPage){
    
        getStateHelper().put(PropertyKeys.fullPage, fullPage);
    
    }
    
    public Object getOptions(){
   
        return getStateHelper().eval(PropertyKeys.options,null);
    
    }
    
    public void setOptions(Object options){
    
        getStateHelper().put(PropertyKeys.options, options);
    
    }
    
    public String getStyle(){
    
        return (String)getStateHelper().eval(PropertyKeys.style,null);
    
    }
    
    public void setStyle(String style){
    
        getStateHelper().put(PropertyKeys.style, style);
    
    }
    
    public String getStyleClass(){
    
       return (String) getStateHelper().eval(PropertyKeys.styleClass,null);
    
    }
    
    public void setStyleClass(String styleClass){
    
       getStateHelper().put(PropertyKeys.styleClass, styleClass);
    
    }
    
    

   
    public String resolveWidgetVar() {
    
    javax.faces.context.FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
    
    String userWidgetVar = (String)getAttributes().get(PropertyKeys.widgetVar.toString());
    
    if(userWidgetVar != null){
    
        return userWidgetVar;
    
    }
    
    return "widget_"+getClientId(context).replaceAll("-|"+UINamingContainer.getSeparatorChar(context), userWidgetVar);
    
    }
    
    
    
}
