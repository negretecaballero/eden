/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenViewComponent;

import org.primefaces.extensions.renderkit.layout.GsonLayoutOptions;

/**
 *
 * @author luisnegrete
 */
public class LayoutOptions implements java.io.Serializable {
    
    //direct options
    
    private java.util.Map<String,Object> options = new java.util.HashMap<String,Object>();
    
    //options for all panes
    
    private LayoutOptions panes;
    
    // options for every specific pane (depends on position)
    
    private LayoutOptions north;
    
    private LayoutOptions south;
    
    private LayoutOptions west;
    
    private LayoutOptions east;
    
    private LayoutOptions center;
    
    //options for child layout
    
    private LayoutOptions child;
    
    public LayoutOptions(){
    
        
    
    }
    
    public java.util.Map<String,Object>getOptions(){
    
        return options;
    
    }
    
    public void setOptions(java.util.Map<String,Object>options){
    
    this.options = options;
        
    }
    
    public void addOption(String key,Object value){
    
        options.put(key, value);
    
    }
    
    public void setPanesOptions(LayoutOptions layoutOptions){
    
        panes = layoutOptions;
    
    }
    
    public LayoutOptions getPanesOptions(){
    
        return panes;
    
    }
    
    public void setNorthOptions(LayoutOptions layoutOptions){
    
        north = layoutOptions;
    
    }
    
    public LayoutOptions getNorthOptions(){
    
        return this.north;
    
    }
    
    public void setSouthOptions(LayoutOptions layoutOptions){
    
        this.south = layoutOptions;
    
    }
    
    public LayoutOptions getSouthOptions(){
    
        return this.south;
    
    }
    
    public void setWestOptions(LayoutOptions layoutOptions){
    
        this.west = layoutOptions;
    
    }
    
    public LayoutOptions getWestOptions(){
    
        return west;
    
    }
    
    public void setEastOptions(LayoutOptions layoutOptions){
    
        this.east = layoutOptions;
    
    }
    
    public LayoutOptions getEastOptions(){
    
        return this.east;
    
    }
    
    public void setCenterOptions(LayoutOptions layoutOptions){
    
        this.center = layoutOptions;
    
    }
    
    public LayoutOptions getCenterOptions(){
    
        return this.center;
    
    }
    
    public void setChildOptions(LayoutOptions layoutOptions){
    
        child = layoutOptions;
        
    }
    
    public LayoutOptions getChildOptions(){
    
        return child;
    
    }
    
    public String render(){
    
        return GsonLayoutOptions.getGson().toJson(this);
    
    }
    
}
