/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenViewComponent;

import javax.faces.component.UIComponentBase;

/**
 *
 * @author luisnegrete
 */
public class LayoutPane extends UIComponentBase {

    public static final String COMPONENT_FAMILY = "org.primefaces.cookbook.component";
    
    private static final String DEFAULT_RENDERER = "org.primefaces.cookbook.component.LayoutPaneRenderer";
    
    protected enum PropertyKeys{
    
        position,
        combinedPosition
    
    }
    
    public LayoutPane(){
    
        setRendererType(DEFAULT_RENDERER);
    
    }
    
    @Override
    public String getFamily() {
 
        return COMPONENT_FAMILY;
    
    }
    
    // position "north" | "south" | "west" | "east" | "center"
    
    public String getPosition(){
    
        return (String) getStateHelper().eval(PropertyKeys.position,null);
    
    }
    
    public void setPosition(String position){
    
        getStateHelper().put(PropertyKeys.position, position);
    
    }
    
    public String getCombinedPosition(){
    
        return (String)getStateHelper().eval(PropertyKeys.combinedPosition,null);
    
    }
    
    public void setCombinedPosition(String combinedPosition){
    
        getStateHelper().put(PropertyKeys.combinedPosition, combinedPosition);
    
    }
    
    
    
    
    
}
