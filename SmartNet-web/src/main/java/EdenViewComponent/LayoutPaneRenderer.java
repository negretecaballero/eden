/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenViewComponent;

import org.primefaces.renderkit.CoreRenderer;

/**
 *
 * @author luisnegrete
 */
public class LayoutPaneRenderer extends CoreRenderer {
    
    @Override
    public void encodeBegin(javax.faces.context.FacesContext fc, javax.faces.component.UIComponent component) throws java.io.IOException{
    
        javax.faces.context.ResponseWriter writer = fc.getResponseWriter();
        
        LayoutPane layoutPane = (LayoutPane)component;
        
        String position = layoutPane.getPosition();
        
        String combinedPosition = position;
        
        javax.faces.component.UIComponent parent = layoutPane.getParent();
        
        while(parent instanceof LayoutPane){
        
            combinedPosition = ((LayoutPane)parent).getPosition()+Layout.POSITION_SEPARATOR+combinedPosition;
            
            parent = parent.getParent();
        
        }
        
        //save combined position
        
        layoutPane.setCombinedPosition(combinedPosition);
        
        boolean hasSubPanes = false;
        
        for(javax.faces.component.UIComponent subChild : layoutPane.getChildren()){
        
            //check first level
            
            if(hasSubPanes){
            
                break;
            
            }
            
            if(subChild instanceof LayoutPane){
            
                if(!subChild.isRendered()){
                
                    continue;
                
                }
                
                hasSubPanes = true;
            
            }
            
            else{
            
                for(javax.faces.component.UIComponent subSubChild:subChild.getChildren()){
                
                    //check second level
                    
                    if(subSubChild instanceof LayoutPane){
                    
                        if(!subSubChild.isRendered()){
                        
                            continue;
                        
                        }
                        
                        hasSubPanes = true;
                    
                        break;
                    }
                    
                   
                
                }
            
            }
        
        }
        
        javax.faces.component.UIComponent header = layoutPane.getFacet("header");
        
        writer.startElement("div", null);
        
        writer.writeAttribute("id", layoutPane.getClientId(fc), "id");
        
        if(hasSubPanes){
        
            writer.writeAttribute("class", "ui-layout-"+position+" "+Layout.STYLE_CLASS_PANE_WITH_SUBPANES, null);
  
        
        }
        else{
        
            if(header != null){
            
                writer.writeAttribute("class", "ui-layout-"+position+" "+Layout.STYLE_CLASS_PANE, null);
            
            }else{
            
                writer.writeAttribute("class", "ui-layout-"+position+" "+Layout.STYLE_CLASS_PANE+" "+Layout.STYLE_CLASS_PANE_CONTENT, null);
            
            }
        
        }
        
        writer.writeAttribute("data-combinedposition", combinedPosition, null);
        
        //encode header
        
        if(header != null){
        
            writer.startElement("div", null);
            
            writer.writeAttribute("class", Layout.STYLE_CLASS_PANE_HEADER, null);
            
            header.encodeAll(fc);
            
            writer.endElement("div");
        
        }
        
        //encode content
        
        if(header != null){
        
            writer.startElement("div", null);
            
            writer.writeAttribute("class", "ui-layout-content "+Layout.STYLE_CLASS_PANE_CONTENT, null);
        
            writer.writeAttribute("style", "border:none", null);
            
            renderChildren(fc,layoutPane);
            
            writer.endElement("div");
            
        }
        else{
        
        renderChildren(fc,layoutPane);
        
        }
    
    }
    
    @Override
    public void encodeEnd(javax.faces.context.FacesContext fc,javax.faces.component.UIComponent component) throws java.io.IOException{
    
        javax.faces.context.ResponseWriter writer = fc.getResponseWriter();
        
        writer.endElement("div");
    
    }
    
    @Override
    public boolean getRendersChildren(){
    
        return true;
    
    }
    
    @Override
    public void encodeChildren(javax.faces.context.FacesContext fc, javax.faces.component.UIComponent component)throws java.io.IOException{
    
        //nothing to do
    
    }
    
}
