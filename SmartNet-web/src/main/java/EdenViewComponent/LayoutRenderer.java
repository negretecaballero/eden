/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenViewComponent;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import org.primefaces.renderkit.CoreRenderer;
import org.primefaces.util.ComponentUtils;

/**
 *
 * @author luisnegrete
 */
public class LayoutRenderer extends CoreRenderer{
    
    @Override
    public void encodeBegin(javax.faces.context.FacesContext fc,javax.faces.component.UIComponent component) throws java.io.IOException{
    
        ResponseWriter writer = fc.getResponseWriter();
        
        Layout layout = (Layout) component;
        
        encodeScript(fc,layout);
        
        if(!layout.isFullPage()){
        
            writer.startElement("div", layout);
            
            writer.writeAttribute("id", layout.getClientId(fc), "id");
            
            if(layout.getStyle() != null){
            
                writer.writeAttribute("style", layout.getStyle(), "style");
                
            }
            
            if(layout.getStyleClass() != null){
            
                writer.writeAttribute("class", layout.getStyleClass(), "styleClass");
            
            }
        
        }
    
    }
    
    
    @Override
    public void encodeEnd(javax.faces.context.FacesContext fc, javax.faces.component.UIComponent component)throws java.io.IOException{
    
        ResponseWriter writer = fc.getResponseWriter();
        
        Layout layout = (Layout) component;
        
        if(!layout.isFullPage()){
        
            writer.endElement("div");
        
        }
    
    }
    
    @Override
    public boolean getRendersChildren(){
    
        return false;
    
    }

  
    protected void encodeScript(FacesContext fc, Layout layout) throws java.io.IOException {
   
        javax.faces.context.ResponseWriter writer = fc.getResponseWriter();
        
        String clientId = layout.getClientId();
        
        startScript(writer,clientId);
        
        writer.write("$function(){");
        
        writer.write("Primefaces.cw('Layout', '"+layout.resolveWidgetVar()+"',{");
    
        writer.write("id:'"+clientId+"'");
        
        if(layout.isFullPage()){
        
            writer.write(",forTarget:'body'");
        
        }
        else{
        
            writer.write(",forTarget:'"+ComponentUtils.escapeJQueryId(clientId)+"'");
            
        }
        
        LayoutOptions layoutOptions = (LayoutOptions) layout.getOptions();
        
        if(layoutOptions != null){
        
            writer.write(",options:"+layoutOptions.render());
        
        }
        else{
        
            writer.write(",options:{}");
        
        }
        
        encodeClientBehaviors(fc,layout);
        
        writer.write("},true);});");
        
        endScript(writer);
        
    }
    
    @Override
    public void decode(javax.faces.context.FacesContext fc, javax.faces.component.UIComponent component){
    
        decodeBehaviors(fc,component);
    
    }
    
    
    
}
