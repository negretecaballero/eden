/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.Controller;

/**
 *
 * @author luisnegrete
 */
public interface EdenWorkerApplications {
    
    java.util.List<Entities.Application>getApplicationList();
    
    void removeApplication(Entities.ApplicationPK applicationPK);
    
    SessionClasses.EdenList<Entities.SupplierApplication>getSupplierApplications();
    
    void deleteSupplierApplication(String nit);
    
    void initPreRenderView(String viewId);
    
    void tabChanged(String tabName);
    
    ENUM.TransactionStatus createFranchise(String buttonId, String nit);
    
    SessionClasses.EdenList<Entities.Application>getApplications();
    
     Entities.ApplicationPK getApplicationPK();
    
}
