/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.Controller;

import java.text.SimpleDateFormat;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class EdenWorkerApplicationsImplementation implements EdenWorkerApplications {
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.ApplicationController applicationController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierApplicationController _supplierApplicationController;
   
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUpload;
    
    @Override
    public void initPreRenderView(String viewId){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
    
        EdenWorker.View.EdenWorkerApplicationsView edenWorkerApplicationsView=(EdenWorker.View.EdenWorkerApplicationsView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenWorkerApplicationsView");
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
        
        if(edenWorkerApplicationsView!=null){
        
            if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
            
            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
            _fileUpload.setActionType(new Clases.ActionType(viewId));
            
            }
        
        }
        
        
    }
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    
    @Override
    public java.util.List<Entities.Application>getApplicationList(){
    
    try{
    
        java.util.List<Entities.Application>applicationList=this.applicationController.findAll();
        
        for(Entities.Application application:applicationList){
        
         String pattern = "MM/dd/yyyy";
         SimpleDateFormat format = new SimpleDateFormat(pattern);
   
         
         java.util.Date date=application.getFechaExpedicion();
         
         date=format.parse("12/31/2006");
         
         application.setFechaExpedicion(date);
         
        
        }
        
        return this.applicationController.findAll();
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
 @Override
    public void removeApplication(Entities.ApplicationPK applicationPK){
    
    try{
    
        Entities.Application application=this.applicationController.findBYKey(applicationPK);
        
        SessionClasses.MailManagement mailManagement=new SessionClasses.MailManagement();
              
        String mail=application.getMail();
        
        this.applicationController.delete(applicationPK);
        
        System.out.print("Send Mail to "+mail);

          mailManagement.sendMail(mail ,"Application Declined", "Your application has been declined please contact us for further information");
    
    
    }
    catch(javax.ejb.EJBException| NullPointerException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    @Override
     public SessionClasses.EdenList<Entities.SupplierApplication>getSupplierApplications(){
   
   try{
       
       SessionClasses.EdenList<Entities.SupplierApplication>result=new SessionClasses.EdenList<Entities.SupplierApplication>();
       
       if(this._supplierApplicationController.findByState(Entitites.Enum.SupplierApplicationState.pending)!=null && !this._supplierApplicationController.findByState(Entitites.Enum.SupplierApplicationState.pending).isEmpty()){
       
           
           for(Entities.SupplierApplication aux:this._supplierApplicationController.findByState(Entitites.Enum.SupplierApplicationState.pending)){
           
               result.addItem(aux);
           
           }
           
       return result;
       
       }
   
       return null;
   }
   catch(javax.ejb.EJBException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       return null;
   }
   
   }
     
     @Override
     public void deleteSupplierApplication(String nit){
     
     try{
     
         this._supplierApplicationController.delete(nit);
     
     }
     catch(Exception ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
     }
     
     
     }
     
     @Override
     public SessionClasses.EdenList<Entities.Application>getApplications(){
     
     try{
     
       return new SessionClasses.EdenList<Entities.Application>(this.applicationController.findByStatus(Entitites.Enum.ApplicationStatusENUM.PENDING));
         
     }
     catch(NullPointerException ex){
     
        ex.printStackTrace(System.out);
     
        return null;
        
     }
     
     }
     
     
     
     @Override
     public ENUM.TransactionStatus createFranchise(String buttonId, String nit){
     
         try{
         
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
         
             if(buttonId!=null && nit!=null && session!=null){
             
                 if(buttonId.contains("createApplication")){
                     
                     System.out.println("YOU ARE CREATING A FRANCHISE");
                 
                    if(this.applicationController.findByNit(nit)!=null){
                    
                        session.setAttribute("auxiliarComponent", this.applicationController.findByNit(nit));
                    
                        return ENUM.TransactionStatus.APPROVED;
                        
                    } 
                 
                 }
                 else if(buttonId.contains("createSupplierApplication")){
                     
                     System.out.println("YOU ARE CREATING A SUPPLIER");
                 
                     if(this._supplierApplicationController.find(nit)!=null){
                     
                         System.out.print("SUPPLIER FOUND "+this._supplierApplicationController.find(nit).getName());
                         
                     session.setAttribute("auxiliarComponent", this._supplierApplicationController.find(nit));
                    
                      return ENUM.TransactionStatus.APPROVED;
                     
                     }
                 }
             
             
             }
             
             return ENUM.TransactionStatus.DISAPPROVED;
             
         }
         catch(NullPointerException ex){
         
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
            return ENUM.TransactionStatus.DISAPPROVED;
            
         }
     
     }

    @Override
    public void tabChanged(String tabName) {
       }
    
     @Override
    public Entities.ApplicationPK getApplicationPK(){
    
    try{
    
        javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        if(request.getParameter("applicationPK")!=null){
        
            if(Clases.EdenString.split(",", request.getParameter("applicationPK")).length==2){
            
                Entities.ApplicationPK key=new Entities.ApplicationPK();
                
                key.setCedula(new java.lang.Integer(Clases.EdenString.split(",", request.getParameter("applicationPK"))[0]));
                
                key.setNit(Clases.EdenString.split(",", request.getParameter("applicationPK"))[1]);
            
                return key;
                
            }
        
        }
        
        return null;
    
    }
    catch(NullPointerException | NumberFormatException ex){
    
        ex.printStackTrace(System.out);
        
        return null;
    
    }
    
    }
    
}
