/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.Create.Controller;

/**
 *
 * @author luisnegrete
 */
public interface EdenWorkerCreateController {
    
    void initPreRenderView(String viewId);
    
    ENUM.TransactionStatus createFranchiseApplication(String franchiseName);
    

    
}
