/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.Create.Controller;

import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class EdenWorkerCreateControllerImplementation implements EdenWorkerCreateController{
  
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ProfessionControllerInterface _professionController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AppGroupControllerDelegate _appGroupController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ApplicationController _applicationController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ApplicationStatusController _applicationStatusController;
    
    
    @Override
    public void initPreRenderView(String viewId){
    
        try{
        
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            
            if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                
                _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                
                _fileUpload.setActionType(new Clases.ActionType(viewId));
            
            }
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            EdenWorker.Create.View.EdenWorkerView edenWorkerView=(EdenWorker.Create.View.EdenWorkerView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "edenWorkerView");
            
            javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
        
              
            if(viewRoot!=null && edenWorkerView!=null){
            
                System.out.print("VIEW ROOT AND EDEN WORKER VIEW ARE NOT NULL");
                
                if(session.getAttribute("auxiliarComponent")instanceof Entities.SupplierApplication){
                
         if(viewRoot.findComponent("form:franchiseApplicationPanelGrid")instanceof org.primefaces.component.panelgrid.PanelGrid){
        
            System.out.print("COMPONENT ISNTANCE OF PANELGRID");
        
            org.primefaces.component.panelgrid.PanelGrid panelGrid=(org.primefaces.component.panelgrid.PanelGrid)viewRoot.findComponent("form:franchiseApplicationPanelGrid");
            
            panelGrid.setRendered(false);
            
            
        }
         
         if(viewRoot.findComponent("form:supplierApplicationPanel")instanceof org.primefaces.component.panelgrid.PanelGrid){
         
             org.primefaces.component.panelgrid.PanelGrid supplierPanel=(org.primefaces.component.panelgrid.PanelGrid)viewRoot.findComponent("form:supplierApplicationPanel");
             
             supplierPanel.setRendered(true);
             
         }
         
         edenWorkerView.setSupplierApplication((Entities.SupplierApplication)session.getAttribute("auxiliarComponent"));
                
        }
                
                else if(session.getAttribute("auxiliarComponent")instanceof Entities.Application){
                
                    System.out.print("AUXILIAR COMPONENT IS AN APPLICATION");
                     
        if(viewRoot.findComponent("form:franchiseApplicationPanelGrid")instanceof org.primefaces.component.panelgrid.PanelGrid){
        
            System.out.print("COMPONENT ISNTANCE OF PANELGRID");
        
            org.primefaces.component.panelgrid.PanelGrid panelGrid=(org.primefaces.component.panelgrid.PanelGrid)viewRoot.findComponent("form:franchiseApplicationPanelGrid");
            
            panelGrid.setRendered(true);
            
            
        }
        
        if(viewRoot.findComponent("form:supplierApplicationPanel")instanceof org.primefaces.component.panelgrid.PanelGrid){
        
            System.out.print("SUPPLIER APPLICATION PANEL IS NIT NULL");
            
            org.primefaces.component.panelgrid.PanelGrid panelGrid=(org.primefaces.component.panelgrid.PanelGrid)viewRoot.findComponent("form:supplierApplicationPanel");
            
            panelGrid.setRendered(false);
        
        }
        
        edenWorkerView.setApplication((Entities.Application)session.getAttribute("auxiliarComponent"));
                
        edenWorkerView.setFranchiseName(edenWorkerView.getApplication().getFranchiseName());
        
        if(edenWorkerView.getApplication().getImagenIdimagen()!=null){
        
            _fileUpload.loadImagesServletContext(edenWorkerView.getApplication().getImagenIdimagen(), session.getAttribute("username").toString());
        
            edenWorkerView.setImageName(_fileUpload.getImages().get(_fileUpload.getImages().size()-1));
            
        }
        else{
        
            edenWorkerView.setImageName(java.io.File.separator+"images"+java.io.File.separator+"noImage.png");
        
        }
        
        
     }
                
            
            }
            
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    /**
	 * 
	 * @param franchiseName
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
     public ENUM.TransactionStatus createFranchiseApplication(String franchiseName){
    
        try{
        
           final javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Application && ((Entities.Application)session.getAttribute("auxiliarComponent")).getIdSubtype()!=null){
            
                Entities.Application application=(Entities.Application)session.getAttribute("auxiliarComponent");
                
                Entities.Subtype subtype=application.getIdSubtype();
                
                Entities.Franquicia franchise=new Entities.Franquicia();
                
                franchise.setNombre(franchiseName);
                
                franchise.setIdSubtype(subtype);
                
                System.out.print("FRANCHISE USERNAME "+application.getMail());
                
                if(_fileUpload.getPhotoList()!=null && !_fileUpload.getPhotoList().isEmpty()){
                
                    franchise.setImagenIdimagen(_fileUpload.getPhotoList().get(0));
                
                }
                
                if(!_loginAdministradorController.exists(application.getMail())){
                
                    Entities.LoginAdministrador loginAdministrador=new Entities.LoginAdministrador();
                
                    loginAdministrador.setUsername(application.getMail());
                    
                    loginAdministrador.setPassword(Clases.EdenString.generatePassword(30));
                    
                    loginAdministrador.setProfessionIdprofession(_professionController.find(1));
                    
                    Entities.Confirmation confirmation=new Entities.Confirmation();
                    
                    confirmation.setConfirmed(false);
                    
                    confirmation.setLoginAdministrador(loginAdministrador);
                    
                    confirmation.setLoginAdministradorusername(loginAdministrador.getUsername());
                    
                    loginAdministrador.setConfirmation(confirmation);
                    
                     Entities.AppGroup appGroup=new Entities.AppGroup();
           
                     Entities.AppGroupPK appGroupPK= new Entities.AppGroupPK();
           
                     appGroupPK.setGroupid("guanabara_franquicia_administrador");
           
                     appGroupPK.setLoginAdministradorusername(loginAdministrador.getUsername());
           
        
           
                     appGroup.setAppGroupPK(appGroupPK);
           
                     appGroup.setLoginAdministrador(loginAdministrador);
                    
                    loginAdministrador.setAppGroupCollection(new java.util.ArrayList<Entities.AppGroup>());
                    
                    loginAdministrador.getAppGroupCollection().add(appGroup);
                    
                    _loginAdministradorController.createAccount(loginAdministrador, appGroup);
                    
                     franchise.setLoginAdministradorusername(loginAdministrador);
                     
                }
                
                else{
                
                      Entities.AppGroup appGroup=new Entities.AppGroup();
           
                     Entities.AppGroupPK appGroupPK= new Entities.AppGroupPK();
           
                     appGroupPK.setGroupid("guanabara_franquicia_administrador");
           
                     appGroupPK.setLoginAdministradorusername(_loginAdministradorController.find(application.getMail()).getUsername());
           
                     appGroup.setLoginAdministrador(_loginAdministradorController.find(application.getMail()));
           
                     appGroup.setAppGroupPK(appGroupPK);
  
                 
                     if(this._appGroupController.find(appGroupPK)!=null)
                     
                     {
                         
                     System.out.print("This AppGroup Exists already");
                     
                     }
                     
                     else{
                        _appGroupController.create(appGroup);
                               
                     }
        
                     
                }
                

                franchise.setLoginAdministradorusername(_loginAdministradorController.find(application.getMail()));
        
                franchise.setIdApplication(application);
                
                 _franchiseController.create(franchise);
                
                 application.setIdApplicationStatus(_applicationStatusController.findByStatus(Entitites.Enum.ApplicationStatusENUM.ACCEPTED));
                 
                 this._applicationController.update(application);
                 
                return ENUM.TransactionStatus.APPROVED;
            
            }
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
        catch(NullPointerException ex){
        
        return ENUM.TransactionStatus.DISAPPROVED;
        
        }
    
    }
    
   
    
    
}
