/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.Create.View;

import Clases.BaseBacking;
import Validators.MailValidator;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import org.primefaces.context.RequestContext;


/**
 *
 * @author luisnegrete
 */

@RolesAllowed("EdenWorker")
public class EdenWorkerView extends BaseBacking{

    /**
     * Creates a new instance of EdenWorkerView
     */
    public EdenWorkerView() {
    }
    
    private Entities.Application application;
    
 
    
    @Inject private CDIEden.EdenWorkerControllerInterface edenWorkerController;
    
    @Inject private CDIBeans.FranchiseControllerInterface franchiseController;
    
    @Inject private CDIBeans.FranchiseTypeControllerInterface tipoController;
    
    @Inject @Default private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.LoginAdministradorControllerInterface _loginAdministratorController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.AppGroupControllerDelegate _appGroupController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SupplierController _supplierController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private Eden.controller.EdenWorkerApplicationConfirmationController _edenWorkerApplicationConfirmationController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private EdenWorker.Create.Controller.EdenWorkerCreateController _edenWorkerCreateController;
    
    private Entities.SupplierApplication _supplierApplication;
    
    @javax.validation.constraints.NotNull
    private Entities.SubtypePK subtypePK;
    
    public Entities.SupplierApplication getSupplierApplication(){
    
    return _supplierApplication;
    
    }
    
    public void setSupplierApplication(Entities.SupplierApplication supplierApplication){
    
        _supplierApplication=supplierApplication;
    
    }
 
    
   public java.util.List<Entities.Tipo>getTypeList(){
   
       try{
       
       return this.edenWorkerController.getTypeList();
       
       }
       catch(NullPointerException ex){
       
           return null;
       
       }
       
   } 
   
   public java.util.List<Entities.Subtype>getSubtypeList(){
   
       try{
       
       return this.edenWorkerController.getSubtypeList();
       
       }
       catch(NullPointerException ex){
       
           return null;
       
       }
   
   }
    
   public Entities.Application getApplication(){
   
   return this.application;
   
   }
   
   public void setApplication(Entities.Application application){
   
   this.application=application;
   
   }
    
    private String imageName="";
    
  
    
    
    public void setImageName(String imageName){
    
    this.imageName=imageName;
    
    }
    
    public String getImageName(){
    
    return this.imageName;
    
    }
    
    @NotNull
    @MailValidator
    private String mail="";
    
    public String getMail(){
    
        return this.mail;
    
    }
    
    
    public void setMail(String mail)
    {
    
        this.mail=mail;
    
    } 
    
    @PostConstruct
    public void init(){
        
      try
      {
        this.edenWorkerController.initLists();
        
       
    
        if(!this.fileUpload.getClassType().equals(EdenWorkerView.class.getName())){
        
         
        this.fileUpload.setClassType(this.getClass().getName());
        
        
        
        
        
        
    
    }
      }
      catch(NullPointerException ex){
      
          ex.printStackTrace(System.out);
      
      }
    }
    
    public Entities.SubtypePK getSubtypePK(){
    
        return this.subtypePK;
    
    }
    
   
    
    public void setSubtypePK(Entities.SubtypePK subtypePK){
    
    this.subtypePK=subtypePK;
    
    }
    
    private int idTipo;
    
    public int getIdTipo(){
    
    return this.idTipo;
    
    }
    
    public void setIdTipo(int idTipo){
    
    
    this.idTipo=idTipo;
    
    }
    
    private String franchiseName="";
    
    public String getFranchiseName(){
    
    return this.franchiseName;
    
    }
    
    public void setFranchiseName(String franchiseName){
    
    this.franchiseName=franchiseName;
    
    }
    
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
    
        
        try{
 if(event.getComponent().findComponent("form:mail") instanceof javax.faces.component.UIInput){
 
 System.out.print("Post Validation Event");
  
 javax.faces.component.UIInput franchisename=(javax.faces.component.UIInput)event.getComponent().findComponent("form:franchise");
 
 System.out.print("Franchise Name "+franchisename.getLocalValue().toString());
 
if(edenWorkerController.checkExistence(franchisename.getLocalValue().toString())){

FacesMessage msg=new FacesMessage("There is already a Franchise with this Name");

msg.setSeverity(FacesMessage.SEVERITY_ERROR);

getContext().addMessage(event.getComponent().getClientId(), msg);

getContext().renderResponse();

}

System.out.print("Proceed Lifecycle");
 
 }
 
    }
        catch(NullPointerException ex){
        
            if(getContext()!=null){
            
            FacesMessage msg=new FacesMessage("Error");

msg.setSeverity(FacesMessage.SEVERITY_ERROR);

getContext().addMessage(event.getComponent().getClientId(), msg);

getContext().renderResponse();
            }
        }
    
    }
    

    public String addSucursalAdministrator(){
    
        try{
            
            switch(this._edenWorkerCreateController.createFranchiseApplication(franchiseName))
            {
            
                case APPROVED:{
                
                    System.out.print("FRANCHISE CREATED");
                
                    return "success";
                    
                }
                
                case DISAPPROVED:{
                
                    System.out.print("DISAPPROVED");
                    
                    return "failure";
                
                }
                
                default:{
                
                    return "failure";
                
                }
            
            }
    /*
          if(this.getSessionAuxiliarComponent()instanceof Entities.Application && ((Entities.Application)this.getSessionAuxiliarComponent()).getIdSubtype()!=null){
            
              Entities.Application application=(Entities.Application)this.getSessionAuxiliarComponent();
              
              Entities.Subtype subtype=((Entities.Application)this.getSessionAuxiliarComponent()).getIdSubtype();
     
            
            Entities.Franquicia franchise=new Entities.Franquicia();
            
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
            
                System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
           System.out.print("Name of franchise "+this.franchiseName);
            
            franchise.setNombre(this.franchiseName);
            
        //franchise.setNombre(this.getContext().getExternalContext().getRequestParameterMap().get("form:franchise")); 
    
         
             
            if(this.getSessionAuxiliarComponent() instanceof Entities.Application){
            
                System.out.print("Session Auxiliar is not null");
                
                subtype=((Entities.Application)this.getSessionAuxiliarComponent()).getIdSubtype();
            
            }
                        
            
            
            if(subtype!=null)
            
            {
                
            System.out.print("Subtype name "+subtype.getName());
            
            franchise.setIdSubtype(subtype);
            
            if(this.application==null){
            
            if(this.getSessionAuxiliarComponent() instanceof Entities.Application){
                
            this.application=(Entities.Application)this.getSessionAuxiliarComponent();
                
            }
            else{
                
             throw new IllegalArgumentException("Object of type "+this.getSessionAuxiliarComponent().getClass().getCanonicalName()+" must be of type "+Entities.Application.class.getName());
                
            }
            
            }
            
           
            System.out.print("Mail to create "+this.application.getMail());
            
            SessionClasses.MailManagement mailManagement=new SessionClasses.MailManagement();
            
            mailManagement.sendMail(application.getMail(),"Congratulations!", "Your franchise has been created");
            
            this.edenWorkerController.createSucursalAdministrador(franchise, this.application.getMail());
            
            FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"Franchise Added Successfully");
           
            getContext().addMessage(event.getComponent().getClientId(),msg);
            
        }
        
            else{
            
            System.out.print("SUBTYPE IS NULL "+this.subtypePK);
            
            }
        
            
          
              
          }
          else{
          
              throw new IllegalArgumentException("Object must be f type "+Entities.Subtype.class.getName());
          
          }
          */
          
        }
        catch(IllegalArgumentException | NullPointerException  | StackOverflowError ex){
        
            Logger.getLogger(EdenWorkerView.class.getName()).log(Level.SEVERE,null,ex);
            
            ex.printStackTrace(System.out);
            
         return "failure";
        
        }
     
    
    
    }
    
    public void preRender(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
        _edenWorkerCreateController.initPreRenderView(this.getCurrentView());
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
            
            this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString());
            
            this.fileUpload.servletContainer(event.getFile(),getSession().getAttribute("username").toString(), 500, 500);
        
         
            
            this.imageName=this.fileUpload.getImages().get(0);
            
            System.out.print("Image name "+this.imageName);
            
            RequestContext.getCurrentInstance().update("form:image");
         
        }
        catch(NullPointerException ex){
        
         
        
        }
    
    }
    
    public java.util.List<Entities.Franquicia>getFranchises(){
    
    try{
    
        try{
        
        return this.franchiseController.findAll();
        
        }
        catch(NullPointerException ex){
        
              ex.printStackTrace(System.out);
            
            return null;
            
          
        
        }
    
    }
    catch(Exception ex){
    
    Logger.getLogger(EdenWorkerView.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new FacesException(ex);
    
    }
    
    }
    
    public java.util.List<Entities.Tipo>getTipo(){
    
        try{
        
          return  this.tipoController.findAll();
            
        }catch(EJBException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return null;
        }
    
    }
    
    public void updateSubtype(javax.faces.event.AjaxBehaviorEvent event){
    
    try{
    
        java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
        
        for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
        
        System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
    
        String idtype=requestMap.get("form:tipeSelector_input");
        
        if(idtype!=null && !idtype.equals(""))
        {
        
            this.edenWorkerController.updateSubtypeList(Integer.parseInt(idtype));
        
                       
        }
        else{
        
        throw new javax.faces.FacesException("Exception caught capturing idtype value");
        
        }
    }
    catch(NumberFormatException  | NullPointerException ex)
    {
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    
        
    }
    }
    
    public void createSupplier(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
            
        for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
        
        System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
        
        javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
            
        _supplierApplication=this._edenWorkerApplicationConfirmationController.getSupplierApplicationObject();
        
        if(servletRequest.getParameter("form:supplierName")!=null){
        
            _supplierApplication.setName(servletRequest.getParameter("form:supplierName"));
        
        }
        
           this._edenWorkerApplicationConfirmationController.createSupplier(_supplierApplication);
            
           javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
           
           nh.handleNavigation(this.getContext(), null, "success");
           
            System.out.print("SUPPLIER CREATED");
        
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
           
            
        }
    
    }
    
}
