package EdenWorker.DeleteAccount.Controller;

public interface DeleteAccountController {

	/**
	 * 
	 * @param viewid
	 */
	void initPreRenderView(String viewid);

	/**
	 * 
	 * @param mail
	 */
	void deleteAccount(String mail);

}