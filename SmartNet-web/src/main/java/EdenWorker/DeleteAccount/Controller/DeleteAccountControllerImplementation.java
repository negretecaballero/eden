package EdenWorker.DeleteAccount.Controller;

import CDIBeans.*;

@javax.enterprise.inject.Alternative
public class DeleteAccountControllerImplementation implements DeleteAccountController {

	/**
	 * 
	 * @param viewid
	 */
	@Override
	public void initPreRenderView(String viewid) {
		
            try{
            
                if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewid)){
                
                   final javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                   final javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                   
                   _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                   
                   _fileUpload.setActionType(new Clases.ActionType(viewid));
                   
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param mail
	 */
	@Override
	public void deleteAccount(String mail) {
		
            try{
                
                this._loginAdministradorController.delete(mail);
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private  FileUploadInterface _fileUpload;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private  LoginAdministradorControllerInterface _loginAdministradorController;

}