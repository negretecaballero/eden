package EdenWorker.DeleteAccount.View;

import EdenWorker.DeleteAccount.Controller.*;

public class DeleteAccount extends Clases.BaseBacking{

	@Validators.MailValidator
	@javax.validation.constraints.NotNull
	private String mail;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private DeleteAccountController _deleteAccountController;

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * 
	 * @param event
	 */
	public void deleteAccount(javax.faces.event.ActionEvent event) {
	
            try{
            
                System.out.print("DELETING ACCOUNT");
                
                this._deleteAccountController.deleteAccount(this.mail);
                
                java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(),"bundle");
                
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("edenWorkerDeleteAccount.Message"));
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
                
                super.getContext().addMessage(null,message);
            
            }
            catch(NullPointerException ex)
            {
            
                ex.printStackTrace(System.out);
                
            }
            
	}

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
		
            try{
            
                this._deleteAccountController.initPreRenderView(super.getCurrentView());
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}
    
    
    
}