/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EdenWorker.View;

import javax.faces.context.FacesContext;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */
public class EdenWorkerApplicationsView extends Clases.BaseBacking{

    /**
     * Creates a new instance of EdenWorkerApplicationsView
     */
    public EdenWorkerApplicationsView() {
    }
    
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private transient EdenWorker.Controller.EdenWorkerApplications _edenWorkerApplicationController;
    

    
    private Entities.ApplicationPK applicationPK;
    
    
    private java.util.List<Entities.SupplierApplication>suppliers;
    
    public java.util.List<Entities.SupplierApplication>getSuppliers(){
    
        return this.suppliers;
    
    }
    
    public void setSuppliers(java.util.List<Entities.SupplierApplication>suppliers){
    
        this.suppliers=suppliers;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
  
    }
    
    public void preRenderView(){
    
        try{
        
            this._edenWorkerApplicationController.initPreRenderView(this.getCurrentView());
            
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    
    public java.util.List<Entities.SupplierApplication>getSupplierApplications(){
    
        try{
        
            if(this._edenWorkerApplicationController.getSupplierApplications()!=null){
     
                return IteratorUtils.toList(this._edenWorkerApplicationController.getSupplierApplications().iterator());
            
            }
            
            return null;
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    }
    
    
    public java.util.List<Entities.Application>getApplications(){
    
        try{
        
            
                   
          return IteratorUtils.toList(this._edenWorkerApplicationController.getApplications().iterator());
            
          
          
        }
        catch(NullPointerException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
      return null;
        
        }
     
    
    }
    
    public void createFranchise(String clientId){
    
        try{
        
            /*
            javax.faces.component.UIViewRoot rootView=this.getContext().getViewRoot();
            
            System.out.print("CLIENT ID "+clientId);
            
            if(clientId.contains("createApplication")){
            
                
            javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
            
            Entities.Application application=this.applicationController.findByNit(request.getParameter("hiddenNit"));
                       
            this.setSessionAuxiliarComponent(application);
            
            System.out.print("You are in Action Listener");
         
            }
            else if(clientId.contains("createSupplierApplication")){
            
             System.out.print("NIT VALUE "+nit);
             
             this.setSessionAuxiliarComponent(_supplierApplicationController.find(nit));
            
            }
        
         */
           
            
            System.out.print("CLIENT Id "+clientId+" NIT "+ this._edenWorkerApplicationController.getApplicationPK().getNit());
            
            switch(this._edenWorkerApplicationController.createFranchise(clientId, this._edenWorkerApplicationController.getApplicationPK().getNit()))
            {
            
                case APPROVED:{
                
                    System.out.print("APPROVED");
                    
                     javax.faces.application.NavigationHandler navigationHandler=FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
   
                     navigationHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/EdenWorker/welcome-page.xhtml?faces-redirect=true");

                
                }
                case DISAPPROVED:{
                    
                    System.out.print("DISSAPRPOVED");
                
                java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(),"bundle");

                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseApplication.ErrorCreateFranchise"));
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                this.getContext().addMessage(null, message);
                
                }
                
                
            
            }
                          
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

            
        }
    
    }
    
    
   public void removeApplcation(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
       if(this._edenWorkerApplicationController.getApplicationPK()!=null){
          
           System.out.print("APPLICATION TO REMOVE "+this._edenWorkerApplicationController.getApplicationPK());
           
           
         _edenWorkerApplicationController.removeApplication(this._edenWorkerApplicationController.getApplicationPK());
     
     
       }
     
       
   }
   catch(IllegalArgumentException | StackOverflowError | NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
   
   
   }
   
   } 
   
   
   
  public void removeSupplierApplication(String nit){
  
  try{
  
      this._edenWorkerApplicationController.deleteSupplierApplication(nit);
  
  }
  catch(NullPointerException | StackOverflowError ex){
  
      java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
      
  }
  
  }
   
  
  public void setApplicationPK(javax.faces.event.AjaxBehaviorEvent event){

      try{
      
      this.applicationPK=this._edenWorkerApplicationController.getApplicationPK();
  
      
     System.out.print("ASSIGNING APPLICATIONPK VALUE "+this.applicationPK);
      }
      catch(NullPointerException ex){
      
          ex.printStackTrace(System.out);
      
      }
  
  }
  
}
