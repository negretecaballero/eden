/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ErrorReport.Controller;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.context.Dependent
public class ReportControllerImplementation implements ReportController{
   
    @javax.inject.Inject 
    @CDIBeans.BugControllerQualifier
    private CDIBeans.BugController _bugController;
    
    @Override
    public void createReport(String comments){
    
    try{
    
        _bugController.create(comments);
    
    }
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
}
