/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ErrorReport.View;

/**
 *
 * @author luisnegrete
 */
public class Report extends Clases.BaseBacking{
  
    @javax.inject.Inject 
    private ErrorReport.Controller.ReportController _bugController;
    
    private String comments;
    
    public String getComments(){
    
        return this.comments;
    
    }
    
    public void setComments(String comments){
    
        this.comments=comments;
    
    }
    
    public void createReport(javax.faces.event.ActionEvent event){
    
        try{

            _bugController.createReport(this.comments);
            
            System.out.print("REPORT SENT");
            
            java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(this.getContext(), "bundle");
        
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("eden.ReportMessage"));
        
            this.getContext().addMessage(null, msg);            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
