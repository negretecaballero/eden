/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import java.util.Iterator;
import javax.faces.FacesException;
import javax.faces.application.NavigationHandler;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

/**
 *
 * @author luisnegrete
 */
public class CustomExceptionHandler extends ExceptionHandlerWrapper{

    private ExceptionHandler wrapped;
    
    
    public CustomExceptionHandler(ExceptionHandler wrapped){
    
        this.wrapped=wrapped;
    
    }
    
    @Override
    public ExceptionHandler getWrapped() {
  
    return wrapped;
    
    }
   
    @Override
    public void handle()throws FacesException{
    
        Iterator i=getUnhandledExceptionQueuedEvents().iterator();
        
        while(i.hasNext()){
        
        ExceptionQueuedEvent event=(ExceptionQueuedEvent)i.next();
        
        ExceptionQueuedEventContext context=(ExceptionQueuedEventContext)event.getSource();
        
        Throwable t=context.getException();
        
        FacesContext fc=FacesContext.getCurrentInstance();
        
        try{
            
            /* here you can use the throwable object in order to verify the exceptions you want to handle in the application*/
        
            NavigationHandler navigationHandler=fc.getApplication().getNavigationHandler();

            System.out.print(context.getException().getMessage());
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(context.getException().getMessage());
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            FacesContext.getCurrentInstance().addMessage(null,msg);
            
            navigationHandler.handleNavigation(fc, null, "/bug/report/error.xhtml?faces-redirect=true");
            
            fc.renderResponse();
            
        }
        
        finally{
        
            i.remove();
            
            getWrapped().handle();
        
        }
        
        }
    
    }
    
    
}
