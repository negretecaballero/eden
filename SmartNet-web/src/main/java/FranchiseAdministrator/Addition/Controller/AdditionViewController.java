/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Addition.Controller;

/**
 *
 * @author luisnegrete
 */
public interface AdditionViewController {
    
    void initPreRenderView(String viewId);
    
    void handleUpload(org.primefaces.event.FileUploadEvent event);
    
    void addToInventoryList();
    
    void removeInventoryList();
    
    boolean additionPostValidate(String name);
    
    void createAddition(String name, double price, String description, java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventoryList);
    
    void deleteAdditions(java.util.List<Entities.Addition>deleteList);
    
    ENUM.TransactionStatus updateAddition(String name,double price, String description, java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventarioList);
    
    boolean postValidateUpdateAddition(String name);
    
}
