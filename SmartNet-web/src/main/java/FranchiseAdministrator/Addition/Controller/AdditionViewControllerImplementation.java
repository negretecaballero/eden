/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Addition.Controller;

import Entities.AdditionConsumesInventario;
import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class AdditionViewControllerImplementation implements AdditionViewController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.InventarioDelegate _inventoryController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AdditionConsumesInventarioDelegate _additionConsumesInventarioController;
   
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AdditionControllerDelegate _additionController;
    
    
    private final void loadAdditionImages(Entities.Addition addition){
    
        try{
        
            if(addition.getImagenCollection()!=null && !addition.getImagenCollection().isEmpty()){
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
                for(Entities.Imagen image:addition.getImagenCollection()){
                
                _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString());
                
                }
            
            }
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    /**
	 * 
	 * @param viewId
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void initPreRenderView(String viewId){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            javax.el.ELResolver resolver=(javax.el.ELResolver)javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Addition.View.AdditionView additionView=(FranchiseAdministrator.Addition.View.AdditionView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "additionCRUDView");
            
            switch(viewId){
            
                case"/adminFranquicia/Addaddition.xhtml":{
                
                    if(_fileUpload.getActionType()!=null && !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                    if(additionView!=null){
                    
                        additionView.setAdditionConsumesInventario(new java.util.ArrayList<Entities.AdditionConsumesInventario>());
                    
                    }
                    
                    }
                    
                    break;
                
                }
                
                case "/adminFranquicia/listaddition.xhtml":{
                

                    
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                        
                        System.out.print("INITIALIZING LIST ADDITIONS");
                    
                      if(additionView!=null)
                    {
                      if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
                          
                             Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                             
                             if(franchise.getAdditionCollection()!=null && !franchise.getAdditionCollection().isEmpty()){
                             
                                 additionView.setAdditionList(this._additionController.findByFranchise(franchise.getIdfranquicia()));
                                 
                                 for(Entities.Addition addition:this._additionController.findByFranchise(franchise.getIdfranquicia())){
                                 
                                     System.out.print(addition.getName());
                                 
                                 }
                             
                             }
                          
                          }
                      
                }
                    
                      _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                      
                      _fileUpload.setActionType(new Clases.ActionType(viewId));
                      
                    }
                
                }
                
                case "/adminFranquicia/editaddition.xhtml":{
                
                     if(session.getAttribute("auxiliarComponent")instanceof Entities.Addition && additionView!=null){
                        

                    
                    if(_fileUpload.getActionType() ==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                        
                        System.out.print("SETTING ADDITION INFORMATION");
                        
                        _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                    //  FranchiseAdministrator.Addition.View.AdditionView additionView=(FranchiseAdministrator.Addition.View.AdditionView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"additionCRUDView");
                        
                        Entities.Addition addition=(Entities.Addition)session.getAttribute("auxiliarComponent");
                        
                       additionView.setName(addition.getName());
                    
                       additionView.setValue(addition.getValue());
                       
                       this.loadAdditionImages(addition);
                       
                       additionView.setAdditionConsumesInventario((java.util.Vector<Entities.AdditionConsumesInventario>)this._additionConsumesInventarioController.findByAddition(addition.getAdditionPK()));
                       
                       _fileUpload.setActionType(new Clases.ActionType(viewId));
                       
                    }
                    
                     }
                    
                    break;
                
                }
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    @Override
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("username")!=null){
            
            _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString(), 500, 500);
            
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    
    }
    
    @Override
    public void addToInventoryList(){
    
    try{
    
     javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
     
     FranchiseAdministrator.Addition.View.AdditionView additionView=(FranchiseAdministrator.Addition.View.AdditionView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "additionCRUDView");
    
     if(additionView!=null && _inventoryController.getAutoInventory()!=null && !_inventoryController.getAutoInventory().isEmpty()){
     
         if(additionView.getAdditionConsumesInventario()==null){
         
               additionView.setAdditionConsumesInventario(new java.util.ArrayList<Entities.AdditionConsumesInventario>());
         
         }
         
         if(additionView.getAdditionConsumesInventario().size()<_inventoryController.getAutoInventory().size()){
         
        Entities.AdditionConsumesInventario aux=new AdditionConsumesInventario();
            
        aux.setAdditionConsumesInventarioPK(new Entities.AdditionConsumesInventarioPK());
        
        aux.setInventario(new Entities.Inventario());
        
        aux.getInventario().setInventarioPK(_inventoryController.getAutoInventory().get(0).getInventarioPK());
        
        additionView.getAdditionConsumesInventario().add(aux);
        
         }
     
     }
     
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void removeInventoryList(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Addition.View.AdditionView additionView=(FranchiseAdministrator.Addition.View.AdditionView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "additionCRUDView");
        
            if(additionView!=null){
            
                if(additionView.getAdditionConsumesInventario()!=null && !additionView.getAdditionConsumesInventario().isEmpty()){
                
                    additionView.getAdditionConsumesInventario().remove(additionView.getAdditionConsumesInventario().size()-1);
                
                }
            
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    @Override
    public boolean additionPostValidate(String name){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
           
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            System.out.print("NAME TO FIND "+name);
            
            for(Entities.Addition addition:franchise.getAdditionCollection()){
            
                System.out.print("ADDITION NAME "+addition.getName());
                
                if(addition.getName().toLowerCase().equals(name.toLowerCase())){
            
                    return true;
            
            }
            
            }
            
            }
            
            
            
            return false;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public void createAddition(String name, double price, String description, java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventoryList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                if(this.validateAdditionConsumesInventoryList(additionConsumesInventoryList))
            {
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");    
                
            Entities.Addition addition=new Entities.Addition();
            
            addition.setAdditionPK(new Entities.AdditionPK());
            
            addition.getAdditionPK().setFranquiciaIdfranquicia(franchise.getIdfranquicia());
            
            addition.setName(name);
            
            addition.setDescription(description);
            
            addition.setValue(price);
            
            addition.setImagenCollection(_fileUpload.getPhotoList());
            
            for(Entities.AdditionConsumesInventario additionConsumesInventario:additionConsumesInventoryList){
            
                additionConsumesInventario.setAddition(addition);
                
                additionConsumesInventario.setInventario(_inventoryController.find(additionConsumesInventario.getInventario().getInventarioPK()));
                
                additionConsumesInventario.setAdditionConsumesInventarioPK(new Entities.AdditionConsumesInventarioPK());
                
                additionConsumesInventario.getAdditionConsumesInventarioPK().setAdditionFranquiciaIdFranquicia(franchise.getIdfranquicia());
                
                additionConsumesInventario.getAdditionConsumesInventarioPK().setInventarioFranquiciaIdFranquicia(franchise.getIdfranquicia());
                
                additionConsumesInventario.getAdditionConsumesInventarioPK().setInventarioIdinventario(_inventoryController.find(additionConsumesInventario.getInventario().getInventarioPK()).getInventarioPK().getIdinventario());
            
                additionConsumesInventario.getAdditionConsumesInventarioPK().setAdditionIdaddition(addition.getAdditionPK().getIdaddition());
                
            }
            
            addition.setAdditionConsumesInventarioCollection(additionConsumesInventoryList);
            
            _additionController.create(addition);
            
            System.out.print("ADDITION CREATED IN BEAN");
            
            }
                
                else{
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddAddition.DuplicatedInventory"));
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, message);
                
                }
            
            }
            
         
            
            
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void deleteAdditions(java.util.List<Entities.Addition>deleteList){
    
        try{
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Addition.View.AdditionView additionView=(FranchiseAdministrator.Addition.View.AdditionView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "additionCRUDView");
        
            if(deleteList!=null && !deleteList.isEmpty() && additionView!=null){
            
                System.out.print("DELETE LIST IS NOT NULL");
                
               for(Entities.Addition addition:deleteList){
               
                   this._additionController.delete(addition.getAdditionPK());
               
               }
            
               _fileUpload.setActionType(null);
               
            }
            
            else{
            
                System.out.print("DELETE LIST IS NULL");
            
            }
            
        }
        catch(Exception ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    private java.util.List<Entities.AdditionConsumesInventario>getAdditionConsumesInventarioList(Entities.Addition addition, java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventoryList){
    
        try{
        
           if(additionConsumesInventoryList!=null && !additionConsumesInventoryList.isEmpty()){
           
               java.util.List<Entities.AdditionConsumesInventario>results=new java.util.ArrayList<Entities.AdditionConsumesInventario>();
               
           for(Entities.AdditionConsumesInventario additionConsumesInventory:additionConsumesInventoryList){
           
               additionConsumesInventory.setAddition(addition);
               
               additionConsumesInventory.getAdditionConsumesInventarioPK().setAdditionFranquiciaIdFranquicia(addition.getFranquicia().getIdfranquicia());
               
               additionConsumesInventory.getAdditionConsumesInventarioPK().setAdditionIdaddition(addition.getAdditionPK().getIdaddition());
               
               additionConsumesInventory.getAdditionConsumesInventarioPK().setInventarioIdinventario(additionConsumesInventory.getInventario().getInventarioPK().getIdinventario());
              
               additionConsumesInventory.getAdditionConsumesInventarioPK().setInventarioFranquiciaIdFranquicia(additionConsumesInventory.getInventario().getInventarioPK().getFranquiciaIdFranquicia());
               
               additionConsumesInventory.setInventario(this._inventoryController.find(additionConsumesInventory.getInventario().getInventarioPK()));
               
               results.add(additionConsumesInventory);
           
           }
           
           return results;
           
           } 
            
        return null;
        }
        catch(Exception ex){
        
        return null;
        }
    
    }
    
    @Override
    public ENUM.TransactionStatus updateAddition(String name,double price, String description, java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventarioList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Addition){
            
                if(this.validateAdditionConsumesInventoryList(additionConsumesInventarioList))
                
                {
                
                Entities.Addition addition=(Entities.Addition)session.getAttribute("auxiliarComponent");
                
                addition.setName(name);
                
                addition.setValue(price);
                
                addition.setDescription(description);
                
                addition.setImagenCollection(_fileUpload.getPhotoList());
                
                   System.out.print("ADDITION CONSUMES INVENTARIO SIZE BEFORE MODIFICATIONS "+additionConsumesInventarioList.size());
                
                addition.setAdditionConsumesInventarioCollection(this.getAdditionConsumesInventarioList(addition, additionConsumesInventarioList));

                
                System.out.print("AdditionConsumesInventario LIST SIZE "+addition.getAdditionConsumesInventarioCollection().size());
                
                this._additionController.edit(addition);
                
                System.out.print("Addition Updated Successfully");
                
                _fileUpload.setActionType(null);
                
                return ENUM.TransactionStatus.APPROVED;
                
                }
                
                 else{
                
                      _fileUpload.setActionType(null);
                    
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddAddition.DuplicatedInventory"));
                
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, message);
                
                return ENUM.TransactionStatus.DISAPPROVED;
                
                }
                
            }
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
    
    }
    
    private boolean validateAdditionConsumesInventoryList(java.util.List<Entities.AdditionConsumesInventario>additionConsumesInventoryList){
    
        try{
        
        if(additionConsumesInventoryList!=null && !additionConsumesInventoryList.isEmpty()){
        
       for(Entities.AdditionConsumesInventario additionConsumesInventario:additionConsumesInventoryList){
       
           java.util.List<Entities.AdditionConsumesInventario>auxList=new java.util.ArrayList<Entities.AdditionConsumesInventario>();
           
          
           
           for(Entities.AdditionConsumesInventario additionConsumesInventario1:additionConsumesInventoryList){
           
              Entities.AdditionConsumesInventario auxi=new Entities.AdditionConsumesInventario();
              
              auxi=additionConsumesInventario1;
              
              auxList.add(auxi);
           
           }
           
           auxList.remove(additionConsumesInventario);
           
           for(Entities.AdditionConsumesInventario auxi:auxList){
           
               if(auxi.getInventario().getInventarioPK().equals(additionConsumesInventario.getInventario().getInventarioPK())){
               
                   return false;
               
               }
           
           }
           
       }
        
        }    
            
        return true;
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return true;
        }
    
    }
    
    @Override
    public boolean postValidateUpdateAddition(String name){
    
    try{
    
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia && session.getAttribute("auxiliarComponent")instanceof Entities.Addition){
        
        Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
        Entities.Addition oldAddition=(Entities.Addition)session.getAttribute("auxiliarComponent");
        
        if(franchise.getAdditionCollection()!=null && !franchise.getAdditionCollection().isEmpty()){
        
            for(Entities.Addition addition:franchise.getAdditionCollection()){
            
            if(addition.getName().toLowerCase().equals(name.toLowerCase()) && !name.toLowerCase().equals(oldAddition.getName().toLowerCase()))
            {
                System.out.print("NAME FOUND IN ADDITION CONTROLLER");
            
                return true;
            
            }
            
            
            }
        
        }
        
        }
        
    return false;
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    return false;
    
    }
    
    }
    
}
