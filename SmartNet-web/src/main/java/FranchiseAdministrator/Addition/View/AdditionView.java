/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Addition.View;

import CDIBeans.AdditionConsumesInventarioDelegate;
import CDIBeans.AdditionControllerDelegate;
import CDIBeans.DesplegableInventarioInterface;
import CDIBeans.FileUploadBean;
import CDIBeans.ImageControllerDelegate;
import CDIBeans.InventarioDelegate;
import Clases.BaseBacking;
import Clases.EdenArrayList;
import Entities.AdditionConsumesInventario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;

import javax.inject.Inject;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */

@ManagedBean(name="additionCRUDView")
@ViewScoped
public class AdditionView extends BaseBacking implements Serializable{

    /**
     * Creates a new instance of addiionCRUDView
     */
    public AdditionView() {
    }
    
    @Inject private AdditionControllerDelegate additionController;
    
    @Inject private DesplegableInventarioInterface desplegableInventario;
    
    @Inject private FileUploadBean fileUpload;
    
    @Inject private ImageControllerDelegate imagenController;
    
    @Inject private AdditionConsumesInventarioDelegate additionConsumesInventarioController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private FranchiseAdministrator.Addition.Controller.AdditionViewController _additionViewController;
    
    @Inject InventarioDelegate inventarioController;
    
    private java.util.Vector<Entities.Addition> additionList;
    
    private List<Entities.Addition>deleteAdditionList;
    
    private Entities.Addition editAddition;
    
    
    public Entities.Addition getEditAddition(){
    
        return this.editAddition;
    
    }
    
    public void setEditAddition(Entities.Addition editAddition){
    
        this.editAddition=editAddition;
    
    }
    
    public List<Entities.Addition>getDeleteAdditionList(){
    
        return this.deleteAdditionList;
    
    }
    
    public void setDeleteAdditionList(List<Entities.Addition>deleteAdditionList){
    
        this.deleteAdditionList=deleteAdditionList;
    
    }
    
    
    public DesplegableInventarioInterface getDesplegableInventario(){
    
        return this.desplegableInventario;
    
    }
    
    public void setDesplegableInventario(DesplegableInventarioInterface desplegableInventario){
    
    this.desplegableInventario=desplegableInventario;
    
    }
    
 
    @Size(min=1,max=45)
    private String name="";
    
    private java.util.List <AdditionConsumesInventario>additionConsumesInventario;
    
    private String description="";
    
    public String getDescription(){
    
        return this.description;
    
    }
    
    public void setDescription(String description){
    
    this.description=description;
    
    }
    
    public String getName(){
    
        return this.name;
    
    }
    
    public void setName(String name){
    
        this.name=name;
    
    }
    @Min(0)
	@Max(1000000)
    private double value;
    
    public double getValue(){
    
    return this.value;
        
    }
    
    public void setValue(double value){
    
    this.value=value;
    
    }
    
    public java.util.List<AdditionConsumesInventario>getAdditionConsumesInventario(){
    
    return this.additionConsumesInventario;
    
    }
    
    public void setAdditionConsumesInventario(java.util.List<AdditionConsumesInventario>additionConsumesInventario){
    
        this.additionConsumesInventario=additionConsumesInventario;
        
    }
    public java.util.Vector<Entities.Addition>getAdditionList(){
    
    return this.additionList;
    
    }
    
    public void setAdditionList(java.util.Vector<Entities.Addition>additionList){
    
        this.additionList=additionList;
    
    }
    
    @PostConstruct
    public void init(){
    
        if(this.additionConsumesInventario==null){
        
            this.additionConsumesInventario=new EdenArrayList();  
        
        }
       /*
        if(!this.fileUpload.getClassType().equals(AdditionView.class.getName()));
        {
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
        
        if(file.exists()){
        
        Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
        
        this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
        
        }
        
        this.fileUpload.setClassType(AdditionView.class.getName());
        
        }*/
    
    }
    
    public void setActionType(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
        _additionViewController.initPreRenderView(super.getContext().getViewRoot().getViewId());
     
        /*
        if(type.equals("listing")){
        
            if(this.additionList==null){
            
                this.additionList=additionController.findAll();
                
                System.out.print("Additions Size "+this.additionList.size());
            
            }
            
          
        
        }
        
        if(type.equals("edit")){
            
                    //idaddition
        //sucursalIdsucursal
        //sucursalFranquiciaIdfranquicia
    
            
        
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
              
            if(this.editAddition==null){
            
                if(getSessionAuxiliarComponent() instanceof Entities.Addition){
          this.editAddition=(Entities.Addition)getSessionAuxiliarComponent();
          
          
          for(Entities.AdditionConsumesInventario aux:editAddition.getAdditionConsumesInventarioCollection()){
          
          this.additionConsumesInventario.add(aux);
          
          }
          
          System.out.print("Edit Sucursal Name "+editAddition.getName());
          
          
                }
                else{
                
                    throw new FacesException("Exception Caugth");
                
                }
            }
            
            
           
        }
        else{
        
            throw new FacesException("Exception Caugth");
        
        }
        
       
        
        }
        
           if(!this.fileUpload.getActionType().getActionType().equals(type)){
            
            if(type.equals("edit")){
              this.fileUpload.Remove("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/AdminSucursal/imagesCache"+File.separator+getSession().getAttribute("username"));
            
            this.fileUpload.setClassType(AdditionView.class.getName());
            
            for(Entities.Imagen image:this.editAddition.getImagenCollection()){
            
               this.fileUpload.loadImagesServletContext(image, this.getSession().getAttribute("selectedOption").toString());
                
            }
            
            }
        
        this.fileUpload.setActionType(new ActionType(type));
        
        }
    */
        
    }
        
        catch(NullPointerException ex){
            
            ex.printStackTrace(System.out);
            
                }
        
    }
    
    public void handleUpload(FileUploadEvent event){
        
        try{
        
     _additionViewController.handleUpload(event);
     
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void deleteAdditionConsumesInventario(){
    
        try{
        
        _additionViewController.removeInventoryList();
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void addAdditionConsumesInventario(){
    /*
        try{
                
            Entities.AdditionConsumesInventario aux=new AdditionConsumesInventario();
            
        aux.setInventario(new Entities.Inventario());
        
        if(this.desplegableInventario.getResults().size()>0){
        
            aux.getInventario().setInventarioPK(this.desplegableInventario.getResults().get(0).getInventarioPK());
        
        }
        
        
            
            this.additionConsumesInventario.add(aux);
            
            System.out.print("Addition added");
            
        }
        catch(Exception ex){
            
            throw new FacesException("Exception Caught");
        
        }
    */
        try{
        
        _additionViewController.addToInventoryList();
        
        }
        catch(NullPointerException ex){
        
        ex.printStackTrace(System.out);
            
        }
       
    }
    
    public void deleteAdditionConsumesInventarioList(){
    
        try{
        
        if(this.additionConsumesInventario.size()>0){
        
        this.additionConsumesInventario.remove(this.additionConsumesInventario.size()-1);
        
        }
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
        }
        
    }
    
    public void checkExcistence(ComponentSystemEvent event){
        
        try{
        
        System.out.print("YOU ARE IN POST VALIDATE EVENT");
        
        if(event.getComponent().findComponent(":form:tabView:name")instanceof javax.faces.component.UIInput){
        
         javax.faces.component.UIInput input=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:name");
         
         if(input.getLocalValue()!=null){
         
             
             String additionName=input.getLocalValue().toString();
         
             System.out.print("ADDITION NAME POSTVALIDATE "+additionName);
             
             if(this._additionViewController.additionPostValidate(additionName)){
                 
                 System.out.print("ADDITION NAME FOUND POST VALIDATE");
             
               java.util.ResourceBundle bundle=super.getContext().getApplication().getResourceBundle(super.getContext(),"bundle");
               
               javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.postValidate"));
             
               message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
               
               super.getContext().addMessage(null,message);
               
               super.getContext().renderResponse();
               
             }
             
         }
        
        }
        
        /*
        if(event.getComponent().getClientId().equals("form"))
        {
    
       System.out.print(event.getComponent().getClientId());
       
       Map<String,Object>componentMap=event.getComponent().getAttributes();
       
       for(Map.Entry<String,Object>entry:componentMap.entrySet()){
       
           System.out.print(entry.getKey()+"-"+entry.getValue());
       
       }
       
       UIInput input=(UIInput)event.getComponent().findComponent("form:tabView:name");
       
       if(input.getLocalValue()!=null)
       {
           if(input.getLocalValue().toString()!=null)
           {
       System.out.print("Input UIInput Value "+input.getLocalValue().toString());
       
       Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
       
       Entities.Addition addition=additionController.findByName(input.getLocalValue().toString(), franchise.getIdfranquicia());
       
       
       if(addition!=null){
       
       FacesMessage msg=new FacesMessage("This addition exists already");
       
       msg.setSeverity(FacesMessage.SEVERITY_ERROR);
       
       getContext().addMessage(event.getComponent().getClientId(),msg);
       
       getContext().renderResponse();
       
       }
       }
    }
        
       
    } */
        
    }
        catch(NullPointerException ex){
        
           ex.printStackTrace(System.out); 
        
        }
        
    }
    
    public void postValidateUpdateAddition(javax.faces.event.ComponentSystemEvent event){
    
        
        try{
        
        if(event.getComponent().findComponent(":form:tabView:name")instanceof javax.faces.component.UIInput){
        
        javax.faces.component.UIInput input=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:name");
        
        if(input.getLocalValue()!=null){
        
            System.out.print("NAME TO FIND POST VALIDATE "+input.getLocalValue().toString());
            
          if(this._additionViewController.postValidateUpdateAddition(input.getLocalValue().toString()))
          {
              
              System.out.print("NAME FOUND IN POSTVALIDATE");
          
          java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
          
          javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddAddition.DuplicatedName"));
          
          message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
          
          this.getContext().addMessage(null, message);
          
          this.getContext().renderResponse();
          
          } 
            
        }
        
        
        }
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    }
    
    public void create(ActionEvent event){
        
        try{
        
            this._additionViewController.createAddition(this.name, this.value, this.description, this.additionConsumesInventario);
            
            System.out.print("ADDITION CREATED");
            
            /*
        System.out.print(event.getComponent().getClientId());
        
        if(event.getComponent().getClientId().equals("form:add")){
           
            Entities.Franquicia franchise;
            
            if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
            
                franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
            
            }
            
            else{
            
                throw new Exception();
            
            }
        for(Entities.Imagen image:this.fileUpload.getPhotoList()){
        
          this.imagenController.create(image);
        
        }
        
       
     
            Entities.Addition additionPOJO=new Entities.Addition();
            
            additionPOJO.setName(this.name);
            
            additionPOJO.setValue(this.value);
            
            Entities.AdditionPK additionPK=new Entities.AdditionPK();
            
            additionPK.setFranquiciaIdfranquicia(franchise.getIdfranquicia());
            
            additionPOJO.setAdditionPK(additionPK);
            
            additionPOJO.setImagenCollection(this.fileUpload.getPhotoList());
            
            additionPOJO.setDescription(this.description);
            
            additionPOJO.setFranquicia(franchise);
            
            additionController.create(additionPOJO);
            
            for(Entities.AdditionConsumesInventario aux:this.additionConsumesInventario){
            
                Entities.Inventario inventario=inventarioController.find(aux.getInventario().getInventarioPK());
                
                AdditionConsumesInventarioPK additionConsumesInventarioPK=new AdditionConsumesInventarioPK();
                
                additionConsumesInventarioPK.setAdditionIdaddition(additionPOJO.getAdditionPK().getIdaddition());
              
                additionConsumesInventarioPK.setAdditionFranquiciaIdFranquicia(additionPOJO.getAdditionPK().getFranquiciaIdfranquicia());
                
                
                 
                additionConsumesInventarioPK.setInventarioIdinventario(inventario.getInventarioPK().getIdinventario());
                
                additionConsumesInventarioPK.setInventarioFranquiciaIdFranquicia(inventario.getInventarioPK().getFranquiciaIdFranquicia());
                
                aux.setAdditionConsumesInventarioPK(additionConsumesInventarioPK);
                
                aux.setInventario(inventario);
                
                aux.setAddition(additionPOJO);
                
                additionConsumesInventarioController.create(aux);
                
            }
            
         this.fileUpload.Remove(File.separator+"Users"+File.separator+"luisnegrete"+File.separator+"Copy"+File.separator+"ProyectoLuis"+File.separator+"SmartNet"+File.separator+"SmartNet-web"+File.separator+"target"+File.separator+"SmartNet-web-1.0-SNAPSHOT"+File.separator+"AdminSucursal"+File.separator+"imagesCache"+File.separator+getSession().getAttribute("username"));
       
         
         FacesMessage message=new FacesMessage(FacesMessage.FACES_MESSAGES,"Addition added Successfully");
         
         getContext().addMessage(event.getComponent().getClientId(),message);
         
        }
        
        */
        }
        catch(NullPointerException ex){
        
       ex.printStackTrace(System.out);
        
        }
        
        }
    
    public void deleteItems(AjaxBehaviorEvent event){
        try
        {
        System.out.print("DELETING ADDITIONS");
        
        this._additionViewController.deleteAdditions(this.deleteAdditionList);
        
       /*
        System.out.print(event.getComponent().getClientId());
        
        System.out.print("List size "+this.deleteAdditionList.size());
        
        for(Entities.Addition addition:this.deleteAdditionList){
            
          for(Entities.Imagen image:addition.getImagenCollection()){
          
              imagenController.delete(image.getIdimagen());
          
          }  
        
        additionController.delete(addition.getAdditionPK());
          
        }
        
        this.additionList=additionController.findAll();
    */
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    }
    
    
    public String edit(){
        
        try{
        
        System.out.print("UPDATING ADDITION ADDITION CONSUMES INVENTARIO SIZE "+this.additionConsumesInventario.size());
        
        switch(this._additionViewController.updateAddition(this.name, this.value, this.description, this.additionConsumesInventario)){
    
            case APPROVED:{
            
                return "success";
            
            }
            
            case DISAPPROVED:{
            
                return "failure";
            
            }
            
            default:{
            
                return "failure";
            
            }
    
        }
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return "failure";
        
        }
    }
    
    public void deleteImage(ActionEvent event,String image){
         
        try{
        
        this.fileUpload.removeImage(image);
        
        }
        catch(NullPointerException ex){
        
        ex.printStackTrace(System.out);
        
        }
        
    }
   
}
