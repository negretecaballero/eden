/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SucursalHasDateLayout {
   
    Entities.Day getDay();
    
    void setDay(Entities.Day day);
    
    Clases.EdenDateInterface getOpenTime();
    
    void setOpenTime(Clases.EdenDateInterface openTime);
    
    Clases.EdenDateInterface getCloseTime();
    
    void setCloseTime(Clases.EdenDateInterface closeTime);
    
}
