/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.Classes;

/**
 *
 * @author luisnegrete
 */
public class SucursalHasDateLayoutImplementation implements SucursalHasDateLayout{
    
    private Entities.Day day;
    
    private Clases.EdenDateInterface openTime;
    
    private Clases.EdenDateInterface closeTime;
    
    public SucursalHasDateLayoutImplementation(){
    
    
    }

    
    @Override
    public Entities.Day getDay(){
    
        return this.day;
    
    }
    @Override
    public void setDay(Entities.Day day){
    
        this.day=day;
    
    }
    @Override
    public Clases.EdenDateInterface getOpenTime(){
    
        return this.openTime;
    
    }
    @Override
    public void setOpenTime(Clases.EdenDateInterface openTime){
    
    this.openTime=openTime;
    
    }
    @Override
    public Clases.EdenDateInterface getCloseTime(){
    
    return this.closeTime;
    
    }
    @Override
    public void setCloseTime(Clases.EdenDateInterface closeTime){
    
        this.closeTime=closeTime;
    
    }
    
}
