/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.Controller;

/**
 *
 * @author luisnegrete
 */
public interface BranchViewController {
    
    void initPreRenderView(String viewId);
    
    void handleFileUpload(org.primefaces.event.FileUploadEvent event);
    
    java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getSucursalHasDayList();
    
    void cityChanged();
    
    void stateChanged();
    
    ENUM.TransactionStatus createBranch(double alcance, int minimalAmount, int deliveryPrice, java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays, String user, Clases.Direccion address,boolean sucursalAdministrador,Entities.CityPK citypk);
}
