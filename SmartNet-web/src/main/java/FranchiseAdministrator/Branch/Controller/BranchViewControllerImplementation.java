/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.Controller;

import Clases.EdenString;
import ENUM.TransactionStatus;
import javax.transaction.Transactional.TxType;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.inject.Alternative
public class BranchViewControllerImplementation implements BranchViewController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private Controllers.CitySessionController _citySessionController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private Controllers.StateSingletonController _stateSingletonController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.DayController _dayController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.StateControllerInterface _stateController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.CityControllerInterface _cityController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SucursalControllerDelegate _sucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AddressTypeControllerInterface _addressTypeController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private Controllers.ProfessionSingletonController _professionSingletonController;

    @Override
    public void initPreRenderView(String viewId){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            switch(viewId){
            
                case "/adminFranquicia/sucursal.xhtml":{
                    
                   
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId))
                    {
                        
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                    if(_stateSingletonController.getStateList()!=null && !_stateSingletonController.getStateList().isEmpty()){
                    
                        _citySessionController.initList(_stateSingletonController.getStateList().get(0));
                    
                    }
                    
                    }
                        
                    break;
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    @Override
    public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getSucursalHasDayList(){
    
        try{
    
        java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>results=new java.util.ArrayList<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>();
        
        for(Entities.Day day:_dayController.getAllDays()){
        
        FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout aux=new FranchiseAdministrator.Branch.Classes.SucursalHasDateLayoutImplementation();
        
        aux.setDay(day);
        
        aux.setOpenTime(new Clases.EdenDate());
        
        aux.setCloseTime(new Clases.EdenDate());
        
        results.add(aux);
        
        }
        
        return results;
    
    }
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
       return null;
    
    }
    
    }
    
    @Override
    public void handleFileUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString(), 500, 500);
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public void stateChanged(){
    
        try{
        
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
           FranchiseAdministrator.Branch.View.Branch branch=(FranchiseAdministrator.Branch.View.Branch)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "branch");
        
           if(branch!=null){
           
               javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
               
               if(request.getParameter("form:tabView:state_input")!=null){
               
               branch.setState_id(request.getParameter("form:tabView:state_input"));
               
               System.out.print("STATE "+branch.getState_id());
               
               System.out.print("SATE QUERY RESULT "+_stateController.findByName(branch.getState_id()).getName());
               
               _citySessionController.initList(_stateController.findByName(branch.getState_id()));
               
               branch.setState(_stateController.findByName(branch.getState_id()).getName());
               
               if(branch.getCity()==null || branch.getCity().equals("")){
               
                   branch.setCity(request.getParameter("city"));
               
               }
               else{
               
                 if(_citySessionController.getCityList()!=null && !_citySessionController.getCityList().isEmpty()){
                 
                 branch.setCity(_citySessionController.getCityList().get(0).getName());
                 
                 }
               
               }
               
               
               for(Entities.City city:_stateController.findByName(branch.getState_id()).getCityCollection()){
               
                   if(city.getName().equals(branch.getCity())){
     
       
         
         branch.setCityPK(city.getCityPK());
         
         break;
     
     }
               
               }
                   
               }
           
           }
           
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
   
    @Override
    public void cityChanged(){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseAdministrator.Branch.View.Branch branch=(FranchiseAdministrator.Branch.View.Branch)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "branch");
    
        if(branch!=null){
        
            branch.setState(_stateController.findByName(branch.getState_id()).getName());
            
            branch.setCity(_cityController.find(branch.getCityPK()).getName());
        
        }
        
    }
    catch(NullPointerException ex){
    
    ex.printStackTrace(System.out);
    
    }
    
    }
    
    
    private final java.util.List<Entities.SucursalHasDay>sucursalHasDayList(java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays){
    
    try{

        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia)
        
        {
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
        if(selectedDays!=null && !selectedDays.isEmpty()){
        
        java.util.List<Entities.SucursalHasDay>sucursalHasDayList=new java.util.ArrayList<Entities.SucursalHasDay>();
        
        for(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout aux:selectedDays){
        
            Entities.SucursalHasDayPK sucursalHasDayPK=new Entities.SucursalHasDayPK();
            
            sucursalHasDayPK.setDayIdday(aux.getDay().getIdDay());
            
            sucursalHasDayPK.setSucursalFranquiciaIdfranquicia(franchise.getIdfranquicia());

            
            Entities.SucursalHasDay sucursalHasDay=new Entities.SucursalHasDay();
            
            sucursalHasDay.setSucursalHasDayPK(sucursalHasDayPK);
            
            sucursalHasDay.setDayId(aux.getDay());
            
            java.util.Date openTime=new java.util.Date();
            
            System.out.print("OPEN TIME "+aux.getOpenTime());
            
            openTime.setHours(aux.getOpenTime().getHour());
            
            openTime.setMinutes(aux.getOpenTime().getMinutes());
            
            sucursalHasDay.setOpenTime(openTime);
            
            java.util.Date closeTime=new java.util.Date();
            
            System.out.print("CLOSE TIME "+aux.getCloseTime());
            
            closeTime.setHours(aux.getCloseTime().getHour());
            
            closeTime.setMinutes(aux.getCloseTime().getMinutes());
            
            sucursalHasDay.setCloseTime(closeTime);
            
            
            
            sucursalHasDayList.add(sucursalHasDay);
            
            
        
        }
        
        return sucursalHasDayList;
        
        }
        
    }
        
        return null;
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
        
        return null;
    
    }
    
    }
    
    private final boolean checkSelectedDaysList(java.util.List<Entities.SucursalHasDay>selectedDays){
    
        try{
        
            if(selectedDays!=null && !selectedDays.isEmpty()){
            
                for(Entities.SucursalHasDay sucursalHasDay:selectedDays){
                
                    if(sucursalHasDay.getCloseTime().before(sucursalHasDay.getOpenTime())){
                    
                        return false;
                    
                    }
                
                }
            
            }
            
        return true;
        }
        catch(NullPointerException ex){
        
        return true;
        }
    
    }
    
    private final boolean checkBranchAdministrator(String username){
    
        try{
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("username")!=null){
            
                if(session.getAttribute("username").toString().equals(username)){
                
                    return false;
                
                }
            
            }
            
        return true;
        }
        catch(NullPointerException ex){
        
            return false;
        
        }
    
    }
    
    /**
	 * 
	 * @param alcance
	 * @param minimalAmount
	 * @param deliveryPrice
	 * @param selectedDays
	 * @param user
	 * @param address
	 * @param sucursalAdministrador
	 * @param citypk
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRED)
    public ENUM.TransactionStatus createBranch(double alcance, int minimalAmount, int deliveryPrice, java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays, String user, Clases.Direccion address, boolean sucursalAdministrador,Entities.CityPK citypk){
    
        try{
        if(this.checkSelectedDaysList(this.sucursalHasDayList(selectedDays)))
            {
            
            System.out.print("ALCALCE "+alcance);
            
            System.out.print("MINIMAL AMOUNT "+minimalAmount);
            
            System.out.print("DELIVERY PRICE "+deliveryPrice);
            
            System.out.print("SELECTED DAYS "+selectedDays.size());
            
            System.out.printf("DIRECCION "+address.toString());
            
            System.out.print("USER "+user);
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null && (!session.getAttribute("username").equals("")) && session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
             Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
             Entities.SucursalPK sucursalPK=new Entities.SucursalPK();
             
             sucursalPK.setFranquiciaIdfranquicia(franchise.getIdfranquicia());
             
             
            Entities.Sucursal sucursal=new Entities.Sucursal();
            
            sucursal.setSucursalPK(sucursalPK);
            
            sucursal.setSucursalHasDayCollection(this.sucursalHasDayList(selectedDays));
            
//            System.out.print("SUCURSAL HAS DAY LIST SIZE "+sucursal.getSucursalHasDayCollection().size());
            
            sucursal.setImagenCollection(_fileUpload.getPhotoList());
            
            sucursal.setLoginAdministradorCollection(new java.util.ArrayList<Entities.LoginAdministrador>());
            
            Entities.AppGroup franchiseAdministratorAppGroup =new Entities.AppGroup();
            
            franchiseAdministratorAppGroup.setAppGroupPK(new Entities.AppGroupPK());
            
            franchiseAdministratorAppGroup.getAppGroupPK().setGroupid("guanabara_sucursal_administrador");
            
            franchiseAdministratorAppGroup.getAppGroupPK().setLoginAdministradorusername(session.getAttribute("username").toString());
            
            franchiseAdministratorAppGroup.setLoginAdministrador(_loginAdministradorController.find(session.getAttribute("username").toString()));
            
            this._loginAdministradorController.createAccount(_loginAdministradorController.find(session.getAttribute("username").toString()), franchiseAdministratorAppGroup);
   
             
            sucursal.getLoginAdministradorCollection().add(_loginAdministradorController.find(session.getAttribute("username").toString()));
            
            if(sucursalAdministrador && this.checkBranchAdministrator(user)){
            
                Entities.AppGroup appGroup=new Entities.AppGroup();
                
                appGroup.setAppGroupPK(new Entities.AppGroupPK());
                
                appGroup.getAppGroupPK().setGroupid("guanabara_sucursal_administrador");
            
                appGroup.getAppGroupPK().setLoginAdministradorusername(user);
                
                Entities.LoginAdministrador loginAdministrador=new Entities.LoginAdministrador();
                
                loginAdministrador.setUsername(user);
                
                loginAdministrador.setPassword(EdenString.generatePassword(30));
                
                loginAdministrador.setProfessionIdprofession(_professionSingletonController.getProfessions().get(0));
                
                Entities.Confirmation confirmation=new Entities.Confirmation();
                
                confirmation.setConfirmed(false);
                
                confirmation.setLoginAdministrador(loginAdministrador);
                
                confirmation.setLoginAdministradorusername(user);

                loginAdministrador.setConfirmation(confirmation);

                _loginAdministradorController.createAccount(loginAdministrador, appGroup);
                
                sucursal.getLoginAdministradorCollection().add(loginAdministrador);
                
            }
            
            
            sucursal.setFranquicia(franchise);
            
            sucursal.setAlcance(alcance);
            
            sucursal.setMinimalDelivery(minimalAmount);
            
            sucursal.setDeliveryPrice(deliveryPrice);
            
            Entities.DireccionPK direccionPK=new Entities.DireccionPK();
            
            System.out.print("CITY PK "+citypk);
            
            direccionPK.setCITYSTATEidSTATE(citypk.getSTATEidSTATE());
            
            direccionPK.setCITYidCITY(citypk.getIdCITY());
            
            Entities.Direccion direccion=new Entities.Direccion();
            
            direccion.setDireccionPK(direccionPK);
            
            Entities.GpsCoordinates gps=new Entities.GpsCoordinates();
            
            Clases.EdenGpsCoordinates coordinates=new Clases.EdenGpsCoordinatesImplementation();
            
            coordinates=Clases.GPSEdenOperations.getCoordinates("Colombia,"+_cityController.find(citypk).getState().getName()+","+_cityController.find(citypk).getName()+","+address.getTipo()+"+"+address.getPrimerDigito()+"+#+"+address.getSegundoDigito()+"+-+"+address.getTercerDigito()+"");
            
            System.out.print("GPS COORDINATES "+coordinates.toString());
            
            gps.setLatitude(coordinates.getLatitude());
            
            gps.setLongitude(coordinates.getLongitude());
            
            direccion.setGpsCoordinatesidgpsCoordinated(gps);
            
            direccion.setPrimerDigito(address.getPrimerDigito());
            
            direccion.setSegundoDigito(address.getSegundoDigito());
            
            direccion.setTercerDigito((short)address.getTercerDigito());
            
            direccion.setAdicional(address.getAdicional_descripcion());
            
            direccion.setTYPEADDRESSidTYPEADDRESS(_addressTypeController.findByname(address.getTipo()));
            
            sucursal.setDireccion(direccion);
            
            System.out.print("PREPARING TO CREATE SUCURSAL");
            
            _sucursalController.createSucursal(sucursal);
            
            System.out.print("SUCURCAL CREATED");
            
            return ENUM.TransactionStatus.APPROVED;
            
            }
            
            return ENUM.TransactionStatus.DISAPPROVED;
        
        }
        
        else
        {
            
            System.out.print("CHECK THE DATES");
        
           // javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage();
            

            return TransactionStatus.DISAPPROVED;
        
        }
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return ENUM.TransactionStatus.DISAPPROVED;
        
        }
    
    }
    
}
