/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.View;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author luisnegrete
 */
public class Branch extends Clases.BaseBacking {
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Branch.Controller.BranchViewController _branchViewController;
    
    private String city;
    
    public String getCity(){
    
    return this.city;
    
    }
    
    public void setCity(String city){
    
        this.city=city;
    
    }
    
    private String state;
    
    public String getState(){
    
    return this.state;
    
    }
    
    public void setState(String state){
    
    this.state=state;
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            _branchViewController.initPreRenderView(super.getContext().getViewRoot().getViewId());
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            _branchViewController.handleFileUpload(event);
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    
    }
 
    
    private Clases.Direccion direccion;
    
    public Clases.Direccion getDireccion(){
    
        return this.direccion;
    
    }
    
    public void setDireccion(Clases.Direccion direccion){
    
        this.direccion=direccion;
    
    }
    
    private double alcance;
    
    public double getAlcance(){
    
        return this.alcance;
    
    }
    
    public void setAlcance(double alcance){
    
        this.alcance=alcance;
    
    }
    
    private int minimalAmount;
    
    public int getMinimalAmount(){
    
        return this.minimalAmount;
    
    }
    
    public void setMinimalAmount(int minimalAmount){
    
        this.minimalAmount=minimalAmount;
    
    }
    
    private int deliveryPrice;
    
    public int getDeliveryPrice()
    {
    
        return this.deliveryPrice;
    
    }  
    
    public void setDeliveryPrice(int deliveryPrice){
    
        this.deliveryPrice=deliveryPrice;
    
    }
    
    private String user;
    
    public String getUser(){
    
        return this.user;
    
    }
    
    public void setUser(String user){
    
        this.user=user;
    
    }
    
    private boolean sucursalAdministrator;
    
    public boolean getSucursalAdministrator(){
    
        return this.sucursalAdministrator;
    
    }
    
    public void setSucursalAdministrator(boolean sucursalAdministrator){
    
        this.sucursalAdministrator=sucursalAdministrator;
    
    }
    
    private org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>dayModel;
    
    public org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getDayModel(){
    
        try{
        
    if(dayModel==null){
    
        dayModel=new FrontEndModel.DayLazyDataModel(this._branchViewController.getSucursalHasDayList());
    
    }
    
    return this.dayModel;
    
        }
        catch(NullPointerException ex){
        
            return null;
        
        }
    
    }
    
    public void setDayModel(org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>dayModel){
    
        this.dayModel=dayModel;
    
    }
    
    
    private java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays;
    
    public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getSelectedDays(){
    
    return this.selectedDays;
    
    }
    
    public void setSelectedDays(java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays){
    
        this.selectedDays=selectedDays;
    
    }
    
     public void daySelectEvent(org.primefaces.event.SelectEvent event){
    
         try{
         
      System.out.print("You are in day table select event");
        
      System.out.print(this.selectedDays.size()+" Elements in Day List");
      
         }
         catch(NullPointerException ex){
         
             ex.printStackTrace(System.out);
         
         }
      
    }
    
         public void changeCity(AjaxBehaviorEvent event){
    
        try{
 
             _branchViewController.cityChanged();

        }
        catch(NullPointerException ex){
        
             
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
         
             public void changeState(AjaxBehaviorEvent event){
        
       try{

       
       this._branchViewController.stateChanged();
   
        }
   catch(NullPointerException ex){
   
      ex.printStackTrace(System.out);
   
   }

    }
         
         private Entities.CityPK cityPK;
         
         public Entities.CityPK getCityPK(){
         
             return this.cityPK;
         
         }
    
         public void setCityPK(Entities.CityPK cityPK)
         {
         
             this.cityPK=cityPK;
         
         }      
         
         
         private String state_id;
         
         public String getState_id(){
         
             return this.state_id;
         
         }
         
         public void setState_id(String state_id){
         
             this.state_id=state_id;
         
         }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
        
            if(this.direccion==null){
            
                this.direccion=new Clases.Direccion();
            
            }
            
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    public String addSucursal(){
    
        try{
        
            switch(this._branchViewController.createBranch(alcance, minimalAmount, deliveryPrice, selectedDays, user, direccion,this.sucursalAdministrator,this.cityPK)){
            
                case APPROVED:{
                
                    System.out.print("APPROOVED");
                    
                return "success";
                }
                
                case DISAPPROVED:{
                    
                    System.out.print("DISSAPROVED");
                
                return "failure";
                }
                
                default:{
                
                    return "failure";
                
                }
            
            }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
            return "failure";
            
        }
    
    }
    
    public void geolocateChangeCity(){
    
        System.out.print("YOU ARE UPDATING CITY");
        
    }
    
}
