/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Category.Classes;

/**
 *
 * @author luisnegrete
 */
public interface CategoryLayout {
 
Entities.Categoria getCategory();
    
void setCategory(Entities.Categoria category);
    
String getImage();
    
void setImage(String image);
     
}
