/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Category.Classes;

import Entities.Categoria;

/**
 *
 * @author luisnegrete
 */



public class CategoryLayoutImplementation implements CategoryLayout{
    
    private Entities.Categoria _category;
    
    String _image;

    @Override
    public Categoria getCategory() {
   
    return _category;
    
    }

    @Override
    public void setCategory(Categoria category) {
   
    _category=category;
    
    }
    
    

    @Override
    public String getImage() {
  
    return _image;
    
    }

    @Override
    public void setImage(String image) {
 
        _image=image;
    
    }
    
    
    
}
