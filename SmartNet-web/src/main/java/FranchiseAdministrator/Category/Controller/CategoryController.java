/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Category.Controller;

/**
 *
 * @author luisnegrete
 */
public interface CategoryController {
 
    public void handleUpload(org.primefaces.event.FileUploadEvent event);
 
    boolean checkExistence(String name);
    
    void createCategory(String name,String description);
    
    void deleteImage();
    
    void drop(String id);
    
    java.util.Vector<Entities.Categoria>categoryArray();
    
    void deleteCategories(java.util.List<Entities.Categoria>categories);
    
    Entities.Categoria initUpdateCategory();
   
    void updateCategory(String name,String description);
    
    void initPreRenderView(String viewId);
   
    java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>getFranchiseCategories();
    
}
