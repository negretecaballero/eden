/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Category.Controller;

import Entities.Categoria;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class CategoryControllerImplementation implements CategoryController {
  
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.CategoryControllerInterface _categoryController;
    
    @Override
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
      
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            _fileUpload.servletContainer(event.getFile(),session.getAttribute("username").toString(), 500, 500);
            
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    @Override
    public boolean checkExistence(String name){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            for(Entities.Categoria category:franchise.getCategoriaCollection()){
            
            if(category.getNombre().toLowerCase().equals(name.toLowerCase())){
            
            return true;
            
            }
            
            }
        
        }
        
        
        return false;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
    }
    
    }
    
    private final void loadimages(){
    
        try{
        
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
           if(session.getAttribute("auxiliarComponent") instanceof Entities.Categoria)
           {
           
               Entities.Categoria category=(Entities.Categoria)session.getAttribute("auxiliarComponent");
           
               if(category.getImagenCollection()!=null && !category.getImagenCollection().isEmpty()){
               
                   for(Entities.Imagen image:category.getImagenCollection()){
                   
                       _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString());
                   
                   }
               
               }
               
           }
           
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public void initPreRenderView(String viewId){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Category.View.Category category=(FranchiseAdministrator.Category.View.Category)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"addCategoria");
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            switch(viewId){
        
                case"/adminFranquicia/business/category/update-category.xhtml":{
                
                    System.out.print("You are updating a Category");
                    
                    if(session.getAttribute("auxiliarComponent")instanceof Entities.Categoria)
                    
                    {
                    
             Entities.Categoria categoria=(Entities.Categoria)session.getAttribute("auxiliarComponent");
            
             category.setCat(categoria.getNombre());
                       
            category.setDescription(categoria.getDescription());
                       
             if(_fileUpload.getActionType()==null || !this._fileUpload.getActionType().getActionType().equals(viewId)){
             
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
             System.out.print("Remove Images");
             
             this._fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
             this._fileUpload.setActionType(new Clases.ActionType(viewId));
             
             this.loadimages();
             
          
                
             
             }
             
                }
                break;
                }
                
                case "/adminFranquicia/business/category/AddCategory.xhtml":{
                
                  
                    
                       if(_fileUpload.getActionType()==null || !this._fileUpload.getActionType().getActionType().equals(viewId)){
             
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
             System.out.print("Remove Images");
             
             this._fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
             this._fileUpload.setActionType(new Clases.ActionType(viewId));
             
             }
                       
                 
                break;
                }
                
                case"/adminFranquicia/business/category/list-categories.xhtml":{
                    
                    System.out.print("You are listig categories");
                
            if(_fileUpload.getActionType()==null || !this._fileUpload.getActionType().getActionType().equals(viewId)){
             
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
          
             this._fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
             this._fileUpload.setActionType(new Clases.ActionType(viewId));
             
             }
                          category.setCategories(this.getFranchiseCategories());  
                break;
                }
                
                case"/adminFranquicia/agregarcategoria.xhtml":{
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                    }
                    
                    break;
                
                }
                
                case "/adminFranquicia/listadocategorias.xhtml":{
                
                      if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                    
                    _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                    }
                    
                    category.setCategoryList(this.categoryArray());
                
                    break;
                    
                }
                
                case "/adminFranquicia/updatecategory.xhtml":{
                
                         System.out.print("You are updating a Category");
                    
                    if(session.getAttribute("auxiliarComponent")instanceof Entities.Categoria)
                    
                    {
                    
             Entities.Categoria categoria=(Entities.Categoria)session.getAttribute("auxiliarComponent");
            
             category.setCat(categoria.getNombre());
                       
            category.setDescription(categoria.getDescription());
                       
             if(_fileUpload.getActionType()==null || !this._fileUpload.getActionType().getActionType().equals(viewId)){
             
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
             System.out.print("Remove Images");
             
             this._fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
             this._fileUpload.setActionType(new Clases.ActionType(viewId));
             
             this.loadimages();
             
          
                
             
             }
             
                }   
                
                break;
                }
        
              }
        
        }
        catch(NullPointerException ex){
        
        ex.printStackTrace(System.out);
        
        }
    
    }
    
    @Override
    public java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>getFranchiseCategories(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>results=new java.util.ArrayList<FranchiseAdministrator.Category.Classes.CategoryLayout>();
            
            if(this._categoryController.findAllFranchise(franchise.getIdfranquicia())!=null && !this._categoryController.findAllFranchise(franchise.getIdfranquicia()).isEmpty())
            {
            
                for(Entities.Categoria category:this._categoryController.findAllFranchise(franchise.getIdfranquicia())){
                
                FranchiseAdministrator.Category.Classes.CategoryLayout aux=new FranchiseAdministrator.Category.Classes.CategoryLayoutImplementation();
                
                aux.setCategory(category);
                
                if(category.getImagenCollection()!=null && !category.getImagenCollection().isEmpty()){
                
                    javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                    Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                    
                    String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+category.getNombre();
                    
                    java.io.File file=new java.io.File(path);
                    
                    if(file.exists()){
                    
                        filesManagement.cleanFolder(path);
                    
                    }
                    else{
                    
                        filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString(), category.getNombre());
                    
                    }
                    
                    _fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)category.getImagenCollection()).get(0), session.getAttribute("username").toString()+java.io.File.separator+category.getNombre());
                    
                    String imagePath=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
                    
                    aux.setImage(imagePath);
                    
                    _fileUpload.getImages().remove(_fileUpload.getImages().size()-1);
                    
                    _fileUpload.getPhotoList().remove(_fileUpload.getPhotoList().size()-1);
                    
                    System.out.print("AUX IMAGE PATH "+imagePath);
                    
                }
                else{
                
                    aux.setImage("/images/noImage.png");
                
                }
                
                results.add(aux);
                
                }
            
            }
            
            return results;
            }
            
            return null;
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
            return null;
        }
    
    }
    
    
    @Override
    public void createCategory(String name,String description){
    
        try{
        
         javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
         
         if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
         
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
             Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
         
             Entities.CategoriaPK categoryPK=new Entities.CategoriaPK();
             
             categoryPK.setFranquiciaIdfranquicia(franchise.getIdfranquicia());
             
             Entities.Categoria category=new Entities.Categoria();
             
             category.setCategoriaPK(categoryPK);
             
             category.setDescription(description);
             
             category.setNombre(name);
             
             category.setFranquicia(franchise);
             
             category.setImagenCollection(this._fileUpload.getPhotoList());
             
             this._categoryController.create(category);
             
             _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
         }
         else{
         
             throw new IllegalArgumentException("Wrong Object Type");
         
         }
        
        }
        catch(IllegalArgumentException | NullPointerException |  StackOverflowError ex){
        
            if(javax.faces.context.FacesContext.getCurrentInstance()!=null){
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("ERROR");
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
        
            javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
            }
        }
    
    }
    
    @Override
    public void deleteImage(){
    
        try{
        
              java.util.Map<String,String>requestMap=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
       
   
       if(requestMap.get("hiddenPath")!=null){
       
       String path=requestMap.get("hiddenPath");
       
       this._fileUpload.removeImage(path);
       
        }
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void drop(String id){
    
        try{
        
      java.util.Vector<String>nameList=new java.util.Vector<String>(4);
        
      nameList.add("/images/categories/dessert.jpg");
      
      nameList.add("/images/categories/drinks.jpeg");
      
      nameList.add("/images/categories/maincourse.jpg");
      
      nameList.add("/images/categories/starter.jpg");
      
      this._fileUpload.removeManually(nameList);
        
      System.out.print("Image Name "+id);
      
      
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
      
        if(id.contains("dessert")){
        
            
            
            _fileUpload.setImageManually(servletContext.getRealPath("")+"/images/categories/dessert.jpg","/images/categories/dessert.jpg","jpg");
        
        }
        
        if(id.contains("drinks")){
        
        _fileUpload.setImageManually(servletContext.getRealPath("")+"/images/categories/drinks.jpeg","/images/categories/drinks.jpeg","jpeg");
        
        }
        
        if(id.contains("maincourse")){
        
            _fileUpload.setImageManually(servletContext.getRealPath("")+"/images/categories/maincourse.jpg","/images/categories/maincourse.jpg","jpg");
        
        }
        
        if(id.contains("starters")){
        
           _fileUpload.setImageManually(servletContext.getRealPath("")+"/images/categories/starter.jpg","/images/categories/starter.jpg","jpg");
        
        }
        
        org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:photo");
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Categoria>categoryArray(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
        Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
        return this._categoryController.findAllFranchise(franchise.getIdfranquicia());
        
        }
    
        
        return null;
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    @Override
    public void deleteCategories(java.util.List<Entities.Categoria>categories){
    
        try{
        
            for(Entities.Categoria key:categories){
            
                this._categoryController.delete(key.getCategoriaPK());
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
      public Entities.Categoria initUpdateCategory(){
   
   try{
   
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
   
       
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
       
       if(session.getAttribute("auxiliarComponent")instanceof Entities.Categoria){
       
           _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
           
           Entities.Categoria category=(Entities.Categoria)session.getAttribute("auxiliarComponent");
           
           if(category.getImagenCollection()!=null && !category.getImagenCollection().isEmpty()){
           
             for(Entities.Imagen image:category.getImagenCollection()){
             
                 _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString());
             
             }
           
           }
           
           return category;
       
       }
       
       return null;
   }
   catch(Exception | StackOverflowError ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       return null;
   
   }
   
   }
    
      @Override
      public void updateCategory(String name,String description){
      
          try{
          
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
              
              if(session.getAttribute("auxiliarComponent")instanceof Entities.Categoria){
              
              Entities.Categoria category=(Entities.Categoria)session.getAttribute("auxiliarComponent");
              
              category.setNombre(name);
              
              category.setDescription(description);
              
              category.setImagenCollection(_fileUpload.getPhotoList());
              
              this._categoryController.update(category);
              
              }
              
          }
          catch(Exception | StackOverflowError ex){
          
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
          }
      
      }
      
}
