/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package FranchiseAdministrator.Category.View;

import CDIBeans.FileUploadInterface;
import Clases.BaseBacking;
import Clases.FilesManagementInterface;

import Entities.Categoria;
import Entities.Sucursal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import jaxrs.service.CategoriaFacadeREST;
import jaxrs.service.ComponeFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.ProductoFacadeREST;
import managedBeans.UploadBean;
import org.primefaces.context.RequestContext;


import org.primefaces.event.FileUploadEvent;
import java.io.*;





/**
 *
 * @author luisnegrete
 */

@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})

public class Category  extends BaseBacking implements Serializable{
   
    private static final long serialVersionUID = 7526472295622776147L;
    
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    
    @EJB
    private CategoriaFacadeREST categoriaFacadeREST;
    
    @EJB
    private ProductoFacadeREST productoFacadeREST;
    
    @EJB
    private ComponeFacadeREST componeFacadeREST;
    
    private org.primefaces.model.DefaultTreeNode root;
    
    private Entities.Categoria updateCategory;
    
    private java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>categories;
    
    public java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>getCategories(){
    
        return this.categories;
        
    }
    
    public void setCategories(java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>categories){
    
        this.categories=categories;
    
    }
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Category.Controller.CategoryController _categoryController;
    
    
    public Entities.Categoria getUpdateCategory(){
    
    return this.updateCategory;
    
    }
    
    public void setUpdateCategory(Entities.Categoria updateCategory){
    
    this.updateCategory=updateCategory;
    
    }
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.franchiseAdministratorCategoryController _franchiseAdministratorCategoryController;
    
    public org.primefaces.model.DefaultTreeNode getRoot(){
    
    return this.root;
    
    }
    
    
    public void setRoot(org.primefaces.model.DefaultTreeNode root){
    
        this.root=root;
    
    }
   
    @Inject 
    private FileUploadInterface fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface franchiseController;
    
   
    
    private Categoria selectedEdit;
    
    private List<Categoria> deleteCategoryList;

    
    private String description="";
    
    
    
    private FilesManagementInterface filesManagement;
    
    public void setDescription(String description){
    
        this.description=description;
    
    }
    
    public String getDescription(){
    
    return this.description;
    
    }
    
    
    public Categoria getSelectedEdit() {
        return selectedEdit;
    }

    public void setSelectedEdit(Categoria selectedEdit) {
        this.selectedEdit = selectedEdit;
    }
    
    private java.util.List<Entities.Categoria>categoryList;
    
    
    
    public List<Categoria> getCategoryList() {
    
    
        return this.categoryList;
        
           
    }
    
    public void setCategoryList(java.util.List<Entities.Categoria>categoryList){
    
        this.categoryList=categoryList;
    
    }

   
    public void setDeleteCategoryList(List<Categoria>deleteCategoryList){
    this.deleteCategoryList=deleteCategoryList;
    
    }
    
    public List<Categoria>getDeleteCategoryList(){
    
    return this.deleteCategoryList;
    
    }

   
   
    
    private List<Categoria>selectedCategories;

    public List<Categoria> getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(List<Categoria> selectedCategories) {
        this.selectedCategories = selectedCategories;
    }
    
    

   
    
    
    
   // @ManagedProperty(value = "#{uploadBean}")
    @Inject UploadBean statelessUploadFile;
   
    @Size(min=1,max=45)         
    private String cat="";

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.CategoryControllerInterface categoryController;

    /**
     * Creates a new instance of AddCategoria
     */
    
    
  
    
    @PostConstruct
    public void init(){      
        /*
    filesManagement=new FilesManagement();
    
    this.root=new org.primefaces.model.DefaultTreeNode("Categories",root);
       
    org.primefaces.model.DefaultTreeNode node=new org.primefaces.model.DefaultTreeNode("/images/categories/dessert.jpg",root);
        
       if(!this.fileUploadBean.getClassType().equals("AddCategoria")){
           
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
       
       System.out.print("AddCategoria set");
       
       this.fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
           
            
       this.fileUploadBean.setClassType("AddCategoria");
  
   
        }
        
       System.out.print("Add Categoria instantiated");
       
        
     Entities.Franquicia franchise;
            
            if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
            franchise=(Entities.Franquicia) getSession().getAttribute("selectedOption");
            }
            else{
                
            franchise=null;
            
            }
            
       PathSegment ps=new PathSegmentImpl("bar;franquicia_idfranquicia="+franchise.getIdfranquicia()+"");

  
       this.selectedCategories=new <Categoria>ArrayList();
       
       try{
           
           
           if(getSessionAuxiliarComponent()!=null){
           
               if(getSessionAuxiliarComponent() instanceof Categoria){
               
                   this.cat=((Categoria)getSessionAuxiliarComponent()).getNombre();
                   
                   System.out.print("Category Name "+cat);
                   
                   this.description=((Categoria)getSessionAuxiliarComponent()).getDescription();
               
                   if(this.fileUploadBean.getPhotoList()==null || this.fileUploadBean.getPhotoList().isEmpty()){
                   
                   this.categoryController.loadImages(getSession().getAttribute("username").toString(),(((Categoria)getSessionAuxiliarComponent())).getCategoriaPK());
                   
                   }
               }
               
           }
           
           else{
           
           System.out.print("Auxiliar component is null");
           
           
           
           }
         
        
       if(this.fileUploadBean.getActionType().getActionType().equals("edit") && getSession().getAttribute("auxiliarComponent")==null){
            
        this.fileUploadBean.setActionType(new ActionType("other"));
        
        if(this.fileUploadBean.getPhotoList()!=null || this.fileUploadBean.getImages()!=null){
   
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
         
            this.fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
        }
        }
        
    }
    catch(Exception ex){
    Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
    
    }
       */
    }
    
    @PreDestroy
    public void preDestroy(){
        
    System.out.print("Add Categoria Pre destroy");
    
    }
    
    
      public void ClassType(String clastype){
    
          try{
          
       this.fileUploadBean.setClassType(clastype);
    
          }
          catch(NullPointerException ex){
          
              ex.printStackTrace(System.out);
          
          }
       
    }

    
    public Category() {
    
    }

    public Short validar(String a) {
        
        try{
        
        a = a.toLowerCase();
        Short f = 0;
        if(!categoriaFacadeREST.findAll().isEmpty()){
            
        for (Categoria categoria : categoriaFacadeREST.findAll()) {
            if (categoria.getNombre().toLowerCase().equals(a)) {
                {
                    f = 1;
                }
            }
        }
        }
        return f;
       
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
        return null;
        }
        
    }
    
    
    
    public void handleFileUpload(FileUploadEvent event){
        
    try{
        
    _categoryController.handleUpload(event);
   
    RequestContext.getCurrentInstance().update("form:tabView:photo");
    
    
    }
    
    catch(NullPointerException ex){
        
        if(this.getContext()!=null)
        {    
    System.out.print("Exception Caught");
        
    Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
    
    FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR+"ERROR UPLOADING PICTURES");
    getContext().addMessage(null, msg);
    
    }
    
    }
        
        
    }
    
    public void removePicture(AjaxBehavior behaviour){
        
        try{
            
    System.out.print("It got in removePicture ActionListener");
      

   System.out.print("After listener");
   
         
    Map<String,String> params = getContext().getExternalContext().getRequestParameterMap();
	
    getContext().getExternalContext().getRequestParameterMap();
    
    System.out.print("You pressed "+params.get("form:hidden"));
    
    int index=0;
    
    if(fileUploadBean.getImages().size()!=fileUploadBean.getPhotoList().size()){
        
        System.out.print("both lists are not same size");
    throw new Exception();
    }
    
    for(int i=0;i<fileUploadBean.getImages().size();i++){
        
        if(fileUploadBean.getImages().get(i).equals(params.get("form:hidden"))){
        index=i;
        }
    
    }
    
    if(getSession().getAttribute("selectedOption") instanceof Sucursal){
    
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
      String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+fileUploadBean.getImages().get(index);
        
      System.out.print(path);
      
      filesManagement.deleteFile(path);
      
        
    }
    
    fileUploadBean.getImages().remove(index);
    
    fileUploadBean.getPhotoList().remove(index);
    
    
    
    System.out.print("Image Removed succesfully");
        }
        catch(Exception ex){
            
        Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    }
    
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
       String viewId=this.getContext().getViewRoot().getViewId();
         
       this._categoryController.initPreRenderView(viewId);
            
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
            
         
        
        }
    
    }
    
    public void editCategory(javax.faces.event.ActionEvent event){
        
     
    try{
        
    this._categoryController.updateCategory(this.cat, this.description);
    
    java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
    
    javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+bundle.getString("franchiseAdministratorUpdateCategory.CategoryUpdate"));
    
    this.getContext().addMessage(null, message);
    
    }
    
    catch(Exception ex){
        FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_ERROR+"It is not possible to redirect you");
        Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
  
    }
        
       
    }

    public  void agregar(javax.faces.event.ActionEvent event) {

try{
         
this._categoryController.createCategory(this.cat, this.description);

javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+"Category Added Succesfully");

this.getContext().addMessage(null, msg);

}

catch(NullPointerException ex){
    
Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);

if(this.getContext()!=null){

getContext().addMessage(null, new FacesMessage("ERROR AGREGANDO CATEGORIA"));           
}

}

           
}
    
    public void checkCategoryName(ComponentSystemEvent event){
        try
        {
        
    UIComponent component=event.getComponent();
   
    if(component.findComponent(":form:tabView:nombre")!=null && component.findComponent(":form:tabView:nombre")instanceof javax.faces.component.UIInput)
    {
    
    System.out.print("FIND COMPONENT IS NOT NULL");
        
    UIInput categoryNAme=(UIInput) component.findComponent(":form:tabView:nombre");

    if(categoryNAme.getLocalValue()!=null)
    {
        System.out.print("NAME TO FIND POST CONSTRUCT "+categoryNAme.getLocalValue().toString());
   
        if(_categoryController.checkExistence(categoryNAme.getLocalValue().toString())){
        
            java.util.ResourceBundle rb=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(rb.getString("franchiseAdministratorCategory.PostValidateCategoryMessage"));
        
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            this.getContext().addMessage(event.getComponent().getClientId(), msg);
            
            this.getContext().renderResponse();
            
        }
 
    }
  
    }
    
    }
   catch(NullPointerException ex){
   
       ex.printStackTrace(System.out);
   
   }
    
    }
    
    public void checkSelectedList(Categoria cat){
        try
        {
    int index=-1;
        
    
    for(int i=0;i<this.selectedCategories.size();i++)
        {
       if(selectedCategories.get(i).getCategoriaPK().getIdcategoria()==cat.getCategoriaPK().getIdcategoria()  && selectedCategories.get(i).getCategoriaPK().getFranquiciaIdfranquicia()==cat.getCategoriaPK().getFranquiciaIdfranquicia()&& selectedCategories.get(i).getCategoriaPK().getFranquiciaIdfranquicia()==cat.getCategoriaPK().getFranquiciaIdfranquicia())
       {
           
       index=i;
      
       }
        
        }
    
    if(index==-1){
    this.selectedCategories.add(cat);
    System.out.print("Category added to removeList");
    }
    
    else{
        
    this.selectedCategories.remove(index);
    System.out.print("Category removed from removeList");
    
    }
    
    System.out.print("Category List size "+this.selectedCategories.size());
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
   
    }

    public void editar(javax.faces.event.ActionEvent event) {
        
        try{
            
            this._categoryController.updateCategory(this.cat, this.description);
      
            java.util.ResourceBundle rb=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+rb.getString("franchiseAdministratorUpdateCategory.CategoryUpdate"));
            
            this.getContext().addMessage(null,msg);
            
            this.getContext().renderResponse();
            
        }
        catch(NullPointerException ex){
            
            if(this.getContext()!=null){
            
        Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
        
        FacesMessage msg=new FacesMessage("You are hacking bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(null,msg);
            }
        
        }
        
       
    }
    
    
    
    public void deleteCategories(ActionEvent event){
    
        try{
        
    System.out.print("Delete List size "+this.deleteCategoryList.size());

    this._categoryController.deleteCategories(this.deleteCategoryList);
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }

    }
    
    public void dropEvent(org.primefaces.event.DragDropEvent event){
    
    try{
    
     _categoryController.drop(event.getDragId());
       
     
    }
    catch(Exception ex){
    
        Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
   public void imageChanged(){
   System.out.print("ImageChanged");
   }
   
   public void deleteImage(javax.faces.event.AjaxBehaviorEvent event){
   
       try{
       
           
     _categoryController.deleteImage();
       
       }
       catch(NullPointerException ex){
       
          Logger.getLogger(Category.class.getName()).log(Level.SEVERE,null,ex);
          
        
       
       }
   
   }

   public java.util.List<FranchiseAdministrator.Category.Classes.CategoryLayout>getListCategories(){
   
   try{
  
      return this._categoryController.getFranchiseCategories();
   
   }
   catch(NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
      return null;
   }
   
   }
   
   public void deleteCategoryFromFranchise( Entities.Categoria category){
   
       try{
       
           
           System.out.print("Category to Delete "+category.getNombre());
           
           _franchiseAdministratorCategoryController.delete(category.getCategoriaPK());
           
           
           
       }
       catch(NullPointerException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
         
           
       }
   
   }
   
   public void updateBusinessCategory(javax.faces.event.ActionEvent event){
   
   try{
   
     
   
       this._categoryController.updateCategory(cat, description);
       
       javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Category updated successfully");
       
        
       this.getContext().addMessage(null,msg);
       
   }
   catch(NullPointerException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
   
   
   }
   
   }
   
   public void removeImage(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
       java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
       
       
       
       for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
       
       System.out.print(entry.getKey()+"-"+entry.getValue());
       
       
       }
       
       this.fileUploadBean.removeImage(requestMap.get("hiddenPath"));
   
   }
   catch(NullPointerException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   

   
   }
   
   }
   
 
   
}
