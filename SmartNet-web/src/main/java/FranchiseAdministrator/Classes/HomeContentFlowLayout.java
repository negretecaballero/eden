/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Classes;

/**
 *
 * @author luisnegrete
 */
public interface HomeContentFlowLayout {
    
    String getImage();
    
    void setImage(String image);
    
    String getLink();
    
    void setLink(String link);
    
    
}
