/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Classes;

/**
 *
 * @author luisnegrete
 */
public class HomeContentFlowLayoutImpl implements HomeContentFlowLayout {
    
    private String _image;
    
    private String _link;
    
    @Override
    public String getImage(){
    
        return _image;
    
    }
    
    @Override
    public void setImage(String image){
    
        _image=image;
    
    }
    
    @Override
    public String getLink(){
    
        return _link;
    
    }
    
    @Override
    public void setLink(String link){
    
        _link=link;
    
    }
    
}
