/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Controller;

/**
 *
 * @author luisnegrete
 */
public interface BusinessHomeController {
    
    java.util.List<Entities.News>getNewsByType();
    
    int getFranchiseId(String name);
    
    public boolean isRestaurant();
    
    java.util.Vector<FranchiseAdministrator.Classes.HomeContentFlowLayout>getHomeContentFlowLayoutArray();
    
}
