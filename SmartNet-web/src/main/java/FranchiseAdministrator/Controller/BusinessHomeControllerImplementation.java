/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class BusinessHomeControllerImplementation implements BusinessHomeController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.NewsController newsController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    
    
    @Override
    public java.util.List<Entities.News>getNewsByType(){
    
        return this.newsController.getOrderedByDate(10);
    
    }
    
    @Override
    public int getFranchiseId(String name){
    
     return  _franchiseController.findByName(name).getIdfranquicia();
        
    }
    
    @Override
    public boolean isRestaurant(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            if(franchise.getIdSubtype().getIdTipo().getNombre().equals("Restaurante")){
            
            return true;
            
            }
        
        }
        
        return false;
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
    
    }
        
    }
    
    
    @Override
    public java.util.Vector<FranchiseAdministrator.Classes.HomeContentFlowLayout>getHomeContentFlowLayoutArray(){
    
        try{
        
            java.util.Vector<FranchiseAdministrator.Classes.HomeContentFlowLayout>results=new java.util.Vector<FranchiseAdministrator.Classes.HomeContentFlowLayout>(4);
        
            
              
        FranchiseAdministrator.Classes.HomeContentFlowLayout aux=new FranchiseAdministrator.Classes.HomeContentFlowLayoutImpl();
        
        aux.setImage(java.io.File.separator+"images"+java.io.File.separator+"news.png");
        
        aux.setLink("gotonews");
        
        results.add(aux);
        
        
        FranchiseAdministrator.Classes.HomeContentFlowLayout aux1=new FranchiseAdministrator.Classes.HomeContentFlowLayoutImpl();
        
        aux1.setImage(java.io.File.separator+"images"+java.io.File.separator+"recommendation.png");
        
        aux1.setLink("gotorecommendations");
        
        results.add(aux1);
        
    
        FranchiseAdministrator.Classes.HomeContentFlowLayout aux2=new FranchiseAdministrator.Classes.HomeContentFlowLayoutImpl();
        
        aux2.setImage(java.io.File.separator+"images"+java.io.File.separator+"stats.jpg");
        
        aux2.setLink("gotostats");
        
        results.add(aux2);
        
        FranchiseAdministrator.Classes.HomeContentFlowLayout aux3=new FranchiseAdministrator.Classes.HomeContentFlowLayoutImpl();
        
        aux3.setImage(java.io.File.separator+"images"+java.io.File.separator+"partner.png");
        
        aux3.setLink("gotopartners");
        
        results.add(aux3);
            
            return results;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
