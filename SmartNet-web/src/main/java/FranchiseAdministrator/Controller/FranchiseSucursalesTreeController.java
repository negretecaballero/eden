/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Controller;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseSucursalesTreeController {
    
    org.primefaces.model.TreeNode getTree();
    
    void navigation(String franchiseName);
    
}
