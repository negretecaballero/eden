/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class FranchiseSucursalesTreeControllerImplementation implements FranchiseSucursalesTreeController {
 
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @Override
    public org.primefaces.model.TreeNode getTree(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);

        
        if(session.getAttribute("username")!=null){
        
            org.primefaces.model.TreeNode root=new org.primefaces.model.DefaultTreeNode();
            
            java.util.ResourceBundle rb=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
            org.primefaces.model.TreeNode node=new org.primefaces.model.DefaultTreeNode(rb.getString("franchiseAdministratorIndex.franchisesTree"),root);
        
            if(_loginAdministradorController.find(session.getAttribute("username").toString())!=null){
            
                Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
                
                if(loginAdministrador.getFranquiciaCollection()!=null && !loginAdministrador.getFranquiciaCollection().isEmpty()){
                
                    for(Entities.Franquicia franchise:loginAdministrador.getFranquiciaCollection()){
                    
                       org.primefaces.model.TreeNode aux=new org.primefaces.model.DefaultTreeNode(franchise.getNombre(),node);
                    
                    }
                
                }
            
            }
        
            return root;
            
        }
        
        return null;
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    @Override
    public void navigation(String franchiseName){
    

        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            System.out.print("FRANCHISE NAME "+franchiseName);
            
            if(session.getAttribute("username")!=null){
            
               Entities.LoginAdministrador loginAdministrador=_loginAdministradorController.find(session.getAttribute("username").toString());
               
               if(loginAdministrador!=null){
               
                   if(loginAdministrador.getFranquiciaCollection()!=null && !loginAdministrador.getFranquiciaCollection().isEmpty()){
                   
                       for(Entities.Franquicia franchise:loginAdministrador.getFranquiciaCollection()){
                       
                           if(franchise.getNombre().equals(franchiseName)){
                           
                               session.setAttribute("selectedOption", franchise);
                           
                               javax.faces.context.ExternalContext ec=javax.faces.context.FacesContext.getCurrentInstance().getExternalContext();
                               
                               ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"home.xhtml?faces-redirect=true&includeViewParams=true");
                               
                               
                           }
                       
                       }
                   
                   }
               
               }
                
            }
            
            javax.faces.application.NavigationHandler nh=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
           
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
}
