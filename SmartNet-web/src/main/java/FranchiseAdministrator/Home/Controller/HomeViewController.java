/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Home.Controller;

/**
 *
 * @author luisnegrete
 */


public interface HomeViewController {
    
    
   void initPreRenderView(String viewId);
   
   void redirectFranchise(String franchiseName);
    
}
