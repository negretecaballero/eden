/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Home.Controller;

import javax.el.MethodExpression;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class HomeViewControllerImplementation implements HomeViewController {
   
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @Override
     public void initPreRenderView(String viewId){
    
    try{
    
        switch(viewId){
        
            case "/adminFranquicia/index.xhtml":{
                
                System.out.print("YOU ARE IN INDEX.XHTML");
            
               javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
               
               javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
               
               FranchiseAdministrator.Home.View.Home home=(FranchiseAdministrator.Home.View.Home)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "home");
               
               if(session.getAttribute("username")!=null && !session.getAttribute("username").toString().equals("")){
               
                  home.setFranchiseList(_franchiseController.findByUsername(session.getAttribute("username").toString()));
               
               }
               
               javax.faces.component.UIViewRoot viewRoot=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
               
               if(viewRoot.findComponent("formulario:sucursalesSubmenu")!=null && viewRoot.findComponent("formulario:sucursalesSubmenu")instanceof org.primefaces.component.submenu.UISubmenu){
               
                   
                   System.out.print("This is a SUBMENU INSTANCE");
                   
                   org.primefaces.component.submenu.UISubmenu submenu=(org.primefaces.component.submenu.UISubmenu)viewRoot.findComponent("formulario:sucursalesSubmenu");
               
                   if(_franchiseController.findByUsername(session.getAttribute("username").toString())!=null && !_franchiseController.findByUsername(session.getAttribute("username").toString()).isEmpty())
                   {
                   
                       
                       for(Entities.Franquicia franchise:_franchiseController.findByUsername(session.getAttribute("username").toString())){
                       
                             org.primefaces.component.menuitem.UIMenuItem item=new org.primefaces.component.menuitem.UIMenuItem();
                     
                            item.setValue(franchise.getNombre());

                            FacesContext context = FacesContext.getCurrentInstance();
                                
                            MethodExpression methodExpression = context.getApplication().getExpressionFactory().createMethodExpression(context.getELContext(), "#{home.actionListener}", null, new Class[] { ActionEvent.class });
                                
                            item.addActionListener(new MethodExpressionActionListener(methodExpression));

                            submenu.getChildren().add(item);
                       
                       
                       }
                       
                   
                   
                   }
                 
                   
                 
                   
               }
             
                
               System.out.print("FRANCHISE SIZE "+home.getFranchiseList().size());
               
            break;
            }
        
        
        }
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
     
     @Override
     public void redirectFranchise(String franchiseName){
     
         try{
         
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
         
             if(session!=null){
             
                    for(Entities.Franquicia franchise:_franchiseController.findByUsername(session.getAttribute("username").toString())){
                       
                          if(franchise.getNombre().equals(franchiseName)){
                          
                              session.setAttribute("selectedOption", franchise);
                          
                              javax.faces.application.NavigationHandler nh=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
                              
                              nh.handleNavigation(javax.faces.context.FacesContext.getCurrentInstance(), null, "success");
                              
                          }
                       
                       
                       }
             
             }
             
         }
         catch(NullPointerException ex){
         
             ex.printStackTrace(System.out);
         
         }
     
     }
     
    
}
