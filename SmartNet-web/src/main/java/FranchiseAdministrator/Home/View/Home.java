/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Home.View;

/**
 *
 * @author luisnegrete
 */
public class Home extends Clases.BaseBacking{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Home.Controller.HomeViewController _homeViewController;
    
    
    private java.util.List<Entities.Franquicia>franchiseList;
    
    public java.util.List<Entities.Franquicia>getFranchiseList(){
    
        return this.franchiseList;
    
    }
    
    public void setFranchiseList(java.util.List<Entities.Franquicia>franchiseList){
    
        this.franchiseList=franchiseList;
    
    }
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
    try{
    
        String viewId=super.getCurrentView();
        
        _homeViewController.initPreRenderView(viewId);
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    public void actionListener(javax.faces.event.ActionEvent event){
    
        try{
        
           System.out.print("Action Listener Called");
           
           if(event.getComponent() instanceof org.primefaces.component.menuitem.UIMenuItem){
           
           org.primefaces.component.menuitem.UIMenuItem item=(org.primefaces.component.menuitem.UIMenuItem)event.getComponent();
           
           System.out.print(item.getValue());
           
           this._homeViewController.redirectFranchise(item.getValue().toString());
           
           }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
}
