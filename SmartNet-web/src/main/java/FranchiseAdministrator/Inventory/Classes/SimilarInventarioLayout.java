/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.Classes;

import Entities.Inventario;
import javax.validation.constraints.Size;

/**
 *
 * @author luisnegrete
 */
public class SimilarInventarioLayout implements SimilarInventarioLayoutInterface{
    
    private Inventario inventario;
    
    //@Min(19) @Max(21)
    private double priceDifference;
    
    private double equivalency;
    
    @Size(min=10,max=11)
    private String type;
    
    
    
    @Override
    public Inventario getInventario(){
    
    return this.inventario;
    
    }
    
    @Override
    public void setInventario(Inventario inventario){
    
    this.inventario=inventario;
    
    }
    
    @Override
    public double getPriceDifference(){
    
        return this.priceDifference;
        
    }
    @Override
    public void setPriceDifference(double priceDifference){
    
        this.priceDifference=priceDifference;
    
    }
    @Override
    public double getEquivalency(){
    
        return this.equivalency;
    
    }
    @Override
    public void setEquivalency(double equivalency){
    
        this.equivalency=equivalency;
    
    }
    @Override
    public String getType(){

return this.type;
}
   
    @Override
    public void setType(String type){
    
    this.type=type;
    
    }
    
    
}
