/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.Classes;

import Entities.Inventario;

/**
 *
 * @author luisnegrete
 */
public interface SimilarInventarioLayoutInterface {
    
    void setInventario(Inventario inventario);
    
    Inventario getInventario();
    
    double getPriceDifference();
    
    void setPriceDifference(double priceDifference);
    
    String getType();
    
    void setType(String type);
    
    double getEquivalency();
    
    void setEquivalency(double equivalency);
    
    
}
