/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.Controller;

/**
 *
 * @author luisnegrete
 */

public interface InventoryController {
    
    boolean checkNameExistence(String name);
    
    void addSimilarInventory();
    
     void deleteSimilar();
     
     void addInventory(String name, String description,boolean enabled,java.util.List<Entities.SimilarInventario>similar);
     
     java.util.Vector<Entities.Inventario>inventoryArray();
     
     void deleteInventory(java.util.List<Entities.Inventario>deleteList);
    
     
     Entities.Inventario getInventory();
     
     void initSimilarInventoryList();
     
     java.util.Vector<Entities.Inventario>getInvSelectOneMenu();
     
     void updateInventory();
     
     void initPreRenderView(String view);
}
