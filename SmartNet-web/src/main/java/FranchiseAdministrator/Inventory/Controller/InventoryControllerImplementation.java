/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.Controller;

import javax.el.ELResolver;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class InventoryControllerImplementation implements InventoryController {
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.InventarioDelegate _inventoryController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SimilarInventarioController _similarInventory;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUpload;
    
    @Override
    public boolean checkNameExistence(String name){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
                for(Entities.Inventario inventory:franchise.getInventarioCollection()){
                
                if(inventory.getNombre().toLowerCase().equals(name.toLowerCase())){
                
                    return true;
                
                }
                
                }
            
            }            
            return false;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public void addSimilarInventory(){
    
    try{
    
    ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
       FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
    
       if(inventoryView!=null)
       {
       
           if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals("/adminFranquicia/agregarinventario.xhtml"))
       {
               
       System.out.print("SIMILAR INVENTORY LIST "+inventoryView.getSimilarInventario().size());
       
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
       Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
       
       if(inventoryView.getSimilarInventario()==null){
       
           inventoryView.setSimilarInventario(new java.util.ArrayList<Entities.SimilarInventario>());
       
       }
        
          
            
          if(inventoryView.getSimilarInventario().size()!=this._inventoryController.findByFranchise(franchise.getIdfranquicia()).size()){
                         
             Entities.SimilarInventario aux=new Entities.SimilarInventario();

             Entities.InventarioPK key=new Entities.InventarioPK();
             
             Entities.Inventario inventory=new Entities.Inventario(key);
             
              aux.setInventario1(inventory);
                 
              inventoryView.getSimilarInventario().add(aux);
         
          }
        
          System.out.print("UPDATED INVENTORY LIST SIZE "+inventoryView.getSimilarInventario().size());
     
       
       }
       
           else if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals("/adminFranquicia/updateInventory.xhtml")){
        
           
            
            System.out.print("INEVNTORY KEY "+inventoryView.getSelectedInventory());
             
            if(inventoryView.getSelectedInventory()!=null){

            Entities.Inventario inv=this._inventoryController.find(inventoryView.getSelectedInventory());
        
            System.out.print("INVENTORY NAME "+inv.getNombre());
           if(!this.checkSimilarExistence(inventoryView.getSelectedInventory()))
            
            {
            FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux=new  FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayout();
            
            aux.setInventario(inv);
            
            aux.setType("inventario1");
            
            inventoryView.getSimilarInventarioLayout().add(aux);
            
            System.out.print("UPDATED SIMILAR PRODUCTS SIZE "+inventoryView.getSimilarInventarioLayout().size());
            } 
           
           else{
           
               java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
               
               javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateInventory.SimilarExists"));
           
               javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
               
               javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
               
           }
           
            }
          
            
        }
           
           
       }
        
         
        
        
       
       
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    public boolean checkSimilarExistence(Entities.InventarioPK key){
        
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
    
        if(inventoryView!=null){
                
               for(FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux:inventoryView.getSimilarInventarioLayout())
               {
               
               if(aux.getInventario().getInventarioPK().equals(key))
               {
               
                   return true;
               
               }
               }
        
        }
       return false; 
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
    
    }
    
    }
    
    
    
    private Entities.Inventario inventory(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
           
            if(franchise.getInventarioCollection()!=null && !franchise.getInventarioCollection().isEmpty()){
            
                for(Entities.Inventario inv:franchise.getInventarioCollection()){
                
                if(!this.similarContains(inv.getInventarioPK())){
                
                    return inv;
                
                }
                
                }
            
            }
            
            }
            
            return null;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    
    @Override
    public void deleteSimilar(){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
    
        FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
        
        if(inventoryView!=null){
        
               if(inventoryView.getSimilarInventario().size()>0){
           
           inventoryView.getSimilarInventario().remove(inventoryView.getSimilarInventario().size()-1);
           
           }
        
        }
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void addInventory(String name, String description,boolean enabled,java.util.List<Entities.SimilarInventario>similarList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                    
                Entities.InventarioPK inventarioPK=new Entities.InventarioPK();
                
                inventarioPK.setFranquiciaIdFranquicia(franchise.getIdfranquicia());
                
              
                Entities.Inventario inventario=new Entities.Inventario();
                
                inventario.setInventarioPK(inventarioPK);
                
                if(!enabled){
                
                     inventario.setHabilitado((short)0);
                    
                }
                else{
                
                     inventario.setHabilitado((short)1);
                
                }
               
                
                inventario.setNombre(name);
                
                inventario.setDescripcion(description);
                
               // inventario.setSimilarInventarioCollection(similar);
                
                _inventoryController.create(inventario);
                
                if(similarList!=null && !similarList.isEmpty())
                {
                
                for(Entities.SimilarInventario similar:similarList){
                
                   
                System.out.print("Simila Inventaro 1 key "+similar.getInventario1().getInventarioPK());
                      
                Entities.InventarioPK inventario1PK=new Entities.InventarioPK();
                
                inventario1PK.setFranquiciaIdFranquicia(similar.getInventario1().getInventarioPK().getFranquiciaIdFranquicia());
                
                inventario1PK.setIdinventario(similar.getInventario1().getInventarioPK().getIdinventario());
                
                Entities.Inventario inventario1=_inventoryController.find(inventario1PK);
                    
                Entities.SimilarInventarioPK similarInventarioPK=new Entities.SimilarInventarioPK();
                
                similarInventarioPK.setInventarioIdinventario(inventario.getInventarioPK().getIdinventario());
                
                similarInventarioPK.setInventarioIdinventario1(inventario1.getInventarioPK().getIdinventario());
                
                similarInventarioPK.setInventarioFranquiciaIdFranquicia(inventario.getInventarioPK().getFranquiciaIdFranquicia());
               
                similarInventarioPK.setInventarioFranquiciaIdFranquicia1(inventario1.getInventarioPK().getFranquiciaIdFranquicia());
               
              
                similar.setSimilarInventarioPK(similarInventarioPK);
                
                similar.setInventario1(inventario1);
                
                similar.setInventario(inventario);
                
                _similarInventory.create(similar);
                }
                
            }
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Inventario Added");
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null,msg);      
                
            
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
    @Override
    public java.util.Vector<Entities.Inventario>inventoryArray(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                if(franchise.getInventarioCollection()!=null && !franchise.getInventarioCollection().isEmpty())
                {
                
                    java.util.Vector<Entities.Inventario>results=new java.util.Vector<Entities.Inventario>(franchise.getInventarioCollection().size());
                
                    for(Entities.Inventario inventory:franchise.getInventarioCollection()){
                    
                        results.add(inventory);
                    
                    }
                    
                    return results;
                    
                }
                
            }
            
            return null;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }
    
    }
    
    @Override
    public void deleteInventory(java.util.List<Entities.Inventario>deleteList){
    
    try{
    
        if(deleteList!=null && !deleteList.isEmpty()){
        
          
            for(Entities.Inventario inventory: deleteList){   
            
                this._inventoryController.delete(inventory.getInventarioPK());
            
            }
            
            java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
            javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorListInventory.InventoryDeleted"));
        
            javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, message);
            
            javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
            
        }
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
    @Override
    public Entities.Inventario getInventory(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
               
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Inventario ){
            
            Entities.Inventario inventory=(Entities.Inventario)session.getAttribute("auxiliarComponent");
       
            return inventory;
            
            }
            
            return null;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public void initSimilarInventoryList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
             javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
            
               FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
       
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Inventario && inventoryView!=null){
            
                if(inventoryView.getSimilarInventarioLayout()==null || inventoryView.getSimilarInventarioLayout().isEmpty()){
                
                inventoryView.setSimilarInventarioLayout(inventoryLayoutList());
            
                System.out.print("SIMILAR INVENTORY SIZE "+inventoryView.getSimilarInventarioLayout().size());
                
                }
                
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    private java.util.List<FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface>inventoryLayoutList(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent") instanceof Entities.Inventario){
            
            java.util.List<FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface>similarInventarioLayout=new java.util.ArrayList<FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface>();
                
            Entities.Inventario inv=(Entities.Inventario)session.getAttribute("auxiliarComponent");
            
            for(Entities.SimilarInventario similar:inv.getSimilarInventarioCollection()){
            
            System.out.print("Similar Inventario List is not null "+inv.getSimilarInventarioCollection().size());
            
            FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux=new FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayout();
            
            aux.setInventario(similar.getInventario1());
            
            aux.setPriceDifference(similar.getPriceDiference());
            
            aux.setEquivalency(similar.getEquivalencia());
            
            aux.setType("inventario");
            
            similarInventarioLayout.add(aux);
            
            }
            
            for(Entities.SimilarInventario similar:inv.getSimilarInventarioCollection1()){
            
            System.out.print("Similar Inventario 1 is not null "+inv.getSimilarInventarioCollection1().size());
            
            FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux=new FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayout();
            
            aux.setInventario(similar.getInventario());
            
            aux.setEquivalency(1/similar.getEquivalencia());
            
            aux.setPriceDifference(similar.getPriceDiference()*-1);
            
            aux.setType("inventario1");
            
            similarInventarioLayout.add(aux);
            
            }
            
            return similarInventarioLayout;
            
            }
            
        return null;
        }
        catch(Exception | StackOverflowError ex){
        
            return null;
        
        }
    
    }
    
    @Override
    public java.util.Vector<Entities.Inventario>getInvSelectOneMenu(){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
           
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                 Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
       
            
            if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals("/adminFranquicia/agregarinventario.xhtml")){
        
            
            java.util.Vector<Entities.Inventario>results=new java.util.Vector<Entities.Inventario>(franchise.getInventarioCollection().size());
            
            for(Entities.Inventario inventory:franchise.getInventarioCollection()){
            
            results.add(inventory);
            
            }
            
                return results;
            }
            else if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals("/adminFranquicia/updateInventory.xhtml")){
            
                if(session.getAttribute("auxiliarComponent")instanceof Entities.Inventario)
                {
                
                    Entities.Inventario inventory=(Entities.Inventario)session.getAttribute("auxiliarComponent");
                    
                java.util.Vector<Entities.Inventario>results=new java.util.Vector<Entities.Inventario>(franchise.getInventarioCollection().size()-1);
            
                for(Entities.Inventario inv:franchise.getInventarioCollection()){
                
                if(!inv.getInventarioPK().equals(inventory.getInventarioPK())){
                
                    results.add(inv);
                
                }
                
                }
                
                return results;
            }
            }
            
            }
            
        
        
        
        return null;
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return null;
    
    }
    
    }
    
    
    private boolean similarContains(Entities.InventarioPK key){
    
        try{
        
          javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
          
          FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
          
          if(inventoryView.getSimilarInventario()!=null && !inventoryView.getSimilarInventario().isEmpty()){
          
              System.out.print("SIMILAR INVENTORY SIZE "+inventoryView.getSimilarInventario().size());
              
          for(Entities.SimilarInventario inventory:inventoryView.getSimilarInventario()){
          
              System.out.print(inventory.getInventario1().getNombre()+" KEY "+inventory.getInventario1().getInventarioPK()+ " KEY TO FIND "+key);

              if(inventory.getInventario1().getInventarioPK().equals(key))
              {
              
                  return true;
              
              }
          }
          
          }
            
        return false;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
   
    
    private Entities.Inventario similar(){
    
        try{
        
            
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }
        
    }
    
    @Override
    public void updateInventory(){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseAdministrator.Inventory.View.Inventory inventoryView=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(inventoryView!=null && session.getAttribute("auxiliarComponent")instanceof Entities.Inventario){
        
            Entities.Inventario inventory=(Entities.Inventario)session.getAttribute("auxiliarComponent");
        
            inventory.setNombre(inventoryView.getName());
            
            inventory.setDescripcion(inventoryView.getDescription());
            
            this._inventoryController.update(inventory);
            
            
     for(Entities.SimilarInventario similar:inventory.getSimilarInventarioCollection()){
     
         System.out.print(similar.getInventario1().getNombre());
     
         boolean found=false;
         
         for(FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux:inventoryView.getSimilarInventarioLayout()){
         
             if(similar.getInventario1().equals(aux.getInventario())){
             
             similar.setPriceDiference(aux.getPriceDifference());
             
             similar.setEquivalencia(aux.getEquivalency());
             
             this._similarInventory.update(similar);
             
                
             
             found=true;
             
             inventoryView.getSimilarInventarioLayout().remove(aux);
             
             break;
             
             }
             
         }
         
         
         if(!found){
         
             _similarInventory.remove(similar.getSimilarInventarioPK());
         
         }
     
     }
     
     
     for(Entities.SimilarInventario similar:inventory.getSimilarInventarioCollection1()){
     
     System.out.print(similar.getInventario().getNombre());
     
     boolean found=false;
     
     for(FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux:inventoryView.getSimilarInventarioLayout()){
     
     if(similar.getInventario().equals(aux.getInventario())){
     
     similar.setPriceDiference(aux.getPriceDifference()*-1);
     
     similar.setEquivalencia(1/aux.getEquivalency());
     
          _similarInventory.update(similar);
           
     
     found=true;
     
     }
     
     }
     
     
     if(!found){
     
              _similarInventory.remove(similar.getSimilarInventarioPK());
        
     
     }
     
     }
     
     if(inventoryView.getSimilarInventarioLayout()!=null && !inventoryView.getSimilarInventarioLayout().isEmpty()){
     
         for(FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface aux:inventoryView.getSimilarInventarioLayout())
         {
         
         Entities.SimilarInventario similarInv=new Entities.SimilarInventario();
         
         Entities.SimilarInventarioPK similarInventarioPK=new Entities.SimilarInventarioPK();
         
         similarInventarioPK.setInventarioIdinventario(inventory.getInventarioPK().getIdinventario());
         
         similarInventarioPK.setInventarioIdinventario1(aux.getInventario().getInventarioPK().getIdinventario());
         
         
         similarInventarioPK.setInventarioFranquiciaIdFranquicia(inventory.getInventarioPK().getFranquiciaIdFranquicia());
         
         similarInventarioPK.setInventarioFranquiciaIdFranquicia1(aux.getInventario().getInventarioPK().getFranquiciaIdFranquicia());
         
         
         similarInv.setSimilarInventarioPK(similarInventarioPK);
         
         similarInv.setInventario(inventory);
         
         similarInv.setInventario1(aux.getInventario());
         
         similarInv.setPriceDiference(aux.getPriceDifference());
         
         similarInv.setEquivalencia(aux.getEquivalency());
         
         _similarInventory.create(similarInv);
         

         }
     
     }
            
        }
        
    }
    catch(NullPointerException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void initPreRenderView(String view){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Inventory.View.Inventory inventory=(FranchiseAdministrator.Inventory.View.Inventory)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "CRUDINVENTARIO");
            
    switch(view){
    
        case "/adminFranquicia/business/inventory/list-inventory.xhtml":{
            
            if(!_fileUpload.getActionType().getActionType().equals(view)){

            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
            _fileUpload.setActionType(new Clases.ActionType(view));
            
            }
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                inventory.setInventoryList(new java.util.HashSet<Entities.Inventario>(this._inventoryController.findByFranchise(franchise.getIdfranquicia())));
            
            }
            
            break;
        
        }
        
        case "/adminFranquicia/business/inventory/add-inventory.xhtml":{
        
            if(!_fileUpload.getActionType().getActionType().equals(view)){
            
             
            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
            _fileUpload.setActionType(new Clases.ActionType(view));
            
            }
            
        break;
        }
        
        case "/adminFranquicia/business/inventory/update-inventory.xhtml":{
            
                System.out.print("UPDATING INVENTORY");
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Inventario){
                
                System.out.print("UPDATING INVENTORY");
            
                Entities.Inventario item=(Entities.Inventario)session.getAttribute("auxiliarComponent");
                
                inventory.setName(item.getNombre());
                
                inventory.setDescription(item.getDescripcion());
                
                if(item.getHabilitado()==0){
                
                    inventory.setEnabled(false);
                
                }
                else{
                
                    inventory.setEnabled(true);
                
                }
            
            }
            

          this.initSimilarInventoryList();
          
            break;
        
        }
        
        case "/adminFranquicia/invlisting.xhtml":{
        
                  System.out.print("UPDATING INVENTORY");
            
          if(!_fileUpload.getActionType().getActionType().equals(view)){

            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
            _fileUpload.setActionType(new Clases.ActionType(view));
            
            }
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                inventory.setInventoryList(new java.util.HashSet<Entities.Inventario>(this._inventoryController.findByFranchise(franchise.getIdfranquicia())));
            
            }
            
            break;
        
        
        }
        
        case "/adminFranquicia/updateInventory.xhtml":{
        
                   System.out.print("UPDATING INVENTORY");
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Inventario){
                
                System.out.print("UPDATING INVENTORY");
            
                Entities.Inventario item=(Entities.Inventario)session.getAttribute("auxiliarComponent");
                
                inventory.setName(item.getNombre());
                
                inventory.setDescription(item.getDescripcion());
                
                if(item.getHabilitado()==0){
                
                    inventory.setEnabled(false);
                
                }
                else{
                
                    inventory.setEnabled(true);
                
                }
            
            }
            

          this.initSimilarInventoryList();
        
        break;
        }
        
        
    
    }
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
}
