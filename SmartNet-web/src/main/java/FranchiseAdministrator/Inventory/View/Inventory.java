/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.View;

import CDIBeans.DesplegableInventarioInterface;
import Clases.BaseBacking;
import SessionClasses.MailManagement;
import FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface;
import Entities.Inventario;
import Entities.SimilarInventario;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import jaxrs.service.InventarioFacadeREST;
import jaxrs.service.SimilarInventarioFacadeREST;


/**
 *
 * @author luisnegrete
 */
@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})

public class Inventory extends BaseBacking{

    /**
     * Creates a new instance of CRUDINVENTARIO
     */
    
    @EJB
    InventarioFacadeREST inventarioFacadeREST;
    
    @EJB
    SimilarInventarioFacadeREST similarInventarioFacadeREST;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private FranchiseAdministrator.Inventory.Controller.InventoryController _inventoryController;
    
    private Entities.InventarioPK selectedInventory;
    
    public Entities.InventarioPK getSelectedInventory(){
    
        return this.selectedInventory;
    
    }
    
    public void setSelectedInventory(Entities.InventarioPK selectedInventory){
    
        this.selectedInventory=selectedInventory;
    
    }
    
    public Inventory() {
    }
    
    
    private boolean _enabled;
    
    public boolean getEnabled(){
    
    return _enabled;
    
    }
    
    public void setEnabled(boolean enabled){
    
    _enabled=enabled;
    
    }
    
    
    private List<Inventario>deleteList;
    
    
    public List<Inventario>getDeleteList(){
    
        return this.deleteList;
    
    }
    
    public void setDeleteList(List<Inventario>deleteList){
    
    this.deleteList=deleteList;
    
    }
    
    
    private List<Inventario>inventarioList;
    
    private List<Inventario> inventarios;
    
  
    
    public java.util.Vector<Inventario>getInventarios(){
    
        try{
        
     return _inventoryController.inventoryArray();
     
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    
    
    
    public void setInventarios(List<Inventario>inventarios){
    
    this.inventarios=inventarios;
        
    }
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.FranchiseAdministratorInventoryController _franchiseAdministratorInventoryController;
    
    @Inject 
    private DesplegableInventarioInterface desplegableInventario;
    
    private List<SimilarInventarioLayoutInterface>similarInventarioLayout;
    
    public List<SimilarInventarioLayoutInterface> getSimilarInventarioLayout(){
    
        try{
        
        return this.similarInventarioLayout;
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    public void setSimilarInventarioLayout(List<SimilarInventarioLayoutInterface>similarInventarioLayout){
    
        this.similarInventarioLayout=similarInventarioLayout;
    
    }
    
    private java.util.HashSet<Entities.Inventario>inventoryList;
    
    public java.util.HashSet<Entities.Inventario>getInventoryList(){
    
        return this.inventoryList;
    
    }
    
    public void setInventoryList(java.util.HashSet<Entities.Inventario>inventoryList){
    
        this.inventoryList=inventoryList;
    
    }
    
    
    public java.util.Vector<Entities.Inventario>getInventarioByFranchise(){
    
        try{
        
    return _inventoryController.inventoryArray();
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    
    @PostConstruct
    public void init(){
        
        try{
        
        inventarios=desplegableInventario.getResults();
                
        this.inventarioList=desplegableInventario.getResults();
        
        if(this.deleteList==null){
        
        this.deleteList=new <Integer>ArrayList();
        
        }
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    @Size(min=1,max=45)
    private String name="";
    
   @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.InventarioDelegate inventarioController;
    
    @Size(min=0,max=20)
    private String description="";
    
    
    private List<Entities.SimilarInventario>similarInventario=null;
    
    public List<Entities.SimilarInventario>getSimilarInventario(){
    
        if(this.similarInventario==null){
        
        this.similarInventario=new<SimilarInventario>ArrayList();
        
       
        }
        
        
    return this.similarInventario;
    
    }
    
    public void setSimilarInventario(List<Entities.SimilarInventario>similarInventario){
    
    this.similarInventario=similarInventario;
    
    }
    
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    public String getName(){
    
        return this.name;
    
    }
    
    
    public void setDescription(String description){
    
        this.description=description;
    
    }
    
    public String getDescription(){
    
        return this.description;
    
    }
    
    public  java.util.Vector<Entities.Inventario>getInventarioList(){
    
    try{
        
        
    
        return _inventoryController.getInvSelectOneMenu();
      
    }
    catch(NullPointerException ex){
    
  
    
    Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
    
    return null;
    
    }
    
    
    }
    
    public void checkInventarioName(ComponentSystemEvent event){
    
        try
   {
    UIComponent component=event.getComponent();
    
    UIInput input=(UIInput)component.findComponent("form:tabView:name");
    
    if(input!=null && input.getLocalValue()!=null)
    {
    
    String nameComponent=input.getLocalValue().toString();
    
    System.out.print("POST VALIDATE "+nameComponent);
   
    if(_inventoryController.checkNameExistence(nameComponent))
    {
    
        FacesMessage msg=new FacesMessage("There is another inventario with the same name in this franchise");
 
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(input.getClientId(),msg);
        
        getContext().renderResponse();
        
    }
    
    }
    
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }

    }
    
    
    public void AddInventario(ActionEvent event){
    
        try{
        
            _inventoryController.addInventory(this.name, this.description, _enabled, this.similarInventario);
          
        }
        
        catch(NullPointerException ex){
        
            if(this.getContext()!=null)
            {
            FacesMessage msg=new FacesMessage ("Exception Caught");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            getContext().addMessage(event.getComponent().getClientId(),msg);
        
        }
        }
        
        
    
    }
    
    public void DeleteInventories(javax.faces.event.ActionEvent event){
    
        try{
        
        this._inventoryController.deleteInventory(this.deleteList);
  
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    public void editInventory(ActionEvent event){
        
        
        try{
    

    this._inventoryController.updateInventory();

     FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"Inventory edited");
     
     getContext().addMessage(event.getComponent().getClientId(),msg);
    
    }
   
    
        
        catch(NullPointerException ex){
            
            if(this.getContext()!=null)
            {
            FacesMessage msg=new FacesMessage("You are hacking bitch");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            getContext().addMessage(event.getComponent().getClientId(),msg);
        
            Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
        
        }
            
        }
    
    }
    
    
    public void AddSimilarInventario(AjaxBehaviorEvent event){
    
    try{
           
        
            this._inventoryController.addSimilarInventory();
      
        
       
        
    
    }
    catch(RuntimeException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void sendMail(AjaxBehaviorEvent event){
    
    MailManagement mail=new MailManagement();
    
    mail.sendMail("diana.moron@outlook.com","Test","Hi cute");
    
    System.out.print("Mail Sent");
    
    
    }
    
    
    
    
    public void deleteSimilar(AjaxBehaviorEvent event,SimilarInventarioLayoutInterface sim){
    
    try{
    
        //System.out.print(event.getComponent().getClientId());
        
        if(this.similarInventarioLayout!=null && !this.similarInventarioLayout.isEmpty()){
    
           this.similarInventarioLayout.remove(sim);
    
    }
        
    
    }
    catch(NullPointerException  ex){
    
        if(this.getContext()!=null)
        {    
    FacesMessage msg=new FacesMessage("You are hacking bitch");
    
    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
    
    getContext().addMessage(null,msg);
    
    Logger.getLogger(Inventory.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    }
    
    public void deleteFromSimilar(AjaxBehaviorEvent event){
    
    
        try{
        
            _inventoryController.deleteSimilar();
        
        }
        catch(RuntimeException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void deleteFranchise(Entities.InventarioPK key){
    
        try{
            
            System.out.print("Item to delete "+key.toString());
        
           _franchiseAdministratorInventoryController.deleteInventory(key);
        
        }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
           
        
        }
    
    }
    
    private Entities.Inventario _updateInventory;
    
    public Entities.Inventario getUpdateInventory(){
    
    return _updateInventory;
    
    }
    
    public void setUpdateInventory(Entities.Inventario updateInventory){
    
    _updateInventory=updateInventory;
    
    }
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
        
        try{
    
    String view=this.getContext().getViewRoot().getViewId();
    
    this._inventoryController.initPreRenderView(view);
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    public void updateInventory(javax.faces.event.ActionEvent event){
    
    try{
    
        if(_enabled){
        
            _updateInventory.setHabilitado((short)1);
        
        }
        else{
        
            _updateInventory.setHabilitado((short)0);
        
        }
        
        _franchiseAdministratorInventoryController.updateInventory(_updateInventory);
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Inventory Updated Successfully");
    
        this.getContext().addMessage(null,msg);
        
    }
    
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
       
    
    }
    
    }
    
}
