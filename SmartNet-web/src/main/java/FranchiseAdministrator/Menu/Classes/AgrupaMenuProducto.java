/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.Classes;

import Entities.Agrupa;
import Entities.CategoriaPK;
import Entities.Producto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class AgrupaMenuProducto implements AgrupaMenuProductoInterface{
    
    private Agrupa agrupa;
    
    private CategoriaPK categoriaPK;
    
    private List<Producto> productList;
    
    private Entities.ProductoPK productKey;
    
    
    @Override
    public Entities.ProductoPK getProductKey(){
    
        return this.productKey;
    
    }
    
    @Override
    public void setProductKey(Entities.ProductoPK productKey){
    
    this.productKey=productKey;
    
    }
    
    
    @Override
    public void setProductList(List<Producto>productList){
        
        this.productList=productList;
    
    }
    
    @Override
    public List<Producto>getProductList(){
    
    return this.productList;
    
    }
    
    public AgrupaMenuProducto(){
    
    this.productList=new <Producto>ArrayList();
    
    }
    
    
    @Override
    public void setAgrupa(Agrupa agrupa){
    
        this.agrupa=agrupa;
    
    }
    
    
    @Override
    public Agrupa getAgrupa(){
    
        return this.agrupa;
    
    }
    
    
    @Override
    public void setCategoriaPK(CategoriaPK categoriaPK){
    
    this.categoriaPK=categoriaPK;
        
    }
    
    @Override
    public CategoriaPK getCategoriaPK(){
    
    return this.categoriaPK;
    
    }
    
   
    
    
}
