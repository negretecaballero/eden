/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.Classes;

import Entities.Agrupa;
import Entities.CategoriaPK;
import Entities.Producto;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface AgrupaMenuProductoInterface {
    
    void setCategoriaPK(CategoriaPK categoriaPK);
    
    CategoriaPK getCategoriaPK();
    
    void setAgrupa(Agrupa agrupa);
    
    Agrupa getAgrupa();
    
    void setProductList(List <Producto>productList);
    
    List<Producto>getProductList();
    
    Entities.ProductoPK getProductKey();
    
    void setProductKey(Entities.ProductoPK productKey);
    
}
