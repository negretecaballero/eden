/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.Controller;

/**
 *
 * @author luisnegrete
 */
public interface MenuViewController {
    
    void initMenu(String viewId);
    
    void handleUpload(org.primefaces.event.FileUploadEvent event);
    
    void addAgrupa();
    
    void createMenu(java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>agrupaMenuProductoList,String name, String description,double price);
    
    void deleteAgrupa(java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface> agrupaMenuProductoList);
    
    void deleteMenu(java.util.List<Entities.Menu>deleteList);
    
    boolean checkExistence(String name);
    
    ENUM.TransactionStatus updateMenu(String name,String description, double price,java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>agrupaMenuProductoList);
   
    boolean updateProductPostValidate(String name);
    
}
