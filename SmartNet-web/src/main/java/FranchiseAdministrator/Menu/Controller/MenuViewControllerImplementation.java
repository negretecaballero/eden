/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.Controller;

import javax.transaction.Transactional.TxType;
import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class MenuViewControllerImplementation implements MenuViewController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient CDIBeans.ProductoControllerInterface _productController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient CDIBeans.MenuControllerInterface _menuController;
    
    
    
    
    @Override
    //@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void initMenu(String viewId){
    
        try{
        
            final javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            final javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            switch(viewId){
            
                case "/adminFranquicia/AddMenu.xhtml":{
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                        _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                    
                        FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
                        
                        if(menuView!=null){
                        
                            menuView.setName("");
                            
                            menuView.setDescription("");
                            
                            menuView.setPrice(0.0);
                            
                            menuView.setSelectedProducts(new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>());
                            
                            menuView.setDeleteAgrupaMenuList(new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>());
                                               
                            
                        }
                        
                        _fileUpload.setActionType(new Clases.ActionType(viewId));
                        
                    }
                    
                    
                    
                    break;
                
                }
                
                case "/adminFranquicia/MenuListing.xhtml":{
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                       _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                     
                       FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
                       
                      if(session.getAttribute("selectedOption")instanceof Entities.Franquicia && menuView!=null){
                      
                      Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                      
                      if(_menuController.findByFranchise(franchise.getIdfranquicia())!=null && !_menuController.findByFranchise(franchise.getIdfranquicia()).isEmpty())
                      {
                      
                          menuView.setMenuList(_menuController.findByFranchise(franchise.getIdfranquicia()));
                      
                      }
                      
                      
                      }
                      
                        _fileUpload.setActionType(new Clases.ActionType(viewId));
                      
                    }
                
                }
                
                case "/adminFranquicia/updateMenu.xhtml":{
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                        FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
                        
                     if(session.getAttribute("auxiliarComponent") instanceof Entities.Menu  && menuView!=null){
                         
                         _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                     
                         Entities.Menu menu=(Entities.Menu)session.getAttribute("auxiliarComponent");
                         
                         _menuController.loadImages(menu);
                         
                         menuView.setName(menu.getNombre());
                         
                         menuView.setDescription(menu.getDescription());
                         
                         menuView.setPrice(menu.getPrecio());
                         
                         menuView.setSelectedProducts(IteratorUtils.toList(this.loadAgrupaMenuProductoList(menu).iterator()));
                         
                         _fileUpload.setActionType(new Clases.ActionType(viewId));
                         
                     }
                    
                    }
                    
                    break;
                
                }
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
    @Override
    public boolean checkExistence(String name){
    
        try{
        
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
       if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
       
           Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
           
           if(_menuController.findByFranchise(franchise.getIdfranquicia())!=null && !_menuController.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
           
           for(Entities.Menu menu:_menuController.findByFranchise(franchise.getIdfranquicia())){
           
           if(menu.getNombre().toLowerCase().equals(name.toLowerCase())){
           
               return true;
           
           }
           
           }
           
           }
           
       }
       
        return false;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return false;
        }
    
    }
    
    @Override
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
    try{
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
         
        
        if(session.getAttribute("username")!=null){
            
        _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString(), 500, 500);
        
        }
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void addAgrupa(){
    
    try{
    
       final  javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        final FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
    
        final javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(menuView!=null && session.getAttribute("selectedOption") instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            if(menuView.getSelectedProducts()==null){
            
                menuView.setSelectedProducts(new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>());
            
            }
            
            
        if(_productController.findByFranchise(franchise.getIdfranquicia())!=null && menuView.getSelectedProducts().size()<_productController.findByFranchise(franchise.getIdfranquicia()).size()){
 
        Entities.AgrupaPK agrupaPK=new Entities.AgrupaPK();
        
        Entities.Agrupa agrupa=new Entities.Agrupa();
        
        agrupa.setAgrupaPK(agrupaPK);
        
        FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto=new FranchiseAdministrator.Menu.Classes.AgrupaMenuProducto();
        
        agrupaMenuProducto.setAgrupa(agrupa);
      
        
        agrupaMenuProducto.setProductList(_productController.findByFranchise(franchise.getIdfranquicia()));
        
        
        
        agrupaMenuProducto.setProductKey(new Entities.ProductoPK());
        
        menuView.getSelectedProducts().add(agrupaMenuProducto);
        
            }
        
        }
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void deleteAgrupa(java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface> agrupaMenuProductoList){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
        
        if(agrupaMenuProductoList!=null && !agrupaMenuProductoList.isEmpty() && menuView!=null){
        
            for(FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto:agrupaMenuProductoList){
            
                System.out.print("AGRUPA MENU PRODYCTO LIST DELETE "+agrupaMenuProducto.getProductKey());
            
                menuView.getSelectedProducts().remove(agrupaMenuProducto);
                
            }
        
        }
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    /**
	 * 
	 * @param agrupaMenuProductoList
	 * @param name
	 * @param description
	 * @param price
	 */
	@Override
	@javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public void createMenu(java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>agrupaMenuProductoList,String name, String description,double price){
    
    try{
    
        if(agrupaMenuProductoList!=null && this.checkAgrupaMenuList(agrupaMenuProductoList)){
            
            Entities.Menu menu=new Entities.Menu();
    
            java.util.List<Entities.Agrupa>agrupaList=new java.util.ArrayList<Entities.Agrupa>();
            
            for(FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto:agrupaMenuProductoList){
            
                System.out.print("CATEGORY PK "+agrupaMenuProducto.getCategoriaPK());
                
                System.out.print("PRODUCT PK "+agrupaMenuProducto.getProductKey());
            
                agrupaMenuProducto.getAgrupa().getAgrupaPK().setProductoCategoriaFranquiciaIdFranquicia(agrupaMenuProducto.getCategoriaPK().getFranquiciaIdfranquicia());
                
                agrupaMenuProducto.getAgrupa().getAgrupaPK().setProductoCategoriaIdCategoria(agrupaMenuProducto.getCategoriaPK().getIdcategoria());
                
                agrupaMenuProducto.getAgrupa().getAgrupaPK().setProductoIdproducto(agrupaMenuProducto.getProductKey().getIdproducto());
                
                agrupaMenuProducto.getAgrupa().setProducto(_productController.find(agrupaMenuProducto.getProductKey()));
                
                agrupaList.add(agrupaMenuProducto.getAgrupa());
                
            }
            
            menu.setAgrupaCollection(agrupaList);
            
            menu.setDescription(description);
            
            menu.setPrecio(price);
            
            menu.setNombre(name);
            
            menu.setImagenCollection(_fileUpload.getPhotoList());
        
            _menuController.create(menu);
            
            
            
            _fileUpload.setActionType(null);
            
        }
        
        else{
        
           System.out.print("THERE ARE 2 SIMILAR PRODUCTS IN THE LIST");
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorMenu.RepeatedProduct"));
             
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null,message);
        
        }
        
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
   
    @Override
    public void deleteMenu(java.util.List<Entities.Menu>deleteList){
    
        try{
            
            if(deleteList!=null && !deleteList.isEmpty()){
            
                for(Entities.Menu menu:deleteList){
                
                    _menuController.delete(menu.getIdmenu());
                
                }
                
            }
        
            _fileUpload.setActionType(null);
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    }
    
    private final SessionClasses.EdenList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>loadAgrupaMenuProductoList(Entities.Menu menu){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            if(menu.getAgrupaCollection()!=null && !menu.getAgrupaCollection().isEmpty() && _productController.findByFranchise(franchise.getIdfranquicia())!=null && !_productController.findByFranchise(franchise.getIdfranquicia()).isEmpty()){
            
                SessionClasses.EdenList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>results=new SessionClasses.EdenList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>();
            
                for(Entities.Agrupa agrupa:menu.getAgrupaCollection()){
                
                    FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto=new FranchiseAdministrator.Menu.Classes.AgrupaMenuProducto();
                    
                    agrupaMenuProducto.setAgrupa(agrupa);
                    
                    agrupaMenuProducto.setProductList(_productController.findByFranchise(franchise.getIdfranquicia()));
                    
                    agrupaMenuProducto.setCategoriaPK(agrupa.getProducto().getCategoria().getCategoriaPK());
                    
                    agrupaMenuProducto.setProductKey(agrupa.getProducto().getProductoPK());
                    
                    results.addItem(agrupaMenuProducto);
                
                }
               
                return results;
                
            }
            
            }
            
        return null;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        }
    
    }
    
    private final boolean checkAgrupaMenuList(java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>agrupaMenuProductoList){
    
        try{
            
            if(agrupaMenuProductoList!=null && !agrupaMenuProductoList.isEmpty()){
            
                for(FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto:agrupaMenuProductoList){
                
                    java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>auxiList=new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>();
                
                    auxiList=agrupaMenuProductoList;
                    
                    auxiList.remove(agrupaMenuProducto);
                    
                    for(FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface aux:auxiList){
                    
                        if(agrupaMenuProducto.getProductKey().equals(aux.getProductKey())){
                            
                            return false;
                            
                        }
                    
                    }
                    
                }
            
            }
        
        return true;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return true;
            
        }
    
    }
    
    @Override
    public boolean updateProductPostValidate(String name){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
          
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia && session.getAttribute("auxiliarComponent")instanceof Entities.Menu){
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            Entities.Menu menu=(Entities.Menu)session.getAttribute("auxiliarComponent");
            
            for(Entities.Menu auxMenu:this._menuController.findByFranchise(franchise.getIdfranquicia())){
            
                if(auxMenu.getNombre().toLowerCase().equals(name.toLowerCase()) && !auxMenu.getNombre().toLowerCase().equals(menu.getNombre().toLowerCase()))
                {
                
                    return true;
                
                }
            }
            
            
            }
            
        return false;
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return false;
        }
    
    }
    
    @Override
    public ENUM.TransactionStatus updateMenu(String name,String description, double price,java.util.List<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>agrupaMenuProductoList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
           FranchiseAdministrator.Menu.View.MenuView menuView=(FranchiseAdministrator.Menu.View.MenuView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "cRUDmenu");
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Menu && agrupaMenuProductoList!=null && !agrupaMenuProductoList.isEmpty() && this.checkAgrupaMenuList(agrupaMenuProductoList)){
            
                Entities.Menu menu=(Entities.Menu)session.getAttribute("auxiliarComponent");
                
                menu.setNombre(name);
                
                menu.setDescription(description);
                
                menu.setPrecio(price);
                
                menu.setImagenCollection(_fileUpload.getPhotoList());
                
                  java.util.List<Entities.Agrupa>agrupaList=new java.util.ArrayList<Entities.Agrupa>();
                  
                  System.out.print("AGRUPA MENU LIST "+agrupaMenuProductoList.size());
                  
                  for(FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface agrupaMenuProducto:agrupaMenuProductoList){
                  
                      Entities.Agrupa agrupa=agrupaMenuProducto.getAgrupa();
                      
                      agrupa.getAgrupaPK().setMenuIdmenu(menu.getIdmenu());
                      
                      agrupa.getAgrupaPK().setProductoCategoriaFranquiciaIdFranquicia(agrupaMenuProducto.getCategoriaPK().getFranquiciaIdfranquicia());
                      
                      agrupa.getAgrupaPK().setProductoCategoriaIdCategoria(agrupaMenuProducto.getCategoriaPK().getIdcategoria());
                      
                      agrupa.getAgrupaPK().setProductoIdproducto(agrupaMenuProducto.getProductKey().getIdproducto());
                      
                      agrupa.setMenu(menu);
                      
                      agrupa.setProducto(_productController.find(agrupaMenuProducto.getProductKey()));
                      
                      agrupaList.add(agrupa);
                  
                  }
                  
                
                  
                _menuController.update(menu);
                
              /*    menu.setAgrupaCollection(agrupaList);
                  
                  menuView.setName("");
                  
                  menuView.setDescription("");
                  
                  menuView.setPrice(0.0);
                  
                  menuView.setSelectedProducts(new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>());
                  
                  menuView.setDeleteAgrupaMenuList(new java.util.ArrayList<FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface>());
                  */
                  _fileUpload.setActionType(null);
            
                  return ENUM.TransactionStatus.APPROVED;
                  
            }
            
            else{
            
                System.out.print("THERE ARE 2 SIMILAR PRODUCTS IN THE LIST");
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
                javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorMenu.RepeatedProduct"));
             
                message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null,message);
                
                javax.faces.context.FacesContext.getCurrentInstance().renderResponse();
                
                _fileUpload.setActionType(null);
                
                   return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
              return ENUM.TransactionStatus.DISAPPROVED;
            
        }
        
    }
    
}
