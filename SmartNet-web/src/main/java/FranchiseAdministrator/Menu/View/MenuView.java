/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.View;

import FranchiseAdministrator.Product.View.AgregarProducto;
import CDIBeans.DesplegableCategoriaInterface;
import CDIBeans.DesplegableMenuInterface;
import CDIBeans.desplegablesProductoInterface;
import FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface;
import Clases.BaseBacking;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;

import Entities.Agrupa;
import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Franquicia;
import Entities.Producto;
import Entities.Sucursal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.AgrupaFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.MenuFacadeREST;
import jaxrs.service.ProductoFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import org.primefaces.event.FileUploadEvent;


/**
 *
 * @author luisnegrete
 */

@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})
public class MenuView extends BaseBacking{

    /**
     * Creates a new instance of CRUDmenu
     */
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Menu.Controller.MenuViewController _menuViewController;
    
    private java.util.List<Clases.MenuUpdateLayoutInterface>menuUpdateList;
    
    
    
    public java.util.List<Clases.MenuUpdateLayoutInterface>getMenuUpdateList(){
    
    return this.menuUpdateList;
    
    }
    
    public void setMenuUpdateList(java.util.List<Clases.MenuUpdateLayoutInterface>menuUpdateList){
    
    this.menuUpdateList=menuUpdateList;
    
    }
    
    @EJB
    private MenuFacadeREST menuFacade;
    
    @EJB
    private ProductoFacadeREST productoFacade;
    
    @EJB
    private ImagenFacadeREST imagenFacade;
    
    
    @EJB
    private AgrupaFacadeREST agrupaFacade;
    
    @Size
    (min=1,max=100)
    private String name="";

    @javax.validation.constraints.Size(min=0,max=65535)
    private String description="";
    
    public String getDescription(){
    
        return this.description;
    
    }
    
    public void setDescription(String description){
    
        this.description=description;
    
    }
    
    public void setName(String name){
    
        this.name=name;
    
    }
    
    public String getName(){
    
    return this.name;
        
    }
    
    public void setPrice(double price){
    
    this.price=price;
    
    }
    
    public double getPrice(){
    
    return this.price;
    
    }
    
    public void setSelectedProducts(List<AgrupaMenuProductoInterface>selectedProducts){
    
    this.selectedProducts=selectedProducts;
    
    }
    
    public List<AgrupaMenuProductoInterface>getSelectedProducts(){
    
    return this.selectedProducts;
    
    }
    
    
    

    
    @Inject private desplegablesProductoInterface desplegableProducto;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.ProductoControllerInterface productController;
    
    @Inject private DesplegableCategoriaInterface desplegableCategoria;
    
    @Inject private DesplegableMenuInterface desplegableMenu;
    
    private List<Producto>products;
    
    private java.util.Vector<Entities.Menu>menuList;
    
    private List<AgrupaMenuProductoInterface> selectedProducts;
    
    
    private List<Categoria>categories;
            
    private double price;
    
    private int selectedProduct;
    
    private List<Entities.Menu>deleteList;
    
    private List<AgrupaMenuProductoInterface> deleteAgrupaMenuList;
    
    public void setDeleteAgrupaMenuList(List<AgrupaMenuProductoInterface> deleteAgrupaMenuList){
    
    this.deleteAgrupaMenuList=deleteAgrupaMenuList;
    
    }
    
    public void postValidateUpdateMenu(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIComponent component=event.getComponent();
            
            if(component.findComponent(":form:tabView:name")instanceof javax.faces.component.UIInput){
            
                javax.faces.component.UIInput input=(javax.faces.component.UIInput)component.findComponent(":form:tabView:name");
                
                if(input.getLocalValue()!=null){
                
                    String menuName=input.getLocalValue().toString();
                    
                    if(this._menuViewController.updateProductPostValidate(menuName)){
                    
                        java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                        
                        javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateMenu.postValidate"));
                    
                        message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
                     
                        System.out.print("POSTVALIDATE IS TRUE");
                        
                        super.getContext().addMessage(null, message);
                        
                        super.getContext().renderResponse();
                        
                    }
                
                }
            
            }
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public List<AgrupaMenuProductoInterface> getDeleteAgrupaMenuList(){
    
    return this.deleteAgrupaMenuList;
    
    }
    
    public void setDeleteList(List<Entities.Menu>deleteList){
    
    this.deleteList=deleteList;
    
    }
    
    public List<Entities.Menu> getDeleteList(){
    
    return this.deleteList;
        
    }
    
    public void setMenuList(java.util.Vector<Entities.Menu> menuList){
    
    this.menuList=menuList;
    
    }
    
    public java.util.Vector<Entities.Menu> getMenuList(){
    
    return this.menuList;
    }
    
    public List<Categoria>getCategories(){
    
        return this.categories;
    
    }
    
    public void setCategories(List<Categoria>categories){
    
        this.categories=categories;
    
    }
    
    public void setProducts(List<Producto>products){
    
    this.products=products;
    
    }
    
    public List <Producto>getProducts(){

return this.products;
}
   
    
    
    
   
    
    
    public void setSelectecProduct(int selectedProduct){
    
    this.selectedProduct=selectedProduct;
    
    }
    
    public int getSelectedProduct(){
    
    return this.selectedProduct;
    
    }
    
    public MenuView() {
        
        
    }
    
    @PostConstruct
    public void init(){
        
     
        
    
         }
    
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIComponent component=event.getComponent();
            
            if(component.findComponent(":form:tabView:name") instanceof javax.faces.component.UIInput){
            
            javax.faces.component.UIInput input=(javax.faces.component.UIInput)component.findComponent(":form:tabView:name");
            
            if(input.getLocalValue()!=null){
            
                String inputName=input.getLocalValue().toString();
                
                if(this._menuViewController.checkExistence(inputName)){
                
                 java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
                 
                 javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddMenu.PostValidate"));
                
                 msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                 
                 this.getContext().addMessage(null,msg);
                 
                 this.getContext().renderResponse();
                 
                }
            
            }
            
            }
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
        try
        
        {
      
//            System.out.print("File Upload Size "+this.fileUpload.getPhotoList().size());
            
  //          System.out.print("Images Before "+this.fileUpload.getImages());
    
            String view=this.getContext().getViewRoot().getViewId();
            
            _menuViewController.initMenu(view);
            /*
            switch(view){
            
                case "/adminFranquicia/AddMenu.xhtml":{
                
                    
                    
                break;
                
                }
            
            }
            
        if(type.equals("AddMenu")){
        
            if(!this.fileUpload.getActionType().getActionType().equals(type)){
            
                this.fileUpload.setActionType(new ActionType(type));
                
                 javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
            
                 this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString());
            
            }
            
            
        
        }
        
        else if(type.equals("listingMenu")){
        
        if(!this.fileUpload.getActionType().getActionType().equals(type)){
        
        this.fileUpload.setActionType(new ActionType(type));
        
        
        }
        
        if(menuList==null){
                    
            if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia)
                {
                    
            
            desplegableMenu.updateMenuList(((Entities.Franquicia)getSession().getAttribute("selectedOption")).getIdfranquicia());
            
            menuList=desplegableMenu.getMenuList();
                    
                    }
            else
            {
            
                throw new RuntimeException ();
            
            }
        
        }
        
        
        }
        
        else if(type.equals("updateMenu")){
            
            Entities.Menu menu=null;
            
            if(getSessionAuxiliarComponent()instanceof Entities.Menu){
            
            menu=(Entities.Menu)getSessionAuxiliarComponent();
            
            }
            else{
            
                throw new RuntimeException();
            
            }
        
        if(!this.fileUpload.getActionType().getActionType().equals("updateMenu")){
        
        this.fileUpload.setActionType(new ActionType("updateMenu"));
        
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia)
        {
        
            
             
                
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
                
                for(Imagen image:menu.getImagenCollection()){
                
                    this.fileUpload.loadImagesServletContext(image, getSession().getAttribute("username").toString());
                    
                }
            
           
        
        }
        else{
        
            throw new RuntimeException ();
        
        }
        
        }
        
        this.name=menu.getNombre();
        
        this.price=menu.getPrecio();
        
        this.categories=this.desplegableCategoria.getResults();
        
        
        
        if(this.selectedProducts.size()<=0)
        
        {
        
        for(Agrupa agrupa:menu.getAgrupaCollection()){
        
            AgrupaMenuProductoInterface agrupaMenuProducto=new AgrupaMenuProducto();
            
            agrupaMenuProducto.setAgrupa(agrupa);
            
            agrupaMenuProducto.setCategoriaPK(agrupa.getProducto().getCategoria().getCategoriaPK());
            
            this.desplegableProducto.resetList();
            
            this.desplegableProducto.UpdateProductsList(agrupa.getProducto().getCategoria().getCategoriaPK());
            
            List<Producto>productList=new<Producto>ArrayList();
            
            for(Producto product:this.desplegableProducto.getProductsList()){
            
            Producto producto=new Producto();
            
            producto=product;
            
            productList.add(producto);
            
            }
            
            System.out.print("DesplegableProductoSize "+this.desplegableProducto.getProductsList().size());
            
            agrupaMenuProducto.setProductList(productList);
            
            agrupaMenuProducto.setProductKey(agrupa.getProducto().getProductoPK());
            
            
            this.selectedProducts.add(agrupaMenuProducto);
        
        }
        
        }
        }
        
        if(!this.fileUpload.getActionType().getActionType().equals(type)){
        
        this.fileUpload.setActionType(new Clases.ActionType(type));
        
        }
        
          System.out.print("File Upload Size "+this.fileUpload.getPhotoList().size()+" after");
        
          System.out.print("Images After "+this.fileUpload.getImages().size());
          
          for(String aux:this.fileUpload.getImages()){
          
          System.out.print(aux);
          
          }
         */ 
        }
        
        catch(RuntimeException ex){
        
            Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
        
    
    }
    
    public void handeFileUpload(FileUploadEvent event){
        
        try{
               
 
    
    _menuViewController.handleUpload(event);
        }
        catch(RuntimeException ex){
        
        Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
  
        }
    }
    
    public void deleteAgrupa(AjaxBehaviorEvent event){
    
        try{
        
       this._menuViewController.deleteAgrupa(this.deleteAgrupaMenuList);
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    private AgrupaMenuProductoInterface updateDesplegablesList(AgrupaMenuProductoInterface agrupaMenuProducto, List<Producto>productList){
    
        try{
        List<Producto>list=new <Producto>ArrayList();
        
        for(Producto product:productList){
        
            Producto prod=new Producto();
            
            prod=product;
            
           list.add(prod);
            
        
        }
        
        agrupaMenuProducto.setProductList(list);
        
        return agrupaMenuProducto;
        
        }
        
        catch(RuntimeException ex){
            
        
            Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
            
            
        return null;
        
        }
    
    }
    
    public void addAgrupa(AjaxBehaviorEvent event){
    
        try{
        /*
        
        if(event.getComponent().getClientId().equals("form:tabView:addButton"))
        {
        
          
            
        AgrupaPK agrupaPK=new AgrupaPK();
        
        Agrupa agrupa=new Agrupa();
        
        agrupa.setAgrupaPK(agrupaPK);
        
        AgrupaMenuProductoInterface agrupaMenuProducto=new AgrupaMenuProducto();
        
        agrupaMenuProducto.setAgrupa(agrupa);
        
        //this.desplegableProducto.resetList();
        
        agrupaMenuProducto=updateDesplegablesList(agrupaMenuProducto,this.desplegableProducto.getProductsList());
        
        //agrupaMenuProducto.setProductKey(agrupaMenuProducto.getProductList().get(agrupaMenuProducto.getProductList().size()-1).getProductoPK());
       
        this.selectedProducts.add(agrupaMenuProducto);
        
         
        System.out.print("Producto Key "+this.productController.find(agrupaMenuProducto.getProductKey()).getNombre());
        
        System.out.print("SelectedProducts size "+this.selectedProducts.size());
        
        }
        
        else{
        
            throw new RuntimeException();
        
        }
        */
            
            _menuViewController.addAgrupa();
            
        }
        
        catch(RuntimeException ex){
        
            Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
 
        
        }
    
    
    }
    
    public void selectCategory(AjaxBehavior event,CategoriaPK categoriapk,AgrupaMenuProductoInterface agrupaMenuCompone){
        
        try{
        
    System.out.print("You are in select Category ajax event");
    
    System.out.print(categoriapk);
    
    
    this.desplegableProducto.UpdateProductsList(categoriapk);
    
    products=this.desplegableProducto.getProductsList();
    
    agrupaMenuCompone=updateDesplegablesList(agrupaMenuCompone,products);
    
    System.out.print("desplegableProducto Size "+this.desplegableProducto.getProductsList().size());
    
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    
    public void removePicture(AjaxBehaviorEvent ajaxBehavior){
        
        try{
  
            String imageRemove="";
              
           
            
            System.out.print("PhaseID "+ajaxBehavior.getPhaseId().getName());
            
            System.out.print("You pressed yes button");
            
            Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
            
            System.out.print("REQUEST MAP SIZE "+requestMap.size());
            
            for(Map.Entry<String,String>entry:requestMap.entrySet()){
            
                System.out.print("Attribute "+entry.getKey()+"-"+entry.getValue());
            
            }
            
            HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
            
            imageRemove=request.getParameter("hiddenComponent");
            
            System.out.print(imageRemove);
            
            if(!imageRemove.equals("")){
            
            int index=0;
            
/*            for(int i=0;i<fileUpload.getImages().size();i++){
            
            if(fileUpload.getImages().get(i).equals(imageRemove)){
            index=i;
            
            break;
            }
            }
            
            fileUpload.getImages().remove(index);
            
            fileUpload.getPhotoList().remove(index);
            */
            if(getSession().getAttribute("selectedOption")instanceof Sucursal){}
            
            String path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/AdminSucursal/imagesCache/"+getSession().getAttribute("username")+"/"+imageRemove;
            
                    FilesManagementInterface filesManagement=new FilesManagement();
                    
                    filesManagement.deleteFile(path);
            
            }
            
            else if(getSession().getAttribute("selectedOption")instanceof Franquicia){
            
                String path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/Smartnet-web/target/SmartNet-web-1.0-SNAPSHOT/adminFranquicia/"+imageRemove;
                
                FilesManagementInterface filesManagement=new FilesManagement();
                
                filesManagement.deleteFile(path);
            
            }
            
            
            
            
        }
        
        catch(NullPointerException ex){
        
            if(this.getContext()!=null){
            
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
            
            FacesMessage msg=new FacesMessage("You are hacking bitch");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            getContext().addMessage(null,msg);
            
            }
           
            
        }
        
        
        
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.MenuControllerInterface menuController;
    
    private boolean checkMenuName(int franchiseId,String Name){
    
        for(Entities.Menu menu:this.menuController.findByFranchise(franchiseId))
        {
        
        if(menu.getNombre().equals(Name)){
        
            return true;
        
        }
        
        }
        
        return false;
    }
    
    public void checkExistence(ComponentSystemEvent event){
        
        try
        {
    UIComponent component=event.getComponent();
    
    UIInput input=(UIInput)component.findComponent("form:tabView:name");
    
    if(input.getLocalValue()!=null){
    System.out.print(input.getLocalValue().toString());
    
    System.out.print("Selected Products size "+this.selectedProducts.size());
    
    String query=input.getLocalValue().toString();
    
    Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
     
    if(checkMenuName(franchise.getIdfranquicia(),query)){
    
        System.out.print("there is a menu with this name");
        
        FacesMessage msg=new FacesMessage("There is a Menu with this name");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        getContext().renderResponse();
    
    }
    
    if(this.selectedProducts.size()<=0){
        
        System.out.print("You must add at least one product the the menu");
    
    FacesMessage msg=new FacesMessage("You must add at least one product the the menu");
    
    msg.setSeverity(FacesMessage.SEVERITY_ERROR);
    
    getContext().addMessage(event.getComponent().getClientId(),msg);
    
    getContext().renderResponse();
        
    
    }
    }
    
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void AddMenu(ActionEvent event){
    
    try
    {
        this._menuViewController.createMenu(this.selectedProducts,this.name,this.description,this.price);
       
        /*
        System.out.print("You are Adding Menu");
        
        for(Imagen image:this.fileUpload.getPhotoList()){
        
        imagenFacade.create(image);
            
        }
    
        List<Agrupa>componeList=new <Agrupa>ArrayList();
        
        
         Entities.Menu menu=new Entities.Menu();
        
        menu.setNombre(this.name);
        
        menu.setPrecio(this.price);
        
        
        for(AgrupaMenuProductoInterface agrupaMenuProducto:this.selectedProducts){
        
        System.out.print("Selected Products size "+this.selectedProducts.size());
        
        System.out.print("Product Id "+agrupaMenuProducto.getAgrupa().getAgrupaPK().getProductoIdproducto());
        
        
        }
        
        for(AgrupaMenuProductoInterface agrupaMenuProducto:this.selectedProducts){
        
        AgrupaPK agrupaPK=agrupaMenuProducto.getAgrupa().getAgrupaPK();
         
        agrupaPK.setProductoIdproducto(agrupaMenuProducto.getProductKey().getIdproducto());
        
        agrupaPK.setProductoCategoriaIdCategoria(agrupaMenuProducto.getProductKey().getCategoriaIdcategoria());
        
        agrupaPK.setProductoCategoriaFranquiciaIdFranquicia(agrupaMenuProducto.getProductKey().getCategoriaFranquiciaIdfranquicia());
        
       agrupaMenuProducto.getAgrupa().setAgrupaPK(agrupaPK);
         
      
       
        agrupaMenuProducto.getAgrupa().setProducto(this.productController.find(agrupaMenuProducto.getProductKey()));
        
        System.out.print("Set Producto to Agrupa "+this.productController.find(agrupaMenuProducto.getProductKey()));
        
        agrupaMenuProducto.getAgrupa().setMenu(menu);
        
        componeList.add(agrupaMenuProducto.getAgrupa());
        
        
        
        
        }
        
        //menu.setAgrupaCollection(componeList);
        
        
        menu.setImagenCollection(this.fileUpload.getPhotoList());
       
        menuFacade.create(menu);
        
        for(Agrupa agrupa:componeList){
            
            agrupa.setMenu(menu);
        
            
            
            agrupa.getAgrupaPK().setMenuIdmenu(menu.getIdmenu());
            
            agrupaFacade.create(agrupa);
            
        
        }
        
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
      
      this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
       
      
      FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"Menu Added");
        
      getContext().addMessage(null,msg);
       */ 
    }
    catch(NullPointerException ex){
    
        if(this.getContext()!=null)
        {
        Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
        
        FacesMessage msg=new FacesMessage("You are hacking bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        getContext().renderResponse();
        
    }
    
    }
    
    }
    
    public void deleteMenu(ActionEvent event){
        
        try{
        
     _menuViewController.deleteMenu(this.deleteList);
     
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public String updateProduct(){
try
        {
           switch(this._menuViewController.updateMenu(this.name, this.description, this.price, this.selectedProducts))
           {  
               case APPROVED:{
               
                 return "success";
               
               }
               
               case DISAPPROVED:{
               
                    return "failure";
               
               }
               
                   default:{
    
                       return "failure";
    
                    }
        
                   
           }

          
         
           /*
           if(event.getComponent().getClientId().equals("form:tabView:updateButton") && getSessionAuxiliarComponent()instanceof MenuView){
           
              for(Imagen image:fileUpload.getPhotoList()){
               
                   if(image.getIdimagen()!=0){
                   
                       imagenFacade.edit(image.getIdimagen(),image);
                   
                   }
                   
                   else{
                   
                   imagenFacade.create(image);
                       
                   }
               
               }
               
               
               
               Entities.Menu menu=(Entities.Menu)getSessionAuxiliarComponent();
              
               for(Imagen image:menu.getImagenCollection()){
               
               if(!fileUpload.getPhotoList().contains(image)){
               
                   imagenFacade.remove(image.getIdimagen());
               
               }
               
               }
               
               menu.setImagenCollection(this.fileUpload.getPhotoList());
               
               menu.setNombre(this.name);
               
               menu.setPrecio(this.price);
               
               menuFacade.edit(menu.getIdmenu(),menu);
               
               
               
               for(Agrupa agrupa:menu.getAgrupaCollection()){
               
                   PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+agrupa.getAgrupaPK().getProductoIdproducto()+";productoCategoriaIdCategoria="+agrupa.getAgrupaPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+agrupa.getAgrupaPK().getProductoCategoriaFranquiciaIdFranquicia()+";menuIdmenu="+agrupa.getAgrupaPK().getMenuIdmenu()+"");
                   
                   agrupaFacade.remove(ps);
               
               }
               
               List<Agrupa>collects=new <Agrupa>ArrayList();
               
               
               for(AgrupaMenuProductoInterface agrupaMenuCompone:this.selectedProducts){
                   
                   System.out.print("Product to add agrupa List "+this.productController.find(agrupaMenuCompone.getProductKey()).getNombre());
                   
                   
                   Entities.AgrupaPK agrupaPK=new Entities.AgrupaPK();
                   
                   agrupaPK.setMenuIdmenu(menu.getIdmenu());
                   
                   agrupaPK.setProductoIdproducto(agrupaMenuCompone.getProductKey().getIdproducto());
                   
                   agrupaPK.setProductoCategoriaIdCategoria(agrupaMenuCompone.getProductKey().getCategoriaIdcategoria());
                   
                   agrupaPK.setProductoCategoriaFranquiciaIdFranquicia(agrupaMenuCompone.getProductKey().getCategoriaFranquiciaIdfranquicia());
                   
                   Agrupa agrupa=agrupaMenuCompone.getAgrupa();
                   
                   agrupa.setAgrupaPK(agrupaPK);
                   
                   agrupa.getAgrupaPK().setMenuIdmenu(menu.getIdmenu());
                   
                   agrupa.setProducto(this.productController.find(agrupaMenuCompone.getProductKey()));
                   
                   agrupa.setMenu(menu);
               
               collects.add(agrupa);
                   
               }
               
               for(Agrupa agrupa:collects){
               
              agrupaFacade.create(agrupa);
               
               }
               
           
               FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"Menu Updated Succesfully");
               
               getContext().addMessage(event.getComponent().getClientId(), msg);
           
           }
           
           else{
           
               throw new RuntimeException ();
           
           }
       */
     
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
            return "failure";
            
        }
    
    }
    
    
    
    
    public void setAgrupaPK(Entities.AgrupaPK agrupaPK){
    
        try{
        
            System.out.print("You are setting agrupaPK Product Name "+this.productController.find(this.productKey).getNombre());
            
            
            System.out.print("ProductPK "+this.productKey);
            
            agrupaPK.setProductoIdproducto(this.productKey.getIdproducto());
            
            agrupaPK.setProductoCategoriaIdCategoria(this.productKey.getCategoriaIdcategoria());
            
            agrupaPK.setProductoCategoriaFranquiciaIdFranquicia(this.productKey.getCategoriaFranquiciaIdfranquicia());
        
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
            
            
        }
    
    }
    
   private Entities.ProductoPK productKey;
    
  public Entities.ProductoPK getProductKey(){
  
  return this.productKey;
  
  }  
  
  public void setProductKey(Entities.ProductoPK productKey){
  
      this.productKey=productKey;
  
  }
  
  
  public void deleteImage(javax.faces.event.AjaxBehaviorEvent event){
  
      try{
      
          java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
          
          for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
          
          System.out.print(entry.getKey()+"-"+entry.getValue());
          
          }
      
//          this.fileUpload.removeImage(requestMap.get("deleteImage"));
          
      }
      catch(Exception ex){
      
          Logger.getLogger(MenuView.class.getName()).log(Level.SEVERE,null,ex);
          
          
      
      }
  
  }
}
