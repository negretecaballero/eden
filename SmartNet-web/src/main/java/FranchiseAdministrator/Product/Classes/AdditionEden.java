/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

/**
 *
 * @author luisnegrete
 */
public interface AdditionEden {
  
   Entities.AdditionPK getAdditionPK();
   
   void setAdditionPK(Entities.AdditionPK additionPK);
   
   String getImage();
   
   void setImage(String image);
    
}
