/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

/**
 *
 * @author luisnegrete
 */
public class AdditionEdenImpl implements AdditionEden{
    
    private Entities.AdditionPK additionPK;
    
    private String image;
    
    @Override
    public Entities.AdditionPK getAdditionPK(){
    
        return this.additionPK;
    
    }
    
    @Override
    public void setAdditionPK(Entities.AdditionPK additionPK){
    
    this.additionPK=additionPK;
    
    }
   
    @Override
    public String getImage(){
    
    return this.image;
    
    }
    @Override
    public void setImage(String image){
    
    this.image=image;
    
    }
}
