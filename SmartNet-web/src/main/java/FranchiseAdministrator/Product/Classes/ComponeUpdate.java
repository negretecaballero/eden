/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import Entities.Compone;

/**
 *
 * @author luisnegrete
 */
public class ComponeUpdate implements ComponeUpdateInterface{

    private boolean render;
    
    private Compone compone;
    
    private boolean selection;
    
    @Override
    public boolean getSelection(){
    
    return this.selection;
    }
    
    @Override
    public void setSelection(boolean selection){
    
    this.selection=selection;
    
    }
    
    
    @Override
    public void setRender(boolean render){
    
        this.render=render;
    
    }
    
    
    @Override
    public boolean getRender(){
    
        return this.render;
    
    }
    @Override
    public void setCompone(Compone compone){
    
    this.compone=compone;
    
    }
    
    
    @Override
    public Compone getCompone(){
    
        return this.compone;
    }
    
    
    public ComponeUpdate(){
    compone=new Compone();
    render=false;
    this.selection=false;
    
    }
    
}
