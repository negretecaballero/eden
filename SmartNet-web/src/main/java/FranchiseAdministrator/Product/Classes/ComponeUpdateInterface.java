/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import Entities.Compone;

/**
 *
 * @author luisnegrete
 */
public interface ComponeUpdateInterface {
    
    void setSelection(boolean selection);
    
    boolean getSelection();
    
    void setRender(boolean render);
    
    boolean getRender();
    
    void setCompone(Compone compone);
    
    Compone getCompone();
    
}
