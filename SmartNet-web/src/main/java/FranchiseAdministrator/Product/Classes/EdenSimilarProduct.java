/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisnegrete
 */
public class EdenSimilarProduct implements EdenSimilarProductInterface{
    
    private Entities.SimilarProducts similarProduct;
    
    private String image;
    
    private Entities.ProductoPK  productId;
    
    
    
    @Override
    public Entities.ProductoPK getProductId(){
    
    return this.productId;
    
    }
    @Override
    public void setProductId(Entities.ProductoPK productId){
    
        this.productId=productId;
    
    }
    
    @Override
    public Entities.SimilarProducts getSimilarProduct(){
    
        return this.similarProduct;
    
    }
    
    @Override
    public void setSimilarProduct(Entities.SimilarProducts similarProduct){
    
        this.similarProduct=similarProduct;
    
    }
    
    @Override
    public String getImage(){
    
        return this.image;
    
    }
    @Override
    public void setImage(String image){
    
    this.image=image;
    
    }
    
    @Override
    public boolean findSimilar(Entities.ProductoPK idProduct,java.util.List<EdenSimilarProductInterface>list){
    
    try{
    
      for(EdenSimilarProductInterface similarProduct:list){
      
      if(similarProduct.getProductId().equals(idProduct)){
      
        
          
          return true;
      
      }
      
      }  
    return false;
    }
    catch(Exception ex){
    
        Logger.getLogger(EdenSimilarProduct.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
}
