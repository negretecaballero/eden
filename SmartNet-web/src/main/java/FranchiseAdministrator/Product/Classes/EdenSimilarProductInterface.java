/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

/**
 *
 * @author luisnegrete
 */
public interface EdenSimilarProductInterface {
    
    void setSimilarProduct(Entities.SimilarProducts similarProduct);
    
    Entities.SimilarProducts getSimilarProduct();
    
    String getImage();
    
    void setImage(String image);
    
    Entities.ProductoPK getProductId();
    
    void setProductId(Entities.ProductoPK productId);
    
    boolean findSimilar(Entities.ProductoPK idProduct,java.util.List<EdenSimilarProductInterface>list);
}
