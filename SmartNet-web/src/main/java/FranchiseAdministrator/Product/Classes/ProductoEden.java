/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

/**
 *
 * @author luisnegrete
 */
public class ProductoEden implements ProductoEdenInterface{
  
   private Entities.ProductoPK idProducto;
    
   private String image;
   
   
   @Override
   public Entities.ProductoPK getIdProducto(){
   
   return this.idProducto;
   
   }
   
   @Override
   public void setIdProducto(Entities.ProductoPK idProducto){
   
       this.idProducto=idProducto;
   
   }
   
   @Override
   public String getImage(){
   
       return this.image;
   
   }
   
   @Override
   public void setImage(String image){
   
       this.image=image;
   
   }
    
}
