/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import Entities.Inventario;
import Entities.InventarioPK;

/**
 *
 * @author luisnegrete
 */
public class ProductoInventarioCompone implements ProductoInventarioComponeInterface{
   private InventarioPK inventarioPK;
   
   
   
   private double quantity;
   
   public ProductoInventarioCompone(){
   this.inventarioPK=new InventarioPK();
   }
  
   @Override
   public double getQuantity(){
   return this.quantity;
   }
   
   
   @Override
   public InventarioPK getInventarioPK(){
   
   return this.inventarioPK;
   }
 
   @Override
   public void setQuantity(double quantity){
   this.quantity=quantity;
   }
   
   @Override
   public void setInventarioPK(InventarioPK inventarioPK){
   this.inventarioPK=inventarioPK;
   }
   
   public static boolean findSimilar(String name,Inventario invList){
  
       boolean coincidence=false;
       
       
       
       if(invList.getNombre().equals(name)){
       
       coincidence=true;
       
       System.out.print("There is a name coincidence");
       }
       
       
       return coincidence;
       
       
   }
   
}
