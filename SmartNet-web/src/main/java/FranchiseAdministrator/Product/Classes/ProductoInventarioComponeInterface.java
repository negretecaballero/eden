/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import Entities.InventarioPK;

/**
 *
 * @author luisnegrete
 */
public interface ProductoInventarioComponeInterface {
   
    double getQuantity();
    
    InventarioPK getInventarioPK();
    
    void setQuantity(double quantity);
    
    void setInventarioPK(InventarioPK inventarioPK);
    
    
}
