/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class SimilarProductsLayout implements SimilarProductsLayoutInterface{
    
    private Entities.Producto product;
    
   private String type;
    
    private double price;
    
    private List<String>productImages;
    
    @Override
    public void setProductImages(List<String>productImages){
    
    this.productImages=productImages;
    
    }
    @Override
    public List<String>getProductImages(){
    
    return this.productImages;
    }
    
    @Override
    public Entities.Producto getProduct(){
    
        return this.product;
    
    }
    
    @Override
    public void setProduct(Entities.Producto product){
    
    this.product=product;
    
    }
    
    @Override
    public String getType(){
    
    return this.type;
    
    }
    
    @Override
    public void setType(String type){
    
    this.type=type;
    
    }
    
    @Override
    public double getPrice(){
    
    return this.price;
    
    }
    
    @Override
    public void setPrice(double price){
    
    this.price=price;
    
    }
    
    public SimilarProductsLayout(){
    
        this.productImages=new <String>ArrayList();
    
    }
    
}
