/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Classes;

import java.util.List;

/**
 *
 * @author luisnegrete
 */
public interface SimilarProductsLayoutInterface {
    
    void setProduct(Entities.Producto product);
    
    Entities.Producto getProduct();
    
    double getPrice();
    
    void setPrice(double price);
    
    String getType();
    
    void setType(String type);
    
    void setProductImages(List<String>productImages);
    
    List<String>getProductImages();
    
}
