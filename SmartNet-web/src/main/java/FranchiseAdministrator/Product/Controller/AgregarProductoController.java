/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Controller;

/**
 *
 * @author luisnegrete
 */
public interface AgregarProductoController {
    
    void initFileUpload(String view);
    
    void loadProductsImages();
    
    void initAdditions();
    
    java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>ProductsList();
    
    void addUndoInventory(String action);
    
    void dragAddition();
    
    void deleteSimilarProduct(Entities.ProductoPK key);
    
    void deleteAddition(Entities.AdditionPK key);
    
    void handleUpload(org.primefaces.event.FileUploadEvent event);
    
    void loadImagesGalleria();
    
    void  AddProduct(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>additionList,String barcode,String description,String name, double price, Entities.CategoriaPK categoryPK,java.util.List<FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface>productInventarioComponeList,java.util.List<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface>similarPRoducts);
    
    boolean checkExistence(String name);
    
    java.util.Vector<Entities.Categoria>getCategories();
    
    java.util.Vector<Entities.Producto>productList(Entities.CategoriaPK key);
    
    void deleteProduct(Entities.ProductoPK productPK);
    
    void initUpdateProduct(String view);
    
    void updateBarcode(org.primefaces.event.FileUploadEvent event);
    
     void deleteSimilarProductList(FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similar);
     
     void addSimilarProductList(Entities.ProductoPK key);
     
     java.util.List<Entities.Producto>getProductList();
     
     java.util.List<Entities.Inventario>getInventoryUpdateList();
     
     void addToComponeUpdateList();
     
     void deleteComponeUpdateList();
    
     boolean checkNameExistenceUpdateProduct(String productName);
     
     ENUM.TransactionStatus updateProduct(String name, double price,String description,java.util.List<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>similarList,org.primefaces.model.LazyDataModel<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>dataModel);

	/**
	 * 
	 * @param viewId
	 */
	void initPreRenderView(String viewId);
     
}
