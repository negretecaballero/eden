/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Controller;

import Entities.ProductoPK;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import FrontEndModel.LazyProductDataModel;
import LocalClases.ComponeListModel;
import java.io.File;
import javax.enterprise.inject.spi.BeanManager;
import javax.faces.application.FacesMessage;
import org.apache.commons.collections.IteratorUtils;


/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class AgregarProductoControllerImplementation implements AgregarProductoController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ProductoControllerInterface _productController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.AdditionControllerDelegate _additionController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.InventarioDelegate _inventoryController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.CategoryControllerInterface _categoryController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.BarcodeController _barcodeController;
 
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SimilarProductsController _similarProductsController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ComponeController _componeController;
    
    @Override
    public void initFileUpload(String view){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(view)){
            
                _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
            
                _fileUpload.setActionType(new Clases.ActionType(view));
                
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
   
    
    @Override
    public boolean checkExistence(String name){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
        Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
         if(this._productController.findByName(franchise.getIdfranquicia(), name)!=null){
      
             return true;
      
      }
        
        
        }
        
        
     
        
    return false;
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
        
    }
    
    }
    
    @Override
     public void loadProductsImages(){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            java.io.File file=new java.io.File(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+"products");
    
            
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia && !file.exists()){
            
           Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
           
           
            
         for(Entities.Producto product:_productController.findByFranchise(franchise.getIdfranquicia())){
         
               if(product!=null){
            
         
           file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products");
      
           if(!file.exists() || !file.isDirectory())
           {
           
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, "products");
           
           }
           
           java.io.File file2=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
           
           if(!file2.exists() || !file2.isDirectory())
           {
           
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator,product.getNombre());
           
           }
           
           if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
               this._fileUpload.loadImagesServletContext(((java.util.List<Entities.Imagen>)product.getImagenCollection()).get(0), session.getAttribute("username").toString()+java.io.File.separator+"products"+java.io.File.separator+product.getNombre());
           
           }
           
           }
         
         }

         this._fileUpload.getImages().clear();
         
         this._fileUpload.getPhotoList().clear();
         
            }
         
        }
        catch(NullPointerException ex){
        
        java.util.logging.Logger.getLogger(AgregarProductoController.class.getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
        }
    
    }
     
     @Override
     public void initAdditions(){
     
         try{
         
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
             
             if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
             
                 Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                 
                 this._additionController.loadImages(franchise.getIdfranquicia());
             
             }
         
         }
         catch(Exception | StackOverflowError ex){
         
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         }
     
     }
     
     public java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>ProductsList(){
     
         try{
         
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
         
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
             
             if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
             
                 Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                 
                 Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
             
                 java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>products=new java.util.ArrayList<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>();
                 
                 for(Entities.Producto product:_productController.findByFranchise(franchise.getIdfranquicia())){
           
           FranchiseAdministrator.Product.Classes.ProductoEdenInterface productEden=new FranchiseAdministrator.Product.Classes.ProductoEden();
           
           productEden.setIdProducto(product.getProductoPK());
           
           String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+"products"+File.separator+product.getNombre();
           if(filesManagement.getFiles(path)!=null)
           {
           if(filesManagement.getFiles(path).length>0){
           
           System.out.print("Paths greatren than 0");
               
           productEden.setImage(File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+"products"+File.separator+product.getNombre()+File.separator+filesManagement.getFiles(path)[0].getName());
           }
           else{
               
               System.out.print("Path lower than 0");
           
               productEden.setImage("/images/noImage.png");
           }
           }
           
           products.add(productEden);
           }
               
                 return products;
                 
             }
             return null;
         }
         catch(Exception ex){
         
             return null;
         
         }
     
     }
     
     
     
     @Override
     public void addUndoInventory(String action){
     
         try{
         
             
             javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
             
             if(resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto") !=null)
             {
                 FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
                 
             switch(action){
             
                 case "agregar":{
                 
                     if( agregarProducto.getCantInv()>0)
                     {
                     
    java.util.Map<String,String> params = javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
   
    
    System.out.print("INVENTORY QUANTITY "+agregarProducto.getCantInv());
    
   FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface aux=new FranchiseAdministrator.Product.Classes.ProductoInventarioCompone();
   
   aux.setQuantity(agregarProducto.getCantInv());
   
   
   String key[]=params.get("form:tabView:inventario_input").split(",");
   
   
   agregarProducto.getId_inv().setFranquiciaIdFranquicia(Integer.parseInt(key[0]));
   
   agregarProducto.getId_inv().setIdinventario(Integer.parseInt(key[1]));
   
   Entities.InventarioPK invKey=new Entities.InventarioPK();
   
   invKey.setFranquiciaIdFranquicia(agregarProducto.getId_inv().getFranquiciaIdFranquicia());
   
   invKey.setIdinventario(agregarProducto.getId_inv().getIdinventario());
   
   aux.setInventarioPK(invKey);
  
   
   if(agregarProducto.getInvList()!=null){
   
       System.out.print("BEFORE ADDING INV SIZE "+agregarProducto.getInvList().size());
       
       for(FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface auxi:agregarProducto.getInvList()){
       
           System.out.print(this._inventoryController.find(auxi.getInventarioPK()).getNombre());
       
       }
   
   }
   
   if(!findSimilar(agregarProducto.getInvList(),aux.getInventarioPK())){
   
   agregarProducto.getInvList().add(aux);
   
   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Succesfully added to the list");
   
   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
   
   javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
   
   }
   
   else{
   
       javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+"This Inventory is already in the list");
   
       javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
       
   }

       if(agregarProducto.getInvList()!=null){
                     
         System.out.print("INVENTORY LIST SIZE "+agregarProducto.getInvList().size());
                         
            for(FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface auxi:agregarProducto.getInvList()){
                         
                System.out.print("INVENTORY NAME "+this._inventoryController.find(auxi.getInventarioPK()).getNombre());
                         
            }
                     
        }
   
                 }
                     else{
                     
                         javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("0 is not a valid quantity");
                         
                         msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                         
                         javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                     
                     }
                     
                 break;
                 }
                 
                 case "deshacer":{
                     
                 FacesMessage msg;
     
         if(agregarProducto.getInvList().size()>0){
         
             agregarProducto.getInvList().remove((agregarProducto.getInvList().size())-1);
             
             System.out.print("Removed from the list");
             
             
             msg=new FacesMessage("Succesfully removed from the list");
             
             msg.setSeverity(FacesMessage.SEVERITY_INFO);
             
             javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
         
         }
         
         else{
         
             msg=new FacesMessage("There are no items to remove");
             
             msg.setSeverity(FacesMessage.SEVERITY_INFO);
             
             javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
         }    
                 
                 break;
                 }
                 
                 case "eliminar":{
                 
                     
         agregarProducto.getInvList().clear();
         
         System.out.print(agregarProducto.getInvList().size());
         
         
         FacesMessage msg=new FacesMessage("List cleared");
         
         msg.setSeverity(FacesMessage.SEVERITY_INFO);
         
         javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                     
                 break;
                 }
             
             }
             
         }
             
             else{
               
                     throw new IllegalArgumentException("");
                     
                }
         
         }
         catch(IllegalArgumentException | StackOverflowError | NullPointerException ex){
         
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         }
     
     }
    
     
     public boolean findSimilar(java.util.List<FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface>inventoryList,Entities.InventarioPK key){
     
         try{
         
             for(FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface inventory:inventoryList){
             
                 if(inventory.getInventarioPK().equals(key)){
                 
                     return true;
                 
                 }
             
             }
             
             return false;
         }
         catch(Exception ex){
         
             
             
             return false;
         
         }
     
     }
 
     
     @Override
     public void dragAddition(){
     
         try{
         
            javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
         
            if(request.getParameter("idAddition")!=null){
            
                  int idAddition=Integer.parseInt(request.getParameter("idAddition"));
                  
                  this.addAddition(idAddition);
            
            }

         }
         catch(NumberFormatException | NullPointerException ex){
         
             java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         }
     
     }
     
     
      private void addAddition(int idAddition){
   
       try{
       
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
           FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
           
           
           if(session.getAttribute("selectedOption") instanceof Entities.Franquicia | agregarProducto!=null){
  
           Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
           
           Entities.AdditionPK additionPK=new Entities.AdditionPK();
           
           additionPK.setIdaddition(idAddition);
           
           additionPK.setFranquiciaIdfranquicia(franchise.getIdfranquicia());
           
           if(!this.checkxistance(agregarProducto.getDragAdditionList(), additionPK))
           
           {
           
           FranchiseAdministrator.Product.Classes.AdditionEden additionEden=new FranchiseAdministrator.Product.Classes.AdditionEdenImpl();
           
           additionEden.setAdditionPK(additionPK);
           
           additionEden.setImage(this._additionController.getImage(additionPK));
           
           agregarProducto.getDragAdditionList().add(additionEden);
        
           }
           
           else{
           
               javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage("This addition has been added already");
           
               message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
               
               javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, message);
           }
           
           }
           
           else{
           
               throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
           
           }
           
       
       }
       catch(Exception ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
     
      
      private boolean checkxistance(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>list,Entities.AdditionPK additionPK){
      
          try{
          
              if(list!=null && !list.isEmpty()){
              
              for(FranchiseAdministrator.Product.Classes.AdditionEden aux:list){
              
                  if(aux.getAdditionPK().equals(additionPK)){
                  
                      return true;
                  
                  }
              
              }
              
              }
              
              return false;
          }
          catch(Exception ex){
          
              return false;
          
          }
      
      }
   
      @Override
      public void deleteSimilarProduct(Entities.ProductoPK key){
      
          try{
          
              javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
              
              FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
          
              if(agregarProducto!=null){
              
                  if(agregarProducto.getSimilarProducts()!=null && !agregarProducto.getSimilarProducts().isEmpty()){
           
             
               
               for(int i=0;i<agregarProducto.getSimilarProducts().size();i++){
               
               if(agregarProducto.getSimilarProducts().get(i).getProductId().equals(key)){
               
                   agregarProducto.getSimilarProducts().remove(i);
                   
                   break;
               
               }
               
               }
           
               
               if(agregarProducto.getSimilarProducts().isEmpty()){
               
                System.out.print("Drag render is true");
           
               agregarProducto.setDragRender(true);
           
           
               
               }
               else{
               
                   agregarProducto.setDragRender(false);
                   
                   System.out.print("Drag Render is false");
               
               }
           }
          
                  javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                  
           
           session.setAttribute("auxiliarComponent",agregarProducto.getDragRender());
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:similars");
       
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:draggingHide");
              
              }
              
          }
          catch(Exception | StackOverflowError ex){
          
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
              
          }
      
      }
    
      @Override
      public void deleteAddition(Entities.AdditionPK key){
      
          try{
          
              javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
              
              FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
          
              if(agregarProducto!=null){
              
                  
                  
                  if(agregarProducto.getDragAdditionList()!=null && !agregarProducto.getDragAdditionList().isEmpty())
                  {
                    for(FranchiseAdministrator.Product.Classes.AdditionEden addition:agregarProducto.getDragAdditionList()){
       
                     if(addition.getAdditionPK().equals(key)){
           
                         System.out.print("ADDITION TO REMOVE "+this._additionController.find(key).getName());
                         
                            agregarProducto.getDragAdditionList().remove(addition);
               
                            break;
           
                     }
            
                    }
                    
              }
              
              }
              
          }
          catch(Exception ex){
          
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
          }
      
      }
      
      @Override
      public void handleUpload(org.primefaces.event.FileUploadEvent event){
      
          try{
          
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
              
              _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString(), 500, 500);
          
          }
          catch(Exception  | StackOverflowError ex){
          
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
          }
      
      }
     
      @Override
      public void loadImagesGalleria(){
      
          try{
          
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
          
             javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
             
             if(session.getAttribute("username")!=null){
             
             this._fileUpload.forceLoadImages(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
             
             
             
             }
          }
          catch(Exception | StackOverflowError ex){
                  
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
                  
                  }
      
      }
      
      @Override
     
      public void AddProduct(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>additionList,String barcode,String description,String name, double price, Entities.CategoriaPK categoryPK,java.util.List<FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface>productInventarioComponeList,java.util.List<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface>similarList){
      
          try{
          
              
              System.out.print("CREATING NEW PRODUCT");
              
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
              
              if(session.getAttribute("selectedOption")instanceof Entities.Franquicia)
              {
              Entities.Producto product=new Entities.Producto();
              
              Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
              
              product.setImagenCollection(this._fileUpload.getPhotoList());
              
              if(additionList!=null && !additionList.isEmpty())
              
              {
              java.util.List<Entities.Addition>additions=new java.util.ArrayList<Entities.Addition>();
              
              for(FranchiseAdministrator.Product.Classes.AdditionEden additionEden:additionList){
              
                  additions.add(this._additionController.find(additionEden.getAdditionPK()));
              
              }
              
              product.setAdditionCollection(additions);
              
              for(Entities.Addition addition:product.getAdditionCollection()){
              
                  System.out.print("ADDITION ADD PRODUCT "+addition+" ADDITION PK "+addition.getAdditionPK());
              
              }
              
              }
              
              if(!barcode.equals(""))
              {
              Entities.Barcode bar=new Entities.Barcode();
              
              if(this._productController.findByBarcode(barcode)!=null){
              
                  bar=_barcodeController.getBarcodeByCode(barcode);
              
              }
              
              
              else{
              bar.setBarcode(barcode);
              
              this._barcodeController.createBarcode(bar);
              
              }
              
              product.setBarcode(bar);
              }
              
              
              
              product.setDescription(description);
              
              product.setNombre(name);
              
              product.setPrecio(price);
              
              product.setCategoria(_categoryController.find(categoryPK));
              
              Entities.ProductoPK productKey=new Entities.ProductoPK();
              
              productKey.setCategoriaIdcategoria(_categoryController.find(categoryPK).getCategoriaPK().getIdcategoria());
              
              productKey.setCategoriaFranquiciaIdfranquicia(franchise.getIdfranquicia());
              
              product.setProductoPK(productKey);
              
              java.util.List<Entities.Compone>productInventarioCompone=new java.util.ArrayList<Entities.Compone>();
              
              if(productInventarioComponeList!=null && !productInventarioComponeList.isEmpty()){
              
              for(FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface productoInventarioCompone:productInventarioComponeList){
              
                  Entities.Compone compone=new Entities.Compone();
                  
                  compone.setCantidadInventario(productoInventarioCompone.getQuantity());
                  
                  compone.setInventario(this._inventoryController.find(productoInventarioCompone.getInventarioPK()));
                  
                  compone.setProducto(product);
                  
                  Entities.ComponePK componePK=new Entities.ComponePK();
                  
                  componePK.setInventarioFranquiciaIdFranquicia(franchise.getIdfranquicia());
                  
                  componePK.setInventarioIdinventario(this._inventoryController.find(productoInventarioCompone.getInventarioPK()).getInventarioPK().getIdinventario());
              
                  componePK.setProductoCategoriaFranquiciaIdFranquicia(product.getCategoria().getCategoriaPK().getFranquiciaIdfranquicia());
                  
                  componePK.setProductoCategoriaIdCategoria(product.getCategoria().getCategoriaPK().getIdcategoria());
                  
                  compone.setComponePK(componePK);
                  
                  productInventarioCompone.add(compone);
                  
              }
              
              product.setComponeCollection(productInventarioCompone);
              
              }
              
              java.util.List<Entities.SimilarProducts>similarProductList=new java.util.ArrayList<Entities.SimilarProducts>();
              
              if(similarList!=null && !similarList.isEmpty())
              {
              for(FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface similar:similarList){
              
                  Entities.SimilarProductsPK similarProductsPK=new Entities.SimilarProductsPK();
     
     
     similarProductsPK.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
     
     similarProductsPK.setProductoCategoriaFranquciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
     
     similarProductsPK.setProductoIdproducto1(similar.getProductId().getIdproducto());

     similarProductsPK.setProductoCategoriaIdCategoria1(similar.getProductId().getCategoriaIdcategoria());
     
     similarProductsPK.setProductoCategoriaFranquiciaIdFranquicia1(similar.getProductId().getCategoriaFranquiciaIdfranquicia());
     
     similar.getSimilarProduct().setSimilarProductsPK(similarProductsPK);
     
     similar.getSimilarProduct().setProducto1(_productController.find(similar.getProductId()));
     
     similarProductList.add(similar.getSimilarProduct());
     
              }
              
              product.setSimilarProductsCollection(similarProductList);
              
              }
              
              this._productController.createProduct(product);
              
              java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
              
              javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+bundle.getString("franchiseAdministratorAddProduct.ProductAdded"));
              
              javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, message);
              
          }
          
          }
          catch(IllegalArgumentException | NullPointerException | StackOverflowError ex){
          
              java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
          }
      
      }
      
      @Override
      public java.util.Vector<Entities.Categoria>getCategories(){
      
      try{
      
          javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
          
          if(session.getAttribute("selectedOption")instanceof Entities.Franquicia ){
          
          Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
          
          java.util.Vector<Entities.Categoria>categories=new java.util.Vector<Entities.Categoria>(franchise.getCategoriaCollection().size());
          
          for(Entities.Categoria category:franchise.getCategoriaCollection()){
          
              categories.add(category);
          
          }
          
          return categories;
          
          }
          
      return null;
      }
      catch(Exception | StackOverflowError ex){
      
          java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
      return null;
      }
      
      }
      
      @Override
      public java.util.Vector<Entities.Producto>productList(Entities.CategoriaPK key){
      
      try{
          
          if(_fileUpload.getActionType()==null || !this._fileUpload.getActionType().getActionType().equals(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId())){
          
              _fileUpload.setActionType(new Clases.ActionType(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId()));
          
          }
      
          Entities.Categoria category=this._categoryController.find(key);
          
          java.util.Vector<Entities.Producto>products=null;
          
          if(category!=null)
          {
         products=new java.util.Vector<Entities.Producto>(category.getProductoCollection().size());
          
          for(Entities.Producto product:category.getProductoCollection()){
          
              products.add(product);
          
          }
          
      }
          else{
          
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
          
              if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
              
                  Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
              
                  if(franchise.getCategoriaCollection()!=null && !franchise.getCategoriaCollection().isEmpty()){
                      
                  Entities.Categoria categoria=((java.util.List<Entities.Categoria>)franchise.getCategoriaCollection()).get(0);
                  
                  products=new java.util.Vector<Entities.Producto>(categoria.getProductoCollection().size());
                  
                  for(Entities.Producto product:categoria.getProductoCollection()){
                  
                      products.add(product);
                  
                  }
                  
                  }
                  
              }
              
          }
          
          return products;
          
      }
      catch(Exception ex){
      
          java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
          return null;
      
      }
      
      }
 
      @Override
   public void deleteProduct(Entities.ProductoPK productPK){
   
       try{
       
           System.out.print("PRODUCT TO DELETE "+productPK);
           
        this._productController.deleteFromFranchise(productPK);
        
        java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
       
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+bundle.getString("franchiseAdministratorAddProduct.ProductDeleted"));
        
        javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
        
       }
       catch(Exception | StackOverflowError ex){
       
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       }
   
   }
   
   public void updateLazyModel(){
   
       try{
       
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
          FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
       
          if(agregarProducto!=null){
          
              org.primefaces.model.LazyDataModel<Entities.Producto>lazyModel=new FrontEndModel.LazyProductDataModel(this.productList(agregarProducto.getId_Categoria()));
              
              agregarProducto.setProductList(lazyModel);
          
          }
          
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
   
   @Override
 
   public void updateBarcode(org.primefaces.event.FileUploadEvent event){
   
       try{
       
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
             java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"barcode");
       
             Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
             
       if(file.exists()){
       
           filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"barcode");
       
       }
       else{
       
           filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator,"barcode");
       
       }
  
       _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString()+java.io.File.separator+"barcode"+java.io.File.separator, 500, 500);
    
       
       String aux=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
           
       _fileUpload.getImages().remove(_fileUpload.getImages().size()-1);
       
       _fileUpload.getPhotoList().remove(_fileUpload.getPhotoList().size()-1);
       
       javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
       
       FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"agregarProducto");
       
       Clases.Barcode barcode=new Clases.BarcodeImplementation();
       
       if(agregarProducto!=null){
       
           agregarProducto.setBarcodeImage(aux);
           
           agregarProducto.setBarcode(barcode.getCode(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+"barcode", event.getFile().getFileName()));
       
           
       if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"addproduct.xhtml") || javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"update-product.xhtml")){
       
           if(_productController.findByBarcode(agregarProducto.getBarcode())!=null)
           {          
               Entities.Producto product=_productController.findByBarcode(agregarProducto.getBarcode());
           
               System.out.print("PRODUCT FOUND "+product.getNombre());
               
              // _businessProduct.setNombre(product.getNombre());
               
               //_businessProduct.setPrecio(product.getPrecio());
               
              // _businessProduct.setDescription(product.getDescription());
               
              // _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+session.getAttribute("username").toString());
               
               //this.agregarProducto.loadImagesBarcode(this.barcode);
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:nombre");
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:price");
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:description");
           
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:galleria");
               
           }
           else{
           
             //  System.out.print("No product found with this barcode "+this.barcode);
           
           }
       }
       
       
       if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"addproduct.xhtml") || javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"update-product.xhtml")){
       
          // this._businessProduct.getBarcode().setBarcode(this.barcode);
       }
       
       if(javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"AgregarProducto.xhtml") || javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"updateProduct.xhtml")){
       
           if(_productController.findByBarcode(agregarProducto.getBarcode())!=null){
           
           Entities.Producto product=_productController.findByBarcode(agregarProducto.getBarcode());
           
           agregarProducto.setNombre(product.getNombre());
           
           agregarProducto.setPrecio(product.getPrecio());
           
           agregarProducto.setDescription(product.getDescription());
           
          // _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+session.getAttribute("username").toString());
           
          // agregarProducto.loadImagesBarcode(this.barcode);
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:nombre");
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:precio");
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:description");
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:photo");
           
           
           }
       
       }
           
       }
       
       
       
      
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
      
       }
   
   }
   
   private SessionClasses.EdenList<String>loadSimilarProductImages(Entities.Producto product){
   
       try{
       
           
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
           _fileUpload.getPhotoList().clear();
           
           _fileUpload.getImages().clear();
           
           SessionClasses.EdenList<String>results=new SessionClasses.EdenList<String>();
           
           java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
           
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
           if(file.exists()){
           
               filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
               
           }
           else{
           
               filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, product.getNombre());
           
           }
           
          for(Entities.Imagen image:product.getImagenCollection()){
          
              _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+product.getNombre());
          
          }
           
          for(String string:_fileUpload.getImages()){
          
              String aux=string;
              
              results.addItem(aux);
          
          }
          
          _fileUpload.getPhotoList().clear();
          
          _fileUpload.getImages().clear();
           
          return results;
       
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       return null;
       }
   
   }
   
   @annotations.MethodAnnotations(author="Luis Negrete",date="",comments="Only for Restaurants")
   private SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>getSimilarProductList(){
   
       try{
       
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
           
               SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>results=new SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>();
           
               Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
               
               //Similar Products
               if(product.getSimilarProductsCollection()!=null && !product.getSimilarProductsCollection().isEmpty()){
               
                   for(Entities.SimilarProducts similar:product.getSimilarProductsCollection()){
                   
                       FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface aux=new FranchiseAdministrator.Product.Classes.SimilarProductsLayout();
                       
                       aux.setPrice(similar.getPriceDiference());
                       
                       aux.setType("producto");
                       
                       aux.setProduct(similar.getProducto1());
                       
                       if(aux.getProduct().getImagenCollection()!=null && !aux.getProduct().getImagenCollection().isEmpty()){
                       
                       aux.setProductImages(IteratorUtils.toList(this.loadSimilarProductImages(aux.getProduct()).iterator()));
                       
                       }
                       else{
                       
                           java.util.List<String>images=new java.util.ArrayList<String>();
                       
                           images.add("/images/noImage.png");
                           
                           aux.setProductImages(images);
                           
                       }
                       
                       
                       results.addItem(aux);
                   
                   }
                   
                   
                   //Similar Products 1
                   
                   if(product.getSimilarProductsCollection1()!=null && !product.getSimilarProductsCollection1().isEmpty())
                   {
                   
                     for(Entities.SimilarProducts similar:product.getSimilarProductsCollection1()){
                   
                       FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface aux=new FranchiseAdministrator.Product.Classes.SimilarProductsLayout();
                       
                       aux.setPrice(similar.getPriceDiference()*-1);
                       
                       aux.setType("producto1");
                       
                       aux.setProduct(similar.getProducto());
                       
                       if(aux.getProduct().getImagenCollection()!=null && !aux.getProduct().getImagenCollection().isEmpty()){
                       
                       aux.setProductImages(IteratorUtils.toList(this.loadSimilarProductImages(aux.getProduct()).iterator()));
                       
                       }
                       else{
                       
                           java.util.List<String>images=new java.util.ArrayList<String>();
                       
                           images.add("/images/noImage.png");
                           
                           aux.setProductImages(images);
                           
                       }
                       
                       
                       results.addItem(aux);
                   
                   }
                   
                       
                       
                       
                       
                   }
               
               }
               
               //similarProduct1
                if(product.getSimilarProductsCollection1()!=null && !product.getSimilarProductsCollection1().isEmpty()){
               
                   for(Entities.SimilarProducts similar:product.getSimilarProductsCollection1()){
                   
                   
                       FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface aux=new FranchiseAdministrator.Product.Classes.SimilarProductsLayout();
                       
                       aux.setProduct(similar.getProducto());
                       
                       aux.setPrice(similar.getPriceDiference()*-1);
                       
                       aux.setType("producto1");
                       
                       if(aux.getProduct().getImagenCollection()!=null && !aux.getProduct().getImagenCollection().isEmpty()){
                       
                       aux.setProductImages(IteratorUtils.toList(this.loadSimilarProductImages(aux.getProduct()).iterator()));
                       
                       }
                       else{
                       
                       java.util.List<String>images=new java.util.ArrayList<String>();
                       
                       images.add("/images/noImage.png");
                       
                       aux.setProductImages(images);
                       
                       }
                       
                       results.addItem(aux);
                       
                   }
               
               }
               
               
               return results;
           }
           return null;
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       return null;
       }
   
   }
   
   private final Entities.ProductoPK forceSimilarListKey(){
   
       try{
       
       
          
              if(getProductList()!=null && !getProductList().isEmpty()){
              
               return getProductList().get(0).getProductoPK();
              
              }
              
          
          
       return null;
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       return null;
       }
   
   }
      
   @Override
   public void initUpdateProduct(String view){
   
       try{
       
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           
           
           if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
           
               javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
               
               FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
           
               javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
               
              if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(view)){
                  
                  _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
               
               if(agregarProducto!=null){
               
                     
                   
                  _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
               
                          
                   Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
                   
                   System.out.print("UPDATE PRODUCT "+product.getNombre());
                   
                   if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                   
                       System.out.print("PRODUCT IMAGES "+product.getImagenCollection().size());
                   
                   }
                   
                   agregarProducto.setNombre(product.getNombre());
                   
                   agregarProducto.setPrecio(product.getPrecio());
         
               
                   agregarProducto.setDescription(product.getDescription());
                   
                   agregarProducto.setSimilarList(IteratorUtils.toList(this.getSimilarProductList().iterator()));
                   
                    //Load Images
            
                if(product.getImagenCollection()!=null && !product.getImagenCollection().isEmpty()){
                       
                    System.out.print("THIS PRODUCT HAS "+product.getImagenCollection().size()+" IMAGES");
                    
                       for(Entities.Imagen image:product.getImagenCollection()){
                       
                           _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString());
                       
                       }
                   
                }
                
                if(product.getComponeCollection()!=null && !product.getComponeCollection().isEmpty())
                {
                    agregarProducto.setComponeListModel(new ComponeListModel(IteratorUtils.toList(this.getComponeUpdateList().iterator())));
  
                } 
                 
               
               }  
              
                 _fileUpload.setActionType(new Clases.ActionType(view));
                      
               }
               
    else{
               
                   System.out.print("ACTION TYPE IS THE SAME");
               
               }
               
           
       
       }
           
       }
       catch(NullPointerException | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
   
   private SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>getComponeUpdateList(){
   
   try{
   
       javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
       if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
       
           Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
           
           if(product.getComponeCollection()!=null && !product.getComponeCollection().isEmpty()){
           
               System.out.print("PRODUCT COMPONE LIST IS NOT NULL");
               
               SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>results=new SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>();
               
               for(Entities.Compone compone:product.getComponeCollection()){
               
               FranchiseAdministrator.Product.Classes.ComponeUpdateInterface componeUpdate=new FranchiseAdministrator.Product.Classes.ComponeUpdate();
               
               componeUpdate.setCompone(compone);

               results.addItem(componeUpdate);
               
               }
               
               return results;
           
           }
           
       }
   return null;
   }
   catch(Exception | StackOverflowError ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       return null;
   
   }
   
   }

   
    @Override
    public void deleteSimilarProductList(SimilarProductsLayoutInterface similar) {
        
        try{
     
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"agregarProducto");
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("username")!=null && agregarProducto!=null){
        
           if(agregarProducto.getSimilarList()!=null && !agregarProducto.getSimilarList().isEmpty()){
           
                Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                
                java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+similar.getProduct().getNombre());
           
                if(file.exists()){
                
                    filesManagement.deleteFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+similar.getProduct().getNombre());
                
                }
                
                agregarProducto.getSimilarList().remove(similar);
           }
        
        }
        
    }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    }
    
    private  java.util.List<Entities.Producto> removeProductFromList(java.util.List<Entities.Producto>list,Entities.Producto product){
    
        if(list!=null && !list.isEmpty()){
        
            for(int i=0;i<list.size();i++){
            
                if(list.get(i).getProductoPK().equals(product.getProductoPK())){
                
                    System.out.print("PRODUCT FOUND IN LIST "+list.get(i).getNombre());
                    
                    list.remove(i);
                    
                    return list;

                }
                
                
            }
        
        }
    return list;
    }
    
    @Override
    public java.util.List<Entities.Producto>getProductList(){

        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
            
            if(this._productController.getAutoFranchiseProducts()!=null && !this._productController.getAutoFranchiseProducts().isEmpty()){
            
                java.util.List<Entities.Producto>results=_productController.getAutoFranchiseProducts();
   
             //  results= removeProductFromList(results,(Entities.Producto)session.getAttribute("auxiliarComponent"));
               
                  
                   for(Entities.Producto product:results){
                
                    System.out.print(product.getNombre());
                
                }
                
                
                return results;
            
            }
            
            }
            
            return null;
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }

    }
    
    @Override
    public java.util.List<Entities.Inventario>getInventoryUpdateList(){
    
    try{
    
      javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
       
      if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
      
          Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
          
          return this._inventoryController.findByFranchise(franchise.getIdfranquicia());
      
      }
        
        return null;
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    return null;
    }
    
    }
    
    
    
    private final boolean checkExistenceSimilarProduct(Entities.ProductoPK key){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
            
                Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
            
                if(product.getProductoPK().equals(key)){
                
                    return true;
                
                }
                
            }
            
            if(agregarProducto!=null){
            
            if(agregarProducto.getSimilarList()!=null && !agregarProducto.getSimilarList().isEmpty()){
            
                for(FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similar:agregarProducto.getSimilarList()){
                
                    if(similar.getProduct().getProductoPK().equals(key)){
                    
                        return true;
                    
                    }
                
                }
            
            }
            
            }
            
            return false;
        }

        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        }
        
    }
    
    private final boolean checkComponeUpdateList(SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>componeList){
    
        try{
        
              System.out.print("COMPONE UPDATE LIST SIZE TO CHECK "+componeList.size());
            
           if(componeList!=null && !componeList.isEmpty()){
           
             
               
               for(FranchiseAdministrator.Product.Classes.ComponeUpdateInterface aux:componeList){
  
                   SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>subList=componeList;
                   
                   subList.remove(aux);
               
                   if(!subList.isEmpty()){
                   
                       System.out.print("AUX PK VALUE "+aux.getCompone().getInventario());
                       
                       for(FranchiseAdministrator.Product.Classes.ComponeUpdateInterface auxi:subList){
                       
                                 System.out.print("AUXI PK VALUE "+auxi.getCompone().getInventario());
                           
                           if(aux.getCompone().getInventario().getInventarioPK().equals(auxi.getCompone().getInventario().getInventarioPK())){
                           
                               System.out.print("COINCIDENCE FOUND");
                               
                               return true;
                           
                           }
                       
                       }
                   
                   }
                   
               }
           
           }
            
           return false;
            
        }
        catch(Exception  | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }

    @Override
    public  void addSimilarProductList(ProductoPK key) {
       BeanManager bm = null;
        try{
        
        
    
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"agregarProducto");
            
            
            System.out.print("KEY "+key);
            
            if(key==null){
            
                key=this.forceSimilarListKey();
            
            }
            
            if(agregarProducto!=null && session.getAttribute("username")!=null && key!=null){
            
                if(!this.checkExistenceSimilarProduct(key)){
                
                    if(agregarProducto.getSimilarList()==null){
                    
                        agregarProducto.setSimilarList(new java.util.ArrayList<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>());
                    
                    }
                    
                    FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similar=new FranchiseAdministrator.Product.Classes.SimilarProductsLayout();
                    
                    similar.setPrice(0.0);
                    
                    similar.setProduct(_productController.find(key));
                    
                    similar.setType("producto");
                    
                    similar.setProductImages(IteratorUtils.toList(this.loadSimilarProductImages(_productController.find(key)).iterator()));
                    
                    agregarProducto.getSimilarList().add(similar);
                
                }
                
                else{
                
                    java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                    
                    javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateProduct.SimilarProductExists"));
                
                    msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                    
                    javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                    
                }
                
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    }
   
    
    @Override
    public void addToComponeUpdateList(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
           
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            
            //Check Session VAriables
            if(agregarProducto!=null && session.getAttribute("selectedOption")instanceof Entities.Franquicia && session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
                Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
                
            if(agregarProducto.getComponeListModel()==null){
            
                agregarProducto.setComponeListModel(new ComponeListModel(new java.util.ArrayList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>()));
            
            }

            System.out.print("ROWS IN COMPONE LIST "+agregarProducto.getComponeListModel().getRowCount());
            
            if(agregarProducto.getComponeListModel().getRowCount()<this._inventoryController.findByFranchise(franchise.getIdfranquicia()).size()){
            
                ComponeListModel model=(ComponeListModel)agregarProducto.getComponeListModel();
                
                

                FranchiseAdministrator.Product.Classes.ComponeUpdateInterface componeUpdate=new FranchiseAdministrator.Product.Classes.ComponeUpdate();
                
                Entities.Compone compone=new Entities.Compone();
                
                Entities.ComponePK componePK=new Entities.ComponePK();
                
                componePK.setInventarioFranquiciaIdFranquicia(franchise.getIdfranquicia());
                
                //componePK.setInventarioIdinventario(_inventoryController.find(inventoryPK).getInventarioPK().getIdinventario());
                
                componePK.setProductoCategoriaFranquiciaIdFranquicia(franchise.getIdfranquicia());
                
                componePK.setProductoCategoriaIdCategoria(product.getCategoria().getCategoriaPK().getIdcategoria());
                
                componePK.setProductoIdproducto(product.getProductoPK().getIdproducto());
                
                compone.setCantidadInventario(0.0);
                
               // compone.setInventario(_inventoryController.find(inventoryPK));
            
                compone.setProducto(product);
                
                compone.setComponePK(componePK);
                
                Entities.Inventario inventory=new Entities.Inventario();
                
                inventory.setInventarioPK(new Entities.InventarioPK());
                
                compone.setInventario(inventory);
                
                componeUpdate.setCompone(compone);
   
              model.getDataSource().add(componeUpdate);
                

            }
            else{
            
                _fileUpload.setActionType(null);
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateProduct.ComponeUpdateListAlart"));
            
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
                
            }

            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
   
    public void deleteComponeUpdateList(){
    
        try{
        
           javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
           
           FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
        
           if(agregarProducto!=null){
           
           ComponeListModel model=(ComponeListModel)agregarProducto.getComponeListModel();
           
           SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>list=new SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>();
           
           if(model.getDataSource()!=null && !model.getDataSource().isEmpty() && model.getDataSource().size()>0){
           
               for(FranchiseAdministrator.Product.Classes.ComponeUpdateInterface compone:model.getDataSource()){
               
               if(compone.getSelection()){
               
                   list.addItem(compone);
               
               }
               
               }
           
                 if(!list.isEmpty()){
           
               for(FranchiseAdministrator.Product.Classes.ComponeUpdateInterface compone:list){
               
                 model.getDataSource().remove(compone);
               
               }
           
           }
               
           }
           
           //ComponeUpdateModel is empty
           else{
           
               System.out.print("COMPONE LIST MODEL IS EMPTY");
               
           java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
               
           javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateProduct.ComponeUpdateListEmpty"));
               
           msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
           
           javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
           
           }

           }
           
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public boolean checkNameExistenceUpdateProduct(String productName){
    
    try{
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto && session.getAttribute("selectedOption")instanceof Entities.Franquicia){
        
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
            Entities.Producto product=(Entities.Producto)session.getAttribute("auxiliarComponent");
        
            String name=product.getNombre();
            
            for(Entities.Producto aux:_productController.findByFranchise(franchise.getIdfranquicia())){
            
                System.out.print(aux.getNombre());
                
                if(productName.toLowerCase().equals(aux.getNombre().toLowerCase()) && !aux.getNombre().toLowerCase().equals(name.toLowerCase())){
                
                    System.out.print("PRODUCT FOUND "+aux.getNombre());
                    
                    return true;
                    
                }
            
            }
            
        }
        
        return false;
    
    }
    catch(Exception | StackOverflowError ex){
    
        return false;
        
    }
    
    }
    
    @Override
    public ENUM.TransactionStatus updateProduct(String name, double price,String description,java.util.List<FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface>similarList,org.primefaces.model.LazyDataModel<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>dataModel){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            java.util.List<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>componeList=((ComponeListModel)dataModel).getDataSource();
            
            
            if(!this.checkComponeUpdateList(new SessionClasses.EdenList<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface>(componeList)))
            {
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Producto){
            
                  
                Entities.Producto AuxiliarProducto=(Entities.Producto)session.getAttribute("auxiliarComponent");
                
                  Entities.ProductoPK productPK=new Entities.ProductoPK();
        
                   java.util.List<Entities.SimilarProducts>updateList=new <Entities.SimilarProducts>java.util.ArrayList();
                  
                   productPK.setIdproducto(AuxiliarProducto.getProductoPK().getIdproducto());
        
                   productPK.setCategoriaIdcategoria(AuxiliarProducto.getProductoPK().getCategoriaIdcategoria());
        
                   productPK.setCategoriaFranquiciaIdfranquicia(AuxiliarProducto.getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
                   Entities.Producto product=new Entities.Producto();
        
                   product.setNombre(name);
        
                   product.setPrecio(price);
        
                   product.setDescription(description);
        
                   product.setCategoria(AuxiliarProducto.getCategoria());
                
                   product.setProductoPK(productPK);
                   
                   if(_fileUpload.getPhotoList()!=null){
                   
                   product.setImagenCollection(_fileUpload.getPhotoList());
                   
                   }
                   
                   product.setAdditionCollection(AuxiliarProducto.getAdditionCollection());
                   
                   java.util.List<SimilarProductsLayoutInterface>deleteSimilar=new <FranchiseAdministrator.Product.Classes.SimilarProductsLayout>java.util.ArrayList();
              
                   if(similarList!=null){
                   
                    for(Entities.SimilarProducts similar:AuxiliarProducto.getSimilarProductsCollection()){
           
      System.out.print("Similar Products Size "+AuxiliarProducto.getSimilarProductsCollection().size());
       
      boolean delete=true;
           
       for(FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similarProductsLayout:similarList){
       
       if(similar.getProducto().equals(similarProductsLayout)){
       
           System.out.print(similarProductsLayout.getProduct().getNombre()+"-"+similarProductsLayout.getType());
       
            Entities.SimilarProducts aux=similar;
           
           aux.setPriceDiference(similarProductsLayout.getPrice());
           
            this._similarProductsController.edit(aux);
           
           deleteSimilar.add(similarProductsLayout);
           
          delete=false;
           
          break;
       }
       
       
       }
       
       if(delete){
           
           _similarProductsController.delete(similar.getSimilarProductsPK());
           
       }
       
       }  
  
        for(Entities.SimilarProducts similar:AuxiliarProducto.getSimilarProductsCollection1()){
            
               System.out.print("Similar Products 1 Size "+AuxiliarProducto.getSimilarProductsCollection1().size());
       
       boolean delete=true;
               
       for(FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similarProductsLayout:similarList){
       
       if(similar.getProducto().equals(similarProductsLayout.getProduct())){
       
           System.out.print(similarProductsLayout.getProduct().getNombre()+"-"+similarProductsLayout.getType());
       
           Entities.SimilarProducts aux=similar;
           
           aux.setPriceDiference(similarProductsLayout.getPrice()*-1);
                
           _similarProductsController.edit(aux);
           
           deleteSimilar.add(similarProductsLayout);
           
           delete=false;
           
       }
       
       
       
       }
       
       if(delete){
   
           _similarProductsController.delete(similar.getSimilarProductsPK());
           
       }
       
       }
        
        
        for(SimilarProductsLayoutInterface aux:deleteSimilar){
        
        similarList.remove(aux);
        
        }
  
        
     
        
        
        product.setSimilarProductsCollection(updateList);
        
        }
            
        
        System.out.print("IMAGES IN PRODUCT TO UPDATE "+_fileUpload.getPhotoList());    
                   
        product.setImagenCollection(_fileUpload.getPhotoList());
        
        
        _productController.editProduct(product);
        
        if(similarList!=null && !similarList.isEmpty()){
        
            System.out.print("ELEMENTS IN SIMILAR LIST "+similarList.size());
            
            //SimilarList
            
        for(SimilarProductsLayoutInterface aux:similarList){
        
        
        Entities.SimilarProducts similarProductss=new Entities.SimilarProducts();
        
        
        Entities.SimilarProductsPK similarProductsPK=new Entities.SimilarProductsPK();
        
        similarProductsPK.setProductoIdproducto(product.getProductoPK().getIdproducto());
        
        similarProductsPK.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
        
        similarProductsPK.setProductoCategoriaFranquciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
        //Product 1
        
        similarProductsPK.setProductoIdproducto1(aux.getProduct().getProductoPK().getIdproducto());
        
        similarProductsPK.setProductoCategoriaIdCategoria1(aux.getProduct().getProductoPK().getCategoriaIdcategoria());
        
        similarProductsPK.setProductoCategoriaFranquiciaIdFranquicia1(aux.getProduct().getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
        similarProductss.setSimilarProductsPK(similarProductsPK);
        
        similarProductss.setProducto(product);
        
        similarProductss.setProducto1(aux.getProduct());
        
        
        
        similarProductss.setPriceDiference(aux.getPrice());
        
        System.out.print(aux.getPrice());
        
        _similarProductsController.create(similarProductss);
        
        updateList.add(similarProductss);
        
        System.out.print(aux.getProduct().getNombre()+" "+ AuxiliarProducto.getNombre() +"Added To Database");
        
        }
        
        }
         
          System.out.print("PRE PROCESS COMPONE LIST");
        if(componeList!=null && !componeList.isEmpty()){
        
            System.out.print("COMPONE LIST IS NOT NULL");
            
              java.util.List<Entities.Compone> componList=new <Entities.Compone> java.util.ArrayList();
              
              System.out.print("COMPONE LIST SIZE TO UPDATE "+componeList.size());
        
        for(FranchiseAdministrator.Product.Classes.ComponeUpdateInterface compone:componeList)
        
        {
        
            Entities.Compone aux=compone.getCompone();
            
            Entities.ComponePK componePK=new Entities.ComponePK();
            
            componePK.setInventarioIdinventario(aux.getInventario().getInventarioPK().getIdinventario());
            
            componePK.setInventarioFranquiciaIdFranquicia(aux.getInventario().getInventarioPK().getFranquiciaIdFranquicia());
                      
            componePK.setProductoIdproducto(aux.getProducto().getProductoPK().getIdproducto());
            
            componePK.setProductoCategoriaIdCategoria(aux.getProducto().getProductoPK().getCategoriaIdcategoria());
            
            componePK.setProductoCategoriaFranquiciaIdFranquicia(aux.getProducto().getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            aux.setComponePK(componePK);
            
            aux.setCantidadInventario(compone.getCompone().getCantidadInventario());
                      
            componList.add(aux);
        
        }
      
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
      _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username"));
        
        
        for(Entities.Compone compone: ((Entities.Producto) session.getAttribute("auxiliarComponent")).getComponeCollection()){
   
        _componeController.delete(compone.getComponePK());
        
        }
        
        for(Entities.Compone compone: componList){
            
        _componeController.create(compone);
        
        }
        
        }
        
        /*java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES+bundle.getString("franchiseAdministratorUpdateProduct.ProductUpdated"));
        
        javax.faces.context.FacesContext.getCurrentInstance().addMessage(null, msg);
        */
        
        }
            
            return ENUM.TransactionStatus.APPROVED;
            
        }
            
            else{
                
               System.out.print(ENUM.TransactionStatus.DISAPPROVED);
                
                return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
        }
      
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
    
    
   
}

	/**
	 * 
	 * @param viewId
	 */
    @Override
	public void initPreRenderView(String viewId) {
		
            try{
                
          javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
          
          FranchiseAdministrator.Product.View.AgregarProducto agregarProducto=(FranchiseAdministrator.Product.View.AgregarProducto)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "agregarProducto");
          
                    
    switch(viewId){
    

        case "/adminFranquicia/AgregarProducto.xhtml":{
            
              this.initFileUpload(viewId);

            this.loadProductsImages();
            
            this.initAdditions();
            
            agregarProducto.setProducts(this.ProductsList());
            
          this.loadImagesGalleria();
           
       
            
           System.out.print("SIMILAR PRODUCTS SIZE "+agregarProducto.getProducts().size());
           
           if(this._fileUpload.getImages()!=null && !this._fileUpload.getImages().isEmpty()){
           
               System.out.print("PHOTOS SIZE "+this._fileUpload.getImages().size());
               
               for(String string:this._fileUpload.getImages()){
               
               System.out.print(string);
               
               }
               
               
           }
            
             if(agregarProducto.getSimilarList()==null || agregarProducto.getSimilarList().isEmpty()){
           
                 agregarProducto.setDragImage("/images/drag.png");
                 
              agregarProducto.setDragRender(true);

           
           }
           
           else{
           
                agregarProducto.setDragRender(false);
           
           }

            break;
            
            }
        
        case "/adminFranquicia/listarproducto.xhtml":{
        
              this.initFileUpload(viewId);
            
             System.out.print("CATEGORY PK "+agregarProducto.getId_Categoria());
       
             agregarProducto.setProductList(new LazyProductDataModel(this.productList(agregarProducto.getId_Categoria())));
             
             //this.ProductList=new LazyProductDataModel(this._agregarProductoController.productList(this.id_Categoria));
   
     //  System.out.print("PRODUCT LIST SIZE "+this._agregarProductoController.productList(this.id_Categoria).size());
        
            break;
        }
        
        case "/adminFranquicia/updateProduct.xhtml":{
        
            System.out.print("UPDATE PRODUCT ACTION TYPE "+this._fileUpload.getActionType().getActionType());
            
            this.initUpdateProduct(viewId);
        
        }
    
    }
                
                
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

}
