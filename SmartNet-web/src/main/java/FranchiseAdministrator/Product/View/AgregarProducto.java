/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FranchiseAdministrator.Product.View;

import CDIBeans.AdditionControllerDelegate;
import CDIBeans.DesplegableCategoriaInterface;
import CDIBeans.DesplegableInventarioInterface;
import CDIBeans.FileUploadInterface;
import CDIBeans.desplegablesProductoInterface;
import Clases.BaseBacking;
import FranchiseAdministrator.Product.Classes.ComponeUpdate;
import FranchiseAdministrator.Product.Classes.ProductoInventarioCompone;
import FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Compone;
import Entities.Imagen;
import Entities.InventarioPK;

import Entities.Producto;
import Entities.SimilarProducts;
import FranchiseAdministrator.Product.Classes.ComponeUpdateInterface;
import FrontEndModel.LazyProductDataModel;
import SessionBeans.SimilarProductsFacadeLocal;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

import javax.faces.event.ComponentSystemEvent;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.transaction.Transactional.TxType;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.CategoriaFacadeREST;
import jaxrs.service.ComponeFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.InventarioFacadeREST;
import jaxrs.service.ProductoFacadeREST;
import jaxrs.service.SimilarProductsFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;




/**
 *
 * @author luisnegrete
 */

@RolesAllowed({"GuanabaraSucAdmin","GuanabaraFrAdmin"})

public class AgregarProducto extends BaseBacking implements Serializable{
    
    @EJB
    private SimilarProductsFacadeLocal similarProductsFacade;
    
    @EJB
    private ProductoFacadeREST productoFacadeREST;
    
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    
    @EJB
    private ComponeFacadeREST componeFacadeREST;
    
    @EJB
    private CategoriaFacadeREST categoriaFacadeREST;
    
    @EJB 
    private InventarioFacadeREST inventarioFacade;
    
    @EJB
    private SimilarProductsFacadeREST similarProductsFacadeREST;
    

    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ProductoControllerInterface productoController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.FranchiseAdministratorProductController _franchiseAdministratorProductController;
    
    @javax.inject.Inject
    private CDIEden.AgregarProductoControllerInterface agregarProducto;
     
    @Inject private FileUploadInterface fileUpload;
   
    @Inject private desplegablesProductoInterface desplegablesProducto;
    
    @Inject private  DesplegableCategoriaInterface desplegablesCategoria;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Product.Controller.AgregarProductoController _agregarProductoController;
      
    private List<Categoria>categories;
    
    private List<ComponeUpdateInterface>deleteComponeList;
    
    private LazyDataModel<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface> componeListModel;
    
    private ComponeUpdate componeUpdate;
    
    private List<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface>similarProducts;
    
    private String description="";
    
    private Entities.ProductoPK similarProductList;
    
    private String _barCodeImage;
   
    
    private java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>products;
    
    private String dragImage;
    
    private boolean dragRender;
    
    private int draggingIdProduct;
    
    private boolean dragged;
    

    
    private String barcode;
    
    private java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>additionList;
    
    private java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>dragAdditionList;
    
    public java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>getDragAdditionList(){
    
        return this.dragAdditionList;
    
    }
    
    public String getBarcode(){
    
    return this.barcode;
    
    }
    
    public void setBarcode(String barcode){
    
    this.barcode=barcode;
    
    }
    
    
    public String getBarcodeImage(){
    
        return _barCodeImage;
    
    }
    
    public void setBarcodeImage(String barCodeImage){
    
        _barCodeImage=barCodeImage;
    
    }
    
    
    public void setDragAdditionList(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>dragAdditionList){
    
    this.dragAdditionList=dragAdditionList;
    
    }
    
    public java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>getAdditionList(){
    
    return this.additionList;
    
    }
    
    public void setAdditionList(java.util.List<FranchiseAdministrator.Product.Classes.AdditionEden>additionList){
    
    this.additionList=additionList;
    
    }
    
    public void setAgregarProducto(CDIEden.AgregarProductoControllerInterface agregarProducto){
    
        this.agregarProducto=agregarProducto;
    
    }
      
    public boolean getDragged(){
    
        return this.dragged;
    
    }
    
    public void setDragged(boolean dragged){
    
        this.dragged=dragged;
    
    }
    
    public int getDraggingIdProduct(){
    
    return this.draggingIdProduct;
    
    }
    
    public void setDraggingIdProduct(int draggingIdProduct){
    
    this.draggingIdProduct=draggingIdProduct;
    
    }
    
    public boolean getDragRender(){
    
        return this.dragRender;
    
    }
    
    public void setDragRender(boolean dragRender){
    
        this.dragRender=dragRender;
    
    }
    
    public String getDragImage(){
    
        return this.dragImage;
    
    }
    
    public void setDragImage(String dragImage){
    
    this.dragImage=dragImage;
    
    }
    
    public CDIBeans.ProductoControllerInterface getProductoController(){
    
        return this.productoController;
    
    }
    
    public void setProductoController(CDIBeans.ProductoControllerInterface productoController){
    
    this.productoController=productoController;
    
    }
    
    
    public java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>getProducts(){
    
        return this.products;
    
    }
    
    public void setProducts(java.util.List<FranchiseAdministrator.Product.Classes.ProductoEdenInterface>products){
    
        this.products=products;
    
    }
    
    @Inject private AdditionControllerDelegate additionController;
    
   
    
    
    public Entities.ProductoPK getSimilarProductList(){
    
    return this.similarProductList;
        
    }
    
    public void setSimilarProductList(Entities.ProductoPK similarProductList){
    
    this.similarProductList=similarProductList;
    
    }
    
    public void setDescription(String description){
    
    this.description=description;
    
    }
    
    public String getDescription(){
    
    return this.description;
    
    }
    
    public List<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface>getSimilarProducts(){
    
        return this.similarProducts;
    
    }
    
    public void setSimilarProducts(List<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface> similarProducts){
    
    this.similarProducts=similarProducts;
    
    }
    
    public void setCategories(List<Categoria>categories){
    
    this.categories=categories;
    
    }
    
    public List<Categoria>getCategories(){
    
        try{
        
      return this._agregarProductoController.getCategories();
      
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
            
        }
    
    }
    
    public void setComponeUpdate(ComponeUpdate componeUpdate){
    
    this.componeUpdate=componeUpdate;
        
    }
    
    public ComponeUpdate getComponeUpdate(){
    
    return this.componeUpdate;
        
    }
    
    
    
    private boolean selection;
    
    public boolean getSelection(){
    
    return this.selection;
    
    }
    
    public void setSelection(boolean selection){
    
    this.selection=selection;
    
    }
    

    public LazyDataModel<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface> getComponeListModel(){
    
    return this.componeListModel;
    
    }
    
    public void setComponeListModel(LazyDataModel<FranchiseAdministrator.Product.Classes.ComponeUpdateInterface> componeListModel){
    
        this.componeListModel=componeListModel;
    
    }
    
    
   
    
    public List<ComponeUpdateInterface>getDeleteComponeList(){
    
    return this.deleteComponeList;
        
    }
    
    public void setDeleteComponeList(List <ComponeUpdateInterface> deleteComponeList){
    
    this.deleteComponeList=deleteComponeList;
        
    }
    
    
    
    private
    @Inject DesplegableInventarioInterface desplegableInventario;
    
    public FileUploadInterface getFileUpload(){


        return this.fileUpload;
}
    
    
    public void setFileUpload(FileUploadInterface fileUpload){
    
        this.fileUpload=fileUpload;
    
    }
    
    public desplegablesProductoInterface getDesplegablesProducto(){
        
        return this.desplegablesProducto;
    
    }
    
    public void setDesplegablesProducto(desplegablesProductoInterface desplegablesProducto){
    
    this.desplegablesProducto=desplegablesProducto;
    }
    
    public DesplegableInventarioInterface getDesplegableInventario(){
    
    return this.desplegableInventario;
    }
    
    public void setDesplegableInventario(DesplegableInventarioInterface desplegableInventario){
    
        this.desplegableInventario=desplegableInventario;
    
    }
    
    
    private List<SimilarProductsLayoutInterface>similarList;
    
    public void setSimilarList(List<SimilarProductsLayoutInterface> similarList){
    
    this.similarList=similarList;
    
    }
    
    public List<SimilarProductsLayoutInterface>getSimilarList(){
    
    
    return this.similarList;
    
    }
    
    
    
    
    private String auxiliar2;
    
    private List<Entities.ProductoPK>deleteProductsList;
    
    
    private List<ComponeUpdateInterface>componeList;
    
    public List<Entities.ProductoPK> getDeleteProductsList(){
    
    return this.deleteProductsList;
    
    }
    
    public void setDeleteProductsList(List<Entities.ProductoPK>deleteProductsList){
    
    this.deleteProductsList=deleteProductsList;
        
    }
    
    public void setComponeList(List <ComponeUpdateInterface>componeList)
    {
    
    this.componeList=componeList;
    }
    
    public List<ComponeUpdateInterface>getComponeList(){
    return this.componeList;
    }
    
    public void setAuxuliar2(String auxiliar2){
    this.auxiliar2=auxiliar2;
    }
    
    public String getAuxiliar2(){
    return this.auxiliar2;
    }
   
    
    
    private LazyDataModel<Producto> ProductList;
    
    
    private List<ProductoInventarioComponeInterface> InvList;
    
    
    private String auxiliar;
    
    public String getAuxiliar(){
    return this.auxiliar;
    }
    
   private  Entities.Producto deletePK;
    
    public Entities.Producto getDeletePK(){
    
    return this.deletePK;
    
    }
    
    public void setDeletePK(Entities.Producto deletePK){
    
    this.deletePK=deletePK;
    
    }
    
    public void setProductList(LazyDataModel <Producto>ProductList){
    
    this.ProductList=ProductList;
    
    }
    
    public LazyDataModel<Producto> getProductList(){
    return this.ProductList;
    }
    
    public void deleteProducts(ActionEvent event){
    
     
        
        try{
      
           
            if(this.deletePK!=null)
            {

          this._agregarProductoController.deleteProduct(deletePK.getProductoPK());

        }  
            else{
            
                java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddProduct.SelectProduct"));
            
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            }
          
        
      
        
        }
        catch(NullPointerException ex){
        
            if(this.getContext()!=null){
            
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
            
            FacesMessage msg=new FacesMessage("You are hacking bitch");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            getContext().addMessage(event.getComponent().getClientId(), msg);            
            }
        }
       
        
        catch(EJBException ex){
        
        Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
        FacesMessage msg=new FacesMessage("EJBException caugth");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        
        
        }
        
    }
    
  
    
    
    public void setType(javax.faces.event.ComponentSystemEvent event){
        
        try
        {
     
      this._agregarProductoController.initPreRenderView(super.getCurrentView());

        }
        
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
       
    }
    
    
    public void updateProducts(AjaxBehaviorEvent ajaxbehavior){
    
        
          
        try{
         
                this.ProductList=new LazyProductDataModel(this._agregarProductoController.productList(this.id_Categoria));

        }
        catch(NullPointerException ex)
            
        {
            
            if(this.getContext()!=null){
            
        FacesMessage msg=new FacesMessage("You are hacking Bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(ajaxbehavior.getComponent().getClientId(), msg);
        
            }
        }
    }
    
    public void setAuxiliar(String auxiliar){
    
        this.auxiliar=auxiliar;
        
    }

    
    
    
    public List<ProductoInventarioComponeInterface> getInvList() {
        return InvList;
    }

    public void setInvList(List<ProductoInventarioComponeInterface> InvList) {
        this.InvList = InvList;
    }
    
    
  
    //@Inject ProductInvList productInvList;
    
    @Size(min=1,max=50)
    private String nombre;
    
    @NotNull
    private CategoriaPK id_Categoria;

   
    @NotNull
    private InventarioPK id_inv;
    
    
    private Entities.ProductoPK id_producto;

    public Entities.ProductoPK getId_producto() {
        return id_producto;
    }

    public void setId_producto(Entities.ProductoPK id_producto) {
        this.id_producto = id_producto;
    }

    public InventarioPK getId_inv() {
        return id_inv;
    }

    public void setId_inv(InventarioPK id_inv) {
        this.id_inv = id_inv;
    }

    public double getCantInv() {
        return cantInv;
    }

    public void setCantInv(double cantInv) {
        this.cantInv = cantInv;
    }
    
    private double cantInv;

    public CategoriaPK getId_Categoria() {
        return id_Categoria;
    }

    public void setId_Categoria(CategoriaPK id_Categoria) {
        this.id_Categoria = id_Categoria;
    }

 

  
    
    private double precio;

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Creates a new instance of AgregarProducto
     */
    public AgregarProducto() {
    }
    
    @PostConstruct
    public void init(){
        
        try{
            
           
            if(this.fileUpload.getAuxiliar()!=null){
            
            System.out.print("File Upload Auxiliar "+this.fileUpload.getAuxiliar());
            
            }
            
         
            
            if(this.getSessionAuxiliarComponent()!=null && this.getSessionAuxiliarComponent() instanceof Boolean){
   
                this.dragRender=(boolean)this.getSessionAuxiliarComponent();
        
                System.out.print("Dragged Value "+this.dragRender);
            }
            
          
            this.dragAdditionList=new java.util.ArrayList<FranchiseAdministrator.Product.Classes.AdditionEden>();
            
            for(Entities.SimilarProducts aux:similarProductsFacade.getProductList()){
                
            FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface similar=new FranchiseAdministrator.Product.Classes.EdenSimilarProduct();
            
            similar.setSimilarProduct(aux);
            
            this.similarProducts.add(similar);
            
            }
            
         
        this.categories=this.desplegablesCategoria.getResults();
        
        if(getSessionAuxiliarComponent()!=null){
        
            System.out.print("SessionComponent is not null");
        
        }
        
        else{
        
        System.out.print("SessionComponent is null");
        
        }
   
        this.auxiliar="hey you";
        
        this.InvList=new <ProductoInventarioCompone> ArrayList();
        
        this.id_Categoria=new CategoriaPK();
      
        this.id_inv=new InventarioPK();
        
        System.out.print("Agregar Producto Postcontructed");
        
        
        
        System.out.print("ActionType "+this.fileUpload.getActionType().getActionType());
        
        
       
       
       
       System.out.print("Images Cache Size "+this.fileUpload.getImages().size());
       
        }
        catch(Exception ex){
        
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    }
    
    
    public void handleFileUpload(FileUploadEvent event){
     
        
        try{
      
        
     this._agregarProductoController.handleUpload(event);
        
     System.out.print("IMAGE UPLOADED");
        
        }
        
        catch(NullPointerException ex){
            
        Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);

        }
    }
    
    
    public void checkExistence(ComponentSystemEvent event){
        
        try{
        
    System.out.print("PostValidateEvent PHASE "+getContext().getCurrentPhaseId().getName());
    
    System.out.print("Id Categoria PostValidate "+this.id_Categoria.getIdcategoria());
    
    UIComponent component=event.getComponent();
    
    UIInput input=(UIInput)component.findComponent(":form:tabView:nombre");
  
    if(input!=null && input.getLocalValue()!=null)
    {
     
    if(this._agregarProductoController.checkExistence(input.getLocalValue().toString())){
    
   java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");

    FacesMessage msg=new FacesMessage(bundle.getString("franchiseAdministratorAddProduct.Existence"));
        
    msg.setSeverity(FacesMessage.SEVERITY_WARN);
        
    getContext().addMessage(input.getClientId(), msg);
        
    getContext().renderResponse();
        
    }
    
    
    }
    
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    @annotations.MethodAnnotations(author="Luis Negrete",date="10/01/2016",comments="Only Update")
    public void postValidateUpdateProduct(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            javax.faces.component.UIComponent component=event.getComponent();
            
            if(component.findComponent(":form:tabView:nombre") instanceof javax.faces.component.UIInput){
            
                javax.faces.component.UIInput input=(javax.faces.component.UIInput)component.findComponent(":form:tabView:nombre");
            
                if(input.getLocalValue()!=null){
                
                String name=input.getLocalValue().toString();
                
                System.out.print("NAME POST VALIDATE EVENT "+name);
                
                if(_agregarProductoController.checkNameExistenceUpdateProduct(name)){
                
                    System.out.print("PRINT EXISTENCE MESSAGE");
                    
                 java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(),"bundle");
                
                 javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateProduct.PostValidateEvent"));
                 
                 msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                 
                 this.getContext().addMessage(null, msg);
                 
                 this.getContext().renderResponse();
                 
                }
                
                }
                
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    
    public List<Producto> ProductList(CategoriaPK categoriaPK){
        
        
    
        List<Producto>results=new <Producto> ArrayList();
    
        
        
    return results;
    }
    
    
    
    //ADD PRODUCT
    
    public void addProducto(ActionEvent event){
        
        try{
 
            this._agregarProductoController.AddProduct(this.dragAdditionList, this.barcode, this.description, this.nombre, this.precio, this.id_Categoria, this.InvList,this.similarProducts);
            
    }
        
      
        catch(NullPointerException ex){
            if(this.getContext()!=null)
            {  
        Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
        FacesMessage msg=new FacesMessage("Exception caugth");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(null, msg);
         
        getContext().renderResponse();
        
        }
        
        }
    
    
       
    }
    

    public void ajaxProduct(AjaxBehaviorEvent ajaxBehaviour){
        
    try{
    
    System.out.print("PhaseID "+ajaxBehaviour.getPhaseId().getName());
     
    System.out.print(ajaxBehaviour.getComponent().getId());
     
    System.out.print(ajaxBehaviour.getPhaseId().getName());
    
      this._agregarProductoController.addUndoInventory(ajaxBehaviour.getComponent().getId());

 }
 
 catch(NullPointerException ex){
     
     if(this.getContext()!=null)
     {
 Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null, ex);
 
 FacesMessage msg =new FacesMessage("NullPointerException caugth");
 
 msg.setSeverity(FacesMessage.SEVERITY_ERROR);
 
 getContext().addMessage(null, msg);
 
 }
 
 }
 
 catch(EJBException ex){
 
     Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
     
     FacesMessage msg=new FacesMessage("EJB Exception Caugth");
     
     msg.setSeverity(FacesMessage.SEVERITY_ERROR);
     
     getContext().addMessage(null, msg);
     
     
     
 }
    
    }
    
    public void deleteProducto(){
        
        try{
        
  System.out.print("Product Erased");
  for(Compone aux:productoFacadeREST.find(new PathSegmentImpl("bar;idproducto="+this.id_producto.getIdproducto()+";categoriaIdcategoria="+this.id_producto.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+this.id_producto.getCategoriaFranquiciaIdfranquicia()+"")).getComponeCollection()){
      
      PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+aux.getComponePK().getProductoIdproducto()+";productoCategoriaIdCategoria="+aux.getComponePK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+aux.getComponePK().getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+aux.getComponePK().getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+aux.getComponePK().getInventarioFranquiciaIdFranquicia()+"");
      
  componeFacadeREST.remove(ps);
  }
  List <Imagen> lista=null;//imagenFacadeREST.findOne("producto", this.id_producto);
  
  
  for(Imagen aux:lista){
      
  imagenFacadeREST.remove(aux.getIdimagen());
  
  }
  
  
  productoFacadeREST.remove(new PathSegmentImpl("bar;idproducto="+this.id_producto.getIdproducto()+";categoriaIdcategoria="+this.id_producto.getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+this.id_producto.getCategoriaFranquiciaIdfranquicia()+""));
  
  
  this.desplegablesProducto.UpdateProductsList(id_Categoria);
  
  
  
  this.ProductList=new LazyProductDataModel(this.desplegablesProducto.getProductsList());
    
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
  
    }
    
   
  
    
 
 
    
    public void addInventarioUpdate(AjaxBehaviorEvent event){
    
    
        try{
            
        
        
            this._agregarProductoController.addToComponeUpdateList();
           
        /*
       if(event.getComponent().getClientId().equals("form:tabView:addInv")){
         
            System.out.print("You pressed addInventarioUpdate");
            
            ComponeUpdate componeUpdat=new ComponeUpdate();
            
            Producto product=(Producto)getSessionAuxiliarComponent();
            
            InventarioPK inventarioPK =new InventarioPK();
            
            Inventario inventario=new Inventario();
            
            inventario.setInventarioPK(inventarioPK);
            
            componeUpdat.getCompone().setInventario(inventario);
            
            
            componeUpdat.setRender(false);
            
            componeUpdat.getCompone().setProducto(product);
            
            this.componeList.add(componeUpdat);
            
                     
            System.out.print("Size ComponeList "+componeList.size());
       }
       else{
       
           throw new Exception ();
       }
            
        
        */
       
        }
        catch(Exception ex){
        
        
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
            
          
            
        }
       
        
    }
   
    
    
    
    public void deleteInvCompone(AjaxBehaviorEvent event){
    
        try{
            
            this._agregarProductoController.deleteComponeUpdateList();
            
            /*
          List<ComponeUpdateInterface>deleteList=new <ComponeUpdate> ArrayList();
            
            
            
      for(ComponeUpdateInterface componeUpdat:this.componeList){
       
     
       if(componeUpdat.getSelection()){
       
       deleteList.add(componeUpdat);
           
       }
      
       }
      
      for(ComponeUpdateInterface delete:deleteList){
      
      this.componeList.remove(delete);
      
      }
         */
   
        
        }
        
        catch(NullPointerException ex){
        
            if(this.getContext()!=null)
            {
            
        Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
        FacesMessage msg=new FacesMessage("You are hacking bitch");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
            
        }
        
        }
    
        
        
      
    
    }
    
     @javax.transaction.Transactional(TxType.REQUIRES_NEW)
    public String updateProduct(){
        
        try{
        
             switch( _agregarProductoController.updateProduct(this.nombre, this.precio, this.description, this.similarList, this.componeListModel)){
           
                 case APPROVED:{
                 
                      return "success";
                      
                     
                 
                 }
                 
                 case DISAPPROVED:{
                     
                      java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorUpdateProduct.RepeatedComponeItem"));
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                javax.faces.context.FacesContext.getCurrentInstance().addMessage(null,msg);
                 
                this.getContext().renderResponse();
                
                 return "failure";
                      
                     
                     
                 }
                 
                 default:{
                 
                       return "failure";
                 
                 }
                 
           }
           
            
           
            /*
            if(getSessionAuxiliarComponent() instanceof Producto)
            
            {
        
        System.out.print("you are updating components");
            
        for(Imagen image: ((Producto)getSessionAuxiliarComponent()).getImagenCollection()){
        
            imagenFacadeREST.remove(image.getIdimagen());
            
            
            System.out.print("Product Image Removed");
        
        } 
        
        System.out.print("Update Product images size "+fileUpload.getPhotoList().size());
        
        List <Imagen>ImageList=new <Imagen>ArrayList();
        
        for(Imagen image:this.fileUpload.getPhotoList())
        {
        
        imagenFacadeREST.create(image);
        
        ImageList.add(image);
        
        System.out.print("Product Image Created");
        
        }

        Producto AuxiliarProducto=(Producto)getSessionAuxiliarComponent();
        
        Entities.ProductoPK productPK=new Entities.ProductoPK();
        
        productPK.setIdproducto(AuxiliarProducto.getProductoPK().getIdproducto());
        
        productPK.setCategoriaIdcategoria(AuxiliarProducto.getProductoPK().getCategoriaIdcategoria());
        
        productPK.setCategoriaFranquiciaIdfranquicia(AuxiliarProducto.getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
        Producto product=new Producto();
        
        product.setNombre(this.nombre);
        
        product.setPrecio(this.precio);
        
        product.setCategoria(AuxiliarProducto.getCategoria());

        product.setProductoPK(productPK);
        
        product.setImagenCollection(ImageList);
        
        product.setAgrupaCollection(AuxiliarProducto.getAgrupaCollection());
        
        List<SimilarProductsLayoutInterface>deleteSimilar=new <SimilarProductsLayout>ArrayList();
       
        
      for(SimilarProducts similar:AuxiliarProducto.getSimilarProductsCollection()){
           
      System.out.print("Similar Products Size "+AuxiliarProducto.getSimilarProductsCollection().size());
       
      boolean delete=true;
           
       for(SimilarProductsLayoutInterface similarProductsLayout:this.getSimilarList()){
       
       if(similar.getProducto().equals(similarProductsLayout)){
       
           System.out.print(similarProductsLayout.getProduct().getNombre()+"-"+similarProductsLayout.getType());
       
            SimilarProducts aux=similar;
           
           aux.setPriceDiference(similarProductsLayout.getPrice());
           
           PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+aux.getSimilarProductsPK().getProductoIdproducto()+";productodCategoriaIdCategoria="+aux.getSimilarProductsPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+aux.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+aux.getSimilarProductsPK().getProductoIdproducto1()+";productoCategoriaIdCategoria1="+aux.getSimilarProductsPK().getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+aux.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia1()+"");
           
           similarProductsFacadeREST.edit(ps,aux);
           
           deleteSimilar.add(similarProductsLayout);
           
          delete=false;
           
          break;
       }
       
       
       }
       
       if(delete){
       
           PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+similar.getSimilarProductsPK().getProductoIdproducto()+";productoCategoriaIdCategoria="+similar.getSimilarProductsPK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+similar.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia()+";productoIdproducto1="+similar.getSimilarProductsPK().getProductoIdproducto1()+";productoCategoriaIdCategoria1="+similar.getSimilarProductsPK().getProductoCategoriaIdCategoria1()+";productoCategoriaFranquiciaIdFranquicia1="+similar.getSimilarProductsPK().getProductoCategoriaFranquiciaIdFranquicia1()+"");
                  
           similarProductsFacadeREST.remove(ps);
           
       }
       
       }
       
        for(SimilarProducts similar:AuxiliarProducto.getSimilarProductsCollection1()){
            
               System.out.print("Similar Products 1 Size "+AuxiliarProducto.getSimilarProductsCollection1().size());
       
       boolean delete=true;
               
       for(SimilarProductsLayoutInterface similarProductsLayout:this.getSimilarList()){
       
       if(similar.getProducto().equals(similarProductsLayout.getProduct())){
       
           System.out.print(similarProductsLayout.getProduct().getNombre()+"-"+similarProductsLayout.getType());
       
           SimilarProducts aux=similar;
           
           aux.setPriceDiference(similarProductsLayout.getPrice()*-1);
           
           PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+aux.getSimilarProductsPK().getProductoIdproducto()+";productoIdproducto1="+aux.getSimilarProductsPK().getProductoIdproducto1()+"");
           
           similarProductsFacadeREST.edit(ps,aux);
           
           deleteSimilar.add(similarProductsLayout);
           
           delete=false;
           
       }
       
       
       
       }
       
       if(delete){
       
           
           PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+similar.getSimilarProductsPK().getProductoIdproducto()+";productoIdproducto1="+similar.getSimilarProductsPK().getProductoIdproducto1()+"");
           similarProductsFacadeREST.remove(ps);
       
       }
       
       }
        
        
        for(SimilarProductsLayoutInterface aux:deleteSimilar){
        
        this.similarList.remove(aux);
        
        }
        
        List<SimilarProducts>updateList=new <SimilarProducts>ArrayList();
     
        
        
        product.setSimilarProductsCollection(updateList);
        
        productoFacadeREST.edit(new PathSegmentImpl("bar;idproducto="+product.getProductoPK().getIdproducto()+";categoriaIdcategoria="+product.getProductoPK().getCategoriaIdcategoria()+";categoriaFranquiciaIdfranquicia="+product.getProductoPK().getCategoriaFranquiciaIdfranquicia()+""),product);
        
        
           
        for(SimilarProductsLayoutInterface aux:this.similarList){
        
        
        SimilarProducts similarProductss=new SimilarProducts();
        
        
        SimilarProductsPK similarProductsPK=new SimilarProductsPK();
        
        similarProductsPK.setProductoIdproducto(product.getProductoPK().getIdproducto());
        
        similarProductsPK.setProductoCategoriaIdCategoria(product.getProductoPK().getCategoriaIdcategoria());
        
        similarProductsPK.setProductoCategoriaFranquciaIdFranquicia(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
        //Product 1
        
        similarProductsPK.setProductoIdproducto1(aux.getProduct().getProductoPK().getIdproducto());
        
        similarProductsPK.setProductoCategoriaIdCategoria1(aux.getProduct().getProductoPK().getCategoriaIdcategoria());
        
        similarProductsPK.setProductoCategoriaFranquiciaIdFranquicia1(aux.getProduct().getProductoPK().getCategoriaFranquiciaIdfranquicia());
        
        similarProductss.setSimilarProductsPK(similarProductsPK);
        
        similarProductss.setProducto(product);
        
        similarProductss.setProducto1(aux.getProduct());
        
        
        
        similarProductss.setPriceDiference(aux.getPrice());
        
        System.out.print(aux.getPrice());
        
        similarProductsFacadeREST.create(similarProductss);
        
        updateList.add(similarProductss);
        
        System.out.print(aux.getProduct().getNombre()+" "+ AuxiliarProducto.getNombre() +"Added To Database");
        
        }
        
        
        List<Compone> componList=new <Compone> ArrayList();
        
        for(ComponeUpdateInterface compone:this.componeList)
        
        {
        
            Compone aux=compone.getCompone();
            
            ComponePK componePK=new ComponePK();
            
            componePK.setInventarioIdinventario(aux.getInventario().getInventarioPK().getIdinventario());
            
            componePK.setInventarioFranquiciaIdFranquicia(aux.getInventario().getInventarioPK().getFranquiciaIdFranquicia());
                      
            componePK.setProductoIdproducto(aux.getProducto().getProductoPK().getIdproducto());
            
            componePK.setProductoCategoriaIdCategoria(aux.getProducto().getProductoPK().getCategoriaIdcategoria());
            
            componePK.setProductoCategoriaFranquiciaIdFranquicia(aux.getProducto().getProductoPK().getCategoriaFranquiciaIdfranquicia());
            
            aux.setComponePK(componePK);
            
            aux.setCantidadInventario(compone.getCompone().getCantidadInventario());
                      
            componList.add(aux);
        
        }
      
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
      this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username"));
        
        
        for(Compone compone: ((Producto) getSessionAuxiliarComponent()).getComponeCollection()){
        
            PathSegment ps=new PathSegmentImpl("bar;productoIdproducto="+compone.getComponePK().getProductoIdproducto()+";productoCategoriaIdCategoria="+compone.getComponePK().getProductoCategoriaIdCategoria()+";productoCategoriaFranquiciaIdFranquicia="+compone.getComponePK().getProductoCategoriaFranquiciaIdFranquicia()+";inventarioIdinventario="+compone.getComponePK().getInventarioIdinventario()+";inventarioFranquiciaIdFranquicia="+compone.getComponePK().getInventarioFranquiciaIdFranquicia()+"");
            
        componeFacadeREST.remove(ps);
        
        }
        
        for(Compone compone: componList){
        
        componeFacadeREST.create(compone);
        
        }
        
        
        FacesMessage msg=new FacesMessage("Product Updated Succesfully",FacesMessage.FACES_MESSAGES);
        
        getContext().addMessage(null, msg);
        
        getContext().renderResponse();
        
            }
            
            else
            {
            
                throw new RuntimeException();
            
            }
        
            */
            
           
        }
        
        catch(RuntimeException ex){
        
        Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
            
      return "failure";
        
        }
        
        
        }
    
    public List <Entities.Producto> getFranchiseProducts(){
    
        try{
            
            if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
        
                Entities.Franquicia franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
                
                
                
                List<Producto>products=productoFacadeREST.findByFranchise(franchise.getIdfranquicia());
                
                if(getSessionAuxiliarComponent() instanceof Producto){
                
                products.remove((Producto)getSessionAuxiliarComponent());
                
                
                  for(SimilarProductsLayoutInterface aux:this.similarList){
                
                    products.remove(aux.getProduct());
                
                }
                  
                  if(!products.isEmpty()){
                  
                      this.similarProductList=products.get(0).getProductoPK();
                      
                      System.out.print(productoFacadeREST.find(this.similarProductList).getNombre());
                  
                  }
                  
                  System.out.print("Similar Products Index "+this.similarProductList);
                
                }
                
              
           
                
        return products;
        
            }
            else{
            
                throw new RuntimeException();
            
            }
        
        }
        catch(NullPointerException ex){
        
            if(this.getContext()!=null)
            {
            FacesMessage msg=new FacesMessage("You are Hacking Bitch");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            getContext().addMessage(null,msg);
            
        }
            
            return null;
        
        }
    
    }
    
    public void addSimilarProduct(String imageRoot,Entities.ProductoPK productId){
        
        try{
            
            if(this.similarProducts==null){
            
                this.similarProducts=new java.util.ArrayList<FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface>();
            
            }
             
            SimilarProducts aux=new SimilarProducts();
            
            aux.setPriceDiference(0.0);
            
            aux.setProducto1(new Producto());
    
            FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface similar=new FranchiseAdministrator.Product.Classes.EdenSimilarProduct();
            
            similar.setImage(imageRoot);
            
            similar.setSimilarProduct(aux);
            
            
            
            similar.setProductId(productId);
            
            if(!similar.findSimilar(productId, this.similarProducts)){
            
                  this.similarProducts.add(similar);
            
            }
            
      
        
        }catch(RuntimeException ex){
        
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void removeSimilarProduct(AjaxBehaviorEvent event){
    
        try{
        
            if(this.similarProducts!=null && !this.similarProducts.isEmpty()){
            
                this.similarProducts.remove(this.similarProducts.size()-1);
            
            }
        
        }
        catch(RuntimeException ex){
        
        
        }
       
    }
    
    
    public java.util.List<Entities.Producto>getProductListUpdate(){
        
        try{
    
        return this._agregarProductoController.getProductList();
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    public void deleteSimilar(SimilarProductsLayoutInterface similar){
    
    try{
    
        
        System.out.print("You are deleting similar");
        
    this._agregarProductoController.deleteSimilarProductList(similar);
      
      
    
    }
    catch(RuntimeException ex){
    
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
   
    
     public void similarSelectOneChanges(AjaxBehaviorEvent event){
    
         try{
         
   Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
   
   if(event.getComponent() instanceof UIInput){
   
   System.out.print("Component is instance of UIInput");
   
   UIInput input=(UIInput) event.getComponent();
   
   System.out.print("UIInput Value "+input);
   
   }
   
   
   for(Map.Entry<String,String>entry:requestMap.entrySet()){
   
   
   System.out.print(entry.getKey()+"-"+entry.getValue());
   
   
   }
    
     }
         catch(NullPointerException ex){
         
             ex.printStackTrace(System.out);
         
         }
   
    }

     
 
     
     public void test(Entities.ProductoPK idProducto,String imageRoot){
     
         if(this.dragged){
         
         System.out.print("Testing dragged "+this.productoController.find(idProducto).getNombre());
         
         System.out.print("Image Root "+imageRoot);
         
         this.addSimilarProduct(imageRoot,idProducto);
         
         if(this.similarProducts!=null && !this.similarProducts.isEmpty()){
         
             System.out.print("Similar Products size "+this.similarProducts.size());
             
             System.out.print("Similar products List is not null");
             
             this.dragRender=false;
         
             org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:similars");
             
             org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:dragImage");
             
         }
         
         else{
         
             this.dragRender=true;
         
         }
         
         }
         
         dragged=false;
     }
   
     public void dragProduct(org.primefaces.event.DragDropEvent event){
           
         
        try{ 
        java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
        
        for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
        
        System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
       
        this.dragged=true;
        
        this.setSessionAuxiliarComponent(this.dragRender);
     
        
        System.out.print("DraggEnd value "+this.dragRender);
        
        int idProduct=Integer.parseInt(requestMap.get("hiddenDrag"));
        
        int idCategory=Integer.parseInt(requestMap.get("IdCategory"));
        
        int idFranchise=Integer.parseInt(requestMap.get("IdFranchise"));
        
        Entities.ProductoPK productPK=new Entities.ProductoPK();
        
        productPK.setCategoriaFranquiciaIdfranquicia(idFranchise);
        
        productPK.setCategoriaIdcategoria(idCategory);
        
        productPK.setIdproducto(idProduct);
        
        String imageRoot="";
        
        for(FranchiseAdministrator.Product.Classes.ProductoEdenInterface productEden:this.products){
        
        if(productEden.getIdProducto().equals(productPK)){
        
            imageRoot=productEden.getImage();
        
            break;
        }
        
        }
        
        System.out.print(imageRoot);
        
            this.addSimilarProduct(imageRoot,productPK);
         
         if(this.similarProducts!=null && !this.similarProducts.isEmpty()){
         
             System.out.print("Similar Products size "+this.similarProducts.size());
             
             System.out.print("Similar products List is not null");
             
             this.dragRender=false;
         
             org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:similars");
             
             org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:dragImage");
             
         }
         
         else{
         
             this.dragRender=true;
         
         }
         
     }
        catch(NumberFormatException | NullPointerException ex){
        
            Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
  
        }
     }
     
     public List<Entities.Addition>getAdditionsResult(){
         
         try{
         
     return this.additionController.findAll();
     
         }
         catch(NullPointerException ex){
                 
                 ex.printStackTrace(System.out);
                 
                 return null;
                 
                 }
     
     }
     
     
     
   public void deleteSimilarList(javax.faces.event.AjaxBehaviorEvent event, Entities.ProductoPK idProduct){
   
       try{
       
         
           
           System.out.print("Id Product Ajax "+idProduct);
           
       this._agregarProductoController.deleteSimilarProduct(idProduct);
           
       }
       catch(Exception ex){
       
           Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
       
       }
   
   }
   
   
   public void dragAddition(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
       this._agregarProductoController.dragAddition();
      
   }
   catch(Exception ex){
   
       Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
   }
     
   
   
   
   private String getImageAddition(int idAddition){
   
       try{
           
           if(getSession().getAttribute("selectedOption")instanceof Entities.Franquicia){
           
               Entities.Franquicia franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
               
               Entities.AdditionPK additionPK=new Entities.AdditionPK();
               
               additionPK.setIdaddition(idAddition);
               
               additionPK.setFranquiciaIdfranquicia(franchise.getIdfranquicia());
               
               
                for(FranchiseAdministrator.Product.Classes.AdditionEden addition:this.additionList){
           
                    if(addition.getAdditionPK().equals(additionPK)){
                    
                        System.out.print("found");
                        
                        return addition.getImage();
                    
                      
                    }
           
                }
               
           }
           else{
           
              throw new IllegalArgumentException("Object of type "+getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName()); 
           
           }
           
          
           
          return"";
       
       }
       catch(Exception ex){
       
           Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
       
   }
   
   public void deleteAddition(Entities.AdditionPK additionPK){
   
       try{

         this.deleteDragAdditionList(additionPK);
           
       }
       catch(Exception ex){
       
       Logger.getLogger(AgregarProducto.class.getName());
       
       }
   
   }
   
   private void deleteDragAdditionList(Entities.AdditionPK additionPK){
   
     this._agregarProductoController.deleteAddition(additionPK);
   
   }
   
   public void removeImage(javax.faces.event.AjaxBehaviorEvent ajaxBehavior){
   
   try{
   
     java.util.Map<String,String>requestMap=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
     
     for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
     
         System.out.print(entry.getKey()+"-"+entry.getValue());
     
     }
      this.fileUpload.removeImage(requestMap.get("deleteImage"));
   
   }
   catch(NullPointerException ex)
   {
   
       Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
      
   
   }
   
   }
   
   
   public void updateDeleteKey(org.primefaces.event.SelectEvent event){
   
       try{
       
          System.out.print(event.getObject());
          
          System.out.print("Delete Product "+this.deletePK);
       
       }
       catch(NullPointerException | NumberFormatException ex){
       
         ex.printStackTrace(System.out);
       
       }
   
   }
   
   
   public java.util.List<Entities.Inventario>getInventoryList(){
       
       try{
   
       return this._agregarProductoController.getInventoryUpdateList();
       
       }
       catch(NullPointerException ex){
       
           ex.printStackTrace(System.out);
       
           return null;
       }
   
   }
   
   public void addToSimilarList(javax.faces.event.AjaxBehaviorEvent event){
   
       try{
           
          
           this._agregarProductoController.addSimilarProductList(this.similarProductList);
           
          // System.out.print("SIMILAR PRODUCT SIZE "+this.similarList.size());
           
           //SimilarProductList
           /*
           System.out.print(this.similarProductList);
           
           if(this.similarProductList!=null){
           
           FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface similarProduct=new FranchiseAdministrator.Product.Classes.SimilarProductsLayout();
           
           similarProduct.setProduct(this.productoController.find(this.similarProductList));
           
           similarProduct.setProductImages(new java.util.ArrayList<String>());
           
           System.out.print("Similar Products Images "+this.fileUpload.getImages().size()+"-"+this.fileUpload.getPhotoList().size());
           
           System.out.print("Photo List Size "+this.fileUpload.getPhotoList().size());
           
           System.out.print("Images Size "+this.fileUpload.getImages().size());
           
           for(String aux:this.fileUpload.getImages()){
           
           System.out.print("Images Name "+aux);
           
           }
           
           for(String aux:this.productoController.getImages(this.similarProductList)){
           
           similarProduct.getProductImages().add(aux);
           
           int remove=this.fileUpload.getImages().size()-1;
           
           System.out.print("Remove Index "+remove);
           
           this.fileUpload.getPhotoList().remove(remove);
           
           this.fileUpload.getImages().remove(remove);
          
           }
                     
          
           similarProduct.setPrice(this.productoController.find(this.similarProductList).getPrecio());
      
           this.similarList.add(similarProduct);
           }
       
           System.out.print("File Upload List sizes "+this.fileUpload.getPhotoList().size()+"-"+this.fileUpload.getImages().size());
           */
       }
       catch(RuntimeException ex){
       
           Logger.getLogger(AgregarProducto.class.getName()).log(Level.SEVERE,null,ex);
        
       }
   
   }
   
   public void deleteUpdateProduct(javax.faces.event.AjaxBehaviorEvent event){
   
       try{
       
          java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
          
          for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
          
              System.out.print(entry.getKey()+"-"+entry.getValue());
          
          }
          
          this.fileUpload.removeImage(requestMap.get("hiddenImage"));
       }
       catch(RuntimeException ex){
       
           
           
       }
   
   }
   
   public void captureBarCode(org.primefaces.event.CaptureEvent event){
   
   try{
   
       System.out.print("Capture event");
       
       Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
       
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
       
       
       java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode");
   
       if(file.exists()){
       
          filesManagement.cleanFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode");
       
       }
       else{
       
       filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator, "barcode");
       
       }
       
       
       byte[]data=event.getData();
       
       String fileName=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode"+java.io.File.separator+"barcode.png";
       
       
       java.io.InputStream inputStream=new java.io.ByteArrayInputStream(data);
       
       BufferedImage bufferedImage = ImageIO.read(inputStream);
       
       int width=500;
       
       int height=500;
       
       int imageWidth=bufferedImage.getWidth();
       
       int imageHeight=bufferedImage.getHeight();
       
       double scaleX = (double)width/imageWidth;
    double scaleY = (double)height/imageHeight;
    AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaleX, scaleY);
    AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform, AffineTransformOp.TYPE_BILINEAR);

    
    BufferedImage bi=bilinearScaleOp.filter(
        bufferedImage,
        new BufferedImage(width, height, bufferedImage.getType()));
    
       ImageIO.write(bi, "png",new File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode","barcode.png"));
  
       
  
       
       _barCodeImage=java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode"+java.io.File.separator+"barcode.png";
       
       
       Clases.Barcode barcode=new Clases.BarcodeImplementation();
       
       
       
       
       this.barcode=barcode.getCode(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode", "barcode.png");
       
       
       //No Food Franchise
       if(this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"addproduct.xhtml") || this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"update-product.xhtml")){
       
       this._businessProduct.getBarcode().setBarcode(this.barcode);
       
        if(this.agregarProducto.findProductByBarcode(this.barcode)!=null)
           {          
               Entities.Producto product=this.agregarProducto.findProductByBarcode(this.barcode);
           
               System.out.print("PRODUCT FOUND "+product.getNombre());
               
               _businessProduct.setNombre(product.getNombre());
               
               _businessProduct.setPrecio(product.getPrecio());
               
               _businessProduct.setDescription(product.getDescription());
               
               this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+this.getSession().getAttribute("username").toString());
               
               this.agregarProducto.loadImagesBarcode(this.barcode);
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:nombre");
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:price");
               
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:description");
           
               org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:galleria");
               
           }
       
       
       }
       
       //FoodFranchise
       
       if(this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"AgregarProducto.xhtml") || this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"AgregarProducto.xhtml")|| this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"updateProduct.xhtml"))
       {
       
           if(this.agregarProducto.findProductByBarcode(this.barcode)!=null)
           {
           
           Entities.Producto product=this.agregarProducto.findProductByBarcode(this.barcode);
           
           this.nombre=product.getNombre();
           
           this.precio=product.getPrecio();
           
           this.description=product.getDescription();
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:nombre");
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:precio");
           
           org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:description");
           
           }
           
           
       }
       
        org.primefaces.context.RequestContext.getCurrentInstance().update("form:messages");
        
        org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:barcodeResult");
     
       
   }
   catch(IOException | NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
      
   
   }
   
   }
   
   public void uploadBarcode(org.primefaces.event.FileUploadEvent event){
   
   try{
   
      
       
       this._agregarProductoController.updateBarcode(event);
       
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
      
       
       org.primefaces.context.RequestContext.getCurrentInstance().update("form:tabView:barcodeResult");
     
   }
   catch(NullPointerException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
  
   }
   
  
   
   }
   
   public void scanCode(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
       
       java.io.File file=new java.io.File(servletContext.getRealPath("")+_barCodeImage);
       
       System.out.print("File to Examine "+file.getPath());
       
       if(file.exists()){
       
           Clases.Barcode barcode=new Clases.BarcodeImplementation();
           
           String []parts=_barCodeImage.split("/");
           
           
           this.barcode=barcode.getCode(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString()+java.io.File.separator+"barcode", parts[parts.length-1]);
       
            if(this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"business"+java.io.File.separator+"product"+java.io.File.separator+"addproduct.xhtml")){
       
           if(this.agregarProducto.findProductByBarcode(this.barcode)!=null)
           {
           
               Entities.Producto product=this.agregarProducto.findProductByBarcode(this.barcode);
               
               _businessProduct.setNombre(product.getNombre());
               
               _businessProduct.setPrecio(product.getPrecio());
               
               _businessProduct.setDescription(product.getDescription());
           
           }
       }
            
       }
       else{
       
           javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("There is no a barcode to scan");
           
           msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
           
           this.getContext().addMessage(null,msg);
           
           
       }
   
   }
   catch(NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
   }
   
   
   }
  
   
   public void showAuxiliar(javax.faces.event.ActionEvent event){
   
       try
       {
   System.out.print("Auxiliar Value "+this.fileUpload.getAuxiliar());
   
   System.out.print("Barcode Value "+this.barcode);
   
   }
       catch(NullPointerException ex){
       
           ex.printStackTrace();
       
       }
   
   }
   
   private Entities.Producto _businessProduct;
   
   public Entities.Producto getBusinessProduct(){
   
       return _businessProduct;
   
   }
   
   public void setBusinessProduct(Entities.Producto businessProduct){
   
   _businessProduct=businessProduct;
   
   }
   
   
   @annotations.MethodAnnotations(author="Luis Negrete",date="23/08/2015",comments="Set Type Method")
   public void preRenderView(javax.faces.event.ComponentSystemEvent event){
   
   try{
   
       String view=this.getContext().getViewRoot().getViewId();
       
       System.out.print("View value "+view);
        
       
       switch(view){
       
           
           case "/adminFranquicia/business/product/addproduct.xhtml":{
           
               System.out.print("You are in "+view+" preRenderView method");
               
               if(_businessProduct==null && this.getSession().getAttribute("selectedOption")instanceof Entities.Franquicia){
               
                   Entities.Franquicia franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
                   
                   _businessProduct=new Entities.Producto();
                   
                   Entities.ProductoPK key=new Entities.ProductoPK();
                   
                   key.setCategoriaFranquiciaIdfranquicia(franchise.getIdfranquicia());
                   
                   _businessProduct.setProductoPK(key);
                   
                   _businessProduct.setCategoria(new Entities.Categoria());
                   
                   _businessProduct.getCategoria().setCategoriaPK(new Entities.CategoriaPK());
               
                   _businessProduct.setComponeCollection(new java.util.ArrayList<Entities.Compone>());
                   
                   _businessProduct.setPrecio(0.0);
                   
               }
               
               if(!this.fileUpload.getActionType().getActionType().equals(view))
               {
               
               javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
               
               this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
               
               this.fileUpload.setActionType(new Clases.ActionType(view));
               
               System.out.print("File Upload photos size "+this.fileUpload.getPhotoList().size());
               
             
               
               }
               
               
               if(_businessProduct.getBarcode()==null){
                
                    _businessProduct.setBarcode(new Entities.Barcode());
                
                }
               
               if(_businessProduct.getBarcode().getBarcode()==null || _businessProduct.getBarcode().getBarcode().equals("")){
               
                     _barCodeImage="/images/noImage.png";
               
               }
               
               break;
           
           }
           
           case "/adminFranquicia/business/product/list-products.xhtml":{
           
               if(!this.fileUpload.getActionType().getActionType().equals(view))
               {
               
               javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
               
               this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
               
               this.fileUpload.setActionType(new Clases.ActionType(view));

            
               
               }
               
                  _franchiseAdministratorProductController.initFranchiseList();

                
                System.out.print("Product List Size "+_franchiseAdministratorProductController.getProductsFranchiseList().size());
               
               break;
           
               
              
           }
           
           case "/adminFranquicia/business/product/update-product.xhtml":{
           
              if(this.getSessionAuxiliarComponent() instanceof Entities.Producto){
               
                   _businessProduct=(Entities.Producto)this.getSessionAuxiliarComponent();
 
                if(_businessProduct.getBarcode()==null){
                
                    _businessProduct.setBarcode(new Entities.Barcode());
                
                }
                
                 if(_businessProduct.getBarcode().getBarcode()==null || _businessProduct.getBarcode().getBarcode().equals("")){
               
                     _barCodeImage="/images/noImage.png";
               
               }
                   
               }
                
                else{
               
                   throw new IllegalArgumentException("Object of type "+this.getSessionAuxiliarComponent().getClass().getName()+" must be of tyoe "+Entities.Franquicia.class.getName());
               
               }   
               
          if(!this.fileUpload.getActionType().getActionType().equals(view)){
          
              javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
          
              this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
              
              this.fileUpload.setActionType(new Clases.ActionType(view));
         
           
              _franchiseAdministratorProductController.loadImagesUpdateProduct(_businessProduct);
               
              
          }      
           
      
           
           }
       
       }
   
   }
   catch(NullPointerException | javax.ejb.EJBException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
 
   
   }
   
   }
   
   public java.util.List<Entities.Categoria>getCategoryList(){
   
       try{
       
   return _franchiseAdministratorProductController.getCategoryList();
   
       }
       catch(NullPointerException ex){
       
           ex.printStackTrace(System.out);
           
           return null;
       
       }
   
   }
   
   
   
   private org.primefaces.model.LazyDataModel<Entities.Compone> lazyModel;
   
   public org.primefaces.model.LazyDataModel<Entities.Compone>getLazyModel(){
   
   
       
       return this.lazyModel;
   
   }
   
   public void setLazyModel(org.primefaces.model.LazyDataModel<Entities.Compone>lazyModel){
   
   this.lazyModel=lazyModel;
   
   }
   
   private Entities.Compone _selectedComponeItem;
   
   public Entities.Compone getSelectedComponeItem(){
   
       return _selectedComponeItem;
   
   }
   
   public void setSelectedComponeItem(Entities.Compone selectedComponeItem){
   
   _selectedComponeItem=selectedComponeItem;
   
   }
   
  
   
   public void addComponeItem(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
      java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
      
      for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
      
      System.out.print(entry.getKey()+"-"+entry.getValue());
       
      }
      
      String value=requestMap.get("form:tabView:inventory_input");
      
      if(value!=null && this.getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
      
        Entities.Inventario inventory=_franchiseAdministratorProductController.findInventoryByConverter(value);
        
        Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
        
        System.out.print(inventory.getNombre());
        
        if(_businessProduct.getComponeCollection()==null){
        
        _businessProduct.setComponeCollection(new java.util.ArrayList<Entities.Compone>());
        
        
        }
        
        
        if(!_franchiseAdministratorProductController.checkComponeExistenceByInventory(_businessProduct,inventory.getInventarioPK())){
        
            
        String view=this.getContext().getViewRoot().getViewId();    
            
        Entities.Compone compone=new Entities.Compone();
        
        Entities.ComponePK componePK=new Entities.ComponePK();
        
        componePK.setInventarioFranquiciaIdFranquicia(inventory.getInventarioPK().getFranquiciaIdFranquicia());
        
        componePK.setInventarioIdinventario(inventory.getInventarioPK().getIdinventario());
           
        componePK.setProductoCategoriaFranquiciaIdFranquicia(franchise.getIdfranquicia());
        
        if(view.equals("/adminFranquicia/business/product/update-product.xhtml")){
        
           componePK.setProductoIdproducto(_businessProduct.getProductoPK().getIdproducto());
        
           componePK.setProductoCategoriaIdCategoria(_businessProduct.getProductoPK().getCategoriaIdcategoria());
           
        }
        
        compone.setComponePK(componePK);
        
        compone.setInventario(inventory);
        
        compone.setCantidadInventario(0.0);
        
        compone.setProducto(_businessProduct);
        
        _businessProduct.getComponeCollection().add(compone);
        
        }
        
        else{
        
        javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("There is an inventory with this name already");
          
        msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
        
        this.getContext().addMessage(null,msg);
        
        }
        
      
      }
     
   }
   catch(NullPointerException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
   
   }
   
   }
   
   public void deleteComponeItem(Entities.Compone compone){
   
   try{
   
    _businessProduct.getComponeCollection().remove(compone);
   
   }
   catch(NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
     
   
   }
   
   }
   
   public java.util.List<Entities.Producto>getProductsList(){
       
       try{
   
   return _franchiseAdministratorProductController.productList();
   
       }
       catch(NullPointerException ex){
           
           ex.printStackTrace(System.out);
           
           return null;
           
       }
   
   }
   
   public void createBusinessProduct(javax.faces.event.ActionEvent event){
   
       try{
           
  
       _franchiseAdministratorProductController.createBusinessProduct(_businessProduct);
       
       javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Product Created");
       
       _businessProduct.getProductoPK().setCategoriaFranquiciaIdfranquicia(_businessProduct.getCategoria().getCategoriaPK().getFranquiciaIdfranquicia());
       
       _businessProduct.getProductoPK().setCategoriaIdcategoria(_businessProduct.getCategoria().getCategoriaPK().getIdcategoria());
       
       
       
       this.getContext().addMessage(null, msg);
       
       }
       catch(NullPointerException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       
       
       }
   
   
   }
   
   public java.util.List<Layouts.EdenLayout<Entities.Producto>>getProductsFranchiseList(){
   
       try{
       
   return _franchiseAdministratorProductController.getProductsFranchiseList();
   
       }
       catch(NullPointerException ex){
       
           ex.printStackTrace(System.out);
           
           return null;
           
       }
   
   }
   
   public void AddSimilarProductBusiness(javax.faces.event.AjaxBehaviorEvent event){
   
   try{
   
       String view=this.getContext().getViewRoot().getViewId();
       
       System.out.print("View Id Name "+view);
       
       
       switch(view){
       
           case "/adminFranquicia/business/product/addproduct.xhtml":{
           
               java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
               
               for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
               
                   System.out.print(entry.getKey()+"-"+entry.getValue());
               
               }
               
               String value=requestMap.get("form:tabView:productSelector_input");
               
               if(value!=null && !value.equals(""))
               {
               
                   if(_businessProduct.getSimilarProductsCollection()==null){
                   
                       _businessProduct.setSimilarProductsCollection(new java.util.ArrayList<Entities.SimilarProducts>());
                   
                   }
                   
                   Entities.Producto product=(Entities.Producto)_franchiseAdministratorProductController.getByConverter(value);
               
                   System.out.print("Selected Product Name "+product.getNombre());
                   
                   
                   Entities.SimilarProducts similar=new Entities.SimilarProducts();
                   
                   Entities.SimilarProductsPK key=new Entities.SimilarProductsPK();
                   
                   
                   similar.setProducto(_businessProduct);
                   
                   similar.setProducto1(product);
                   
                   if(!_franchiseAdministratorProductController.findSimilarCoincidence((java.util.List<Entities.SimilarProducts>)_businessProduct.getSimilarProductsCollection(), product)){
                   
                   _businessProduct.getSimilarProductsCollection().add(similar);
                  
                   }
                   
                   else{
                   
                   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("You have already added this product");
                   
                   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                   
                   this.getContext().addMessage(null,msg);
                   
                   }
                   
               }
               
               
               break;
           
           }
           
           case "/adminFranquicia/business/product/update-product.xhtml":{
           
           System.out.print("Updating Product");
           
           
           for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
           
           System.out.print(entry.getKey()+"-"+entry.getValue());
           
           
           }
           
            String value=this.getContext().getExternalContext().getRequestParameterMap().get("form:tabView:productSelector_input");
               
               if(value!=null && !value.equals(""))
               {
                   System.out.print("Value is not null");
               
                   if(_businessProduct.getSimilarProductsCollection()==null){
                   
                       _businessProduct.setSimilarProductsCollection(new java.util.ArrayList<Entities.SimilarProducts>());
                   
                   }
                   
                   Entities.Producto product=(Entities.Producto)_franchiseAdministratorProductController.getByConverter(value);
               
                   Entities.SimilarProducts similar=new Entities.SimilarProducts();
                   
                   Entities.SimilarProductsPK key=new Entities.SimilarProductsPK();
                   
                   key.setProductoCategoriaFranquciaIdFranquicia(_businessProduct.getProductoPK().getCategoriaFranquiciaIdfranquicia());
                   
                   key.setProductoCategoriaIdCategoria(_businessProduct.getProductoPK().getCategoriaIdcategoria());
                   
                   key.setProductoIdproducto(_businessProduct.getProductoPK().getIdproducto());
                   
                   key.setProductoCategoriaFranquiciaIdFranquicia1(product.getProductoPK().getCategoriaFranquiciaIdfranquicia());
                   
                   key.setProductoCategoriaIdCategoria1(product.getProductoPK().getCategoriaIdcategoria());
                   
                   key.setProductoIdproducto1(product.getProductoPK().getIdproducto());
                   
                   similar.setSimilarProductsPK(key);
                   
                   similar.setProducto(_businessProduct);
                   
                   similar.setProducto1(product);
                   
                   if(!_businessProduct.getSimilarProductsCollection().contains(similar)){
                   
                   _businessProduct.getSimilarProductsCollection().add(similar);
                   
                   System.out.print("BusinessProduct size "+_businessProduct.getSimilarProductsCollection().size());
                  
                   }
                   else{
                   
                   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("You have already added this product");
                   
                   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                   
                   this.getContext().addMessage(null,msg);
                   
                   }
                   
               }
              
           break;
           }
       
       }
   
   }
   catch(NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
       
   
   }
   
   }
   
   public void deleteSimilarButinessProduct(Entities.SimilarProducts similar){
   
       try{
       
           String view=this.getContext().getViewRoot().getViewId();
           
           switch(view){
           
           case"/adminFranquicia/business/product/addproduct.xhtml":{
           
               if(_businessProduct.getSimilarProductsCollection1()!=null && !_businessProduct.getSimilarProductsCollection1().isEmpty()){
               
               }
               
               
           break;
           
           }
           
           }
       
       }
       catch(NullPointerException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
        
       }
   
   }
   
   
   public void removeSimilarList(javax.faces.event.AjaxBehaviorEvent event,Entities.SimilarProducts product){
   
   try{
   
     //  System.out.print("Product to Remove "+product.getNombre());
       
    
       if(_businessProduct.getSimilarProductsCollection()!=null && !_businessProduct.getSimilarProductsCollection().isEmpty())
       
       {
       
       for(int i=0;i<_businessProduct.getSimilarProductsCollection().size();i++){
       
       if(((java.util.List<Entities.SimilarProducts>)_businessProduct.getSimilarProductsCollection()).get(i).getProducto1().equals(product.getProducto1())){
       
          ((java.util.List<Entities.SimilarProducts>)_businessProduct.getSimilarProductsCollection()).remove(i);
       
          
          break;
          
       }
       
       }
       
       }
           
      for(Entities.SimilarProducts aux:_businessProduct.getSimilarProductsCollection()){
       
       System.out.print(aux.getProducto1().getNombre());
       
       }
       
   }
   catch(NullPointerException ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
    
   
   }
   
   }
   
   
   public void updateBusinessProduct(javax.faces.event.ActionEvent event){
   
       try{
       
           if(this.barcode!=null){
           
               Entities.Barcode code=new Entities.Barcode();
               
               code.setBarcode(barcode);
               
               _businessProduct.setBarcode(code);
           
           }
           
           _franchiseAdministratorProductController.updateProduct(_businessProduct);
       
           javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Product Updated Succesfully");
           
           this.getContext().addMessage(null,msg);
           
       }
       
       catch(StackOverflowError | NullPointerException ex){
       
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
            
       
       }
   
   }
   
   
   @annotations.MethodAnnotations(author="Luis Negrete",date="31/08/2015",comments="Delete Image fro FileUploadBean")
   public void deleteBusinessImage(javax.faces.event.AjaxBehaviorEvent event,String path){
   
       try{
       
       System.out.print("Image to delete "+path);
       
       this.fileUpload.removeImage(path);
       }
       catch(NullPointerException ex){
       
           ex.printStackTrace(System.out);
       
       }
   
   }
   
   
}

