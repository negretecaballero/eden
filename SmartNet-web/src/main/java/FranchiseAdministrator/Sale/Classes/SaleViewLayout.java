/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.Classes;

/**
 *
 * @author luisnegrete
 */
public interface SaleViewLayout {
   
    Entities.Sale getSale();
    
    void setSale(Entities.Sale sale);
    
    java.util.List<String>getImages();
    
    void setImages(java.util.List<String>images);
    
}
