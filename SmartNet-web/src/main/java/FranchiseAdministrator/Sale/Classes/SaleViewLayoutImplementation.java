/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.Classes;

/**
 *
 * @author luisnegrete
 */
public class SaleViewLayoutImplementation implements SaleViewLayout{

    
  private  Entities.Sale sale;
  
  private java.util.List<String>images;
  
  @Override
  public Entities.Sale getSale(){
  
  return this.sale;
  
  }
  @Override
  public void setSale(Entities.Sale sale){
  
  this.sale=sale;
  
  }
  @Override
  public java.util.List<String>getImages(){
  
      return this.images;
  
  }
  @Override
  public void setImages(java.util.List<String>images){
  
  this.images=images;
  
  }
  
  public SaleViewLayoutImplementation(Entities.Sale sale,java.util.List<String>images){
  
      this.sale=sale;
      
      this.images=images;
  
  }
  
  public SaleViewLayoutImplementation(){
  
  }
  
}
