/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.Controller;

/**
 *
 * @author luisnegrete
 */
public interface SaleViewController {
    
    void initPreRenderView();
    
    boolean postValidateAddSale(String name);
    
    boolean checkDates(java.util.Date startDate, java.util.Date endDate);
    
    ENUM.TransactionStatus createSale(String name, double price, java.util.Date startDate,java.util.Date finishDate,java.util.List<Entities.SaleHasProducto>saleHasProductoList,java.util.List<Entities.SaleHasMenu>saleHasMenuList);
    
    void addSaleHasProducto();
    
    void addSaleHasMenu();
    
    void deleteSaleHasMenu();
    
    void deleteSaleHasProducto();
    
    void deleteSale(Entities.SalePK salePK);
    
    void handleFileUpload(org.primefaces.event.FileUploadEvent event);
    
    void addUpdateSaleHasMenu();
    
    void addUpdateSaleHasProducto();
    
    void deleteUpdateSaleHasProducto(Entities.SaleHasProducto saleHasProducto);
    
    void deleteUpdateSaleHasMenu(Entities.SaleHasMenu saleHasMenu);
    
    ENUM.TransactionStatus updateSale(String name, double price,java.util.Date startDate,java.util.Date finishDate,java.util.List<Entities.SaleHasProducto>saleHasProductoList,java.util.List<Entities.SaleHasMenu>saleHasMenuList);
    
    boolean postValidateUpdateSale(String name);
    
}
