/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.Controller;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class SaleViewControllerImplementation implements SaleViewController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.SaleController _saleController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.ProductoControllerInterface _productController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIBeans.MenuControllerInterface _menuController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SaleHasProductoControllerInterface _saleHasProductoController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SaleHasMenuControllerInterface _saleHasMenuController;
    
    @Override
    public void initPreRenderView(){
    
        try{
        
            String viewId=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot().getViewId();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
            
            System.out.print("PRERENDER VIEW SALE CALLED");
            
            switch(viewId){
            
                case "/adminFranquicia/add-sale.xhtml":{
                
                         System.out.print("PRERENDER VIEW ADDSALE CALLED");
                    
                         
                         
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                        System.out.print("PRERENDER VIEW ADD SALE");
                        
                        saleView.setName("");
                        
                        saleView.setPrice(0.0);

                        saleView.setStartDate(new java.util.Date());
                        
                        saleView.setFinishDate(new java.util.Date());
                        
                        saleView.setSaleHasMenuList(new java.util.ArrayList<Entities.SaleHasMenu>());
                        
                        saleView.setSaleHasProductoList(new java.util.ArrayList<Entities.SaleHasProducto>());
                        
                       _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                        
                       _fileUpload.setActionType(new Clases.ActionType(viewId));
                       
                    }
                break;
                }
                
                case "/adminFranquicia/list-sale.xhtml":{
                
                    
                    
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId) && session.getAttribute("selectedOption")instanceof Entities.Franquicia){
                
                        Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                        
                         _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
                        
                        
                        if(saleView!=null){
                        
                            java.util.List<Entities.Sale>saleList=this._saleController.findByFranchise(franchise.getIdfranquicia());
                            
                            saleView.setSales(IteratorUtils.toList(this.getSaleViewLayoutList(saleList).iterator()));
                        
                        }
                        
                       
                        _fileUpload.setActionType(new Clases.ActionType(viewId));
                
                     }
                break;
                }
                
                case "/adminFranquicia/update-sale.xhtml":{
                
                    if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId)){
                    
                           _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+session.getAttribute("username").toString());
                 
                        
                        if(saleView!=null && session.getAttribute("auxiliarComponent")instanceof Entities.Sale){
                        
                            Entities.Sale sale=(Entities.Sale)session.getAttribute("auxiliarComponent");
                            
                            saleView.setName(sale.getName());
                            
                            saleView.setPrice(sale.getValue());
                            
                            _saleController.loadImages(sale, session.getAttribute("username").toString());
                        
                            saleView.setStartDate(sale.getStartDate());
                            
                            saleView.setFinishDate(sale.getEndDate());
                            
                            saleView.setSaleHasProductoList(IteratorUtils.toList(this._saleHasProductoController.findBySale(sale.getSalePK()).iterator()));
                            
                            saleView.setSaleHasMenuList(IteratorUtils.toList(_saleHasMenuController.findBySale(sale.getSalePK()).iterator()));
                            
                        }
                        
                    
                    _fileUpload.setActionType(new Clases.ActionType(viewId));
                    
                    }
                break;
                
                }
            
            }
        
            if(_fileUpload.getImages()!=null && !_fileUpload.getImages().isEmpty()){
                         
                             for(String aux:_fileUpload.getImages()){
                             
                                 System.out.print("SWITCH "+aux);
                             
                             }
                         
                         }
            
            else{
            
                System.out.print("FileUpload is NULL");
                
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    
    }
    
    @Override
    public boolean postValidateAddSale(String name){
    
        try{
        
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
           if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
           
               Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
               
               for(Entities.Sale sale:_saleController.findByFranchise(franchise.getIdfranquicia())){
               
               if(name.toLowerCase().equals(sale.getName().toLowerCase())){
               
                   return true;
               
               }
               
               }
               
           }
           
            return false;
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return false;
            
        }
    
    }
    
    private final java.util.List<String>getSaleImageList(Entities.Sale sale){
    
        try{
        
            if(sale.getImagenCollection()!=null && !sale.getImagenCollection().isEmpty()){
            
                javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                java.util.List<String>imageList=new java.util.ArrayList<String>();
            
                Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
                
                String path=servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator+sale.getName();
                
                java.io.File file=new java.io.File(path);
                
                if(!file.exists() || !file.isDirectory()){
                
                    filesManagement.createFolder(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString()+java.io.File.separator, sale.getName());
                
                }
                else{
                
                    filesManagement.cleanFolder(path);
                
                }
                
                for(Entities.Imagen image:sale.getImagenCollection()){
                
                   _fileUpload.loadImagesServletContext(image, session.getAttribute("username").toString()+java.io.File.separator+sale.getName());
                
                   String aux=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
                   
                   imageList.add(aux);
                   
                   _fileUpload.getImages().remove(_fileUpload.getImages().size()-1);
                   
                   _fileUpload.getPhotoList().remove(_fileUpload.getPhotoList().size()-1);
                   
                }
                
                return imageList;
                
            }
            
            return null;
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    private final SessionClasses.EdenList<FranchiseAdministrator.Sale.Classes.SaleViewLayout>getSaleViewLayoutList(java.util.List<Entities.Sale>saleList){
    
    try{
    
        if(saleList!=null && !saleList.isEmpty()){
        
            SessionClasses.EdenList<FranchiseAdministrator.Sale.Classes.SaleViewLayout>results=new SessionClasses.EdenList<FranchiseAdministrator.Sale.Classes.SaleViewLayout>();
            
            for(Entities.Sale sale:saleList){
            
               FranchiseAdministrator.Sale.Classes.SaleViewLayout saleViewLayout=new FranchiseAdministrator.Sale.Classes.SaleViewLayoutImplementation();
            
               saleViewLayout.setSale(sale);
               
             
               
               if(sale.getImagenCollection()==null || sale.getImagenCollection().isEmpty()){
               
                     saleViewLayout.setImages(new java.util.ArrayList<String>());
                   
                   saleViewLayout.getImages().add("/images/noImage.png");
               
               }
               
               else{
               
                   saleViewLayout.setImages(this.getSaleImageList(sale));
               
               }
             
               results.addItem(saleViewLayout);
               
            }
        
            return results;
            
        }
        
    return null;
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    return null;
  
    }
    
    }
    
    @Override
    public boolean checkDates(java.util.Date startDate, java.util.Date endDate){
    
        try{
            
            java.text.SimpleDateFormat dateFormat=new java.text.SimpleDateFormat("MM/dd/yyyy");
        
            System.out.print("Start Date"+startDate);
            
            System.out.print("Finish Date "+endDate);
            
            if(startDate!=null && endDate!=null){
            
            if(startDate.before(endDate)){
            
            
                return true;
            
            }
            
            }
            
            return false;
        
        }
        catch(Exception  ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
    
    }
    
    @Override
    public void addSaleHasProducto(){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        
        if(saleView!=null && session.getAttribute("selectedOption")instanceof Entities.Franquicia && saleView.getSaleHasProductoList().size()<this._productController.getAutoFranchiseProducts().size()){
            
            Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
        
          Entities.SaleHasProducto saleHasProducto=new Entities.SaleHasProducto();
           
           Entities.SaleHasProductoPK saleHasProductoPK=new Entities.SaleHasProductoPK();
         
           saleHasProductoPK.setProductoCategoriaFranquiciaIdFranquicia(franchise.getIdfranquicia());
           
          
         
           saleHasProductoPK.setSaleFranquiciaIdfranquicia(franchise.getIdfranquicia());
           
           
           
           //product.setProductoPK(this.getProducts().get(this.saleHasProductoList.size()).getProductoPK());
           
           saleHasProducto.setProducto(new Entities.Producto());
           
           saleHasProducto.getProducto().setProductoPK(new Entities.ProductoPK());
           
           saleHasProducto.setSaleHasProductoPK(saleHasProductoPK);
           
           saleHasProducto.setQuantityLimit(0);
           
           saleView.getSaleHasProductoList().add(saleHasProducto);
        
        }
        
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
    @Override
    public void deleteSaleHasProducto(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
            
            if(saleView!=null){
            
            if(saleView.getSaleHasProductoList()!=null && !saleView.getSaleHasProductoList().isEmpty()){
            
            saleView.getSaleHasProductoList().remove(saleView.getSaleHasProductoList().size()-1);
            
            }
            
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void addSaleHasMenu(){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
    
        FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
     
        if(saleView!=null && saleView.getSaleHasMenuList().size()<this._menuController.getAutoMenuList().size()){
        
        Entities.SaleHasMenu saleHasMenu=new Entities.SaleHasMenu();
       
        saleHasMenu.setSaleHasMenuPK(new Entities.SaleHasMenuPK());
              
       Entities.Menu menu=new Entities.Menu();
        
       saleHasMenu.setMenu(new Entities.Menu());
       
       saleHasMenu.setQuantityLimit(0);
       
       saleView.getSaleHasMenuList().add(saleHasMenu);
        
        }
        
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override
    public void deleteSaleHasMenu(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
            
            if(saleView!=null){
            
                if(saleView.getSaleHasMenuList()!=null){
                
                    saleView.getSaleHasMenuList().remove(saleView.getSaleHasMenuList().size()-1);
                
                }
            
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    private final SessionClasses.EdenList<Entities.SaleHasProducto>getSaleHasProductoList(java.util.List<Entities.SaleHasProducto>saleHasProductoList,Entities.Sale sale){
    
        try{
        
            if(saleHasProductoList!=null  && !saleHasProductoList.isEmpty()){
            
                SessionClasses.EdenList<Entities.SaleHasProducto>results=new SessionClasses.EdenList<Entities.SaleHasProducto>();
                
                for(Entities.SaleHasProducto saleHasProducto:saleHasProductoList){
                
                    Entities.SaleHasProducto aux=saleHasProducto;
                    
                    aux.setProducto(this._productController.find(aux.getProducto().getProductoPK()));
                    
                    aux.getSaleHasProductoPK().setProductoCategoriaFranquiciaIdFranquicia(aux.getProducto().getProductoPK().getCategoriaFranquiciaIdfranquicia());
                    
                    aux.getSaleHasProductoPK().setProductoCategoriaIdCategoria(aux.getProducto().getProductoPK().getCategoriaIdcategoria());
                    
                    aux.getSaleHasProductoPK().setProductoIdproducto(aux.getProducto().getProductoPK().getIdproducto());
                    
                    aux.getSaleHasProductoPK().setSaleIdsale(sale.getSalePK().getIdsale());
                    
                    aux.setSale(sale);
                    
                    
                    results.addItem(aux);
                
                }
            
                return results;
                
            }
            
            
            return null;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    private final SessionClasses.EdenList<Entities.SaleHasMenu>getSaleHasMenuList(java.util.List<Entities.SaleHasMenu>saleHasMenuList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(saleHasMenuList!=null && !saleHasMenuList.isEmpty() && session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                SessionClasses.EdenList<Entities.SaleHasMenu>results=new SessionClasses.EdenList<Entities.SaleHasMenu>();
            
                for(Entities.SaleHasMenu saleHasMenu:saleHasMenuList){
                
                saleHasMenu.setMenu(_menuController.find(saleHasMenu.getMenu().getIdmenu()));
                
                saleHasMenu.getSaleHasMenuPK().setMenuIdmenu(saleHasMenu.getMenu().getIdmenu());
                
                saleHasMenu.getSaleHasMenuPK().setSaleFranquiciaIdfranquicia(franchise.getIdfranquicia());
                
                results.addItem(saleHasMenu);
                
                }
                
                return results;
                
            }
        
        return null;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }
    
    }
    
    private final boolean checkProductMenuList(java.util.List<Entities.SaleHasProducto>saleHasProductoList,java.util.List<Entities.SaleHasMenu>saleHasMenuList){
    
        try{
            
            if(saleHasMenuList!=null && !saleHasMenuList.isEmpty())
            {
 
                for(Entities.SaleHasMenu saleHasMenu:saleHasMenuList){
                
                  int count=0;
                  
                  for(Entities.SaleHasMenu aux:saleHasMenuList){
                  
                      if(saleHasMenu.getMenu().getIdmenu()==aux.getMenu().getIdmenu()){
                      
                          count++;
                          
                          if(count==2){
                          
                              return false;
                          
                          }
                      
                      }
                  
                  }
                
                }
                
            }
            
            if(saleHasProductoList!=null && !saleHasProductoList.isEmpty()){
            
                for(Entities.SaleHasProducto saleHasProducto:saleHasProductoList){
                
                    int count=0;
                    
                    for(Entities.SaleHasProducto aux:saleHasProductoList){
                    
                        if(saleHasProducto.getProducto().getProductoPK().equals(aux.getProducto().getProductoPK())){
                        
                        count++;
                        
                        if(count==2){
                        
                            return false;
                        
                        }
                        
                        }
                    
                    }
                
                }
            
            }
            
            return true;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return true;
        
        }
    
    }
    
    
    @Override
    public ENUM.TransactionStatus createSale(String name, double price, java.util.Date startDate,java.util.Date finishDate,java.util.List<Entities.SaleHasProducto>saleHasProductoList,java.util.List<Entities.SaleHasMenu>saleHasMenuList){
    
        try{
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("selectedOption")instanceof Entities.Franquicia){
            
                System.out.print(this.checkDates(startDate, finishDate));
                
                if(this.checkDates(startDate, finishDate) && this.checkProductMenuList(saleHasProductoList, saleHasMenuList)){
                
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
            Entities.Sale sale=new Entities.Sale();
            
            sale.setSalePK(new Entities.SalePK());
            
            sale.getSalePK().setFranquiciaIdfranquicia(franchise.getIdfranquicia());
            
            sale.setName(name);
            
            sale.setValue(price);
            
            sale.setImagenCollection(_fileUpload.getPhotoList());
            
            sale.setStartDate(startDate);
            
            sale.setEndDate(finishDate);
            
            if(this.getSaleHasProductoList(saleHasProductoList, sale)!=null)
                    {
                        
            sale.setSaleHasProductoCollection(IteratorUtils.toList(this.getSaleHasProductoList(saleHasProductoList, sale).iterator()));
                    
                    }
            else{
            
                sale.setSaleHasProductoCollection(null);
            
            }
            
            if(this.getSaleHasMenuList(saleHasMenuList)!=null){
                
            sale.setSaleHasMenuCollection(IteratorUtils.toList(this.getSaleHasMenuList(saleHasMenuList).iterator()));
          
            }
            
            else{
            
                sale.setSaleHasMenuCollection(null);
            
            }
            
            _saleController.create(sale);
            
            _fileUpload.setActionType(null);
            
            return ENUM.TransactionStatus.APPROVED;
            
                
                    
                    
            
                }
                
                else{
                
             
                
                return ENUM.TransactionStatus.DISAPPROVED;
                
                }
            
            }
            
         return ENUM.TransactionStatus.DISAPPROVED;
         
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
             return ENUM.TransactionStatus.DISAPPROVED;
            
        }
    
    }
    
    @Override
    public void deleteSale(Entities.SalePK salePK){
    
        try{
        
            System.out.print("SALE TO DELETE "+salePK);
            
            _saleController.delete(salePK);
            
            _fileUpload.setActionType(null);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void handleFileUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("username")!=null){
            
            _fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString(), 500, 500);
            
            System.out.print("FILE UPLOAD SALE BEAN SIZE "+_fileUpload.getImages().size());
            
            if(_fileUpload.getImages().size()>0){
            
                for(String aux:_fileUpload.getImages()){
                
                    System.out.print("SALES VIEW BEAN IMAGE "+aux);
                
                }
            
            }
            
            }
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void addUpdateSaleHasMenu(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(saleView!=null && session.getAttribute("auxiliarComponent")instanceof Entities.Sale){
            
                Entities.Sale sale=(Entities.Sale)session.getAttribute("auxiliarComponent");
                
                if(saleView.getSaleHasMenuList().size()<_menuController.getAutoMenuList().size())
                {
                
                       Entities.SaleHasMenu saleHasMenu=new Entities.SaleHasMenu();
             
             Entities.SaleHasMenuPK saleHasMenuPK=new Entities.SaleHasMenuPK();
             
             saleHasMenuPK.setSaleIdsale(sale.getSalePK().getIdsale());
             
             saleHasMenuPK.setSaleFranquiciaIdfranquicia(sale.getSalePK().getIdsale());
             
             saleHasMenu.setSaleHasMenuPK(saleHasMenuPK);
             
             saleHasMenu.setSale(sale);
             
             saleHasMenu.setMenu(new Entities.Menu());
             
         
             
             saleHasMenu.setQuantityLimit(0);
             
             saleView.getSaleHasMenuList().add(saleHasMenu);
             
             System.out.print("SALE HAS MENU ITEM IDDED");
                
                }
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void deleteUpdateSaleHasMenu(Entities.SaleHasMenu saleHasMenu){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
        
            if(saleView!=null && saleView.getSaleHasMenuList()!=null && !saleView.getSaleHasMenuList().isEmpty()){
            
                for(Entities.SaleHasMenu aux:saleView.getSaleHasMenuList()){
                
                    if(aux.equals(saleHasMenu)){
                    
                        saleView.getSaleHasMenuList().remove(aux);
                        
                        break;
                    
                    }
                
                }
            
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    
    
    @Override
    public void addUpdateSaleHasProducto(){
    
        try{
        
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
            
            if(saleView!=null && session.getAttribute("auxiliarComponent")instanceof Entities.Sale){
            
          Entities.Sale sale=(Entities.Sale)session.getAttribute("auxiliarComponent");
                
         Entities.SaleHasProducto product=new Entities.SaleHasProducto();
         
         Entities.SaleHasProductoPK key=new Entities.SaleHasProductoPK();
         
         key.setSaleIdsale(sale.getSalePK().getIdsale());
         
         key.setSaleFranquiciaIdfranquicia(sale.getSalePK().getFranquiciaIdfranquicia());
         
         product.setSaleHasProductoPK(key);
         
         product.setProducto(new Entities.Producto());
         
         product.getProducto().setProductoPK(new Entities.ProductoPK());
         
         product.setQuantityLimit(0);
         
         product.setSale(sale);
         
         saleView.getSaleHasProductoList().add(product);
                
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void deleteUpdateSaleHasProducto(Entities.SaleHasProducto saleHasProducto){
    
    try{
    
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
    
        FranchiseAdministrator.Sale.View.SaleView saleView=(FranchiseAdministrator.Sale.View.SaleView)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "saleView");
        
        if(saleView!=null){
        
            if(saleView.getSaleHasProductoList()!=null && !saleView.getSaleHasProductoList().isEmpty())
            {
            
                for(Entities.SaleHasProducto aux:saleView.getSaleHasProductoList()){
                
                    if(aux.equals(saleHasProducto)){
                    
                        saleView.getSaleHasProductoList().remove(aux);
                        
                        break;
                    
                    }
                
                }
                
            }
            
        }
        
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    @Override

    public ENUM.TransactionStatus updateSale(String name, double price,java.util.Date startDate,java.util.Date finishDate,java.util.List<Entities.SaleHasProducto>saleHasProductoList,java.util.List<Entities.SaleHasMenu>saleHasMenuList){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            System.out.print("CHECK DATES "+this.checkDates(startDate, finishDate));
            
            System.out.print("CHECK MENU AND PRODUCT LIST "+this.checkProductMenuList(saleHasProductoList, saleHasMenuList));
            
            if(session.getAttribute("auxiliarComponent")instanceof Entities.Sale && this.checkDates(startDate, finishDate) && this.checkProductMenuList(saleHasProductoList, saleHasMenuList))
            {
            
                Entities.Sale sale=(Entities.Sale)session.getAttribute("auxiliarComponent");
                
                sale.setName(name);
                
                sale.setValue(price);
                
                sale.setStartDate(startDate);
                
                sale.setEndDate(finishDate);
                
                sale.setImagenCollection(_fileUpload.getPhotoList());
                
                 if(this.getSaleHasProductoList(saleHasProductoList, sale)!=null)
                    {
            sale.setSaleHasProductoCollection(IteratorUtils.toList(this.getSaleHasProductoList(saleHasProductoList, sale).iterator()));
                    }
                 else{
                 
                     sale.setSaleHasProductoCollection(null);
                     
                 }
                 
            
            if(this.getSaleHasMenuList(saleHasMenuList)!=null){
            sale.setSaleHasMenuCollection(IteratorUtils.toList(this.getSaleHasMenuList(saleHasMenuList).iterator()));
            }
            else{
            
                sale.setSaleHasMenuCollection(null);
            
            }
                
//            System.out.print("SALE HAS PRODUCTO LIST "+sale.getSaleHasProductoCollection().size());
            
  //          System.out.print("SALE HAS MENU LIST "+sale.getSaleHasMenuCollection().size());
            
            _saleController.update(sale);
                
            System.out.print("SALE APPROVED");
            
            return ENUM.TransactionStatus.APPROVED;
            
            }
            else{
            
                System.out.print("UPDATE DISAPPROVED");
                
                return ENUM.TransactionStatus.DISAPPROVED;
            }
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return ENUM.TransactionStatus.DISAPPROVED;
            
        }
    
    }
    
    @Override
    public boolean postValidateUpdateSale(String name){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
            if(session.getAttribute("auxiliarComponent") instanceof Entities.Sale && session.getAttribute("selectedOption")instanceof Entities.Franquicia)
            {
            
                Entities.Sale sale=(Entities.Sale)session.getAttribute("auxiliarComponent");
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                if(franchise.getSaleCollection()!=null && !franchise.getSaleCollection().isEmpty()){
                
                    for(Entities.Sale aux:franchise.getSaleCollection()){
                    
                        if(aux.getName().toLowerCase().equals(name.toLowerCase()) && !name.toLowerCase().equals(sale.getName().toLowerCase()))
                        {
                        
                            return true;
                        
                        }
                    }
                
                }
                
            }
            
            
        return false;
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return false;
            
        }
    
    }
    
}
