/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.View;

import CDIBeans.FileUploadInterface;
import CDIBeans.ImageControllerDelegate;
import CDIBeans.MenuControllerInterface;
import CDIBeans.ProductoControllerInterface;
import CDIBeans.SaleControllerInterface;
import Clases.BaseBacking;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.faces.FacesException;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;

/**
 *
 * @author luisnegrete
 */
@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})
public class SaleView extends BaseBacking{

    @Inject private ProductoControllerInterface productoController;
    
   @Inject private MenuControllerInterface menuController;
   
   @Inject @javax.enterprise.inject.Default private ImageControllerDelegate imageController;
   
   @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.EdenRedirection _edenRedirection;
   
   @Inject private SaleControllerInterface saleController;
   
 //  @Inject private SaleHasProductoControllerInterface saleHasProductoController;
   
  // @Inject private SaleHasMenuControllerInterface saleHasMenuController;
 
   @javax.inject.Inject @javax.enterprise.inject.Default
   private FranchiseAdministrator.Sale.Controller.SaleViewController _saleViewController;
   
   private String name;
   
   public String getName(){
   
       return this.name;
   
   }
   
   public void setName(String name){
   
       this.name=name;
   
   }
   
   private double price;
   
   public double getPrice(){
   
       return this.price;
   
   }
   
   public void setPrice(double price){
   
       this.price=price;
   
   }
   
   private java.util.Date startDate;
   
   public java.util.Date getStartDate(){
   
       return this.startDate;
   
   }
   
   public void setStartDate(java.util.Date startDate){
   
       this.startDate=startDate;
   
   }
   
   private java.util.Date finishDate;
   
   public java.util.Date getFinishDate(){
   
       return this.finishDate;
   
   }
   
   public void setFinishDate(java.util.Date finishDate){
   
       this.finishDate=finishDate;
   
   }
    
    /**
     * Creates a new instance of SaleView
     */
    public SaleView() {
    }
    
    
    public java.util.List<Entities.Producto>getProducts(){
    
    try{
        
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
            
            System.out.print("selected option is Franquicia");
        
          Entities.Franquicia franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
          
          java.util.List<Entities.Producto>products=productoController.findByFranchise(franchise.getIdfranquicia());
         
          java.util.List<Entities.SaleHasProducto>saleHasProductoList=this.saleHasProductoList;
          
          for(Entities.SaleHasProducto aux:this.saleHasProductoList){
          
          System.out.print("Sale ID "+aux.getProducto().getProductoPK());
          
          }
       
          
          return productoController.findByFranchise(franchise.getIdfranquicia());
            
        }
        else{
        
            throw new IllegalArgumentException();
        
        }
    
    }
    
    catch(IllegalArgumentException | NullPointerException ex){
    
    ex.printStackTrace(System.out);   
      
    return null;
    
    }
    
  
    
    }
    
    public void postValidateUpdateSale(javax.faces.event.ComponentSystemEvent event){
    
        try{
        
            if(event.getComponent().findComponent(":form:tabView:name") instanceof javax.faces.component.UIInput)
            {
            
                javax.faces.component.UIInput input=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:name");
                
                if(input.getLocalValue().toString()!=null){
                
                    String saleName=input.getLocalValue().toString();
                    
                    if(_saleViewController.postValidateUpdateSale(saleName)){
                    
                        java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
                    
                        javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.postValidate"));
                        
                        message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                        
                        this.getContext().addMessage(null,message);
                        
                        this.getContext().renderResponse();;
                        
                    }
                    
                }
                
            }
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Inject FileUploadInterface fileUpload;
    
   private  Entities.Sale sale;
   
   private java.util.List<Entities.SaleHasMenu>saleHasMenuList;
   
   private java.util.List<Entities.SaleHasProducto>saleHasProductoList;
   
   private java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>sales;
   
   public java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>getSales(){
   
       return this.sales;
   
   }
   
   public void setSales(java.util.List<FranchiseAdministrator.Sale.Classes.SaleViewLayout>sales){
   
   this.sales=sales;
   
   }
   
   @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.SaleViewController saleViewController;
   
   public Entities.Sale getSale(){
   
       return this.sale;
   
   }
   
   public void setSate(Entities.Sale sale){
   
       this.sale=sale;
   
   
  }
  
   
   
    @PostConstruct
    public void init(){
      /*
      if(!this.fileUpload.getClassType().equals(SaleView.class.getName()))  
      {
      
     
      
      this.fileUpload.setClassType(SaleView.class.getName());
      
      
      }
        
      if(this.sale==null){
      
      this.sale=new Entities.Sale();
        
      this.sale.setStartDate(null);
        
      this.sale.setEndDate(null);
      
      sale.setValue(0.0);
        
      this.saleHasMenuList=new java.util.ArrayList<Entities.SaleHasMenu>();
      
      this.saleHasProductoList=new java.util.ArrayList<Entities.SaleHasProducto>();
      
      this.fileUpload.setClassType(this.getClass().getName());
      
      }
        */
    }
    
    
    public java.util.List<Entities.SaleHasMenu>getSaleHasMenuList(){
    
    return this.saleHasMenuList;
    
    }
    
    public java.util.List<Entities.SaleHasProducto>getSaleHasProductoList(){
    
    return this.saleHasProductoList;
    
    }
    
    public void setSaleHasMenuList(java.util.List<Entities.SaleHasMenu>saleHasMenuList){
    
        this.saleHasMenuList=saleHasMenuList;
    
    }
    
    public void setSaleHasProductoList(java.util.List<Entities.SaleHasProducto>saleHasProductoList){
    
    this.saleHasProductoList=saleHasProductoList;
    
    }
    
    public void handleFileUpload(org.primefaces.event.FileUploadEvent event){
      try
        {
        _saleViewController.handleFileUpload(event);
        
        System.out.print("FILE UPLOAD IMAGES SIZE "+this.fileUpload.getImages().size());
        
        if(this.fileUpload.getImages().size()>0){
        
            for(String aux:fileUpload.getImages()){
            
                System.out.print(aux);
            
            }
        
        }
  
    }
      catch(NullPointerException ex){
      
          ex.printStackTrace(System.out);
      
      }
                
    }
    
    public void printImages(javax.faces.event.AjaxBehaviorEvent event){
        
        try{
    
        for(String aux:this.fileUpload.getImages()){
        
            System.out.print("ImageName "+aux);
        
        }
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    
    }
    
    public void actionType(){
    
        try{
        
        _saleViewController.initPreRenderView();
        
    /*
        try
        {
          System.out.print(action);
        
        if(action.equals("addSale")){
        
             if(!fileUpload.getActionType().getActionType().equals(action)){
        
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                 
         this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+this.getSession().getAttribute("username").toString());
        
         this.fileUpload.setActionType(new ActionType(action));
        }
                  
    
    }
        
         if(action.equals("listing")){
             
         if(!fileUpload.getActionType().getActionType().equals(action)){
        
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                      
        this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+this.getSession().getAttribute("username").toString());
        
         this.fileUpload.setActionType(new ActionType(action));
       
             
             }
                  
                  this.sales=this.saleViewController.sales();
        
        }
         
         if(action.equals("update")){
         
         if(!this.fileUpload.getActionType().getActionType().equals(action)){
         
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
         
         this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
         
         this.fileUpload.setActionType(new Clases.ActionType(action));
         
         this.saleViewController.updateSaleImages();
         
         }
         
         System.out.print("Session Auxiliar Component "+this.getSessionAuxiliarComponent().getClass().getName());
         
         if(this.getSessionAuxiliarComponent() instanceof Entities.Sale){
         
             this.sale=(Entities.Sale)this.getSessionAuxiliarComponent();
             
            
             
         }
         else{
         
             throw new IllegalArgumentException("Object of type "+this.getSessionAuxiliarComponent().getClass().getName()+" must be of type "+Entities.Sale.class.getName());
         
         }
         
         }
    }
    catch(RuntimeException ex){

        Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
        
        */
        
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    public void addSaleHasProducto(AjaxBehaviorEvent event){
        try
        {
        _saleViewController.addSaleHasProducto();
        
    /*
        try{
            
            if(this.saleHasProductoList.size()<this.getProducts().size()){
            
           System.out.print("You are adding SaleHasProducto "+event.getComponent().getClientId());
        
           Entities.SaleHasProducto saleHasProducto=new Entities.SaleHasProducto();
           
           Entities.SaleHasProductoPK saleHasProductoPK=new Entities.SaleHasProductoPK();
           
           Entities.Producto product=new Entities.Producto();
           
           product.setProductoPK(this.getProducts().get(this.saleHasProductoList.size()).getProductoPK());
           
           saleHasProducto.setProducto(product);
           
           saleHasProducto.setSaleHasProductoPK(saleHasProductoPK);
           
           saleHasProducto.setQuantityLimit(0);
             
           this.saleHasProductoList.add(saleHasProducto);
           
           System.out.print("SaleHasProducto List "+this.sale.getSaleHasProductoCollection().size());
              
            }
            
        }
        catch(Exception ex){
        
           Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex); 
        
        }*/
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
    }
    
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
        
           // this.fileUpload.servletContainer(event.getFile(), this.getSession().getAttribute("username").toString(), 500, 500);
        
            System.out.print("File Uploaded");
            
        }
        catch(Exception ex){
        
        Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    public void removeSaleHasProducto(AjaxBehaviorEvent event){
        try
        {
        _saleViewController.deleteSaleHasProducto();
        
        /*
    try{
    
        if(this.saleHasProductoList.size()>0){
        
        System.out.print("removing item");
            
       
        this.saleHasProductoList.remove(saleHasProductoList.size()-1);
        
        System.out.print("Item removed");
                
        }
    
    }
    catch(Exception ex){
    Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
    }
    */
    }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    }
    
 public java.util.List<Entities.Menu>getMenuList(){
 
     try{
         
         if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
     
        Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
             
         return this.menuController.findByFranchise(franchise.getIdfranquicia());
         }
         else{
         
             throw new IllegalArgumentException("Object of Type "+getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Sucursal.class.getName());
         
         }
     
     }
     catch(NullPointerException ex){
     
         Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
     
         return null;
     }
 
 }   
   
 public void addMenuList(AjaxBehaviorEvent event){
     try
     {
     _saleViewController.addSaleHasMenu();
 /*
     try{
       
         if(this.getMenuList().size()>this.saleHasMenuList.size()){
       
       Entities.SaleHasMenu saleHasMenu=new Entities.SaleHasMenu();
       
       Entities.Menu menu=new Entities.Menu();
       
       menu.setIdmenu(this.getMenuList().get(this.saleHasMenuList.size()).getIdmenu());
       
       
       
       saleHasMenu.setMenu(menu);
       
       saleHasMenu.setQuantityLimit(0);
        
         this.saleHasMenuList.add(saleHasMenu);
         
         }
     
     }
     catch(Exception ex){
     
         Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
           
     }
 */
     
 }
     
     catch(NullPointerException ex){
     
         ex.printStackTrace(System.out);
     
     }
     
 }
 
 public void removeSaleHasMenu(javax.faces.event.AjaxBehaviorEvent event){
     
     try{
 
     _saleViewController.deleteSaleHasMenu();
     
     }
     catch(NullPointerException ex){
     
         ex.printStackTrace(System.out);
     
     }
 
 }
 
 
 public void createSale(ActionEvent event){
   try
     {
     
    switch(_saleViewController.createSale(this.name, this.price, this.startDate, this.finishDate, this.saleHasProductoList, this.saleHasMenuList)){
    
        case APPROVED:{
        
            java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
        
            javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.SaleCreated"));
            
            message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            this.getContext().addMessage(null, message);
            
            
        }
        
        case DISAPPROVED:{
        
            java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
        
            javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.CheckInformation"));
            
            message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            this.getContext().addMessage(null, message);
            
        }
    
    }
   
 }
   
   catch(NullPointerException ex){
   
       ex.printStackTrace(System.out);
   
   }
    
 }
 
 
 public void postValidateSale(ComponentSystemEvent event){
 try
     {
     if(event.getComponent().findComponent(":form:tabView:name")instanceof javax.faces.component.UIInput && event.getComponent().findComponent(":form:tabView:startDate")instanceof javax.faces.component.UIInput && event.getComponent().findComponent(":form:tabView:finishDate") instanceof javax.faces.component.UIInput){
     
     javax.faces.component.UIInput input=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:name");
     
     
     //Name
     if(input.getLocalValue()!=null){
     
     String name=input.getLocalValue().toString();
     
     if(_saleViewController.postValidateAddSale(name)){
     
         //PostValidate Message
     
         java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
         
         javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.postValidate"));
         
         message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
         
         this.getContext().addMessage(null,message);
         
         this.getContext().renderResponse();
         
     }
     
     }
     /*
     javax.faces.component.UIInput start=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:startDate");

     javax.faces.component.UIInput finish=(javax.faces.component.UIInput)event.getComponent().findComponent(":form:tabView:finishDate");
     
     if(start.getLocalValue().toString()!=null && finish.getLocalValue().toString()!=null){
     
         SimpleDateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm");
      
         System.out.print("START DATE "+start.getLocalValue().toString());
         
         System.out.print("FINISH DATE "+finish.getLocalValue().toString());
      
         java.util.Date StartDate=dateFormat.parse(start.getLocalValue().toString());
         
         java.util.Date FinishDate=dateFormat.parse(finish.getLocalValue().toString());
       
         if(_saleViewController.checkDates(StartDate, FinishDate)){
         
             //Date Message
         
             final java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
             
             javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorAddSale.postValidateDate"));
             
             message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
             
             this.getContext().addMessage(null, message);
             
             this.getContext().renderResponse();
             
         }
         
     }
     
     }
     */
     }
     
    
 }
 
 catch(NullPointerException ex){
 
     ex.printStackTrace(System.out);
 
 }
 
 }
 
 
 
 
 public void delete(Entities.SalePK saleKey){
     
     try{
     
     _saleViewController.deleteSale(saleKey);
     
     java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
     
     javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseAdministratorListSale.SaleDeleted"));
     
     msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
     
     this.getContext().addMessage(null,msg);
     
     }
     catch(NullPointerException ex){
     
         ex.printStackTrace(System.out);
     
     }
     
 /*
 try{
 
    System.out.print("Delete Key "+saleKey);
    
    this.saleViewController.delete(saleKey);
    
    this.sales=this.saleViewController.sales();
    
    
 
 }
 catch(Exception ex){
 
 throw new javax.faces.FacesException(ex);
 
 }
 */
 }
 
 public String actionListener(Entities.Sale selectedSale){
 
 try{
     
     if(selectedSale instanceof Entities.Sale){
     
        return _edenRedirection.setAuxiliarComponent(selectedSale);
     
     }
     else{
     
         throw new IllegalArgumentException();
         
     }
 
 }catch(NullPointerException | IllegalArgumentException ex){
 
     Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
     
 return null;
 
 }
 
 }
 
 public void removeSaleHasProduct(Entities.SaleHasProducto saleHasProduct){
 
 try{
 
     _saleViewController.deleteUpdateSaleHasProducto(saleHasProduct);
 
 }
 catch(NullPointerException ex){
 
 Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
 
 
 
 }
 
 }
 
 public void updateAddSaleHasProduct(){
     
     try{

     _saleViewController.addUpdateSaleHasProducto();
     
     }
     catch(NullPointerException ex){
     
         ex.printStackTrace(System.out);
     
     }
 
 }
 
 public void deleteUpdateSaleHasMenu(Entities.SaleHasMenu saleHasMenu){
     
     try{
 
     _saleViewController.deleteUpdateSaleHasMenu(saleHasMenu);
     
     }
     catch(NullPointerException ex){
     
         ex.printStackTrace(System.out);
     
     }
    
 }
     
     public void addUpdateSaleHasMenu(javax.faces.event.AjaxBehaviorEvent event){
     
         try{
         
          _saleViewController.addUpdateSaleHasMenu();
         
         }
         catch(NullPointerException | IllegalArgumentException ex){
         
             Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
             
          
         
         }
     
     }
     
 public String update(){
 try
     {
     
     switch(_saleViewController.updateSale(this.name, this.price, this.startDate, this.finishDate, this.saleHasProductoList, this.saleHasMenuList)){
     
         case APPROVED:{
         
             System.out.print("SALE UPDATED");
             
             return "success";
         
         }
         
         case DISAPPROVED:{
         
             return "failure";
         
         }
         
          default:{
         
             return "failure";
         
         }
     
     }
     
 }
 
 catch(NullPointerException ex){
 
     ex.printStackTrace(System.out);
     
     return "failure";
 
 }
     
 }
 
 
 private void foundSaleHasProduct(){
 
    
     
 for(Entities.SaleHasProducto saleHasProduct:this.saleController.find(this.sale.getSalePK()).getSaleHasProductoCollection()){
  boolean found=false;
 
 for(Entities.SaleHasProducto saleHasProductFile:this.sale.getSaleHasProductoCollection())
 {
 
     if(saleHasProduct.getSaleHasProductoPK().equals(saleHasProductFile.getSaleHasProductoPK())){
     
         found=true;
     
     }
 
 }
 if(!false){
 
//    this.saleHasProductoController.delete(saleHasProduct.getSaleHasProductoPK());
 
 }
 }   
     
     
 
 
 }
 
 private void foundSaleHasMenu(){
 
     try{
     
         for(Entities.SaleHasMenu saleHasMenu:this.saleController.find(this.sale.getSalePK()).getSaleHasMenuCollection()){
         
         boolean found=false;
         
         for(Entities.SaleHasMenu saleHasMenuFile:this.sale.getSaleHasMenuCollection()){
         
         if(saleHasMenu.getSaleHasMenuPK().equals(saleHasMenuFile.getSaleHasMenuPK())){
         
         found=true;
         
         }
         
         }
         
         if(!found){
         
//             this.saleHasMenuController.delete(saleHasMenu.getSaleHasMenuPK());
         
         }
         
         }
     
     }
     catch(IllegalArgumentException | NullPointerException ex){
     
         Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
         
         throw new javax.faces.FacesException(ex);
     
     }
 
 
 }
 
 public void deleteImage(javax.faces.event.AjaxBehaviorEvent event){
 
     try{
     
         java.util.Map<String,String>requestMap=this.getContext().getExternalContext().getRequestParameterMap();
         
         for(java.util.Map.Entry<String,String> entry:requestMap.entrySet()){
         
         System.out.print(entry.getKey()+"-"+entry.getValue());
         
         }
         
     String path=requestMap.get("deleteImagePath");
     
     this.fileUpload.removeImage(path);
     
     }
     catch(javax.faces.event.AbortProcessingException | IllegalArgumentException | NullPointerException ex){
     
         Logger.getLogger(SaleView.class.getName()).log(Level.SEVERE,null,ex);
         
   
         
     
     }
 
 }
 
 public void preRenderView(javax.faces.event.ComponentSystemEvent event){
/* 
     try{
     
         String view=this.getContext().getViewRoot().getViewId();
         
         switch(view){
         
             case "/adminFranquicia/business/sale/addsale.xhtml":{
             
                 if(!this.fileUpload.getActionType().getActionType().equals(view)){
                 
                     javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                 
                     this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                     
                     this.fileUpload.setActionType(new Clases.ActionType(view));
                     
                 }
                 
                 if(this.sale.getSaleHasProductoCollection()==null){
                 
                 this.sale.setSaleHasProductoCollection(new java.util.ArrayList<Entities.SaleHasProducto>());
                 
                 }
                 
                 break;
             
             }
             
             case "/adminFranquicia/business/sale/list-sale.xhtml":{
             
                 
                 if(!this.fileUpload.getActionType().getActionType().equals(view)){
                 
                 javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                 
                 this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                 
                 this.fileUpload.setActionType(new Clases.ActionType(view));
                                     
                 }
                 
                 
                 break;
                         
             }
             
             case "/adminFranquicia/business/sale/update-sale.xhtml":{
             
                 System.out.print("You are in update Sale");
                 
                 if(this.getSessionAuxiliarComponent()instanceof Entities.Sale)
                 {
                 
                     if(this.getSessionAuxiliarComponent()instanceof Entities.Sale){
                     
                         System.out.print("Auxiliar Component Sale is not null");
                         
                         this.sale=(Entities.Sale)this.getSessionAuxiliarComponent();
                         
                     
                     }
                 
                 }
                 else{
                 
                     throw new IllegalArgumentException("Object of type "+this.getSessionAuxiliarComponent().getClass().getName()+" must be of type "+Entities.Sale.class.getName());
                 
                 }
             break;
             
             }
         
         }
     
     }
     catch(NullPointerException | IllegalArgumentException | StackOverflowError ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
         throw new javax.faces.FacesException(ex);
     
     }
 */
 }
 
 
 public void addSaleHasProductoBusiness(javax.faces.event.AjaxBehaviorEvent event){
 
 try{
 
     if(this.sale.getSaleHasProductoCollection()==null){
     
     this.sale.setSaleHasProductoCollection(new java.util.ArrayList<Entities.SaleHasProducto>());
     
     }
     
     Entities.SaleHasProducto saleHasProduct=new Entities.SaleHasProducto();
     
     saleHasProduct.setQuantityLimit(0);
     
     saleHasProduct.setSale(this.sale);
     
     saleHasProduct.setProducto(new Entities.Producto());
     
     saleHasProduct.getProducto().setProductoPK(new Entities.ProductoPK());
     
     saleHasProduct.setSaleHasProductoPK(new Entities.SaleHasProductoPK());
     
     this.sale.getSaleHasProductoCollection().add(saleHasProduct);
 
 }
 catch(NullPointerException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
   
 
 }
 
 }
 
 public void changeDate(org.primefaces.event.SelectEvent event){
 
 try{
 
     
     System.out.print(event.getComponent().getClientId());
     
     if(event.getComponent().getClientId().equals("form:tabView:startDate"))
     {
     
    if(event.getObject()instanceof java.util.Date)
    {
    
    System.out.print("Object of type Date");
    
    this.sale.setStartDate((java.util.Date)event.getObject());
    
    } 
     }
     
     else if(event.getComponent().getClientId().equals("form:tabView:finishDate")){
     
    if(event.getObject() instanceof java.util.Date){
    
        this.sale.setEndDate((java.util.Date)event.getObject());
    
    }
     
     }
 
 }
 catch(IllegalArgumentException  | NullPointerException| StackOverflowError ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
  
 
 }
 
 }
 
 public void saveBusinessSale(javax.faces.event.ActionEvent event){
 
 try{
 
     System.out.print("Create Business Sale");
     
     this.saleViewController.createBusinessSale(sale);
     
     javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Sale Created Succesfully");
     
     this.getContext().addMessage(null,msg);
 
 }
 catch(NullPointerException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
    
 
 }
 
 }
 
 public java.util.List<Layouts.EdenLayout<Entities.Sale>>getBusinessSaleList(){
 
 try{
 
     System.out.print("Return sale list size "+this.saleViewController.getBusinessSaleList().size());
     
     return this.saleViewController.getBusinessSaleList();
 
 }
 catch(NullPointerException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
     return null;
 
 }
 
 }
 
 public void deleteBusinessSales(javax.faces.event.AjaxBehaviorEvent event,Entities.SalePK key){
 
 try{
 
 this.saleViewController.delete(key);
 
 }
 catch(NullPointerException ex){
 
 java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
 

 
 }
 
 
 }
 
 
 public void updateBusinessSale(){
 
 try{
 
     if(this.sale!=null){
     
         this.saleViewController.updateBusinessSale(this.sale);
     
         javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Sale Updated Succesfully");
         
         this.getContext().addMessage(null,msg);  
                 
     }
     else{
     
         javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("There is no sale to update");
     
         msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
         
         this.getContext().addMessage(null,msg);
         
     }
 
 }
 catch(NullPointerException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
  
 
 }
 
 }
 
 
 public void removeBusinessSaleHasProduct(javax.faces.event.AjaxBehaviorEvent event,Entities.SaleHasProducto key){
 
 try{
 
     this.sale.getSaleHasProductoCollection().remove(key);
 
 }
 catch(NullPointerException ex){
 
     java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
     
    
     
 
 }
 
 }
 
 public void removeBusinessSalePicture(javax.faces.event.AjaxBehaviorEvent event){
 
     try{
     
         javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
     
         String value=request.getParameter("hiddenPath");
         
         if(value!=null && !value.equals("")){
         
            this.fileUpload.removeImage(value);
         
         }
         
     }
     catch(NullPointerException ex){
     
         java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
         
        
     }
 
 }
 
 
}
