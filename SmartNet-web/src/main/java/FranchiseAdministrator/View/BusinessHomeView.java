/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.View;

import ENUM.Role;

/**
 *
 * @author luisnegrete
 */
public class BusinessHomeView extends Clases.BaseBacking implements java.io.Serializable{

    /**
     * Creates a new instance of BusinessHomeView
     */
    public BusinessHomeView() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private transient Controllers.NewsController _newsController;
    
    @javax.inject.Inject
    @javax.enterprise.inject.Default
    private transient FranchiseAdministrator.Controller.BusinessHomeController _businessHomeController;
    
    @javax.inject.Inject
    @javax.enterprise.inject.Default
    private transient CDIBeans.FileUploadInterface fileUploadBean;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIEden.AutoCompleteController _autoCompleteController;

  
    
    private String style;
    
    public String getStyle(){
    
        return this.style;
    
    }
    
    public void setStyle(String style){
    
        this.style=style;
    
    }
    
    
    
    private java.util.List<FranchiseAdministrator.Classes.HomeContentFlowLayout>_contentFlowOptions;
 
    private String _search;
    
    public String getSearch(){
    
    return _search;
    
    }
    
    public boolean isRestaurant(){
    
        return this._businessHomeController.isRestaurant();
    
    }
    
    public void setSearch(String search){
    
    _search=search;
    
    }
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
    }
    
    public void updateAutoComplete(){
    
        try{
        
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
            
                System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
        
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
        }
    
    }
    
    public java.util.Vector<FranchiseAdministrator.Classes.HomeContentFlowLayout>getContentFlowOptions(){
    
    return this._businessHomeController.getHomeContentFlowLayoutArray();
    
    }
    
   
    public void preRenderView(){
    
        try{
        
            String view=this.getContext().getViewRoot().getViewId();
            
             if(this.fileUploadBean.getActionType()==null || !this.fileUploadBean.getActionType().getActionType().equals(view))
                    {
                       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
                    
                       this.fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
                        
                       this.fileUploadBean.setActionType(new Clases.ActionType(view));
                        
                    }
            
            switch(view){
            
                case "/adminFranquicia/business/home.xhtml":{
                
                   
                    
                    int width;
                    
                    if(this.isRestaurant()){
                    
                        width=100/8;
                    
                                       
                    }
                    else{
                    
                 
                        
                        width=100/6;
                    
                    }
                    
                  
                    System.out.print("WIDTH VALUE "+width);
                    
                    style="width:"+width+"%";
                    
                    _newsController.init(10);
                    
                    
                    
                    break;
                
                }
                
               
            
            }
        
            javax.faces.component.UIComponent component=this.getContext().getViewRoot().findComponent("form:megamenu");
            
            if(component instanceof org.primefaces.component.megamenu.MegaMenu){
            
                System.out.print("MEGAMENU INSTANCE");
                
                org.primefaces.component.megamenu.MegaMenu megaMenu=(org.primefaces.component.megamenu.MegaMenu)this.getContext().getViewRoot().findComponent("form:megamenu");
                
                for(javax.faces.component.UIComponent aux:megaMenu.getChildren()){
                
                    System.out.print(aux.getClientId());
                
                }
            
            }
            
        }
        catch(Exception ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
        
        }
    
    }
   
    public void imageClicked(javax.faces.event.ActionEvent event){
    
        try{
        
            System.out.print("Image Clicked");
        
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    /**
     *
     * @param selection
     * @return
     */
    @annotations.MethodAnnotations(author="Luis Negrete",date="19/10/2015", comments="redirect to selection")
    public String redirect(String selection)
    {
    
        System.out.print("Selection "+selection);
        
        return selection;
    
    }
    
    
    
    
    public java.util.List<String>getAutoComplete(String query){
    
    try{
    
        if(this.getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
        
           Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
            
      if(franchise.getIdSubtype().getIdTipo().getNombre().equals("Supplier")){
      
          
          _autoCompleteController.updateAutoCompleteList(query, Role.Supplier);
          
           
           System.out.print("AUTOCOMPLETE METHOD "+this._autoCompleteController.getAutoComplete(query, Role.Supplier).size()+" OBJECTS "+this._autoCompleteController.getObjectAutoComplete().size());
          
          return _autoCompleteController.getAutoComplete(query, Role.Supplier);
          
      
      }
      
      else{
      
          System.out.print("THIS IS A FRANCHISE");
          
          _autoCompleteController.updateAutoCompleteList(query, Role.Franchise);
          
           System.out.print("AUTOCOMPLETE METHOD "+this._autoCompleteController.getAutoComplete(query, Role.Supplier).size()+" OBJECTS "+this._autoCompleteController.getObjectAutoComplete().size());
        
          
          return _autoCompleteController.getAutoComplete(query, Role.Supplier);
      
      }
      
      
     
      
        }
        else{
        
            throw new IllegalArgumentException("Object of type "+this.getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
        
        }
    
    }
    catch(javax.ejb.EJBException ex){
    
    return null;
    
    }
        
    
    }
    
    public void dosearch(org.primefaces.event.SelectEvent itemSelect){
        
    
        try{
        
            
            String franchiseName=itemSelect.getObject().toString();
            
            System.out.print("Selected Value "+itemSelect.getObject().toString());
            
            javax.faces.context.ExternalContext ex=this.getContext().getExternalContext();
            
            System.out.print("Request Context Path "+ex.getRequestContextPath());
            
            
            
            ex.redirect(ex.getRequestContextPath()+"/"+"adminFranquicia"+"/"+"search"+"/"+"selection.xhtml?faces-redirect=true&idFranchise="+this._businessHomeController.getFranchiseId(franchiseName)+"");
            
              }
        catch(javax.faces.event.AbortProcessingException | java.io.IOException ex){
        
            
            
        }
    
    }
    
    public void globalSearch(javax.faces.event.ActionEvent event){
    
    try{
    
        System.out.print("SEARCH "+_search);
        
        javax.faces.context.ExternalContext ec=this.getContext().getExternalContext();
        
        ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"adminFranquicia"+java.io.File.separator+"search"+java.io.File.separator+"results.xhtml");
    
    }
    catch(javax.faces.event.AbortProcessingException | java.io.IOException ex){
    
    
    }
        
    
    }
    
}
