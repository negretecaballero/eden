/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.View;

import Clases.BaseBacking;

import Entities.Franquicia;
import Entities.Sucursal;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.enterprise.inject.Default;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import jaxrs.service.LoginAdministradorFacadeREST;

import org.primefaces.context.RequestContext;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

/**
 *
 * @author luisnegrete
 */

public class FranquiciasSucursalesTree extends BaseBacking{
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
   /* @EJB 
    private SucursalhasloginAdministradorFacadeREST sucursalhasloginAdministradorFacadeREST;*/

    @Inject @Default private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
   
    @javax.inject.Inject @javax.enterprise.inject.Default
    private FranchiseAdministrator.Controller.FranchiseSucursalesTreeController _franchiseSucursalesTreeController;
    
    private String id;
    
    public void setId(String id){
    
        this.id=id;
    
    }
    
    public String getId(){
    
        return this.id;
    
    }
    
    public void peRenderView(javax.faces.event.ComponentSystemEvent event){
    
        this.id="Luis Negrete";
        
            }
    
    /**
     * Creates a new instance of FranquiciasSucursalesTree
     */
    
    private DefaultTreeNode selectedNode; 

    private byte NavFlow;

    public byte getNavFlow() {
        return NavFlow;
    }

    public void setNavFlow(byte NavFlow) {
        this.NavFlow = NavFlow;
    }
    
    
    public DefaultTreeNode getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(DefaultTreeNode selectedNode) {
        this.selectedNode = selectedNode;
    }
    
    private String selected;

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }
    
    
    
    
    public FranquiciasSucursalesTree() {
    }

    public TreeNode getRoot() {
      return this._franchiseSucursalesTreeController.getTree();
    }

  
    
    @PostConstruct
    public void Initialize(){
        
          System.out.print("Tree instantiated");
        
  
    
    this.completion=this.loginAdministradorController.accountCompletion(getSession().getAttribute("username").toString());
    
    this.imageName=this.loginAdministradorController.getImageRoot(getSession().getAttribute("username").toString());
    
   
    
    System.out.print(this.imageName);
    
    if(this.completion<0.5){
    
        isCompleted=false;
        
        javax.faces.application.FacesMessage facesMessage=new javax.faces.application.FacesMessage(FacesMessage.FACES_MESSAGES,"Please complete your account information");
    
        getContext().addMessage(null,facesMessage);
        
        RequestContext.getCurrentInstance().update("formulario:messages");
    }
    
    else{
    
        isCompleted=true;
    
    }
    
    }
    
   
    private boolean isCompleted;
    
    public boolean getIsCompleted(){
    
    return this.isCompleted;
    
    }
    
    public void setIsCompleted(boolean isCompleted){
    
        this.isCompleted=isCompleted;
    
    }
   
    
    @PreDestroy
    public void cleanUp(){
    
    System.out.print("Tree Cleaned");
    
    }
    
    public void clickedNode(){
        
        if(selectedNode!=null){
           
         FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "You have selected ",selectedNode.getData().toString());
           getContext().addMessage(null, message);
           
           getContext().renderResponse();
        }
    
    
    }
    
      private List<Entities.Sucursal> sucursalesList;
    
     public String displaySelectedSingle() {
         
     
         
         
        if(selectedNode != null) {
            
            if(selectedNode.getParent().getData().toString().equals("Mis Franquicias")){
            this.NavFlow=1;
           
             String result="";
            List <Franquicia> franquiciaCollection=(List<Franquicia>)getSession().getAttribute("franquicias");
            
            for(Franquicia aux:franquiciaCollection){
            if(aux.getNombre().equals(selectedNode.getData()))
            {
                
             getSession().setAttribute("selectedOption", aux);
             
               result="succesful";
               
             if(getSession().getAttribute("selectedOption") instanceof Franquicia){
                 
             System.out.print("It is instance of franquicia");
             
             }
            }
            }
           
            
             return "succesful"; 
            }
            
            else if(selectedNode.getParent().getData().toString().equals("Mis Sucursales"))
            {
                String result="";
            this.NavFlow=2;
            
           
            
            for(Sucursal aux:sucursalesList){
              
                String combined= aux.getFranquicia().getNombre()+" "+aux.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+aux.getDireccion().getPrimerDigito()+"#"+aux.getDireccion().getSegundoDigito()+"-"+aux.getDireccion().getTercerDigito()+","+aux.getDireccion().getCity().getName()+","+aux.getDireccion().getCity().getState().getName();
          
          if(combined.equals(selectedNode.getData())){
            getSession().setAttribute("selectedOption", aux);
          result="succesful";
           }
            }
            
            return result; 
            }
         
       return "unsuccesful";
         
        }
        
        
        else{
        
        return "unsuccesful";
        
        }
     
    }
    
     
   private  double completion;
     
     public double getCompletion(){
     
     return this.completion;
     
     }
     
     public void setCompletion(double completion){
     
         this.completion=completion;
     
     }
    
     
     private String imageName;
     
     public String getImageName(){
     
     return this.imageName;
     
     }
     
     public void setimageName(String imageName){
     
     this.imageName=imageName;
     
     }
     
     public void selectNode(org.primefaces.event.NodeSelectEvent event){
     
         try{
         
             this._franchiseSucursalesTreeController.navigation(event.getTreeNode().getData().toString());
         
         }catch(Exception ex){
         
             Logger.getLogger(FranquiciasSucursalesTree.class.getName()).log(Level.SEVERE,null,ex);
         
         }
     
     }
     
    
}






