/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

/**
 *
 * @author luisnegrete
 */
public interface PartnersController {
    
    java.util.List<Entities.PartnershipRequest>getPartnersList();
    
    Entities.Franquicia getSupplier(String supplierName);
    
}
