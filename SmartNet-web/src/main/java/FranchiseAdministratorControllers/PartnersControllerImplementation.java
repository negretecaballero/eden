/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

import Entitites.Enum.StateEnum;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class PartnersControllerImplementation implements PartnersController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.PartnershipRequestController _partnershipRequestController;
    
    @Override
    public java.util.List<Entities.PartnershipRequest>getPartnersList()
    {
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
           Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
            return _partnershipRequestController.findByBuyerState(franchise.getIdfranquicia(), StateEnum.accepted);
            }
            else{
            
                throw new IllegalArgumentException("Object must be of type "+Entities.Franquicia.class.getName());
            
            }
        }
        catch(javax.ejb.EJBException | IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public Entities.Franquicia getSupplier(String supplierName){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
                System.out.print("SELECTED OPTION IS A FRANCHISE");
                
                System.out.print("Supplier Name "+supplierName);
                
                Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                System.out.print("List Size "+_partnershipRequestController.findByBuyerState(franchise.getIdfranquicia(), StateEnum.accepted).size());
                
            for(Entities.PartnershipRequest partnerShipRequest:_partnershipRequestController.findByBuyerState(franchise.getIdfranquicia(), StateEnum.accepted))
            {
               System.out.print("Object NAme "+partnerShipRequest.getIdSupplier().getNombre());
                
                if(partnerShipRequest.getIdSupplier().getNombre().equals(supplierName)){
                
                    System.out.print("Returning "+partnerShipRequest.getIdSupplier().getNombre());
                    
                    return partnerShipRequest.getIdSupplier();
                
                }
            
            }
            
            }
            return null;
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
}
