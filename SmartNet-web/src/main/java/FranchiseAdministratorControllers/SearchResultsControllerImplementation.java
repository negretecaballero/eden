/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent


public class SearchResultsControllerImplementation implements SearchResultsController{
    
   @javax.inject.Inject
   @CDIEden.AutoCompleteControllerQualifier
   private CDIEden.AutoCompleteController _autoCompleteController;
   
   @javax.inject.Inject
   @CDIBeans.FileUploadBeanQualifier
   private CDIBeans.FileUploadBean _fileUploadBean;
    
   @Override
  public SessionClasses.EdenList<Clases.FranchiseLayout>getSearchResults(){
   
   try{
   
   SessionClasses.EdenList<Clases.FranchiseLayout>results=new SessionClasses.EdenList<Clases.FranchiseLayout>();
   
   System.out.print("OBJECTS TO EVALUATE "+_autoCompleteController.getObjectAutoComplete().size());
   
   if(_autoCompleteController.getObjectAutoComplete()!=null && !_autoCompleteController.getObjectAutoComplete().isEmpty()){
       
       for(Object o:_autoCompleteController.getObjectAutoComplete()){
       
           if(o instanceof Entities.Franquicia){
               
           Entities.Franquicia franchise=(Entities.Franquicia)o;
           
           Clases.FranchiseLayout aux=new Clases.FranchiseLayoutImplementation();
           
           aux.setFranchise((Entities.Franquicia)o);
           
           if(franchise.getImagenIdimagen()!=null){
           
               javax.faces.context.FacesContext fc=javax.faces.context.FacesContext.getCurrentInstance();
               
              javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext) fc.getExternalContext().getContext();
           
              javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)fc.getExternalContext().getSession(true);
              
              _fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+session.getAttribute("username").toString());
              
              _fileUploadBean.loadImagesServletContext(franchise.getImagenIdimagen(), session.getAttribute("username").toString());
              
              aux.setImage(_fileUploadBean.getImages().get(_fileUploadBean.getImages().size()-1));
           }
           else{
           
              aux.setImage(java.io.File.separator+"images"+java.io.File.separator+"noImage.png");
           
           }
           
           results.addItem(aux);
           
           }
       
       }
   
   }
   
   System.out.print("RESULTS SIZE "+results.size());
   
   return results;
   
   }
   catch(Exception ex){
   
       java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
       return null;
   }
   
   }
    
    
    
}
