/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

/**
 *
 * @author luisnegrete
 */
public interface SelectionViewController {
    
    boolean showMenu(Entities.Franquicia franchise);
    
    void addPartnershipRequest(int idBuyer,int idSupplier);
    
    String getLinkMessage(int idSupplier);
    
    boolean showDialog(int idSupplier);
    
    boolean isPartner(int idPartner);
    
}
