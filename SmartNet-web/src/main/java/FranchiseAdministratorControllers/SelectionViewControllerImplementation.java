/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

import Entitites.Enum.StateEnum;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative
public class SelectionViewControllerImplementation implements SelectionViewController {
 
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.PartnershipRequestController _partnershipController;
    
    @Override
    public boolean showMenu(Entities.Franquicia franchise){
    
        try{
        
            if(franchise.getIdSubtype().getIdTipo().getNombre().equals("Comidas")){
            
                return true;
            
            }
            
            return false;
        
        }
        catch(Exception ex){
        
            return false;
        
        }
    
    }
  
    @Override
    public void addPartnershipRequest(int idBuyer,int idSupplier){
    
        try{
        
            System.out.print("RECEIVED ID "+idBuyer+"-"+idSupplier);
            
            Entities.PartnershipRequest partnershipRequest=new Entities.PartnershipRequest();
            
            Entities.PartnershipRequestPK key=new Entities.PartnershipRequestPK();
            
            key.setIdBuyer(idBuyer);
            
            key.setIdSupplier(idSupplier);
            
            partnershipRequest.setPartnershipRequestPK(key);
            
            partnershipRequest.setIdBuyer(_franchiseController.find(idBuyer));
            
            partnershipRequest.setIdSupplier(_franchiseController.find(idSupplier));
            
            java.util.GregorianCalendar cal=new java.util.GregorianCalendar(java.util.TimeZone.getTimeZone("America/Bogota"));
            
   
            
           
            
           
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            
            String dateTime=cal.get(java.util.Calendar.YEAR)+"/"+cal.get(java.util.Calendar.MONTH)+"/"+cal.get(java.util.Calendar.DAY_OF_MONTH);
            
            Date date = dateFormat.parse(dateTime);
            
            
            java.sql.Date dateDB = new java.sql.Date(date.getTime());
            
            partnershipRequest.setDate(dateDB);
        
            partnershipRequest.setState(StateEnum.pending);
            
            if(_partnershipController.find(key)==null){
            
                System.out.print("PARTNERSHIP IS NULL");
                
                _partnershipController.create(partnershipRequest);
            
            }
            
            else{
            
            System.out.print("PARTNERSHIP IS NOT NULL");
                
            
            }
            
            
        }
        catch(javax.ejb.EJBException | ParseException  ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
   
    @Override
    public boolean showDialog(int idSupplier){
    
        try{
        
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
           if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
           
               Entities.Franquicia buyer=(Entities.Franquicia)session.getAttribute("selectedOption");
               
               Entities.Franquicia supplier=_franchiseController.find(idSupplier);
               
               Entities.PartnershipRequestPK key=new Entities.PartnershipRequestPK();
               
               key.setIdBuyer(buyer.getIdfranquicia());
               
               key.setIdSupplier(supplier.getIdfranquicia());
               
               if(_partnershipController.find(key)!=null){
               
                   return false;
               
               }
               
               return true;
           
           }
           else{
           
               throw new IllegalArgumentException("Object must be of type "+session.getAttribute("selectedOption").getClass().getName());
           
           }
           
        }
        catch(javax.ejb.EJBException | IllegalArgumentException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return false;
        
        }
    
    }
    
    @Override
    public String getLinkMessage(int idSupplier){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
         java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
            
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
                Entities.Franquicia buyer=(Entities.Franquicia)session.getAttribute("selectedOption");
                
                Entities.Franquicia supplier=_franchiseController.find(idSupplier);
                
                Entities.PartnershipRequestPK key=new Entities.PartnershipRequestPK();
                
                key.setIdBuyer(buyer.getIdfranquicia());
                
                key.setIdSupplier(supplier.getIdfranquicia());
                
                if(_partnershipController.find(key)!=null){
                
                    Entities.PartnershipRequest pr=_partnershipController.find(key);
                    
                    if(pr.getState().equalsName("pending")){
                    
                       return  bundle.getString("franchiseAdministratorSelection.RequestSent");
                        
                    }
                    else if(pr.getState().equalsName("accepted")){
                    
                        return "";
                    
                    }
                  return "";  
                }
                else{
                
                    return bundle.getString("franchiseAdministratorSelection.AddPartnerRequest");
                
                }
                
            }
            else{
            
                throw new IllegalArgumentException("Object of type "+session.getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
            
            }
        
        }
        catch(javax.ejb.EJBException | IllegalArgumentException ex){
        
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        return null;
        
        }
    
    }
    
    @Override
    public boolean isPartner(int idPartner){
    
        try{
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            
               Entities.Franquicia franchise=(Entities.Franquicia)session.getAttribute("selectedOption");
            
               Entities.PartnershipRequestPK key=new Entities.PartnershipRequestPK();
               
               key.setIdBuyer(franchise.getIdfranquicia());
               
               key.setIdSupplier(idPartner);
               
               
               
               if(this._partnershipController.find(key)!=null){
               
               System.out.print("KEY FOUND "+this._partnershipController.find(key));    
                   
               Entities.PartnershipRequest partnershipRequest=this._partnershipController.find(key);
               
               if(partnershipRequest.getState()==StateEnum.accepted){
               
                   System.out.print("RETURN TRUE");
                   
                   return true;
               
               }
               
               }
               else{
               
                   System.out.print("KEY NOT FOUND");
               
               }
                
            }
        
            return false;
        }
        catch(javax.ejb.EJBException | IllegalArgumentException |  StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        }
    
    }
    
}
