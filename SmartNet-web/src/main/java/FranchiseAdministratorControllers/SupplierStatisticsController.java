/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

/**
 *
 * @author luisnegrete
 */
public interface SupplierStatisticsController {
    
    Entities.Franquicia getFranchise(int idFranchise);
    
    java.util.List<Entities.QualificationVariableHasRate>getQualificationVariableHasRateList(int idFranchise);
    
    SessionClasses.EdenList<Entities.QualificationVariableHasRate> getQualificarionVariableList(int idSupplier);
    
}
