/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministratorControllers;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Alternative

public class SupplierStatisticsControllerImplementation implements SupplierStatisticsController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.QualificationVariableHasRateControllerImplementation _qualificationVariableHasRateController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.QualificationVariableController _qualificationVariableController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.RateControllerInterface _rateController;
    
    @Override
    public Entities.Franquicia getFranchise(int idFranchise){
    
        try{
        
            return _franchiseController.find(idFranchise);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    @Override
    public SessionClasses.EdenList<Entities.QualificationVariableHasRate>getQualificarionVariableList(int idSupplier){
    
        try{
        
          SessionClasses.EdenList<Entities.QualificationVariableHasRate>results=new SessionClasses.EdenList<Entities.QualificationVariableHasRate>();
          
         
          
          for(Entities.QualificationVariable qualificationVariable:_qualificationVariableController.findAll()){
          
                Entities.QualificationVariableHasRate aux=new Entities.QualificationVariableHasRate();
          
          aux.setIdFranquicia(this._franchiseController.find(idSupplier));
      
              
             if(this.getQualificationRate(qualificationVariable, idSupplier)!=null){
             
                 aux.setIdRate(this.getQualificationRate(qualificationVariable, idSupplier));
                 
             }else{
             
                aux.setIdRate(_rateController.findByStars((short)0));
             
             }
          
             aux.setIdQualificationVariable(qualificationVariable);
             
             results.addItem(aux);
          }
        
          return results;
        }
        catch(javax.ejb.EJBException | NullPointerException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    private Entities.Rate getQualificationRate(Entities.QualificationVariable qualificationVariable,int idFranchise){
    
        try{
        
            for(Entities.QualificationVariableHasRate qualificationVariableHasRate:this._qualificationVariableHasRateController.findByFranchise(idFranchise)){
            
                if(qualificationVariableHasRate.getIdQualificationVariable().getName().equals(qualificationVariable.getName())){
                
                    return qualificationVariableHasRate.getIdRate();
                
                }
            
            }
            
            return null;
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
            
        }
    
    }
    
    @Override
    public java.util.List<Entities.QualificationVariableHasRate>getQualificationVariableHasRateList(int idFranchise){
    
        return _qualificationVariableHasRateController.findByFranchise(idFranchise);
    
    }
    
}
