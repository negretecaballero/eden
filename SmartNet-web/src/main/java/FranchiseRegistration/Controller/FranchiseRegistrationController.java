/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseRegistration.Controller;

/**
 *
 * @author luisnegrete
 */
public interface FranchiseRegistrationController {
    
    void initSubtypeArray();
    
    void updateSubtypeArray(int idTye);
    
    void updateCities();
    
    String imageRoot(String folderName);
    
    void handleFileUpload(org.primefaces.event.FileUploadEvent event,String folderName);
    
     ENUM.TransactionStatus registerApplication(Entities.Application application,String folderName);
    
    boolean checkExistence(String nit);
    
    void initPreRenderView(String viewId);
    
}
