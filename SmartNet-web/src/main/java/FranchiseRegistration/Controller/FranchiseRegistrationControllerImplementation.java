/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseRegistration.Controller;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.context.Dependent
public class FranchiseRegistrationControllerImplementation implements FranchiseRegistrationController{
    
    //CDIBeans
    
    @javax.inject.Inject
    @CDIBeans.TipoControllerQualifier
    private CDIBeans.TipoController _tipoController;
    
    @javax.inject.Inject 
    
    private transient Controllers.FranchiseSubtypeController _franchiseSubtypeController;
    
    @javax.inject.Inject 
    @CDIBeans.StateControllerQualifier
    private transient CDIBeans.StateController _stateController;
    
    @javax.inject.Inject 
  
    private transient Controllers.StateSingletonController _stateSingletonController;
    
    @javax.inject.Inject 

    private transient Controllers.CitySessionController _citySessionController;
    
    @javax.inject.Inject 
    @CDIBeans.ApplicationStatusControllerQualifier
    private CDIBeans.ApplicationStatusControllerImplementation _applicationStatusController;
    
    @javax.inject.Inject
    private Controllers.MailSingletonControllerImplementation _mailSingletonController;
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    private transient CDIBeans.FileUploadInterface _fileUpload;
    
    
    @javax.inject.Inject 
    
    private transient Controllers.FranchiseTypeSingletonController _franchiseTypeController;
    
    @javax.inject.Inject 
    @CDIBeans.ApplicationControllerQualifier
    private CDIBeans.ApplicationController _applicationController;
    
    @javax.inject.Inject 
    @CDIBeans.CityControllerQualifier
    private CDIBeans.CityControllerInterface _cityController;
    
    @javax.inject.Inject
    @CDIBeans.SubtypeControllerQualifier
    private CDIBeans.SubtypeController _subtypeController;
    
    
    @Override
    public void initSubtypeArray(){
    
        try{
        
            System.out.print("TYPE ARRAY SIZE "+_franchiseTypeController.getFranchiseTypeArray().size());
            
            if(_franchiseTypeController.getFranchiseTypeArray()!=null && !_franchiseTypeController.getFranchiseTypeArray().isEmpty()){
            
                System.out.print("TYPE TO FIND "+_franchiseTypeController.getFranchiseTypeArray().get(0).getNombre());
                
                _franchiseSubtypeController.updateSubtypeArray(_franchiseTypeController.getFranchiseTypeArray().get(0));
            
            }
            
            if(_stateSingletonController.getStateList()!=null && !_stateSingletonController.getStateList().isEmpty()){
            
                _citySessionController.initList(_stateSingletonController.getStateList().get(0));
            
            }
            
           
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    @Override
    public void initPreRenderView(String viewId){
        
        if(javax.faces.context.FacesContext.getCurrentInstance()!=null && javax.faces.context.FacesContext.getCurrentInstance().getApplication()!=null && javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver()!=null)
        {
        javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
        
        FranchiseRegistration.View.FranchiseRegistration franchiseRegistration=(FranchiseRegistration.View.FranchiseRegistration)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), null, "franchiseRegistration");
        
        if(viewId.equals("/select-registration/franchise-registration.xhtml") && franchiseRegistration!=null){
            
      
        
        franchiseRegistration.setApplication(new Entities.Application());
        
        franchiseRegistration.getApplication().setCity(new Entities.City());
        
        franchiseRegistration.getApplication().getCity().setCityPK(new Entities.CityPK());
        
        franchiseRegistration.getApplication().setApplicationPK(new Entities.ApplicationPK());
        
        franchiseRegistration.getApplication().setIdSubtype(new Entities.Subtype());
        
        franchiseRegistration.getApplication().getIdSubtype().setSubtypePK(new Entities.SubtypePK());
    
       
        
        if(_fileUpload.getActionType()==null || !_fileUpload.getActionType().getActionType().equals(viewId))
        {
        
          javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
        
           this.initSubtypeArray();
          
          _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images");
          
          _fileUpload.setActionType(new Clases.ActionType(viewId));
          
        }
     
        
        System.out.print("FranchiseRegistration Initialized");
        
        }
        
    }
    
    }
    
    @Override
    public void updateSubtypeArray(int idTye){
    
        try{
        
            if(this._tipoController.find(idTye)!=null){
            
                this._franchiseSubtypeController.updateSubtypeArray(_tipoController.find(idTye));
            
            }
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
        
    }
    
    
    @Override
    public void updateCities(){
    
    try{
    
        
        
        if(javax.faces.context.FacesContext.getCurrentInstance()!=null && javax.faces.context.FacesContext.getCurrentInstance().getExternalContext()!=null && javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true)!=null){
        

        javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequest();
        
        for(java.util.Map.Entry<String,String>entry:javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().entrySet()){
        
            System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
        
        if(servletRequest.getParameter("form:state_input")!=null && !servletRequest.getParameter("form:state_input").equals("")){
        
        int idState=java.lang.Integer.valueOf(servletRequest.getParameter("form:state_input"));
        
        if(_stateController.find(idState)!=null){
        
            System.out.print("State to find update cities "+_stateController.find(idState).getName());
            
            _citySessionController.initList(_stateController.find(idState));
        
        }
        
        }
        }
    }
    catch(NumberFormatException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
    @Override
    public String imageRoot(String folderName){
    
        try{
        
            System.out.print("FOLDER NAME "+folderName);
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            java.io.File file=new java.io.File(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
            
            if(file.exists()){
            
                
                
                if(file.listFiles().length==1){
                
                    java.io.File logo=file.listFiles()[0];
                    
                
                    return java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName+java.io.File.separator+file.listFiles()[0].getName();
                
                }
                
                else{
                
                       return java.io.File.separator+"images"+java.io.File.separator+"noImage.png";
                
                }
                
            }
            else{
            
                System.out.print("THIS FOLDER DOES NOT EXIST");
                
                
                
                return java.io.File.separator+"images"+java.io.File.separator+"noImage.png";
            
            }
            
            
     
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return "";
        
        }
    
    }
    
    
 
    @Override
       public void handleFileUpload(org.primefaces.event.FileUploadEvent event,String folderName){
    
        try{
        
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
                 
            _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
            
            _fileUpload.servletContainer(event.getFile(), folderName, 500, 500);
            
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
        
    
    }
 
       
           
        private Entities.Imagen getImage(){
        
            try{
            
                if(_fileUpload.getPhotoList()!=null && !_fileUpload.getPhotoList().isEmpty())
                {
                
                    return _fileUpload.getPhotoList().get(0);
                
                }
                
                return null;
            }
            catch(Exception | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
                return null;
            }
        
        }
       
        @Override
        public boolean checkExistence(String nit){
        
        try{
    
            
            return this._applicationController.findByNit(nit)!=null;
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return false;
        
        }
        
        }
        
    
        @Override
        public ENUM.TransactionStatus registerApplication(Entities.Application application,String folderName){
        
            try
            {
            
                 application.setImagenIdimagen(this.getImage());
                
                 application.setCity(_cityController.find(application.getCity().getCityPK()));  
          
                 application.setIdSubtype(_subtypeController.find(application.getIdSubtype().getSubtypePK()));
                 
                 application.setIdApplicationStatus(_applicationStatusController.findByStatus(Entitites.Enum.ApplicationStatusENUM.PENDING));
                
                _applicationController.create(application);
                
                SessionClasses.MailManagement mailManagement=new SessionClasses.MailManagement();
                
                java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
                
               _mailSingletonController.sendMail(application.getMail(), bundle.getString("franchiseApplication.MailSubject"), bundle.getString("franchiseApplication.MailContent"));
                
                
                
                return ENUM.TransactionStatus.APPROVED;

                
            }
            catch(Exception | StackOverflowError ex){
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return ENUM.TransactionStatus.DISAPPROVED;
            
            }
        
        }
        
}
