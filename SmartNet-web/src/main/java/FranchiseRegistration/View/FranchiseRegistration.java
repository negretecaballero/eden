/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseRegistration.View;

import FranchiseRegistration.Controller.FranchiseRegistrationController;

/**
 *
 * @author luisnegrete
 */
public class FranchiseRegistration extends Clases.BaseBacking{

    /**
     * Creates a new instance of FranchiseRegistration
     */
    public FranchiseRegistration() {
    }
    
    
    //Inject CDIBeans
 
    @javax.inject.Inject
    
    @CDIEden.FileControllerUpdatedQualifier
 
    private transient  CDIEden.FileController fileController;
    
    private Entities.Application application;
   
    
    @javax.inject.Inject 
    
    private   FranchiseRegistrationController  _franchiseRegistrationController;
     
    
    private int idType;
    
    public int getIdType(){
    
        return this.idType;
    
    }
    
    public void setIdType(int idType){
    
        this.idType=idType;
    
    }
    
    
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    try{
        _franchiseRegistrationController.initPreRenderView(this.getContext().getViewRoot().getViewId());
    
         if(this.fileController.getFolderName()==null || this.fileController.getFolderName().equals(""))
            {
                    
                this.fileController.createFolder();
                 
                System.out.print("FranchiseRegistratio Folder Created");
                
            }
    }
    catch(NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    }
     
    @javax.annotation.PostConstruct
    public void init(){
         
    }

       
  
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
        try
        {
        javax.faces.component.UIComponent component=event.getComponent();
        
        javax.faces.component.UIInput input=(javax.faces.component.UIInput)component.findComponent("form:nit");
        
        if(input!=null && input.getLocalValue()!=null)
        {
        
        String value=input.getLocalValue().toString();
        
        System.out.print("NIT TO FIND "+value);
        
        if(this._franchiseRegistrationController.checkExistence(value))
            
        {
        
            java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(),"bundle");
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(bundle.getString("franchiseApplication.PostValidate"));
            
            msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
            
            this.getContext().addMessage(null,msg);
            
            this.getContext().renderResponse();
            
        }
        
        System.out.print("Input Text "+value);
        
    }
    }
        catch(NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
        
    }
    

  
    
    public Entities.Application getApplication(){
    
    return this.application;
    
    }
    
    public void setApplication(Entities.Application application){
    
        this.application=application;
    
    }
    
    public void stateChanges(javax.faces.event.AjaxBehaviorEvent event){
    
    try{
    
    this._franchiseRegistrationController.updateCities();
    
    }catch(NumberFormatException | NullPointerException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    
    
    }
    
    }
     
    
    
    public String saveChanges(){
        
      try{
        
       switch(this._franchiseRegistrationController.registerApplication(this.application, this.fileController.getFolderName())){
       
           case APPROVED:{
           
           System.out.print("RETURNING SUCCESS");
               
           return "success";
           
           }
           
           case DISAPPROVED:{
        
            
            java.util.ResourceBundle rb=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
        
            javax.faces.application.FacesMessage message=new javax.faces.application.FacesMessage(rb.getString("operationError"));
            
            message.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
            
            this.getContext().addMessage(null, message);
            
            return"failure";
           
           }
           
           default:{
           
               return "failure";
           
           }
       
           
           
       }
            
        
      }
      catch(NullPointerException ex){
      
      java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
      
      return "failure";
      
      }
      
    
        
    }
    

    
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
            
        this._franchiseRegistrationController.handleFileUpload(event, this.fileController.getFolderName());
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
         
        
        }
    
    }
  
    
  
    
    public void changeTipo(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
     System.out.print("IDTYPE "+this.idType);
     
     this._franchiseRegistrationController.updateSubtypeArray(this.idType);
        
        }
        catch(NumberFormatException | javax.faces.event.AbortProcessingException | NullPointerException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

            
        }
    
    }
    
    
    public String getImageLogo(){
    
    try{
    
        return this._franchiseRegistrationController.imageRoot(this.fileController.getFolderName());
        
    }
    catch(NullPointerException | StackOverflowError  ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
       return "";
    
    }
    
    }
 
    
}
