/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Collections;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class ComponeLazyModel extends org.primefaces.model.LazyDataModel<Entities.Compone> {
 
    
    private java.util.List<Entities.Compone>datasource;
    
    
    public ComponeLazyModel(java.util.List<Entities.Compone>datasource)
    {
    
    this.datasource=datasource;
    
    }
    
    public java.util.List<Entities.Compone>getDatasource(){
    
    return this.datasource;
    
    }
    
    public void setDatasource(java.util.List<Entities.Compone>datasource){
    
    this.datasource=datasource;
    
    }
    
    
    @Override
     public Entities.Compone getRowData(String rowKey){
     
         System.out.print("RowKey "+rowKey);
         
         if(rowKey.split(",").length==5)
         {
             
         for(Entities.Compone compone:datasource){
         
         if(compone.getComponePK().toString().equals(rowKey)){
         
         return compone;
         
         }
         
         
         }
         
         }
          return null; 
     }
         
       @Override
       public Entities.ComponePK getRowKey(Entities.Compone compone){
       
       System.out.print("InventarioPK LazyModel Value "+compone.getInventario().getInventarioPK());
           
       return compone.getComponePK();
       
       }
     
      @Override
    public java.util.List<Entities.Compone> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
     
        java.util.List<Entities.Compone> data = new java.util.ArrayList<Entities.Compone>();
 
        //filter
        for(Entities.Compone compone : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (java.util.Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(compone.getClass().getField(filterProperty).get(compone));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(compone);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new ComponeSorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
}
