/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import Entities.Compone;
import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class ComponeSorter  implements Comparator<Entities.Compone>{
  
       private String sortField;
     
    private SortOrder sortOrder;
     
    public ComponeSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.Compone compone1, Entities.Compone compone2) {
        try {
            Object value1 = Entities.Producto.class.getField(this.sortField).get(compone1);
            Object value2 = Entities.Producto.class.getField(this.sortField).get(compone2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }

   
    
}
