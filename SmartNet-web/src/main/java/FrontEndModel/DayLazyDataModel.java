/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Collections;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class DayLazyDataModel extends org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>{
    
    java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>datasource;
    
    public DayLazyDataModel(java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>datasource){
    
        this.datasource=datasource;
    
    }
 
    public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getDatasource(){
    
    return this.datasource;
    
    }
    
    public void setDatasource(java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>datasource){
    
        this.datasource=datasource;
    
    }
    
     @Override
     public FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout getRowData(String rowKey){
     
         System.out.print("RowKey ");
         
        
             
         for(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout day:datasource){
         
         if(day.getDay().getIdDay()==Integer.parseInt(rowKey)){
        
        System.out.print("Day Match "+day.getDay().getDayName());
             
        return day;
         
         }
         
         
         }
         
         
          return null; 
     }
     
      @Override
       public Integer getRowKey(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout day){

       return day.getDay().getIdDay();
       
       }
       
          @Override
    public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
     
        java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout> data = new java.util.ArrayList<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>();
 
        //filter
        for(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout day : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (java.util.Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(day.getClass().getField(filterProperty).get(day));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(day);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new DaySorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
    
    
}
