/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class DaySorter implements Comparator<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout> {
 
         private String sortField;
     
    private SortOrder sortOrder;
     
    public DaySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout day1, FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout day2) {
        try {
            Object value1 = FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout.class.getField(this.sortField).get(day1);
            Object value2 = FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout.class.getField(this.sortField).get(day2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
    
}
