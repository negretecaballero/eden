/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class LazyProductDataModel extends org.primefaces.model.LazyDataModel<Entities.Producto> {
    
        private java.util.List<Entities.Producto> datasource;
     
    public LazyProductDataModel(java.util.List<Entities.Producto> datasource) {
        this.datasource = datasource;
    }
    @Override
    public Entities.Producto getRowData(String rowKey) {
        
        try{
        
        String[]keyComponents=rowKey.split(",");
            
        System.out.print(keyComponents.length);
        
        if(keyComponents.length==3){
        
         Entities.ProductoPK productPK= new Entities.ProductoPK();
         
         productPK.setIdproducto(new java.lang.Integer(keyComponents[0]));
         
         productPK.setCategoriaIdcategoria(new java.lang.Integer(keyComponents[1]));
         
         productPK.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(keyComponents[2]));
            
        System.out.print("Get Row Data (String) "+rowKey);
        
        for(Entities.Producto producto : datasource) {
            if(producto.getProductoPK().equals(productPK))
                return producto;
        }
 
        return null;
        }
        else{
        
            throw new RuntimeException();
         
        }
        }
        catch(RuntimeException ex){
        
        Logger.getLogger(LazyProductDataModel.class.getName()).log(Level.SEVERE,null,ex);
       
        throw new javax.faces.FacesException(ex);
        
        }
    }
 
    @Override
    public Entities.ProductoPK getRowKey(Entities.Producto product) {
        
        System.out.print("Get Row Key "+product);
        
        return product.getProductoPK();
        
    }
    
    
 
    @Override
    public java.util.List<Entities.Producto> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
        List<Entities.Producto> data = new ArrayList<Entities.Producto>();
 
      
        
        if(this.datasource!=null && !this.datasource.isEmpty())
        {
        for(Entities.Producto product : this.datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(product.getClass().getField(filterProperty).get(product));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(product);
            }
        }
        
    }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
    
}
