/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class LazySorter implements Comparator<Entities.Producto> {
 
    private String sortField;
     
    private SortOrder sortOrder;
     
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.Producto product1, Entities.Producto product2) {
        try {
            Object value1 = Entities.Producto.class.getField(this.sortField).get(product1);
            Object value2 = Entities.Producto.class.getField(this.sortField).get(product2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}