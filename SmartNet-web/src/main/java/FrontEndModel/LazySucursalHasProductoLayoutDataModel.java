/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.SortOrder;
import java.util.*;


/**
 *
 * @author luisnegrete
 */
public class LazySucursalHasProductoLayoutDataModel extends org.primefaces.model.LazyDataModel<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>{
    
    private java.util.List<SucursalHasProductoFrontEndLayout>attribute;
	private Collection<SucursalHasProductoFrontEndLayout> datasource;
    
    public LazySucursalHasProductoLayoutDataModel(java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>datasource)
        {
        
            this.datasource=datasource;
        
        }
        
        @Override
        public SucursalHasProductoFrontEndLayout getRowData(String rowKey){
        
        try{
        
            if(rowKey.split(",").length==3){
            
            Entities.ProductoPK key=new Entities.ProductoPK();
            
            key.setIdproducto(new java.lang.Integer(rowKey.split(",")[0]));
            
            key.setCategoriaIdcategoria(new java.lang.Integer(rowKey.split(",")[1]));
            
            key.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(rowKey.split(",")[2]));
                
            for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:this.datasource){
            
            if(aux.getProduct().getProductoPK().equals(key)){
            
                return aux;
            
            }
            
            }
            
            return null;
            }
            else{
            
            throw new javax.faces.FacesException("Exception Caugth");
                
            }
            
        }
        catch(Exception ex){
        
            Logger.getLogger(LazySucursalHasProductoLayoutDataModel.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        }
        
        }        
    
        @Override
    public Entities.ProductoPK getRowKey(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout sucursalHasProducto){
    
    return sucursalHasProducto.getProduct().getProductoPK();
    
    }
    
      @Override
    public java.util.List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
        List<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> data = new java.util.ArrayList<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout>();
 
        //filter
        for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout sucursalHasProducto : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(sucursalHasProducto.getClass().getField(filterProperty).get(sucursalHasProducto));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(sucursalHasProducto);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new SucursalHasProductoDataSorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
}
