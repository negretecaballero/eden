/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class LazySucursalInventarioDataModel extends org.primefaces.model.LazyDataModel<Entities.InventarioHasSucursal>{
    
    
    private java.util.List<Entities.InventarioHasSucursal>datasource;
    
     public LazySucursalInventarioDataModel(java.util.List<Entities.InventarioHasSucursal> datasource) {
        this.datasource = datasource;
    }
     
     public java.util.List<Entities.InventarioHasSucursal>getDatasource(){
     
         return this.datasource;
     
     }
     
     public void setDatasource(java.util.List<Entities.InventarioHasSucursal>datasource){
     
         this.datasource=datasource;
     
     }
    
      @Override
    public Entities.InventarioHasSucursal getRowData(String rowKey) {
        
        try{
        
        System.out.print("Row Key Lazy Model "+rowKey);    
            
        String[]keyComponents=rowKey.split(",");
            
        System.out.print(keyComponents.length);
        
        if(keyComponents.length==4){
        
        Entities.InventarioHasSucursalPK inventarioHasSucursalPK=new Entities.InventarioHasSucursalPK();
        
        inventarioHasSucursalPK.setInventarioIdinventario(new java.lang.Integer(keyComponents[0]));
        
        inventarioHasSucursalPK.setInventarioFranquiciaIdfranquicia(new java.lang.Integer(keyComponents[1]));
        
        inventarioHasSucursalPK.setSucursalIdsucursal(new java.lang.Integer(keyComponents[2]));
        
        inventarioHasSucursalPK.setSucursalFranquiciaIdfranquicia(new java.lang.Integer(keyComponents[3]));
        
        for(Entities.InventarioHasSucursal inventarioHasSucursal : datasource) {
            
            if(inventarioHasSucursal.getInventarioHasSucursalPK().equals(inventarioHasSucursalPK))
                
           return inventarioHasSucursal;
        
        }
 
        return null;
        }
        else{
        
            throw new RuntimeException();
         
        }
        }
        catch(RuntimeException ex){
        
        Logger.getLogger(LazyProductDataModel.class.getName()).log(Level.SEVERE,null,ex);
       
        throw new javax.faces.FacesException(ex);
        
        }
    }
 
    @Override
    public Entities.InventarioHasSucursalPK getRowKey(Entities.InventarioHasSucursal inventarioHasSucursal) {
        
       
        
        return inventarioHasSucursal.getInventarioHasSucursalPK();
        
    }
    
    
 
    @Override
    public java.util.List<Entities.InventarioHasSucursal> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
        List<Entities.InventarioHasSucursal> data = new ArrayList<Entities.InventarioHasSucursal>();
 
        //filter
        for(Entities.InventarioHasSucursal inventarioHasSucursal : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(inventarioHasSucursal.getClass().getField(filterProperty).get(inventarioHasSucursal));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(inventarioHasSucursal);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new SucursalInventarioDataSorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            
            return data;
        
        }
    }
}
