/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SucursalHasProductoDataSorter implements Comparator<BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout> {
    
    private String sortField;
     
    private SortOrder sortOrder;
     
    public SucursalHasProductoDataSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout sucursalHasProducto1, BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout sucursalHasProducto2) {
        try {
            Object value1 = BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout.class.getField(this.sortField).get(sucursalHasProducto1);
            Object value2 = BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout.class.getField(this.sortField).get(sucursalHasProducto2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
