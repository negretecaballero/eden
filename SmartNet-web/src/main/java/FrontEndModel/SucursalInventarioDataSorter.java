/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Comparator;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SucursalInventarioDataSorter implements Comparator<Entities.InventarioHasSucursal> {
  
      private String sortField;
     
    private SortOrder sortOrder;
     
    public SucursalInventarioDataSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.InventarioHasSucursal inventarioHasSucursal1, Entities.InventarioHasSucursal inventarioHasSucursal2) {
        try {
            Object value1 = Entities.Producto.class.getField(this.sortField).get(inventarioHasSucursal1);
            Object value2 = Entities.Producto.class.getField(this.sortField).get(inventarioHasSucursal2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
    
}
