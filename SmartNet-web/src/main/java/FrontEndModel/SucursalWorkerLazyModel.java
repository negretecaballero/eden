/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import Entities.LoginAdministrador;
import java.util.Collections;
import java.util.List;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SucursalWorkerLazyModel extends org.primefaces.model.LazyDataModel<Entities.LoginAdministrador>{
    
    private java.util.List<Entities.LoginAdministrador>datasource;
    
    public SucursalWorkerLazyModel(java.util.List<Entities.LoginAdministrador>datasource){
    
        this.datasource=datasource;
    
    }

    public List<LoginAdministrador> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<LoginAdministrador> datasource) {
        this.datasource = datasource;
    }
    
    @Override
    public Entities.LoginAdministrador getRowData(String rowKey){
    
        if(rowKey!=null){
        
        for(Entities.LoginAdministrador aux:this.datasource){
        
            if(aux.getUsername().equals(rowKey)){
            
                return aux;
            
            }
        
        }
        
        
        }
        
    return null;
    }
    
    @Override
    public String getRowKey(Entities.LoginAdministrador loginAdministrador){
    
        return loginAdministrador.getUsername();
    
    }
    
     @Override
    public java.util.List<Entities.LoginAdministrador> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
     
        java.util.List<Entities.LoginAdministrador> data = new java.util.ArrayList<Entities.LoginAdministrador>();
 
        //filter
        for(Entities.LoginAdministrador loginAdministrador : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (java.util.Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(loginAdministrador.getClass().getField(filterProperty).get(loginAdministrador));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(loginAdministrador);
            }
            
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new SucursalWorkerSorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
        
        
    }

    
    
}
