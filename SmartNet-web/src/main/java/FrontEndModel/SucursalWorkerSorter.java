/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SucursalWorkerSorter implements java.util.Comparator<Entities.LoginAdministrador>{

    private String sortField;
     
    private SortOrder sortOrder;
     
    public SucursalWorkerSorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.LoginAdministrador supplierProductCategory1, Entities.LoginAdministrador supplierProductCategory2) {
        try {
            Object value1 = Entities.LoginAdministrador.class.getField(this.sortField).get(supplierProductCategory1);
            Object value2 = Entities.LoginAdministrador.class.getField(this.sortField).get(supplierProductCategory2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
