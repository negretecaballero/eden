/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import java.util.Collections;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SupplierProductCategoryLazyModel extends org.primefaces.model.LazyDataModel<Entities.SupplierProductCategory>{
    
    private java.util.List<Entities.SupplierProductCategory>datasource;
    
    public SupplierProductCategoryLazyModel(java.util.List<Entities.SupplierProductCategory>datasource){
    
        this.datasource=datasource;
    
    }
    
    public java.util.List<Entities.SupplierProductCategory>getDatasource(){
    
        return this.datasource;
    
    }
    
    public void setDatasource(java.util.List<Entities.SupplierProductCategory>datasource){
    
        this.datasource=datasource;
    
    }
    
    @Override
    public Entities.SupplierProductCategory getRowData(String rowKey){
    
        
        System.out.print("RowKey to FIND "+rowKey);
        
        if(rowKey.split(",").length==3){
        
            Entities.SupplierProductCategoryPK key=new Entities.SupplierProductCategoryPK();
            
            key.setIdSupplierProductCategory(new java.lang.Integer(rowKey.split(",")[0]));
            
            key.setSupplierIdsupplier(new java.lang.Integer(rowKey.split(",")[1]));
        
            key.setSupplierLoginAdministradorUsername(rowKey.split((","))[2]);
           
            
            for(Entities.SupplierProductCategory supplierProductCategory:this.datasource)
            {
                System.out.print("KEY VALUE "+key.toString());
            
            if(supplierProductCategory.getSupplierProductCategoryPK().equals(key)){
            
                System.out.print("OBJECT FOUND "+supplierProductCategory);
                
                return supplierProductCategory;
            
            }
            
            }
        }
        
    return null;
    }
    
    @Override
    public Entities.SupplierProductCategoryPK getRowKey(Entities.SupplierProductCategory supplierProductCategory){
    
    return supplierProductCategory.getSupplierProductCategoryPK();
    
    }
    
    
     @Override
    public java.util.List<Entities.SupplierProductCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder, java.util.Map<String,Object> filters) {
     
        java.util.List<Entities.SupplierProductCategory> data = new java.util.ArrayList<Entities.SupplierProductCategory>();
 
        //filter
        for(Entities.SupplierProductCategory supplierProductCategory : datasource) {
            boolean match = true;
 
            if (filters != null) {
                for (java.util.Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(supplierProductCategory.getClass().getField(filterProperty).get(supplierProductCategory));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(supplierProductCategory);
            }
        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new SupplierProductCategorySorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }

    
    
    
}



