/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FrontEndModel;

import Entities.SupplierProductCategory;
import org.primefaces.model.SortOrder;

/**
 *
 * @author luisnegrete
 */
public class SupplierProductCategorySorter implements java.util.Comparator<Entities.SupplierProductCategory>{

       private String sortField;
     
    private SortOrder sortOrder;
     
    public SupplierProductCategorySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }
 
    public int compare(Entities.SupplierProductCategory supplierProductCategory1, Entities.SupplierProductCategory supplierProductCategory2) {
        try {
            Object value1 = Entities.Producto.class.getField(this.sortField).get(supplierProductCategory1);
            Object value2 = Entities.Producto.class.getField(this.sortField).get(supplierProductCategory2);
 
            int value = ((Comparable)value1).compareTo(value2);
             
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}
