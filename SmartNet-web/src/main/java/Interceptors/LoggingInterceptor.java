/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 *
 * @author luisnegrete
 */

@Interceptor
public class LoggingInterceptor {
 
    @AroundInvoke
    public Object log(InvocationContext context)  { 
        
        try
        {
            
    String name = context.getMethod().getName();

    System.out.print("INTERCEPTOR CALLED");
    
     System.out.println("Interceptor invoked for " + context.getMethod().getName());

     Object[] params = context.getParameters();
     
     if(context.getMethod().getName().equals("getSpecialGreeting")){
 
         java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
     
         
         
         
     
    if(params!=null && params.length>0){
     params[0]=bundle.getString("Selection.Saludation.Welcome")+" "+params[0].toString();
    }
    
     }
     
     if(context.getMethod().getName().equals("addToBranchProductList")){
     
         if(params!=null && params.length>0){
         
             if(params[0]instanceof Entities.ProductoPK){
             
                 if(checkExistenceList((Entities.ProductoPK)params[0])){
                 
                     params[0]=null;
                 
                 }
             
             }
         
         }
     
     }
            
return context.proceed();

    }
        
        catch(java.lang.Exception ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    private final boolean checkExistenceList(Entities.ProductoPK key){
    
        try{
            
            javax.el.ELResolver resolver=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getELResolver();
            
            BranchAdministrator.Product.View.Product product=(BranchAdministrator.Product.View.Product)resolver.getValue(javax.faces.context.FacesContext.getCurrentInstance().getELContext(),null,"product");
        
            if(product!=null){
            
                if(product.getSelectedImages()!=null && !product.getSelectedImages().isEmpty())
                {
                
                   for(BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout aux:product.getSelectedImages()){
                   
                       if(aux.getProduct().getProductoPK().equals(key)){
                       
                           return true;
                       
                       }
                   
                   }
                
                }
                
                
            }
            
            return false;
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
        return false;
        
        }
    
    }
    
}
