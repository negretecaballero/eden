/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Language.Controller;

import java.util.Locale;
import java.util.Map;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author luisnegrete
 */
public interface LanguageController {

    void countryLocaleCodeChanged(ValueChangeEvent e);

    void forceLocale();

    Map<String, Object> getCountriesInMap();

    Locale getLocale();

    String getLocaleCode();

    void setLanguageManually(String locale);
    
    void setLocaleCode(String localeCode);

	boolean getLoaded();

	/**
	 * 
	 * @param loaded
	 */
	void setLoaded(boolean loaded);
    
}
