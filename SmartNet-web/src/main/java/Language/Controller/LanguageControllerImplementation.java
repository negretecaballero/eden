/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Language.Controller;

import java.util.Locale;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import java.io.*;
import Controllers.*;

/**
 *
 * @author luisnegrete
 */


@javax.inject.Named("language")
@javax.enterprise.context.SessionScoped
@LanguageQualifier
public class LanguageControllerImplementation implements java.io.Serializable, LanguageController {
    
    private final static long serialVersionUID=1L;
    
    private String localeCode;
    
   
    
    private static java.util.Map<String,Object>countries;
	@javax.inject.Inject()
	@javax.enterprise.inject.Default()
	private CountryCodesSingletonControllerImplementation _countryCodeSingleton;
	private boolean _loaded;
    
    static{
    
        countries=new java.util.LinkedHashMap<String,Object>();
        
        countries.put("English", Locale.ENGLISH);
        
        countries.put("Español", new Locale("es", "CO"));
        
      
        
    }
    
    @Override
    public java.util.Map<String,Object>getCountriesInMap(){
    
        return countries;
    
    }
    
    @Override
    public String getLocaleCode(){
    
        return localeCode;
    
    }
    @Override
    public void setLocaleCode(String localeCode){
    
        this.localeCode=localeCode;
    
    }
    
    @Override
    public void forceLocale(){
    
    try
    {
    
        System.out.print("LOCALE CODE LANGUAGE "+this.localeCode);
        
    }
    catch(NullPointerException ex){
    
        ex.printStackTrace(System.out);
    
    }
    
    }
    
    @Override
    public void countryLocaleCodeChanged(ValueChangeEvent e){
		
		String newLocaleValue = e.getNewValue().toString();
		
             
                
		//loop country map to compare the locale code
                for (Map.Entry<String, Object> entry : countries.entrySet()) {
        
        	   if(entry.getValue().toString().equals(newLocaleValue)){
        		
                       System.out.print("SELECTED VALUE "+entry.getValue().toString());
                       
                       System.out.print(Locale.SIMPLIFIED_CHINESE.toString());
                       
        		FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale)entry.getValue());
        		
        	  }
               }
	}
    
    
    @Override
    public Locale getLocale(){
    
        try{
        
            if(this.localeCode!=null && !this.localeCode.equals("")){
            
                System.out.print("PROBABLY VALID LOCALE");
                
                if(this.localeCode.split("_").length==2){
                
                    return new Locale(this.localeCode.split("_")[0],this.localeCode.split("_")[1]);
                
                }
                
                
            
            
            }
            
            return null;
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
    @Override
    public void setLanguageManually(String locale){
    
        try{
        
               for (Map.Entry<String, Object> entry : this.getCountriesInMap().entrySet()) {
        
        	   if(entry.getValue().toString().equals(locale)){

        	      FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale)entry.getValue());
        		
                       this.setLocaleCode(locale);
                       
                       _loaded=true;
                       
        
        	  }
               }
        
        
        }
        catch(NullPointerException ex){
        
            ex.printStackTrace(System.out);
        
        }
        
    
    }

	@Override
	public boolean getLoaded() {
		
            return _loaded;
                    
	}

	/**
	 * 
	 * @param loaded
	 */
	@Override
	public void setLoaded(boolean loaded) {
		
            _loaded=loaded;
            
	}
    
}
