/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Language.Controller;

/**
 *
 * @author luisnegrete
 */
public class LanguageProducer {
    
    
    @javax.enterprise.inject.Produces
    public Language.Controller.LanguageController languageControllerFactory(){
    
        return new Language.Controller.LanguageControllerImplementation();
    
    }
    
}
