/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Language.Listeners;

import java.util.Locale;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author luisnegrete
 */
public class ActionListeners implements javax.faces.event.ValueChangeListener{

    @javax.inject.Inject @javax.enterprise.inject.Default private Language.Controller.LanguageControllerImplementation _languageController;
    
    @Override
    public void processValueChange(ValueChangeEvent vce) throws AbortProcessingException {
    
    try{
        
          String newLocaleValue = vce.getNewValue().toString();
          
           Clases.EdenCookie cookie=new Clases.EdenCookieImplementation();
		
               cookie.addCookie("language", vce.getNewValue().toString(), 1000);
                
		//loop country map to compare the locale code
                for (Map.Entry<String, Object> entry : _languageController.getCountriesInMap().entrySet()) {
        
        	   if(entry.getValue().toString().equals(newLocaleValue)){

                       
        		FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale)entry.getValue());
        		
                       
                        
                      
                        
        	  }
               }
    
        System.out.print("NEW VALUE "+vce.getNewValue().toString());
    
    }
    catch(NullPointerException ex){
    
    
    }
    
    }

  
    
}
