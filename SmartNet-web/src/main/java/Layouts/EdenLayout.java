/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Layouts;

/**
 *
 * @author luisnegrete
 */
public interface EdenLayout<T> {
    
    T getObject();
    
    void setObject(T object);
    
    String getImage();
    
    void setImage(String image);
    
}
