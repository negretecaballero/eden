/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Layouts;

/**
 *
 * @author luisnegrete
 */
public class EdenLayoutImplementation<T> implements EdenLayout<T> {
    
    private T _object;
    
    private String _image;

    @Override
    public T getObject() {
       
    return _object;
    
    }

    @Override
    public void setObject(T object) {
   
        _object=object;
    
    }

    @Override
    public String getImage() {
  
        return _image;
    
    }

    @Override
    public void setImage(String image) {
  
        _image=image;
        
    }
    
    
    
}
