/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Layouts;

/**
 *
 * @author luisnegrete
 */
public class ProductLayoutImplementation implements ProductLayout {
    
    private Entities.Producto _product;
    
    private String _image;
    
    
    @Override
    public Entities.Producto getProduct(){
    
    return _product;
    
    }
    
    
    @Override
    public void setProduct(Entities.Producto product){
        
     _product=product;   
        
    }
    
    
    @Override
    public String getImage(){
    
        return _image;
    
    }
    
    
    @Override
    public void setImage(String image){
    
    _image=image;
    
    }
    
    
}
