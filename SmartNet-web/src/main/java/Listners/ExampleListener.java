/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author luisnegrete
 */
public class ExampleListener implements ServletContextListener{

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    
        System.out.print("EXAMPLE CONTEXT INITIALIZED");
        
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
  
        System.out.print("LISTENER CONTEXT DESTROYED");
        
    }
}
