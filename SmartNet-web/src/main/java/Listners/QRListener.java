/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listners;

import SessionBeans.QrCodeLocal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import managedBeans.Qrcode;

/**
 *
 * @author luisnegrete
 */
public class QRListener implements ActionListener{
    
    
    private QrCodeLocal qrCode ;

    
    
    @Override
    public void processAction(ActionEvent event) throws AbortProcessingException {
    System.out.print("entro QR Listener");
    ELContext context=FacesContext.getCurrentInstance().getELContext();
     Qrcode qrcode = (Qrcode)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,null,"qrcode");
       System.out.print(qrcode.getCode());
       
       qrCode.CodeToImage(qrcode.getCode(), qrcode.getCode());
    }

    private QrCodeLocal lookupQrCodeLocal() {
        try {
            Context c = new InitialContext();
            return (QrCodeLocal) c.lookup("java:global/guanabara/QrCode!SessionBeans.QrCodeLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
}
