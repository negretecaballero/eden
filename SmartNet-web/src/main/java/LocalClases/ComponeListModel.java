/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LocalClases;

import FranchiseAdministrator.Product.Classes.ComponeUpdate;
import Entities.ComponePK;
import FranchiseAdministrator.Product.Classes.ComponeUpdateInterface;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import FranchiseAdministrator.Product.View.AgregarProducto;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;



/**
 *
 * @author luisnegrete
 */
public class ComponeListModel extends LazyDataModel<ComponeUpdateInterface>{
    
    
    private List<ComponeUpdateInterface>dataSource;
    
    private AgregarProducto agregarProducto;
    

    public ComponeListModel(List<ComponeUpdateInterface> dataSource) {
        
        this.dataSource = dataSource;
        
        ELContext context=FacesContext.getCurrentInstance().getELContext();
        
        agregarProducto=(AgregarProducto)FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,null,"agregarProducto");
         
        
    }

    
    public List<ComponeUpdateInterface>getDataSource(){
    
        return this.dataSource;
    
    }
    
    public void setgetDataSource(List<ComponeUpdateInterface> dataSource ){
    
        this.dataSource=dataSource;
    
    }
   
    
  
    
    @Override
    public Object getRowKey(ComponeUpdateInterface t) {

    return t;
    
    }

    @Override
    public ComponeUpdateInterface getRowData(String string) {
        
        try{
        
        ComponePK componePK=new ComponePK();
        
        System.out.print("Results getRowData "+string);
        
        
        if(string!=null)
        {
        String []results=string.split(",");
        
        
        System.out.print("Agregar Producto value "+agregarProducto.getNombre());
        
        System.out.print("String value "+string);
        
        System.out.print("Results Size "+results.length);
        
        if(results.length==4){
        
            //componePK.setInventarioSucursalFranquiciaIdfranquicia(Integer.parseInt(results[0]));
            
            //componePK.setInventarioSucursalIdsucursal(Integer.parseInt(results[1]));
            
            componePK.setProductoIdproducto(Integer.parseInt(results[2]));
            
            componePK.setInventarioIdinventario(Integer.parseInt(results[3]));
              
        }
        
        else
        {
        
          return null;
        
        }
        
        
        System.out.print(string);

                
       
        for(ComponeUpdateInterface componeUpdate : dataSource) {
            
            if(componeUpdate.getCompone().getComponePK().equals(componePK))
               
            { 
               
                
                
                updateDeleteList(componeUpdate);
                  return componeUpdate;
                
            }       
        
        }
        
        return null;
        
        }
        
        else{
        
        return null;
        
        }
        }
        catch(RuntimeException ex){
        
            Logger.getLogger(ComponeListModel.class.getName()).log(Level.SEVERE,null,ex);
            
            
            return null;
        }
        
        
        
    
    }
    
    
    private void updateDeleteList(ComponeUpdateInterface componeUpdate){
    
    boolean add=true;
        
   for(ComponeUpdateInterface result:this.agregarProducto.getDeleteComponeList()){
   
   if(result.equals(componeUpdate)){
   
       add=false;
   
   }
   
   }
   
   if(add){
   
   this.agregarProducto.getDeleteComponeList().add(componeUpdate);
   
   System.out.print("Component added to the list");
   }
   
   else{
   
       this.agregarProducto.getDeleteComponeList().remove(componeUpdate);
       System.out.print("COmponent removed from the list");
       
   }
        
    }
    
    
    @Override
    public List<ComponeUpdateInterface> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<ComponeUpdateInterface> data = new ArrayList<ComponeUpdateInterface>();
 
        //filter
        for(ComponeUpdateInterface car : dataSource) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(car);
            }
        }
 
        //sort
        if(sortField != null) {
//            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
    
   
    
}
