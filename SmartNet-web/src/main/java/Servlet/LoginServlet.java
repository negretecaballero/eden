/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import Language.Controller.LanguageQualifier;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





/**
 *
 * @author luisnegrete
 */
@WebServlet(name="LoginServlet", urlPatterns={"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    @javax.inject.Inject @javax.enterprise.inject.Default
    private  Controllers.CountryCodesSingletonController _countryCodeSingletonController;
    
    @javax.inject.Inject @LanguageQualifier
    private  Language.Controller.LanguageControllerImplementation _languageController;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @javax.annotation.Resource(lookup="java:comp/BeanManager")
        BeanManager bm  = null;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        
        try (PrintWriter out = response.getWriter()) {
        
            javax.naming.InitialContext initialContext = new javax.naming.InitialContext();
            
            
            
            //bm = (BeanManager) initialContext.lookup("java:comp/BeanManager");
            
            /*Bean<CDIBeans.CategoryControllerInterface> bean = (Bean<CDIBeans.CategoryControllerInterface>)bm.getBeans(CDIBeans.CategoryControllerInterface.class).iterator().next();
            
            CreationalContext<CDIBeans.CategoryControllerInterface> ctx = bm.createCreationalContext(bean);
            
            CDIBeans.CategoryControllerInterface categoryController = (CDIBeans.CategoryControllerInterface) bm.getReference(bean, CDIBeans.CategoryControllerInterface.class, ctx);
            
            java.util.List<Entities.Categoria> category = categoryController.findAll();
            
            if(category!=null && !category.isEmpty()){
            
                System.out.print("THERE ARE "+category.size()+" categories");
            
            }*/
            
            System.out.print("LoginServletCalled");
            
            javax.faces.context.FacesContext context=javax.faces.context.FacesContext.getCurrentInstance();
            
            if(context!=null){
            
            System.out.print("Context is not null");
            
            }
            
            String locale="en";
            
            for(java.util.Map.Entry<String,String[]>map:request.getParameterMap().entrySet()){
            
                System.out.print(map.getKey());
                
                if(map.getKey().equals("test")){
                    
           
                    
                    JsonReader jsonReader=Json.createReader(new StringReader(map.getValue()[0]));
                    
                    JsonArray json=jsonReader.readArray();
                    
              
                    
                    System.out.print("JSON SIZE "+json.size());
                    
              
                    
                    String ip=json.getJsonObject(0).get("value").toString();
                    
                    String path=json.getJsonObject(1).get("value").toString();
                    
                     
                      
         
            
             System.out.print("IP CODE "+Clases.GPSEdenOperations.locateIP(ip,path));
             
            locale=_countryCodeSingletonController.getCodeMap().get(Clases.GPSEdenOperations.locateIP(ip,path));
            
            System.out.print("LOCALE VALUE "+locale);
            
            }
            
            }
        
            String string="0";
            
         
            
                _languageController.setLocaleCode(locale);
                
                   if(_languageController.getLocale()!=null){
            
                string="1";
            
            }
                
                System.out.print("LANGUAGE CONTROLLER LOCALE "+locale);
            
            System.out.print("LOCALE "+locale);
            
     
     response.setContentType("text/html");
response.setCharacterEncoding("utf-8");

//JsonGeneratorFactory factory = Json.createGeneratorFactory(null);

//JsonGenerator gen=factory.createGenerator(System.out);


JsonObject value = Json.createObjectBuilder().add("id", "Locale").add("Value", string).build();



   //gen.writeStartObject().write("id", "Locale").write("Value", locale).writeEnd().close();
    
    
    System.out.print("JSON STRING "+value.toString());
    
    // finally output the json string       
    out.print(value.toString());
        
            /* TODO output your page here. You may use following s
        ample code. */
       
        } 
        
        catch(Exception ex){
        
            ex.printStackTrace(System.out);
            
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
