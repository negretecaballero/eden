/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.enterprise.inject.spi.BeanManager;
import javax.json.Json;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luisnegrete
 */
public class SeverityServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @javax.annotation.Resource(lookup="java:comp/BeanManager")
    BeanManager bm;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
           
        System.out.print("YOU HAVE CALLED SeverityServlet");

        if(request.getSession().getAttribute("severity")!=null){
        
            System.out.print("SEVERITY "+request.getSession().getAttribute("severity").toString());
        
        }
        
        if(request.getSession().getAttribute("title")!=null){
        
            System.out.print("TITLE "+request.getSession().getAttribute("title").toString());
        
        }
        
        if(request.getSession().getAttribute("message")!=null){
        
            System.out.print("MESSAGE "+request.getSession().getAttribute("message").toString());
        
        }
        
        
        if(request.getSession().getAttribute("severity")!=null && request.getSession().getAttribute("title")!=null && request.getSession().getAttribute("message")!=null){
          
         System.out.print("SEVERITY "+request.getSession().getAttribute("severity").toString()+ " Message "+request.getSession().getAttribute("message"));
          
         response.setContentType("text/html");
          
         response.setCharacterEncoding("utf-8");
          
         JsonObject value = Json.createObjectBuilder().add("severity", request.getSession().getAttribute("severity").toString()).add("title", request.getSession().getAttribute("title").toString()).add("message", request.getSession().getAttribute("message").toString()).build();
  
         out.print(value.toString());
           
        }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
