/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Socket;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnOpen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 *
 * @author luisnegrete
 */

@org.primefaces.push.annotation.PushEndpoint("/chat/{idSucursal}/{idFranquicia}/{username}")
public class OrderChatResource {
 
     private final Logger logger = LoggerFactory.getLogger(OrderChatResource.class);
    
    @OnOpen
    public void onOpen(RemoteEndpoint r, EventBus eventBus) {
        logger.info("OnOpen {}", r);
 
        System.out.print("Order Chat Resource Push End Point started");
        
        }
    
    /**
	 * 
	 * @param message
	 */
	@org.primefaces.push.annotation.OnMessage(decoders={DecoderEncoder.MessageDecoder.class}, encoders={DecoderEncoder.MessageEncoder.class})
    public Clases.Message onMessage(Clases.Message message){
    
        return message;
    
    }
    
}
