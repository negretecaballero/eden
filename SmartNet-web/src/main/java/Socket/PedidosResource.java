/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Socket;


import javax.faces.application.FacesMessage;
import javax.ws.rs.PathParam;

import org.primefaces.push.EventBus;
import org.primefaces.push.RemoteEndpoint;
import org.primefaces.push.annotation.OnClose;
import org.primefaces.push.annotation.OnMessage;
import org.primefaces.push.annotation.OnOpen;
import org.primefaces.push.annotation.PushEndpoint;
import org.primefaces.push.impl.JSONEncoder;


/**
 *
 * @author luisnegrete
 */

@PushEndpoint("/{idFranquicia}/{idSucursal}")

public class PedidosResource {
    
    
    @PathParam("idFranquicia")
    private Integer idFranquicia;
    
    
    @PathParam("idSucursal")
    private Integer idSucursal;
    
    @javax.ws.rs.PathParam("username")
    private String username;
    
    @OnOpen
    public void onOpen(RemoteEndpoint r, EventBus eventBus) {
        
        try{
        
       
            
        System.out.print("URI "+r.uri());
        
        System.out.print("Path "+r.path());
        
        eventBus.publish(r.path(), "Welcome to Eden");
               
        System.out.print("Body "+r.body());
        
        System.out.print("Address "+r.address());
        
       
       
        
        
        }
        catch(java.lang.IllegalStateException | NullPointerException ex){
        
            System.out.print("Exception in pedido resource");
        
        }
        
    }
 
    @OnClose
    public void onClose(RemoteEndpoint r, EventBus eventBus) {
        
        try{
        
        
        }
        catch(NullPointerException  ex){
        
        
        }
    }
    
    /**
	 * 
	 * @param message
	 */
	@OnMessage(encoders={JSONEncoder.class})
    public FacesMessage onMessage(FacesMessage message){
    try{
        
        System.out.print("Web Socket Called Pedido Resource");

        System.out.print("Id franquicia "+this.idFranquicia);
          
        return message;
        
    }
    catch(NullPointerException  ex){
    
        return null;
        
    }
    }
}
