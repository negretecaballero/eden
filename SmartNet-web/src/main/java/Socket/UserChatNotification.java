/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Socket;

import org.primefaces.push.impl.JSONEncoder;

/**
 *
 * @author luisnegrete
 */

@org.primefaces.push.annotation.PushEndpoint("/{username}")

public class UserChatNotification {
    
    
    @javax.ws.rs.PathParam("{username}")
    public String username;
    
    
    /**
	 * 
	 * @param message
	 */
	@org.primefaces.push.annotation.OnMessage(encoders={JSONEncoder.class})
    
    public String onMessage(String message){
    
        return message;
    
    }
    
}
