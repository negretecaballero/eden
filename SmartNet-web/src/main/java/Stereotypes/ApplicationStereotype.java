/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Stereotypes;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;

/**
 *
 * @author luisnegrete
 */

@javax.enterprise.inject.Stereotype
@Target({FIELD, TYPE, METHOD})
@Retention(RUNTIME)
@javax.enterprise.context.ApplicationScoped
public @interface ApplicationStereotype {
    
}
