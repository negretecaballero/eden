/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupplierRegistration.Controller;

/**
 *
 * @author luisnegrete
 */
public interface SupplierRegistrationController {
    
 String generateFolderName();
 
 void uploadFile(org.primefaces.event.FileUploadEvent event,String folderName);
 
 void removeFolder(String folder);
 
 String updateLogoImage();
 
 void initCityList();
 
 void stateChanged(String idState);
 
 boolean checkExistence(String nit);
 
 void saveApplication(Entities.SupplierApplication application);
    
}
