/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupplierRegistration.Controller;

import javax.transaction.UserTransaction;

/**
 *
 * @author luisnegrete
 */


@javax.enterprise.context.Dependent

@SupplierRegistrationControllerQualifier

public class SupplierRegistrationControllerImplementation implements SupplierRegistrationController,java.io.Serializable{
  
   @javax.inject.Inject
   @CDIBeans.FileUploadBeanQualifier
   private transient CDIBeans.FileUploadInterface _fileUploadController;
   
   @javax.inject.Inject
   
   private transient Controllers.CitySessionController _citySessionController;
   
   @javax.inject.Inject 

   private transient Controllers.StateSingletonController _stateSingletonController;
   
   @javax.inject.Inject 
   @CDIBeans.StateControllerQualifier
   private transient CDIBeans.StateControllerInterface _stateController;
   
   @javax.inject.Inject
   @CDIBeans.SupplierApplicationControllerQualifier
   private transient CDIBeans.SupplierApplicationController _supplierApplicationController;
   
   @javax.inject.Inject
   @CDIBeans.CityControllerQualifier
   private transient CDIBeans.CityControllerInterface _cityController;
   
   @javax.inject.Inject
   @CDIBeans.SupplierTypeControllerQualifier
   private  CDIBeans.SupplierTypeController _supplierTypeController;
   
   @javax.inject.Inject
   @CDIBeans.SupplierProductTypeControllerQualifier
   private transient CDIBeans.SupplierProductTypeController _supplierProductTypeController;
   
   @javax.inject.Inject
   @CDIBeans.LoginAdministradorControllerQualifier
   private transient CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
   
   @javax.inject.Inject
   @CDIBeans.AppGroupControllerQualifier
   private transient CDIBeans.AppGroupControllerDelegate _appGroupController;
   
   UserTransaction ut;
   
   @Override
   public void uploadFile(org.primefaces.event.FileUploadEvent event,String folderName){
   
       try{
       
           javax.faces.context.FacesContext fc=javax.faces.context.FacesContext.getCurrentInstance();
           
           javax.servlet.ServletContext sc=(javax.servlet.ServletContext)fc.getExternalContext().getContext();
           
           
           _fileUploadController.Remove(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+folderName);
           
           System.out.print("Folder Cleaned");
           
           _fileUploadController.servletContainer(event.getFile(), folderName, 500, 500);
           
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
   }
  
   @Override
   public void removeFolder(String folder){
   
       try{
       
           Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
           
       }
       catch(Exception | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       }
       finally{
       
           System.out.print("Folder Removed");
       
       }
   
   }
   
   @Override
   public String updateLogoImage(){
   
       try{
       
         if(_fileUploadController.getImages()!=null && !_fileUploadController.getImages().isEmpty()){
         
             return _fileUploadController.getImages().get(_fileUploadController.getImages().size()-1);
         
         }
         else{
         
             return "/images/noImage.png";
         
         }
           
       }
       catch(NullPointerException | IllegalArgumentException | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
   
       return "";
   }
   
   @Override
    public String generateFolderName(){
    
        try{
        
            Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
            
            javax.faces.context.FacesContext fc=javax.faces.context.FacesContext.getCurrentInstance();
            
            javax.servlet.ServletContext sc=(javax.servlet.ServletContext)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getContext();
            
            String folder=filesManagement.generateFolderName(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images");
        
            filesManagement.createFolder(sc.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator, folder);
            
            return folder;  
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
          
            return "";
        }
    
        
     
        
    }
    
    
    @Override
       public void initCityList(){
        
            try{
            
               if(_stateSingletonController.getStateList()!=null && !_stateSingletonController.getStateList().isEmpty()){
               
                   _citySessionController.initList(_stateSingletonController.getStateList().get(0));
               
               }
            
            }
            catch(Exception | StackOverflowError ex){
            
                java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            }
        
        }
    
       
       @Override
       public void stateChanged(String idState){
       
       try{
       
           if(_stateController.find(Integer.parseInt(idState))!=null){
           
               this._citySessionController.initList(_stateController.find(Integer.parseInt(idState)));
           
           }
       
       }
       catch(javax.ejb.EJBException | NumberFormatException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
       
       }
       
       }
    
       @Override
       public boolean checkExistence(String nit){
       
       try{
       
           return this._supplierApplicationController.find(nit)!=null;
       
       }
       catch(javax.ejb.EJBException | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       return false;
       
       }
       
       }
       
       @annotations.MethodAnnotations(author="Luis Negrete",date="03/12/2015")
       @Override
       public void saveApplication(Entities.SupplierApplication application){
       
           try{
           
               application.setCity(this._cityController.find(application.getCity().getCityPK()));
           
               application.setIdSupplierType(_supplierTypeController.find(application.getIdSupplierType().getIdSupplierType()));
               
               application.setIdSupplierProductType(_supplierProductTypeController.find(application.getIdSupplierProductType().getIdSupplierProductType()));
               
               application.setApplicationState(Entitites.Enum.SupplierApplicationState.pending);
               
               if(this._fileUploadController.getPhotoList()!=null && !this._fileUploadController.getPhotoList().isEmpty()){
               
                   application.setLogo(this._fileUploadController.getPhotoList().get(this._fileUploadController.getPhotoList().size()-1));
                   
               }
               
               System.out.print("Nit "+application.getNit()+" Mail "+application.getMail()+" Name "+application.getName()+" CIUU "+application.getCodigoCiuu()+ "Cedula "+application.getCedula()+" Expedicion "+application.getFechaExpedicion()+" City "+application.getCity());
               
               _supplierApplicationController.create(application);
               
           
               
           }
           catch(javax.ejb.EJBException | StackOverflowError ex){
           
               java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
               
           } 
       
       }
       
       
}
