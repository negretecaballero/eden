/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SupplierRegistration.View;

import CDIBeans.FileControllerQualifier;
import javax.inject.Named;
import javax.faces.flow.FlowScoped;
import Clases.*;
import SupplierRegistration.Controller.SupplierRegistrationControllerQualifier;

/**
 *
 * @author luisnegrete
 */
@Named("supplierRegistrationFlow")
@FlowScoped("supplier-registration")

public class SupplierRegistrationFlow extends BaseBacking implements java.io.Serializable{

      
    
    @CDIEden.FileControllerUpdatedQualifier
    
    @javax.inject.Inject
    
    private  CDIEden.FileController _fileController;
    
    @javax.inject.Inject
    
    @SupplierRegistrationControllerQualifier
    
    private  
    SupplierRegistration.Controller.SupplierRegistrationController _supplierRegistrationController;
    
  
    
    private String folderName;
    
    private String _logo;
    
    private Entities.SupplierApplication _application;
    
    public Entities.SupplierApplication getApplication(){
    
    return _application;
    
    }
    
    public String getLogo(){
    
    return _logo;
    
    }
    
    public void setLogo(String logo){
    
    _logo=logo;
    
    }
    
    public String getFolderName(){
    
        return this.folderName;
    
    }
    
    public void setFolderName(String folderName){
    
        this.folderName=folderName;
    
    }
    
    public void setApplication(Entities.SupplierApplication application){
    
        _application=application;
    
    }
    
    public void postValidate(javax.faces.event.ComponentSystemEvent event){
    
    try{
    
        System.out.print("POST VALIDATE EVENT");
    
        
        javax.faces.component.UIComponent component=event.getComponent().findComponent("form:nit");
        
        if(component!=null){
        
            System.out.print("COMPONENT IS NOT NULL");
            
            if(component instanceof javax.faces.component.UIInput){
            
            System.out.print("INPUT TYPE");
            
            javax.faces.component.UIInput nit=(javax.faces.component.UIInput)component;
            
            //System.out.print(nit.getLocalValue().toString());
            
            String nitString=(nit.getLocalValue().toString());
            
           
            
            if(this._supplierRegistrationController.checkExistence(nitString)){
            
                  java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
                
                javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("supplierRegistration.NitExistError");
                
                msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                
                this.getContext().addMessage(null, msg);
                
                this.getContext().renderResponse();
            
            
            }
            
       
            
            }
        
        }
        
    }
    catch(NullPointerException | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
    }
    
    }
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
            
            if(this.folderName==null || !this.folderName.equals("")){
            
                _fileController.createFolder();
                
                this.folderName=_fileController.getFolderName();
            
            }
            
            this.folderName=_supplierRegistrationController.generateFolderName();
        
            _application=new Entities.SupplierApplication();
            
            _application.setCity(new Entities.City());
            
            _application.getCity().setCityPK(new Entities.CityPK());
            
          
            _application.setIdSupplierProductType(new Entities.SupplierProductType());
            
            _application.setIdSupplierType(new Entities.SupplierType());
             
            _logo=this._supplierRegistrationController.updateLogoImage();
            
            System.out.print("LOGO "+_logo);
            
            this._supplierRegistrationController.initCityList();
        
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void uploadLogo(org.primefaces.event.FileUploadEvent event){
    
        try{
        
            System.out.print("UPLOADING LOGO");
            
            this._supplierRegistrationController.uploadFile(event, this.folderName);
            
            this._logo=this._supplierRegistrationController.updateLogoImage();
            
            System.out.print("LOGO IMAGE "+this._logo);
            
        }
        catch(Exception | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
  
    
    public SupplierRegistrationFlow() {
        
        
        
    }
    
    @javax.annotation.PreDestroy
    private void preDestroy(){
    
    try{
    
        this._supplierRegistrationController.removeFolder(folderName);
    
    }
    catch(Exception | StackOverflowError ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void stateChanged(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet())
            {
            
            System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
            if(this.getRequest().getParameter("form:state_input")!=null)
            
            {
            
                String idState=this.getRequest().getParameter("form:state_input");
                
                this._supplierRegistrationController.stateChanged(idState);
            
            }            
            
        }
        catch(NullPointerException | IllegalArgumentException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    
    }
    
    
    public String saveApplication(){
    
    try{
    
        
        this._supplierRegistrationController.saveApplication(_application);
    
        return "success";
        
    }
    catch(Exception ex){
    
    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    return "failure";
    
    }
    
    
    }
    
}
