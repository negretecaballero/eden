/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author luisnegrete
 */

@Documented
@java.lang.annotation.Target({ElementType.METHOD, ElementType.FIELD})
@java.lang.annotation.Inherited
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)

public @interface MethodAnnotations {
    
    String author() default "Luis Negrete";
    
    String date();
    
    int revision() default 1;
    
    String comments() default "none";
    
}
