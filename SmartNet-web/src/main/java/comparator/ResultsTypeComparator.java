package comparator;

import EdenFinalUser.Search.Results.Classes.ResultsLayout;

public class ResultsTypeComparator implements java.util.Comparator<EdenFinalUser.Search.Results.Classes.ResultsLayout> {

    @Override
    public int compare(ResultsLayout o1, ResultsLayout o2) {
       
        if(o1.getSimilarity()==o2.getSimilarity()){
        
            return 0;
        
        }
        
        if(o1.getSimilarity()<o2.getSimilarity()){
        
            return 1;
        
        }
        
        return -1;
    }
}