/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package franchsieAdministratorFrontEnd;

import org.primefaces.model.diagram.DefaultDiagramModel;

/**
 *
 * @author luisnegrete
 */
public class PartnerView extends Clases.BaseBacking{
    
    private DefaultDiagramModel model;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private FranchiseAdministratorControllers.PartnersController _partnersController;
    
    public org.primefaces.model.diagram.DefaultDiagramModel getModel(){
    
    return model;
    
    }
    
    public void setModel(org.primefaces.model.diagram.DefaultDiagramModel model){
    
        this.model=model;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
       System.out.print("INITIALIZING PARTNER");
        
       System.out.print("PARTNERS "+_partnersController.getPartnersList().size());
       
       model=new org.primefaces.model.diagram.DefaultDiagramModel();
       
       model.setMaxConnections(-1);
       
       java.util.ResourceBundle bundle=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getResourceBundle(javax.faces.context.FacesContext.getCurrentInstance(), "bundle");
       
       org.primefaces.model.diagram.Element elementA=new org.primefaces.model.diagram.Element(bundle.getString("PartnerView.Suppliers"),"20em","6em");
       
       SessionClasses.EdenList<org.primefaces.model.diagram.Element>elements=new SessionClasses.EdenList<org.primefaces.model.diagram.Element>();
       
       for(Entities.PartnershipRequest aux:this._partnersController.getPartnersList()){
       
           org.primefaces.model.diagram.Element element=new org.primefaces.model.diagram.Element(aux.getIdSupplier().getNombre(),"10em","18em");
       
           element.addEndPoint(new org.primefaces.model.diagram.endpoint.DotEndPoint(org.primefaces.model.diagram.endpoint.EndPointAnchor.TOP));
           
           model.addElement(element);
           
           elements.addItem(element);
           
       }
       
    
       elementA.addEndPoint(new org.primefaces.model.diagram.endpoint.DotEndPoint(org.primefaces.model.diagram.endpoint.EndPointAnchor.BOTTOM));
    
       model.addElement(elementA);
       
       for(org.primefaces.model.diagram.Element element:elements){
       
           model.connect(new org.primefaces.model.diagram.Connection(elementA.getEndPoints().get(0),element.getEndPoints().get(0)));
       
       }
       
    }
    
    public void diagramSelection(javax.faces.event.ActionEvent event){
    
        try{
            
            System.out.print("YOU ARE IN DIAGRAMSELECTION METHOD");
        
            java.util.Map<String,String>requestMap=this.getRequestMap();
            
            for(java.util.Map.Entry<String,String>entry:requestMap.entrySet()){
            
                System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
            
            String name=requestMap.get("name");
            
            System.out.print("NAME "+name);
            
            System.out.print(this._partnersController.getSupplier(name));
            
            if(_partnersController.getSupplier(name)!=null){
            
                javax.faces.context.ExternalContext ec=this.getContext().getExternalContext();
                
                ec.redirect(ec.getRequestContextPath()+java.io.File.separator+"adminFranquicia"+java.io.File.separator+"search"+java.io.File.separator+"selection.xhtml?faces-redirect=true&idFranchise="+this._partnersController.getSupplier(name).getIdfranquicia()+"");
            
            }
        
        }
        catch(javax.faces.event.AbortProcessingException | java.io.IOException | StackOverflowError ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
