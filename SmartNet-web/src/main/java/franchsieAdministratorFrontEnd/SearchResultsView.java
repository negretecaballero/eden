/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package franchsieAdministratorFrontEnd;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */
public class SearchResultsView {

    /**
     * Creates a new instance of SearchResultsView
     */
    public SearchResultsView() {
    }
    
    private int idFranchise;
    
    public int getIdFranchise(){
    
        return this.idFranchise;
    
    }
    
    public void setIdFranchise(int idFranchise){
    
    this.idFranchise=idFranchise;
    
    }
    
   
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIEden.AutoCompleteController _autoCompleteController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private FranchiseAdministratorControllers.SearchResultsController _searchResultsController;
    
    public java.util.List<Clases.FranchiseLayout>getResults(){
    
    return IteratorUtils.toList(_searchResultsController.getSearchResults().iterator());
    
    }

    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
        
          
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            
        
        }
    
    }
    
    public String select(Entities.Franquicia franchise){
    
        this.idFranchise=franchise.getIdfranquicia();
        
        return "selected";
    
    }
}
