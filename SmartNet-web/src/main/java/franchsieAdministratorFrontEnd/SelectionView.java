/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package franchsieAdministratorFrontEnd;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */

public class SelectionView extends Clases.BaseBacking{

    /**
     * Creates a new instance of SelectionView
     */
    public SelectionView() {
    }
    
    
    private boolean _showDialog;
    
    public void setShowDialog(boolean showDialog){
    
        _showDialog=showDialog;
    
    }
    
    private int franchiseId;
    
    public int getFranchiseId(){
    
        return this.franchiseId;
    
    }
    
    public void setFranchiseId(int franchiseId){
    
        this.franchiseId=franchiseId;
    
    }
    
    private Clases.FranchiseLayout _franchise;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface _franchiseController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface _fileUpload;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.ProductoControllerInterface _productController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private FranchiseAdministratorControllers.SelectionViewController _selectionViewController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.MenuControllerInterface _menuController;
    
   
    
    public Clases.FranchiseLayout getFranchise(){
    
        return _franchise;
    
    }
    
    public void setFranchise(Clases.FranchiseLayout franchise){
    
        _franchise=franchise;
    
    }
    
    
    
    public java.util.List<Clases.MenuLayout>getMenuCollection(){
    
    return _menuController.getMenuLayoutCollection(_franchise.getFranchise());
   
    }
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
       try{
           
           
       
           javax.servlet.http.HttpServletRequest request=this.getRequest();
       
           int idFranchise;
           
           idFranchise=Integer.parseInt(request.getParameter("idFranchise"));
           
           _franchise=new Clases.FranchiseLayoutImplementation();
           
           _franchise.setFranchise(_franchiseController.find(idFranchise));
           
           javax.servlet.ServletContext servletContext=this.getServletContext();
           
           _fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.getSession().getAttribute("username").toString());
        
           if(_franchise.getImage()!=null){

               _fileUpload.loadImagesServletContext(_franchise.getFranchise().getImagenIdimagen(), this.getSession().getAttribute("username").toString());
           
               String cache=_fileUpload.getImages().get(_fileUpload.getImages().size()-1);
               
               _franchise.setImage(cache);
  
           }
           else{
           
                  _franchise.setImage(java.io.File.separator+"images"+java.io.File.separator+"noImage.png");
               
           }
           
       }
       catch(NumberFormatException ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
       }
    
       //Bundle Messages
       
      javax.faces.component.UIViewRoot viewRoot =this.getContext().getViewRoot();
       
      if(viewRoot.findComponent("form:partnershipLink")!=null){
      
          System.out.print("CommandLink is not null");
          
          if(viewRoot.findComponent("form:partnershipLink") instanceof org.primefaces.component.commandlink.CommandLink){
                  
              System.out.print("INSTANCE OF COMMANDLINK");
              
              org.primefaces.component.commandlink.CommandLink link=(org.primefaces.component.commandlink.CommandLink)viewRoot.findComponent("form:partnershipLink");
              
              link.setValue(this._selectionViewController.getLinkMessage(_franchise.getFranchise().getIdfranquicia()));
             
              
              
          }
      
      } 
      
      if(viewRoot.findComponent("form:orderLink") instanceof org.primefaces.component.commandlink.CommandLink){
      
          System.out.print("ORDER LINK IS NOT NULL");
          
      org.primefaces.component.commandlink.CommandLink commandLink=(org.primefaces.component.commandlink.CommandLink)viewRoot.findComponent("form:orderLink");
 
      
      
      if(this._selectionViewController.isPartner(_franchise.getFranchise().getIdfranquicia())){
      
          System.out.print("PARTNER FOUND");
          
          java.util.ResourceBundle bundle=this.getContext().getApplication().getResourceBundle(this.getContext(), "bundle");
          
          commandLink.setValue(bundle.getString("franchiseAdministratorSelection.PlaceOrder"));
      
      }
      
      else{
      
          System.out.print("Partner Not Found");
      
      }
      
      
      }
      
    }
    
    public boolean showMenu(){
    
        return _selectionViewController.showMenu(_franchise.getFranchise());
    
    }
    
    public java.util.List<Clases.ProductoLayout>getProducts(){
    
        try{
        
            java.util.List<Entities.Producto>results=IteratorUtils.toList(_franchiseController.getProducts(_franchise.getFranchise().getIdfranquicia()).iterator());
        
            
                
            java.util.List<Clases.ProductoLayout>productLayout=new java.util.ArrayList<Clases.ProductoLayout>();
            
            if(results!=null && !results.isEmpty()){
            
                for(Entities.Producto product:results){
                
                    Clases.ProductoLayout aux=new Clases.ProductoLayoutImplementation();
                    
                    aux.setProduct(product);
                    
                    aux.setImages(_productController.getImageCollection(product.getProductoPK()));
                
                    productLayout.add(aux);
                    
                    System.out.print("Images "+aux.getProduct().getNombre());
                    
                     for(String auxi:aux.getImages()){
                     
                        System.out.print(auxi); 
                     
                     }
                }
            
            }
            
           
            System.out.print("PRODUCTS SIZE "+productLayout.size());
            
            return productLayout;
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
            return null;
        
        }
    
    }
    
    
    public void createPartnerShip(){
    
    try{
       
        if(_selectionViewController.showDialog(_franchise.getFranchise().getIdfranquicia()))
        
        {
        if(this.getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
    
            Entities.Franquicia franchise=(Entities.Franquicia)this.getSession().getAttribute("selectedOption");
            
            System.out.print("Create partnership between "+franchise.getIdfranquicia()+" and "+_franchise.getFranchise().getIdfranquicia());
            
            this._selectionViewController.addPartnershipRequest(franchise.getIdfranquicia(), _franchise.getFranchise().getIdfranquicia());
        
            System.out.print("PartnetShip Created");
        }
        else{
        
           javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Error Ocurred");
        
           msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
           
           this.getContext().addMessage(null,msg);
        }
        
    }
    }
    catch(javax.faces.event.AbortProcessingException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    public boolean getShowDialog(){
    
    try{
    
        _showDialog=this._selectionViewController.showDialog(_franchise.getFranchise().getIdfranquicia());
        
        return _showDialog;
        
    }
    
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        return false;
    
    }
    
    }
    
    
}
