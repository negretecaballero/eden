/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package franchsieAdministratorFrontEnd;

import org.apache.commons.collections.IteratorUtils;

/**
 *
 * @author luisnegrete
 */
public class SupplierStatisticsView extends Clases.BaseBacking{
    
    private Entities.Franquicia _franchise;
    
    @javax.inject.Inject @javax.enterprise.inject.Default FranchiseAdministratorControllers.SupplierStatisticsController _supplierStatisticsController;
    
    @javax.annotation.PostConstruct
    public void init(){
    
        Integer franchiseId=Integer.parseInt(this.getRequest().getParameter("idFranchise"));
        
        System.out.print("ID FRANCHISE TO FIND "+franchiseId.intValue());
        
        if(franchiseId!=null){
        
            System.out.print("ID FRANCHISE TO FIND "+franchiseId.intValue());
            
            _franchise=_supplierStatisticsController.getFranchise(franchiseId.intValue());
        
        }
        
    
    
    }
  
    
    
    
  
    public java.util.List<Entities.QualificationVariableHasRate>getQualificationVariableHasRateCollection(){
    
        System.out.print("Returning Qualification Variable");
        
        
        
        return IteratorUtils.toList(_supplierStatisticsController.getQualificarionVariableList(_franchise.getIdfranquicia()).iterator());
    
    }
    
    
}
