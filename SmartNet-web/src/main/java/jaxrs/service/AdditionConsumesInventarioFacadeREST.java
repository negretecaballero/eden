/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.AdditionConsumesInventario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */

@Stateless
@Path("entities.additionconsumesinventario")
public class AdditionConsumesInventarioFacadeREST extends AbstractFacade<AdditionConsumesInventario>{

      @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
    return em;
          
          }  

    public AdditionConsumesInventarioFacadeREST() {
        super(AdditionConsumesInventario.class);
    }
    
    private Entities.AdditionConsumesInventarioPK getPrimaryKey(PathSegment ps){
    
      try{
      
      Entities.AdditionConsumesInventarioPK additionConsumesInventarioPK=new Entities.AdditionConsumesInventarioPK();
      
      javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
      
      java.util.List<String>additionIdaddition=map.get("additionIdaddition");
      
      if(additionIdaddition!=null & !additionIdaddition.isEmpty()){
      
      additionConsumesInventarioPK.setAdditionIdaddition(new java.lang.Integer(additionIdaddition.get(0)));
      
      }
      
      java.util.List<String>additionFranquiciaIdFranquicia=map.get("additionFranquiciaIdFranquicia");
      
      if(additionFranquiciaIdFranquicia!=null && !additionFranquiciaIdFranquicia.isEmpty()){
      
      additionConsumesInventarioPK.setAdditionFranquiciaIdFranquicia(new java.lang.Integer(additionFranquiciaIdFranquicia.get(0)));
      
      }
      
      java.util.List<String>inventarioIdinventario=map.get("inventarioIdinventario");
      
      if(inventarioIdinventario!=null && !inventarioIdinventario.isEmpty()){
      
      additionConsumesInventarioPK.setInventarioIdinventario(new java.lang.Integer(inventarioIdinventario.get(0)));
      
      }
      
      java.util.List<String>inventarioFranquiciaIdFranquicia=map.get("inventarioFranquiciaIdFranquicia");
      
      if(inventarioFranquiciaIdFranquicia!=null && !inventarioFranquiciaIdFranquicia.isEmpty()){
      
          additionConsumesInventarioPK.setInventarioFranquiciaIdFranquicia(new java.lang.Integer(inventarioFranquiciaIdFranquicia.get(0)));
      
      }
      return additionConsumesInventarioPK;
      }
      catch(RuntimeException ex){
      
          throw new javax.faces.FacesException(ex);
          
      }
      
    }
    
    private final Entities.AdditionPK getAdditionPK(javax.ws.rs.core.PathSegment ps){
    
        try{
        
            Entities.AdditionPK additionPK=new Entities.AdditionPK();
            
            javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
            
            java.util.List<String>idaddition=map.get("idaddition");
            
            if(idaddition!=null && !idaddition.isEmpty()){
            
                additionPK.setIdaddition(new java.lang.Integer(idaddition.get(0)));
            
            }
            
            java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
            
            if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
            
                additionPK.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
            
            }
            
            return additionPK;
        
        }
        catch(Exception ex){
        
            return null;
        
        }
    
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(AdditionConsumesInventario entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, AdditionConsumesInventario entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.AdditionConsumesInventarioPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public AdditionConsumesInventario find(@PathParam("id") PathSegment id) {
        Entities.AdditionConsumesInventarioPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<AdditionConsumesInventario> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<AdditionConsumesInventario> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByAddition/{additionPK}")
    public java.util.List<Entities.AdditionConsumesInventario>findByAddition(@javax.ws.rs.PathParam("additionPK") javax.ws.rs.core.PathSegment additionPK){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("AdditionConsumesInventario.findByAddition");
            
            query.setParameter("additionPK", this.getAdditionPK(additionPK));
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
 
    
}
