/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Addition;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path(("entitites.addition"))
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class AdditionFacadeREST extends AbstractFacade<Addition>{

    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @javax.annotation.Resource
    javax.ejb.EJBContext context;
    
    @Override
    protected EntityManager getEntityManager() {
        
        return em;
     }
    
    private Entities.SucursalPK getSucursalPK(PathSegment ps){
    
    Entities.SucursalPK sucursalPK=new Entities.SucursalPK();
    
    MultivaluedMap<String,String> map=ps.getMatrixParameters();
    
    if(map.get("idsucursal")!=null && !map.get("idsucursal").isEmpty()){
    
    sucursalPK.setIdsucursal(new java.lang.Integer(map.get("idsucursal").get(0)));
    
    System.out.print("SucursalPK FACADE idsucursal "+map.get("idsucursal").get(0));
    
    }
    
    if(map.get("franquiciaIdfranquicia")!=null && !map.get("franquiciaIdfranquicia").isEmpty()){
    
        sucursalPK.setFranquiciaIdfranquicia(new java.lang.Integer(map.get("franquiciaIdfranquicia").get(0)));
    
        System.out.print("SucursalPK FACADE franquiciaIdfranquicia "+map.get("franquiciaIdfranquicia").get(0));
    }
    
    return sucursalPK;
    }
    
    private Entities.AdditionPK getPrimaryKey(PathSegment ps){
    
        MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        Entities.AdditionPK key=new Entities.AdditionPK();
    
        //idaddition
        //sucursalIdsucursal
        //sucursalFranquiciaIdfranquicia
        
        java.util.List<String>idaddition=map.get("idaddition");
        
        if(idaddition!=null && !idaddition.isEmpty()){
        
            key.setIdaddition(new java.lang.Integer(idaddition.get(0)));
       
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty())      
        {
        
            key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
            
        return key;
    }
    
    public AdditionFacadeREST() {
        super(Addition.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Addition entity) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
        userTransaction.begin();
            
        super.create(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Addition entity) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
         userTransaction.begin();
            
        super.edit(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
            userTransaction.begin();
            

            
        Entities.AdditionPK key = getPrimaryKey(id);
        
        Entities.Addition addition=super.find(key);
        
        userTransaction.commit();
        
        userTransaction.begin();
        
        super.remove(addition);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AdditionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Addition find(@PathParam("id") PathSegment id) {
        Entities.AdditionPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Addition> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Addition> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @GET
    @Path("name/{name}/{franchiseId}")
    @Produces({"application/json","application/xml"})
    public Entities.Addition findByname(@PathParam("name")String name,@PathParam("franchiseId")Integer franchiseId){
    
        try{
        
            System.out.print("Name FACADE "+name);
            
            Query query=em.createNamedQuery("Addition.findByName");
            
            query.setParameter("name", name);
            
            query.setParameter("franchiseId",franchiseId);
            
            Entities.Addition addition= (Entities.Addition)query.getSingleResult();
            
            System.out.print("Addition Name"+addition.getName());
            
            return addition;
            
        }
        catch(NoResultException ex){
        
            return null;
        
        }
    
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findBySucursal/{sucursalPK}")
    public List<Entities.Addition>findBySucursal(@PathParam("sucursalPK") PathSegment sucursalPK){
    
    try{
    
        Query query=em.createNamedQuery("Addition.findBySucursal");
        
        query.setParameter("sucursalPK",getSucursalPK(sucursalPK));
        
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    return null;
    }
    
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByFranchise/{idfranchise}")
    public java.util.List<Entities.Addition>findByFranchise(@PathParam("idfranchise")Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Addition.findByFranchise");
        
        query.setParameter("franchiseId",idFranchise);
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
        
    }
    
    }
    
    
}
