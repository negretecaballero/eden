/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Agrupa;
import Entities.AgrupaPK;
import Entities.SucursalPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.agrupa")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class AgrupaFacadeREST extends AbstractFacade<Agrupa> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Resource
    javax.ejb.EJBContext context;

    private AgrupaPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;productoIdproducto=productoIdproductoValue;menuIdmenu=menuIdmenuValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.AgrupaPK key = new Entities.AgrupaPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> productoIdproducto = map.get("productoIdproducto");
        if (productoIdproducto != null && !productoIdproducto.isEmpty()) {
            key.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
        }
        
        java.util.List<String>productoCategoriaIdCategoria=map.get("productoCategoriaIdCategoria");
        
        if(productoCategoriaIdCategoria!=null && !productoCategoriaIdCategoria.isEmpty()){
        
        key.setProductoCategoriaIdCategoria(new java.lang.Integer(productoCategoriaIdCategoria.get(0)));
        
        }
        
        java.util.List<String> productoCategoriaFranquiciaIdFranquicia=map.get("productoCategoriaFranquiciaIdFranquicia");
        
        if(productoCategoriaFranquiciaIdFranquicia!=null && !productoCategoriaFranquiciaIdFranquicia.isEmpty()){
        
        key.setProductoCategoriaFranquiciaIdFranquicia(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia.get(0)));
        
        }
        
        java.util.List<String> menuIdmenu = map.get("menuIdmenu");
        if (menuIdmenu != null && !menuIdmenu.isEmpty()) {
            key.setMenuIdmenu(new java.lang.Integer(menuIdmenu.get(0)));
        }
        return key;
    }
    
     private SucursalPK getSucursalPK(PathSegment segment){
    
   SucursalPK sucursalpk=new SucursalPK();
        
    MultivaluedMap<String,String> map=segment.getMatrixParameters();
    
    java.util.List<String>idsucursal=map.get("idsucursal");
    
    if(idsucursal!=null && !idsucursal.isEmpty()){
    
    sucursalpk.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
    
    }
    
    java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
    
    sucursalpk.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
    
    }
    
    return sucursalpk;
        
    }
     
     
     private Entities.ProductoPK getProductPK(javax.ws.rs.core.PathSegment segment){
     
        
     Entities.ProductoPK productPK=new Entities.ProductoPK();
     
     javax.ws.rs.core.MultivaluedMap<String,String>map=segment.getMatrixParameters();
     
     java.util.List<String>idproducto=map.get("idproducto");
     
     if(idproducto!=null && !idproducto.isEmpty()){
   
         productPK.setIdproducto(new java.lang.Integer(idproducto.get(0)));
         
   
    }
     
     java.util.List<String>categoriaIdcategoria=map.get("categoriaIdcategoria");
     
     if(categoriaIdcategoria!=null && !categoriaIdcategoria.isEmpty()){
     
         productPK.setCategoriaIdcategoria(new java.lang.Integer(categoriaIdcategoria.get(0)));
     
     }
     
     java.util.List<String>categoriaFranquiciaIdfranquicia=map.get("categoriaFranquiciaIdfranquicia");
     
     if(categoriaFranquiciaIdfranquicia!=null && !categoriaFranquiciaIdfranquicia.isEmpty()){
     
     productPK.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(categoriaFranquiciaIdfranquicia.get(0)));
     
     }
     
     return productPK;
     }

    public AgrupaFacadeREST() {
        super(Agrupa.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Agrupa entity) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
        userTransaction.begin();
            
        super.create(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Agrupa entity) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
        userTransaction.begin();
            
        super.edit(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
        Entities.AgrupaPK key = getPrimaryKey(id);
        
        userTransaction.begin();
        
        Entities.Agrupa agrupa=super.find(key);
        
        userTransaction.commit();
        
        userTransaction.begin();
        
        super.remove(agrupa);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AgrupaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Agrupa find(@PathParam("id") PathSegment id) {
        Entities.AgrupaPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Agrupa> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Agrupa> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
      @GET
    @Path("sucursal/{sucursalid}")
    @Produces({"application/xml","application/json"})
    public List<Agrupa>findByIdSucursal(@PathParam("sucursalid")PathSegment sucursalid){
    
        try{
        
        Query query=em.createNamedQuery("Agrupa.findBySucursalId");
        
        query.setParameter("sucursalPK",getSucursalPK(sucursalid));
        
        return query.getResultList();
        
        }
        
        catch(NoResultException ex){
        
            return null;
        
        }
    
    
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("idMenu/{idmenu}")
    public List<Agrupa>findIdMenu(@PathParam("idmenu") int idmenu){

        try{
        
            Query query=em.createNamedQuery("Agrupa.findByMenuIdmenu");
            
            query.setParameter("menuIdmenu",idmenu);
            
            return query.getResultList();
        }
        catch(NoResultException ex){
        
            return null;
        
        }

}
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByfranchise/{idfanchise}")
    public java.util.List<Entities.Agrupa> findByFranchise(@PathParam("idfranchise") Integer idfranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Agrupa.findByFranchise");
        
        query.setParameter("franchiseid",idfranchise);
        
        return query.getResultList();
    
      }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByProduct/{idproduct}")
    public java.util.List<Entities.Agrupa>findByProduct(@javax.ws.rs.PathParam("idproduct")javax.ws.rs.core.PathSegment key){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Agrupa.findByProduct");
            
            query.setParameter("productPK",this.getProductPK(key));
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            
            return null;
        
        }
    
    }
}
