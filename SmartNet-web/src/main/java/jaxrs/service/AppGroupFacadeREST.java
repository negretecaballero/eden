/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.AppGroup;
import Entities.AppGroupPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.appgroup")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class AppGroupFacadeREST extends AbstractFacade<AppGroup> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;

    private AppGroupPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;groupid=groupidValue;loginAdministradorusername=loginAdministradorusernameValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.AppGroupPK key = new Entities.AppGroupPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> groupid = map.get("groupid");
        if (groupid != null && !groupid.isEmpty()) {
            key.setGroupid(groupid.get(0));
        }
        java.util.List<String> loginAdministradorusername = map.get("loginAdministradorusername");
        if (loginAdministradorusername != null && !loginAdministradorusername.isEmpty()) {
            key.setLoginAdministradorusername(loginAdministradorusername.get(0));
        }
        return key;
    }

    public AppGroupFacadeREST() {
        super(AppGroup.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(AppGroup entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.create(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, AppGroup entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        Entities.AppGroup appGroup=super.find(getPrimaryKey(id));
        
        ut.commit();
        
        ut.begin();
 
        super.remove(appGroup);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(AppGroupFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public AppGroup find(@PathParam("id") PathSegment id) {
        Entities.AppGroupPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<AppGroup> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<AppGroup> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("username/{username}")
    @Produces({"application/xml","application/json"})
    public AppGroup findByUsername(@PathParam("username")PathSegment ps){
    
        try{
        
            AppGroupPK key=getPrimaryKey(ps);
            
            Query query=em.createNamedQuery("AppGroup.findSpecific");
            
            query.setParameter("loginAdministrador",key.getLoginAdministradorusername());
            
            query.setParameter("appgroup",key.getGroupid());
            
            return (AppGroup) query.getSingleResult();
        
        }
        catch(NoResultException ex){
            
            return null;
        
        }
    
    }
    
    @GET 
    @Path("usernameList/{username}")
    @Produces({"application/json","application/xml"})
    public List<AppGroup>findByName(@PathParam("username") String username){
    
    try{
    
        Query query=em.createNamedQuery("AppGroup.findByUsername");
        
        query.setParameter("username", username);
        
        return query.getResultList();
    
    }
    catch(NoResultException ex)
    
    {
    
        return null;
    
    }
    }
    
    
    @GET
    @Path("usernameCount/{username}")
    @Produces({"application/json","application/xml"})
    public int usernameCounter(@PathParam("username")String username){
    
    try{
    
        Query query=em.createNamedQuery("AppGroup.findByUsername");
        
        query.setParameter("username", username);
        
        return query.getResultList().size();
        
    
    }
    catch(NoResultException ex){
    
    return 0;
    }
    
    }
    
}
