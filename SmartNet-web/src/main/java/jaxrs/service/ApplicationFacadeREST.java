/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.LoginAdministrador;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ws.rs.Path("entities.application")
@javax.ejb.Stateless
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ApplicationFacadeREST extends AbstractFacade<Entities.Application>{

    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;

    private Entities.ApplicationPK getKey(javax.ws.rs.core.PathSegment path){
    
        javax.ws.rs.core.MultivaluedMap<String,String>map=path.getMatrixParameters();
        
        Entities.ApplicationPK key=new Entities.ApplicationPK();
        
        java.util.List<String>cedula=map.get("cedula");
        
        if(cedula!=null && !cedula.isEmpty()){
        
            key.setCedula(new java.lang.Long(cedula.get(0)));
        
        }
        
        java.util.List<String>nit=map.get("nit");
        
        if(nit!=null && !nit.isEmpty()){
        
            key.setNit(nit.get(0));
        
        }
        
        return key;
        
    }
    
    
    public ApplicationFacadeREST(){
    
        super(Entities.Application.class);
    
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.Application entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.create(entity);
        
        transaction.commit();
        
        }
        
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @javax.ws.rs.Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") javax.ws.rs.core.PathSegment id, Entities.Application entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        Entities.Application application=super.find(getKey(id));
        
        transaction.commit();
        
        transaction.begin();
        
        super.remove(application);
        
        transaction.commit();
        
        }
        
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.Application find(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        return super.find(getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.Application> findAll() {
       
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Application.findAll");
            
            return query.getResultList();
            
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
        
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.Application> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @Override
    protected EntityManager getEntityManager() {
   return em;
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces
    @javax.ws.rs.Path("findByNit/{nit}")
    public Entities.Application findByNit(@javax.ws.rs.PathParam("nit")String nit){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Application.findByNit");
        
        query.setParameter("nit", nit);
        
        return (Entities.Application) query.getSingleResult();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    public java.util.List<Entities.Application>findByStatus(Entitites.Enum.ApplicationStatusENUM status){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Application.findByStatus");
        
        query.setParameter("status",status);
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @javax.ws.rs.Produces({"applicartion/json","application/xml"})
    @javax.ws.rs.GET
    @javax.ws.rs.Path("findByUsername/{mail}")
    public java.util.List<Entities.Application>findByMail(@javax.ws.rs.PathParam("mail")String mail){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Application.findByMail");
            
            query.setParameter("mail", mail);
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
    
    }
    
}
