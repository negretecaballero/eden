/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Stateless
@javax.ws.rs.Path("entities.applicationstatus")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ApplicationStatusFacadeREST extends jaxrs.service.AbstractFacade<Entities.ApplicationStatus>{
    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    javax.persistence.EntityManager em;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;

    @Override
    protected EntityManager getEntityManager() {
      return this.em;
    }
   
    public ApplicationStatusFacadeREST(){
    
    super(Entities.ApplicationStatus.class);
    
    }
    
    
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    @Override
    @javax.ws.rs.POST
    public void create(Entities.ApplicationStatus applicationStatus){
   
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
            super.create(applicationStatus);
            
            ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
                
                ex.printStackTrace(System.out);
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }

        }
        
    }
    
    @javax.ws.rs.PUT
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    public void edit(@javax.ws.rs.PathParam("id")Integer id,Entities.ApplicationStatus applicationStatus){
    
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
            super.edit(applicationStatus);
            
            ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    
    }
    
    @javax.ws.rs.DELETE
    @javax.ws.rs.Path("{id}")
    public void remove(@javax.ws.rs.PathParam("id")Integer id){
    
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
            Entities.ApplicationStatus applicationStatus=super.find(id);
            
            ut.commit();
            
            ut.begin();
            
            super.remove(applicationStatus);
            
            ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ApplicationStatusFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    
    }
    
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("{id}")
    public Entities.ApplicationStatus find(@javax.ws.rs.PathParam("id")Integer id){

       
        
        return super.find(id);

    }
    
    @Override
    @javax.ws.rs.Produces({"application/json","application.xml"})
    public java.util.List<Entities.ApplicationStatus>findAll(){
    
        return super.findAll();
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
   @javax.ws.rs.Path("findByStatus/{status}")
    public Entities.ApplicationStatus findByStatus(@javax.ws.rs.PathParam("status") Entitites.Enum.ApplicationStatusENUM status){

try{

    javax.persistence.Query query=em.createNamedQuery("ApplicationStatus.findByStatus");
    
    query.setParameter("status", status);
    
    return (Entities.ApplicationStatus)query.getSingleResult();

}
catch(javax.persistence.NoResultException ex){

return null;

}

    }
    
     @javax.ws.rs.GET
    @javax.ws.rs.Path("{from}/{to}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public java.util.List<Entities.ApplicationStatus> findRange(@javax.ws.rs.PathParam("from") Integer from, @javax.ws.rs.PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Path("count")
    @javax.ws.rs.Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    
}
