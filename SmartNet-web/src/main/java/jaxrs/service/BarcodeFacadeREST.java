/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ws.rs.Path("entities.barcode")

@javax.ejb.Stateless

public class BarcodeFacadeREST extends AbstractFacade<Entities.Barcode>{

    @javax.persistence.PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {

    return em;
    
    }
  
    
    public BarcodeFacadeREST(){
    
        super(Entities.Barcode.class);
    
    }
   
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.Barcode entity) {
        
        super.create(entity);
        
        em.flush();
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Entities.Barcode entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.Barcode find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.Barcode> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.Barcode> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @javax.ws.rs.Path("findByBarcode/{barcode}")
    @javax.ws.rs.Produces({"application/xml","application/json"})
    @javax.ws.rs.GET
    public Entities.Barcode findByBarcode(@javax.ws.rs.PathParam("barcode") String barcode){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Barcode.findByBarcode");
            
            query.setParameter("barcode",barcode);
            
            return (Entities.Barcode)query.getSingleResult();
            
        
        }
        catch(javax.persistence.NoResultException | NullPointerException ex){
              
            
        return null;
        
        
        }
    
    }

    
}
