/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Stateless
@javax.ws.rs.Path("entities.bug")

public class BugFacadeREST extends AbstractFacade<Entities.Bug>{
    
 
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
   return em;
    }
    
    public BugFacadeREST(){
    
        super(Entities.Bug.class);
    
    }
    
    @javax.ws.rs.POST
    @Override
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    public void create(Entities.Bug bug){

            super.create(bug);

    }
    
    @javax.ws.rs.PUT
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    public void edit(@javax.ws.rs.PathParam("id")Integer id, Entities.Bug bug){
    
        try{
        
           
            
            super.edit(bug);
            
           
            
        }
        catch(Exception ex){
        
        
        
        }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("{id}")
    public Entities.Bug find(@javax.ws.rs.PathParam("id") Integer id){
    
    return super.find(id);
    
    }
    
    @javax.ws.rs.GET
    @Override
    @javax.ws.rs.Produces({"application/json","application/xml"})
    public java.util.List<Entities.Bug>findAll(){
    
        return super.findAll();
    
    }
    
    @javax.ws.rs.DELETE
    @javax.ws.rs.Path("{id}")
    public void Remove(@javax.ws.rs.PathParam("id")Integer id){
    
        try{
      
            
            Entities.Bug bug=super.find(id);
            
          
            
            super.remove(bug);
       
            
        }
        catch(Exception ex){
    
        }
    
    }
    
    
    @GET
    @Path("{from}/{to}")
    @javax.ws.rs.Produces({"application/xml", "application/json"})
    public java.util.List<Entities.Bug> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
}
