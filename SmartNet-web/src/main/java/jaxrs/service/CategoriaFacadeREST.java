/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.SucursalPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.categoria")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class CategoriaFacadeREST extends AbstractFacade<Categoria> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @javax.annotation.Resource
    javax.ejb.EJBContext context;
    
    private CategoriaPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idcategoria=idcategoriaValue;sucursalIdsucursal=sucursalIdsucursalValue;sucursalFranquiciaIdfranquicia=sucursalFranquiciaIdfranquiciaValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        
        Entities.CategoriaPK key = new Entities.CategoriaPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idcategoria = map.get("idcategoria");
        if (idcategoria != null && !idcategoria.isEmpty()) {
            key.setIdcategoria(new java.lang.Integer(idcategoria.get(0)));
        }
        
        java.util.List<String> sucursalFranquiciaIdfranquicia = map.get("franquiciaIdfranquicia");
        if (sucursalFranquiciaIdfranquicia != null && !sucursalFranquiciaIdfranquicia.isEmpty()) {
            key.setFranquiciaIdfranquicia(new java.lang.Integer(sucursalFranquiciaIdfranquicia.get(0)));
        }
        return key;
    }
    
    private SucursalPK getSucursalPK(PathSegment segment){
    
        MultivaluedMap<String,String> map=segment.getMatrixParameters();
        
        SucursalPK key=new SucursalPK();
        
        java.util.List<String> idsucursal=map.get("idsucursal");
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(idsucursal!=null && !idsucursal.isEmpty()){
        
            key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
        
        }
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
        key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        
        return key;
    
    }

    public CategoriaFacadeREST() {
        super(Categoria.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Categoria entity) {
        
        try{
        
        context.getUserTransaction().begin();
            
        super.create(entity);
        
        context.getUserTransaction().commit();
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Categoria entity) {
        
        try{
        
            context.getUserTransaction().begin();
            
        super.edit(entity);
        
        context.getUserTransaction().commit();
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.CategoriaPK key = getPrimaryKey(id);
        
        try{
        
            context.getUserTransaction().begin();
            
        Entities.Categoria category=super.find(getPrimaryKey(id));
        
        context.getUserTransaction().commit();
        
        context.getUserTransaction().begin();
        
        super.remove(category);
        
        context.getUserTransaction().commit();
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(CategoriaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Categoria find(@PathParam("id") PathSegment id) {
        Entities.CategoriaPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Categoria> findAll() {
       
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Categoria.findAll");
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
        
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Categoria> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @GET
    @Path("sucursal/{idSucursal}")
    @Produces({"application/json","application/xml"})
    public List<Categoria>findByFranchise(Integer franchiseId){
    
    
    try{
    
        Query query=em.createNamedQuery("Categoria.findByFranchise");
        
        query.setParameter("idfranchise",franchiseId);
          
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @GET
    @Path("name/{name}/{idSucursal}")
    @Produces({"application/json","application/xml"})
    public Entities.Categoria findByName(@PathParam("name")String name,Integer franchiseID){
    
        try{
          
            Query query=em.createNamedQuery("Categoria.findByNombre");
            
            query.setParameter("nombre", name);
            
            query.setParameter("idfranquicia", franchiseID);
            
              
            return (Categoria)query.getSingleResult();
        
        }
        catch(NoResultException ex){
        
            return null;
        
        }
    
    }
}
