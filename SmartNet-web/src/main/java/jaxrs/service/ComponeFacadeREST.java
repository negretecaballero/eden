/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Compone;
import Entities.ComponePK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.compone")
public class ComponeFacadeREST extends AbstractFacade<Compone> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    private ComponePK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;productoIdproducto=productoIdproductoValue;inventarioIdinventario=inventarioIdinventarioValue;inventarioSucursalFranquiciaIdfranquicia=inventarioSucursalFranquiciaIdfranquiciaValue;inventarioSucursalIdsucursal=inventarioSucursalIdsucursalValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.ComponePK key = new Entities.ComponePK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> productoIdproducto = map.get("productoIdproducto");
        if (productoIdproducto != null && !productoIdproducto.isEmpty()) {
            key.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
        }
        java.util.List<String>productoCategoriaIdCategoria=map.get("productoCategoriaIdCategoria");
        if(productoCategoriaIdCategoria!=null && !productoCategoriaIdCategoria.isEmpty()){
    
            key.setProductoCategoriaIdCategoria(new java.lang.Integer(productoCategoriaIdCategoria.get(0)));
    
         }
        
        java.util.List<String>productoCategoriaFranquiciaIdFranquicia=map.get("productoCategoriaFranquiciaIdFranquicia");
        
        if(productoCategoriaFranquiciaIdFranquicia!=null && !productoCategoriaFranquiciaIdFranquicia.isEmpty()){
        
            key.setProductoCategoriaFranquiciaIdFranquicia(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia.get(0)));
        
        }
        
        java.util.List<String> inventarioIdinventario = map.get("inventarioIdinventario");
        if (inventarioIdinventario != null && !inventarioIdinventario.isEmpty()) {
            key.setInventarioIdinventario(new java.lang.Integer(inventarioIdinventario.get(0)));
        }
        
        java.util.List<String>inventarioFranquiciaIdFranquicia=map.get("inventarioFranquiciaIdFranquicia");
      
        if(inventarioFranquiciaIdFranquicia!=null && !inventarioFranquiciaIdFranquicia.isEmpty()){
        
        key.setInventarioFranquiciaIdFranquicia(new java.lang.Integer(inventarioFranquiciaIdFranquicia.get(0)));
        
        }
        
        return key;
    }
    
    private Entities.ProductoPK getProductPK(PathSegment ps){
    
    try{
    
    javax.ws.rs.core.MultivaluedMap<String,String> map=ps.getMatrixParameters();    
    
    java.util.List<String>idproducto=map.get("idproducto");
    
    Entities.ProductoPK productPK=new Entities.ProductoPK();   
    
    if(idproducto!=null && !idproducto.isEmpty()){
    
    productPK.setIdproducto(new java.lang.Integer(idproducto.get(0)));
    
    }
    
    java.util.List<String>categoriaIdcategoria=map.get("categoriaIdcategoria");
    
    if(categoriaIdcategoria!=null && !categoriaIdcategoria.isEmpty())
    {
    
        productPK.setCategoriaIdcategoria(new java.lang.Integer(categoriaIdcategoria.get(0)));
    
    }
    
    java.util.List<String>categoriaFranquiciaIdfranquicia=map.get("categoriaFranquiciaIdfranquicia");
    
    if(categoriaFranquiciaIdfranquicia!=null && !categoriaFranquiciaIdfranquicia.isEmpty()){
    
    productPK.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(categoriaFranquiciaIdfranquicia.get(0)));
    
    }
  
    
    return productPK;
    
    
    }
    catch(Exception ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }

    public ComponeFacadeREST() {
        super(Compone.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Compone entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Compone entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.ComponePK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Compone find(@PathParam("id") PathSegment id) {
        Entities.ComponePK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Compone> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Compone> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("idproducto/{idproducto}")
    public List<Compone>findIdProducto(@PathParam("idproducto")PathSegment idProducto){
    
        try{
        
        Query query=em.createNamedQuery("Compone.findByProductoIdproducto");
        
        query.setParameter("idProduct",this.getProductPK(idProducto));
        
        return query.getResultList();
        }
        
        catch(NoResultException ex){
            
        return null;
        
        }
       
        
    }
   
}
