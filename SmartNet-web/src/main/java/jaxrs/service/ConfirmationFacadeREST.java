/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;


import Entities.Confirmation;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.confirmation")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ConfirmationFacadeREST extends AbstractFacade<Confirmation>{
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    
    public ConfirmationFacadeREST() {
        super(Confirmation.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Confirmation entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") String id, Confirmation entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Confirmation entity) {
       
        super.remove(entity);
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Confirmation find(@PathParam("id") String id) {
       
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Confirmation> findAll() {
        return super.findAll();
    }

    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/xml","application/json"})
    @javax.ws.rs.Path("findByUsernameConfirmed/{username}/{confirmed}")
    public Entities.Confirmation findByUsernameConfirmed(@javax.ws.rs.PathParam("username")String username,@javax.ws.rs.PathParam("confirmed")boolean confirmed){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Confirmation.exists");
            
            query.setParameter("username", username);
            
            query.setParameter("confirmed",confirmed);
            
            System.out.print("USERNAME "+username+" CONFIRMED "+confirmed);
            
            Entities.Confirmation confirmation = ((Entities.Confirmation)query.getSingleResult());
            
            System.out.print("IS CONFIRMED "+confirmation.getConfirmed());
            
            return confirmation;
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
    
    
    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Confirmation> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
}
