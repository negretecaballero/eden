/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Direccion;
import Entities.DireccionPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.direccion")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class DireccionFacadeREST extends AbstractFacade<Direccion> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    @Resource
    private EJBContext context;
    
    private DireccionPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idDireccion=idDireccionValue;cITYidCITY=cITYidCITYValue;cITYSTATEidSTATE=cITYSTATEidSTATEValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.DireccionPK key = new Entities.DireccionPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idDireccion = map.get("idDireccion");
        if (idDireccion != null && !idDireccion.isEmpty()) {
            key.setIdDireccion(new java.lang.Integer(idDireccion.get(0)));
        }
        java.util.List<String> cITYidCITY = map.get("cITYidCITY");
        if (cITYidCITY != null && !cITYidCITY.isEmpty()) {
            key.setCITYidCITY(new java.lang.Integer(cITYidCITY.get(0)));
        }
        java.util.List<String> cITYSTATEidSTATE = map.get("cITYSTATEidSTATE");
        if (cITYSTATEidSTATE != null && !cITYSTATEidSTATE.isEmpty()) {
            key.setCITYSTATEidSTATE(new java.lang.Integer(cITYSTATEidSTATE.get(0)));
        }
        return key;
    }

    public DireccionFacadeREST() {
        super(Direccion.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Direccion entity) {
        
        UserTransaction ut=context.getUserTransaction();
        
        try {
            ut.begin();
            
             super.create(entity);
             
             ut.commit();
            
        } catch (Exception ex) {
         
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        } 
        
       
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Direccion entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {

        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
       
       ut.begin();
            
        Entities.Direccion address=super.find(getPrimaryKey(id));
        
        ut.commit();
        
        ut.begin();
        
        super.remove(address);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(DireccionFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Direccion find(@PathParam("id") PathSegment id) {
        Entities.DireccionPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Direccion> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Direccion> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
