/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Franquicia;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.franquicia")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class FranquiciaFacadeREST extends AbstractFacade<Franquicia> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;

    public FranquiciaFacadeREST() {
        super(Franquicia.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Franquicia entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.create(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
      
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Franquicia entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
            
            transaction.begin();
        
            Entities.Franquicia franchise=super.find(id);
            
            transaction.commit();
            
            transaction.begin();
            
        super.remove(franchise);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(FranquiciaFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Franquicia find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Franquicia> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Franquicia> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("username/{username}")
    @Produces({"application/json","application/xml"})
    public List<Franquicia>findByUsername(@PathParam("username")String username){
    
    try{
    
        Query query=em.createNamedQuery("Franquicia.findByUsername");
        
        query.setParameter("username", username);
        
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @GET
    @Path("type/{type}")
    @Produces({"application/json","application/xml"})
    public List<Franquicia>findByType(@PathParam("type") Integer type){
    
        try{
        
            Query query=em.createNamedQuery("Franquicia.findByType");
            
            query.setParameter("type",type);
            
            return query.getResultList();
        
        }
        catch(NoResultException ex){
        
        return null;
        
        }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/xml","application/json"})
    @javax.ws.rs.Path("typename/{name}")
    public java.util.List<Entities.Franquicia>findByTypeName(@javax.ws.rs.PathParam("name")String name){
    
    try{
        
        System.out.print("NAME TO FIND "+name);
    
        javax.persistence.Query query=em.createNamedQuery("Franquicia.findByTypeName");
        
        query.setParameter("name", name);
        
        System.out.print("Result Size "+query.getResultList().size());
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @GET
    @Path("name/{name}")
    @Produces({"application/json","application/xml"})
    public Franquicia findByName(@PathParam("name")String name){
    
    try{
    
        Query query=em.createNamedQuery("Franquicia.findByNombre");
        
        query.setParameter("nombre", name);
        
        return (Franquicia)query.getSingleResult();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
}
