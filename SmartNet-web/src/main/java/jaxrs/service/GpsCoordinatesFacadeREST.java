/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.GpsCoordinates;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.gpscoordinates")

@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class GpsCoordinatesFacadeREST extends AbstractFacade<GpsCoordinates> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public GpsCoordinatesFacadeREST() {
        super(GpsCoordinates.class);
    }
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(GpsCoordinates entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.create(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, GpsCoordinates entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
            Entities.GpsCoordinates gps=super.find(id);
            
            ut.commit();
            
            ut.begin();
            
        super.remove(gps);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(GpsCoordinatesFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public GpsCoordinates find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<GpsCoordinates> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<GpsCoordinates> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByCoordinates/{latitude}/{longitude}")
    public Entities.GpsCoordinates findByCoordinates(@PathParam("latitude")Double latitude,@PathParam("longitude")Double longitude){
    
        try{
        
         javax.persistence.Query query=em.createNamedQuery("GpsCoordinates.findByCoordinates");
         
         query.setParameter("longitude",longitude);
         
         query.setParameter("latitude", latitude);
         
         return (Entities.GpsCoordinates)query.getSingleResult();
        
        }
        catch(NoResultException ex){
        
            return null;
        
        }
    
    }
}
