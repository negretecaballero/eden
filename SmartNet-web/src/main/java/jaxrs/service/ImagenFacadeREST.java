/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Imagen;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.imagen")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ImagenFacadeREST extends AbstractFacade<Imagen> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Resource
    javax.ejb.EJBContext context;
    
    
    
    public ImagenFacadeREST() {
        super(Imagen.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Imagen entity) {
      
        try{
        
        context.getUserTransaction().begin();
        
        super.create(entity);
        
        context.getUserTransaction().commit();
    
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Imagen entity) {
        
        try
        {
        context.getUserTransaction().begin();
        
        super.edit(entity);
        
       context.getUserTransaction().commit();
    }
        catch ( IllegalStateException | SecurityException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException ex) {
         
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
        } 
       
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        
        System.out.print("Id image to remove "+id);
        
        try{
        
            context.getUserTransaction().begin();
            
        Entities.Imagen image=super.find(id);
        
         context.getUserTransaction().commit();
        
         context.getUserTransaction().begin();
         
        super.remove(image);

       context.getUserTransaction().commit();
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ImagenFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Imagen find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Imagen> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Imagen> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
