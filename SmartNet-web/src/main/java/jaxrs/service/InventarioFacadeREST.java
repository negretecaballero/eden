/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Inventario;
import Entities.InventarioPK;
import Entities.SucursalPK;
import com.aspose.barcode.internal.cv.at;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.inventario")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class InventarioFacadeREST extends AbstractFacade<Inventario> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;



    
    @Resource
    javax.ejb.EJBContext context;
    
    private InventarioPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idinventario=idinventarioValue;sucursalIdsucursal=sucursalIdsucursalValue;sucursalFranquiciaIdfranquicia=sucursalFranquiciaIdfranquiciaValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.InventarioPK key = new Entities.InventarioPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idinventario = map.get("idinventario");
        if (idinventario != null && !idinventario.isEmpty()) {
            key.setIdinventario(new java.lang.Integer(idinventario.get(0)));
        }
     
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
            key.setFranquiciaIdFranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        return key;
    }
    
    private SucursalPK getSucursalKey(PathSegment segment){
        
        SucursalPK sucursalpk=new SucursalPK();
    
    MultivaluedMap<String,String> map=segment.getMatrixParameters();
    
    java.util.List<String>idsucursal=map.get("idsucursal");
    
    if(idsucursal!=null && !idsucursal.isEmpty()){
    
    sucursalpk.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
        
    }
    
    java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty())
    {
    
        sucursalpk.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
    
    }
    return sucursalpk;
        
    }

    public InventarioFacadeREST() {
        super(Inventario.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
   
    public void create(Inventario entity) {
        
        try{
        
        context.getUserTransaction().begin();
            
        super.create(entity);
        
        context.getUserTransaction().commit();
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
     
    public void edit(@PathParam("id") PathSegment id, Inventario entity) {
        
        try{
        
            context.getUserTransaction().begin();
            
        super.edit(entity);
        
        context.getUserTransaction().commit();
        
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")

    public void remove(@PathParam("id") PathSegment id) {
        Entities.InventarioPK key = getPrimaryKey(id);
       
        try{
        
            context.getUserTransaction().begin();
            
         Entities.Inventario invetory=super.find(key);
        
       context.getUserTransaction().commit();
        
       context.getUserTransaction().begin();

       
        super.remove(super.find(key));
        
        context.getUserTransaction().commit();
        }
        catch(Exception ex){
        
            try {
                context.getUserTransaction().rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
   
    public Inventario find(@PathParam("id") PathSegment id) throws IllegalStateException {
        Entities.InventarioPK key = getPrimaryKey(id);
        
      
      
            
        return super.find(key);
        
        
        
       
        
        }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})

    public List<Inventario> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Inventario> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
@GET
    @Path("idsucursal/{sucursalid}")
    @Produces({"application/xml","application/json"})
    public List<Inventario>getByIdSucursal(@PathParam("sucursalid")PathSegment sucursalid){
    
        try{
        
            SucursalPK sucursalpk=getSucursalKey(sucursalid);
            
           Query query=em.createNamedQuery("Inventario.findBySucursal&Franquicia");
    
    query.setParameter("sucursalIdsucursal", sucursalpk.getIdsucursal());
    
    query.setParameter("sucursalFranquiciaIdfranquicia", sucursalpk.getFranquiciaIdfranquicia());
    
    
    
    return query.getResultList();
     
            
        }
        catch(NoResultException ex){
        
            return null;
        }
        
    }
    
    @GET 
    @Path("name/{name}/{idfranchise}")
    @Produces({"application/xml","application/json"})
    public Inventario findByName(@PathParam("name")String name,@PathParam("idfranchise")Integer idFranchise){
    
    try{
    
       
        
        Query query=em.createNamedQuery("Inventario.findByName");
        
        query.setParameter("name", name);
        
        query.setParameter("idFranchise", idFranchise);
        
        return (Inventario)query.getSingleResult();
        
        
    
    }
    catch(NoResultException ex){
    
    return null;
    
    }
    
    
    }
  
    @GET
    @Produces({"application/xml","application/json"})
    @Path("findByFranchise/{idfranchise}")
    public java.util.List<Entities.Inventario>getByFranchise(@PathParam("idfranchise")Integer idfranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Inventario.findByFranchise");
        
        query.setParameter("franchiseId",idfranchise);
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    
    }
    
}
