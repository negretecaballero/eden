/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@javax.ws.rs.Path("entities.inventariohassucursal")
@javax.ejb.Stateless
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class InventarioHasSucursalFacadeREST extends AbstractFacade<Entities.InventarioHasSucursal>{

    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    public InventarioHasSucursalFacadeREST (){
    
        super(Entities.InventarioHasSucursal.class);
    
    }
    
    private Entities.InventarioHasSucursalPK getKey(PathSegment ps){
    
    javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    Entities.InventarioHasSucursalPK key=new Entities.InventarioHasSucursalPK();
    
    java.util.List<String>inventarioIdinventario=map.get("inventarioIdinventario");
    
    if(inventarioIdinventario!=null && !inventarioIdinventario.isEmpty()){
    
        key.setInventarioIdinventario(new java.lang.Integer(inventarioIdinventario.get(0)));
    
    }
    
    java.util.List<String>inventarioFranquiciaIdfranquicia=map.get("inventarioFranquiciaIdfranquicia");
    
    if(inventarioFranquiciaIdfranquicia!=null && !inventarioFranquiciaIdfranquicia.isEmpty()){
    
        key.setInventarioFranquiciaIdfranquicia(new java.lang.Integer(inventarioFranquiciaIdfranquicia.get(0)));
    
    }
    
    java.util.List<String>sucursalIdsucursal=map.get("sucursalIdsucursal");
    
    if(sucursalIdsucursal!=null && !sucursalIdsucursal.isEmpty()){
    
    key.setSucursalIdsucursal(new java.lang.Integer(sucursalIdsucursal.get(0)));
    
    }
    
    java.util.List<String>sucursalFranquiciaIdfranquicia=map.get("sucursalFranquiciaIdfranquicia");
    
    if(sucursalFranquiciaIdfranquicia!=null && !sucursalFranquiciaIdfranquicia.isEmpty()){
    
    key.setSucursalFranquiciaIdfranquicia(new java.lang.Integer(sucursalFranquiciaIdfranquicia.get(0)));
    
    }
    
    return key;
    
    }
    
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
    return em; 
    }
   
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.InventarioHasSucursal entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.create(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.InventarioHasSucursal entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
            Entities.InventarioHasSucursal inventarioHasSucursal=super.find(this.getKey(id));
            
            ut.commit();
        
            ut.begin();
        
        super.remove(inventarioHasSucursal);
        
        ut.commit();
        
        }
        
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(InventarioHasSucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.InventarioHasSucursal find(@PathParam("id") PathSegment id) {
        Entities.InventarioHasSucursalPK key = getKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.InventarioHasSucursal> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.InventarioHasSucursal> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    private Entities.SucursalPK getSucursalPK(PathSegment ps){
    
        Entities.SucursalPK key=new Entities.SucursalPK();
        
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idsucursal=map.get("idsucursal");
        
        if(idsucursal!=null && !idsucursal.isEmpty()){
        
            key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
        
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
            key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        return key;
    
    }
    
    @javax.ws.rs.Path("findBySucursalPK/{sucursalPK}")
    @GET
    @Produces({"application/json","application/xml"})
    
    public java.util.List<Entities.InventarioHasSucursal>findBySucursal(@PathParam("sucursalPK")PathSegment ps){
    
    try{
    
        Entities.SucursalPK key=this.getSucursalPK(ps);
        
        javax.persistence.Query query=em.createNamedQuery("InventarioHasSucursal.findBySucursalPK");
        
        query.setParameter("idFranchise", key.getFranquiciaIdfranquicia());
        
        query.setParameter("idSucursal", key.getIdsucursal());
        
        
        return query.getResultList();
    
    }
    
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
}
