/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.LastSeenMenu;
import Entities.LastSeenMenuPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.lastseenmenu")
public class LastSeenMenuFacadeREST extends AbstractFacade<LastSeenMenu>{
    
     @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
     
     public LastSeenMenuFacadeREST(){
     
         super(LastSeenMenu.class);
     
     }

     
     private LastSeenMenuPK getPrimaryKey(PathSegment ps){
     
         MultivaluedMap <String,String> map=ps.getMatrixParameters();
         
         LastSeenMenuPK key=new LastSeenMenuPK();
     
         java.util.List<String>loginAdministradorusername=map.get("loginAdministradorusername");
         
         if(loginAdministradorusername!=null && !loginAdministradorusername.isEmpty()){
         
             key.setLoginAdministradorusername(loginAdministradorusername.get(0));
         
         }
         
         java.util.List<String>menuIdmenu=map.get("menuIdmenu");
         
         if(menuIdmenu!=null && !menuIdmenu.isEmpty()){
         
             key.setMenuIdmenu(new java.lang.Integer(menuIdmenu.get(0)));
         
         }
         
         
         return key;
     }
     
    @Override
    protected EntityManager getEntityManager() {
  return em; 
    }
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(LastSeenMenu entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, LastSeenMenu entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.LastSeenMenuPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public LastSeenMenu find(@PathParam("id") PathSegment id) {
        Entities.LastSeenMenuPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<LastSeenMenu> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<LastSeenMenu> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

}
