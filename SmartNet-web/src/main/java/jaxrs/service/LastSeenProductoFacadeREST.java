/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.LastSeenProducto;
import Entities.LastSeenProductoPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.lastseenproducto")
public class LastSeenProductoFacadeREST extends AbstractFacade<LastSeenProducto>{
    
     @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
     
     public LastSeenProductoFacadeREST(){
     
         super(LastSeenProducto.class);
     
     }

    @Override
    protected EntityManager getEntityManager() {
   return em;
    }
    
    private LastSeenProductoPK getPrimaryKey(PathSegment ps){
    
        LastSeenProductoPK key=new LastSeenProductoPK();
        
        MultivaluedMap <String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>loginAdministradorusername=map.get("");
        
        if(loginAdministradorusername!=null && !loginAdministradorusername.isEmpty()){
        
            key.setLoginAdministradorusername(loginAdministradorusername.get(0));
        
        }
        
        java.util.List<String>productoIdproducto=map.get("productoIdproducto");
        
        if(productoIdproducto!=null && !productoIdproducto.isEmpty()){
        
        key.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
        
        }
        
        return key;
    
    }
    
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(LastSeenProducto entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, LastSeenProducto entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.LastSeenProductoPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public LastSeenProducto find(@PathParam("id") PathSegment id) {
        Entities.LastSeenProductoPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<LastSeenProducto> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<LastSeenProducto> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
}
