/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Menu;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.menu")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class MenuFacadeREST extends AbstractFacade<Menu> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Resource
    private javax.ejb.EJBContext context;
    
    public MenuFacadeREST() {
        super(Menu.class);
    }

    
    private Entities.SucursalPK getSucursalPK(PathSegment ps){
    
        Entities.SucursalPK sucursalPK=new Entities.SucursalPK();
        
        MultivaluedMap <String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idsucursal=map.get("idsucursal");
        
        if(idsucursal!=null && !idsucursal.isEmpty()){
        
            sucursalPK.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
        
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
            sucursalPK.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        return sucursalPK;
    
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Menu entity) {
        
        UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
           userTransaction.begin();
            
               super.create(entity);
        
           userTransaction.commit();
        
        }
        catch(NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Menu entity) {
        
         UserTransaction userTransaction=context.getUserTransaction();
        
        try{

            userTransaction.begin();
            
             super.edit(entity);
             
             userTransaction.commit();
        
        }
        catch(Exception ex){
        
             try {
                 userTransaction.rollback();
             } catch (IllegalStateException ex1) {
                 Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
             } catch (SecurityException ex1) {
                 Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
             } catch (SystemException ex1) {
                 Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
             }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        
        UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
            userTransaction.begin();
            
            Entities.Menu menu=super.find(id);
            
            userTransaction.commit();
            
            userTransaction.begin();
            
        super.remove(menu);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(MenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Menu find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Menu> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Menu> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
}
