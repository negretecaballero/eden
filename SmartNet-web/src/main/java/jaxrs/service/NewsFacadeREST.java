/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateless

@javax.ws.rs.Path("entities.news")

public class NewsFacadeREST extends AbstractFacade<Entities.News>{

    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;
    
    
    private Entities.Tipo getType(javax.ws.rs.core.PathSegment ps){
    
        if(ps!=null){
        
        Entities.Tipo type=new Entities.Tipo();
        
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idtipo=map.get("idtipo");
        
        if(idtipo!=null && !idtipo.isEmpty()){
        
           type.setIdtipo(new java.lang.Integer(idtipo.get(0)));
        
        }
        
        java.util.List<String>nombre=map.get("nombre");
        
        if(nombre!=null && !nombre.isEmpty()){
        
        type.setNombre(nombre.get(0));
        
        }
        
        return type;
        
        }
    
        return null;
    }
    
    @Override
    protected EntityManager getEntityManager() {
    
        return em;
    
    }
   
    
    public NewsFacadeREST(){
    
    super(Entities.News.class);
    
    }
    
    
       @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.News entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Entities.News entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
      
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.News find(@PathParam("id") Integer id) {
 
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.News> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.News> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
  
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("ordered")
    public java.util.Collection gerOrdered(){
    
     try{
     
         javax.persistence.Query query=em.createNamedQuery("News.orderedByDate");
         
         return query.getResultList();
     
     }
     catch(javax.persistence.NoResultException ex){
     
         return null;
     
     }
    
    }
    
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByType/{type}/{tipo}")
    public java.util.Collection<Entities.News> findByType(@javax.ws.rs.PathParam("type")String type,@javax.ws.rs.PathParam("tipo")javax.ws.rs.core.PathSegment tipo){
          
    try{
    
        javax.persistence.Query query=em.createNamedQuery("News.findByType");
        
        query.setParameter("type",type);
        
        query.setParameter("global","Global");
        
        System.out.print("Tipo Value" +this.getType(tipo));
        
        query.setParameter("tipo", this.getType(tipo));
        
        
        java.util.List<Entities.News>resultList=new java.util.ArrayList<Entities.News>();
        
        resultList=query.getResultList();
        
        javax.persistence.Query query2=em.createNamedQuery("News.findByType");
        
           query2.setParameter("type",type);
        
        query2.setParameter("global","Global");
        
      
        
        query2.setParameter("tipo", null);
        
        System.out.print("Query2 size "+query2.getResultList().size());
        
        
        for(Entities.News aux:(java.util.List<Entities.News>)query2.getResultList()){
        
            resultList.add(aux);
        
        }
        
        return resultList;
    
    }
    catch(javax.persistence.NoResultException ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByTipo/{tipo}")
    public java.util.List<Entities.News> findByTipo(@javax.ws.rs.PathParam("tipo")javax.ws.rs.core.PathSegment tipo){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("News.findByTipo");
  
            
            return query.getResultList();
        
        }
        catch(javax.ejb.EJBException | StackOverflowError ex){
        
            return null;
        
        }
    
    }
}
