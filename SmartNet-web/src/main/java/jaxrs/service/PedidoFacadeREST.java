/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Pedido;
import Entities.SucursalPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.pedido")
public class PedidoFacadeREST extends AbstractFacade<Pedido> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PedidoFacadeREST() {
        super(Pedido.class);
    }
    
    private SucursalPK getSucursalPK (PathSegment ps){
    
    SucursalPK key=new SucursalPK();
    
    MultivaluedMap <String,String> map=ps.getMatrixParameters();
    
    java.util.List<String>idsucursal=map.get("idsucursal");
    
    if(idsucursal!=null && !idsucursal.isEmpty()){
    
    key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
    
    }
    
    java.util.List<String>franquiciaIdfranqucia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranqucia!=null && !franquiciaIdfranqucia.isEmpty()){
    
    key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranqucia.get(0)));
    
    }
    
    
    
    return key;
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Pedido entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Pedido entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Pedido find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Pedido> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Pedido> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("pedidoState/{pedidoStateId}/{sucursalPK}")
    @Produces({"application/json","application/xml"})
    public List<Pedido>findByState(@PathParam("pedidoStateId") String pedidoState, @PathParam("sucursalPK") PathSegment sucursalPK){
    
    try{
    
    Query query=em.createNamedQuery("Pedido.findByState");
    
    query.setParameter("pedidoStateId",pedidoState);
    
    query.setParameter("sucursalPK", getSucursalPK(sucursalPK));
    
    return query.getResultList();
    
    }
    
    catch(NoResultException ex){
    
        return null;
    
    }
    
    
    }
    
    
    @GET
    @Path("sucursalId/{idsucursal}")
    @Produces({"applicaton/json","application/xml"})
    public List<Pedido>findBySucursal(@PathParam("idsucursal")PathSegment idsucursal){
    
    try{
    
        SucursalPK key=getSucursalPK(idsucursal);
        
        Query query=em.createNamedQuery("Pedido.findBySucursal");
        
        query.setParameter("sucursalPK", key);
        
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    
    return null;
    
    }
    
    
    
    }
    
    @GET
    @Path("findByUsername/{loginAdministrador}/{idstate}")
    @Produces({"application/json","application/xml"})
    public List<Entities.Pedido>findByUser(@PathParam("loginAdministrador")String loginAdministrador, @PathParam("idstate")String idstate){
    
        try{
        
            Query query=em.createNamedQuery("Pedido.findByUser");
            
            query.setParameter("loginAdministrador",loginAdministrador);
            
            query.setParameter("pedidoStateId", idstate);
            
            return query.getResultList();
            
        }
        catch(NoResultException ex){
        
            return null;
        
        }
    
    }
    
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByIdFranquicia/{idfranquicia}")
    public java.util.List<Entities.Pedido>findByIdFranquicia(@PathParam("idfranquicia") Integer idfranquicia){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Pedido.findByIdFranquicia");
            
            query.setParameter("idfranquicia", idfranquicia);
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
        
    }
    
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByDateInterval/{from}/{to}/{sucursalPK}")
    public java.util.List<Entities.Pedido>findByDateInterval(@PathParam("from")java.util.Date from,@PathParam("to") java.util.Date to,@PathParam("sucursalPK")PathSegment pathSegment){
    
        try{
        
            Entities.SucursalPK sucursalPK=this.getSucursalPK(pathSegment);
            
            javax.persistence.Query query=em.createNamedQuery("Pedido.findByDateInterval");
        
            query.setParameter("from", from);
            
            query.setParameter("to", to);
            
            query.setParameter("sucursalPK",sucursalPK);
            
            return query.getResultList();
            
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
    
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("orderedByState/{idsucursal}")
    public java.util.List<Entities.Pedido>orderedByState(@PathParam("idsucursal")PathSegment pathSegment){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Pedido.orderedByState");
            
            query.setParameter("sucursalPK",this.getSucursalPK(pathSegment));
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
    
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.GET
    @javax.ws.rs.Path("findSpecificFranchise/{idPedido}/{idFranchise}")
    public Entities.Pedido findSpecificFranchise(@javax.ws.rs.PathParam("idPedido")Integer idPedido,@javax.ws.rs.PathParam("idFranchise")Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Pedido.FindSpecificByFranchise");
        
        query.setParameter("idPedido",idPedido);
        
        query.setParameter("franchiseId",idFranchise);
        
        return (Entities.Pedido)query.getSingleResult();
    
    }
    catch(javax.persistence.NoResultException ex){
     
        return null;
    
    }
    
    }
    
}
