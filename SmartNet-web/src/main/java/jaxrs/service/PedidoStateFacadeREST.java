/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Pedido;
import Entities.PedidoState;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.pedidostate")
public class PedidoStateFacadeREST extends AbstractFacade<PedidoState> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PedidoStateFacadeREST() {
        super(PedidoState.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(PedidoState entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, PedidoState entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public PedidoState find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<PedidoState> findAll() {
        return super.findAll();
    }

    @GET
    @Produces({"application/json","application/xml"})
    @Path("state/{state}")
    public Entities.PedidoState findByState(@PathParam("state")String state){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("PedidoState.findByState");
        
            query.setParameter("state", state);
            
            return (Entities.PedidoState) query.getSingleResult();
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
            
        }
    
    }
    
    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<PedidoState> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    
    
}
