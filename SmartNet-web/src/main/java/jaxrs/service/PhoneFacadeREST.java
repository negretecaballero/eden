/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Phone;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.phone")
public class PhoneFacadeREST extends AbstractFacade<Phone> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public PhoneFacadeREST() {
        super(Phone.class);
      
    }
    
    

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Phone entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") String id, Phone entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Phone find(@PathParam("id") String id) {
        return super.find(id);
    }

    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByNumber/{phoneNumber}")
    public Entities.Phone findByNumber(@PathParam("phoneNumber")String phoneNumber){
    
    try{
    
        javax.persistence.Query  query=em.createNamedQuery("Phone.findByPhoneNumber");
        
        query.setParameter("phoneNumber",phoneNumber);
        
        return (Entities.Phone)query.getSingleResult();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Phone> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Phone> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByUsername/{username}")
    public java.util.List<Entities.Phone>findByName(@PathParam("username")String username){
    
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Phone.findByUSername");
        
        query.setParameter("username",username);
    
        return query.getResultList();
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
}
