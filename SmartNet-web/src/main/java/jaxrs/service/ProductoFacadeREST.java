/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.CategoriaPK;
import Entities.Producto;
import Entities.SucursalPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.producto")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class ProductoFacadeREST extends AbstractFacade<Producto> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @Resource
    javax.ejb.EJBContext context;

    public ProductoFacadeREST() {
        super(Producto.class);
    }
    
    private Entities.ProductoPK getKey(PathSegment ps){
    
    try{
        
        Entities.ProductoPK key=new Entities.ProductoPK();
        
        MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idproducto=map.get("idproducto");
        
        if(idproducto!=null && !idproducto.isEmpty()){
        
            key.setIdproducto(new java.lang.Integer(idproducto.get(0)));
        
        }
        
        java.util.List<String>categoriaIdcategoria=map.get("categoriaIdcategoria");
        
        if(categoriaIdcategoria!=null && !categoriaIdcategoria.isEmpty()){
        
        key.setCategoriaIdcategoria(new java.lang.Integer(categoriaIdcategoria.get(0)));
        
        }
        
        java.util.List<String>categoriaFranquiciaIdfranquicia=map.get("categoriaFranquiciaIdfranquicia");
        
        if(categoriaFranquiciaIdfranquicia!=null && !categoriaFranquiciaIdfranquicia.isEmpty())
        {
        
            key.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(categoriaFranquiciaIdfranquicia.get(0)));
        
        }
        
        return key;
    
    }
    catch(Exception ex){
    
       
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    CategoriaPK getCategoriaPK(PathSegment ps){
    
    MultivaluedMap<String,String> map=ps.getMatrixParameters();
    
    CategoriaPK categoriaPK=new CategoriaPK();
    
    java.util.List<String>idcategoria=map.get("idcategoria");
    
    if(idcategoria!=null && !idcategoria.isEmpty()){
    
        categoriaPK.setIdcategoria( new java.lang.Integer(idcategoria.get(0)));
    
    }
    
  
    
    java.util.List<String>sucursalFranquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(sucursalFranquiciaIdfranquicia!=null && !sucursalFranquiciaIdfranquicia.isEmpty()){
    
        categoriaPK.setFranquiciaIdfranquicia(new java.lang.Integer(sucursalFranquiciaIdfranquicia.get(0)));
    }
    
    return categoriaPK;
    
    }
    
    private SucursalPK getSucursalPK(PathSegment sucursalKey){
    
    SucursalPK key=new SucursalPK();
    
    MultivaluedMap<String,String> map=sucursalKey.getMatrixParameters();
    
    java.util.List<String> idsucursal=map.get("idsucursal");
    
    if(idsucursal!=null && !idsucursal.isEmpty()){
    
    System.out.print("Id Sucursal Value "+idsucursal.get(0));
        
    key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
    
    }
    
    java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
    System.out.print("Id Franquicia Value "+franquiciaIdfranquicia.get(0));
    
    key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
    
    }
    
    return key;
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Producto entity) {
        
          javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
 
        transaction.begin();
            
        super.create(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
              try {
                  transaction.rollback();
              } catch (IllegalStateException ex1) {
                  Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
              } catch (SecurityException ex1) {
                  Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
              } catch (SystemException ex1) {
                  Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
              }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Producto entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
            Entities.Producto product=super.find(this.getKey(id));
            
            transaction.commit();
            
            transaction.begin();
            
            super.remove(product);
        
            transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(ProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Producto find(@PathParam("id") PathSegment id) {
  
        return super.find(this.getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Producto> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Producto> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @GET
    @Path("name/{name}/{idCategoria}")
    @Produces({"application/json","application/xml"})
    public Producto findByName(@PathParam("name")String name,@PathParam("idCategoria")PathSegment idCategoria){
    
        try{
        
            Query query=em.createNamedQuery("Producto.findByName");
            
            query.setParameter("name", name);
            
            CategoriaPK key=getCategoriaPK(idCategoria);
            
            query.setParameter("categoryPK",key);
            
            return (Producto)query.getSingleResult();
        
        }
        catch(NoResultException ex){
        
            return null;
        
        }
    
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByFranchise/{idfranchise}")
    public java.util.List<Entities.Producto>findByFranchise(@PathParam("idfranchise")Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Producto.findByCategoriaFranquiciaIdfranquicia");
        
        query.setParameter("categoriaFranquiciaIdfranquicia",idFranchise);
        
        return query.getResultList();
        
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
        
    }
    
    }
    
    
    @GET
    @Path("sucursalId/{sucursalPK}")
    @Produces({"application/json","application/xml"})
    public List<Entities.Producto>findBySucursal(@PathParam("sucursalPK") PathSegment sucursalPK){
    
    try{
    
        Query query=em.createNamedQuery("Producto.findBySucursal");
        
        query.setParameter("sucursalPK", getSucursalPK(sucursalPK));
        
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByBarcode/{barcode}")
    public java.util.List<Entities.Producto> findByBarcode(@javax.ws.rs.PathParam("barcode")String barcode){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Producto.findByBarcode");
            
            query.setParameter("barcode", barcode);
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
    
}
