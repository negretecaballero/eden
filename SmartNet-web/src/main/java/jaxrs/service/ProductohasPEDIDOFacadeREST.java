/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.ProductohasPEDIDO;
import Entities.ProductohasPEDIDOPK;
import Entities.SucursalPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.productohaspedido")
public class ProductohasPEDIDOFacadeREST extends AbstractFacade<ProductohasPEDIDO> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    
    private SucursalPK getSucursalPK(PathSegment ps){
    
    SucursalPK key=new SucursalPK();
    
    MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    java.util.List<String>idsucursal=map.get("idsucursal");
    
    if(idsucursal!=null && !idsucursal.isEmpty()){
    
        key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
    
    }
    
    java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
    
    key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
    
    }
    
    
    return key;
    
    }

    private ProductohasPEDIDOPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;productoIdproducto=productoIdproductoValue;pEDIDOidPEDIDO=pEDIDOidPEDIDOValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.ProductohasPEDIDOPK key = new Entities.ProductohasPEDIDOPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> productoIdproducto = map.get("productoIdproducto");
        if (productoIdproducto != null && !productoIdproducto.isEmpty()) {
            key.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
        }
        
        java.util.List<String>productoCategoriaIdCategoria=map.get("productoCategoriaIdCategoria");
        
        if(productoCategoriaIdCategoria!=null && !productoCategoriaIdCategoria.isEmpty()){
        
        key.setProductoCategoriaIdCategoria(new java.lang.Integer(productoCategoriaIdCategoria.get(0)));
        
        }
        
        java.util.List<String>productoCategoriaFranquiciaIdFranquicia=map.get("productoCategoriaFranquiciaIdFranquicia");
        
        if(productoCategoriaFranquiciaIdFranquicia!=null && !productoCategoriaFranquiciaIdFranquicia.isEmpty()){
        
        key.setproductoCategoriaFranquiciaIdFranquicia(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia.get(0)));
        
        }
        
        
        
        java.util.List<String> pEDIDOidPEDIDO = map.get("pEDIDOidPEDIDO");
        if (pEDIDOidPEDIDO != null && !pEDIDOidPEDIDO.isEmpty()) {
            key.setPEDIDOidPEDIDO(new java.lang.Integer(pEDIDOidPEDIDO.get(0)));
        }
        return key;
    }

    public ProductohasPEDIDOFacadeREST() {
        super(ProductohasPEDIDO.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(ProductohasPEDIDO entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, ProductohasPEDIDO entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.ProductohasPEDIDOPK key = getPrimaryKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public ProductohasPEDIDO find(@PathParam("id") PathSegment id) {
        Entities.ProductohasPEDIDOPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<ProductohasPEDIDO> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<ProductohasPEDIDO> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("idpedido/sucursalpk/{idPedido}/{sucursalPK}")
    @Produces({"application/json","application/xml"})
    public List<ProductohasPEDIDO>findByPedidoSucursal(@PathParam("idPedido")Integer idPedido,@PathParam("sucursalPK") PathSegment sucursalPK){
    
    try{
    
        Query query=em.createNamedQuery("ProductohasPEDIDO.findByPedido&Sucursal");
        
        query.setParameter("idPedido", idPedido);
        
        query.setParameter("sucursalPK", getSucursalPK(sucursalPK));
        
        return query.getResultList();
    
    }
    catch(NoResultException ex){
    
    return null;
    
    }
    
    }
    
}
