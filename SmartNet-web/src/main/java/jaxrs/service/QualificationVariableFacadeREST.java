/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateless
@javax.ws.rs.Path("entities.qualificationvariable")
public class QualificationVariableFacadeREST extends jaxrs.service.AbstractFacade<Entities.QualificationVariable>{

    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    javax.persistence.EntityManager em;
    
    public QualificationVariableFacadeREST(){
    
        super(Entities.QualificationVariable.class);
    
    }
    
    @Override
    protected EntityManager getEntityManager() {
    
        return em;
    
    }
    
      @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.QualificationVariable entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Entities.QualificationVariable entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.QualificationVariable find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.QualificationVariable> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.QualificationVariable> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
}
