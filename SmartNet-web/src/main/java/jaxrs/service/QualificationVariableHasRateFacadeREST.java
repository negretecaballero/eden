/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateless
@javax.ws.rs.Path("entities.qualificationvariablehasrate")
@TransactionManagement( TransactionManagementType.BEAN )
public class QualificationVariableHasRateFacadeREST extends jaxrs.service.AbstractFacade<Entities.QualificationVariableHasRate>{

    @Resource
 private EJBContext context;
    
 
    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;
    
    public QualificationVariableHasRateFacadeREST(){
    
    super(Entities.QualificationVariableHasRate.class);
    
    }
    
    @Override
    protected EntityManager getEntityManager() {
  
    return em;
    
    }
    
    private Entities.QualificationVariableHasRatePK getKey(javax.ws.rs.core.PathSegment ps){
    
    javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    Entities.QualificationVariableHasRatePK key=new Entities.QualificationVariableHasRatePK();
    
    java.util.List<String>qualificationVariableIdqualificationVariable=map.get("qualificationVariableIdqualificationVariable");
    
    if(qualificationVariableIdqualificationVariable!=null && !qualificationVariableIdqualificationVariable.isEmpty()){
    
        key.setQualificationVariableIdqualificationVariable(new java.lang.Integer(qualificationVariableIdqualificationVariable.get(0)));
    
    }
    
    java.util.List<String>rateIdrate=map.get("rateIdrate");
    
    if(rateIdrate!=null && !rateIdrate.isEmpty()){
    
    key.setRateIdrate(new java.lang.Integer(rateIdrate.get(0)));
    
    }
    
    java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
    
    if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
    
        key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
    
    }
    
    
    return key;
    }
    
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.QualificationVariableHasRate entity) {
      
        UserTransaction ut=this.context.getUserTransaction();
        
        try{
        
        ut.begin();
        
        }
        catch(RollbackException ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        } catch (NotSupportedException ex) {
            Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SystemException ex) {
            Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        finally{
        
               super.create(entity);
        
            try {
                ut.commit();
            } catch (javax.transaction.RollbackException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            } catch (HeuristicMixedException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            } catch (HeuristicRollbackException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SecurityException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalStateException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SystemException ex) {
                Logger.getLogger(QualificationVariableHasRateFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
     
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") javax.ws.rs.core.PathSegment id, Entities.QualificationVariableHasRate entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        super.remove(super.find(this.getKey(id)));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.QualificationVariableHasRate find(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        return super.find(this.getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.QualificationVariableHasRate> findAll() {
        
        System.out.print("Finding All");
        
        return super.findAll();
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    @javax.ws.rs.Path("findByFranchise/{idFranchise}")
    public java.util.List<Entities.QualificationVariableHasRate>findByFranchise(@javax.ws.rs.PathParam("idFranchise") Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("QualificationVariableHasRate.findByFranchiseId");
    
        query.setParameter("idFranchise",idFranchise);
        
        return query.getResultList();
        
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.QualificationVariableHasRate> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
}
