/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Rate;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Path("entities.rate")
@Stateless
public class RateFacadeREST extends AbstractFacade<Rate>{

  @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    @Override
    protected EntityManager getEntityManager() {
        
        return em;
     }
    
    public RateFacadeREST(){
    
        super(Rate.class);
    
    }
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Rate entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Rate entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
    
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Rate find(@PathParam("id") Integer id) {
          return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Rate> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Rate> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @GET
    @Path("findByStars/{stars}")
    @Produces({"application/json","application/xml"})
    public Entities.Rate findByStars(@PathParam("stars") Short stars){
    
        System.out.print(stars);
        
        try{
        
            Query query=em.createNamedQuery("Rate.findByStars");
            
            query.setParameter("stars", stars);
            
            return (Entities.Rate) query.getSingleResult();
        
        }
        catch(NoResultException ex){
        
        return null;
        
        }
        
    
    }
    
}
