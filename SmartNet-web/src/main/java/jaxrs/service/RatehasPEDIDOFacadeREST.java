/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.RatehasPEDIDO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Path("entities.ratehaspedido")
@Stateless
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class RatehasPEDIDOFacadeREST extends AbstractFacade<RatehasPEDIDO>{

    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @javax.annotation.Resource
    javax.ejb.EJBContext context;
    
    @Override
    protected EntityManager getEntityManager() {
    return em;
          
          }  
    
    public RatehasPEDIDOFacadeREST(){
    
        super(RatehasPEDIDO.class);
    
    }
    
  private Entities.RatehasPEDIDOPK getKey(javax.ws.rs.core.PathSegment ps){
  
      try{
      
          Entities.RatehasPEDIDOPK key=new Entities.RatehasPEDIDOPK();
          
          javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
          
          java.util.List<String>pEDIDOidPEDIDO=map.get("pEDIDOidPEDIDO");
          
          if(pEDIDOidPEDIDO!=null && !pEDIDOidPEDIDO.isEmpty()){
          
              key.setPEDIDOidPEDIDO(new java.lang.Integer(pEDIDOidPEDIDO.get(0)));
          
          }
          
          java.util.List<String>rateIdrate=map.get("rateIdrate");
          
          if(rateIdrate!=null && !rateIdrate.isEmpty()){
          
             key.setRateIdrate(new java.lang.Integer(rateIdrate.get(0)));
          
          }
          
          return key;
      
      }
      catch(NullPointerException ex){
      
          ex.printStackTrace(System.out);
      
          return null;
          
      }
  
  }
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(RatehasPEDIDO entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.create(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") javax.ws.rs.core.PathSegment id, RatehasPEDIDO entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") javax.ws.rs.core.PathSegment id) {
   
        
            javax.transaction.UserTransaction ut=context.getUserTransaction();
            
            try {
            
                ut.begin();
                
                Entities.RatehasPEDIDO ratehasPEDIDO=super.find(this.getKey(id));
                
                ut.commit();
                
                ut.begin();
                
                super.remove(ratehasPEDIDO);
                
                ut.commit();
          
            }
            catch(Exception ex){
            
             try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(RatehasPEDIDOFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
            
            }
          
        
     
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public RatehasPEDIDO find(@PathParam("id") javax.ws.rs.core.PathSegment id) {
           return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<RatehasPEDIDO> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<RatehasPEDIDO> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    @GET
    @Path("findByPedido/{idpedido}")
    @Produces({"application/json","application/xml"})
    public Entities.RatehasPEDIDO findBySucursal(@PathParam("idpedido") Integer idPedido){
    
    try{
        
        Query query=em.createNamedQuery("RatehasPEDIDO.findByPEDIDOidPEDIDO");
        
        query.setParameter("pEDIDOidPEDIDO",idPedido);
        
        return (Entities.RatehasPEDIDO)query.getSingleResult();
    
    }
    catch(NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @Path("findByIdPedido/{idpedido}")
    @GET
    @Produces({"application/json","application/xml"})
    public Entities.RatehasPEDIDO findByIdPedido(@PathParam("idpedido")Integer idpedido){
    
    try{
    
        Query query=em.createNamedQuery("RatehasPEDIDO.findByPEDIDOidPEDIDO");
        
        query.setParameter("pEDIDOidPEDIDO",idpedido);
        
        return(Entities.RatehasPEDIDO)query.getSingleResult();
    
    }
    catch(NoResultException ex){
    
    return null;
    
    }
    
    
    }

}
