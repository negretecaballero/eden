/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Sale;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.sale")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class SaleFacadeREST extends AbstractFacade<Sale>{

     @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
     
    @Override
    protected EntityManager getEntityManager() {
   return em;
    
    }
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    public SaleFacadeREST(){
    
        super(Sale.class);
    
    }
    
    public Entities.SalePK getKey(PathSegment key){
    
    try{
   
        Entities.SalePK salePK=new Entities.SalePK();
        
        MultivaluedMap<String,String>map=key.getMatrixParameters();
        
        java.util.List<String>idsale=map.get("idsale");
        
        if(idsale!=null && !idsale.isEmpty())
        {
        
            salePK.setIdsale(new java.lang.Integer(idsale.get(0)));
        
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty())
        {
        
            salePK.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        return salePK;
    }
    catch(Exception ex){
    
        Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE,null,ex);
        
       throw new javax.faces.FacesException(ex);
    
    }
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.Sale entity) {
        
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
            userTransaction.begin();
            
        super.create(entity);
       // em.persist(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.Sale entity) {
       
        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
            userTransaction.begin();
            
        super.edit(entity);
        
        userTransaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {

        javax.transaction.UserTransaction userTransaction=context.getUserTransaction();
        
        try{
        
            userTransaction.begin();
            
        Entities.Sale sale=super.find(this.getKey(id));
        
        userTransaction.commit();
        
        userTransaction.begin();
        
        super.remove(sale);
        
        userTransaction.commit();
        
        }
        
        catch(Exception ex){
        
            try {
                userTransaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.Sale find(@PathParam("id") PathSegment id) {
      
        Entities.SalePK salePK=this.getKey(id);
        
        return super.find(salePK);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.Sale> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.Sale> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByFranchise/{idFranchise}")
    public java.util.List<Entities.Sale>findByFranchise(@PathParam("idFranchise")Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Sale.findByFranquiciaIdfranquicia");
        
        query.setParameter("franquiciaIdfranquicia", idFranchise);
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
}
