/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.SaleHasMenu;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.salehasmenu")

@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class SaleHasMenuFacadeREST extends AbstractFacade<SaleHasMenu>{
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public SaleHasMenuFacadeREST(){
    
        super(SaleHasMenu.class);
    
    }

    @Override
    protected EntityManager getEntityManager() {
    
        return em;
    
    }
    
    private Entities.SaleHasMenuPK getPrimaryKey(PathSegment ps){
    
        Entities.SaleHasMenuPK saleHasMenuPK=new Entities.SaleHasMenuPK();
        
        MultivaluedMap <String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>saleIdsale=map.get("saleIdsale");
        
        if(saleIdsale!=null && !saleIdsale.isEmpty()){
        
            saleHasMenuPK.setSaleIdsale(new java.lang.Integer(saleIdsale.get(0)));
        
        }
        
        java.util.List<String>menuIdmenu=map.get("menuIdmenu");
        
        if(menuIdmenu!=null && !menuIdmenu.isEmpty()){
        
            saleHasMenuPK.setMenuIdmenu(new java.lang.Integer(menuIdmenu.get(0)));
        
        }
        
        java.util.List<String>saleFranquiciaIdfranquicia=map.get("saleFranquiciaIdfranquicia");
        
        if(saleFranquiciaIdfranquicia!=null && !saleFranquiciaIdfranquicia.isEmpty()){
    
        saleHasMenuPK.setSaleFranquiciaIdfranquicia(new java.lang.Integer(saleFranquiciaIdfranquicia.get(0)));
    
        }
        
        
        
        return saleHasMenuPK;
    
    }
    
    private final Entities.SalePK getSalePK(javax.ws.rs.core.PathSegment ps){
    
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        Entities.SalePK key=new Entities.SalePK();
    
        java.util.List<String>idsale=map.get("idsale");
        
        if(idsale!=null && !idsale.isEmpty()){
        
            key.setIdsale(new java.lang.Integer(idsale.get(0)));
        
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
       
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
            key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        
        }
        
        return key;
        
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.SaleHasMenu entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.create(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.SaleHasMenu entity) {
       
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
        
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
            transaction.begin();
            
        Entities.SaleHasMenu saleHasMenu=super.find(this.getPrimaryKey(id));
            
        transaction.commit();

        transaction.begin();
        
        super.remove(saleHasMenu);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasMenuFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.SaleHasMenu find(@PathParam("id") PathSegment id) {
        Entities.SaleHasMenuPK key = getPrimaryKey(id);
        return super.find(key);
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findBySale/{salePK}")
    public java.util.List<Entities.SaleHasMenu>findBySale(@javax.ws.rs.PathParam("salePK")javax.ws.rs.core.PathSegment ps){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("SaleHasMenu.findBySale");
            
            query.setParameter("salePK", this.getSalePK(ps));
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.SaleHasMenu> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.SaleHasMenu> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    
}
