/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Path("entities.salehasproducto")
@Stateless
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class SaleHasProductoFacadeREST extends AbstractFacade<Entities.SaleHasProducto> {

      @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

      @javax.annotation.Resource
      private javax.ejb.EJBContext context;
    
    @Override
    protected EntityManager getEntityManager() {
  return em;
    }
    
    public SaleHasProductoFacadeREST(){
    
        super(Entities.SaleHasProducto.class);
    
    }
    
    private Entities.SaleHasProductoPK getPrimaryKey(PathSegment ps){
    
        MultivaluedMap<String,String> map=ps.getMatrixParameters();
        
        Entities.SaleHasProductoPK saleHasProductoPK=new Entities.SaleHasProductoPK();
        
        java.util.List<String>saleIdsale=map.get("saleIdsale");
        
        if(saleIdsale!=null && !saleIdsale.isEmpty()){
        
        saleHasProductoPK.setSaleIdsale(new java.lang.Integer(saleIdsale.get(0)));
        
        }
        
        java.util.List<String>saleFranquiciaIdfranquicia=map.get("saleFranquiciaIdfranquicia");
        
        if(saleFranquiciaIdfranquicia!=null && !saleFranquiciaIdfranquicia.isEmpty()){
        
            saleHasProductoPK.setSaleFranquiciaIdfranquicia(new java.lang.Integer(saleFranquiciaIdfranquicia.get(0)));
        
        }
        
     java.util.List<String>productoIdproducto=map.get("productoIdproducto");
     
     if(productoIdproducto!=null && !productoIdproducto.isEmpty())
     {
     
         saleHasProductoPK.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
     
     }
     
     java.util.List<String>productoCategoriaIdCategoria=map.get("productoCategoriaIdCategoria");
     
     if(productoCategoriaIdCategoria!=null && !productoCategoriaIdCategoria.isEmpty()){
     
     saleHasProductoPK.setProductoCategoriaIdCategoria(new java.lang.Integer(productoCategoriaIdCategoria.get(0)));
     
     }
     
     
     java.util.List<String>productoCategoriaFranquiciaIdFranquicia=map.get("productoCategoriaFranquiciaIdFranquicia");
     
     if(productoCategoriaFranquiciaIdFranquicia!=null && !productoCategoriaFranquiciaIdFranquicia.isEmpty()){
     
     saleHasProductoPK.setProductoCategoriaFranquiciaIdFranquicia(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia.get(0)));
     
     }
     
        return saleHasProductoPK;
    
    }
  
    private final Entities.SalePK getSalePK(javax.ws.rs.core.PathSegment ps){
    
        Entities.SalePK salePK=new Entities.SalePK();
    
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idsale=map.get("idsale");
        
        if(idsale!=null && !idsale.isEmpty()){
        
            salePK.setIdsale(new java.lang.Integer(idsale.get(0)));
            
        }
        
        java.util.List<String>franquiciaIdfranquicia=map.get("franquiciaIdfranquicia");
        
        if(franquiciaIdfranquicia!=null && !franquiciaIdfranquicia.isEmpty()){
        
            salePK.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
            
        }
        
        return salePK;
        
    }
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.SaleHasProducto entity) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
        transaction.begin();
            
        super.create(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.SaleHasProducto entity) {
       
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
        transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
        transaction.begin();
        
        Entities.SaleHasProducto saleHasProducto=super.find(this.getPrimaryKey(id));
        
        transaction.commit();
        
        transaction.begin();
        
        super.remove(saleHasProducto);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SaleHasProductoFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.SaleHasProducto find(@PathParam("id") PathSegment id) {
        Entities.SaleHasProductoPK key = getPrimaryKey(id);
        return super.find(key);
    }
    
    
    @javax.ws.rs.GET
    @javax.ws.rs.Path("findByFranchise/{idFranchise}")
    @javax.ws.rs.Produces({"application/xml","application/json"})
    public java.util.List<Entities.SaleHasProducto>findByFranchise(@javax.ws.rs.PathParam("idFranchise")Integer idFranchise){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("SaleHasMenu.findBySaleFranquiciaIdfranquicia");
        
        query.setParameter("saleFranquiciaIdfranquicia", idFranchise);
        
        return query.getResultList();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/xml","application/json"})
    @javax.ws.rs.Path("findBySale/{salePK}")
    public java.util.List<Entities.SaleHasProducto>findBySale(@javax.ws.rs.PathParam("salePK")javax.ws.rs.core.PathSegment salePK){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("SaleHasProducto.findBySale");
            
            query.setParameter("salePK",this.getSalePK(salePK));
        
            return query.getResultList();
            
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.SaleHasProducto> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.SaleHasProducto> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    
    
}
