/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.SimilarInventario;
import Entities.SimilarInventarioPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.similarinventario")

public class SimilarInventarioFacadeREST extends AbstractFacade<SimilarInventario>{

      @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    
  public SimilarInventarioFacadeREST(){
        super(SimilarInventario.class);
    }
    
    
    private SimilarInventarioPK getKey(PathSegment ps){
    
        MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        SimilarInventarioPK key=new SimilarInventarioPK();
    
        java.util.List<String>inventarioIdinventario=map.get("inventarioIdinventario");
        
        if(inventarioIdinventario!=null && !inventarioIdinventario.isEmpty()){
        
            key.setInventarioIdinventario(new java.lang.Integer(inventarioIdinventario.get(0)));
        
        }
        
        java.util.List<String>inventarioFranquiciaIdFranquicia=map.get("inventarioFranquiciaIdFranquicia");
        
        if(inventarioFranquiciaIdFranquicia!=null && !inventarioFranquiciaIdFranquicia.isEmpty()){
        
            key.setInventarioFranquiciaIdFranquicia(new java.lang.Integer(inventarioFranquiciaIdFranquicia.get(0)));
        
        }
        
        java.util.List<String>inventarioIdinventario1=map.get("inventarioIdinventario1");
        
        if(inventarioIdinventario1!=null && !inventarioIdinventario1.isEmpty()){
        
        key.setInventarioIdinventario1(new java.lang.Integer(inventarioIdinventario1.get(0)));
        
        }
        
        java.util.List<String>inventarioFranquiciaIdFranquicia1=map.get("inventarioFranquiciaIdFranquicia1");
        
        if(inventarioFranquiciaIdFranquicia1!=null && !inventarioFranquiciaIdFranquicia1.isEmpty()){
        
            key.setInventarioFranquiciaIdFranquicia1(new java.lang.Integer(inventarioFranquiciaIdFranquicia1.get(0)));
        
        }
        
        System.out.print("RETURNED SIMILAR KEY "+key);
        
        return key;
    }

    @Override
    protected EntityManager getEntityManager() {
      return em;    }
    
    
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(SimilarInventario entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, SimilarInventario entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        super.remove(super.find(getKey(id)));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public SimilarInventario find(@PathParam("id") PathSegment id) {
        return super.find(getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<SimilarInventario> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<SimilarInventario> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    
    
}
