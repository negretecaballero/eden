/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.SimilarProducts;
import Entities.SimilarProductsPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */

@Stateless
@Path("entities.similarproducts")
public class SimilarProductsFacadeREST extends AbstractFacade<SimilarProducts>{
    
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
      
    private SimilarProductsPK getKey(PathSegment ps){
    
    SimilarProductsPK similarProductsPK=new SimilarProductsPK();
        
    MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    java.util.List<String>productoIdproducto=map.get("productoIdproducto");
    
    if(productoIdproducto!=null && !productoIdproducto.isEmpty()){
    
        similarProductsPK.setProductoIdproducto(new java.lang.Integer(productoIdproducto.get(0)));
    
    }
    
    java.util.List<String>productoCategoriaIdCategoria=map.get("productoCategoriaIdCategoria");
    
    if(productoCategoriaIdCategoria!=null && !productoCategoriaIdCategoria.isEmpty()){
    
    similarProductsPK.setProductoCategoriaIdCategoria(new java.lang.Integer(productoCategoriaIdCategoria.get(0)));
    
    }
    
    java.util.List<String>productoCategoriaFranquiciaIdFranquicia=map.get("productoCategoriaFranquiciaIdFranquicia");
    
    if(productoCategoriaFranquiciaIdFranquicia!=null && !productoCategoriaFranquiciaIdFranquicia.isEmpty()){
    
        similarProductsPK.setProductoCategoriaFranquciaIdFranquicia(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia.get(0)));
    
    }
    
       
    java.util.List<String>productoIdproducto1=map.get("productoIdproducto1");
    
    if(productoIdproducto1!=null && !productoIdproducto1.isEmpty()){
        
        
        similarProductsPK.setProductoIdproducto1(new java.lang.Integer(productoIdproducto1.get(0)));
    
    
    }
    
    java.util.List<String>productoCategoriaIdCategoria1=map.get("productoCategoriaIdCategoria1");
    
    if(productoCategoriaIdCategoria1!=null && !productoCategoriaIdCategoria1.isEmpty())
    {
    
        similarProductsPK.setProductoCategoriaIdCategoria1(new java.lang.Integer(productoCategoriaIdCategoria1.get(0)));
    
    }
    
    java.util.List<String>productoCategoriaFranquiciaIdFranquicia1=map.get("productoCategoriaFranquiciaIdFranquicia1");
    
    if(productoCategoriaFranquiciaIdFranquicia1!=null && !productoCategoriaFranquiciaIdFranquicia1.isEmpty()){
    
    similarProductsPK.setProductoCategoriaFranquiciaIdFranquicia1(new java.lang.Integer(productoCategoriaFranquiciaIdFranquicia1.get(0)));
    
    }
    
    return similarProductsPK;
    }
      
     
    private Entities.ProductoPK getProductPK(javax.ws.rs.core.PathSegment ps){
    
    Entities.ProductoPK key=new Entities.ProductoPK();
    
    javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    java.util.List<String>idproducto=map.get("idproducto");
    
    if(idproducto!=null && !idproducto.isEmpty()){
    
    key.setIdproducto(new java.lang.Integer(idproducto.get(0)));
    
    }
    
    java.util.List<String>categoriaIdcategoria=map.get("categoriaIdcategoria");
    
    if(categoriaIdcategoria!=null && !categoriaIdcategoria.isEmpty()){
    
    key.setCategoriaIdcategoria(new java.lang.Integer(categoriaIdcategoria.get(0)));
    
    }
    
    
    java.util.List<String>categoriaFranquiciaIdfranquicia=map.get("categoriaFranquiciaIdfranquicia");
    
    if(categoriaFranquiciaIdfranquicia!=null && !categoriaFranquiciaIdfranquicia.isEmpty()){
    
        key.setCategoriaFranquiciaIdfranquicia(new java.lang.Integer(categoriaFranquiciaIdfranquicia.get(0)));
    
    
    }
    
    
    return key;
    }
    

    public SimilarProductsFacadeREST(){
        super(SimilarProducts.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(SimilarProducts entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, SimilarProducts entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        super.remove(super.find(getKey(id)));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public SimilarProducts find(@PathParam("id") PathSegment id) {
        return super.find(getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<SimilarProducts> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<SimilarProducts> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    
    @javax.ws.rs.Path("findByFirstProduct/{key}")
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.GET
    public SessionClasses.EdenList<Entities.SimilarProducts>findByFirstProduct(@javax.ws.rs.PathParam("key")javax.ws.rs.core.PathSegment key){
    
    try{
    
        SessionClasses.EdenList<Entities.SimilarProducts>results=new SessionClasses.EdenList<Entities.SimilarProducts>();
        
        javax.persistence.Query query=em.createNamedQuery("SimilarProducts.findByFirstProduct");
    
        query.setParameter("productPK",this.getProductPK(key));
        
        for(Entities.SimilarProducts aux:(java.util.List<Entities.SimilarProducts>)query.getResultList()){
        
        results.addItem(aux);
        
        }
        
        
       return results;
        
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    
    }
}
