/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.State;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.state")
public class StateFacadeREST extends AbstractFacade<State> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public StateFacadeREST() {
        super(State.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(State entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, State entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public State find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<State> findAll() {
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("State.findAll");
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
        
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<State> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Produces({"application/json","application/xml"})
    @Path("findByname/{name}")
    public Entities.State findByName(@PathParam("name")String name){
    
    try{
    
        Query query=em.createNamedQuery("State.findByName");
        
        query.setParameter("name", name);
        
        return (Entities.State)query.getSingleResult();
        
    }
    catch(NoResultException ex){
        
    return null;
    
    }
    
    }
    
}
