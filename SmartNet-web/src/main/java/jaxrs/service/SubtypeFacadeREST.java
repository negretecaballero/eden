/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Stateless

@javax.ws.rs.Path("entities.subtype")

public class SubtypeFacadeREST extends AbstractFacade<Entities.Subtype>{
  
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    public SubtypeFacadeREST(){
    
        super(Entities.Subtype.class);
    
    }
    
    
    private Entities.SubtypePK getKey(javax.ws.rs.core.PathSegment ps){
    
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        java.util.List<String>idSubtype=map.get("idSubtype");
        
        
        java.util.List<String>tipoIdtipo=map.get("tipoIdtipo");
        
        Entities.SubtypePK subtypePK=new Entities.SubtypePK();
        
        if(idSubtype!=null && !idSubtype.isEmpty()){
        
        
            subtypePK.setIdSubtype(new java.lang.Integer(idSubtype.get(0)));
        
        }
        
        if(tipoIdtipo!=null && !tipoIdtipo.isEmpty()){
        
            subtypePK.setTipoIdtipo(new java.lang.Integer(tipoIdtipo.get(0)));
        
        }
        
        
        return subtypePK;
    
    }

    @Override
    protected EntityManager getEntityManager() {
   return this.em;
    
    }
    
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.Subtype entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.Subtype entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        Entities.SubtypePK key = getKey(id);
        super.remove(super.find(key));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.Subtype find(@PathParam("id") PathSegment id) {
        Entities.SubtypePK key = getKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.Subtype> findAll() {
        
        try{
        
            javax.persistence.Query query=em.createNamedQuery("Subtype.findAll");
            
            return query.getResultList();
        
        }
        catch(javax.persistence.NoResultException ex){
        
            ex.printStackTrace(System.out);
            
            return null;
        
        }
        
    }
    
    
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.Path("findByTypeSubtype/{subtypeName}/{typeName}")
    public Entities.Subtype findSubtypeByNames(@javax.ws.rs.PathParam("subtypeName") String subtypeName,@javax.ws.rs.PathParam("typeName")String typeName){
    
    try{
    
        javax.persistence.Query query=em.createNamedQuery("Subtype.findByTypeSubtypeName");
        
        query.setParameter("subtypeName", subtypeName);
        
        query.setParameter("typeName", typeName);
        
        return (Entities.Subtype)query.getSingleResult();
    
    }
    catch(javax.persistence.NoResultException ex){
    
        return null;
    
    }
    
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.Subtype> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
}
