/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import Entities.Sucursal;
import Entities.SucursalPK;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */
@Stateless
@Path("entities.sucursal")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class SucursalFacadeREST extends AbstractFacade<Sucursal> {
    @PersistenceContext(unitName = "com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    
    private SucursalPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;idsucursal=idsucursalValue;franquiciaIdfranquicia=franquiciaIdfranquiciaValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        Entities.SucursalPK key = new Entities.SucursalPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> idsucursal = map.get("idsucursal");
        if (idsucursal != null && !idsucursal.isEmpty()) {
            key.setIdsucursal(new java.lang.Integer(idsucursal.get(0)));
        }
        java.util.List<String> franquiciaIdfranquicia = map.get("franquiciaIdfranquicia");
        if (franquiciaIdfranquicia != null && !franquiciaIdfranquicia.isEmpty()) {
            key.setFranquiciaIdfranquicia(new java.lang.Integer(franquiciaIdfranquicia.get(0)));
        }
        return key;
    }

    public SucursalFacadeREST() {
        super(Sucursal.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    @RolesAllowed("GuanabaraFrAdmin")
    public void create(Sucursal entity) {
        
     final  javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
         ut.begin();
            
        super.create(entity);
        
        ut.commit();
        
        }
        catch(NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex){
        
         try {
             ut.rollback();
         } catch (IllegalStateException ex1) {
             Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
         } catch (SecurityException ex1) {
             Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
         } catch (SystemException ex1) {
             Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
         }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Sucursal entity) {
        
        final javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
       final javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        Entities.Sucursal sucursal=super.find(getPrimaryKey(id));
        
        ut.commit();
        
        ut.begin();
        
        super.remove(sucursal);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
           try {
               ut.rollback();
           } catch (IllegalStateException ex1) {
               Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
           } catch (SecurityException ex1) {
               Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
           } catch (SystemException ex1) {
               Logger.getLogger(SucursalFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
           }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Sucursal find(@PathParam("id") PathSegment id) {
        Entities.SucursalPK key = getPrimaryKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Sucursal> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Sucursal> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
