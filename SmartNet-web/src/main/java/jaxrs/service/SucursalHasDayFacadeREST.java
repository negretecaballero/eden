/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateless
@javax.ws.rs.Path("entities.sucursalhasday")
@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)
public class SucursalHasDayFacadeREST extends AbstractFacade<Entities.SucursalHasDay>{

    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;
    
    @javax.annotation.Resource
    private javax.ejb.EJBContext context;
    
    @Override
    protected EntityManager getEntityManager() {
    
        return em;
    
    }
    
    public SucursalHasDayFacadeREST(){
    
    super(Entities.SucursalHasDay.class);
    
    }
    
    private Entities.SucursalHasDayPK getKey(javax.ws.rs.core.PathSegment ps){
    
    javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    Entities.SucursalHasDayPK key=new Entities.SucursalHasDayPK();
    
    java.util.List<String>sucursalIdSucursal=map.get("sucursalIdSucursal");
    
    if(sucursalIdSucursal!=null && !sucursalIdSucursal.isEmpty()){
    
        key.setSucursalIdSucursal(new java.lang.Integer(sucursalIdSucursal.get(0)));
    
    }
    
    java.util.List<String>sucursalFranquiciaIdfranquicia=map.get("sucursalFranquiciaIdfranquicia");
    
    if(sucursalFranquiciaIdfranquicia!=null && !sucursalFranquiciaIdfranquicia.isEmpty()){
    
    key.setSucursalFranquiciaIdfranquicia(new java.lang.Integer(sucursalFranquiciaIdfranquicia.get(0)));
    
    }
    
    java.util.List<String>dayIdday=map.get("dayIdday");
    
    if(dayIdday!=null && !dayIdday.isEmpty()){
    
    key.setDayIdday(new java.lang.Integer(dayIdday.get(0)));
    
    }
    
    return key;
    }
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    
    public void create(Entities.SucursalHasDay entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
            
            ut.begin();
        
        super.create(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") PathSegment id, Entities.SucursalHasDay entity) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        super.edit(entity);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") PathSegment id) {
        
        javax.transaction.UserTransaction ut=context.getUserTransaction();
        
        try{
        
            ut.begin();
            
        Entities.SucursalHasDay sucursalHasDay = super.find(getKey(id));
        
        ut.commit();
        
        ut.begin();
        
        super.remove(sucursalHasDay);
        
        ut.commit();
        
        }
        catch(Exception ex){
        
            try {
                ut.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SucursalHasDayFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.SucursalHasDay find(@PathParam("id") PathSegment id) {
        Entities.SucursalHasDayPK key = getKey(id);
        return super.find(key);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.SucursalHasDay> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.SucursalHasDay> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
}
