/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ws.rs.Path("entities.supplierapplication")

@javax.ejb.Stateless

@javax.ejb.TransactionManagement(javax.ejb.TransactionManagementType.BEAN)

public class SupplierApplicationFacadeREST extends jaxrs.service.AbstractFacade<Entities.SupplierApplication> {
    
    @javax.annotation.Resource
    private  javax.ejb.EJBContext context;
    
    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;

    
    
    @Override
    protected EntityManager getEntityManager() {
  
    return em;
    
    }
    
    public SupplierApplicationFacadeREST(){
    
        super(Entities.SupplierApplication.class);
    
    }
    
    
    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.SupplierApplication entity) {
   
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
            
            transaction.begin();
        
        super.create(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
 
        
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") String id, Entities.SupplierApplication entity) {
     
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
        
        transaction.begin();
            
        super.edit(entity);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
   
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") String id) {
        
        javax.transaction.UserTransaction transaction=context.getUserTransaction();
        
        try{
            
        transaction.begin();
        
        Entities.SupplierApplication supplierApplication=find(id);
        
        transaction.commit();
        
        transaction.begin();
        
        super.remove(supplierApplication);
        
        transaction.commit();
        
        }
        catch(Exception ex){
        
            try {
                transaction.rollback();
            } catch (IllegalStateException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SecurityException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            } catch (SystemException ex1) {
                Logger.getLogger(SupplierApplicationFacadeREST.class.getName()).log(Level.SEVERE, null, ex1);
            }
        
        }
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.SupplierApplication find(@PathParam("id") String id) {
      
        System.out.print("SUPPLIER APPLICATION TO FIND "+id);
        
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.SupplierApplication> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.SupplierApplication> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    @javax.ws.rs.Path("findByUsernameState/{username}/{applicationState}")
    @javax.ws.rs.GET
    @javax.ws.rs.Produces({"application/json","application/xml"})
    public SessionClasses.EdenList<Entities.SupplierApplication>findByUsernameApplicationState(@javax.ws.rs.PathParam("username")String mail, @javax.ws.rs.PathParam("applicationState")Entitites.Enum.SupplierApplicationState supplierApplicationState){
    
        try{
        
            javax.persistence.Query query=em.createNamedQuery("SupplierApplication.findByStateUsername");
            
            query.setParameter("mail", mail);
            
            query.setParameter("applicationState", supplierApplicationState);
            
            SessionClasses.EdenList<Entities.SupplierApplication>results;
            
            results=new SessionClasses.EdenList<Entities.SupplierApplication>(query.getResultList());
        
            return results;
            
        }
        catch(javax.persistence.NoResultException ex){
        
            return null;
        
        }
    
    }
    
  @javax.ws.rs.Path("findByState/{applicationState}")
  @javax.ws.rs.Produces({"application/json","application/xml"})
  @javax.ws.rs.GET
  public java.util.List<Entities.SupplierApplication>findByState(@javax.ws.rs.PathParam("applicationState")Entitites.Enum.SupplierApplicationState applicationState){
  
      try{
      
          javax.persistence.Query query=em.createNamedQuery("SupplierApplication.findByState");
      
          query.setParameter("applicationState",applicationState);
          
          return query.getResultList();
          
      }
      catch(javax.persistence.NoResultException ex){
      
          return null;
      
      }
  
  }
    
}
