/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */
@javax.ejb.Stateless

@javax.ws.rs.Path("entities.supplier")



public class SupplierFacadeREST extends jaxrs.service.AbstractFacade<Entities.Supplier>{

    

    
    private Entities.SupplierPK getKey(javax.ws.rs.core.PathSegment ps){
    
    Entities.SupplierPK key=new Entities.SupplierPK();
    
    javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
    
    java.util.List<String>idSupplier=map.get("idSupplier");
    
    if(idSupplier!=null && !idSupplier.isEmpty()){
    
        key.setIdSupplier(new java.lang.Integer(idSupplier.get(0)));
    
    }
    
    java.util.List<String>loginAdministradorUsername=map.get("loginAdministradorUsername");
    
    if(loginAdministradorUsername!=null && !loginAdministradorUsername.isEmpty()){
    
        key.setLoginAdministradorUsername(loginAdministradorUsername.get(0));
    
    }
    
    
    return key;
    }
    
    
     @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Entities.Supplier entity) {
   
        
        super.create(entity);
 
        
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") javax.ws.rs.core.PathSegment id, Entities.Supplier entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        super.remove(super.find(this.getKey(id)));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Entities.Supplier find(@PathParam("id") javax.ws.rs.core.PathSegment id) {
        return super.find(this.getKey(id));
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Entities.Supplier> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Entities.Supplier> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        
        return em;
    
    }
    
    public SupplierFacadeREST(){
    
        super(Entities.Supplier.class);
    
    }
    
    
    
}
