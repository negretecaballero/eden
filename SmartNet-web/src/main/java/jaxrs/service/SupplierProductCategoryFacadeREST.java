/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jaxrs.service;

import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author luisnegrete
 */

@javax.ejb.Stateless

@javax.ws.rs.Path("entities.supplierproductcategory")

public class SupplierProductCategoryFacadeREST extends jaxrs.service.AbstractFacade<Entities.SupplierProductCategory> {
    
    @javax.persistence.PersistenceContext(unitName="com.mycompany_SmartNet-web_war_1.0-SNAPSHOTPU")
    private javax.persistence.EntityManager em;
    
    
    
    
    private Entities.SupplierProductCategoryPK getKey(javax.ws.rs.core.PathSegment ps){
    
        Entities.SupplierProductCategoryPK key=new Entities.SupplierProductCategoryPK();
    
        javax.ws.rs.core.MultivaluedMap<String,String>map=ps.getMatrixParameters();
        
        SessionClasses.EdenList<String>idSupplierProductCategory=new SessionClasses.EdenList(map.get("idSupplierProductCategory"));
        
        if(idSupplierProductCategory!=null && idSupplierProductCategory.size()>0){
        
            key.setIdSupplierProductCategory(new java.lang.Integer(idSupplierProductCategory.get(0)));
        
        }
        
        SessionClasses.EdenList<String>supplierIdsupplier=new SessionClasses.EdenList<String>(map.get("supplierIdsupplier"));
        
        if(supplierIdsupplier!=null && supplierIdsupplier.size()>0){
        
            key.setSupplierIdsupplier(new java.lang.Integer(supplierIdsupplier.get(0)));
        
        }
        
        SessionClasses.EdenList<String>supplierLoginAdministradorUsername=new SessionClasses.EdenList<String>(map.get("supplierLoginAdministradorUsername"));
        
        if(supplierLoginAdministradorUsername!=null && supplierLoginAdministradorUsername.size()>0){
        
            key.setSupplierLoginAdministradorUsername(supplierLoginAdministradorUsername.get(0));
        
        }
        
        return key;
    }

    @Override
    protected EntityManager getEntityManager() {
  
    return em;
    
    }
    
    public SupplierProductCategoryFacadeREST(){
    
        super(Entities.SupplierProductCategory.class);
    
    }
    
    @Override
    @javax.ws.rs.POST
    @javax.ws.rs.Consumes({"application/json","application/xml"})
    public void create(Entities.SupplierProductCategory entity){
    
        super.create(entity);
    
    }
    
    

    @javax.ws.rs.Consumes({"application/json","application/xml"})
    @javax.ws.rs.PUT
    @javax.ws.rs.Path("{id}")
    public void edit(Entities.SupplierProductCategory entity,@javax.ws.rs.PathParam("id") javax.ws.rs.core.PathSegment id){
    
        super.edit(entity);
    
    }
    
    @javax.ws.rs.Path("{id}")
    @javax.ws.rs.Produces({"application/json","application/xml"})
    @javax.ws.rs.GET
    public Entities.SupplierProductCategory find(@javax.ws.rs.PathParam("id")javax.ws.rs.core.PathSegment ps){
    
      return  super.find(this.getKey(ps));
        
    }
    
    
    @javax.ws.rs.DELETE
    @javax.ws.rs.Path("{id}")
    public void remove(@javax.ws.rs.PathParam("id")javax.ws.rs.core.PathSegment ps){
    
        super.remove(super.find(ps));
    
    }
    
    @javax.ws.rs.Produces({"",""})
    @Override
    @javax.ws.rs.GET
    public java.util.List<Entities.SupplierProductCategory>findAll(){
    
        return super.findAll();
    
    }
    
    
    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public java.util.List<Entities.SupplierProductCategory> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }
    
}
