/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.ConfirmationDelegate;
import CDIBeans.DesplegableCitiesInterface;
import CDIBeans.DesplegableFranchiseInterface;
import CDIBeans.DesplegableProfessionInterface;
import CDIBeans.DesplegableStateInterface;
import CDIBeans.FileUploadInterface;
import CDIBeans.StateControllerInterface;

import Clases.ActionType;
import Clases.BaseBacking;
import Clases.DataGeneration;
import Clases.EdenDate;
import Clases.EdenDateInterface;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import SessionClasses.MailManagement;
import Entities.AppGroup;
import Entities.AppGroupPK;
import Entities.City;
import Entities.CityPK;
import Entities.Confirmation;

import Entities.DireccionPK;
import Entities.Franquicia;
import Entities.GpsCoordinates;
import Entities.Imagen;
import Entities.LoginAdministrador;
import Entities.State;
import Entities.Sucursal;
import Entities.SucursalPK;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.PathSegment;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import jaxrs.service.AppGroupFacadeREST;
import jaxrs.service.CityFacadeREST;
import jaxrs.service.DireccionFacadeREST;
import jaxrs.service.GpsCoordinatesFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.ProfessionFacadeREST;
import jaxrs.service.StateFacadeREST;
import jaxrs.service.SucursalFacadeREST;

import jaxrs.service.TypeAddressFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.event.FileUploadEvent;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author luisnegrete
 */
//@RolesAllowed({"GuanabaraFrAdmin","GuanabaraSucAdmin"})
public class CRUDSUC extends BaseBacking{

    /**
     * Creates a new instance of CRUDSUC
     */
    
    @Inject private FileUploadInterface fileUpload;
    
    @Inject private DesplegableFranchiseInterface desplegableInterface;
    
    @Inject private DesplegableStateInterface deplegableState;
    
    @Inject private DesplegableCitiesInterface desplegableCities;
    
//    @Inject private SucursalhasloginAdministradorControllerDelegate sucursalhasloginAdministradorController;
    
    @Inject private CDIEden.CRUDSUCViewController _crudSucViewController;
    
    
    @Inject private ConfirmationDelegate confirmationController;
    
    @EJB
    private StateFacadeREST stateFacadeREST;
    
    @EJB
    private CityFacadeREST cityFacadeREST;
    
    @EJB
    private SucursalFacadeREST sucursalFacadeREST;
    
    @EJB
    private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
    
    @EJB
    private DireccionFacadeREST direccionFacadeREST;
    
    @EJB
    private TypeAddressFacadeREST typeAddressFacadeREST;
    
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
    
    @EJB
    private AppGroupFacadeREST appGroupFacadeREST;
    
    
/*    @EJB 
    private SucursalhasloginAdministradorFacadeREST sucursalhasloginAdministradorFacadeREST;*/
    
    @EJB
    private ProfessionFacadeREST professionFacadeREST;
    
    @Inject
    private StateControllerInterface stateController;
    
    private int _minimalAmount;
    
    private org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>dayModel;
    
    private java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays;
    
   private double alcance;
   
   private Clases.Direccion direccion;
   
   private String jsDireccion;
   
   private String state_id;
   
   private String state;
   
   private String city;
   
   private CityPK cityPK;
   
   private String user;
   
   private int idProfession;
   
   private EdenDateInterface openingTime;
   
   private EdenDateInterface closingTime;
   
   private boolean sucursalAdministrador;
   
   private Date opening;
   
   private Date closing;
   
   private int _deliveryPrice;
   
   public int getDeliveryPrice(){
   
   return _deliveryPrice;
   
   }
   
   public void setDeliveryPrice(int deliveryPrice){
   
       _deliveryPrice=deliveryPrice;
   
   }
   
   public int getMinimalAmount(){
   
   return _minimalAmount;
   
   }
   
   public void setMinimalAmount(int minimalAmount){
   
   _minimalAmount=minimalAmount;
   
   }
   
   public org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>getDayModel(){
   
   if(this.dayModel==null){
   
   this.dayModel=new FrontEndModel.DayLazyDataModel(_crudSucViewController.getSucursalHasdayList());
   
   }    
       
   return this.dayModel;
   
   }
   
   public java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout> getSelectedDays(){
   
   return this.selectedDays;
   
   }
   
   public void setSelectedDays(java.util.List<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>selectedDays){
   
   this.selectedDays=selectedDays;
   
   }
   
   public void setDayModel(org.primefaces.model.LazyDataModel<FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout>dayModel){
   
   this.dayModel=dayModel;
   
   }
   
   public boolean getSucursalAdministrador(){
   
   return this.sucursalAdministrador;
   
   }
   
   public void setSucursalAdministrador(boolean sucursalAdministrador){
   
       this.sucursalAdministrador=sucursalAdministrador;
   
   }
   
   
   public int getIdProfession(){
   
   return this.idProfession;
   
   }
   
   public void setIdProfession(int idProfession){
   
   this.idProfession=idProfession;
   
   }
   
   @Inject private DesplegableProfessionInterface desplegableprofession;
   
   public DesplegableProfessionInterface getDesplegableProfession(){
   
       return this.desplegableprofession;
   
   }
   
   
   public String getUser(){
   
    
       
       return this.user;
   
   }
   
   public void setUser(String user){
   
       this.user=user;
   
   }
   
   
   public CityPK getCityPK(){
   
   return this.cityPK;
   
   }
   
   public void setCityPK(CityPK cityPK){
   
       this.cityPK=cityPK;
   
   }
   
   public String getState(){
   
       return this.state;
   
   }
   
   public void setState(String state){
   
       this.state=state;
   
   }
   
   public String getCity(){
   
   return this.city;
   
   }
   
   
   
   public void setCity(String city){
   
  this.city=city;
   
   }
   
   public String getState_id(){
   
   return this.state_id;
   
   }
   
   public void setState_id(String state_id){
   
       this.state_id=state_id;
   
   }
   
   @ManagedProperty("tipoDireccion")
   private TipoDireccion addressType;

    public TipoDireccion getAddressType() {
        return addressType;
    }

    public void setAddressType(TipoDireccion addressType) {
        this.addressType = addressType;
    }
   
   
   
   public void setJsDireccion (String jsDireccion){
   
   this.jsDireccion=jsDireccion;
   
   }
   
   public String getJsDireccion(){
   
   return this.jsDireccion;
   
   }
   
   private String longitude;
   
   public void setLongitude(String longitude){
   
   this.longitude=longitude;
   
   }
   
   public String getLongitude(){
   
   
       return this.longitude;
   
   }
   
   private String latitude;
   
   public void setLatitude(String latitude){
   
   this.latitude=latitude;
   
   }
   
   public String getLatitude(){
   
   return this.latitude;
   
   }
   
   
   public void setDireccion(Clases.Direccion direccion){
   
       this.direccion=direccion;
   
   }
   
   public Clases.Direccion getDireccion(){
   
   return this.direccion;
   
   }
   
   public void setAlcance(double alcance){
       
   this.alcance=alcance;
   
   }
   
   public double getAlcance(){
   
       return this.alcance;
   
   }
   
     private List<State>states;
    
    public void setStates(List<State>states){
    
        this.states=states;
    
    }
    
    public List<State>getStates(){
    
        return this.states;
    
    }
    
    private List<City>cities;
    
    public List<City>getCities(){
    
        return this.cities;
    
    }
    
    public void setCities(List<City>cities){
    
        this.cities=cities;
    
    }
    
   public void removePicture(AjaxBehaviorEvent event){
   
   try{
   
       if(event.getComponent().getClientId().equals("form:tabView:yesButton")){
       
       HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
       
       String image=request.getParameter("hiddenComponent");
       
       System.out.print(image);
       
       FilesManagementInterface filesManagement=new FilesManagement();
       
       System.out.print(getSession().getAttribute("username"));
       
       filesManagement.deleteFile("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/adminFranquicia/"+image);
       
      for(int i=0;i<this.fileUpload.getImages().size();i++){
      
      if(this.fileUpload.getImages().get(i).equals(image)){
      
      this.fileUpload.getImages().remove(i);
      
      this.fileUpload.getPhotoList().remove(i);
      
      break;
      
      }
      
      }
       
       }
   
   }
   catch(Exception ex){
   
   
   
   }
   
   }
   
   public void postValidate(javax.faces.event.ComponentSystemEvent event){
   
       try{
           
           if(this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"sucursal.xhtml"))
           
           {
           
               if(this.selectedDays!=null && !this.selectedDays.isEmpty())
           {
           for(FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout aux:this.selectedDays){
           
           double netOpenTime=aux.getOpenTime().getHour()+Double.valueOf((double)(aux.getOpenTime().getMinutes()/60.0));
           
           System.out.print("Open Time "+aux.getOpenTime().getHour()+":"+aux.getOpenTime().getMinutes());
           
           System.out.print("Net Open Time "+netOpenTime);
           
           double netCloseTime=aux.getCloseTime().getHour()+Double.valueOf((double)(aux.getCloseTime().getMinutes()/60.0));
           
           System.out.print("Close Time "+aux.getCloseTime().getHour()+":"+aux.getCloseTime().getMinutes());
           
           System.out.print("Net Close Time "+netCloseTime);
           
           if(netCloseTime<netOpenTime){
           
               javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Error with "+aux.getDay().getDayName()+" Close Time cannot be earlier than Open Time");
           
               msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_ERROR);
               
               this.getContext().addMessage(null,msg);
               
               this.getContext().renderResponse();
               
           }
           
           }
           
           }
       }
       }
       catch(ArrayIndexOutOfBoundsException | StackOverflowError ex){
       
           java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
           
           throw new javax.faces.FacesException(ex);
       
       }
       
   
   }
   
   public void handleFileUpload(FileUploadEvent event){
   
       try{
       
           if(event.getComponent().getClientId().equals("form:tabView:uploader")){
           
               String path="";
               
           if(getSession().getAttribute("selectedOption")instanceof Franquicia){
           
               path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/Smartnet-web-1.0-SNAPSHOT/adminFranquicia/imagesCache/"+getSession().getAttribute("username");
           
           }
           
           else{
           
           throw new RuntimeException();
           
           }
           
           fileUpload.servletContainer(event.getFile(), getSession().getAttribute("username").toString(),500,500);
           
           }
           else
           {
           
           throw new RuntimeException();
           
           }
       
       }
       catch(RuntimeException ex){
       
         FacesMessage msg=new FacesMessage("You are Hacking Bitch");
         
         msg.setSeverity(FacesMessage.SEVERITY_ERROR);
         
         getContext().addMessage(event.getComponent().getClientId(),msg);
       
       }
       
   }
    
    public CRUDSUC() {
        
        this.city="";
        
    }
    
    public EdenDateInterface getOpeningTime(){
    
        return this.openingTime;
    
    }
    
    public EdenDateInterface getClosingTime(){
    
        return this.closingTime;
    
    }
    
    public void setOpeningTime(EdenDateInterface openingTime){
    
        this.openingTime=openingTime;
    
    }
    
    public void setClosingTime(EdenDateInterface closingTime){
    
        this.closingTime=closingTime;
    
    }
    
    
    @PostConstruct
    public void init(){
    
        try{
            
            this.openingTime=new EdenDate();
            
            this.closingTime=new EdenDate();
            
            
            
            this.sucursalAdministrador=true;
            
//   this.user=new LoginAdministrador();
   
  // this.user.setUsername("");
    
   if(this.direccion==null){
   
   this.direccion=new Clases.Direccion();
   
   this.direccion.setPrimerDigito("");
   
   this.direccion.setSegundoDigito("");
   
   this.direccion.setTercerDigito(0);
   
   this.direccion.setTipo("");
   
   this.direccion.setAdicional_descripcion("");
   
   }
   
      if(getSession().getAttribute("selectedOption")instanceof Franquicia){
        
          if(!this.fileUpload.getClassType().equals(CRUDSUC.class.getName())){
          
         fileUpload.setClassType(CRUDSUC.class.getName());
         
         javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
         
         this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
         
          System.out.print("Cleaning CRUDSUC folder");
          }
       
        
        }
        
        
        else{
        
        throw new RuntimeException();
        
        }
        }
        catch(RuntimeException ex){
        
            Logger.getLogger(CRUDSUC.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    
    
    }
    
    public void setType(String type) {
    
    
        try{
        
if(this.getContext().getViewRoot().getViewId().equals(java.io.File.separator+"adminFranquicia"+java.io.File.separator+"sucursal.xhtml")){
            
String kohaNimi = "Tallinn";
URL myUrl = new URL("http://maps.googleapis.com/maps/api/geocode/xml?address=Colombia,Bogotá,Bogotá,Diagonal+16+#+118+-+16&sensor=false");
DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
DocumentBuilder builder = factory.newDocumentBuilder();
Document doc = builder.parse(myUrl.openStream());
XPathFactory xPathfactory = XPathFactory.newInstance();
XPath xpath = xPathfactory.newXPath();
javax.xml.xpath.XPathExpression expr = xpath.compile("/GeocodeResponse/result/geometry/location/lat");
String swLat = expr.evaluate(doc, XPathConstants.STRING).toString();
expr=xpath.compile("/GeocodeResponse/result/geometry/location/lng");
String swLng=expr.evaluate(doc,XPathConstants.STRING).toString();
System.out.println("swLat: " + swLat+","+swLng );

expr=xpath.compile("/GeocodeResponse/status");

String status=expr.evaluate(doc,XPathConstants.STRING).toString();

System.out.print("Status "+status);

NodeList nodeList = (NodeList) xpath.compile("/GeocodeResponse/result/address_component").evaluate(doc, XPathConstants.NODESET);

System.out.print("Node List Size "+nodeList.getLength());



String nodeName,nodeValue;

Node node;

for(int i=0;i<nodeList.getLength();i++){
node = nodeList.item(i);
System.out.print(node);
//node = nodeList.item(i).getFirstChild();
//nodeName = node.getNodeName();
//nodeValue = node.getChildNodes().item( 0 ).getNodeValue();


}

//https://maps.googleapis.com/maps/api/geocode/xml?latlng=40.714224,-73.961452

            
            }
            
      if(type.equals("AddSucursal")){
          
          System.out.print("ACTION TYPE ADDSUCURSAL");
      
      if(!this.fileUpload.getActionType().getActionType().equals(type)){
      
      this.fileUpload.setActionType(new ActionType(type));
      
      
      }
      
      
      this.states=this.deplegableState.getResults();
      this.cities=this.desplegableCities.getResultsCities();
      
      
      /*
      if(city.equals("") || state.equals("")){
      
          if(state.equals("")){
      this.state=states.get(0).getName();
          
          }
      if(city.equals("")){
      this.city=cities.get(0).getName();
      }
      }
      */
      }
        
        }
        
        catch(RuntimeException  | javax.xml.xpath.XPathException | ParserConfigurationException  | SAXException | IOException ex){
        
        
        }
        
    
    }
    
   
    
    public void addSucursal(javax.faces.event.ActionEvent event){
    
    try{
           System.out.print("Boolean Checkbow "+this.sucursalAdministrador);
     
        if(!this.sucursalAdministrador){
        
//        this.user=this.loginAdministradorFacadeREST.find(getSession().getAttribute("username"));
        
        }
    
        
        System.out.print("Component Id "+event.getComponent().getClientId());
        
        if(getSession().getAttribute("selectedOption")instanceof Franquicia){
        
        for(Imagen image:this.fileUpload.getPhotoList()){
        
        imagenFacadeREST.create(image);
            
        }
        
        java.util.Map<String,String>map=this.getContext().getExternalContext().getRequestParameterMap();
        
        for(java.util.Map.Entry<String,String>entry:map.entrySet()){
        
            System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
       
        this.direccion.setPrimerDigito(map.get("form:tabView:primerDigito"));
        
        this.direccion.setSegundoDigito(map.get("form:tabView:segundoDigito"));
        
        this.direccion.setTercerDigito(java.lang.Integer.parseInt(map.get("form:tabView:tercerDigito")));
        
        this.direccion.setAdicional_descripcion(map.get("form:tabView:adicional"));
            
        System.out.print("Address "+this.direccion.getTipo()+"-"+this.direccion.getPrimerDigito()+"#"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito()+"  "+this.direccion.getAdicional_descripcion());
        
        System.out.print(event.getComponent().getClientId());
        
        GpsCoordinates gpsCoordinates=new GpsCoordinates();
        
        HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
        
        String latitudeString=request.getParameter("latitude");
        
        String longitudeString=request.getParameter("longitude");
        
        System.out.print(latitudeString);
        
        System.out.print(longitudeString);
               
        gpsCoordinates.setLatitude(Double.valueOf(latitudeString));
        
        gpsCoordinates.setLongitude(Double.valueOf(longitudeString));
        
        gpsCoordinatesFacadeREST.create(gpsCoordinates);
        
        Entities.Direccion address=new Entities.Direccion();
        
        
        
        address.setGpsCoordinatesidgpsCoordinated(gpsCoordinates);
        
        PathSegment ps=new PathSegmentImpl("bar;idCITY="+this.cityPK.getIdCITY()+";sTATEidSTATE="+this.cityPK.getSTATEidSTATE()+"");
        
        address.setCity(cityFacadeREST.find(ps));
        
        DireccionPK direccionPK=new DireccionPK();
        
        direccionPK.setCITYSTATEidSTATE(stateController.findByName(this.state_id).getIdSTATE());
        
        direccionPK.setCITYidCITY(this.cityPK.getIdCITY());
        
        
        
        address.setDireccionPK(direccionPK);
        
        address.setPrimerDigito(this.direccion.getPrimerDigito());
        
        address.setSegundoDigito(this.direccion.getSegundoDigito());
        
        address.setTercerDigito((short)this.direccion.getTercerDigito());
        
        address.setAdicional(this.direccion.getAdicional_descripcion());
        
        address.setTYPEADDRESSidTYPEADDRESS(typeAddressFacadeREST.findByType(this.direccion.getTipo()));
        
        direccionFacadeREST.create(address);
        
        Sucursal sucursal=new Sucursal();
        
        sucursal.setMinimalDelivery(_minimalAmount);
        
        SucursalPK sucursalPK=new SucursalPK();
        
        sucursalPK.setFranquiciaIdfranquicia(((Franquicia)getSession().getAttribute("selectedOption")).getIdfranquicia());
        
        sucursal.setSucursalPK(sucursalPK);
        
        sucursal.setAlcance(this.alcance);
        
        sucursal.setDireccion(address);
        
        sucursal.setFranquicia((Franquicia)getSession().getAttribute("selectedOption"));
        
        sucursal.setImagenCollection(this.fileUpload.getPhotoList());
        
        sucursal.setDeliveryPrice(_deliveryPrice);
        
        //sucursal.setLoginAdministradorusername(loginAdministradorFacadeREST.find(getSession().getAttribute("username").toString()));
        
        Calendar c=Calendar.getInstance();
    
    
        c.set(Calendar.HOUR_OF_DAY,this.openingTime.getHour());
    
    c.set(Calendar.MINUTE, openingTime.getMinutes());
    
    c.set(Calendar.SECOND, 0);
    
    System.out.print("Opening time "+c.getTime());
    
    this.opening=c.getTime();
    
   
    
    Calendar c1=Calendar.getInstance();
    
    c1.set(Calendar.HOUR_OF_DAY, this.closingTime.getHour());
    
    c1.set(Calendar.MINUTE,this.closingTime.getMinutes());
    
    c1.set(Calendar.SECOND,0);
    
    System.out.print("Closint Time "+c1.getTime());
        
    this.closing=c1.getTime();
    
   
    
        sucursalFacadeREST.create(sucursal);
        
      
            
          /*  if(loginAdministradorFacadeREST.find(this.user.getUsername())==null )
            
            {
        this.user.setProfessionIdprofession(professionFacadeREST.find(1));
        
        this.user.setPassword(DataGeneration.passwordGeneration(30));
        
        
        loginAdministradorFacadeREST.create(user);
        
        Confirmation confirmation=new Confirmation();
        
      
        
       confirmation.setLoginAdministradorusername(this.user.getUsername());
        
        confirmation.setLoginAdministrador(user);
        
        confirmation.setConfirmed(false);
        
        confirmationController.create(confirmation);
        
        MailManagement mailManagement=new MailManagement();
        
        mailManagement.sendMail(this.user.getGender(), "Welcome to Eden", "Please follow this link to confirm your account https://localhost:8181/SmartNet-web/confirmation.xhtml?faces-redirect=true&id="+this.user.getUsername()+"");
            }*/
           
        
            
        HttpSession session=(HttpSession)getContext().getExternalContext().getSession(true);
        
        /*SucursalhasloginAdministradorPK sucursalhasloginAdministradorPK=new SucursalhasloginAdministradorPK();
        
        sucursalhasloginAdministradorPK.setAPPGROUPgroupid("guanabara_franquicia_administrador");
        
        sucursalhasloginAdministradorPK.setAPPGROUPloginAdministradorusername(session.getAttribute("username").toString());
        
        sucursalhasloginAdministradorPK.setLoginAdministradorusername(session.getAttribute("username").toString());
        
        sucursalhasloginAdministradorPK.setSucursalFranquiciaIdfranquicia(sucursal.getFranquicia().getIdfranquicia());
        
        sucursalhasloginAdministradorPK.setSucursalIdsucursal(sucursal.getSucursalPK().getIdsucursal());
        
        SucursalhasloginAdministrador sucursalhasloginAdministrador=new SucursalhasloginAdministrador();
        
        sucursalhasloginAdministrador.setSucursalhasloginAdministradorPK(sucursalhasloginAdministradorPK);
        
        sucursalhasloginAdministrador.setLoginAdministrador(loginAdministradorFacadeREST.find(session.getAttribute("username")));
        
        PathSegment pss=new PathSegmentImpl("bar;groupid=guanabara_franquicia_administrador;loginAdministradorusername="+(loginAdministradorFacadeREST.find(session.getAttribute("username")).getUsername())+"");
        
        sucursalhasloginAdministrador.setAppGroup(appGroupFacadeREST.find(pss));
        
        sucursalhasloginAdministrador.setSucursal(sucursal);
        
        if(this.sucursalAdministrador){
        
        sucursalhasloginAdministradorFacadeREST.create(sucursalhasloginAdministrador);
        }
        
        SucursalhasloginAdministradorPK sucursalhasloginAdministradorPK1=new SucursalhasloginAdministradorPK();
        
        sucursalhasloginAdministradorPK1.setAPPGROUPgroupid("guanabara_sucursal_administrador");
        
        sucursalhasloginAdministradorPK1.setLoginAdministradorusername(this.user.getUsername());
        
       sucursalhasloginAdministradorPK1.setAPPGROUPloginAdministradorusername(this.user.getUsername());
        
        sucursalhasloginAdministradorPK1.setSucursalFranquiciaIdfranquicia(sucursal.getFranquicia().getIdfranquicia());
        
        sucursalhasloginAdministradorPK1.setSucursalIdsucursal(sucursal.getSucursalPK().getIdsucursal());
        
        SucursalhasloginAdministrador sucursalhasloginAdministrador1=new SucursalhasloginAdministrador();
        
        sucursalhasloginAdministrador1.setSucursalhasloginAdministradorPK(sucursalhasloginAdministradorPK1);
        
        sucursalhasloginAdministrador1.setLoginAdministrador(user);*/
        
       /* if(appGroupFacadeREST.findByUsername(new PathSegmentImpl("bar;groupid=guanabara_sucursal_administrador;loginAdministradorusername="+user.getUsername()+""))==null){
        
        AppGroupPK appGroupPK=new AppGroupPK();
        
        appGroupPK.setGroupid("guanabara_sucursal_administrador");
        
       // appGroupPK.setLoginAdministradorusername(this.user.getUsername());
        
        AppGroup appGroup=new AppGroup();
        
        appGroup.setAppGroupPK(appGroupPK);
        
        appGroup.setLoginAdministrador(this.user);
        
        
      
        appGroupFacadeREST.create(appGroup);
        }*/
        
        //PathSegment psss=new PathSegmentImpl("bar;groupid=guanabara_sucursal_administrador;loginAdministradorusername="+this.user.getUsername()+"");
         
//        System.out.print("Group Id: "+appGroupFacadeREST.find(psss).getAppGroupPK().getGroupid()+" Username: "+appGroupFacadeREST.find(psss).getAppGroupPK().getLoginAdministradorusername());
        
      //  sucursalhasloginAdministrador1.setAppGroup(appGroupFacadeREST.find(psss));
        
       // sucursalhasloginAdministrador1.setSucursal(sucursal);
        
       // sucursalhasloginAdministradorFacadeREST.create(sucursalhasloginAdministrador1);
        
        //Add admin franchise to admin Sucursal
        /*
        SucursalhasloginAdministradorPK auxi=new SucursalhasloginAdministradorPK();
        
        auxi.setAPPGROUPgroupid("guanabara_sucursal_administrador");
        
        auxi.setAPPGROUPloginAdministradorusername(getSession().getAttribute("username").toString());
        
        auxi.setLoginAdministradorusername(getSession().getAttribute("username").toString());
        
        auxi.setSucursalFranquiciaIdfranquicia(sucursal.getSucursalPK().getFranquiciaIdfranquicia());
        
        auxi.setSucursalIdsucursal(sucursal.getSucursalPK().getIdsucursal());
        
        sucursalhasloginAdministradorController.create(auxi);
        */
            
        this._crudSucViewController.createSucursalOpenList(sucursal, selectedDays);
        
        FacesMessage msg=new FacesMessage(FacesMessage.FACES_MESSAGES,"Sucursal Added Succesfully");
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
        
        }
        
        else{
        
        throw new RuntimeException();
        
        }
    
    }
    
    catch(RuntimeException ex){
    
        FacesMessage msg=new FacesMessage("Exception Caugth");
        
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        
        
        getContext().addMessage(event.getComponent().getClientId(),msg);
    
        Logger.getLogger(CRUDSUC.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void changeState(AjaxBehaviorEvent event){
        
        
       Map<String,String>requestMap=getContext().getExternalContext().getRequestParameterMap();
       
       for(Map.Entry<String,String>entry:requestMap.entrySet()){
       
       System.out.print(entry.getKey()+"-"+entry.getValue());
       
       }
    
        try{
    String component="Component Id "+event.getComponent().getClientId();
    
    System.out.print(component);
    
    this.state_id=requestMap.get("form:tabView:state_input");
    
     System.out.print("You are in");
        
     System.out.print("Selected State Id "+this.state_id);
     
     this.desplegableCities.updateResultsCities(stateController.findByName(this.state_id));
     
     this.state=stateFacadeREST.findByName(this.state_id).getName(); 
     
     this.cities=this.desplegableCities.getResultsCities();
     
     if(this.city.equals("")){
     
      this.city=requestMap.get("city");
     
     }
     else{
     this.city=this.cities.get(0).getName();
     }
    
     
     for(City citie:stateController.findByName(this.state_id).getCityCollection()){
     
     if(citie.getName().equals(this.city)){
     
         this.cityPK=citie.getCityPK();
         
         break;
     
     }
     
     }
     
     System.out.print("New city value "+this.city);
     
     System.out.print("New State name: "+this.state);
    
    
    
   
        }
        catch(RuntimeException ex){
        
            Logger.getLogger(CRUDSUC.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    
    }
    
    public void changeCity(AjaxBehaviorEvent event){
    
        try{
        
         
            
                System.out.print("CityPK "+this.cityPK);
                
                PathSegment ps=new PathSegmentImpl("bar;idCITY="+this.cityPK.getIdCITY()+";sTATEidSTATE="+this.cityPK.getSTATEidSTATE()+"");
           
                
              
                this.state=stateFacadeREST.find(this.state_id).getName();
                
                this.city=cityFacadeREST.find(ps).getName();
            
                
                System.out.print("New State value "+this.state);
                              
                System.out.print("New City value "+this.city);
                
            
            
           
        
        }
        catch(RuntimeException ex){
        
             
            Logger.getLogger(CRUDSUC.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    
    public void getStateSelection(AjaxBehaviorEvent event){
    
    UIComponent component=event.getComponent();
    
    if(component instanceof SelectBooleanCheckbox){
    
        SelectBooleanCheckbox box=(SelectBooleanCheckbox)component;
        
        this.sucursalAdministrador=box.isSelected();
      
        
    }
    
    }
    
    public void daySelectEvent(org.primefaces.event.SelectEvent event){
    
      System.out.print("You are in day table select event");
        
      System.out.print(this.selectedDays.size()+" Elements in Day List");
      
    }
    
}
