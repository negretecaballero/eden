/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import Entities.Franquicia;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.http.HttpServletRequest;
import jaxrs.service.FranquiciaFacadeREST;

/**
 *
 * @author luisnegrete
 */
public class Categories extends BaseBacking{

    /**
     * Creates a new instance of Categories
     */
    public Categories() {
    }
    
    private String test;
    
    
    public String getTest(){
    
    return this.test;
    
    }
    
    public void setTest(String test){
    
    this.test=test;
    
    }
    
    @EJB
    private FranquiciaFacadeREST franquiciaFacadeREST;
    
    private int id;
    
    private List<Franquicia> franchises;
    
    public void setFranchises(List <Franquicia>franchises){
    
    this.franchises=franchises;
    
    }
    
    public List<Franquicia>getFranchises(){
    
    return this.franchises;
    
    }
    
    
    @PostConstruct
    public void init(){
        
        try{
    
        franchises=new <Franquicia>ArrayList();
        
        
     System.out.print("Test value "+test);
        
      HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
      
     id=Integer.parseInt(request.getParameter("id"));
       
     franchises=franquiciaFacadeREST.findByType(id);
     
     System.out.print(franchises.size());
     
        }
        catch(EJBException ex){
        
            Logger.getLogger(Categories.class.getName()).log(Level.SEVERE,null,ex);
            
        }
    
    }
    
}
