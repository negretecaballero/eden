/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.flow.FlowScoped;
import javax.imageio.stream.FileImageOutputStream;
import java.io.*;

/**
 *
 * @author luisnegrete
 */
@Named("completeRegistration")
@FlowScoped("complete-registration")

public class CompleteRegistration implements java.io.Serializable{

    
   private boolean checkAddress;
   
   
 
   private  Entities.LoginAdministrador user;
           
   private Entities.Imagen image;
    
    @javax.inject.Inject
    @CDIBeans.LoginAdministradorControllerQualifier
    transient private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
   
    @javax.inject.Inject
    @CDIEden.CompleteRegistrationControllerQualifier
    transient private CDIEden.CompleteRegistrationControllerInterface completeRegistrationController;
    
    @javax.inject.Inject
    @CDIBeans.StateControllerQualifier
    transient private CDIBeans.StateControllerInterface stateController;
    
    @javax.inject.Inject 
    @CDIBeans.CityControllerQualifier
    transient private CDIBeans.CityControllerInterface cityController;
  
    @javax.inject.Inject
    @CDIBeans.PhoneControllerQualifier
    transient private CDIBeans.PhoneControllerInterface phoneController;
    /**
     * 
     * Creates a new instance of CompleteRegistration
     */
    public CompleteRegistration() {
    }
    
    @javax.inject.Inject
    @CDIBeans.FileUploadBeanQualifier
    transient private CDIBeans.FileUploadInterface fileUpload;
    
    
    @javax.annotation.PostConstruct
    public void init(){
             
        
        
        javax.faces.context.FacesContext fc=FacesContext.getCurrentInstance();
            
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)fc.getExternalContext().getSession(true);
        
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        if(this.loginAdministradorController.find(session.getAttribute("username").toString())!=null){
        
        java.io.File profileFolder=new java.io.File(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username")+File.separator,"profile");
           
        this.user=this.loginAdministradorController.find(session.getAttribute("username").toString());
            
        System.out.print("Phones found "+this.user.getPhoneCollection().size());
        
        this.check=false;
        
        java.io.File file=new java.io.File(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username"));
      
        System.out.print("Path to check "+file.getPath());
      
        if(file.exists()){
      
        if(file.isDirectory()){
      
        File[]files=file.listFiles();
        
        System.out.print("Files in path "+files.length);
        
        boolean found=false;
        
          for (File file1 : files) {
              if (file1.isFile()) {
                  found=true;
                  this.profileImage = servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator + file1.getName();
                  break;
              }
          }
        
        
        if(!found){
        
        if(this.loginAdministradorController.getImageRoot(session.getAttribute("username").toString())!=null){
        
            this.profileImage=this.loginAdministradorController.getImageRoot(session.getAttribute("username").toString());
        
        }
            
       
        }
      }
      
      }
      
      System.out.print("Profile Image "+this.profileImage);
      
        
        }
        
        else{
        
        Logger.getLogger(CompleteRegistration.class.getName()).log(Level.SEVERE,null,new Exception());
        
        }
           
      }
    
   
   @javax.validation.constraints.Size(min=0,max=100)
   private String name;
   
   @javax.validation.constraints.Size(min=0,max=45)
   private String zipCode;
   
   private Date birthday;
   
   private String gender;
   
   private int idProfession;
   
   private String profileImage;
   
   private String stateName;
   
   private String cityName;
   
   private String typeAddress;
   
   private String firstDigit;
   
   private boolean check;
   
   public Entities.LoginAdministrador getUser(){
   
   return this.user;
   
   }
   
   public boolean getCheckAddress(){
   
   return this.checkAddress;
   
   }
   
   public void setCheckAddress(boolean checkAddress){
   
 this.checkAddress=checkAddress;  
   
   }
   
   public void setUser(Entities.LoginAdministrador user){
   
   this.user=user;
   
   }
   
   
   public String getProfileImage(){
   
       return this.profileImage;
       
   }
   
   public void setProfileImage(String profileImage){
   
       this.profileImage=profileImage;
   
   }
   
   public boolean getCheck(){
   
   return this.check;
   
   }
 
   public void setCheck(boolean check){
   
   this.check=check;
   
   }
   public String getFirstDigit(){
   
       return this.firstDigit;
       
   }
   
   public void setFirstDigit(String firstDigit){
   
       this.firstDigit=firstDigit;
   
   }
   
   
   private String secondDigit;
   
   public String getSecondDigit(){
   
       return this.secondDigit;
   
   }
   
   public void setSecondDigit(String secondDigit){
   
   this.secondDigit=secondDigit;
   
   }
   
   private short thirdDigit;
   
   public short getThirdDigit(){
   
   return this.thirdDigit;
   
   }
   
   public void setThirdDigit(short thirdDigit){
   
   this.thirdDigit=thirdDigit;
   
   }
   
   private String description;
   
   public String getDescription(){
   
   return this.description;
   
   }
   
   public void setDescription(String description){
   
   this.description=description;
   
   }
   
   
   @javax.inject.Inject
   @CDIBeans.DesplegableProfessionQualifier
   transient private CDIBeans.DesplegableProfessionInterface professionDesplegables;
   
   @javax.inject.Inject
   @CDIBeans.DesplegableStateQualifier
   transient private CDIBeans.DesplegableStateInterface desplegablesState;
   
   
   @javax.inject.Inject
   @CDIBeans.DesplegableCitiesQualifier
   transient private CDIBeans.DesplegableCitiesInterface desplegablesCities;
   
   @javax.inject.Inject
   @CDIBeans.AddressTypeControllerQualifier
   transient private CDIBeans.AddressTypeController addressType;
   
   
   public java.util.List<Entities.Profession>getProfession(){
   
   return this.professionDesplegables.getProfessions();
   
   }
   
   
   public String getGender(){
   
       return this.gender;
   
   }
   
   public void setGender(String gender){
   
       this.gender=gender;
   
   }
   
   public int getIdProfession(){
   
   return this.idProfession;
   
   }
   
   public void setIdProfession(int idProfession){
   
   this.idProfession=idProfession;
   
   }
   
   public String getName(){
   
   return this.name;
   
   }
   
   public void setName(String name){
   
       this.name=name;
   
   }
  
  private String phoneNumber;
  
  public String getPhoneNumber(){
  
      return this.phoneNumber;
  
  }
  
  public void setPhoneNumber(String phoneNumber){
  
  this.phoneNumber=phoneNumber;
  
  }
  
  public String getZipCode(){
  
  return this.zipCode;
  
  }
  
  public void setZipCode(String zipCode){
  
      this.zipCode=zipCode;
  
  }
  
  
  public Date getBirthday(){
  
      return this.birthday;
  
  }
  
  public void setBirthday(Date birthday){
  
      this.birthday=birthday;
  
  }
  
  
  public java.util.List<Entities.State>getStates(){
      
      
   java.util.List<Entities.State>stateList=new java.util.ArrayList<Entities.State>();
   
   for(int i=this.desplegablesState.getResults().size()-1;i>=0;i--){
   
       stateList.add(this.desplegablesState.getResults().get(i));
   
   }
  
   this.stateName=stateList.get(0).getName();
   
  return stateList;
  
  }
  
  public java.util.List<Entities.City>getCities(){
  
  this.desplegablesCities.updateResultsCities(this.stateController.findByName(stateName));
      
  System.out.print("First City "+this.desplegablesCities.getResultsCities().get(0).getName());
  
  return this.desplegablesCities.getResultsCities();
  
  }
  
  public void stateChanged(javax.faces.event.AjaxBehaviorEvent event){
  
      System.out.print(event.getComponent().getClientId());
      
      System.out.print(this.stateName);
      
      this.desplegablesCities.updateResultsCities(this.stateController.findByName(stateName));
      
      
      
  }
  
  public String getStateName(){
  
  return this.stateName;
  
  }
  
  public void setStateName(String stateName){
  
      this.stateName=stateName;
  
  }
  
  public String getCityName(){
  
      return this.cityName;
  
  }
  
  
  public void setCityName(String cityName){
  
  this.cityName=cityName;
  
  }
  
  public java.util.List<Entities.TypeAddress>getTypeAddressResult(){
  
      return this.addressType.findAll();
  
  }
  
  public String getTypeAddress(){
  
      return this.typeAddress;
  
  }
  
  public void setTypeAddress(String typeAddress){
  
  
      this.typeAddress=typeAddress;
  
  }
  
  public void onCapture(org.primefaces.event.CaptureEvent event){
  
      
      
     String filename=getRandomImageName(); 
     
     byte[] data = event.getData();
        
     
     javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
  
     javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
     
     String newFileName=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+File.separator+"profile"+File.separator+filename+".jpeg";
   
     System.out.print(newFileName);
     
     Clases.FilesManagementInterface filesManagement=new Clases.FilesManagement();
     
     filesManagement.cleanFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username")+File.separator+"profile");
             this.profileImage=File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username").toString()+File.separator+"profile"+File.separator+filename+".jpeg";
    
    System.out.print("Profile Image "+this.profileImage);
             
       FileImageOutputStream imageOutput;
        try {
            
            imageOutput = new FileImageOutputStream(new File(newFileName));
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
            
        }
        catch(IOException e) {
            throw new FacesException("Error in writing captured image.", e);
        }
        
        image=new Entities.Imagen();
        
        image.setImagen(data);
        
        image.setExtension(".jpeg");
        
        
  }
  
  public void handleUpload(org.primefaces.event.FileUploadEvent event){
  
      javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
  
      javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
      this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+session.getAttribute("username")+File.separator+"profile");
      
      this.fileUpload.servletContainer(event.getFile(), session.getAttribute("username").toString()+File.separator+"profile", 500, 500);

      this.profileImage=this.fileUpload.getImages().get(0);
      
      image=this.fileUpload.getPhotoList().get(0);
  }
  
   private String getRandomImageName() {
        int i = (int) (Math.random() * 10000000);
         
        return String.valueOf(i);
    }
   
   
   
   
   
   public void saveChanges(javax.faces.event.AjaxBehaviorEvent event){
   
   try
   {
   
      System.out.print("Saving changes");
       
       
       java.util.Map<String,String>requestMap=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
      
       for(Map.Entry<String,String> entry:requestMap.entrySet()){
       
           System.out.print(entry.getKey()+"-"+entry.getValue());
           
       }
       
      Entities.GpsCoordinates gpsCoordinates=new Entities.GpsCoordinates();
       
      gpsCoordinates.setLatitude(Double.parseDouble(requestMap.get("form:latitude")));
       
      gpsCoordinates.setLongitude(Double.parseDouble(requestMap.get("form:longitude")));
       
      Entities.Direccion address= null;
      
      System.out.print("Checkbox "+this.checkAddress);
       
      if(this.checkAddress)
      {
      address=new Entities.Direccion();
      
      Entities.DireccionPK direccionPK=new Entities.DireccionPK();
       
      Entities.City city=this.cityController.findByState(requestMap.get("form:tabView:city_input"), this.stateController.findByName(requestMap.get("form:tabView:state_input")));
       
      direccionPK.setCITYSTATEidSTATE(city.getCityPK().getSTATEidSTATE());
      
      direccionPK.setCITYidCITY(city.getCityPK().getIdCITY());
      
      address.setDireccionPK(direccionPK);
       
      address.setAdicional(requestMap.get("form:tabView:description"));
       
      address.setTYPEADDRESSidTYPEADDRESS(this.addressType.findByname(requestMap.get("form:tabView:type_input")));
       
      System.out.print("City: "+requestMap.get("form:tabView:city_input")+" State:"+requestMap.get("form:tabView:state_input"));
      
      address.setCity(this.cityController.findByState(requestMap.get("form:tabView:city_input"), this.stateController.findByName(requestMap.get("form:tabView:state_input"))));
      
      System.out.print("Ready to apply changes");
      
      address.setGpsCoordinatesidgpsCoordinated(gpsCoordinates);
       
      address.setPrimerDigito(requestMap.get("form:tabView:firstDigit"));
      
      address.setSegundoDigito(requestMap.get("form:tabView:secondDigit"));
      
      address.setTercerDigito(Short.parseShort(requestMap.get("form:tabView:thirdDigit")));
         
      System.out.print(address.getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+address.getPrimerDigito()+" #"+address.getSegundoDigito()+"-"+address.getTercerDigito()+" "+address.getAdicional());
   }
      this.completeRegistrationController.completeAccount(user, address, image);
      
      if(this.image!=null){
      
      System.out.print("Image is not null");
      
      }
      
      System.out.print("Changes Saved");
      
      FacesContext.getCurrentInstance().getExternalContext().redirect("https://localhost:8181/SmartNet-web/index.xhtml");
     
   }
   catch(NumberFormatException | IOException ex){
   
    Logger.getLogger(CompleteRegistration.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
   }
   
   public void deleteAddress(javax.faces.event.AjaxBehaviorEvent event , Entities.Direccion address){
   
   try{
   
       System.out.print("Address "+address.getPrimerDigito());
       
       user.getDireccionCollection().remove(address);
   
   }
   catch(Exception ex){
   
     Logger.getLogger(CompleteRegistration.class.getName()).log(Level.SEVERE,null,ex);
       
   }
   
   
   }
   
   public void deletePhone(javax.faces.event.AjaxBehaviorEvent event, Entities.Phone phone){
   
   try{
   
       this.user.getPhoneCollection().remove(phone);
   
   }
   catch(Exception ex){
   
       Logger.getLogger(CompleteRegistration.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
   }
   
       
    public void addPhoneNumber(javax.faces.event.AjaxBehaviorEvent event){
    
    try{
    
      this.user.getPhoneCollection().add(new Entities.Phone());
    
    }
    catch(Exception ex){
    
    Logger.getLogger(CompleteRegistration.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
   
}
