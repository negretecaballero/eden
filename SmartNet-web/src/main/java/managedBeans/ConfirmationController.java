/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Entities.Confirmation;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.ConfirmationFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;


/**
 *
 * @author luisnegrete
 */
public class ConfirmationController {

    /**
     * Creates a new instance of ConfirmationController
     */
    public ConfirmationController() {
    }
    
    @EJB
    private ConfirmationFacadeREST confirmationFacadeREST;
    
    private String text;
    
    private String username;
    
    private String type;
    
    public String getText(){
    
    return this.text;
    
    }
    
    public void setText(String text){
    
    this.text=text;
    
    }
    
    
    @PostConstruct
    public void init(){
    
    Map<String,String>requestMap=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    
    
    for(Map.Entry<String,String>entry:requestMap.entrySet()){
    
    System.out.print(entry.getKey()+"-"+entry.getValue());
    
    }
    
    this.username=requestMap.get("username");
    
    this.type=requestMap.get("type");
        
    }
    
    public String confirm(){
    
   try{
 
       if(type.equals("user")){
           
           Confirmation  confirmation=new Confirmation();
           
           PathSegment ps=new PathSegmentImpl("bar;aPPGROUPgroupid=guanabara_user;aPPGROUPloginAdministradorusername="+this.username+"");
           
           confirmation=confirmationFacadeREST.find(ps);
       
           confirmation.setConfirmed(true);
           
         //  confirmationFacadeREST.edit(ps, confirmation);
      
       
       
       }
       
   
       
       return "confirmed";
   }
   catch(EJBException ex){
       
      Logger.getLogger(ConfirmationController.class.getName()).log(Level.SEVERE,null,ex);
       
       return "failure";
   
   }
    }
}
