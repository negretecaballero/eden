/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import Clases.Direccion;
import Clases.ImageType;

import Entities.GpsCoordinates;
import Entities.Imagen;
import Entities.Sucursal;

import SessionBeans.PhotoListLocal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.event.ValueChangeListener;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import jaxrs.service.FranquiciaFacadeREST;
import jaxrs.service.GpsCoordinatesFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.SucursalFacadeREST;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.map.MapModel;




/**
 *
 * @author luisnegrete
 */
public class CrudSucursal implements Serializable{
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    @EJB
    private FranquiciaFacadeREST franquiciaFacadeREST;
    @EJB
    private SucursalFacadeREST sucursalFacadeREST;
    @EJB
    private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
    
    
    
   //@Inject UploadFilesInterface upload;

    public List<String> getOriginalImage() {
        return originalImage;
    }

    public void setOriginalImage(List<String> originalImage) {
        this.originalImage = originalImage;
    }

  
private List <String> originalImage;



   
    /**
     * Creates a new instance of CrudSucursal
     */
    public CrudSucursal() {
    }
    
   
    
    @Size(min=40, max=100)
     private String jsDireccion;

    public String getJsDireccion() {
        return jsDireccion;
    }

    public void setJsDireccion(String jsDireccion) {
        this.jsDireccion = jsDireccion;
    }
    
    
    
    
    private int idFranquicia;
    
    private Direccion direccion;
    
    private double alcance;
    
    @Min(-180)
	@Max(180)
    private double longitude;
    
    @Min(-90)
	@Max(90)
    private double latitude;
    
    @Inject 
    private PhotoListLocal listImages;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getAlcance() {
        return alcance;
    }

    public void setAlcance(double alcance) {
        this.alcance = alcance;
    }
    
     private MapModel model;

    public MapModel getModel() {
        return model;
    }

    public void setModel(MapModel model) {
        this.model = model;
    }
    
    
    @PostConstruct
    private void Initialize(){
        
      if(direccion==null){
        direccion=new Direccion();
        this.jsDireccion=this.direccion.getTipo()+" "+this.direccion.getPrimerDigito()+"#"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito()+" Bogota,Cundinamarca,Colombia";

        } 
    if(originalImage==null)
    {
    originalImage=new ArrayList<>();
    }
System.out.print("entro Crud Sucursal");
    }

    public void postValidateAddress (ComponentSystemEvent event)
    {
    FacesContext fc=FacesContext.getCurrentInstance();
    
    UIComponent components=event.getComponent();
    
     UIInput pDigito=(UIInput)components.findComponent("primerDigito");
     
     System.out.print(pDigito.getLocalValue().toString());
    
     UIInput typo=(UIInput)components.findComponent("tipo");
     
     System.out.print(typo.getLocalValue().toString());
     
    
     
     UIInput sDigito=(UIInput)components.findComponent("segundoDigito");
     
     System.out.print(sDigito.getLocalValue().toString());
     
      UIInput tDigito=(UIInput)components.findComponent("tercerDigito");
      
       System.out.print(tDigito.getLocalValue().toString());
       
       String direccionn=typo.getLocalValue().toString()+" "+pDigito.getLocalValue().toString()+"#"+sDigito.getLocalValue().toString()+"-"+tDigito.getLocalValue().toString()+" Bogota,Cundinamarca,Colombia";
      
       for(Sucursal a:sucursalFacadeREST.findAll()){
     /* Error here  if(a.getDireccion().toLowerCase().equals(direccionn.toLowerCase())){
        FacesMessage msg = new FacesMessage("Ya existe una sucursal en la direccion digitada");
		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		fc.addMessage(typo.getClientId(), msg);
		fc.renderResponse();
      }*/
       }
       
    }
    
    public String agregarSucursal(){
     HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    
     String value=req.getParameter("latitude");
     System.out.print(value);
     
  this.direccion.setTipo(this.direccion.getTipo());
  this.direccion.setPrimerDigito(this.direccion.getPrimerDigito());
  this.direccion.setSegundoDigito(this.direccion.getSegundoDigito());
  this.direccion.setTercerDigito(this.direccion.getTercerDigito());
 
   
   try{
       
   this.latitude=Double.parseDouble(value);
   this.longitude=Double.parseDouble(req.getParameter("longitude"));
   System.out.print(latitude+" "+longitude);
   
    this.jsDireccion=this.direccion.getTipo()+" "+this.direccion.getPrimerDigito()+"#"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito()+" Bogota,Cundinamarca,Colombia";
    System.out.print(jsDireccion.length());

   
   }
   
   catch(NumberFormatException ex){
       
   System.out.print("La longitud y latitud deben ser valores numericos");
   
   }
    
   
   
   try{
   //Add Gps Coordinates
    GpsCoordinates coordinates=new GpsCoordinates();
    
    coordinates.setLatitude(this.latitude);
    
    coordinates.setLongitude(this.longitude);
    
    gpsCoordinatesFacadeREST.create(coordinates);
    
     //Add Sucursal
   
     Sucursal sucursal=new Sucursal();
     
     sucursal.setAlcance(this.alcance);
     
     //Error here sucursal.setDireccion(jsDireccion);
     
     //Error here sucursal.setFranquiciaIdfranquicia(franquiciaFacadeREST.find(this.idFranquicia));
     
     //Error here sucursal.setGpsCoordinatesidgpsCoordinated(coordinates);
     
     sucursalFacadeREST.create(sucursal);
    
    
    //Add Images
    /*for(ImageType aux:upload.getPhotoList())
        
    {
    Imagen image=new Imagen();
    image.setImagen(aux.getArray());
    image.setExtension(aux.getExt());
    //image.setTipo("franquicia");
    //Error here image.setIdComponente(sucursal.getIdsucursal());
    imagenFacadeREST.create(image);
    }*/
    
    
   }
   
   catch(EJBException ex){
   
   }
     
     
     return "index";
     
    }
    
  
    public void valueChanged(ValueChangeListener event){
        
    System.out.print("entro value change");
    
    }
     
    public void handleFileUpload(FileUploadEvent event) throws IOException{
        
//    upload.addFiles(event.getFile());  
    
    System.out.print("foto agregada a la lista");
    
}
    
   private @Inject PhotoListLocal photoList;
    
    public void cleanList(){
    //upload.getPhotoList().clear();
    photoList.clearList("adminFranquicia/imagesCache");
    }
    
    public StreamedContent pictures(int pointer){

        try{
            
        System.out.print("Pointer length "+originalImage.size());
        System.out.print(pointer);
//        InputStream is=new ByteArrayInputStream(upload.getPhotoList().get(pointer).getArray());
        StreamedContent prueba=new DefaultStreamedContent(null, "image/jpg");
        return prueba;
     
   
        }
        
   
        
        catch(NumberFormatException e){
        return null;
        }
        
    
   
    }
    
      public void doGeocoding(){
        
  this.direccion.setTipo(this.direccion.getTipo());
  this.direccion.setPrimerDigito(this.direccion.getPrimerDigito());
  this.direccion.setSegundoDigito(this.direccion.getSegundoDigito());
  this.direccion.setTercerDigito(this.direccion.getTercerDigito());
   System.out.print(this.direccion.getTipo()+" "+this.direccion.getPrimerDigito()+"#"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito()+" Bogota,Cundinamarca,Colombia");

this.jsDireccion=this.direccion.getTipo()+" "+this.direccion.getPrimerDigito()+"#"+this.direccion.getSegundoDigito()+"-"+this.direccion.getTercerDigito()+" Bogota,Cundinamarca,Colombia";
System.out.print(jsDireccion.length());
// String value = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(":formulario:componentId");     
    
     HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    
   String value = req.getParameter("latitude");
   try{
   this.latitude=Double.parseDouble(value);
   this.longitude=Double.parseDouble(req.getParameter("longitude"));
   System.out.print(latitude+" "+longitude);}
   
   catch(NumberFormatException e){
   
   }
   
    }
      
      public void reverseGeocoding(){
      System.out.print("tab changed");
      }
   
    
    public Direccion getDireccion() {
       
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public int getIdFranquicia() {
        return idFranquicia;
    }

    public void setIdFranquicia(int idFranquicia){
        this.idFranquicia = idFranquicia;
    }
 
}
