/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;


import Entities.CategoriaPK;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class DeleteEditCategoria {

    /**
     * Creates a new instance of DeleteEditCategoria
     */
    public DeleteEditCategoria() {
    }

    public List<CategoriaPK> getSelected() {
        return selected;
    }

    public void setSelected(List<CategoriaPK> selected) {
        this.selected = selected;
    }
    
    
    
    private List<CategoriaPK> selected;
}
