/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */

@javax.annotation.security.RolesAllowed("GuanabaraFrAdmin")
public class IndexAdminFranchiseFrontView extends BaseBacking{

    /**
     * Creates a new instance of IndexAdminFranchiseFrontView
     */
    public IndexAdminFranchiseFrontView() {
    }
    
    private java.util.List<String>sucursalesImages;
    
    public void setSucursalesImages(java.util.List<String>sucursalesImages){
    
        this.sucursalesImages=sucursalesImages;
                
    
    }
    
    public java.util.List<String>getSucursalesImages(){
    
        return this.sucursalesImages;
    
    }
    
    
    
    private Entities.Franquicia franchise;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FranchiseControllerInterface franchiseController;
    
    @javax.annotation.PostConstruct
    public void init(){
    
    try{
    
       if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
           
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
       
           
           this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
           
           
          this.franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
       
          this.sucursalesImages=this.franchiseController.SucursalesPhotos(franchise);
      
       
       
       }
       else{
       
           throw new IllegalArgumentException("Objcet of type "+getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+getSession().getAttribute("selectedOption").getClass().getName());
       
       }
        
    }
    catch(Exception ex){
    
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    
    public String getLogo(){
    
    
    try{
    
        return this.franchiseController.FranchiseLogo(franchise);
        
        
    }
    catch(javax.ejb.EJBException   | IllegalArgumentException ex){
    
    throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    

}
