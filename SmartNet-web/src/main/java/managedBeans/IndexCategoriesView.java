/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import SessionClasses.Key;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class IndexCategoriesView extends Clases.BaseBacking{

    /**
     * Creates a new instance of IndexCategoriesView
     */
    
     private @javax.inject.Inject @javax.enterprise.inject.Default CDIBeans.CategoryControllerInterface categoryController;
 
    private Key selectedCategory;
    
    public Key getSelectedCategory(){
    
    return this.selectedCategory;
    
    }
    
    public void setSelectedCategory(Key selectedCategory){
    
        this.selectedCategory=selectedCategory;
    
    }
    
    public IndexCategoriesView() {
    }
    
   
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.IndexCategoriesViewControllerInterface indexCategoriesViewController;
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
            
            if(!this.fileUpload.getClassType().equals(IndexCategoriesView.class.getName())){
            
            this.fileUpload.setClassType(IndexCategoriesView.class.getName());
            
            }
        
            if(getSession().getAttribute("selectedOption")instanceof Entities.Sucursal){
            
            Entities.Sucursal sucursal=(Entities.Sucursal)getSession().getAttribute("selectedOption");    
                
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
          
            this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
            
           indexCategoriesViewController.init(sucursal);
            
           if(this.indexCategoriesViewController.getIndexCategoryList().isEmpty()){
           
               System.out.print("Index Category List is empty");
           
               Clases.IndexCategoryPOJOInterface aux=new Clases.IndexCategoryPOJO();
               
               aux.setImageRoot("/images/noImage.png");
               
               this.indexCategoriesViewController.getIndexCategoryList().add(aux);
           }
           
            }
            
            else{
            
                throw new IllegalArgumentException();
            
            }
            
        }
        catch(Exception ex){
        
            Logger.getLogger(IndexCategoriesView.class.getName()).log(Level.SEVERE,null,ex);
            
        }
    
    }
    
public CDIEden.IndexCategoriesViewControllerInterface getIndexCategoriesViewController(){

return this.indexCategoriesViewController;

}

public void setIndexCategoriesViewController(CDIEden.IndexCategoriesViewControllerInterface indexCategoriesViewController){

this.indexCategoriesViewController=indexCategoriesViewController;

}
   
public String selectCategory(){
try{
if(this.selectedCategory instanceof Entities.CategoriaPK){

    Entities.CategoriaPK key=(Entities.CategoriaPK)this.selectedCategory;
    
    
    
    System.out.print("The object is of the rigth type");
    
    Entities.Categoria cat=(Entities.Categoria)this.categoryController.find(key);
    
    System.out.print(cat.getNombre());
    
    this.categoryController.loadImages(getSession().getAttribute("username").toString(), key);
    
    this.setSessionAuxiliarComponent(this.categoryController.find(key));
     
    
    
    
return "updateCategory";
}
else{

    throw new IllegalArgumentException("Object of type "+this.selectedCategory.getClass().getName()+" must be of type "+Entities.CategoriaPK.class.getName());

}
}
catch(Exception ex){

    Logger.getLogger(IndexCategoriesView.class.getName()).log(Level.SEVERE,null,ex);
    
    throw new javax.faces.FacesException(ex);

}

}
   
}
