/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author luisnegrete
 */
public class IndexProductView extends Clases.BaseBacking{

    /**
     * Creates a new instance of IndexProductView
     */
    public IndexProductView() {
        
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.SucursalControllerDelegate sucursalController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.annotation.PostConstruct
    public void init(){
    try{
    
     if(this.getSession().getAttribute("selectedOption") instanceof Entities.Sucursal){
     
         Entities.Sucursal sucursal=(Entities.Sucursal)getSession().getAttribute("selectedOption");
         
          
         this.sucursalController.loadProductImages(getSession().getAttribute("username").toString(), sucursal.getSucursalPK());   
       
         if(!this.fileUpload.getClassType().equals(IndexProductView.class.getName())){
         
             this.fileUpload.setClassType(IndexProductView.class.getName());
             
             javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)this.getContext().getExternalContext().getContext();
         
         this.fileUpload.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+getSession().getAttribute("username").toString());
         
         }
         
     }
     else{
     
         throw new javax.ejb.EJBException();
     
     }
    
    }
    catch(javax.ejb.EJBException ex){
    
        Logger.getLogger(IndexProductView.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    
    
    public java.util.List<String>getPhotoList(){
    
    return this.fileUpload.getImages();
    
    }
}
