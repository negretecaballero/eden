/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import Entities.Inventario;


import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.event.ActionListener;
import javax.faces.event.ComponentSystemEvent;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import jaxrs.service.InventarioFacadeREST;
import jaxrs.service.SucursalFacadeREST;


/**
 *
 * @author luisnegrete
 */
public class InventarioBackingBean extends BaseBacking{
    @EJB
    private SucursalFacadeREST sucursalFacadeREST;
  
   
    
    @EJB
    private InventarioFacadeREST inventarioFacadeREST;
    
    

    /**
     * Creates a new instance of InventarioBackingBean
     */
    public InventarioBackingBean() {
    
    }
    
    @Size(min=1,max=45)
    private String name;
    
   // private
  //@Inject ListingInvLocal listingInv;
    
    
   
    private double cantidad;
    
    @Size(min=0,max=20)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public void postValidation(ComponentSystemEvent event){
    
        UIComponent component=event.getComponent();
        
        UIInput nombre=(UIInput)component.findComponent("name");
        
        System.out.print(nombre.getLocalValue().toString());
        
     
      
        /*Error here if(inventarioFacadeREST.findbyName(nombre.getLocalValue().toString(),typeSession.getIdFranquicia())!=null)
        {
            
        FacesMessage msg=new FacesMessage("Ya existe un inventario con este nombre en esta franqucia");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        getContext().addMessage(nombre.getClientId(),msg);
        getContext().renderResponse();
        
        }*/
       
       
        
    }
    
    public String AddInv(){
       
    Inventario aux=new Inventario();
   
    aux.setDescripcion(description);
    aux.setNombre(name);
  
   //Error here  aux.setSucursalIdsucursal(sucursalFacadeREST.find(typeSession.getIdSucursal()));
    try{
        
    inventarioFacadeREST.create(aux);
   //  listingInv.updateModel();
    }
    catch(EJBException ex){
    System.out.print("Bean exception cought");
    }
    
    
    return "index";
    
    }
    
    public void editInv(ActionListener event){
    try{
     /* for(String a:listingInv.getEditList())  
      {
      inventarioFacadeREST.edit(listingInv.findInv(Integer.parseInt(a)));
      }
       FacesMessage msg = new FacesMessage("Editado con exito");
		
      getContext().addMessage(null,msg);
      getContext().renderResponse();
      listingInv.getEditList().clear();
     listingInv.updateModel();*/
    }
    catch(EJBException ex){
    
    }
        
    }
    
    public void deleteInv(ActionListener event){
    try{
      //  System.out.print("Elements to delete "+listingInv.getDeleteList().size());
        
   /* for(Inventario aux:listingInv.getDeleteList()){
        
 //inventarioFacadeREST.remove(aux.getIdinventario());
    
    }*/
   // listingInv.updateModel();
    
    }
    catch(EJBException ex){
    
    }
    catch(NumberFormatException ex){
    
    }
    }
    
    
    
}
