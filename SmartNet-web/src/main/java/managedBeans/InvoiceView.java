/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author luisnegrete
 */
public class InvoiceView extends Clases.BaseBacking{

    /**
     * Creates a new instance of InvoiceView
     */
    public InvoiceView() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.InvoiceControllerInterface invoiceController;

   
    @javax.inject.Inject @javax.enterprise.inject.Default private Controllers.InvoiceControllerInterface invoiceControllerr;
    
    private boolean noInvoicesRender;
    
    public boolean getNoInvoicesRender(){
    
        return this.noInvoicesRender;
        
    }
    
    public void setNoInvoicesRender(boolean noInvoicesRender){
    
        this.noInvoicesRender=noInvoicesRender;
    
    }
    
   
    @javax.annotation.PostConstruct
    public void init(){
    
    try{
        
        Calendar calendar=Calendar.getInstance();
        
        System.out.print("Original invoices size "+this.invoiceController.getInvoices().size());
        
      System.out.print("Flag Value "+this.invoiceController.getFlag());
        
      
        if(this.getInvoices()!=null && !this.getInvoices().isEmpty()){
        
        this.noInvoicesRender=true;
        
        }
        
        else{
        
            this.noInvoicesRender=false;
        
        }
        
        System.out.print("No Invoices Renderer "+this.noInvoicesRender);
    
    }
    catch(Exception ex){
    
        Logger.getLogger(InvoiceView.class.getName()).log(Level.SEVERE,null,ex);
        
        throw new javax.faces.FacesException(ex);
    
    }
    
    }
    
    public java.util.List<SessionClasses.Invoice>getInvoices(){
    
    
        
    return this.invoiceController.getInvoices();
    
    }

}
