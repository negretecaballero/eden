/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import Clases.CategoriaUpdated;
import Clases.CategoriaUpdatedInterface;

import Entities.Categoria;
import Entities.Sucursal;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateful;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.CategoriaFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;




/**
 *
 * @author luisnegrete
 */
@Stateful
@Named(value = "listadoCategorias")
@SessionScoped

public class ListadoCategorias {
    
    @EJB
    private CategoriaFacadeREST categoriaFacadeREST;
  
   

    /**
     * Creates a new instance of ListadoCategorias
     */
    public ListadoCategorias() {
    }
   
    
    public List<CategoriaUpdated> getAllCategorias(){
           
        try{
            List <CategoriaUpdated>listaCategoria=new <CategoriaUpdatedInterface> ArrayList();
            HttpSession session=(HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            
            Entities.Franquicia franchise;
            
            if(session.getAttribute("selectedOption") instanceof Entities.Franquicia){
            franchise=(Entities.Franquicia) session.getAttribute("selectedOption");
            }
            else{
                
            franchise=null;
            
            }
      
             List <Categoria>listaCat=categoriaFacadeREST.findByFranchise(franchise.getIdfranquicia());
           
          for(Categoria aux:listaCat){
              
           CategoriaUpdated updated=new CategoriaUpdated();
           
           updated.setCategoria(aux);
           
           updated.setConcatKey(aux.getCategoriaPK().getIdcategoria()+","+aux.getCategoriaPK().getFranquiciaIdfranquicia()+"");
           
           listaCategoria.add(updated);
           }
            return listaCategoria;
           
          
        
        }
        
     
        catch(EJBException ex){
            
        return null;
        
        }
   
    }
       
    
}
