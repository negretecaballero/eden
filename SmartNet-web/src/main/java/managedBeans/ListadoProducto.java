/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;


import Entities.Producto;

import java.util.List;
import javax.ejb.EJB;
import jaxrs.service.ProductoFacadeREST;






/**
 *
 * @author luisnegrete
 */
public class ListadoProducto {
    @EJB
    private ProductoFacadeREST productoFacadeREST;

    /**
     * Creates a new instance of ListadoProducto
     */
    private List <Producto> productos;

    public List<Producto> getProductos() {
        this.productos=productoFacadeREST.findAll();
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    
    
    
}
