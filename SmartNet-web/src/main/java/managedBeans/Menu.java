/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import Clases.MenuList;
import Clases.MenuListInterface;
import Entities.Agrupa;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.AgrupaFacadeREST;
import jaxrs.service.MenuFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
public class Menu extends BaseBacking{

    /**
     * Creates a new instance of Menu
     */
    
    @EJB 
    private MenuFacadeREST menuFacadeREST;
    
    @EJB
    private AgrupaFacadeREST agrupaFacadeREST;
    
    public Menu() {
    }
    
    private List<MenuListInterface>menuList;
    
    public List<MenuListInterface>getMenuList(){
    
        return this.menuList;
    
    }
    
    public void setMenuList(List<MenuListInterface>menuList){
    
        this.menuList=menuList;
    
    }
    
    
    private boolean checkMenu(Agrupa agrupa){
    
    
        for(MenuListInterface aux:this.menuList){
        
        if(aux.getMenu().equals(agrupa.getMenu())){
        
        return true;
        
        }
        
        }
    
    return false;
    
    }
    
   @PostConstruct
   public void init(){
   
       this.menuList=new <MenuList> ArrayList();
       
   if(getSessionAuxiliarComponent()!=null && getSessionAuxiliarComponent()instanceof Entities.Sucursal)
       
   {
   
       Entities.Sucursal suc=(Entities.Sucursal)getSessionAuxiliarComponent();
       
       PathSegment ps=new PathSegmentImpl("bar;idsucursal="+suc.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+suc.getSucursalPK().getFranquiciaIdfranquicia()+"");
       
     List <Agrupa> ListAgrupa=agrupaFacadeREST.findByIdSucursal(ps);
       
      for(Agrupa agrupa:ListAgrupa){
          
          if(!checkMenu(agrupa)){
      
      MenuListInterface aux=new MenuList();
      
      aux.setMenu(agrupa.getMenu());
      
      this.menuList.add(aux);
          }
          
      }
       
   }
   
   else
   
   {
   
       setSessionAuxiliarComponent(null);
   
   }
       
   }
    
}
