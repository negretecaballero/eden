/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.FileUploadInterface;
import CDIBeans.LoginAdministradorControllerInterface;
import CDIBeans.MenuControllerInterface;
import CDIBeans.ProductoControllerInterface;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Clases.ProductsResultsTypeInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import SessionClasses.CartItem;
import SessionClasses.Item;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author luisnegrete
 */
public class MenuInfoView {

    @Inject MenuControllerInterface menuController;
    
    @Inject FileUploadInterface fileUploadBean;
    
    private Entities.Menu menu;
    
    private List<ProductsResultsTypeInterface>products;
    
    private List<SimilarProductsLayoutInterface>similarProductsList;
    
    @Inject
    private LoginAdministradorControllerInterface loginAdministradorController;
    
    @Inject
    private ProductoControllerInterface productoController;
    
    public List<SimilarProductsLayoutInterface> getSimilarProductsList(){
    
        return this.similarProductsList;
    
    }
    
    public void setSimilarProductsList(List<SimilarProductsLayoutInterface>similarProductsList){
    
        this.similarProductsList=similarProductsList;
    
    }
    
    public ProductoControllerInterface getProductoController(){
    
    return this.productoController;
    
    }
    
    public void setProductoController(ProductoControllerInterface productoController){
    
    
        this.productoController=productoController;
    
    }
    
    public List<ProductsResultsTypeInterface>getProducts(){
    
    return this.products;
    
    }
    
    public void setProducts(List<ProductsResultsTypeInterface>products){
    
    
    this.products=products;
    
    }
    
    public Entities.Menu getMenu(){
    
        return this.menu;
    
    }
    
    public void setMenu(Entities.Menu menu){
    
    this.menu=menu;
    
    }
    
    /**
     * Creates a new instance of MenuInfoView
     */
    public MenuInfoView() {
    }
    
    @PostConstruct
    public void init(){
    
        try{
        
         FacesContext fc=FacesContext.getCurrentInstance();
        
        HttpServletRequest request=(HttpServletRequest)fc.getExternalContext().getRequest();
            
        HttpSession session=(HttpSession)fc.getExternalContext().getSession(true);
        
        int id=Integer.parseInt(request.getParameter("idmenu"));
        
        System.out.print("ID Menu "+id);
        
        menu=menuController.find(id);
        
        fileUploadBean.Remove("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/USer/imagesCache/"+session.getAttribute("username"));
        
        FilesManagementInterface filesManagement=new FilesManagement();
        
        this.products=new<Clases.ProductsResultsType> ArrayList();
        
        for(Entities.Producto product:this.menuController.products(menu.getIdmenu())){
        
            ProductsResultsTypeInterface aux=new Clases.ProductsResultsType();
            
            aux.setProduct(product);
            
//            List<SimilarProductsLayoutInterface> auxi=productoController.findSimilar(product.getIdproducto());
         
            filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/USer/imagesCache/"+session.getAttribute("username")+"/", product.getNombre());
            
            for(Entities.Imagen image:product.getImagenCollection()){
            
            fileUploadBean.AddPhoto(image, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product.getNombre(), session.getAttribute("username")+"/"+product.getNombre(), 500, 500);
            
            aux.getImages().add(this.fileUploadBean.getImages().get(this.fileUploadBean.getImages().size()-1));
            
            }
                
              this.fileUploadBean.getImages().clear();
              
              this.fileUploadBean.getPhotoList().clear();
              
              
              System.out.print("Photos in aux Object "+aux.getImages().size());
                
           this.products.add(aux);
        
        }
        
        
        for(Entities.Imagen image:menu.getImagenCollection()){
        
        fileUploadBean.AddPhoto(image, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username"), session.getAttribute("username").toString(), 500, 500);
        
        }
        
        System.out.print("PhotoList size "+this.fileUploadBean.getPhotoList().size());
        
        System.out.print("Images Size "+this.fileUploadBean.getImages().size());
        
        for(String aux:this.fileUploadBean.getImages()){
        
        System.out.print(aux);
        
        }
        loginAdministradorController.AddLastSeen(((loginBean)session.getAttribute("loginBean")).getLog(), (Item)menu);
        
        }
        catch(RuntimeException ex){
        
        Logger.getLogger(MenuInfoView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void quantityChanged(CartItem item){
    
        FacesContext fc=FacesContext.getCurrentInstance();
        
        HttpServletRequest request=(HttpServletRequest)fc.getExternalContext().getRequest();
        
        int quantity=Integer.parseInt(request.getParameter("form2:table:0:quantity"));
    
        System.out.print("Quantity: "+quantity);
        
       if(menuController.menuQuantity(this.menu.getIdmenu())<quantity){
       
           FacesMessage msg=new FacesMessage("There is not enough inventory for this menu");
           
           msg.setSeverity(FacesMessage.SEVERITY_ERROR);
           
           fc.addMessage(null,msg);
       
           item.setQuantity(1);
       
       }
    }
    
    public void changeMenuProduct(Entities.Producto product1, Entities.Producto product2){
    
    System.out.print("Product1 "+product1.getNombre());
    
    System.out.print("Product 2 "+product2.getNombre());

     HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    
    FilesManagementInterface filesManagement=new FilesManagement();
    
    String path="/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product1.getNombre();
    
    System.out.print("Path to Delete "+path);
    
    
    
    
    filesManagement.deleteFolder(path);
    
    filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/",product2.getNombre());
    
    
    for(ProductsResultsTypeInterface aux:products){
    
        if(aux.getProduct().equals(product1)){
        
            aux.setProduct(product2);
           
            this.productoController.findSimilar(product2.getProductoPK());
            
            aux.getImages().clear();
            
            List<String>auxiliarList=new <String>ArrayList();
            
            for(String auxi:fileUploadBean.getImages()){
            
            auxiliarList.add(auxi);
            
            }
            
            fileUploadBean.getImages().clear();
            
            fileUploadBean.getPhotoList().clear();
            
            for(Entities.Imagen image:product2.getImagenCollection() ){
           
               fileUploadBean.AddPhoto(image, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+product2.getNombre(), session.getAttribute("username")+"/"+product2.getNombre(), 500, 500);
              
               aux.getImages().add(fileUploadBean.getImages().get(fileUploadBean.getImages().size()-1));
            }
            
            fileUploadBean.getImages().clear();
            
            fileUploadBean.getPhotoList().clear();
           
            
            
            for(String auxi:auxiliarList){
            
                this.fileUploadBean.getImages().add(auxi);
            
            }
        break;
        }
    
    }
    
   
    }
    
    public void updateSimilarProductsList(Entities.ProductoPK idProducto){
        
        System.out.print("Updating list for "+productoController.find(idProducto));
        
    this.similarProductsList=this.productoController.findSimilar(idProducto);
    }
}
