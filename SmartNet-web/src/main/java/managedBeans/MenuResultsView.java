/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.FileUploadInterface;
import CDIBeans.MenuControllerInterface;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Clases.MenuResultsViewType;
import Clases.MenuResultsViewTypeInterface;
import SessionClasses.CartItem;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import jaxrs.service.AgrupaFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

/**
 *
 * @author luisnegrete
 */
public class MenuResultsView {

    @EJB
    private AgrupaFacadeREST agrupaFacadeREST;
    
    /**
     * Creates a new instance of MenuResultsView
     */
    public MenuResultsView() {
    }
    
    private List<MenuResultsViewTypeInterface> menuList;
    
    public List<MenuResultsViewTypeInterface> getMenuList(){
    
    return this.menuList;
    }
    
    public void setMenuList(List<MenuResultsViewTypeInterface> menuList){
    
    this.menuList=menuList;
    
    }
    
    @Inject
    private FileUploadInterface fileUpload;
    
    @Inject
    private MenuControllerInterface menuController;
    
    
    
    @PostConstruct
    public void init(){
    
        try{
        
            this.menuList=new <MenuResultsViewType>ArrayList();
            
            List<Entities.Menu>Lista=new <Entities.Menu>ArrayList();
            
            FacesContext fc=FacesContext.getCurrentInstance();
            
          HttpSession session=(HttpSession)fc.getExternalContext().getSession(true);  
        
          if(session.getAttribute("auxiliarComponent") instanceof Entities.Sucursal){
          
              Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("auxiliarComponent");
              
              
             List<Entities.Agrupa>agrupaList=agrupaFacadeREST.findByIdSucursal(new PathSegmentImpl("bar;idsucursal="+sucursal.getSucursalPK().getIdsucursal()+";franquiciaIdfranquicia="+sucursal.getSucursalPK().getFranquiciaIdfranquicia()+""));
              
             System.out.print("Agrupa List size "+agrupaList.size());
             
             for(Entities.Agrupa agrupa:agrupaList){
             
                 if(!Lista.contains(agrupa.getMenu())){
                 
                 Lista.add(agrupa.getMenu());
                 
                 }
             
             }
             
             FilesManagementInterface filesManagement=new FilesManagement();
             
             fileUpload.Remove("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username"));
             
             for(Entities.Menu menu:Lista){
             
             filesManagement.createFolder("/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/", menu.getNombre());
             
             MenuResultsViewTypeInterface aux=new MenuResultsViewType();
            
             
             aux.setMenu(menu);
             
             aux.setMenuQuantity(menuController.menuQuantity(menu.getIdmenu()));
             
             for(Entities.Imagen imagen:menu.getImagenCollection()){
             
             
             
             
             this.fileUpload.AddPhoto(imagen, "/Users/luisnegrete/Copy/ProyectoLuis/SmartNet/SmartNet-web/target/SmartNet-web-1.0-SNAPSHOT/User/imagesCache/"+session.getAttribute("username")+"/"+menu.getNombre()+"", session.getAttribute("username")+"/"+menu.getNombre(), 500, 500);
             
             aux.getImages().add(this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1));
             
             }
             
             this.fileUpload.getImages().clear();
             
             this.fileUpload.getPhotoList().clear();
             
             this.menuList.add(aux);
             }
             
             
          }
          
        }
        catch(RuntimeException ex){
        
            Logger.getLogger(MenuResultsView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void menuSelection(Entities.Menu menu){
    
    try{
    
      
        
         
            System.out.print("You have selected "+menu.getNombre());
        
       NavigationHandler navigationHandler=FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
        
       navigationHandler.handleNavigation(FacesContext.getCurrentInstance(), null, "/User/menuinfo.xhtml?faces-redirect=true&idmenu="+menu.getIdmenu()+"");
    
    }
    catch(RuntimeException ex){
    
        Logger.getLogger(MenuResultsView.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    }
    
    public void quantityChanged(CartItem item){
    
        FacesContext fc=FacesContext.getCurrentInstance();
        
        Map<String,String>requestMap=fc.getExternalContext().getRequestParameterMap();
        
        for(Map.Entry<String,String>entry:requestMap.entrySet()){
        
            System.out.print(entry.getKey()+"-"+entry.getValue());
        
        }
        
        HttpServletRequest request=(HttpServletRequest)fc.getExternalContext().getRequest();
        
        int quantity=Integer.parseInt(request.getParameter("form2:table:0:quantity"));
        
        System.out.print("Quantity : "+quantity);
    
        Entities.Menu menu=(Entities.Menu)item.getItem();
        
        if(menuController.menuQuantity(menu.getIdmenu())<quantity){
        
            FacesMessage msg=new FacesMessage("There is not enough inventory for this menu");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            fc.addMessage(null, msg);
            
            item.setQuantity(1);
        
        }
        
        
        
    }
}
