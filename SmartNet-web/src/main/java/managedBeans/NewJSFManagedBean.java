/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import java.util.Map;


import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.*;

/**
 *
 * @author luisnegrete
 */
@Named("UserSettingsController")
@SessionScoped
public class NewJSFManagedBean implements Serializable {

    /**
     * Creates a new instance of NewJSFManagedBean
     */
  private Map<String,String>themes;

    public Map<String, String> getThemes() {
        return themes;
    }

    public void setThemes(Map<String, String> themes) {
        this.themes = themes;
    }

    public NewJSFManagedBean() {
        
    }

  

  

    public void initThemes() {
        
        System.out.print("Retrieving themes");
        
          Map<String,Object>ApplicationMap=FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();
        
          if(this.themes==null){
          
          this.themes=(Map<String, String>) ApplicationMap.get("temas");
        
         System.out.print("Size theme "+this.themes.size());
         
          }
         
        
    }
    
    
    

   
    
}
