/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;

/**
 *
 * @author luisnegrete
 */
public class OrdersView {

    /**
     * Creates a new instance of OrdersView
     */
    public OrdersView() {
    }
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIEden.OrdersControllerInterface ordersController;
    
    private java.util.Date from;
    
    public java.util.Date getFrom(){
    
        return this.from;
    
    }
    
    public void setFrom(java.util.Date from)
    {
    
        this.from=from;
    
    }
    
    private java.util.Date to;
    
    public java.util.Date getTo(){
    
    return this.to;
    
    }
    
    public void setTo(java.util.Date to){
    
    this.to=to;
    
    }
    
    
    private java.util.List<Entities.Pedido>orders;
    
    public java.util.List<Entities.Pedido>getOrders(){
    
        return this.orders;
    
    }
    
    public void setOrders(java.util.List<Entities.Pedido>orders){
    
        this.orders=orders;
    
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
            
           this.from=this.ordersController.getFromDate();
        
           this.to=this.ordersController.getToDate();
           
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
           if(session.getAttribute("selectedOption") instanceof Entities.Sucursal){
           
           Entities.Sucursal sucursal=(Entities.Sucursal)session.getAttribute("selectedOption");
               
           this.orders=this.ordersController.getOrders(from, to,sucursal.getSucursalPK());
           
           System.out.print("Orders Size "+this.orders.size());
           
           for(Entities.Pedido order:this.orders){
           
           System.out.print(order.getIdPEDIDO()+"-"+order.getSucursal().getSucursalPK().getIdsucursal());
           
           }
           
           
           }
           else{
           
               throw new javax.faces.FacesException("Exception Caugth");
           
           }
        }
        catch(Exception ex){
        
            Logger.getLogger(OrdersView.class.getName()).log(Level.SEVERE,null,ex);
            
            throw new javax.faces.FacesException(ex);
        
        }
    
    }
}
