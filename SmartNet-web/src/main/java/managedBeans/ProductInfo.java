/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.FileUploadInterface;
import CDIBeans.LastSeenControllerInterface;
import CDIBeans.LoginAdministradorControllerInterface;
import CDIBeans.ProductoControllerInterface;
import Clases.BaseBacking;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Clases.ProductsResultsType;
import Clases.ProductsResultsTypeInterface;
import Entities.SimilarProducts;
import SessionClasses.CartItem;
import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import jaxrs.service.ProductoFacadeREST;

/**
 *
 * @author luisnegrete
 */
@RolesAllowed("Guanabarauser")
public class ProductInfo extends BaseBacking{

    /**
     * Creates a new instance of ProductInfo
     */
    
    private List<String> path;
    
    @Inject private LoginAdministradorControllerInterface loginAdministradorController;
    
    @Inject private ProductoControllerInterface productoController;
    
    @Inject private LastSeenControllerInterface lastSeenController;
    
    private List<ProductsResultsTypeInterface>similarProducts;
    
    public List<ProductsResultsTypeInterface>getSimilarProducts(){
    
    return this.similarProducts;
    
    }
    
    public void setSimilarProducts(List<ProductsResultsTypeInterface>similarProducts){
    
    this.similarProducts=similarProducts;
    
    }
    
    public List<String> getPath(){
    
    return this.path;
    
    }
    
    public void setPath(List<String> path){
    
        this.path=path;
    
    }
    public ProductInfo() {
    }
    
    private Entities.Producto product;
    
    public Entities.Producto getProduct(){
    
    return this.product;
    
    }
    
    public void setProduct(Entities.Producto product){
    
    this.product=product;
    
    }
    
    @EJB 
    private ProductoFacadeREST productoFacadeREST;
    
    @Inject FileUploadInterface fileUpload;
    
    @PostConstruct
    public void init(){
    
        try{
        
            this.similarProducts=new <ProductsResultsType>ArrayList();
                                   
             
            FacesContext fc=FacesContext.getCurrentInstance();
            
            this.path=new<String>ArrayList();
            
            HttpServletRequest request=(HttpServletRequest)fc.getExternalContext().getRequest();
            
            product=productoFacadeREST.find(Integer.parseInt(request.getParameter("id")));
            
            HttpSession session=(HttpSession)fc.getExternalContext().getSession(true);
            
            
            lastSeenController.updateLastSeen(((loginBean)session.getAttribute("loginBean")).getLog());
            
            lastSeenController.addLastSeenProducto(product, ((loginBean)session.getAttribute("loginBean")).getLog());
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
              
            fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
            
            FilesManagementInterface filesManagement=new FilesManagement();
            
            filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username")+File.separator, "SimilarProducts");
            
           
            
            System.out.printf("Similar Products Size ", this.product.getSimilarProductsCollection().size());
            
            System.out.print("Similar Products 1 size "+this.product.getSimilarProductsCollection1().size());
            
            for(SimilarProducts similarProducts:this.product.getSimilarProductsCollection()){
            
               System.out.print(similarProducts.getProducto1().getNombre());
               
               Entities.Producto producto=similarProducts.getProducto1();
               
               ProductsResultsTypeInterface aux=new ProductsResultsType();
               
               aux.setProduct(producto);
               
              
               
               if(producto.getImagenCollection().size()>0){
               
               filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username")+"/SimilarProducts/", producto.getNombre());
               
               }
               
              
               
               System.out.print("Producto "+producto.getNombre());
               
               for(Entities.Imagen image:producto.getImagenCollection()){
               
              this.fileUpload.loadImagesServletContext(image, getSession().getAttribute("username").toString()+"/SimilarProducts/"+producto.getNombre()+"");
            
               aux.getImages().add(this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1));
               
               }
            
               
                this.similarProducts.add(aux);
                
                this.fileUpload.getImages().clear();
                
                this.fileUpload.getPhotoList().clear();
                
                 System.out.print("Products  image size "+aux.getImages().size());
           
            }
            
            for(SimilarProducts similarProducts:this.product.getSimilarProductsCollection1()){
            
             System.out.print(similarProducts.getProducto1().getNombre());
             
             Entities.Producto producto=similarProducts.getProducto();
             
             ProductsResultsTypeInterface aux=new ProductsResultsType();
             
             aux.setProduct(producto);
             
             
             
             if(producto.getImagenCollection().size()>0){
               
                 
               System.out.print("Creating folder");
                 
               filesManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username")+"/SimilarProducts/", producto.getNombre());
               
               }
             
             else{
             
                 aux.getImages().add(File.separator+"images"+File.separator+"noImage.png");
             
             }
             
             System.out.print("Producto 1 "+producto.getNombre());
             
             
             for(Entities.Imagen image:producto.getImagenCollection()){
             
             this.fileUpload.loadImagesServletContext(image,getSession().getAttribute("username")+"/SimilarProducts/"+producto.getNombre()+"");
            
             
             aux.getImages().add(this.fileUpload.getImages().get(this.fileUpload.getImages().size()-1));
            
                 
             }
            this.fileUpload.getPhotoList().clear();
            
            this.fileUpload.getImages().clear();
            
            System.out.print("Products 1 image size "+aux.getImages().size());
            
             this.similarProducts.add(aux);
            
            }
            
            
            if(this.product.getImagenCollection().size()>0){
              
            for(Entities.Imagen image:product.getImagenCollection()){
            
                String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username");
                
            fileUpload.loadImagesServletContext(image,getSession().getAttribute("username").toString());
            
            }
            }
            else{
            
                this.fileUpload.getImages().add(File.separator+"images"+File.separator+"noImage.png");
            
            }
            
            System.out.print(product.getNombre());
            
            
        
        }
        
        catch(EJBException ex){
        
                 Logger.getLogger(ProductInfo.class.getName()).log(Level.SEVERE,null,ex);
      
        
        }
        catch(RuntimeException ex){
        
            Logger.getLogger(ProductInfo.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void checkQuantity(CartItem item){
        
      
        System.out.print("Check Quantity Ajax Event");
        
        
        
        FacesContext fc=FacesContext.getCurrentInstance();
        
        HttpServletRequest request=(HttpServletRequest)fc.getExternalContext().getRequest();
        
        int quantity=Integer.parseInt(request.getParameter("westForm:table:0:quantity").toString());
      
        System.out.print("Quantity : "+quantity);
        
        System.out.print("Quantity input value "+item.getQuantity());
        
        if(quantity>productoController.productQuantity(this.product.getProductoPK())){
        
            FacesMessage msg=new FacesMessage("There is not enough inventory for this product");
            
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            
            item.setQuantity(1);
            
            fc.addMessage(null,msg);
        }
        
          
    }
}
