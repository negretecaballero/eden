/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author luisnegrete
 */
@Named("productoInventario")
@RequestScoped
public class ProductoInventario {
int idInv;

double cantInv;

    public int getIdInv() {
        return idInv;
    }

    public void setIdInv(int idInv) {
        this.idInv = idInv;
    }

    public double getCantInv() {
        return cantInv;
    }

    public void setCantInv(double cantInv) {
        this.cantInv = cantInv;
    }
   
    public ProductoInventario() {
    }
    
}
