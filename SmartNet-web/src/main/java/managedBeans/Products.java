/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import Clases.ProductsList;
import Clases.ProductsListInterface;
import Entities.Categoria;
import Entities.Producto;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author luisnegrete
 */
@RolesAllowed("Guanabarauser")
public class Products extends BaseBacking{

    /**
     * Creates a new instance of Products
     */
    public Products() {
    }
    
    List<ProductsListInterface>productsList;
    
    public List<ProductsListInterface>getProductsList(){
    
        return this.productsList;
    
    }
    
    public void setProductsList(List<ProductsListInterface>productsList){
    
    
        this.productsList=productsList;
    
    }
    
    private int quantity;
    
    public int getQuantity(){
    
    return this.quantity;
    
    }
    
    public void setQuantity(int quantity){
    
    this.quantity=quantity;
    
    }
    
    
    
    @PostConstruct
    public void init(){
        
        
    
    if(getSessionAuxiliarComponent()!=null && getSessionAuxiliarComponent() instanceof Entities.Sucursal){
        
        System.out.print("Auxiliar Component is not null");
    
    this.productsList=new <ProductsList>ArrayList();
        
        Entities.Sucursal suc=(Entities.Sucursal)getSessionAuxiliarComponent();
        
//            System.out.print("Categoria Size "+suc.getCategoriaCollection().size());
        
       /* for(Categoria cat:suc.getCategoriaCollection()){
            
         System.out.print("Product Size "+cat.getProductoCollection().size());
        
        for(Producto product:cat.getProductoCollection()){
        
            ProductsListInterface aux=new ProductsList();
            
            aux.setProduct(product);
            
            this.productsList.add(aux);
        
        }
        
        }*/
        
        System.out.print(this.productsList.size());
        
    }
    else{
    
    setSessionAuxiliarComponent(null);
    
    }
    
    }
    
    
    public void valueChanged(AjaxBehaviorEvent event){
    
        System.out.print("Value Changed");
    
    }
    
    
}
