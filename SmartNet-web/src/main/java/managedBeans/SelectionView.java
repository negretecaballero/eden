/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIEden.Special;
import CDIEden.SpecialGreeting;
import Clases.BaseBacking;
import EdenViewComponent.LayoutOptions;
import javax.inject.Inject;

/**
 *
 * @author luisnegrete
 */
public class SelectionView extends BaseBacking implements java.io.Serializable{

    /**
     * Creates a new instance of SelectionView
     */
    
    @javax.inject.Inject @Special
    
    private SpecialGreeting  specialGreeting;
    
    private EdenViewComponent.LayoutOptions layoutOptions;
    
    public LayoutOptions getLayoutOptions(){
    
        return this.layoutOptions;
    
    }
    
    public void setLayoutOptions(LayoutOptions layoutOptions){
    
        this.layoutOptions = layoutOptions;
    
    }
    
    @Inject 
    @CDIBeans.FileUploadBeanQualifier
    private transient CDIBeans.FileUploadInterface fileUpload;
    
    public SelectionView() {
    }
    
    
    public void preRendetView(javax.faces.event.ComponentSystemEvent event){
    
         SpecialGreeting greet = new SpecialGreeting();
        
         //CHECKING ANNOTATION
         if(greet.getClass().isAnnotationPresent(Special.class)){
         
         System.out.print("SPECIAL GREETING IS ANNOTATED WITH SPECIAL");
         
         }
        
         if(!this.fileUpload.getActionType().getActionType().equals(this.getClass().getName())){
     
     this.fileUpload.setActionType(new Clases.ActionType(this.getClass().getName()));
     
     }
      
     
       if(this.getSession().getAttribute("username")!=null){
      
   javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(specialGreeting.getSpecialGreeting(this.getSession().getAttribute("username").toString()));

   msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
   
   getContext().addMessage(null,msg);
   
       }
   
   
    
         
    }
    
    
    @javax.annotation.PostConstruct
    public void init(){
    
        layoutOptions = new EdenViewComponent.LayoutOptions();
     
        
        //for all panes 
        
        LayoutOptions panes = new LayoutOptions();
        
        panes.addOption("resizable", true);
        panes.addOption("closable",true);
        panes.addOption("slidable",false);
        panes.addOption("spacing", 6);
        panes.addOption("resizeWithWindow", false);
        panes.addOption("resizeWhileDragging", true);
        layoutOptions.setPanesOptions(panes);
        
        //north pane
        LayoutOptions north = new LayoutOptions();
        north.addOption("resizable", false);
        north.addOption("closable", false);
        north.addOption("size", 60);
        layoutOptions.setNorthOptions(north);
        
        //South pane
        LayoutOptions south = new LayoutOptions();
        south.addOption("resizable", false);
        south.addOption("closable", false);
        south.addOption("size", 40);
        layoutOptions.setSouthOptions(south);
        
        //Center pane
        LayoutOptions center= new LayoutOptions();
        center.addOption("resizable", false);
        center.addOption("closable", false);
        center.addOption("resizeWhileDragging", false);
        center.addOption("minWidth", 200);
        center.addOption("minHeight", 60);
        layoutOptions.setCenterOptions(center);
        
        //West pane
        LayoutOptions west = new LayoutOptions();
        west.addOption("size", 210);
        west.addOption("minSize",180);
        west.addOption("maxSize", 500);
        layoutOptions.setCenterOptions(west);
        
        //east pane
        LayoutOptions east = new LayoutOptions();
        east.addOption("size",448);
        east.addOption("minSize", 180);
        east.addOption("maxSize", 650);
        layoutOptions.setEastOptions(east);
        
        //nested east layout
        LayoutOptions childEastOptions = new LayoutOptions();
        east.setChildOptions(childEastOptions);
        
        //east center pane
        LayoutOptions eastCenter = new LayoutOptions();
        eastCenter.addOption("minHeight",60);
        childEastOptions.setCenterOptions(eastCenter);
        
        //south-center pane
        LayoutOptions southCenter = new LayoutOptions();
        southCenter.addOption("size", "70%");
        south.addOption("minSize",60);
        childEastOptions.setSouthOptions(southCenter);
    
    }
}
