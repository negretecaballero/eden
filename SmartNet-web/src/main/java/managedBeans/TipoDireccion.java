/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author luisnegrete
 */
public class TipoDireccion implements Serializable{

    /**
     * Creates a new instance of TipoDireccion
     */
    public TipoDireccion() {
 
    }
    
    private List <String>tipo;

    public List<String> getTipo() {
        
        
        return tipo;
    }

    public void setTipo(List<String> tipo) {
        this.tipo = tipo;
    }
}
