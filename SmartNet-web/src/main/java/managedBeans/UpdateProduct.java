/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import CDIBeans.FileUploadInterface;
import Clases.ComponeItems;

import Entities.Compone;
import Entities.ComponePK;
import Entities.Imagen;
import Entities.Producto;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import jaxrs.service.CategoriaFacadeREST;
import jaxrs.service.ComponeFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.InventarioFacadeREST;
import jaxrs.service.ProductoFacadeREST;

import org.primefaces.event.FileUploadEvent;




/**
 *
 * @author luisnegrete
 */
public class UpdateProduct implements Serializable{
    @EJB
    private CategoriaFacadeREST categoriaFacadeREST;
    @EJB
    private InventarioFacadeREST inventarioFacadeREST;
    @EJB
    private ComponeFacadeREST componeFacadeREST;
    
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    @EJB
    private ProductoFacadeREST productoFacadeREST;

    
    
   // @Inject ListComponeLocal listCompone;
    @Inject FileUploadInterface uploadFiles;
    /**
     * Creates a new instance of UpdateProduct
     */
    
    public UpdateProduct() {
    }
   private int idProduct;
  
   private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
   
   private double precio;
   
   private int categoriaId;

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        this.categoriaId = categoriaId;
    }
   
    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
   
   
     public String updateNavigation(){
        
        this.nombre=productoFacadeREST.find(this.idProduct).getNombre();
        this.precio=productoFacadeREST.find(this.idProduct).getPrecio();
      
     //   listCompone.updateList(this.idProduct);
    return "updateProduct";
    }
    
    public String Editar(){
       
     Producto aux=productoFacadeREST.find(this.idProduct);
    aux.setNombre(this.nombre);
    aux.setPrecio(this.precio);
//    aux.setCategoriaIdcategoria(categoriaFacadeREST.find(this.categoriaId));
    
    if(uploadFiles.getPhotoList()!=null && uploadFiles.getPhotoList().size()>0){
    List <Imagen> lista=null; //  imagenFacadeREST.findOne("producto", this.idProduct);
         
    
    for(Imagen a:lista){
    imagenFacadeREST.remove(a);
    }
    
    for(Imagen a:uploadFiles.getPhotoList()){
    Imagen auxi=new Imagen();
    auxi.setExtension(a.getExtension());
    //auxi.setIdComponente(this.idProduct);
    //auxi.setTipo("producto");
    auxi.setImagen(a.getImagen());
    imagenFacadeREST.create(auxi);
    }
   
    
    
    
    }
    
    System.out.print("Lista a eliminar "+productoFacadeREST.find(this.idProduct).getComponeCollection().size());
    
    for(Compone auxi:productoFacadeREST.find(this.idProduct).getComponeCollection()){
        
       
    componeFacadeREST.remove(auxi);
    
    }
    /*
    for(ComponeItems auxi:listCompone.getComponeList()){
    ComponePK componePK=new ComponePK();
    Compone compone=new Compone();
    
    if(auxi.getEnabled())
    {
    componePK.setInventarioIdinventario(auxi.getIdInv());
    componePK.setProductoIdproducto(auxi.getIdPro());
    
    //Compone compone=new Compone();
    compone.setComponePK(componePK);
    
    compone.setCantidadInventario(auxi.getCant());
    compone.setProducto(productoFacadeREST.find(auxi.getIdPro()));
    compone.setInventario(inventarioFacadeREST.find(auxi.getIdInv()));
    }
    else
    {
    componePK.setInventarioIdinventario(auxi.getIdInv());
    componePK.setProductoIdproducto(this.idProduct);
    compone.setComponePK(componePK);
    compone.setCantidadInventario(auxi.getCant());
    compone.setProducto(productoFacadeREST.find(this.idProduct));
    compone.setInventario(inventarioFacadeREST.find(auxi.getIdInv()));
    }
    componeFacadeREST.create(compone);
    }*/
    
    productoFacadeREST.edit(aux);
    return "producto";
    }
     
    public void deletProductInv(){
    System.out.print("delete producto de inv");
    }
    
     public void handleFileUpload(FileUploadEvent event){
//    uploadFiles.addFiles(event.getFile());
    
    }
}
