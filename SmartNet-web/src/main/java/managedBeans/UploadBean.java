/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import Clases.ImageType;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author luisnegrete
 */
@Stateful
@SessionScoped
@LocalBean
@Named(value="uploadBean")
public class UploadBean implements Serializable{
private byte [] foto;

 private String str;

private List <ImageType> photoList;

    public List<ImageType> getPhotoList() {
        return photoList;
    }

    public void setPhotoList(List<ImageType> photoList) {
        this.photoList = photoList;
    }
 
 @PostConstruct//initialized
 private void Initialize(){
 System.out.print("Bean instantiated "+str);
 if (photoList==null){
 photoList=new <ImageType> ArrayList();
 }
 }
 
 @PreDestroy//before destroyig
 private void CleanUp(){
  photoList.clear();
  
 System.out.println("You can do the cleanup here");
}
 


    
    
    private static final long serialVersionUID = 1L;
    private  UploadedFile file;
    
 
    
 
 


    public String upload() {
        
        return "agregarcategoria?faces-redirect=true";
        
    }

 
public void handleFileUpload(FileUploadEvent event){
    


        if (event.getFile() != null) {
        
        try{
        this.file=event.getFile();
        ImageType aux=new ImageType();
        this.str=event.getFile().getFileName();
        aux.setExt(str.substring(str.lastIndexOf('.'), str.length()));
        InputStream fin2 = file.getInputstream();
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        
        while ((bytesRead = fin2.read(buffer)) != -1)
    {
       output.write(buffer, 0, bytesRead);
   }
        
        aux.setArray(new byte[output.toByteArray().length]);
        aux.setArray(output.toByteArray());
        this.photoList.add(aux);
         FacesMessage msg = new FacesMessage("Succesful", file.getFileName()+ " is uploaded. "+(double)(output.toByteArray().length)/1000000+" Megabytes" );
                      
        FacesContext.getCurrentInstance().addMessage(null, msg);

        FacesContext.getCurrentInstance().renderResponse();
        
        System.out.println("Uploaded File Name Is : "+file.getFileName()+" :: Uploaded File Size :: "+file.getSize());
    
        }
        catch(Exception e){
        
        }
        // Print out the information of the file
        
            }
        
        else{
        FacesMessage msg = new FacesMessage("Please select image!!");
                FacesContext.getCurrentInstance().addMessage(null, msg);
        }
}

    public byte[] getFoto() {
        return foto;
    }

  
}
