/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

/**
 *
 * @author luisnegrete
 */




public class UserChatView extends Clases.BaseBacking{

    
private String _username;

String message;

public String getMessage(){

    return message;

}

public void setMessage(String message){

    this.message=message;

}

public String getUsername(){

    return _username;

}

public void setUsername(String username){

    _username=username;

}

private int idFranchise;

private int idSucursal;
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
        
         javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
        
         this.idFranchise=Integer.valueOf(servletRequest.getParameter("idFranchise"));
         
         this.idSucursal=Integer.valueOf(servletRequest.getParameter("idSucursal"));
         
         
         System.out.print("IdFranchise"+this.idFranchise+" IdSucursal "+this.idSucursal);
         
        }
        catch(NullPointerException ex){
                
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }    
        
    }
    
    public void testChat(javax.faces.event.ActionEvent event){
    
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);
    
        org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
        
        String name=session.getAttribute("username").toString();
        
        bus.publish("/user/private/chat/"+name,name+":"+message);      
    }
    
    public void send(javax.faces.event.ActionEvent event){
    
          System.out.print("YOU ARE TRYING TO SEND A MESSAGE");
      
        
        javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getSession();

        
        org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
        
        System.out.print("SENDING MESSAGE TO "+"/chat/"+idFranchise+"/"+idSucursal+"/"+_username);
        
        
        bus.publish("/chat/"+idFranchise+"/"+idSucursal+"/"+_username,session.getAttribute("username").toString()+":"+message);
    
    }
    
    public void updateUsername(javax.faces.event.ActionEvent event){
    
        try{
        
            System.out.print("UPDATE USERNAME CALLED");
            
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
            
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
            
            System.out.print("ACTION EVENT : "+entry.getKey()+"-"+entry.getValue());
            
            }
            
            if(servletRequest.getParameter("username")!=null){
            
                
                System.out.print("ACTION EVENT USERNAME NOT NULL");
                
                this._username=servletRequest.getParameter("username");
            
                System.out.print(_username);
                
            }
            else{
            
                System.out.print("ACTION EVENT USERNAME IS NULL");
            
            }
        
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
