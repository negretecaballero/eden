/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.RatehasPEDIDOControllerInterface;
import Clases.PedidoWorkerInterface;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.json.Json;
import javax.servlet.http.HttpSession;
import org.apache.commons.collections.IteratorUtils;




/**
 *
 * @author luisnegrete
 */
public class UserPedidosView extends Clases.BaseBacking{

  
    
    @Inject RatehasPEDIDOControllerInterface ratehasPEDIDOController;
    
    @javax.inject.Inject @javax.enterprise.inject.Default
    private CDIEden.UserPedidoController _userPedidoController;
    
    private java.util.List<PedidoWorkerInterface>pedidoList;
    
    public java.util.List<PedidoWorkerInterface>getPedidoList(){
    
    return this.pedidoList;
    
    }
    
    public void setPedidoList(java.util.List<PedidoWorkerInterface>pedidoList){
    
    this.pedidoList=pedidoList;
    
    }
    
    public UserPedidosView() {
    }
    
    private int idFranchise;
    
    private int idSucursal;
    
    public int getIdFranchise(){
    
        return this.idFranchise;
    
    }
    
    public void setIdFranchise(int idFranchise){
    
        this.idFranchise=idFranchise;
    
    }
    
    public int getIdSucursal(){
    
        return this.idSucursal;
    
    }
    
    public void setIdSucursal(int idSucursal){
    
        this.idSucursal=idSucursal;
    
    }
    
    @PostConstruct
    public void init(){
    
    try{
    
        HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
  
        
      //pedidoList=pedidoController.loadUserPedidos(_loginAdministradorBean.find(session.getAttribute("username").toString()),"Processing");
    
       System.out.print("This user has "+_userPedidoController.findOrdersByUser(session.getAttribute("username").toString()).size()); 
        
       this.pedidoList=IteratorUtils.toList(_userPedidoController.findOrdersByUser(session.getAttribute("username").toString()).iterator());
       
    }
    catch(Exception ex){
    
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
  
    private void loadPedidoList(){
    
        try{
        
        }
        catch(Exception ex){
        
        
        }
    
    }
    
    public int getDeliveriesToRate(){
    
        HttpSession session=(HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        
        loginBean loginbean=(loginBean)session.getAttribute("loginBean");
        
        return this.ratehasPEDIDOController.shipmentsToRate(loginbean.getLog());
    
    }
    
    public void initChat(Entities.Pedido order){
    
        try{
        
            System.out.print("Order "+order);
            
            this.idFranchise=order.getSucursal().getSucursalPK().getFranquiciaIdfranquicia();
            
            this.idSucursal=order.getSucursal().getSucursalPK().getIdsucursal();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);
             
            /*
            org.primefaces.json.JSONObject value=new org.primefaces.json.JSONObject();
    
            value.put("title", session.getAttribute("username").toString());
    
            value.put("content", "Do you wanna start a conversation with this user?");
    
            value.put("username",session.getAttribute("username").toString());
            */
                    
            org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
            
            String username=session.getAttribute("username").toString();
            
            System.out.print("/initChat/"+order.getSucursal().getSucursalPK().getFranquiciaIdfranquicia()+"/"+order.getSucursal().getSucursalPK().getIdsucursal());
            
            bus.publish(java.io.File.separator+"initChat"+java.io.File.separator+order.getSucursal().getSucursalPK().getFranquiciaIdfranquicia()+java.io.File.separator+order.getSucursal().getSucursalPK().getIdsucursal(), username+":"+username+" want to chat, init chat?:"+username);
         
            javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
            
            nh.handleNavigation(this.getContext(),null,"inChat");
            
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
           
        }
    
    }
}
