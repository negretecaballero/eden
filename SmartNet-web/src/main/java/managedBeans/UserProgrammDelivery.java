/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.EdenDate;
import Clases.EdenDateInterface;
import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;

/**
 *
 * @author luisnegrete
 */
public class UserProgrammDelivery {

    /**
     * Creates a new instance of UserProgrammDelivery
     */
    public UserProgrammDelivery() {
    }
    
    @PostConstruct
    public void init(){
    
     this.date=new EdenDate();   
    
    }
    
    private EdenDateInterface date;
    
    public EdenDateInterface getDate(){
    
       return this.date;
    
    }
    
    public void setDate(EdenDateInterface date){
    
    this.date=date;
    
    }
    
    
    public void checkTime(ComponentSystemEvent event){
    
        try{
            
            System.out.print("You are post validating");
        
            UIComponent component=event.getComponent();
            
           
            UIInput hour=(UIInput)component.findComponent("hourSpinner");
            
            UIInput minutes=(UIInput)component.findComponent("minutesSpinner");
            
            System.out.print("Hour "+hour.getLocalValue().toString());
            
            System.out.print("MInutes "+minutes.getLocalValue().toString());
            
        }
        catch(Exception ex){
        
        
        }
    
    }
    
    public void programmDelivery(ActionEvent event){
    
        try{
        
           System.out.print("Action Event event");
        
        }
        catch(Exception ex){
        
        
        }
    
    }
}
