/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;


import CDIBeans.FileUploadInterface;
import Clases.ActionType;
import Clases.ActionTypeInterface;
import Clases.BaseBacking;
import EdenFinalUser.Classes.ResultsType;
import EdenFinalUser.Classes.ResultsTypeInterface;

import Entities.Categoria;
import Entities.Franquicia;
import Entities.Imagen;
import Entities.Menu;
import Entities.Producto;
import Entities.Sucursal;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.CategoriaFacadeREST;
import jaxrs.service.ImagenFacadeREST;
import jaxrs.service.SucursalFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;

import org.primefaces.event.FileUploadEvent;
import java.io.*;




/**
 *
 * @author luisnegrete
 */

@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin"})

public class commandlinks extends BaseBacking implements Serializable{
 
    private String auxiliar="auxiliarComponent";
    
    @EJB
    private CategoriaFacadeREST categoriaFacade;
    
    @EJB
    private ImagenFacadeREST imagenFacadeREST;
    
    @EJB
    private SucursalFacadeREST sucursalFacadeREST;

    @Inject private FileUploadInterface fileUpload;
    
    public String getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(String categoriaId) {
        this.categoriaId = categoriaId;
    }
    
   
    private String categoriaId;
    
    
    private String NombreCat;

    public String getNombreCat() {
     return null;
        //return categoriaFacadeREST.find(this.Selectedcategoria).getNombre();
    }

    public void setNombreCat(String NombreCat) {
        this.NombreCat = NombreCat;
    }
    
    public commandlinks() {
        
        if(getSession().getAttribute(auxiliar) instanceof Categoria){
            
    this.NombreCat=((Categoria)getSession().getAttribute("auxiliarComponent")).getNombre();
    
            
    }
    }
    
    public String setAuxiliarComponent(Object obj){

        try{
            

            System.out.print("You are in command links");
            
            if(obj instanceof Categoria){
            
          javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
                
          this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
          
            Categoria cat=(Categoria)obj;
            
            
            System.out.print("Category Name "+cat.getNombre());
            
            getSession().setAttribute(auxiliar, cat);
            
            
            
            for(Imagen image:cat.getImagenCollection()){
            
                fileUpload.loadImagesServletContext(image,getSession().getAttribute("username").toString());
                
            }
            
            return "success";
            
        }
        
        
        
        if(obj instanceof Producto){
                
            Producto product=(Producto)obj;
            
            System.out.print("You have selected a product "+product.getNombre());
            
            
            getSession().setAttribute(auxiliar, product);
                 
            this.setAuxiliarComponent(product);
  
            ActionTypeInterface action=new ActionType("editProduct");
            
                    
            this.fileUpload.setActionType(action);
            
            
            System.out.print(product.getNombre()+" has "+product.getImagenCollection().size()+" images");
    
            
            return "success";
        
        }
        
        if(obj instanceof Entities.Sale){
        
            this.setAuxiliarComponent(obj);
            
            return "success";
        
        }
        
        if(obj instanceof Entities.Inventario){
        
        setSessionAuxiliarComponent(obj);
        
        return "success";
        
        }
        
        
        if(obj instanceof Entities.Addition){
        
            setSessionAuxiliarComponent(obj);
                
            return "success";
        
        }
        
        if(obj instanceof Menu){
        
            Menu menu=(Menu)obj;
            
            setSessionAuxiliarComponent(menu);
            
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
           
           this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
           
            ActionTypeInterface actionType=new ActionType("updateMenu");
            
            this.fileUpload.setActionType(actionType);
            
            for(Imagen image:menu.getImagenCollection()){
            
            this.fileUpload.loadImagesServletContext(image, getSession().getAttribute("username").toString());
            
            }
            
           return "success"; 
        
        }
        
        if(obj instanceof ResultsType){
        
            System.out.print("OBJ is instance of ResultsType");
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
            
            String path=servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString();
            
            this.fileUpload.Remove(path);
            
            ResultsTypeInterface resultsType=(ResultsType)obj;
            
            System.out.print(resultsType);
            
            PathSegment ps=new PathSegmentImpl("bar;idsucursal="+resultsType.getKey().getIdsucursal()+";franquiciaIdfranquicia="+resultsType.getKey().getFranquiciaIdfranquicia()+"");
            
            Sucursal suc=sucursalFacadeREST.find(ps);
            
            System.out.print(suc);
            
            for(Imagen image:suc.getImagenCollection()){
            
            
            this.fileUpload.loadImagesServletContext(image,getSession().getAttribute("username").toString());
            
            
            setSessionAuxiliarComponent(suc);
            
            }
            
            
        return "sucursal?faces-redirect=true";
        }
        
        if(obj instanceof Entities.InventarioHasSucursal){
        
            this.setSessionAuxiliarComponent(obj);
            
            return "success";
        
        }
        
        return "failure";
        
    }catch(StackOverflowError ex){
        
        Logger.getLogger(commandlinks.class.getName()).log(Level.SEVERE,null,ex);
        
        
        
        return "failure";

}
        

}
    
    @PostConstruct
    public void Initialize(){
        
    System.out.print("commandlinks created");
      
    }
    
   
    public void handleUpload(FileUploadEvent event){
   String path="";
   
   String username="";
   
   try{
       
  
   if(getSession().getAttribute("username")!=null){
   username=(String)getSession().getAttribute("username");
   }
   else{
       
   throw new Exception();
   
   }
   
   if(getSession().getAttribute("selectedOption") instanceof Sucursal){
   path="/Users/luisnegrete/Copy/ProyectoLuis/TuBusinet/TuBusinet-war/web/AdminSucursal/imagesCache/"+username+"/";
   }
   
   else if(getSession().getAttribute("selectedOption") instanceof Franquicia){
   path="/Users/luisnegrete/Copy/ProyectoLuis/TuBusinet/TuBusinet-war/web/adminFranquicia/imagesCache/"+username+"/";
  
   }
   else
   {
   throw new Exception();
   }
   
   fileUpload.addFiles(event.getFile(), path,500,500);
   
   
   
    }
   
   catch(Exception ex){
   FacesMessage msg=new FacesMessage(FacesMessage.SEVERITY_WARN+"It is not posible to upload the file");
   
   getContext().addMessage(null,msg);
   
   Logger.getLogger(commandlinks.class.getName()).log(Level.SEVERE,null,ex);
   
   }
   
    }
    
    

    public Categoria getCCategoria(){
    //return categoriaFacadeREST.find(this.categoriaID);
    return null;
    }
    
//      public void actionSubmit(ActionEvent event) {
//	
//        FacesMessage msg = new FacesMessage(String.valueOf(categoriaID));
//               FacesContext.getCurrentInstance().addMessage(null, msg);
//               System.out.println(String.valueOf(categoriaID));
//               Categoria aux=new Categoria();
//                  aux=categoriaFacade.find(categoriaID);
//             categoriaFacade.remove(aux);
//}
    
    public String retorno(){
     
        
      //  FacesMessage msg = new FacesMessage(String.valueOf(categoriaID));
        //        FacesContext.getCurrentInstance().addMessage(null, msg);
          //      System.out.println(categoriaID);
    return "updatecategory";
    }
    
    public void deleteCategory(ActionEvent event){
        System.out.print("entro");
        
         //FacesMessage msg = new FacesMessage(String.valueOf(categoriaID));
           //     FacesContext.getCurrentInstance().addMessage(null, msg);
             //   System.out.println(categoriaID);
        
    //Categoria aux=categoriaFacadeREST.find(categoriaID);
   
// List<Imagen> auxi=imagenFacadeREST.findOne("categoria", aux.getIdcategoria());
   // if(auxi!=null){
       // for(Imagen a:auxi){
   // imagenFacadeREST.remove(a);
        //}
    
   // }
    //categoriaFacadeREST.remove(aux);
    
    

    }
    
    public void actionListener(){
    
    System.out.print("ACTION LISTENER COMMAND LINKS CALLED");
    
    //fileUpload.setActionType(null);
    
    }
    
}

   