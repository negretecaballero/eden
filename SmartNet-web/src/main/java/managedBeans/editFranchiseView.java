/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import Clases.BaseBacking;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.inject.Inject;

/**
 *
 * @author luisnegrete
 */

@RolesAllowed("GuanabaraFrAdmin")
public class editFranchiseView extends BaseBacking{

    /**
     * Creates a new instance of editFranchiseView
     */
    
    @Inject @Default private CDIBeans.FranchiseTypeControllerInterface franchiseTypeController;
    
    @Inject @Default private CDIBeans.FileUploadInterface fileUpload;
    
    @Inject @Default private CDIBeans.FranchiseControllerInterface franchiseController;
    
    
    
    private Entities.Subtype idType;
    
    private String logo;
    
    private Entities.Franquicia franchise;
    
    public String getLogo(){
    
    return this.logo;
    
    }
    
    public void setLogo(String logo){
    
    this.logo=logo;
    
    }
    
    public Entities.Subtype getIdType(){
    
    return this.idType;
    
    }
    
    public void setIdType(Entities.Subtype idType){
    
        this.idType=idType;
    
    }
    
    public editFranchiseView() {
    }
    
    public java.util.List<Entities.Tipo>getType(){
    
    try{
    
        return this.franchiseTypeController.findAll();
    
    }
    catch(EJBException ex){
    
        return null;
    
    }
    
    }
    
    @javax.annotation.PostConstruct 
    public void init(){
    
        try{
            
        javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
        
       
     
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
      
      System.out.print("Object is of type franquicia");
      
       franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
       
        if(!this.fileUpload.getClassType().equals(editFranchiseView.class.getName()))
        {
            this.fileUpload.setClassType(editFranchiseView.class.getName());
               this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username").toString());
          this.logo=this.franchiseController.getLogo(franchise);
       
        }
        else{
        
        if(this.fileUpload.getImages()!=null && !this.fileUpload.getImages().isEmpty()){
        
        this.logo=this.fileUpload.getImages().get(0);
        
        
        }
        
        }
      this.idType=franchise.getIdSubtype();
      
     
      
      }
      
      else{
      
          throw new IllegalArgumentException("Object of type "+getSession().getAttribute("selectedOption").getClass().getName()+" must be of type "+Entities.Franquicia.class.getName());
      
      }
      
      
      System.out.print("Image Name "+this.logo);
    
    }
        catch(Exception ex){
        
        
        
        }
    }
    
    public void handleFileUpload(org.primefaces.event.FileUploadEvent event){
    
        try{
            
            javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
        
            this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
        
            this.fileUpload.servletContainer(event.getFile(), getSession().getAttribute("username").toString(), 500, 500);
            
            this.logo=this.fileUpload.getImages().get(0);
            
            System.out.printf("logo value ", this.logo);
        
        }
        catch(Exception ex){
        
        Logger.getLogger(editFranchiseView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    
    public void editFranchise(javax.faces.event.ActionEvent event){
    
        try{
            
            System.out.print("Event ID "+event.getComponent().getClientId());
        
            if(event.getComponent().getClientId().equals("form:edit")){
            
            if(this.fileUpload.getPhotoList()!=null && !this.fileUpload.getPhotoList().isEmpty()){
            
            this.franchise.setImagenIdimagen(this.fileUpload.getPhotoList().get(0));
            
             
            this.franchiseController.edit(franchise);
            
            javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Franchise edited succesfully");
            
            getContext().addMessage(event.getComponent().getClientId(),msg);
            
            
            }
            }
             
            else{
            
                throw new FacesException("Exception Caugth");
            
            }
        
            
        }
        catch(Exception ex){
        
            Logger.getLogger(editFranchiseView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
}
