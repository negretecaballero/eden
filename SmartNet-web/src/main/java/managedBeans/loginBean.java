/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package managedBeans;

import Clases.BaseBacking;
import Clases.FilesManagement;
import Clases.FilesManagementInterface;
import Clases.SelectionLogin;
import Entities.AppGroup;
import Entities.Confirmation;
import Entities.LoginAdministrador;
import Entities.MenuhasPEDIDO;
import Entities.ProductohasPEDIDO;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.ServletException;
import jaxrs.service.AppGroupFacadeREST;
import jaxrs.service.ConfirmationFacadeREST;
import jaxrs.service.DireccionFacadeREST;
import jaxrs.service.GpsCoordinatesFacadeREST;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.MenuhasPEDIDOFacadeREST;
import jaxrs.service.PedidoFacadeREST;
import jaxrs.service.ProductohasPEDIDOFacadeREST;

import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;





/**
 *
 * @author luisnegrete
 */
@RolesAllowed({"GuanabaraSucAdmin", "GuanabaraFrAdmin", "Guanabarauser", "GuanabaraWorker", "GuanabaraSupplier"})
@javax.enterprise.context.SessionScoped
@javax.inject.Named("loginBean")
//@javax.enterprise.inject.Alternative

public class loginBean extends BaseBacking implements java.io.Serializable{
    
    private List<SelectionLogin>selectionLogin;
    
    private boolean confirmation;
    
    private boolean isCompleted;
    
    @javax.inject.Inject 
    @CDIBeans.FileUploadBeanQualifier
    private transient CDIBeans.FileUploadInterface fileUploadBean;
    
    @javax.inject.Inject  
    @CDIBeans.LoginAdministradorControllerQualifier
    private  CDIBeans.LoginAdministradorControllerInterface _loginAdministradorController;
    
    @javax.inject.Inject 
    @CDIBeans.ConfirmationControllerQualifier
    private transient CDIBeans.ConfirmationDelegate _confirmationController;
    
    @javax.inject.Inject 
    @CDIBeans.AppGroupControllerQualifier
    private  CDIBeans.AppGroupControllerDelegate _appGroupController;
    
    public boolean getIsCompleted(){
    
    return this.isCompleted;
    
    }
    
    public void setIsCompleted(boolean isCompleted){
    
        this.isCompleted=isCompleted;
    
    }
    
    public boolean getConfirmation(){
    
    return this.confirmation;
    
    }
    
    public void setConfirmation(boolean confirmation){
    
        this.confirmation=confirmation;
    
    }
    
    public List<SelectionLogin>getSelectionLogin(){
    
    
    return this.selectionLogin;
    
    }
    
    public void setSelectionLogin(List<SelectionLogin>selectionLogin){
    
        this.selectionLogin=selectionLogin;
    
    }
    
    private String str;
    
    public void setStr(String str){
    
        if(str==null){
        
            str="Hi Smart";
        
        }
        
    this.str=str;
    
    }
    
    public String getStr(){
    
        return this.str;
    
    }
    
    @EJB
    private PedidoFacadeREST pedidoFacadeREST;
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorREST;
    
    @EJB
    private ConfirmationFacadeREST confirmationFacadeREST;
    
    @EJB
    private AppGroupFacadeREST appgroupFacadeREST;
    
/*    @EJB 
    private SucursalhasloginAdministradorFacadeREST sucursalhasloginAdministradorFacadeREST;*/
    
    @EJB
    private DireccionFacadeREST direccionFacadeREST;
    
    @EJB
    private GpsCoordinatesFacadeREST gpsCoordinatesFacadeREST;
    
    @EJB
    private MenuhasPEDIDOFacadeREST menuhasPEDIDOFacadeREST;
    
    @EJB
    private ProductohasPEDIDOFacadeREST productohasPEDIDOFacadeREST;

    private boolean loaded;
    
    public LoginAdministrador getLog() {
        return log;
    }

   
    public void setLog(LoginAdministrador log) {
        this.log = log;
    }
  
  private  LoginAdministrador log;

  public void loadUser(ComponentSystemEvent event){
       
  try{
          
  System.out.print(getRequest().getUserPrincipal().getName());
  
  LoginAdministrador aux=loginAdministradorREST.find(getRequest().getUserPrincipal().getName());

  log=aux;
  
  getSession().setAttribute("username", aux.getUsername());
  
//  List<SucursalhasloginAdministrador> sucursalesList=sucursalhasloginAdministradorFacadeREST.findByUsername("gunabara_sucursal_administrador", aux.getUsername());
  
  List<Entities.Sucursal>sucList=new <Entities.Sucursal>ArrayList();
  
  /*for(SucursalhasloginAdministrador auxi: sucursalesList)
  {
  sucList.add(auxi.getSucursal());
  }*/
  
  getSession().setAttribute("sucursales", sucList);
  
  System.out.print(sucList.size());
  
  getSession().setAttribute("franquicias", aux.getFranquiciaCollection());
  
  System.out.print(aux.getFranquiciaCollection().size());

}

catch(NullPointerException ex){
    
    Logger.getLogger(loginBean.class.getName()).log(Level.SEVERE,null,ex);
    getContext().addMessage(null, new FacesMessage("ERROR DE SISTEMA"));   
    
}     
   
}
   
   public String selection(){
        
       try{
   //System.out.print("Account completion coefficient "+this.loginAdministradorController.accountCompletion(getRequest().getUserPrincipal().getName()));
     
          Clases.EdenXML.exampleParse();
           
          String key=Clases.EdenString.generatePassword(45);
           
          String loginUsername=("LOGIN USERNAME "+this.getRequest().getUserPrincipal().getName());

          SessionClasses.EdenList<Object>list=new SessionClasses.EdenList<Object>();
           
          list.addItem("Luis");
           
          list.addItem("David");
           
          System.out.print("LIST SIZE "+list.size());
           
          //Testing Recursive class
          System.out.print("KEY GENERATION "+key+" size "+key.length());
           
          System.out.print( recursion(2));
          
          System.out.print("USER PRINCIPAL "+this.getRequest().getUserPrincipal().getName());
           
          if(this._loginAdministradorController!=null){
          
  if(_loginAdministradorController.accountCompletion(getRequest().getUserPrincipal().getName())>=0.5){
  
      this.isCompleted=true;
  
  }         
  else{
  
      this.isCompleted=false;
  
  }
   
       }
       
       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
              
       FilesManagementInterface fileManagement=new FilesManagement();    
      
       fileManagement.createFolder(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator,getRequest().getUserPrincipal().getName());
       
       System.out.print("STR VALUE "+this.str);
           
       this.log=this._loginAdministradorController.find(getRequest().getUserPrincipal().getName());
       
       System.out.print("USERNAME "+this.log.getName());
       
       if(_confirmationController.findUsernameConfirmed(this.log.getUsername(), true)!=null){
       
       System.out.print("THIS ACCOUNT IS CONFIRMED");
           
       getSession().setAttribute("username", this.log.getUsername());
       
       System.out.print("Login Username "+getRequest().getUserPrincipal().getName());

       Entities.AppGroupPK psuser=new Entities.AppGroupPK();
       
       psuser.setGroupid("guanabara_user");
       
       psuser.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());  
         
       Entities.AppGroupPK psfranchise=new Entities.AppGroupPK();
               
       psfranchise.setGroupid("guanabara_franquicia_administrador");
       
       psfranchise.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());
        
       Entities.AppGroupPK pssuc=new Entities.AppGroupPK();
       
       pssuc.setGroupid("guanabara_sucursal_administrador");
       
       pssuc.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());
          
       Entities.AppGroupPK pswor=new Entities.AppGroupPK();
       
       pswor.setGroupid("guanabara_worker");
       
       pswor.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());
          
       Entities.AppGroupPK psedenworker=new Entities.AppGroupPK();
       
       psedenworker.setGroupid("eden_worker");
       
       psedenworker.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());
         
       Entities.AppGroupPK psedensupplier=new Entities.AppGroupPK();
       
       psedensupplier.setGroupid("guanabara_supplier");
       
       psedensupplier.setLoginAdministradorusername(this.getRequest().getUserPrincipal().getName());
       
       System.out.print("YOU HAVE "+_loginAdministradorController.find(this.getRequest().getUserPrincipal().getName()).getAppGroupCollection().size()+" PROFILES");
       
       
       //Just One Profile
       if(this._loginAdministradorController.find(getRequest().getUserPrincipal().getName()).getAppGroupCollection()!=null && this._loginAdministradorController.find(getContext().getExternalContext().getUserPrincipal().getName()).getAppGroupCollection().size()==1)
       {
       
       if(!loaded){
       
         if(this.selectionLogin==null){
           
               this.selectionLogin=new <SelectionLogin>ArrayList();
           
           }
         else{
         
             this.selectionLogin.clear();
         
         }
         
         
            if(_appGroupController.find(pswor)!=null){
       
       System.out.print("You are a worker");
       
       
           
       loaded=true;
       
       
        SelectionLogin aux=new SelectionLogin();
               
                   aux.setText("/images/login/worker.jpg");
                   
                   aux.setResult("worker");
                   
                   this.selectionLogin.add(aux);  
       
           return "Worker";
       
       }
           
       
       
       
       
      else if(_appGroupController.find(psuser)!=null){
       
       System.out.print("You are an user");
           
       AppGroup appGroup= _appGroupController.find(psuser);
      
         SelectionLogin aux=new SelectionLogin();
               
         aux.setText("/images/login/user.png");
               
         aux.setResult("user");
               
         this.selectionLogin.add(aux);  
       
       Confirmation confirmationn=_confirmationController.find(getRequest().getUserPrincipal().getName());
      
       this.confirmation=confirmationn.getConfirmed();
       
       loaded=true;
       
       return "User";
       
       }
      
      else if(_appGroupController.find(psedenworker)!=null){
      
              SelectionLogin aux=new SelectionLogin();
                   
                   aux.setText("/images/login/eden.png");
                   
                   aux.setResult("edenWorker");
                   
                   this.selectionLogin.add(aux);
          
                   return"EdenWorker";
      
      }
       
      else if(_appGroupController.find(psfranchise)!=null){
       
       System.out.print("You are a Franchise Administrator");
       
        SelectionLogin aux=new SelectionLogin();
               
               aux.setText("/images/login/Administrator.jpg");
                   
               aux.setResult("administrator");
                   
               this.selectionLogin.add(aux);  
       
       return "FranchiseAdministrator";
       
       }
            
      else if(_appGroupController.find(pssuc)!=null){
      
          System.out.print("You are a Sucursal Administrator");
          
            SelectionLogin aux=new SelectionLogin();
               
               aux.setText("/images/login/subadministrator.png");
               
               aux.setResult("sucAdministrador");
               
               this.selectionLogin.add(aux);  
          
          return "SucAdministrator";
      
      }
       
      else if(_appGroupController.find(psedensupplier)!=null){
      
                   SelectionLogin aux=new SelectionLogin();
                   
                    aux.setText("/images/supplier.png");
                   
                   aux.setResult("GuanabaraSupplier");
                   
                   this.selectionLogin.add(aux);
          
      return "GuanabaraSupplier";
      
      }
     
       } 
   }
       
       
       //More Than One Profile
       else if(_loginAdministradorController.find(getRequest().getUserPrincipal().getName())!=null && _loginAdministradorController.find(getRequest().getUserPrincipal().getName()).getAppGroupCollection()!=null && _loginAdministradorController.find(getRequest().getUserPrincipal().getName()).getAppGroupCollection().size()>1)
       {
       
       if(!loaded)
       {

           System.out.print("You have "+appgroupFacadeREST.usernameCounter(getContext().getExternalContext().getUserPrincipal().getName())+" profiles");
       
     
           if(this.selectionLogin==null){
           
               this.selectionLogin=new <SelectionLogin>ArrayList();
           
           }
           
           else{
           
               this.selectionLogin.clear();
           
           }
           
           for(AppGroup appGroup:_appGroupController.findByUsername(getRequest().getUserPrincipal().getName()))
           {
           
               System.out.print(appGroup.getAppGroupPK().getLoginAdministradorusername()+"-"+appGroup.getAppGroupPK().getGroupid());

               if(appGroup.getAppGroupPK().getGroupid().equals("guanabara_user")){
                   
               SelectionLogin aux=new SelectionLogin();
               
               aux.setText("/images/login/user.png");
               
               aux.setResult("user");
               
               this.selectionLogin.add(aux);  
               
               }
               
               else if(appGroup.getAppGroupPK().getGroupid().equals("guanabara_franquicia_administrador")){
                   
               SelectionLogin aux=new SelectionLogin();
               
               aux.setText("/images/login/Administrator.jpg");
                   
               aux.setResult("administrator");
                   
               this.selectionLogin.add(aux);  
               
               }
               
               else if(appGroup.getAppGroupPK().getGroupid().equals("guanabara_sucursal_administrador")){
                   
               SelectionLogin aux=new SelectionLogin();
               
               aux.setText("/images/login/subadministrator.png");
               
               aux.setResult("sucAdministrador");
               
                this.selectionLogin.add(aux);  
               
               }
               
               else if(appGroup.getAppGroupPK().getGroupid().equals("guanabara_worker")){
                   
                   SelectionLogin aux=new SelectionLogin();
               
                   aux.setText("/images/login/worker.jpg");
                   
                   aux.setResult("worker");
                   
                   this.selectionLogin.add(aux);  
                  
               }
               
               else if(appGroup.getAppGroupPK().getGroupid().equals("eden_worker")){
               
                   SelectionLogin aux=new SelectionLogin();
                   
                   aux.setText("/images/login/eden.png");
                   
                   aux.setResult("edenWorker");
                   
                   this.selectionLogin.add(aux);
                   
               
               }
               
               else if(appGroup.getAppGroupPK().getGroupid().equals("guanabara_supplier")){
               
               
                   SelectionLogin aux=new SelectionLogin();
                   
                    aux.setText("/images/supplier.png");
                   
                   aux.setResult("GuanabaraSupplier");
                   
                   this.selectionLogin.add(aux);
               
               }
               
          
           }
           
       loaded=true;
       
       System.out.print("Selection Login Size "+this.selectionLogin.size());
       
           return "selection";
       
       
       
      }
   }
       
   }  
       else{
       
           System.out.print("THIS ACCOUNT HAS NOT BEEN CONFIRMED");
           
       return"confirmation";
       
       }
       
       loaded=true;
       
   return "failure";
   }
       catch(NullPointerException ex){
       
           Logger.getLogger(loginBean.class.getName()).log(Level.SEVERE,null,ex);
           
         
             return "failure";
       }
   }

    
    
    public loginBean() {
        
        loaded=false;
        
    }
    
     public String  cerrarSesion(){
  
        
      try{

       javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
        
        this.fileUploadBean.Remove(servletContext.getRealPath("")+java.io.File.separator+"resources"+java.io.File.separator+"demo"+java.io.File.separator+"images"+java.io.File.separator+this.log.getUsername());
        
        getRequest().logout();
        
        return "/login.xhtml?faces-redirect=true";
      }
      
      catch(ServletException ex){
          
      Logger.getLogger(loginBean.class.getName()).log(Level.SEVERE,null,ex);
      
      return null;
      
      }
        
   
    }
    
    public void deleteLogin(LoginAdministrador login){
    
        try{
        
     for(Entities.Pedido pedido:login.getPedidoCollection()){
     
     Entities.Direccion address=pedido.getDireccion();
     
     Entities.GpsCoordinates gpsCoordinates=address.getGpsCoordinatesidgpsCoordinated();
     
     gpsCoordinatesFacadeREST.remove(gpsCoordinates.getIdgpsCoordinated());
     
     direccionFacadeREST.remove(new PathSegmentImpl("bar;idDireccion="+address.getDireccionPK().getIdDireccion()+";cITYidCITY="+address.getDireccionPK().getCITYidCITY()+";cITYSTATEidSTATE="+address.getDireccionPK().getCITYSTATEidSTATE()+""));
     
     
     for(MenuhasPEDIDO menuhasPEDIDO:pedido.getMenuhasPEDIDOCollection()){
     
       menuhasPEDIDOFacadeREST.remove(new PathSegmentImpl("bar;menuIdmenu="+menuhasPEDIDO.getMenuhasPEDIDOPK().getMenuIdmenu()+";pEDIDOidPEDIDO="+menuhasPEDIDO.getMenuhasPEDIDOPK().getPEDIDOidPEDIDO()+""));
     
     }
     
     for(ProductohasPEDIDO productohasPEDIDO:pedido.getProductohasPEDIDOCollection()){
     
         productohasPEDIDOFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+productohasPEDIDO.getProductohasPEDIDOPK().getProductoIdproducto()+";pEDIDOidPEDIDO="+productohasPEDIDO.getProductohasPEDIDOPK().getPEDIDOidPEDIDO()+""));
     
     }
     
    pedidoFacadeREST.remove(pedido.getIdPEDIDO());
     
     }     
         loginAdministradorREST.remove(login.getUsername());
        }
        catch(EJBException ex){
        
        Logger.getLogger(loginBean.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void deleteLoginAppGroup(LoginAdministrador login,String pathId){
    
    try{
    
        AppGroup appGroup=appgroupFacadeREST.find(new PathSegmentImpl("bar;groupid="+pathId+";loginAdministradorusername="+login.getUsername()+""));
    
        
     for(Entities.Pedido pedido:login.getPedidoCollection()){
        
     Entities.Direccion address=pedido.getDireccion();
     
     Entities.GpsCoordinates gpsCoordinates=address.getGpsCoordinatesidgpsCoordinated();
     
     gpsCoordinatesFacadeREST.remove(gpsCoordinates.getIdgpsCoordinated());
     
     direccionFacadeREST.remove(new PathSegmentImpl("bar;idDireccion="+address.getDireccionPK().getIdDireccion()+";cITYidCITY="+address.getDireccionPK().getCITYidCITY()+";cITYSTATEidSTATE="+address.getDireccionPK().getCITYSTATEidSTATE()+""));
     
     
     for(MenuhasPEDIDO menuhasPEDIDO:pedido.getMenuhasPEDIDOCollection()){
     
       menuhasPEDIDOFacadeREST.remove(new PathSegmentImpl("bar;menuIdmenu="+menuhasPEDIDO.getMenuhasPEDIDOPK().getMenuIdmenu()+";pEDIDOidPEDIDO="+menuhasPEDIDO.getMenuhasPEDIDOPK().getPEDIDOidPEDIDO()+""));
     
     }
     
     for(ProductohasPEDIDO productohasPEDIDO:pedido.getProductohasPEDIDOCollection()){
     
         productohasPEDIDOFacadeREST.remove(new PathSegmentImpl("bar;productoIdproducto="+productohasPEDIDO.getProductohasPEDIDOPK().getProductoIdproducto()+";pEDIDOidPEDIDO="+productohasPEDIDO.getProductohasPEDIDOPK().getPEDIDOidPEDIDO()+""));
     
     }
     
    pedidoFacadeREST.remove(pedido.getIdPEDIDO());
        
        }
        
        appgroupFacadeREST.remove(new PathSegmentImpl("bar;groupid="+pathId+";loginAdministradorusername="+login.getUsername()+""));
        
        
        
    }
    catch(EJBException ex){
        
        Logger.getLogger(loginBean.class.getName()).log(Level.SEVERE,null,ex);
    
    }
    
    
    }
    
    public void redirect(SelectionLogin loginSelection){
    
      try{
          
          System.out.print("You have clicked the image "+loginSelection.getText());
      
          if(loginSelection instanceof SelectionLogin){
          
              System.out.print("Object is of type "+loginSelection.getClass().getName());
              
              NavigationHandler nh=getContext().getApplication().getNavigationHandler();
              
              nh.handleNavigation(getContext(), null, loginSelection.getResult());
          
          }
          else{
          
              throw new IllegalArgumentException("Object of type "+loginSelection.getClass().getName()+" must be of type "+SelectionLogin.class.getName());
          
          }
      
      }
      catch(Exception ex){
      
      
      
      }
    
    }
    
    
   private int recursion(int a){
   
       int n=a*2;
       
   if(n<100){
   
       System.out.print(n);
       
       n=recursion(n);

   }
   return n;
   } 
   
   private String _franchiseType;
   
   public String getFranchiseType(){
   
   return _franchiseType;
   
   }
   
   
   public String retrieveFranchiseType(){
   
   return _franchiseType;
   
   }
   
   public void setFranchiseType(String franchiseType){
   
       _franchiseType=franchiseType;
   
   }
   
}
