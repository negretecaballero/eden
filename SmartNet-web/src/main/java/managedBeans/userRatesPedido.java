/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import CDIBeans.RatehasPEDIDOControllerInterface;
import Clases.BaseBacking;
import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.component.UIParameter;
import javax.inject.Inject;
import org.primefaces.event.RateEvent;

/**
 *
 * @author luisnegrete
 */

@RolesAllowed("GuanabaraUser")
public class userRatesPedido extends BaseBacking{

   
    /**
     * Creates a new instance of userRatesPedido
     */
    public userRatesPedido() {
    }
    
    @Inject private RatehasPEDIDOControllerInterface ratehasPEDIDOController;
    
    java.util.List<Clases.UserRateInterface>pedidoRateList;
    
    public java.util.List<Clases.UserRateInterface>getPedidoRateList(){
    
    return this.pedidoRateList;
    
    }
    
    public void setPedidoRateList(java.util.List<Clases.UserRateInterface>pedidoRateList){
    
    this.pedidoRateList=pedidoRateList;
    
    }
    
    @PostConstruct 
    public void init(){
    
        this.pedidoRateList=new java.util.ArrayList<Clases.UserRateInterface>();
        
        loginBean loginbean=(loginBean)getSession().getAttribute("loginBean");
        
        this.pedidoRateList=ratehasPEDIDOController.unratedPedidos(loginbean.getLog());
    
    }
    

    
    public void onRate(RateEvent event){
        
        int idPedido=0;
    
        System.out.print(((Short)event.getRating()).shortValue());
        
        UIComponent component=event.getComponent();
        
        for(UIComponent aux:component.getChildren()){
        
        if(aux instanceof UIParameter){
        
         UIParameter parameter=(UIParameter)aux;
         
         if(parameter.getName().equals("selectedPedido")){
         
         System.out.print(parameter.getName()+"-"+parameter.getValue().toString());
         
         idPedido=Integer.parseInt(parameter.getValue().toString());
         
         ratehasPEDIDOController.updateRate(idPedido, ((Short) event.getRating()).shortValue());
         
         
         System.out.print("Rate has pedido updated");
         }
         else{
         
         throw new FacesException("You are hacking bitch");
         
         }
        
        }
        
        
        }
    
    }
}
