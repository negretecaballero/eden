/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.netbeans.rest.application.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author luisnegrete
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(jaxrs.service.AdditionConsumesInventarioFacadeREST.class);
        resources.add(jaxrs.service.AdditionFacadeREST.class);
        resources.add(jaxrs.service.AgrupaFacadeREST.class);
        resources.add(jaxrs.service.AppGroupFacadeREST.class);
        resources.add(jaxrs.service.ApplicationFacadeREST.class);
        resources.add(jaxrs.service.ApplicationStatusFacadeREST.class);
        resources.add(jaxrs.service.BarcodeFacadeREST.class);
        resources.add(jaxrs.service.BugFacadeREST.class);
        resources.add(jaxrs.service.CategoriaFacadeREST.class);
        resources.add(jaxrs.service.CityFacadeREST.class);
        resources.add(jaxrs.service.ComponeFacadeREST.class);
        resources.add(jaxrs.service.ConfirmationFacadeREST.class);
        resources.add(jaxrs.service.DayFacadeREST.class);
        resources.add(jaxrs.service.DireccionFacadeREST.class);
        resources.add(jaxrs.service.FranquiciaFacadeREST.class);
        resources.add(jaxrs.service.GpsCoordinatesFacadeREST.class);
        resources.add(jaxrs.service.ImagenFacadeREST.class);
        resources.add(jaxrs.service.InventarioFacadeREST.class);
        resources.add(jaxrs.service.InventarioHasSucursalFacadeREST.class);
        resources.add(jaxrs.service.LastSeenMenuFacadeREST.class);
        resources.add(jaxrs.service.LastSeenProductoFacadeREST.class);
        resources.add(jaxrs.service.LoginAdministradorFacadeREST.class);
        resources.add(jaxrs.service.MenuFacadeREST.class);
        resources.add(jaxrs.service.MenuhasPEDIDOFacadeREST.class);
        resources.add(jaxrs.service.NewsFacadeREST.class);
        resources.add(jaxrs.service.NewsTypeFacadeREST.class);
        resources.add(jaxrs.service.PartnershipRequestFacadeREST.class);
        resources.add(jaxrs.service.PedidoFacadeREST.class);
        resources.add(jaxrs.service.PedidoStateFacadeREST.class);
        resources.add(jaxrs.service.PhoneFacadeREST.class);
        resources.add(jaxrs.service.ProductoFacadeREST.class);
        resources.add(jaxrs.service.ProductohasPEDIDOFacadeREST.class);
        resources.add(jaxrs.service.ProfessionFacadeREST.class);
        resources.add(jaxrs.service.QualificationVariableFacadeREST.class);
        resources.add(jaxrs.service.QualificationVariableHasRateFacadeREST.class);
        resources.add(jaxrs.service.RateFacadeREST.class);
        resources.add(jaxrs.service.RatehasPEDIDOFacadeREST.class);
        resources.add(jaxrs.service.SaleFacadeREST.class);
        resources.add(jaxrs.service.SaleHasMenuFacadeREST.class);
        resources.add(jaxrs.service.SaleHasProductoFacadeREST.class);
        resources.add(jaxrs.service.SimilarInventarioFacadeREST.class);
        resources.add(jaxrs.service.SimilarProductsFacadeREST.class);
        resources.add(jaxrs.service.StateFacadeREST.class);
        resources.add(jaxrs.service.SubtypeFacadeREST.class);
        resources.add(jaxrs.service.SucursalFacadeREST.class);
        resources.add(jaxrs.service.SucursalHasDayFacadeREST.class);
        resources.add(jaxrs.service.SupplierApplicationFacadeREST.class);
        resources.add(jaxrs.service.SupplierFacadeREST.class);
        resources.add(jaxrs.service.SupplierProductCategoryFacadeREST.class);
        resources.add(jaxrs.service.SupplierProductTypeFacadeREST.class);
        resources.add(jaxrs.service.SupplierTypeFacadeREST.class);
        resources.add(jaxrs.service.TipoFacadeREST.class);
        resources.add(jaxrs.service.TypeAddressFacadeREST.class);
    }
    
}
