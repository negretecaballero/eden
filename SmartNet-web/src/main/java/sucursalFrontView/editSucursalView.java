/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sucursalFrontView;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import org.primefaces.model.DefaultTreeNode;
import Clases.*;

/**
 *
 * @author luisnegrete
 */

@javax.annotation.security.RolesAllowed("GuanabaraFrAdmin")
public class editSucursalView extends BaseBacking{

    /**
     * Creates a new instance of editSucursalView
     */
    public editSucursalView() {
    }
    
    private DefaultTreeNode root;
    
    private DefaultTreeNode selection;
    
    public DefaultTreeNode getSelection(){
    
        return this.selection;
    
    }
    
    public void setSelection(DefaultTreeNode selection){
    
        this.selection=selection;
    
    }
    
    public DefaultTreeNode getRoot(){
    
    return this.root;
    
    }
    
    public void setRoot(DefaultTreeNode root){
    
    this.root=root;
    
    }
    
   private  Entities.Franquicia franchise;
   
   @Inject @Default private CDIBeans.FileUploadInterface fileUpload;
    
    @javax.annotation.PostConstruct
    public void init(){
    
        if(!this.fileUpload.getClassType().equals(editSucursalView.class.getName())){
        
        this.fileUpload.setClassType(editSucursalView.class.getName());
        
        }
        
        if(getSession().getAttribute("selectedOption") instanceof Entities.Franquicia){
        
          root=new DefaultTreeNode("root",null);
              
       DefaultTreeNode sucursal=new DefaultTreeNode("Sucursales",root);
       
franchise=(Entities.Franquicia)getSession().getAttribute("selectedOption");
        
       for(Entities.Sucursal aux:franchise.getSucursalCollection()){
       
       DefaultTreeNode auxi=new DefaultTreeNode(franchise.getNombre()+" "+aux.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+aux.getDireccion().getPrimerDigito()+"  #"+aux.getDireccion().getSegundoDigito()+"-"+aux.getDireccion().getTercerDigito()+" "+aux.getDireccion().getAdicional(),sucursal);
       
       
       System.out.print("Id Sucursal "+aux.getSucursalPK().getIdsucursal());
       
       }
        
        }
        
     
       
      
    }
    
    public void nodeSelection(org.primefaces.event.NodeSelectEvent node){
        
        try{
        
            System.out.print(node.getTreeNode().getData().toString());
            
          for(Entities.Sucursal aux:this.franchise.getSucursalCollection()){
          
            String address=franchise.getNombre()+" "+aux.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+aux.getDireccion().getPrimerDigito()+"  #"+aux.getDireccion().getSegundoDigito()+"-"+aux.getDireccion().getTercerDigito()+" "+aux.getDireccion().getAdicional();
          
            if(node.getTreeNode().getData().toString().equals(address)){
            
                javax.faces.application.NavigationHandler nh=getContext().getApplication().getNavigationHandler();
                
                nh.handleNavigation(getContext(), null, "/adminFranquicia/list-sucursales.xhtml?faces-redirect=true&idFranchise="+aux.getSucursalPK().getFranquiciaIdfranquicia()+"&idSucursal="+aux.getSucursalPK().getIdsucursal()+"");
                
            
            }
          }
        
        }
        catch(Exception ex){
        
        Logger.getLogger(editSucursalView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    
    }
}
