/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sucursalFrontView;

import CDIBeans.AdressControllerInterface;
import Clases.BaseBacking;
import Clases.EdenExceptionHandler;
import Validators.MailValidator;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.Default;
import javax.faces.FacesException;
import javax.inject.Inject;

/**
 *
 * @author luisnegrete
 */
public class listSucursalesView extends BaseBacking{

    /**
     * Creates a new instance of listSucursalesView
     */
    public listSucursalesView() {
    }
    
    
    
    private double latitude;
    
    private double longitude;
    
    private String state;
    
    private String city;
    
    private String userImage;
    
    @Inject @Default private CDIBeans.AddressTypeController addressTypeController;
    
    public String getUserImage(){
    
        return this.userImage;
    
    }
    
    public String getState(){
    
        return this.state;
    
    }
    
    public void setState(String state){
    
        this.state=state;
    
    }
    
    public String getCity(){
    
    return this.city;
    
    }
    
    public void setCity(String city){
    
        this.city=city;
    
    }
    
    public double getLatitude(){
    
        return this.latitude;
    
    }
    
    public void setLatitude(double latitude){
    
    this.latitude=latitude;
    
    }
    
    public double getLongitude(){
    
    return this.longitude;
    
    }
    
    public void setLongitude(double longitude){
    
    this.longitude=longitude;
    
    }
    
    private double radio;
    
    public double getRadio(){
    
        return this.radio;
    
    }
    
    public void setRadio(double radio){
    
    this.radio=radio;
    
    }
    
    @Inject @Default private CDIBeans.SucursalControllerDelegate sucursalController;
    
    @Inject @Default private CDIBeans.FileUploadInterface fileUpload;
    
    @Inject @Default private CDIBeans.LoginAdministradorControllerInterface loginAdministradorController;
    
   private Entities.Sucursal sucursal;
   
   public Entities.Sucursal getSucursal(){
   
   return this.sucursal;
   
   }
   
   
   public void setSucursal(Entities.Sucursal sucursal){
   
   
   this.sucursal=sucursal;
   
   }
   
   
   @javax.validation.constraints.Size(min=1,max=45)
   @MailValidator
   private String username;
   
   public String getUsername(){
   
       return this.username;
   
   }
   
   public void setUsername(String username){
   
   this.username=username;
   
   }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
        
             javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)getContext().getExternalContext().getRequest();
            
            Entities.SucursalPK sucursalPK=new Entities.SucursalPK();
            
            sucursalPK.setFranquiciaIdfranquicia(Integer.parseInt(request.getParameter("idFranchise")));
            
            sucursalPK.setIdsucursal(Integer.parseInt(request.getParameter("idSucursal")));

             
             sucursal=sucursalController.find(sucursalPK);
             
             
             
             this.city=sucursal.getDireccion().getCity().getName();
             
             this.state=sucursal.getDireccion().getCity().getState().getName();
             
            this.username=this.sucursalController.getSucursalAdministrator(sucursal.getSucursalPK());
             
             
            System.out.print("Sucursal Administrator "+this.username);
            
            System.out.print("idSucursal "+sucursal.getSucursalPK().getIdsucursal());
            
            System.out.print("idFranchise "+sucursal.getSucursalPK().getFranquiciaIdfranquicia());
           
            
            this.latitude=sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLatitude();
            
            this.longitude=sucursal.getDireccion().getGpsCoordinatesidgpsCoordinated().getLongitude();
     
            
            System.out.print("Latitude: "+this.latitude);
            
            System.out.print("Longitude: "+this.longitude);
            
             
           
           javax.servlet.ServletContext servletContext=(javax.servlet.ServletContext)getContext().getExternalContext().getContext();
        
          
           if(!this.fileUpload.getClassType().equals(listSucursalesView.class.getName())){
           
           this.fileUpload.Remove(servletContext.getRealPath("")+File.separator+"resources"+File.separator+"demo"+File.separator+"images"+File.separator+getSession().getAttribute("username"));
           
           this.fileUpload.setClassType(listSucursalesView.class.getName());
              
           this.sucursalController.loadImages(sucursal, getSession().getAttribute("username").toString());
          
           System.out.print("images loaded successfully");
           
           }
           
           System.out.print("Username "+this.username);
           
             this.userImage=this.loginAdministradorController.getImageRoot(username);
           
           }
        catch(Exception ex){
        
            Logger.getLogger(listSucursalesView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
 
    
    public void editSucursal(javax.faces.event.ActionEvent event){
    
        try{
        System.out.print(this.latitude);
        
        javax.faces.application.FacesMessage fm=new javax.faces.application.FacesMessage(javax.faces.application.FacesMessage.FACES_MESSAGES,"Sucursal Edited");
        }
        catch(Exception ex){
        
        Logger.getLogger(listSucursalesView.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
  
    @Inject @Default private AdressControllerInterface addressController;
    
    
   
    
    public java.util.List<Entities.TypeAddress>getType(){
    
    try{
        
        return this.addressController.getTipo();
        
    }
    catch(Exception ex){
    
        throw new FacesException("Exception Caugth");
    
    }
    
    }
    
    public void handleUpload(org.primefaces.event.FileUploadEvent event){
    
    
    try{
    
        this.fileUpload.servletContainer(event.getFile(), getSession().getAttribute("username").toString(), 500, 500);
    
    }
    catch (Exception ex){
    
    EdenExceptionHandler.handleException(listSucursalesView.class.getName(), ex);
    
    
    }
    
    }
    
    public void updateSucursal(javax.faces.event.ActionEvent event){
    
    try{
    
     System.out.print(event.getComponent().getClientId());
        
     this.sucursal.getDireccion().setTYPEADDRESSidTYPEADDRESS(this.addressTypeController.findByname(this.sucursal.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()));
    
     this.sucursalController.editSucursal(sucursal, this.username);
      
     javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Sucursal Edited Successfully");
     
     msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_INFO);
     
     getContext().addMessage(event.getComponent().getClientId(),msg);
       
    }
    catch(Exception ex){
    
    EdenExceptionHandler.handleException(listSucursalesView.class.getName(), ex);
    
    }
    
    }
    
    public void delete(javax.faces.event.ActionEvent event){
    
        try{
            
        System.out.print("Delete Sucursal");
            
        this.sucursalController.delete(this.sucursal.getSucursalPK());
      
        javax.faces.application.NavigationHandler nh=getContext().getApplication().getNavigationHandler();
        
        nh.handleNavigation(getContext(), null, "/index.xhtml?faces-redirect=true");
        
        }
        
        catch(Exception ex){
        
            System.out.print("Exception Caugth");
            
          
            
        }
    
    }
}
