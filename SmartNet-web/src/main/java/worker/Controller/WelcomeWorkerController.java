package worker.Controller;

import ENUM.*;

public interface WelcomeWorkerController {

	/**
	 * 
	 * @param view
	 */
	void initPreRenderView(String view);

	/**
	 * 
	 * @param branchDescription
	 */
	TransactionStatus selectBranch(String branchDescription);
        
}