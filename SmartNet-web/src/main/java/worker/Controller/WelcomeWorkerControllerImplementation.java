package worker.Controller;

import CDIBeans.*;
import ENUM.*;

@javax.enterprise.inject.Alternative
public class WelcomeWorkerControllerImplementation implements WelcomeWorkerController {

	/**
	 * 
	 * @param view
	 */
	@Override
	public void initPreRenderView(String view) {
		
            try{
            
                javax.faces.component.UIViewRoot root=javax.faces.context.FacesContext.getCurrentInstance().getViewRoot();
                
                if(root.findComponent("form:menu") instanceof org.primefaces.component.submenu.UISubmenu){
                
                    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                    org.primefaces.component.submenu.UISubmenu menu=(org.primefaces.component.submenu.UISubmenu)root.findComponent("form:menu");
                    
                    if(this._loginAdministradorController.find(session.getAttribute("username").toString())!=null){                           
                    
                        Entities.LoginAdministrador worker=(Entities.LoginAdministrador)this._loginAdministradorController.find(session.getAttribute("username").toString());
                        
                        if(worker.getSucursalWorkerCollection()!=null && !worker.getSucursalWorkerCollection().isEmpty()){
                        
                            System.out.print("BRANCHES "+worker.getSucursalWorkerCollection().size());
                            
                            for(Entities.Sucursal branch:worker.getSucursalWorkerCollection()){
                            
                                org.primefaces.component.menuitem.UIMenuItem item=new org.primefaces.component.menuitem.UIMenuItem();
                                
                                item.setValue(branch.getFranquicia().getNombre()+" "+branch.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+branch.getDireccion().getPrimerDigito()+" #"+branch.getDireccion().getSegundoDigito()+"-"+branch.getDireccion().getTercerDigito()+" "+branch.getDireccion().getAdicional());
 
                                menu.getChildren().add(item);
                                
                                javax.el.MethodExpression expression=javax.faces.context.FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createMethodExpression(javax.faces.context.FacesContext.getCurrentInstance().getELContext(), "#{welcomeWorker.actionEvent}", null, new Class[] {javax.faces.event.ActionEvent.class});
                                
                                item.addActionListener(new javax.faces.event.MethodExpressionActionListener(expression));
                                
                            }
                        
                        }
                    
                    }
                  
                    
                }
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
            
            }
            
	}

	/**
	 * 
	 * @param branchDescription
	 */
	@Override
	public TransactionStatus selectBranch(String branchDescription) {
		
            try{
            
                javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)javax.faces.context.FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                
                if(session.getAttribute("username").toString()!=null){
                
                Entities.LoginAdministrador user=(Entities.LoginAdministrador)_loginAdministradorController.find(session.getAttribute("username").toString());
                
                if(_loginAdministradorController.exists(session.getAttribute("username").toString())){
                
                    if(user.getSucursalWorkerCollection()!=null && !user.getSucursalWorkerCollection().isEmpty()){
                    
                        System.out.print("BRANCH DESCRIPTION "+branchDescription);
                        
                        for(Entities.Sucursal branch:user.getSucursalWorkerCollection()){
                        
                            if(branchDescription.equals(branch.getFranquicia().getNombre()+" "+branch.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+branch.getDireccion().getPrimerDigito()+" #"+branch.getDireccion().getSegundoDigito()+"-"+branch.getDireccion().getTercerDigito()+" "+branch.getDireccion().getAdicional())){
                            
                                session.setAttribute("auxiliarComponent", branch);
                                
                                return ENUM.TransactionStatus.APPROVED;
                            
                            }
                        
                        }
                    
                    }
                
                }
                
                }
            
                 return ENUM.TransactionStatus.DISAPPROVED;
                
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
                return ENUM.TransactionStatus.DISAPPROVED;
            
            }
            
	}

	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private LoginAdministradorController _loginAdministradorController;
}