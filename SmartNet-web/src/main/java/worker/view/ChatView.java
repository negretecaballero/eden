/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker.view;

/**
 *
 * @author luisnegrete
 */
public class ChatView extends Clases.BaseBacking{

    @javax.inject.Inject @javax.enterprise.inject.Default
    private workerController.ChatViewController _chatViewController;
    
    private String _channel;
    
    private Entities.Pedido order;
    
    private int idFranchise;
    
    private int idSucursal;
    
    private String _message;
    
    public String getMessage(){
    
        return _message;
    
    }
    
    public void setMessage(String message){
    
        _message=message;
    
    }
    
    public String getChannel(){
    
        return _channel;
    
    }
    
    public void setChannel(String channel){
    
    _channel=channel;
    
    }
    
    private String _chatUsername;
    
    public String getChatUsername(){
    
        return _chatUsername;
    
    }
    
    public void setChatUsername(String chatUsername){
    
        _chatUsername=chatUsername;
    
    }
    
    public Entities.Pedido getOrder(){
    
        return this.order;
    
    }
    
    public void setOrder(Entities.Pedido order){
    
        this.order=order;
    
    }
    
    /**
     * Creates a new instance of ChatView
     */
    public ChatView() {
    }
    
    @javax.annotation.PostConstruct
    public void init(){
    
        try{
            
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
            
                System.out.print(entry.getKey()+"-"+entry.getValue());
            
            }
        
            javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
        
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);
            
            _chatUsername=request.getParameter("userChat");
            
            this.idFranchise=Integer.valueOf(request.getParameter("idFranquicia"));
            
            this.idSucursal=Integer.valueOf(request.getParameter("idSucursal"));
            
            _channel=java.io.File.separator+"chat"+java.io.File.separator+String.valueOf(this.idFranchise)+java.io.File.separator+String.valueOf(this.idSucursal)+java.io.File.separator+session.getAttribute("username").toString();
            
            
            
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void actionListener(){
    
    System.out.print("Action Listener In");
    
    }
    
    private int _idPedido;
    
    public int getIdPedido(){
    
        return _idPedido;
    
    }
    
    public void setIdPedido(int idPedido){
    
        _idPedido=idPedido;
    
    }
    
    
    
    public void findOrder(javax.faces.event.AjaxBehaviorEvent event){
    
        try{
        
            System.out.print("Id Order "+this._idPedido);

            
            javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
            
            System.out.print("Order to find "+idFranchise+"-"+Integer.valueOf(request.getParameter("east-form:idpedido")));
            
            order=this._chatViewController.findOrder(idFranchise, Integer.valueOf(request.getParameter("east-form:idpedido")));
            
            for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
            
            System.out.print(entry.getKey()+"-"+entry.getValue());
            
            
            
            }
            
            
            javax.faces.component.UIViewRoot viewRoot=this.getContext().getViewRoot();
            
            System.out.print("Component Class "+viewRoot.findComponent("east-form:outputPanel").getClass().getName());
            
           org.primefaces.component.outputpanel.OutputPanel outputPanel=(org.primefaces.component.outputpanel.OutputPanel)viewRoot.findComponent("east-form:outputPanel");
            
         
            
            
            if(order!=null){
            
               outputPanel.getAttributes().put("styleClass", "chatOrderPanelVisible");
            
            }
            else{
            
        
                  outputPanel.getAttributes().put("styleClass", "chatOrderPanelHidden");
                  
                  javax.faces.application.FacesMessage msg=new javax.faces.application.FacesMessage("Order not found");
                
                  msg.setSeverity(javax.faces.application.FacesMessage.SEVERITY_WARN);
                  
                  this.getContext().addMessage(null, msg);
                  
            }
            
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
    public void sendMessage(javax.faces.event.ActionEvent event){
    
        try{
        
           org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
           
           javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);

           System.out.print("Channel: "+this._channel+" Message: "+this._message);
           
           bus.publish("/user/private/chat/"+this._chatUsername,session.getAttribute("username").toString()+":"+_message);
           
           _message="";
        
        }
        
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
    public void updateUsername(javax.faces.event.ActionEvent event){
    
        try{
        
            javax.servlet.http.HttpServletRequest servletRequest=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
        
            
            if(servletRequest.getParameter("username")!=null){
            
                this._chatUsername=servletRequest.getParameter("username");
            
            }
            
            
        }
        catch(javax.faces.event.AbortProcessingException ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
            
        }
    
    }
    
}
