/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker.view;

import CDIBeans.PedidoStateControllerInterface;
import Clases.BaseBacking;
import Clases.PedidoWorker;
import Clases.PedidoWorkerInterface;
import Entities.LoginAdministrador;
import Entities.MenuhasPEDIDO;
import Entities.Pedido;
import Entities.ProductohasPEDIDO;
import SessionBeans.PedidoFacadeLocal;
import SessionClasses.CartItem;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.PathSegment;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.MenuhasPEDIDOFacadeREST;
import jaxrs.service.PedidoFacadeREST;
import jaxrs.service.PedidoStateFacadeREST;
import jaxrs.service.ProductohasPEDIDOFacadeREST;
import jaxrs.service.SucursalFacadeREST;
import org.jboss.errai.enterprise.client.jaxrs.api.PathSegmentImpl;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 *
 * @author luisnegrete
 */
public class PedidoController extends BaseBacking{

    private String test;
    
    
    
    private  String channel;
    
    private String idSucursal;
    
    private String idFranquicia;
    
    private String sucursalName;
    
    private String _userChat;
    
    private String _data;
    
    public String getUserChat(){
    
        return _userChat;
    
    }
    
    public void setUserChat(String userChat){
    
    _userChat=userChat;
    
    }
    
    public String getData(){
    
        return _data;
    
    }
    
    public void setData(String data){
    
        _data=data;
    
    }
    
   
    public String getIdFranquicia(){
    
    return this.idFranquicia;
    
    }
    
    public String getIdSucursal(){
    
    return this.idSucursal;
    
    }
    
    public void setIdFranquicia(String idFranquicia){
    
    this.idFranquicia=idFranquicia;
    
    }
    
    public void setIdSucursal(String idSucursal){
    
    this.idSucursal=idSucursal;
    
    }
    
    public String getSucursalName(){
    
    return this.sucursalName;
    
    }
    
    public void setSucursalName(String sucursalName){
    
    this.sucursalName=sucursalName;
    
    }
    
    
    private String _chatChannel;
    
    public String getChatChannel(){
    
        return _chatChannel;
    
    }
    
    public void setChatChannel(String chatChannel){
    
        _chatChannel=chatChannel;
    
    }
    
    
    @Inject PedidoStateControllerInterface pedidoStateController;
    
    public PedidoStateControllerInterface getPedidoStateController(){
    
    return this.pedidoStateController;
    
    }
    
    public void setTest(String test){
    
        this.test=test;
    
    }
    
    public String getTest(){
    
    return this.test;
    
    }
    
    private List<PedidoWorkerInterface>pedidoList;
    
    public List<PedidoWorkerInterface>getPedidoList(){
    
        return this.pedidoList;
    
    }
    
    public void setPedidoList(List<PedidoWorkerInterface>pedidoList){
    
    this.pedidoList=pedidoList;
    
    }
    
    public String getChannel(){
    
        return this.channel;
    
    }
    
    public void setChannel(String channel){
    
        this.channel=channel;
    
    }
    
    
    @EJB 
    private PedidoStateFacadeREST pedidoStateFacadeREST;
    
    @EJB
    private PedidoFacadeLocal pedidoFacade;
    
    
    @EJB
    private PedidoFacadeREST pedidoFacadeREST;
    
    @EJB
    private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
    
    @EJB
    private MenuhasPEDIDOFacadeREST menuhasPEDIDOFacadeREST;
    
    @EJB
    private ProductohasPEDIDOFacadeREST productohasPEDIDOFacadeREST;
    
  @EJB
  private SucursalFacadeREST sucursalFacadeREST;
    
  
    
    @PostConstruct
    public void init(){
    
        try{

            System.out.print("View "+this.getContext().getViewRoot().getViewId());
                  
            _data="Luis";
            
            LoginAdministrador login=loginAdministradorFacadeREST.find(((HttpSession)getContext().getExternalContext().getSession(true)).getAttribute("username"));
            
            HttpServletRequest request=(HttpServletRequest)getContext().getExternalContext().getRequest();
            
            javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);
            
            System.out.print("Id Sucursal "+request.getParameter("idsucursal"));
            
            System.out.print("Id Franquicia "+request.getParameter("idfranquicia"));
            
            this.channel=java.io.File.separator+request.getParameter("idfranquicia")+java.io.File.separator+request.getParameter("idsucursal");
                
            _chatChannel=java.io.File.separator+"initChat"+java.io.File.separator+request.getParameter("idfranquicia")+java.io.File.separator+request.getParameter("idsucursal");
            
            this.idSucursal=request.getParameter("idsucursal");
            
            this.idFranquicia=request.getParameter("idfranquicia");
            
            Entities.Sucursal sucursal=sucursalFacadeREST.find(new PathSegmentImpl("bar;idsucursal="+this.idSucursal+";franquiciaIdfranquicia="+this.idFranquicia+""));
            
            this.sucursalName=login.getUsername();
            
            System.out.print("Sucursal Name "+this.sucursalName);
            
            updateResultList(ENUM.OrderState.Pending.name());

        
        }
        catch(NullPointerException ex){
        
            Logger.getLogger(PedidoController.class.getName()).log(Level.SEVERE,null,ex);
        
        }
    
    }
    
   
    public void preRenderView(javax.faces.event.ComponentSystemEvent event){
    
        String view=this.getContext().getViewRoot().getViewId();
        
        System.out.print("View value is "+view);
        
        switch(view){
        
            case "/Worker/pedidos.xhtml":{
            
                javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
                
                
                break;
            
            }
        
        }
    
    }
    
    public void testSocket(ActionEvent event){
 
 System.out.print("You pressed the  Socket teestin button");
     
 EventBus eventBus=EventBusFactory.getDefault().eventBus();
 
 
 String CHANNEL=channel;

 System.out.print("Channel Value "+channel);
 
 eventBus.publish(CHANNEL,new FacesMessage("Testting","hi"));
 
 }
    
    
    public void testChat(javax.faces.event.ActionEvent event){
    try{
    
        for(javax.faces.component.UIComponent aux:event.getComponent().getChildren()){
        
            if(aux instanceof javax.faces.component.UIParameter){
            
                System.out.print("Parameter found");
                
                javax.faces.component.UIParameter parameter=(javax.faces.component.UIParameter)aux;
                
                System.out.print(parameter.getName()+"-"+parameter.getValue().toString());
            
            }
        
        }
        
    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);

    
    org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
    
    String username=session.getAttribute("username").toString();

    
    bus.publish(_chatChannel, username+":"+username+" wants to chat, init chat?:"+username);
    
    }
    catch(Exception ex){
    
        java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
    
    }
    
    }
    
    /**
     * Creates a new instance of PedidoController
     */
    public PedidoController() {
        
        test="Testing";
    }
    
    public void tabChanged(TabChangeEvent event){
    
    System.out.print(event);
        
    System.out.print(event.getData());
    
    System.out.print(event.getTab());
    
    if(event.getTab() instanceof org.primefaces.component.tabview.Tab){
    
    System.out.print("This is a tab Component");
    
    org.primefaces.component.tabview.Tab tab=event.getTab();
    
    System.out.print(tab.getTitle());
    
    }
    
    //this.pedidoList.clear();
    
    updateResultList(event.getTab().getTitle());
    
    System.out.print(this.pedidoList.size());
    
    }
    
    public void pedidoStateChanged(AjaxBehaviorEvent event,Pedido pedido,String tab){
    
        try{
        
        System.out.print("PEdido state changed");
        
        System.out.print("Id Pedido "+pedido.getIdPEDIDO());
        
        System.out.print("Pedido State "+pedido.getPedidoStatestateId().getStateId());
        
        pedido.setPedidoStatestateId(pedidoStateFacadeREST.find(pedido.getPedidoStatestateId().getStateId()));
        
        pedidoFacadeREST.edit(pedido.getIdPEDIDO(), pedido);
        
        updateResultList(tab);
        
        }
        catch(EJBException ex){
        
        
            FacesMessage msg=new FacesMessage("Exception Caught");
            
            
            
        }
    
    }
    
    
    private void updateResultList(String state){
    
    PathSegment ps=new PathSegmentImpl("bar;idsucursal="+this.idSucursal+";franquiciaIdfranquicia="+this.idFranquicia+"");
        
    this.pedidoList=new <PedidoWorker>ArrayList();
            
    List<Pedido>pedidos=pedidoFacadeREST.findByState(state, ps);
            
            System.out.print("Orders in STATE "+state+" "+pedidos.size());
            
            for(Pedido aux:pedidos){
            
            PedidoWorkerInterface pedidoWorker=new PedidoWorker();
            
            pedidoWorker.setPedido(aux);
            
            List<CartItem>itemList=new <CartItem>ArrayList();
            
            PathSegment ps1=new PathSegmentImpl("bar;idsucursal="+this.idSucursal+";franquiciaIdfranquicia="+this.idFranquicia+"");
            
            for(MenuhasPEDIDO auxi:menuhasPEDIDOFacadeREST.findIdPedidoSucursalPK(aux.getIdPEDIDO(), ps1)){
                
                System.out.print("Menu Has pedido List "+menuhasPEDIDOFacadeREST.findIdPedidoSucursalPK(aux.getIdPEDIDO(), ps1).size());
            
                CartItem item=new CartItem();
                
                item.setItem(auxi.getMenu());
                
                item.setName(auxi.getMenu().getNombre());
                
                item.setQuantity(auxi.getQuantity());
                
                itemList.add(item);    
            
            }
            
            
            
            for(ProductohasPEDIDO auxi:productohasPEDIDOFacadeREST.findByPedidoSucursal(aux.getIdPEDIDO(), ps1))
            
            {
            
                System.out.print("producto Has pedido List "+productohasPEDIDOFacadeREST.findByPedidoSucursal(aux.getIdPEDIDO(), ps1).size());
                          
                CartItem item=new CartItem();
                        
                item.setItem(auxi.getProducto());
             
                if(auxi.getProducto()!=null){
                item.setName(auxi.getProducto().getNombre());
              
                
                item.setQuantity(auxi.getQuantity());
                
                itemList.add(item);
                }
                
            }
            
            pedidoWorker.setItemList(itemList);
            this.pedidoList.add(pedidoWorker);
            
          
            
            }
            
              System.out.print("ORDERS TO VIEW "+this.pedidoList.size());
    
    }
    
   
    public void goChat(javax.faces.event.AjaxBehaviorEvent event){
    
   try{
   
    javax.faces.context.ExternalContext ex=this.getContext().getExternalContext();
    
    javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
    
    for(java.util.Map.Entry<String,String>entry:this.getContext().getExternalContext().getRequestParameterMap().entrySet()){
    
        System.out.print(entry.getKey()+"-"+entry.getValue());
    
    }
    
    System.out.print(ex.getRequestContextPath());
    
    org.primefaces.push.EventBus bus=org.primefaces.push.EventBusFactory.getDefault().eventBus();
    
    javax.servlet.http.HttpSession session=(javax.servlet.http.HttpSession)this.getContext().getExternalContext().getSession(true);

    bus.publish("/"+request.getParameter("username"),request.getParameter("username"));
    
    ex.redirect(ex.getRequestContextPath()+"/Worker/chat.xhtml?faces-redirect=true&idFranquicia="+this.idFranquicia+"&idSucursal="+this.idSucursal+"&userChat="+request.getParameter("username")+"");
   
   }
   
   catch(javax.faces.event.AbortProcessingException | java.io.IOException ex){
   
   java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
   
   }
    
    }
    
    private org.primefaces.model.map.MapModel mapModel=new org.primefaces.model.map.DefaultMapModel();
    
    public org.primefaces.model.map.MapModel getMapModel(){
    
    return this.mapModel;
    
    }
  
    
    public void setMapModel(org.primefaces.model.map.DefaultMapModel mapModel){
    
        this.mapModel=mapModel;
    
    }
    
    public void viewMap(Entities.Direccion address){
      try{
   
   javax.servlet.http.HttpServletRequest request=(javax.servlet.http.HttpServletRequest)this.getContext().getExternalContext().getRequest();
   
   mapModel=new org.primefaces.model.map.DefaultMapModel();
   
   if(request.getParameter("latitude")!=null && request.getParameter("longitude")!=null){
   
      mapModel.addOverlay(new org.primefaces.model.map.Marker(new org.primefaces.model.map.LatLng(Double.valueOf(request.getParameter("latitude")),Double.valueOf(request.getParameter("longitude"))),"Your Location"));
   
   }
   
   
   
   mapModel.addOverlay(new org.primefaces.model.map.Marker(new org.primefaces.model.map.LatLng(address.getGpsCoordinatesidgpsCoordinated().getLatitude(),address.getGpsCoordinatesidgpsCoordinated().getLongitude()),"Delivery Address"));
      
    }
    
    catch(NumberFormatException ex){

    java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);

}
        
    }
    
    
    public void updateOrders(javax.faces.event.ActionEvent event){
    
        try{
        
        if(this.getContext().getViewRoot().findComponent("form:tabView") instanceof org.primefaces.component.tabview.TabView){
        
            System.out.print("The Component is a tabView");
            
            org.primefaces.component.tabview.TabView tabView=(org.primefaces.component.tabview.TabView)this.getContext().getViewRoot().findComponent("form:tabView");
            
            this.updateResultList(ENUM.OrderState.Pending.name());

        }
        
        }
        catch(javax.faces.event.AbortProcessingException ex)
        {
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
        }
    
    }
    
}
