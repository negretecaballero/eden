/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package worker.view;

import Clases.BaseBacking;
import Entities.LoginAdministrador;
import Entities.Sucursal;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.NavigationHandler;
import javax.servlet.http.HttpSession;
import jaxrs.service.LoginAdministradorFacadeREST;
import jaxrs.service.SucursalFacadeREST;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import worker.Controller.*;

/**
 *
 * @author luisnegrete
 */
@RolesAllowed("GuanabaraWorker")
public class WelcomeWorker extends BaseBacking{

    private TreeNode root;
    
    private String userName;
    
    @EJB private SucursalFacadeREST sucursalFacadeREST;
    
    private int idsucursal;
    
    private int idfranquicia;
    
    public int getIdsucursal(){
    
        return this.idsucursal;
    }
    
    public int getIdfranquicia(){
    
        return this.idfranquicia;
    
    }
    
    public void setIdsucursal(int idsucursal){
    
        this.idsucursal=idsucursal;
    
    }
    
    public void setIdfranquicia(int idfranquicia){
    
    this.idfranquicia=idfranquicia;
    
    }
    
    public void setUserName(String userName){
    
    this.userName=userName;
    
    }
    
    public String getUserName(){
    
        return this.userName;
    
    }
    
    public TreeNode getRoot(){
    
    return this.root;
        
    }
    
    public void setRoot(TreeNode root){
    
    this.root=root;
    
    }
    
    @EJB private LoginAdministradorFacadeREST loginAdministradorFacadeREST;
	@javax.inject.Inject
	@javax.enterprise.inject.Default
	private WelcomeWorkerController _workerViewController;
    
//    @EJB private SucursalhasloginAdministradorFacadeREST sucursalhasloginAdministradorFacadeREST;
    
    
    /**
     * Creates a new instance of WelcomeWorker
     */
    public WelcomeWorker() {
    }
    
    
    @PostConstruct
    public void init(){
        
        
    
        HttpSession session=(HttpSession)getContext().getExternalContext().getSession(true);
        
        root=new DefaultTreeNode("Root",null);
        
        LoginAdministrador login=loginAdministradorFacadeREST.find(((HttpSession)getContext().getExternalContext().getSession(true)).getAttribute("username"));
    
        this.userName=login.getName();
        
        TreeNode sucursalNode=new DefaultTreeNode("Mis Sucursales",root);
        
/*        for(SucursalhasloginAdministrador aux: sucursalhasloginAdministradorFacadeREST.findByUsername(session.getAttribute("username").toString(), "guanabara_worker"))
        {
        
            TreeNode auxi=new DefaultTreeNode(aux.getSucursal().getFranquicia().getNombre()+" "+aux.getSucursal().getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+aux.getSucursal().getDireccion().getPrimerDigito()+" #"+aux.getSucursal().getDireccion().getSegundoDigito()+"-"+aux.getSucursal().getDireccion().getTercerDigito()+" "+aux.getSucursal().getDireccion().getAdicional(),sucursalNode);
        
        }
       */
        
        
    }
    
    public void nodeSelect(NodeSelectEvent event){
    
        String value=event.getTreeNode().getData().toString();
        
      
        
        if(!value.equals("Mis Sucursales"))
        {
        
         
              for(Entities.Sucursal suc:this.sucursalFacadeREST.findAll()){
        
        System.out.print("Sucursal Id "+suc.getSucursalPK().getIdsucursal()+" "+suc.getFranquicia().getNombre()+" "+suc.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+suc.getDireccion().getPrimerDigito()+" #"+suc.getDireccion().getSegundoDigito()+"-"+suc.getDireccion().getTercerDigito()+" "+suc.getDireccion().getAdicional());
        
        }
            
        for (Sucursal suc:this.sucursalFacadeREST.findAll()){
            
            System.out.print("You are in "+suc.getSucursalPK().getIdsucursal());
            
            String  compare=suc.getFranquicia().getNombre()+" "+suc.getDireccion().getTYPEADDRESSidTYPEADDRESS().getTypeAddress()+" "+suc.getDireccion().getPrimerDigito()+" #"+suc.getDireccion().getSegundoDigito()+"-"+suc.getDireccion().getTercerDigito()+" "+suc.getDireccion().getAdicional();
               
          
            System.out.print(compare+"-"+value);
            
            if(value.equals(compare)){
            
                System.out.print("Value Found");
                
                this.idsucursal=suc.getSucursalPK().getIdsucursal();
                
                this.idfranquicia=suc.getSucursalPK().getFranquiciaIdfranquicia();
                
                System.out.print("Item Found");
                
            break;
            }
        
            
         
        
      
        }
        
           NavigationHandler navigationhandler=getContext().getApplication().getNavigationHandler();
        
           navigationhandler.handleNavigation(getContext(), null, "Pedidos");  
        
           
    }
    
    }

	/**
	 * 
	 * @param event
	 */
	public void preRenderView(javax.faces.event.ComponentSystemEvent event) {
		
            try{
            
                String view=this.getCurrentView();
                
                this._workerViewController.initPreRenderView(view);
            
            }
            catch(NullPointerException ex){
            
                ex.printStackTrace(System.out);
                
            }
            
	}

	/**
	 * 
	 * @param event
	 */
	public void actionEvent(javax.faces.event.ActionEvent event) {
		
           if(event.getComponent() instanceof org.primefaces.component.menuitem.UIMenuItem){
           
               org.primefaces.component.menuitem.UIMenuItem item=(org.primefaces.component.menuitem.UIMenuItem)event.getComponent();
               
               System.out.print("MENU ITEM VALUE "+item.getValue());
               
               switch(this._workerViewController.selectBranch(item.getValue().toString())){
               
                   case APPROVED:{
                   
                       System.out.print("APPROVED");
                       
                       javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
                       
                       nh.handleNavigation(this.getContext(), null, "Pedidos");
                       
                       break;
                   
                   }
                   
                   case DISAPPROVED:{
                   
                       System.out.print("DISAPPROVED CASE");
                       
                       javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
                       
                       nh.handleNavigation(this.getContext(), null, "failure");
                       
                       break;
                   
                   }
                   
                   default:{
                       
                        System.out.print("DISAPPROVED DEFAULT");
                   
                       javax.faces.application.NavigationHandler nh=this.getContext().getApplication().getNavigationHandler();
                       
                       nh.handleNavigation(this.getContext(), null, "failure");
                       
                       break;
                   
                   }
               
               }
           
           }
            
	}
    
}
