/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package workerController;

/**
 *
 * @author luisnegrete
 */
@javax.enterprise.inject.Alternative
public class ChatViewControllerImplementation implements ChatViewController{
    
    @javax.inject.Inject @javax.enterprise.inject.Default private CDIBeans.PedidoControllerInterface _pedidoController;
    
    @Override
    public Entities.Pedido findOrder(int IdFranchise, int idOrder){
    
        try{
        
           return _pedidoController.findEspecificFranchise(idOrder, IdFranchise);
        
        }
        catch(Exception ex){
        
            java.util.logging.Logger.getLogger(this.getClass().getName()).log(java.util.logging.Level.SEVERE,null,ex);
        
            return null;
        }
    
    }
    
}
