/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Primefaces.widget.EdenImage=Primefaces.widget.BaseWidget.extend({
    
   /*
    * 
    initialize the widget 
    **/    
   
   
    
    init:function (cfg){
        
        this._super(cfg);
        
        this.cfg=cfg;
        
        this.id=cfg.id;
        
        this.jqTarget=$(cfg.forTarget);
        
        var _self=this;
        
        if(this.jqTarget.is(':visible')){
            
            this.createImage();
            
            
        }
        
    }
    
});