/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var jsonObject;

function eraseImage(imagePath){
   
  
    
    if(jsonObject==null){
        
        jsonObject={'identifier':'eden','results':[]};
        
    }
    
    if(existence('removeImage'))
    {
        
        jsonObject.results[index('removeImage')].value=imagePath;
        
    }
    else{
        
        var cache={'id':'removeImage','value':imagePath};
        
        jsonObject.results.push(cache);
        
    }
   
   document.getElementById("deleteImage").value=jsonObject.results[index('removeImage')].value;
   
  
   
   showDialog();
    
}


function existence(id){
    
    for(var key in jsonObject.results){
        
        if(jsonObject.results[key].id==id){
            
            return true;
            
        }
        
        
    }
    
   return false; 
}

function index(id){
    
    for(var key in jsonObject.results){
        
        if(jsonObject.results[key].id==id){
            
            return key;
            
        }
        
    }
    
    return 0;
}

function showDialog(){
    
    if(jsonObject!=null){
        
        var dialog=PF('dialog');
        
        dialog.jq.find('.ui-dialog-title').text('Delete Image');
        
        dialog.jq.find('.ui-dialog-content').text('Are you sure you want to delete '+jsonObject.results[index('removeImage')].value+'?');
        
        dialog.show();
    }
    
}