/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var geocoder;
var map;
var marker;
var markers = [];
var infowindow = new google.maps.InfoWindow();
var addresss;

var city;

function initialize() {
   
    
    PF('w_gmap').jq.width(PF('layout').layout.state.center.innerWidth*.9);
    
    PF('w_gmap').jq.height(PF('layout').layout.state.center.innerHeight);
    
    google.maps.event.trigger(PF('w_gmap').getMap(),'resize');
     
     
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
 
  marker = new google.maps.Marker({  
    map: PF('w_gmap').getMap(),
    zoom: 15,
    draggable:true
});

 
 
 map =PF('w_gmap').getMap();
 
 google.maps.event.addListener(marker, 'dragend', function() {
  
 
    
 document.getElementById("latitudeValue").value = marker.getPosition().lat().toLocaleString();
 document.getElementById("longitudeValue").value = marker.getPosition().lng().toLocaleString();

codeLatLng(marker.getPosition().lat().toLocaleString()+","+marker.getPosition(). lng().toLocaleString());

setMapCenter(parseFloat(marker.getPosition().lat()), parseFloat(marker.getPosition().lng()));
 


  });
 
 
 
 if (navigator.geolocation) {
     
        checkGeolocationByHTML5();
    
 } 
    else {
        
        checkGeolocationByLoaderAPI(); // HTML5 not supported! Fall back to Loader API.
    
 }
 
 google.maps.event.trigger(map,'resize');
 
 }

    function checkGeolocationByHTML5() {
        
 map=PF('w_gmap').getMap();
        
 navigator.geolocation.getCurrentPosition(function(position) {
           
   
 setMapCenter(parseFloat(position.coords.latitude), parseFloat(position.coords.longitude));
         
  marker.setMap(PF('w_gmap').getMap());
  
  marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
  
  marker.setTitle("current locaton");
     
codeLatLng(position.coords.latitude.toLocaleString()+","+position.coords.longitude.toLocaleString());

document.getElementById("latitudeValue").value=position.coords.latitude.toLocaleString();
            
document.getElementById("longitudeValue").value=position.coords.longitude.toLocaleString();

alert("Latitude Value "+document.getElementById("latitudeValue").value);

alert("Longitude Value "+document.getElementById("longitudeValue").value);
   
        }, function() {
            checkGeolocationByLoaderAPI(); // Error! Fall back to Loader API.
        });
        
    }

    function checkGeolocationByLoaderAPI() {
        if (google.loader.ClientLocation) {
        // setMapCenter(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
        } else {
            // Unsupported! Show error/warning?
        }
    }

    function setMapCenter(latitude, longitude) {
        
          PF('w_gmap').getMap().setCenter(new google.maps.LatLng(latitude, longitude));
        
    }


function codeAddress() {
 
alert("Hidden State "+document.getElementById("form:tabView:hiddenState").value);

alert("Hidden City " +document.getElementById("form:tabView:hiddenCity").value);
 
var address= PF('select').getSelectedValue()+" "+PF('primerDigito').jq.val()+"#"+PF('segundoDigito').jq.val()+"-"+PF('tercerDigito').jq.val()+" "+document.getElementById("form:tabView:hiddenCity").value+","+document.getElementById("form:tabView:hiddenState").value+",Colombia";

alert(address);

map=  PF('w_gmap').getMap();
geocoder = new google.maps.Geocoder(); 

  //var address = document.getElementById("formulario:hiddenDir").value;
  
  geocoder.geocode( { 'address': address}, function(results, status) {
      
    if (status == google.maps.GeocoderStatus.OK) {     
    
  //PF('w_gmap').getMap().setCenter(results[0].geometry.location);
  
  marker.setPosition(results[0].geometry.location);
  
  marker.setMap(PF('w_gmap').getMap());
   
  //alert(results[0].geometry.location.lat().toLocaleString());
   
 document.getElementById("latitudeValue").value=results[0].geometry.location.lat().toLocaleString();
            
 document.getElementById("longitudeValue").value=results[0].geometry.location.lng().toLocaleString();
 
 alert("Latitude Value "+document.getElementById("latitudeValue").value);
 
 alert("Longitude Value "+document.getElementById("longitudeValue").value);
        
 setMapCenter(parseFloat(results[0].geometry.location.lat()),parseFloat(results[0].geometry.location.lng()))         
 infowindow.setContent(address);
 infowindow.open(PF('w_gmap').getMap(), marker);
     
       
    }
    else {
        
      alert('Geocode was not successful for the following reason: ' + status);
      
    }
  });
  
}
google.maps.event.addDomListener(window, 'load', initialize);



function getReverseGeocodingData(lat, lng) {
    
    alert('GetReverseGeocoding');
    
    var latlng = new google.maps.LatLng(lat, lng);
    
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
           console.log(results);
           addresss = (results[0].address_components);
        }
    });
}

    function checkGeolocationByLoaderAPI() {
        if (google.loader.ClientLocation) {
            setMapCenter(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
        } else {
            // Unsupported! Show error/warning?
        }
    }

  
    
    
    function deleteMarkers() {
  clearMarkers();
  markers = [];
  
  
}
function clearMarkers() {
  setAllMap(null);
}

function codeLatLng(input) {

  var latlngStr = input.split(',', 2);
  //Latitude
  var lat = parseFloat(latlngStr[0]);
  //Longitude
  var lng = parseFloat(latlngStr[1]);
  
  var latlng = new google.maps.LatLng(lat, lng);
  
  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[0]) {
     var result = results[0];
     
         city = "";
         var state = "";
         var street_number="";
         var country;
         
for(var i=0, len=result.address_components.length; i<len; i++) {
    
    
    
	var ac = result.address_components[i];
        
  	if(ac.types.toLocaleString()=="locality,political"){ city = ac.long_name;}
	if(ac.types.toLocaleString()=="administrative_area_level_1,political") {state = ac.long_name;}
        if(ac.types.toLocaleString()=="country,political") {street_number = ac.long_name;}

if(ac.types.indexOf("country")>=0){
    
    country=ac.long_name;
    
}

}

document.getElementById("city").value=city;



 var address = results[0].formatted_address.split(',', 1);
 
 
              //divide by white spaces
 var sections=address.toLocaleString().split(" ");
 var type="";
 var primer_digito="";
 var segundo_digito="";
 var tercer_digito="";
 
 for(var i=0,len=sections.length;i<len;i++){
     
     var flag=0;
     
     
     
     if(sections[i].toLocaleString().indexOf('-')!=-1){
      
     // alert("it contains -");
      
      var filter= sections[i].toLocaleString().split('-');
     
                      //alert("filtrado "+filter[0]);
      
     if(type==""){
    
        type=filter[0].replace('#','');
        flag==1;
    }
    
    
    if(sections[i].toLocaleString().indexOf('0')!=-1 || sections[i].toLocaleString().indexOf('1')!=-1 || sections[i].toLocaleString().indexOf('2')!=-1 || sections[i].toLocaleString().indexOf('3')!=-1 || sections[i].toLocaleString().indexOf('4')!=-1 || sections[i].toLocaleString().indexOf('5')!=-1 || sections[i].toLocaleString().indexOf('6')!=-1 || sections[i].toLocaleString().indexOf('7')!=-1  || sections[i].toLocaleString().indexOf('8')!=-1 || sections[i].toLocaleString().indexOf('9')!=-1){
    
   
    
  if(primer_digito=="" && flag==0){
        
   primer_digito= filter[0].replace('#','');  
   flag=1;
    }
    
    if(segundo_digito=="" && flag==0){
        segundo_digito=filter[0].replace('#','');
        flag=1;
    }
    
    if(tercer_digito=="" && flag==0){
        if(filter.length<2){
      tercer_digito=filter[0].replace('#','');
      flag=1;
  }
  else{
     tercer_digito=filter[1].replace('#','');
      flag=1; 
      
  }
        
    }
         
         
         
     }
 }
                  
    else if(sections[i].toLocaleString().indexOf('#')==-1){
     
    if(type==""){
       // alert("type is null");
        type=sections[i];
        flag=1;
    }
    
    
    if(sections[i].toLocaleString().indexOf('0')!=-1 || sections[i].toLocaleString().indexOf('1')!=-1 || sections[i].toLocaleString().indexOf('2')!=-1 || sections[i].toLocaleString().indexOf('3')!=-1 || sections[i].toLocaleString().indexOf('4')!=-1 || sections[i].toLocaleString().indexOf('5')!=-1 || sections[i].toLocaleString().indexOf('6')!=-1 || sections[i].toLocaleString().indexOf('7')!=-1  || sections[i].toLocaleString().indexOf('8')!=-1 || sections[i].toLocaleString().indexOf('9')!=-1){
    
   
    
    if(primer_digito=="" && flag==0){
        
   primer_digito= sections[i];  
   flag=1;
    }
    
    if(segundo_digito=="" && flag==0){
        segundo_digito=sections[i];
        flag=1;
    }
    
    if(tercer_digito=="" && flag==0){
      tercer_digito=  sections[i];
        flag=1;
    }
    
                      
                      
    }
    
     }
     
     else if(sections[i].toLocaleString().indexOf('#')!=-1){
         
      
     }
    
    
 }
 
 
 var similarity=0;
 
 var selected;
 
 $("select[name='form:tabView:select_input'] option").each(function(){
     
     if(similar(type,$(this).val())>similarity){
         
         similarity=similar(type,$(this).val());
         
         selected=$(this).val();
         
         
     }
     
 });
 
 PF('select').selectValue(selected);
 PF('primerDigito').jq.val(primer_digito);  
 PF('segundoDigito').jq.val(segundo_digito);
 PF('tercerDigito').jq.val(tercer_digito);
 
 marker.setMap(PF('w_gmap').getMap());
 marker.setPosition(latlng);
 marker.setTitle("Current Position");

         
        infowindow.setContent(PF('select').getSelectedValue()+" "+PF('primerDigito').jq.val()+"#"+PF('segundoDigito').jq.val()+"-"+PF('tercerDigito').jq.val()+" "+city+","+state);
        infowindow.open(PF('w_gmap').getMap(), marker);
        
        var coincidence=0;
        
        var selectedState="";
        
$(PrimeFaces.escapeClientId("form:tabView:state option")).each(function(){
    
    if(similar($(this).val(),state)>coincidence){
        
        selectedState=$(this).val();
        
        coincidence=similar($(this).val(),state);
        
    }

    
});


        
       
PF('state').selectValue(selectedState);
PF('cities').selectValue(city);

//rc();

console.log("UPDATING CITY");

      }
      
          
       else {
        alert('No results found');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });
}

 
google.maps.event.addDomListener(window, 'load', initialize);
   
    //Assign value
    
    function assignTipe(type){
        
      document.getElementsByName("hidden1").value=type;
            
    }
    
    
    
    
    function cityChanged(){
        
        alert("city changed");
        
    }
    
