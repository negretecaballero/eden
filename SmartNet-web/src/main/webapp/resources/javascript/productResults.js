/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


window.onload =function init(){

var panelGrid=$(PrimeFaces.escapeClientId("form:panelGrid"));

var googlePanel=$(PrimeFaces.escapeClientId("form:googlePanelGrid"));

var heigth=PF('layout').layout.state.center.innerHeight;

var width=PF('layout').layout.state.center.innerWidth;


panelGrid.width(googlePanel.width());


PF('map').jq.width(PF('layout').layout.state.center.innerWidth*.5);
 
PF('map').jq.css('height', PF('panel').jq.height()*.1);

google.maps.event.trigger(PF('map'), 'resize');

initialize();

}



var geocoder;
var map;
var marker;
var markers = [];
var infowindow = new google.maps.InfoWindow();
var addresss;
var lat;
var lon;

function initialize() {
    
     
      lat=document.getElementById("form:latitude").value;
     
      lon=document.getElementById("form:longitude").value;
     
    
     
     setMapCenter(parseFloat(lat),parseFloat(lon));
     
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(-34.397, 150.644);
 
  marker = new google.maps.Marker({  
    map: PF('map').getMap(),
    zoom: 15,
    draggable:false
});

 
 
 map =PF('map').getMap();
 

 
 
 
 if (navigator.geolocation) {
     
        checkGeolocationByHTML5();
    
 } 
    else {
        
        checkGeolocationByLoaderAPI(); // HTML5 not supported! Fall back to Loader API.
    
    }
    
    
    PF('map').jq.width(PF('layout').layout.state.center.innerWidth*.3);
    
    PF('map').jq.height(PF('layout').layout.state.center.innerHeight*.3);
    
    google.maps.event.trigger(map,'resize');
    
    } 
    

    function checkGeolocationByHTML5() {
        
        map=PF('map').getMap();
        
       navigator.geolocation.getCurrentPosition(function(position) {
           
       //PF('w_gmap').getMap().setCenter(new google.maps.LatLng(parseFloat(position.coords.latitude.toLocaleString()) , parseFloat(position.coords.longitude.toLocaleString())));
     
 
     
         
  marker.setMap(PF('map').getMap());
  
  marker.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
  
  marker.setTitle("Your Locaton");
     

 var infowindowCurrent = new google.maps.InfoWindow({
      content: "Your location"
  });

 
 infowindowCurrent.open(PF('map').getMap(), marker);
 
 
 var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    
    var labelMarker = new google.maps.Marker({
        position: latlng,  
        map: map,
        visible: false
    });
 
 
  var flightPlanCoordinates = [
    new google.maps.LatLng(parseFloat(lat), parseFloat(lon)),
    new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
  ];
  var flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2
  });

  flightPath.setMap(PF('map').getMap());
  
  var stavanger= new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
    // create an invisible marker
    labelMarker = new google.maps.Marker({
        position: stavanger,  
        map: map,
        visible: false
    });


   
        }, function() {
            checkGeolocationByLoaderAPI(); // Error! Fall back to Loader API.
        });
        
    }
    
    
    
    
     function checkGeolocationByLoaderAPI() {
        if (google.loader.ClientLocation) {
        // setMapCenter(google.loader.ClientLocation.latitude, google.loader.ClientLocation.longitude);
        } else {
            // Unsupported! Show error/warning?
        }
    }
    
    function setMapCenter(latitude, longitude) {
        
          PF('map').getMap().setCenter(new google.maps.LatLng(latitude, longitude));
        
    }