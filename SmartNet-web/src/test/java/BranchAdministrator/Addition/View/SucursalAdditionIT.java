/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Addition.View;

import BranchAdministrator.Addition.Classes.SucursalHasAdditionLayout;
import java.util.List;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class SucursalAdditionIT {
    
    public SucursalAdditionIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUpdateAdditionList method, of class SucursalAddition.
     */
    @Test
    public void testGetUpdateAdditionList() {
        System.out.println("getUpdateAdditionList");
        SucursalAddition instance = new SucursalAddition();
        List<SucursalHasAdditionLayout> expResult = null;
        List<SucursalHasAdditionLayout> result = instance.getUpdateAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateAdditionList method, of class SucursalAddition.
     */
    @Test
    public void testSetUpdateAdditionList() {
        System.out.println("setUpdateAdditionList");
        List<SucursalHasAdditionLayout> updateAdditionList = null;
        SucursalAddition instance = new SucursalAddition();
        instance.setUpdateAdditionList(updateAdditionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedAddition method, of class SucursalAddition.
     */
    @Test
    public void testGetSelectedAddition() {
        System.out.println("getSelectedAddition");
        SucursalAddition instance = new SucursalAddition();
        List<SucursalHasAdditionLayout> expResult = null;
        List<SucursalHasAdditionLayout> result = instance.getSelectedAddition();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedAddition method, of class SucursalAddition.
     */
    @Test
    public void testSetSelectedAddition() {
        System.out.println("setSelectedAddition");
        List<SucursalHasAdditionLayout> selectedAddition = null;
        SucursalAddition instance = new SucursalAddition();
        instance.setSelectedAddition(selectedAddition);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionList method, of class SucursalAddition.
     */
    @Test
    public void testGetAdditionList() {
        System.out.println("getAdditionList");
        SucursalAddition instance = new SucursalAddition();
        List<SucursalHasAdditionLayout> expResult = null;
        List<SucursalHasAdditionLayout> result = instance.getAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAdditionList method, of class SucursalAddition.
     */
    @Test
    public void testSetAdditionList() {
        System.out.println("setAdditionList");
        List<SucursalHasAdditionLayout> additionList = null;
        SucursalAddition instance = new SucursalAddition();
        instance.setAdditionList(additionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class SucursalAddition.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        SucursalAddition instance = new SucursalAddition();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class SucursalAddition.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        SucursalAddition instance = new SucursalAddition();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class SucursalAddition.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        SucursalAddition instance = new SucursalAddition();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToSelectedAddition method, of class SucursalAddition.
     */
    @Test
    public void testAddToSelectedAddition() {
        System.out.println("addToSelectedAddition");
        SucursalHasAdditionLayout sucursalHasAddition = null;
        SucursalAddition instance = new SucursalAddition();
        instance.addToSelectedAddition(sucursalHasAddition);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSelectedAddition method, of class SucursalAddition.
     */
    @Test
    public void testRemoveSelectedAddition() {
        System.out.println("removeSelectedAddition");
        SucursalHasAdditionLayout item = null;
        SucursalAddition instance = new SucursalAddition();
        instance.removeSelectedAddition(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveChanges method, of class SucursalAddition.
     */
    @Test
    public void testSaveChanges() {
        System.out.println("saveChanges");
        SucursalAddition instance = new SucursalAddition();
        String expResult = "failure";
        String result = instance.saveChanges();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deteleUpdateAdditionList method, of class SucursalAddition.
     */
    @Test
    public void testDeteleUpdateAdditionList() {
        System.out.println("deteleUpdateAdditionList");
        SucursalHasAdditionLayout item = null;
        SucursalAddition instance = new SucursalAddition();
        instance.deteleUpdateAdditionList(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
