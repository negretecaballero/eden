/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Inventory.Controller;

import ENUM.TransactionStatus;
import Entities.InventarioHasSucursal;
import Entities.InventarioHasSucursalPK;
import SessionClasses.EdenList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class InventoryControllerImplementationIT {
    
    public InventoryControllerImplementationIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class InventoryControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String viewId = "";
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.initPreRenderView(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUpdateInventoryList method, of class InventoryControllerImplementation.
     */
    @Test
    public void testGetUpdateInventoryList() {
        System.out.println("getUpdateInventoryList");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        EdenList<InventarioHasSucursal> expResult = null;
        EdenList<InventarioHasSucursal> result = instance.getUpdateInventoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToInventoryHasSucursalList method, of class InventoryControllerImplementation.
     */
    @Test
    public void testAddToInventoryHasSucursalList() {
        System.out.println("addToInventoryHasSucursalList");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.addToInventoryHasSucursalList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAddInventoryList method, of class InventoryControllerImplementation.
     */
    @Test
    public void testDeleteAddInventoryList() {
        System.out.println("deleteAddInventoryList");
        InventarioHasSucursalPK key = null;
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.deleteAddInventoryList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveChanges method, of class InventoryControllerImplementation.
     */
    @Test
    public void testSaveChanges() {
        System.out.println("saveChanges");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.saveChanges();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateInventories method, of class InventoryControllerImplementation.
     */
    @Test
    public void testUpdateInventories() {
        System.out.println("updateInventories");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.updateInventories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testDeleteInventory() {
        System.out.println("deleteInventory");
        InventarioHasSucursalPK key = null;
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        TransactionStatus expResult = TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.deleteInventory(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
