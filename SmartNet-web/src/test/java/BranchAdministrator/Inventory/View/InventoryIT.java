/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Inventory.View;

import Entities.InventarioHasSucursal;
import Entities.InventarioHasSucursalPK;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author luisnegrete
 */
public class InventoryIT {
    
    public InventoryIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLazyModel method, of class Inventory.
     */
    @Test
    public void testGetLazyModel() {
        System.out.println("getLazyModel");
        Inventory instance = new Inventory();
        LazyDataModel<InventarioHasSucursal> expResult = null;
        LazyDataModel<InventarioHasSucursal> result = instance.getLazyModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setLazyModel method, of class Inventory.
     */
    @Test
    public void testSetLazyModel() {
        System.out.println("setLazyModel");
        LazyDataModel<InventarioHasSucursal> lazyModel = null;
        Inventory instance = new Inventory();
        instance.setLazyModel(lazyModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUpdateInventory method, of class Inventory.
     */
    @Test
    public void testGetUpdateInventory() {
        System.out.println("getUpdateInventory");
        Inventory instance = new Inventory();
        InventarioHasSucursal expResult = null;
        InventarioHasSucursal result = instance.getUpdateInventory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateInventory method, of class Inventory.
     */
    @Test
    public void testSetUpdateInventory() {
        System.out.println("setUpdateInventory");
        InventarioHasSucursal updateInventory = null;
        Inventory instance = new Inventory();
        instance.setUpdateInventory(updateInventory);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedItem method, of class Inventory.
     */
    @Test
    public void testGetSelectedItem() {
        System.out.println("getSelectedItem");
        Inventory instance = new Inventory();
        InventarioHasSucursal expResult = null;
        InventarioHasSucursal result = instance.getSelectedItem();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedItem method, of class Inventory.
     */
    @Test
    public void testSetSelectedItem() {
        System.out.println("setSelectedItem");
        InventarioHasSucursal selectedItem = null;
        Inventory instance = new Inventory();
        instance.setSelectedItem(selectedItem);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAddInventoryList method, of class Inventory.
     */
    @Test
    public void testGetAddInventoryList() {
        System.out.println("getAddInventoryList");
        Inventory instance = new Inventory();
        List<InventarioHasSucursal> expResult = null;
        List<InventarioHasSucursal> result = instance.getAddInventoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAddInventoryList method, of class Inventory.
     */
    @Test
    public void testSetAddInventoryList() {
        System.out.println("setAddInventoryList");
        List<InventarioHasSucursal> addInventoryList = null;
        Inventory instance = new Inventory();
        instance.setAddInventoryList(addInventoryList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventarioDataModel method, of class Inventory.
     */
    @Test
    public void testGetInventarioDataModel() {
        System.out.println("getInventarioDataModel");
        Inventory instance = new Inventory();
        LazyDataModel expResult = null;
        LazyDataModel result = instance.getInventarioDataModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setInventarioDataModel method, of class Inventory.
     */
    @Test
    public void testSetInventarioDataModel() {
        System.out.println("setInventarioDataModel");
        LazyDataModel<InventarioHasSucursal> inventarioDataModel = null;
        Inventory instance = new Inventory();
        instance.setInventarioDataModel(inventarioDataModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventoryHasSucursalList method, of class Inventory.
     */
    @Test
    public void testGetInventoryHasSucursalList() {
        System.out.println("getInventoryHasSucursalList");
        Inventory instance = new Inventory();
        List<InventarioHasSucursal> expResult = null;
        List<InventarioHasSucursal> result = instance.getInventoryHasSucursalList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class Inventory.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Inventory instance = new Inventory();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToInventoryList method, of class Inventory.
     */
    @Test
    public void testAddToInventoryList() {
        System.out.println("addToInventoryList");
        SelectEvent event = null;
        Inventory instance = new Inventory();
        instance.addToInventoryList(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAddInventoryList method, of class Inventory.
     */
    @Test
    public void testDeleteAddInventoryList() {
        System.out.println("deleteAddInventoryList");
        InventarioHasSucursalPK key = null;
        Inventory instance = new Inventory();
        instance.deleteAddInventoryList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventarioHasSucursalList method, of class Inventory.
     */
    @Test
    public void testGetInventarioHasSucursalList() {
        System.out.println("getInventarioHasSucursalList");
        Inventory instance = new Inventory();
        LazyDataModel<InventarioHasSucursal> expResult = null;
        LazyDataModel<InventarioHasSucursal> result = instance.getInventarioHasSucursalList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addInventory method, of class Inventory.
     */
    @Test
    public void testAddInventory() {
        System.out.println("addInventory");
        ActionEvent event = null;
        Inventory instance = new Inventory();
        instance.addInventory(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class Inventory.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        Inventory instance = new Inventory();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class Inventory.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        String type = "";
        Inventory instance = new Inventory();
        instance.setType(event, type);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteInventory method, of class Inventory.
     */
    @Test
    public void testDeleteInventory() {
        System.out.println("deleteInventory");
        InventarioHasSucursal inventoryHasSucursal = null;
        Inventory instance = new Inventory();
        instance.deleteInventory(inventoryHasSucursal);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateSelection method, of class Inventory.
     */
    @Test
    public void testUpdateSelection() {
        System.out.println("updateSelection");
        SelectEvent event = null;
        Inventory instance = new Inventory();
        instance.updateSelection(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateInventories method, of class Inventory.
     */
    @Test
    public void testUpdateInventories() {
        System.out.println("updateInventories");
        AjaxBehaviorEvent event = null;
        Inventory instance = new Inventory();
        instance.updateInventories(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of quantityChanged method, of class Inventory.
     */
    @Test
    public void testQuantityChanged() {
        System.out.println("quantityChanged");
        Inventory instance = new Inventory();
        instance.quantityChanged();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

	/**
	 * Test of saveChanges method, of class Inventory.
	 */
	@Test()
	public void testSaveChanges() {
		// TODO - implement InventoryIT.testSaveChanges
		throw new UnsupportedOperationException();
	}

    /**
     * Test of saveChanges method, of class Inventory.
     */
   
}
