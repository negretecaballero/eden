/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.Controller;

import BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout;
import ENUM.TransactionStatus;
import Entities.Producto;
import Entities.ProductoPK;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class ProductControllerImplementationIT {
    
    public ProductControllerImplementationIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class ProductControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String viewId = "";
        ProductControllerImplementation instance = new ProductControllerImplementation();
        instance.initPreRenderView(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductsList method, of class ProductControllerImplementation.
     */
    @Test
    public void testGetProductsList() {
        System.out.println("getProductsList");
        List<Producto> products = null;
        String viewId = "";
        ProductControllerImplementation instance = new ProductControllerImplementation();
        List<SucursalHasProductoFrontEndLayout> expResult = null;
        List<SucursalHasProductoFrontEndLayout> result = instance.getProductsList(products, viewId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToBranchProductList method, of class ProductControllerImplementation.
     */
    @Test
    public void testAddToBranchProductList() {
        System.out.println("addToBranchProductList");
        ProductoPK key = null;
        ProductControllerImplementation instance = new ProductControllerImplementation();
        instance.addToBranchProductList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeBranchProductList method, of class ProductControllerImplementation.
     */
    @Test
    public void testRemoveBranchProductList() {
        System.out.println("removeBranchProductList");
        ProductoPK key = null;
        ProductControllerImplementation instance = new ProductControllerImplementation();
        instance.removeBranchProductList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveChanges method, of class ProductControllerImplementation.
     */
    @Test
    public void testSaveChanges() {
        System.out.println("saveChanges");
        ProductControllerImplementation instance = new ProductControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.saveChanges();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFromBranch method, of class ProductControllerImplementation.
     */
    @Test
    public void testDeleteFromBranch() {
        System.out.println("deleteFromBranch");
        ProductoPK key = null;
        ProductControllerImplementation instance = new ProductControllerImplementation();
        instance.deleteFromBranch(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
