/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Product.View;

import BranchAdministrator.Product.Classes.SucursalHasProductoFrontEndLayout;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class ProductIT {
    
    public ProductIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUpdateLazyModel method, of class Product.
     */
    @Test
    public void testGetUpdateLazyModel() {
        System.out.println("getUpdateLazyModel");
        Product instance = new Product();
        List<SucursalHasProductoFrontEndLayout> expResult = null;
        List<SucursalHasProductoFrontEndLayout> result = instance.getUpdateLazyModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateLazyModel method, of class Product.
     */
    @Test
    public void testSetUpdateLazyModel() {
        System.out.println("setUpdateLazyModel");
        List<SucursalHasProductoFrontEndLayout> updateLazyModel = null;
        Product instance = new Product();
        instance.setUpdateLazyModel(updateLazyModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedImages method, of class Product.
     */
    @Test
    public void testGetSelectedImages() {
        System.out.println("getSelectedImages");
        Product instance = new Product();
        List<SucursalHasProductoFrontEndLayout> expResult = null;
        List<SucursalHasProductoFrontEndLayout> result = instance.getSelectedImages();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedImages method, of class Product.
     */
    @Test
    public void testSetSelectedImages() {
        System.out.println("setSelectedImages");
        List<SucursalHasProductoFrontEndLayout> selectedImages = null;
        Product instance = new Product();
        instance.setSelectedImages(selectedImages);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProducts method, of class Product.
     */
    @Test
    public void testGetProducts() {
        System.out.println("getProducts");
        Product instance = new Product();
        List<SucursalHasProductoFrontEndLayout> expResult = null;
        List<SucursalHasProductoFrontEndLayout> result = instance.getProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProducts method, of class Product.
     */
    @Test
    public void testSetProducts() {
        System.out.println("setProducts");
        List<SucursalHasProductoFrontEndLayout> products = null;
        Product instance = new Product();
        instance.setProducts(products);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class Product.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Product instance = new Product();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class Product.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        Product instance = new Product();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class Product.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        String action = "";
        Product instance = new Product();
        instance.setType(event, action);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToList method, of class Product.
     */
    @Test
    public void testAddToList() {
        System.out.println("addToList");
        SucursalHasProductoFrontEndLayout item = null;
        Product instance = new Product();
        instance.addToList(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFromProductList method, of class Product.
     */
    @Test
    public void testDeleteFromProductList() {
        System.out.println("deleteFromProductList");
        SucursalHasProductoFrontEndLayout item = null;
        Product instance = new Product();
        instance.deleteFromProductList(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class Product.
     */
    @Test
    public void testUpdate_0args() {
        System.out.println("update");
        Product instance = new Product();
        String expResult = "failure";
        String result = instance.update();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of delete method, of class Product.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");
        SucursalHasProductoFrontEndLayout item = null;
        Product instance = new Product();
        instance.delete(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class Product.
     */
    @Test
    public void testUpdate_ActionEvent() {
        System.out.println("update");
        ActionEvent event = null;
        Product instance = new Product();
        instance.update(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
