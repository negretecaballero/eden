/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Sale.Controller;

import ENUM.TransactionStatus;
import Entities.SalePK;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class SucursalSaleViewControllerImplementationIT {
    
    public SucursalSaleViewControllerImplementationIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class SucursalSaleViewControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String viewId = "";
        SucursalSaleViewControllerImplementation instance = new SucursalSaleViewControllerImplementation();
        instance.initPreRenderView(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToSelectedList method, of class SucursalSaleViewControllerImplementation.
     */
    @Test
    public void testAddToSelectedList() {
        System.out.println("addToSelectedList");
        SalePK key = null;
        SucursalSaleViewControllerImplementation instance = new SucursalSaleViewControllerImplementation();
        instance.addToSelectedList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveChanges method, of class SucursalSaleViewControllerImplementation.
     */
    @Test
    public void testSaveChanges() {
        System.out.println("saveChanges");
        SucursalSaleViewControllerImplementation instance = new SucursalSaleViewControllerImplementation();
        TransactionStatus expResult = TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.saveChanges();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSelectedList method, of class SucursalSaleViewControllerImplementation.
     */
    @Test
    public void testRemoveSelectedList() {
        System.out.println("removeSelectedList");
        SalePK key = null;
        SucursalSaleViewControllerImplementation instance = new SucursalSaleViewControllerImplementation();
        instance.removeSelectedList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeUpdateList method, of class SucursalSaleViewControllerImplementation.
     */
    @Test
    public void testRemoveUpdateList() {
        System.out.println("removeUpdateList");
        SalePK key = null;
        SucursalSaleViewControllerImplementation instance = new SucursalSaleViewControllerImplementation();
        TransactionStatus expResult = TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.removeUpdateList(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
