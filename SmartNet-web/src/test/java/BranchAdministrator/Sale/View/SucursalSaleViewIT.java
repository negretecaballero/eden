/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAdministrator.Sale.View;

import BranchAdministrator.Sale.Classes.SucursalHasSaleLayout;
import Entities.SalePK;
import java.util.List;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class SucursalSaleViewIT {
    
    public SucursalSaleViewIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class SucursalSaleView.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        SucursalSaleView instance = new SucursalSaleView();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUpdateSaleList method, of class SucursalSaleView.
     */
    @Test
    public void testGetUpdateSaleList() {
        System.out.println("getUpdateSaleList");
        SucursalSaleView instance = new SucursalSaleView();
        List<SucursalHasSaleLayout> expResult = null;
        List<SucursalHasSaleLayout> result = instance.getUpdateSaleList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateSaleList method, of class SucursalSaleView.
     */
    @Test
    public void testSetUpdateSaleList() {
        System.out.println("setUpdateSaleList");
        List<SucursalHasSaleLayout> updateSaleList = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.setUpdateSaleList(updateSaleList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedSale method, of class SucursalSaleView.
     */
    @Test
    public void testGetSelectedSale() {
        System.out.println("getSelectedSale");
        SucursalSaleView instance = new SucursalSaleView();
        List<SucursalHasSaleLayout> expResult = null;
        List<SucursalHasSaleLayout> result = instance.getSelectedSale();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedSale method, of class SucursalSaleView.
     */
    @Test
    public void testSetSelectedSale() {
        System.out.println("setSelectedSale");
        List<SucursalHasSaleLayout> selectedSale = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.setSelectedSale(selectedSale);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSaleList method, of class SucursalSaleView.
     */
    @Test
    public void testGetSaleList() {
        System.out.println("getSaleList");
        SucursalSaleView instance = new SucursalSaleView();
        List<SucursalHasSaleLayout> expResult = null;
        List<SucursalHasSaleLayout> result = instance.getSaleList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSaleList method, of class SucursalSaleView.
     */
    @Test
    public void testSetSaleList() {
        System.out.println("setSaleList");
        List<SucursalHasSaleLayout> saleList = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.setSaleList(saleList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class SucursalSaleView.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToSelectedSale method, of class SucursalSaleView.
     */
    @Test
    public void testAddToSelectedSale() {
        System.out.println("addToSelectedSale");
        SalePK key = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.addToSelectedSale(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSelectedSale method, of class SucursalSaleView.
     */
    @Test
    public void testDeleteSelectedSale() {
        System.out.println("deleteSelectedSale");
        SucursalHasSaleLayout sale = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.deleteSelectedSale(sale);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveChanges method, of class SucursalSaleView.
     */
    @Test
    public void testSaveChanges() {
        System.out.println("saveChanges");
        SucursalSaleView instance = new SucursalSaleView();
        String expResult = "failure";
        String result = instance.saveChanges();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFromUpdateList method, of class SucursalSaleView.
     */
    @Test
    public void testDeleteFromUpdateList() {
        System.out.println("deleteFromUpdateList");
        SucursalHasSaleLayout item = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.deleteFromUpdateList(item);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class SucursalSaleView.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        SucursalSaleView instance = new SucursalSaleView();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
