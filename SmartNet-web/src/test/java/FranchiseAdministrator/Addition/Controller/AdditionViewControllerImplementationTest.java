/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Addition.Controller;

import ENUM.TransactionStatus;
import Entities.Addition;
import Entities.AdditionConsumesInventario;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class AdditionViewControllerImplementationTest {
    
    public AdditionViewControllerImplementationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String viewId = "";
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.initPreRenderView(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToInventoryList method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testAddToInventoryList() {
        System.out.println("addToInventoryList");
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.addToInventoryList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeInventoryList method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testRemoveInventoryList() {
        System.out.println("removeInventoryList");
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.removeInventoryList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of additionPostValidate method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testAdditionPostValidate() {
        System.out.println("additionPostValidate");
        String name = "";
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.additionPostValidate(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createAddition method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testCreateAddition() {
        System.out.println("createAddition");
        String name = "";
        double price = 0.0;
        String description = "";
        List<AdditionConsumesInventario> additionConsumesInventoryList = null;
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.createAddition(name, price, description, additionConsumesInventoryList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAdditions method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testDeleteAdditions() {
        System.out.println("deleteAdditions");
        List<Addition> deleteList = null;
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        instance.deleteAdditions(deleteList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateAddition method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testUpdateAddition() {
        System.out.println("updateAddition");
        String name = "";
        double price = 0.0;
        String description = "";
        List<AdditionConsumesInventario> additionConsumesInventarioList = null;
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.updateAddition(name, price, description, additionConsumesInventarioList);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateAddition method, of class AdditionViewControllerImplementation.
     */
    @Test
    public void testPostValidateUpdateAddition() {
        System.out.println("postValidateUpdateAddition");
        String name = "";
        AdditionViewControllerImplementation instance = new AdditionViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.postValidateUpdateAddition(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
