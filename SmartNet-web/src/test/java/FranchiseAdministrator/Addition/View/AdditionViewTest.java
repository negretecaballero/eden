/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Addition.View;

import CDIBeans.DesplegableInventarioInterface;
import Entities.Addition;
import Entities.AdditionConsumesInventario;
import java.util.List;
import java.util.Vector;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class AdditionViewTest {
    
    public AdditionViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getEditAddition method, of class AdditionView.
     */
    @Test
    public void testGetEditAddition() {
        System.out.println("getEditAddition");
        AdditionView instance = new AdditionView();
        Addition expResult = null;
        Addition result = instance.getEditAddition();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setEditAddition method, of class AdditionView.
     */
    @Test
    public void testSetEditAddition() {
        System.out.println("setEditAddition");
        Addition editAddition = null;
        AdditionView instance = new AdditionView();
        instance.setEditAddition(editAddition);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteAdditionList method, of class AdditionView.
     */
    @Test
    public void testGetDeleteAdditionList() {
        System.out.println("getDeleteAdditionList");
        AdditionView instance = new AdditionView();
        List<Addition> expResult = null;
        List<Addition> result = instance.getDeleteAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteAdditionList method, of class AdditionView.
     */
    @Test
    public void testSetDeleteAdditionList() {
        System.out.println("setDeleteAdditionList");
        List<Addition> deleteAdditionList = null;
        AdditionView instance = new AdditionView();
        instance.setDeleteAdditionList(deleteAdditionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDesplegableInventario method, of class AdditionView.
     */
    @Test
    public void testGetDesplegableInventario() {
        System.out.println("getDesplegableInventario");
        AdditionView instance = new AdditionView();
        DesplegableInventarioInterface expResult = null;
        DesplegableInventarioInterface result = instance.getDesplegableInventario();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDesplegableInventario method, of class AdditionView.
     */
    @Test
    public void testSetDesplegableInventario() {
        System.out.println("setDesplegableInventario");
        DesplegableInventarioInterface desplegableInventario = null;
        AdditionView instance = new AdditionView();
        instance.setDesplegableInventario(desplegableInventario);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescription method, of class AdditionView.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        AdditionView instance = new AdditionView();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDescription method, of class AdditionView.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        AdditionView instance = new AdditionView();
        instance.setDescription(description);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getName method, of class AdditionView.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        AdditionView instance = new AdditionView();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class AdditionView.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        AdditionView instance = new AdditionView();
        instance.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getValue method, of class AdditionView.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        AdditionView instance = new AdditionView();
        double expResult = 0.0;
        double result = instance.getValue();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setValue method, of class AdditionView.
     */
    @Test
    public void testSetValue() {
        System.out.println("setValue");
        double value = 0.0;
        AdditionView instance = new AdditionView();
        instance.setValue(value);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionConsumesInventario method, of class AdditionView.
     */
    @Test
    public void testGetAdditionConsumesInventario() {
        System.out.println("getAdditionConsumesInventario");
        AdditionView instance = new AdditionView();
        List<AdditionConsumesInventario> expResult = null;
        List<AdditionConsumesInventario> result = instance.getAdditionConsumesInventario();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAdditionConsumesInventario method, of class AdditionView.
     */
    @Test
    public void testSetAdditionConsumesInventario() {
        System.out.println("setAdditionConsumesInventario");
        List<AdditionConsumesInventario> additionConsumesInventario = null;
        AdditionView instance = new AdditionView();
        instance.setAdditionConsumesInventario(additionConsumesInventario);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionList method, of class AdditionView.
     */
    @Test
    public void testGetAdditionList() {
        System.out.println("getAdditionList");
        AdditionView instance = new AdditionView();
        Vector<Addition> expResult = null;
        Vector<Addition> result = instance.getAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAdditionList method, of class AdditionView.
     */
    @Test
    public void testSetAdditionList() {
        System.out.println("setAdditionList");
        Vector<Addition> additionList = null;
        AdditionView instance = new AdditionView();
        instance.setAdditionList(additionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class AdditionView.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        AdditionView instance = new AdditionView();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setActionType method, of class AdditionView.
     */
    @Test
    public void testSetActionType() {
        System.out.println("setActionType");
        ComponentSystemEvent event = null;
        AdditionView instance = new AdditionView();
        instance.setActionType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class AdditionView.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        AdditionView instance = new AdditionView();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAdditionConsumesInventario method, of class AdditionView.
     */
    @Test
    public void testDeleteAdditionConsumesInventario() {
        System.out.println("deleteAdditionConsumesInventario");
        AdditionView instance = new AdditionView();
        instance.deleteAdditionConsumesInventario();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAdditionConsumesInventario method, of class AdditionView.
     */
    @Test
    public void testAddAdditionConsumesInventario() {
        System.out.println("addAdditionConsumesInventario");
        AdditionView instance = new AdditionView();
        instance.addAdditionConsumesInventario();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAdditionConsumesInventarioList method, of class AdditionView.
     */
    @Test
    public void testDeleteAdditionConsumesInventarioList() {
        System.out.println("deleteAdditionConsumesInventarioList");
        AdditionView instance = new AdditionView();
        instance.deleteAdditionConsumesInventarioList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkExcistence method, of class AdditionView.
     */
    @Test
    public void testCheckExcistence() {
        System.out.println("checkExcistence");
        ComponentSystemEvent event = null;
        AdditionView instance = new AdditionView();
        instance.checkExcistence(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateAddition method, of class AdditionView.
     */
    @Test
    public void testPostValidateUpdateAddition() {
        System.out.println("postValidateUpdateAddition");
        ComponentSystemEvent event = null;
        AdditionView instance = new AdditionView();
        instance.postValidateUpdateAddition(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of create method, of class AdditionView.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        ActionEvent event = null;
        AdditionView instance = new AdditionView();
        instance.create(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteItems method, of class AdditionView.
     */
    @Test
    public void testDeleteItems() {
        System.out.println("deleteItems");
        AjaxBehaviorEvent event = null;
        AdditionView instance = new AdditionView();
        instance.deleteItems(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of edit method, of class AdditionView.
     */
    @Test
    public void testEdit() {
        System.out.println("edit");
        AdditionView instance = new AdditionView();
        String expResult = "failure";
        String result = instance.edit();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteImage method, of class AdditionView.
     */
    @Test
    public void testDeleteImage() {
        System.out.println("deleteImage");
        ActionEvent event = null;
        String image = "";
        AdditionView instance = new AdditionView();
        instance.deleteImage(event, image);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
