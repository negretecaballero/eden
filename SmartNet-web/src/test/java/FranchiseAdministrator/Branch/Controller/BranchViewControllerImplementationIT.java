/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.Controller;

import Clases.Direccion;
import ENUM.TransactionStatus;
import Entities.CityPK;
import FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class BranchViewControllerImplementationIT {
    
    public BranchViewControllerImplementationIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String viewId = "";
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        instance.initPreRenderView(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSucursalHasDayList method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testGetSucursalHasDayList() {
        System.out.println("getSucursalHasDayList");
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        List<SucursalHasDateLayout> expResult = null;
        List<SucursalHasDateLayout> result = instance.getSucursalHasDayList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUpload method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testHandleFileUpload() {
        System.out.println("handleFileUpload");
        FileUploadEvent event = null;
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        instance.handleFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of stateChanged method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testStateChanged() {
        System.out.println("stateChanged");
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        instance.stateChanged();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of cityChanged method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testCityChanged() {
        System.out.println("cityChanged");
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        instance.cityChanged();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createBranch method, of class BranchViewControllerImplementation.
     */
    @Test
    public void testCreateBranch() {
        System.out.println("createBranch");
        double alcance = 0.0;
        int minimalAmount = 0;
        int deliveryPrice = 0;
        List<SucursalHasDateLayout> selectedDays = null;
        String user = "";
        Direccion address = null;
        boolean sucursalAdministrador = false;
        CityPK citypk = null;
        BranchViewControllerImplementation instance = new BranchViewControllerImplementation();
        TransactionStatus expResult = TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.createBranch(alcance, minimalAmount, deliveryPrice, selectedDays, user, address, sucursalAdministrador, citypk);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
