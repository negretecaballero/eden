/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Branch.View;

import Clases.Direccion;
import Entities.CityPK;
import FranchiseAdministrator.Branch.Classes.SucursalHasDateLayout;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author luisnegrete
 */
public class BranchIT {
    
    public BranchIT() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCity method, of class Branch.
     */
    @Test
    public void testGetCity() {
        System.out.println("getCity");
        Branch instance = new Branch();
        String expResult = null;
        String result = instance.getCity();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCity method, of class Branch.
     */
    @Test
    public void testSetCity() {
        System.out.println("setCity");
        String city = "";
        Branch instance = new Branch();
        instance.setCity(city);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getState method, of class Branch.
     */
    @Test
    public void testGetState() {
        System.out.println("getState");
        Branch instance = new Branch();
        String expResult = null;
        String result = instance.getState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setState method, of class Branch.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        String state = "";
        Branch instance = new Branch();
        instance.setState(state);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class Branch.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        Branch instance = new Branch();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class Branch.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        Branch instance = new Branch();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDireccion method, of class Branch.
     */
    @Test
    public void testGetDireccion() {
        System.out.println("getDireccion");
        Branch instance = new Branch();
        Direccion expResult = null;
        Direccion result = instance.getDireccion();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDireccion method, of class Branch.
     */
    @Test
    public void testSetDireccion() {
        System.out.println("setDireccion");
        Direccion direccion = null;
        Branch instance = new Branch();
        instance.setDireccion(direccion);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAlcance method, of class Branch.
     */
    @Test
    public void testGetAlcance() {
        System.out.println("getAlcance");
        Branch instance = new Branch();
        double expResult = 0.0;
        double result = instance.getAlcance();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAlcance method, of class Branch.
     */
    @Test
    public void testSetAlcance() {
        System.out.println("setAlcance");
        double alcance = 0.0;
        Branch instance = new Branch();
        instance.setAlcance(alcance);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getMinimalAmount method, of class Branch.
     */
    @Test
    public void testGetMinimalAmount() {
        System.out.println("getMinimalAmount");
        Branch instance = new Branch();
        int expResult = 0;
        int result = instance.getMinimalAmount();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setMinimalAmount method, of class Branch.
     */
    @Test
    public void testSetMinimalAmount() {
        System.out.println("setMinimalAmount");
        int minimalAmount = 0;
        Branch instance = new Branch();
        instance.setMinimalAmount(minimalAmount);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeliveryPrice method, of class Branch.
     */
    @Test
    public void testGetDeliveryPrice() {
        System.out.println("getDeliveryPrice");
        Branch instance = new Branch();
        int expResult = 0;
        int result = instance.getDeliveryPrice();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeliveryPrice method, of class Branch.
     */
    @Test
    public void testSetDeliveryPrice() {
        System.out.println("setDeliveryPrice");
        int deliveryPrice = 0;
        Branch instance = new Branch();
        instance.setDeliveryPrice(deliveryPrice);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUser method, of class Branch.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        Branch instance = new Branch();
        String expResult = null;
        String result = instance.getUser();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUser method, of class Branch.
     */
    @Test
    public void testSetUser() {
        System.out.println("setUser");
        String user = "";
        Branch instance = new Branch();
        instance.setUser(user);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSucursalAdministrator method, of class Branch.
     */
    @Test
    public void testGetSucursalAdministrator() {
        System.out.println("getSucursalAdministrator");
        Branch instance = new Branch();
        boolean expResult = false;
        boolean result = instance.getSucursalAdministrator();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSucursalAdministrator method, of class Branch.
     */
    @Test
    public void testSetSucursalAdministrator() {
        System.out.println("setSucursalAdministrator");
        boolean sucursalAdministrator = false;
        Branch instance = new Branch();
        instance.setSucursalAdministrator(sucursalAdministrator);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDayModel method, of class Branch.
     */
    @Test
    public void testGetDayModel() {
        System.out.println("getDayModel");
        Branch instance = new Branch();
        LazyDataModel<SucursalHasDateLayout> expResult = null;
        LazyDataModel<SucursalHasDateLayout> result = instance.getDayModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDayModel method, of class Branch.
     */
    @Test
    public void testSetDayModel() {
        System.out.println("setDayModel");
        LazyDataModel<SucursalHasDateLayout> dayModel = null;
        Branch instance = new Branch();
        instance.setDayModel(dayModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedDays method, of class Branch.
     */
    @Test
    public void testGetSelectedDays() {
        System.out.println("getSelectedDays");
        Branch instance = new Branch();
        List<SucursalHasDateLayout> expResult = null;
        List<SucursalHasDateLayout> result = instance.getSelectedDays();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedDays method, of class Branch.
     */
    @Test
    public void testSetSelectedDays() {
        System.out.println("setSelectedDays");
        List<SucursalHasDateLayout> selectedDays = null;
        Branch instance = new Branch();
        instance.setSelectedDays(selectedDays);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of daySelectEvent method, of class Branch.
     */
    @Test
    public void testDaySelectEvent() {
        System.out.println("daySelectEvent");
        SelectEvent event = null;
        Branch instance = new Branch();
        instance.daySelectEvent(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of changeCity method, of class Branch.
     */
    @Test
    public void testChangeCity() {
        System.out.println("changeCity");
        AjaxBehaviorEvent event = null;
        Branch instance = new Branch();
        instance.changeCity(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of changeState method, of class Branch.
     */
    @Test
    public void testChangeState() {
        System.out.println("changeState");
        AjaxBehaviorEvent event = null;
        Branch instance = new Branch();
        instance.changeState(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCityPK method, of class Branch.
     */
    @Test
    public void testGetCityPK() {
        System.out.println("getCityPK");
        Branch instance = new Branch();
        CityPK expResult = null;
        CityPK result = instance.getCityPK();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCityPK method, of class Branch.
     */
    @Test
    public void testSetCityPK() {
        System.out.println("setCityPK");
        CityPK cityPK = null;
        Branch instance = new Branch();
        instance.setCityPK(cityPK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getState_id method, of class Branch.
     */
    @Test
    public void testGetState_id() {
        System.out.println("getState_id");
        Branch instance = new Branch();
        String expResult = null;
        String result = instance.getState_id();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setState_id method, of class Branch.
     */
    @Test
    public void testSetState_id() {
        System.out.println("setState_id");
        String state_id = "";
        Branch instance = new Branch();
        instance.setState_id(state_id);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class Branch.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Branch instance = new Branch();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSucursal method, of class Branch.
     */
    @Test
    public void testAddSucursal() {
        System.out.println("addSucursal");
        Branch instance = new Branch();
        String expResult = "failure";
        String result = instance.addSucursal();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of geolocateChangeCity method, of class Branch.
     */
    @Test
    public void testGeolocateChangeCity() {
        System.out.println("geolocateChangeCity");
        Branch instance = new Branch();
        instance.geolocateChangeCity();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
