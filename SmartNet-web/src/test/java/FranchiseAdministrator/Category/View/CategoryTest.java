/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Category.View;

import Entities.Categoria;
import FranchiseAdministrator.Category.Classes.CategoryLayout;
import java.util.List;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultTreeNode;

/**
 *
 * @author luisnegrete
 */
public class CategoryTest {
    
    public CategoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCategories method, of class Category.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        Category instance = new Category();
        List<CategoryLayout> expResult = null;
        List<CategoryLayout> result = instance.getCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCategories method, of class Category.
     */
    @Test
    public void testSetCategories() {
        System.out.println("setCategories");
        List<CategoryLayout> categories = null;
        Category instance = new Category();
        instance.setCategories(categories);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getUpdateCategory method, of class Category.
     */
    @Test
    public void testGetUpdateCategory() {
        System.out.println("getUpdateCategory");
        Category instance = new Category();
        Categoria expResult = null;
        Categoria result = instance.getUpdateCategory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateCategory method, of class Category.
     */
    @Test
    public void testSetUpdateCategory() {
        System.out.println("setUpdateCategory");
        Categoria updateCategory = null;
        Category instance = new Category();
        instance.setUpdateCategory(updateCategory);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getRoot method, of class Category.
     */
    @Test
    public void testGetRoot() {
        System.out.println("getRoot");
        Category instance = new Category();
        DefaultTreeNode expResult = null;
        DefaultTreeNode result = instance.getRoot();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setRoot method, of class Category.
     */
    @Test
    public void testSetRoot() {
        System.out.println("setRoot");
        DefaultTreeNode root = null;
        Category instance = new Category();
        instance.setRoot(root);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDescription method, of class Category.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        Category instance = new Category();
        instance.setDescription(description);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescription method, of class Category.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Category instance = new Category();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedEdit method, of class Category.
     */
    @Test
    public void testGetSelectedEdit() {
        System.out.println("getSelectedEdit");
        Category instance = new Category();
        Categoria expResult = null;
        Categoria result = instance.getSelectedEdit();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedEdit method, of class Category.
     */
    @Test
    public void testSetSelectedEdit() {
        System.out.println("setSelectedEdit");
        Categoria selectedEdit = null;
        Category instance = new Category();
        instance.setSelectedEdit(selectedEdit);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getCategoryList method, of class Category.
     */
    @Test
    public void testGetCategoryList() {
        System.out.println("getCategoryList");
        Category instance = new Category();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getCategoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCategoryList method, of class Category.
     */
    @Test
    public void testSetCategoryList() {
        System.out.println("setCategoryList");
        List<Categoria> categoryList = null;
        Category instance = new Category();
        instance.setCategoryList(categoryList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteCategoryList method, of class Category.
     */
    @Test
    public void testSetDeleteCategoryList() {
        System.out.println("setDeleteCategoryList");
        List<Categoria> deleteCategoryList = null;
        Category instance = new Category();
        instance.setDeleteCategoryList(deleteCategoryList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteCategoryList method, of class Category.
     */
    @Test
    public void testGetDeleteCategoryList() {
        System.out.println("getDeleteCategoryList");
        Category instance = new Category();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getDeleteCategoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedCategories method, of class Category.
     */
    @Test
    public void testGetSelectedCategories() {
        System.out.println("getSelectedCategories");
        Category instance = new Category();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getSelectedCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedCategories method, of class Category.
     */
    @Test
    public void testSetSelectedCategories() {
        System.out.println("setSelectedCategories");
        List<Categoria> selectedCategories = null;
        Category instance = new Category();
        instance.setSelectedCategories(selectedCategories);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCat method, of class Category.
     */
    @Test
    public void testGetCat() {
        System.out.println("getCat");
        Category instance = new Category();
        String expResult = "";
        String result = instance.getCat();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCat method, of class Category.
     */
    @Test
    public void testSetCat() {
        System.out.println("setCat");
        String cat = "";
        Category instance = new Category();
        instance.setCat(cat);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class Category.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Category instance = new Category();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of preDestroy method, of class Category.
     */
    @Test
    public void testPreDestroy() {
        System.out.println("preDestroy");
        Category instance = new Category();
        instance.preDestroy();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ClassType method, of class Category.
     */
    @Test
    public void testClassType() {
        System.out.println("ClassType");
        String clastype = "";
        Category instance = new Category();
        instance.ClassType(clastype);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of validar method, of class Category.
     */
    @Test
    public void testValidar() {
        System.out.println("validar");
        String a = "";
        Category instance = new Category();
        Short expResult = null;
        Short result = instance.validar(a);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUpload method, of class Category.
     */
    @Test
    public void testHandleFileUpload() {
        System.out.println("handleFileUpload");
        FileUploadEvent event = null;
        Category instance = new Category();
        instance.handleFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removePicture method, of class Category.
     */
    @Test
    public void testRemovePicture() {
        System.out.println("removePicture");
        AjaxBehavior behaviour = null;
        Category instance = new Category();
        instance.removePicture(behaviour);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class Category.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        Category instance = new Category();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of editCategory method, of class Category.
     */
    @Test
    public void testEditCategory() {
        System.out.println("editCategory");
        ActionEvent event = null;
        Category instance = new Category();
        instance.editCategory(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of agregar method, of class Category.
     */
    @Test
    public void testAgregar() {
        System.out.println("agregar");
        ActionEvent event = null;
        Category instance = new Category();
        instance.agregar(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkCategoryName method, of class Category.
     */
    @Test
    public void testCheckCategoryName() {
        System.out.println("checkCategoryName");
        ComponentSystemEvent event = null;
        Category instance = new Category();
        instance.checkCategoryName(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkSelectedList method, of class Category.
     */
    @Test
    public void testCheckSelectedList() {
        System.out.println("checkSelectedList");
        Categoria cat = null;
        Category instance = new Category();
        instance.checkSelectedList(cat);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of editar method, of class Category.
     */
    @Test
    public void testEditar() {
        System.out.println("editar");
        ActionEvent event = null;
        Category instance = new Category();
        instance.editar(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteCategories method, of class Category.
     */
    @Test
    public void testDeleteCategories() {
        System.out.println("deleteCategories");
        ActionEvent event = null;
        Category instance = new Category();
        instance.deleteCategories(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dropEvent method, of class Category.
     */
    @Test
    public void testDropEvent() {
        System.out.println("dropEvent");
        DragDropEvent event = null;
        Category instance = new Category();
        instance.dropEvent(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of imageChanged method, of class Category.
     */
    @Test
    public void testImageChanged() {
        System.out.println("imageChanged");
        Category instance = new Category();
        instance.imageChanged();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteImage method, of class Category.
     */
    @Test
    public void testDeleteImage() {
        System.out.println("deleteImage");
        AjaxBehaviorEvent event = null;
        Category instance = new Category();
        instance.deleteImage(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getListCategories method, of class Category.
     */
    @Test
    public void testGetListCategories() {
        System.out.println("getListCategories");
        Category instance = new Category();
        List<CategoryLayout> expResult = null;
        List<CategoryLayout> result = instance.getListCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteCategoryFromFranchise method, of class Category.
     */
    @Test
    public void testDeleteCategoryFromFranchise() {
        System.out.println("deleteCategoryFromFranchise");
        Categoria category = null;
        Category instance = new Category();
        instance.deleteCategoryFromFranchise(category);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateBusinessCategory method, of class Category.
     */
    @Test
    public void testUpdateBusinessCategory() {
        System.out.println("updateBusinessCategory");
        ActionEvent event = null;
        Category instance = new Category();
        instance.updateBusinessCategory(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeImage method, of class Category.
     */
    @Test
    public void testRemoveImage() {
        System.out.println("removeImage");
        AjaxBehaviorEvent event = null;
        Category instance = new Category();
        instance.removeImage(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
