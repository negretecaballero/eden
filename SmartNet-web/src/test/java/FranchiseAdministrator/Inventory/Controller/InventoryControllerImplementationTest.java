/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.Controller;

import Entities.Inventario;
import Entities.InventarioPK;
import Entities.SimilarInventario;
import java.util.List;
import java.util.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class InventoryControllerImplementationTest {
    
    public InventoryControllerImplementationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of checkNameExistence method, of class InventoryControllerImplementation.
     */
    @Test
    public void testCheckNameExistence() {
        System.out.println("checkNameExistence");
        String name = "";
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkNameExistence(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSimilarInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testAddSimilarInventory() {
        System.out.println("addSimilarInventory");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.addSimilarInventory();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkSimilarExistence method, of class InventoryControllerImplementation.
     */
    @Test
    public void testCheckSimilarExistence() {
        System.out.println("checkSimilarExistence");
        InventarioPK key = null;
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkSimilarExistence(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilar method, of class InventoryControllerImplementation.
     */
    @Test
    public void testDeleteSimilar() {
        System.out.println("deleteSimilar");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.deleteSimilar();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testAddInventory() {
        System.out.println("addInventory");
        String name = "";
        String description = "";
        boolean enabled = false;
        List<SimilarInventario> similarList = null;
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.addInventory(name, description, enabled, similarList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of inventoryArray method, of class InventoryControllerImplementation.
     */
    @Test
    public void testInventoryArray() {
        System.out.println("inventoryArray");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        Vector<Inventario> expResult = null;
        Vector<Inventario> result = instance.inventoryArray();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testDeleteInventory() {
        System.out.println("deleteInventory");
        List<Inventario> deleteList = null;
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.deleteInventory(deleteList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testGetInventory() {
        System.out.println("getInventory");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        Inventario expResult = null;
        Inventario result = instance.getInventory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of initSimilarInventoryList method, of class InventoryControllerImplementation.
     */
    @Test
    public void testInitSimilarInventoryList() {
        System.out.println("initSimilarInventoryList");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.initSimilarInventoryList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInvSelectOneMenu method, of class InventoryControllerImplementation.
     */
    @Test
    public void testGetInvSelectOneMenu() {
        System.out.println("getInvSelectOneMenu");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        Vector<Inventario> expResult = null;
        Vector<Inventario> result = instance.getInvSelectOneMenu();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateInventory method, of class InventoryControllerImplementation.
     */
    @Test
    public void testUpdateInventory() {
        System.out.println("updateInventory");
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.updateInventory();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of initPreRenderView method, of class InventoryControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        String view = "";
        InventoryControllerImplementation instance = new InventoryControllerImplementation();
        instance.initPreRenderView(view);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
