/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Inventory.View;

import Entities.Inventario;
import Entities.InventarioPK;
import Entities.SimilarInventario;
import FranchiseAdministrator.Inventory.Classes.SimilarInventarioLayoutInterface;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisnegrete
 */
public class InventoryTest {
    
    public InventoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSelectedInventory method, of class Inventory.
     */
    @Test
    public void testGetSelectedInventory() {
        System.out.println("getSelectedInventory");
        Inventory instance = new Inventory();
        InventarioPK expResult = null;
        InventarioPK result = instance.getSelectedInventory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedInventory method, of class Inventory.
     */
    @Test
    public void testSetSelectedInventory() {
        System.out.println("setSelectedInventory");
        InventarioPK selectedInventory = null;
        Inventory instance = new Inventory();
        instance.setSelectedInventory(selectedInventory);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getEnabled method, of class Inventory.
     */
    @Test
    public void testGetEnabled() {
        System.out.println("getEnabled");
        Inventory instance = new Inventory();
        boolean expResult = false;
        boolean result = instance.getEnabled();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setEnabled method, of class Inventory.
     */
    @Test
    public void testSetEnabled() {
        System.out.println("setEnabled");
        boolean enabled = false;
        Inventory instance = new Inventory();
        instance.setEnabled(enabled);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteList method, of class Inventory.
     */
    @Test
    public void testGetDeleteList() {
        System.out.println("getDeleteList");
        Inventory instance = new Inventory();
        List<Inventario> expResult = null;
        List<Inventario> result = instance.getDeleteList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteList method, of class Inventory.
     */
    @Test
    public void testSetDeleteList() {
        System.out.println("setDeleteList");
        List<Inventario> deleteList = null;
        Inventory instance = new Inventory();
        instance.setDeleteList(deleteList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventarios method, of class Inventory.
     */
    @Test
    public void testGetInventarios() {
        System.out.println("getInventarios");
        Inventory instance = new Inventory();
        Vector<Inventario> expResult = null;
        Vector<Inventario> result = instance.getInventarios();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setInventarios method, of class Inventory.
     */
    @Test
    public void testSetInventarios() {
        System.out.println("setInventarios");
        List<Inventario> inventarios = null;
        Inventory instance = new Inventory();
        instance.setInventarios(inventarios);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSimilarInventarioLayout method, of class Inventory.
     */
    @Test
    public void testGetSimilarInventarioLayout() {
        System.out.println("getSimilarInventarioLayout");
        Inventory instance = new Inventory();
        List<SimilarInventarioLayoutInterface> expResult = null;
        List<SimilarInventarioLayoutInterface> result = instance.getSimilarInventarioLayout();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSimilarInventarioLayout method, of class Inventory.
     */
    @Test
    public void testSetSimilarInventarioLayout() {
        System.out.println("setSimilarInventarioLayout");
        List<SimilarInventarioLayoutInterface> similarInventarioLayout = null;
        Inventory instance = new Inventory();
        instance.setSimilarInventarioLayout(similarInventarioLayout);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventoryList method, of class Inventory.
     */
    @Test
    public void testGetInventoryList() {
        System.out.println("getInventoryList");
        Inventory instance = new Inventory();
        HashSet<Inventario> expResult = null;
        HashSet<Inventario> result = instance.getInventoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setInventoryList method, of class Inventory.
     */
    @Test
    public void testSetInventoryList() {
        System.out.println("setInventoryList");
        HashSet<Inventario> inventoryList = null;
        Inventory instance = new Inventory();
        instance.setInventoryList(inventoryList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventarioByFranchise method, of class Inventory.
     */
    @Test
    public void testGetInventarioByFranchise() {
        System.out.println("getInventarioByFranchise");
        Inventory instance = new Inventory();
        Vector<Inventario> expResult = null;
        Vector<Inventario> result = instance.getInventarioByFranchise();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class Inventory.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        Inventory instance = new Inventory();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSimilarInventario method, of class Inventory.
     */
    @Test
    public void testGetSimilarInventario() {
        System.out.println("getSimilarInventario");
        Inventory instance = new Inventory();
        List<SimilarInventario> expResult = new java.util.ArrayList<Entities.SimilarInventario>();
        List<SimilarInventario> result = instance.getSimilarInventario();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSimilarInventario method, of class Inventory.
     */
    @Test
    public void testSetSimilarInventario() {
        System.out.println("setSimilarInventario");
        List<SimilarInventario> similarInventario = null;
        Inventory instance = new Inventory();
        instance.setSimilarInventario(similarInventario);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class Inventory.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Inventory instance = new Inventory();
        instance.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getName method, of class Inventory.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Inventory instance = new Inventory();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDescription method, of class Inventory.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        Inventory instance = new Inventory();
        instance.setDescription(description);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescription method, of class Inventory.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Inventory instance = new Inventory();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventarioList method, of class Inventory.
     */
    @Test
    public void testGetInventarioList() {
        System.out.println("getInventarioList");
        Inventory instance = new Inventory();
        Vector<Inventario> expResult = null;
        Vector<Inventario> result = instance.getInventarioList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkInventarioName method, of class Inventory.
     */
    @Test
    public void testCheckInventarioName() {
        System.out.println("checkInventarioName");
        ComponentSystemEvent event = null;
        Inventory instance = new Inventory();
        instance.checkInventarioName(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AddInventario method, of class Inventory.
     */
    @Test
    public void testAddInventario() {
        System.out.println("AddInventario");
        ActionEvent event = null;
        Inventory instance = new Inventory();
        instance.AddInventario(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of DeleteInventories method, of class Inventory.
     */
    @Test
    public void testDeleteInventories() {
        System.out.println("DeleteInventories");
        ActionEvent event = null;
        Inventory instance = new Inventory();
        instance.DeleteInventories(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of editInventory method, of class Inventory.
     */
    @Test
    public void testEditInventory() {
        System.out.println("editInventory");
        ActionEvent event = null;
        Inventory instance = new Inventory();
        instance.editInventory(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AddSimilarInventario method, of class Inventory.
     */
    @Test
    public void testAddSimilarInventario() {
        System.out.println("AddSimilarInventario");
        AjaxBehaviorEvent event = null;
        Inventory instance = new Inventory();
        instance.AddSimilarInventario(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of sendMail method, of class Inventory.
     */
    @Test
    public void testSendMail() {
        System.out.println("sendMail");
        AjaxBehaviorEvent event = null;
        Inventory instance = new Inventory();
        instance.sendMail(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilar method, of class Inventory.
     */
    @Test
    public void testDeleteSimilar() {
        System.out.println("deleteSimilar");
        AjaxBehaviorEvent event = null;
        SimilarInventarioLayoutInterface sim = null;
        Inventory instance = new Inventory();
        instance.deleteSimilar(event, sim);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFromSimilar method, of class Inventory.
     */
    @Test
    public void testDeleteFromSimilar() {
        System.out.println("deleteFromSimilar");
        AjaxBehaviorEvent event = null;
        Inventory instance = new Inventory();
        instance.deleteFromSimilar(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFranchise method, of class Inventory.
     */
    @Test
    public void testDeleteFranchise() {
        System.out.println("deleteFranchise");
        InventarioPK key = null;
        Inventory instance = new Inventory();
        instance.deleteFranchise(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getUpdateInventory method, of class Inventory.
     */
    @Test
    public void testGetUpdateInventory() {
        System.out.println("getUpdateInventory");
        Inventory instance = new Inventory();
        Inventario expResult = null;
        Inventario result = instance.getUpdateInventory();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setUpdateInventory method, of class Inventory.
     */
    @Test
    public void testSetUpdateInventory() {
        System.out.println("setUpdateInventory");
        Inventario updateInventory = null;
        Inventory instance = new Inventory();
        instance.setUpdateInventory(updateInventory);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class Inventory.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        Inventory instance = new Inventory();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateInventory method, of class Inventory.
     */
    @Test
    public void testUpdateInventory() {
        System.out.println("updateInventory");
        ActionEvent event = null;
        Inventory instance = new Inventory();
        instance.updateInventory(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
