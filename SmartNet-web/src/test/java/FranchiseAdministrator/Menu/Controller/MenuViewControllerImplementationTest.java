/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.Controller;

import ENUM.TransactionStatus;
import Entities.Menu;
import FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class MenuViewControllerImplementationTest {
    
    public MenuViewControllerImplementationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initMenu method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testInitMenu() {
        System.out.println("initMenu");
        String viewId = "";
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.initMenu(viewId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkExistence method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testCheckExistence() {
        System.out.println("checkExistence");
        String name = "";
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkExistence(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAgrupa method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testAddAgrupa() {
        System.out.println("addAgrupa");
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.addAgrupa();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAgrupa method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testDeleteAgrupa() {
        System.out.println("deleteAgrupa");
        List<AgrupaMenuProductoInterface> agrupaMenuProductoList = null;
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.deleteAgrupa(agrupaMenuProductoList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createMenu method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testCreateMenu() {
        System.out.println("createMenu");
        List<AgrupaMenuProductoInterface> agrupaMenuProductoList = null;
        String name = "";
        String description = "";
        double price = 0.0;
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.createMenu(agrupaMenuProductoList, name, description, price);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteMenu method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testDeleteMenu() {
        System.out.println("deleteMenu");
        List<Menu> deleteList = null;
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        instance.deleteMenu(deleteList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateProductPostValidate method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testUpdateProductPostValidate() {
        System.out.println("updateProductPostValidate");
        String name = "";
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.updateProductPostValidate(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateMenu method, of class MenuViewControllerImplementation.
     */
    @Test
    public void testUpdateMenu() {
        System.out.println("updateMenu");
        String name = "";
        String description = "";
        double price = 0.0;
        List<AgrupaMenuProductoInterface> agrupaMenuProductoList = null;
        MenuViewControllerImplementation instance = new MenuViewControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.updateMenu(name, description, price, agrupaMenuProductoList);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
