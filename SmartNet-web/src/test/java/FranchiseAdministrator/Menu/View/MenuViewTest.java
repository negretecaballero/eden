/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Menu.View;

import Clases.MenuUpdateLayoutInterface;
import Entities.AgrupaPK;
import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Menu;
import Entities.Producto;
import Entities.ProductoPK;
import FranchiseAdministrator.Menu.Classes.AgrupaMenuProductoInterface;
import java.util.List;
import java.util.Vector;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class MenuViewTest {
    
    public MenuViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMenuUpdateList method, of class MenuView.
     */
    @Test
    public void testGetMenuUpdateList() {
        System.out.println("getMenuUpdateList");
        MenuView instance = new MenuView();
        List<MenuUpdateLayoutInterface> expResult = null;
        List<MenuUpdateLayoutInterface> result = instance.getMenuUpdateList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setMenuUpdateList method, of class MenuView.
     */
    @Test
    public void testSetMenuUpdateList() {
        System.out.println("setMenuUpdateList");
        List<MenuUpdateLayoutInterface> menuUpdateList = null;
        MenuView instance = new MenuView();
        instance.setMenuUpdateList(menuUpdateList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescription method, of class MenuView.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        MenuView instance = new MenuView();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDescription method, of class MenuView.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        MenuView instance = new MenuView();
        instance.setDescription(description);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class MenuView.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        MenuView instance = new MenuView();
        instance.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getName method, of class MenuView.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        MenuView instance = new MenuView();
        String expResult = "";
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPrice method, of class MenuView.
     */
    @Test
    public void testSetPrice() {
        System.out.println("setPrice");
        double price = 0.0;
        MenuView instance = new MenuView();
        instance.setPrice(price);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPrice method, of class MenuView.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        MenuView instance = new MenuView();
        double expResult = 0.0;
        double result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedProducts method, of class MenuView.
     */
    @Test
    public void testSetSelectedProducts() {
        System.out.println("setSelectedProducts");
        List<AgrupaMenuProductoInterface> selectedProducts = null;
        MenuView instance = new MenuView();
        instance.setSelectedProducts(selectedProducts);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedProducts method, of class MenuView.
     */
    @Test
    public void testGetSelectedProducts() {
        System.out.println("getSelectedProducts");
        MenuView instance = new MenuView();
        List<AgrupaMenuProductoInterface> expResult = null;
        List<AgrupaMenuProductoInterface> result = instance.getSelectedProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteAgrupaMenuList method, of class MenuView.
     */
    @Test
    public void testSetDeleteAgrupaMenuList() {
        System.out.println("setDeleteAgrupaMenuList");
        List<AgrupaMenuProductoInterface> deleteAgrupaMenuList = null;
        MenuView instance = new MenuView();
        instance.setDeleteAgrupaMenuList(deleteAgrupaMenuList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateMenu method, of class MenuView.
     */
    @Test
    public void testPostValidateUpdateMenu() {
        System.out.println("postValidateUpdateMenu");
        ComponentSystemEvent event = null;
        MenuView instance = new MenuView();
        instance.postValidateUpdateMenu(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteAgrupaMenuList method, of class MenuView.
     */
    @Test
    public void testGetDeleteAgrupaMenuList() {
        System.out.println("getDeleteAgrupaMenuList");
        MenuView instance = new MenuView();
        List<AgrupaMenuProductoInterface> expResult = null;
        List<AgrupaMenuProductoInterface> result = instance.getDeleteAgrupaMenuList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteList method, of class MenuView.
     */
    @Test
    public void testSetDeleteList() {
        System.out.println("setDeleteList");
        List<Menu> deleteList = null;
        MenuView instance = new MenuView();
        instance.setDeleteList(deleteList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteList method, of class MenuView.
     */
    @Test
    public void testGetDeleteList() {
        System.out.println("getDeleteList");
        MenuView instance = new MenuView();
        List<Menu> expResult = null;
        List<Menu> result = instance.getDeleteList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setMenuList method, of class MenuView.
     */
    @Test
    public void testSetMenuList() {
        System.out.println("setMenuList");
        Vector<Menu> menuList = null;
        MenuView instance = new MenuView();
        instance.setMenuList(menuList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getMenuList method, of class MenuView.
     */
    @Test
    public void testGetMenuList() {
        System.out.println("getMenuList");
        MenuView instance = new MenuView();
        Vector<Menu> expResult = null;
        Vector<Menu> result = instance.getMenuList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCategories method, of class MenuView.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        MenuView instance = new MenuView();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCategories method, of class MenuView.
     */
    @Test
    public void testSetCategories() {
        System.out.println("setCategories");
        List<Categoria> categories = null;
        MenuView instance = new MenuView();
        instance.setCategories(categories);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProducts method, of class MenuView.
     */
    @Test
    public void testSetProducts() {
        System.out.println("setProducts");
        List<Producto> products = null;
        MenuView instance = new MenuView();
        instance.setProducts(products);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProducts method, of class MenuView.
     */
    @Test
    public void testGetProducts() {
        System.out.println("getProducts");
        MenuView instance = new MenuView();
        List<Producto> expResult = null;
        List<Producto> result = instance.getProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectecProduct method, of class MenuView.
     */
    @Test
    public void testSetSelectecProduct() {
        System.out.println("setSelectecProduct");
        int selectedProduct = 0;
        MenuView instance = new MenuView();
        instance.setSelectecProduct(selectedProduct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedProduct method, of class MenuView.
     */
    @Test
    public void testGetSelectedProduct() {
        System.out.println("getSelectedProduct");
        MenuView instance = new MenuView();
        int expResult = 0;
        int result = instance.getSelectedProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class MenuView.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        MenuView instance = new MenuView();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidate method, of class MenuView.
     */
    @Test
    public void testPostValidate() {
        System.out.println("postValidate");
        ComponentSystemEvent event = null;
        MenuView instance = new MenuView();
        instance.postValidate(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class MenuView.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        MenuView instance = new MenuView();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handeFileUpload method, of class MenuView.
     */
    @Test
    public void testHandeFileUpload() {
        System.out.println("handeFileUpload");
        FileUploadEvent event = null;
        MenuView instance = new MenuView();
        instance.handeFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAgrupa method, of class MenuView.
     */
    @Test
    public void testDeleteAgrupa() {
        System.out.println("deleteAgrupa");
        AjaxBehaviorEvent event = null;
        MenuView instance = new MenuView();
        instance.deleteAgrupa(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addAgrupa method, of class MenuView.
     */
    @Test
    public void testAddAgrupa() {
        System.out.println("addAgrupa");
        AjaxBehaviorEvent event = null;
        MenuView instance = new MenuView();
        instance.addAgrupa(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of selectCategory method, of class MenuView.
     */
    @Test
    public void testSelectCategory() {
        System.out.println("selectCategory");
        AjaxBehavior event = null;
        CategoriaPK categoriapk = null;
        AgrupaMenuProductoInterface agrupaMenuCompone = null;
        MenuView instance = new MenuView();
        instance.selectCategory(event, categoriapk, agrupaMenuCompone);
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removePicture method, of class MenuView.
     */
    @Test
    public void testRemovePicture() {
        System.out.println("removePicture");
        AjaxBehaviorEvent ajaxBehavior = null;
        MenuView instance = new MenuView();
        instance.removePicture(ajaxBehavior);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkExistence method, of class MenuView.
     */
    @Test
    public void testCheckExistence() {
        System.out.println("checkExistence");
        ComponentSystemEvent event = null;
        MenuView instance = new MenuView();
        instance.checkExistence(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AddMenu method, of class MenuView.
     */
    @Test
    public void testAddMenu() {
        System.out.println("AddMenu");
        ActionEvent event = null;
        MenuView instance = new MenuView();
        instance.AddMenu(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteMenu method, of class MenuView.
     */
    @Test
    public void testDeleteMenu() {
        System.out.println("deleteMenu");
        ActionEvent event = null;
        MenuView instance = new MenuView();
        instance.deleteMenu(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateProduct method, of class MenuView.
     */
    @Test
    public void testUpdateProduct() {
        System.out.println("updateProduct");
        MenuView instance = new MenuView();
        String expResult = "failure";
        String result = instance.updateProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAgrupaPK method, of class MenuView.
     */
    @Test
    public void testSetAgrupaPK() {
        System.out.println("setAgrupaPK");
        AgrupaPK agrupaPK = null;
        MenuView instance = new MenuView();
        instance.setAgrupaPK(agrupaPK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductKey method, of class MenuView.
     */
    @Test
    public void testGetProductKey() {
        System.out.println("getProductKey");
        MenuView instance = new MenuView();
        ProductoPK expResult = null;
        ProductoPK result = instance.getProductKey();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProductKey method, of class MenuView.
     */
    @Test
    public void testSetProductKey() {
        System.out.println("setProductKey");
        ProductoPK productKey = null;
        MenuView instance = new MenuView();
        instance.setProductKey(productKey);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteImage method, of class MenuView.
     */
    @Test
    public void testDeleteImage() {
        System.out.println("deleteImage");
        AjaxBehaviorEvent event = null;
        MenuView instance = new MenuView();
        instance.deleteImage(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
