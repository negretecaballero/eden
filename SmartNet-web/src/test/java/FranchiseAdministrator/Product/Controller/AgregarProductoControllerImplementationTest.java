/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.Controller;

import ENUM.TransactionStatus;
import Entities.AdditionPK;
import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Inventario;
import Entities.InventarioPK;
import Entities.Producto;
import Entities.ProductoPK;
import FranchiseAdministrator.Product.Classes.AdditionEden;
import FranchiseAdministrator.Product.Classes.ComponeUpdateInterface;
import FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface;
import FranchiseAdministrator.Product.Classes.ProductoEdenInterface;
import FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import java.util.List;
import java.util.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author luisnegrete
 */
public class AgregarProductoControllerImplementationTest {
    
    public AgregarProductoControllerImplementationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initFileUpload method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testInitFileUpload() {
        System.out.println("initFileUpload");
        String view = "";
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.initFileUpload(view);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkExistence method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testCheckExistence() {
        System.out.println("checkExistence");
        String name = "";
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkExistence(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of loadProductsImages method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testLoadProductsImages() {
        System.out.println("loadProductsImages");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.loadProductsImages();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of initAdditions method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testInitAdditions() {
        System.out.println("initAdditions");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.initAdditions();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ProductsList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testProductsList() {
        System.out.println("ProductsList");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        List<ProductoEdenInterface> expResult = null;
        List<ProductoEdenInterface> result = instance.ProductsList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addUndoInventory method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testAddUndoInventory() {
        System.out.println("addUndoInventory");
        String action = "";
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.addUndoInventory(action);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of findSimilar method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testFindSimilar() {
        System.out.println("findSimilar");
        List<ProductoInventarioComponeInterface> inventoryList = null;
        InventarioPK key = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        boolean expResult = false;
        boolean result = instance.findSimilar(inventoryList, key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dragAddition method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDragAddition() {
        System.out.println("dragAddition");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.dragAddition();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilarProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDeleteSimilarProduct() {
        System.out.println("deleteSimilarProduct");
        ProductoPK key = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.deleteSimilarProduct(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAddition method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDeleteAddition() {
        System.out.println("deleteAddition");
        AdditionPK key = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.deleteAddition(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of loadImagesGalleria method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testLoadImagesGalleria() {
        System.out.println("loadImagesGalleria");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.loadImagesGalleria();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AddProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testAddProduct() {
        System.out.println("AddProduct");
        List<AdditionEden> additionList = null;
        String barcode = "";
        String description = "";
        String name = "";
        double price = 0.0;
        CategoriaPK categoryPK = null;
        List<ProductoInventarioComponeInterface> productInventarioComponeList = null;
        List<EdenSimilarProductInterface> similarList = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.AddProduct(additionList, barcode, description, name, price, categoryPK, productInventarioComponeList, similarList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCategories method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        Vector<Categoria> expResult = null;
        Vector<Categoria> result = instance.getCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of productList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testProductList() {
        System.out.println("productList");
        CategoriaPK key = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        Vector<Producto> expResult = null;
        Vector<Producto> result = instance.productList(key);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDeleteProduct() {
        System.out.println("deleteProduct");
        ProductoPK productPK = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.deleteProduct(productPK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateLazyModel method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testUpdateLazyModel() {
        System.out.println("updateLazyModel");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.updateLazyModel();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateBarcode method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testUpdateBarcode() {
        System.out.println("updateBarcode");
        FileUploadEvent event = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.updateBarcode(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of initUpdateProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testInitUpdateProduct() {
        System.out.println("initUpdateProduct");
        String view = "";
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.initUpdateProduct(view);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilarProductList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDeleteSimilarProductList() {
        System.out.println("deleteSimilarProductList");
        SimilarProductsLayoutInterface similar = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.deleteSimilarProductList(similar);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testGetProductList() {
        System.out.println("getProductList");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        List<Producto> expResult = null;
        List<Producto> result = instance.getProductList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventoryUpdateList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testGetInventoryUpdateList() {
        System.out.println("getInventoryUpdateList");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        List<Inventario> expResult = null;
        List<Inventario> result = instance.getInventoryUpdateList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSimilarProductList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testAddSimilarProductList() {
        System.out.println("addSimilarProductList");
        ProductoPK key = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.addSimilarProductList(key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToComponeUpdateList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testAddToComponeUpdateList() {
        System.out.println("addToComponeUpdateList");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.addToComponeUpdateList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteComponeUpdateList method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testDeleteComponeUpdateList() {
        System.out.println("deleteComponeUpdateList");
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        instance.deleteComponeUpdateList();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkNameExistenceUpdateProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testCheckNameExistenceUpdateProduct() {
        System.out.println("checkNameExistenceUpdateProduct");
        String productName = "";
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkNameExistenceUpdateProduct(productName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateProduct method, of class AgregarProductoControllerImplementation.
     */
    @Test
    public void testUpdateProduct() {
        System.out.println("updateProduct");
        String name = "";
        double price = 0.0;
        String description = "";
        List<SimilarProductsLayoutInterface> similarList = null;
        LazyDataModel<ComponeUpdateInterface> dataModel = null;
        AgregarProductoControllerImplementation instance = new AgregarProductoControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.updateProduct(name, price, description, similarList, dataModel);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

	public void initPreRenderView() {
		// TODO - implement AgregarProductoControllerImplementationTest.initPreRenderView
		throw new UnsupportedOperationException();
	}
    
}
