/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Product.View;

import CDIBeans.DesplegableInventarioInterface;
import CDIBeans.FileUploadInterface;
import CDIBeans.ProductoControllerInterface;
import CDIBeans.desplegablesProductoInterface;
import CDIEden.AgregarProductoControllerInterface;
import Entities.Addition;
import Entities.AdditionPK;
import Entities.Categoria;
import Entities.CategoriaPK;
import Entities.Compone;
import Entities.Inventario;
import Entities.InventarioPK;
import Entities.Producto;
import Entities.ProductoPK;
import Entities.SimilarProducts;
import FranchiseAdministrator.Product.Classes.AdditionEden;
import FranchiseAdministrator.Product.Classes.ComponeUpdate;
import FranchiseAdministrator.Product.Classes.ComponeUpdateInterface;
import FranchiseAdministrator.Product.Classes.EdenSimilarProductInterface;
import FranchiseAdministrator.Product.Classes.ProductoEdenInterface;
import FranchiseAdministrator.Product.Classes.ProductoInventarioComponeInterface;
import FranchiseAdministrator.Product.Classes.SimilarProductsLayoutInterface;
import Layouts.EdenLayout;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.CaptureEvent;
import org.primefaces.event.DragDropEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author luisnegrete
 */
public class AgregarProductoTest {
    
    public AgregarProductoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDragAdditionList method, of class AgregarProducto.
     */
    @Test
    public void testGetDragAdditionList() {
        System.out.println("getDragAdditionList");
        AgregarProducto instance = new AgregarProducto();
        List<AdditionEden> expResult = null;
        List<AdditionEden> result = instance.getDragAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getBarcode method, of class AgregarProducto.
     */
    @Test
    public void testGetBarcode() {
        System.out.println("getBarcode");
        AgregarProducto instance = new AgregarProducto();
        String expResult =null;
        String result = instance.getBarcode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setBarcode method, of class AgregarProducto.
     */
    @Test
    public void testSetBarcode() {
        System.out.println("setBarcode");
        String barcode = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setBarcode(barcode);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getBarcodeImage method, of class AgregarProducto.
     */
    @Test
    public void testGetBarcodeImage() {
        System.out.println("getBarcodeImage");
        AgregarProducto instance = new AgregarProducto();
        String expResult = null;
        String result = instance.getBarcodeImage();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setBarcodeImage method, of class AgregarProducto.
     */
    @Test
    public void testSetBarcodeImage() {
        System.out.println("setBarcodeImage");
        String barCodeImage = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setBarcodeImage(barCodeImage);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDragAdditionList method, of class AgregarProducto.
     */
    @Test
    public void testSetDragAdditionList() {
        System.out.println("setDragAdditionList");
        List<AdditionEden> dragAdditionList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDragAdditionList(dragAdditionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionList method, of class AgregarProducto.
     */
    @Test
    public void testGetAdditionList() {
        System.out.println("getAdditionList");
        AgregarProducto instance = new AgregarProducto();
        List<AdditionEden> expResult = null;
        List<AdditionEden> result = instance.getAdditionList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAdditionList method, of class AgregarProducto.
     */
    @Test
    public void testSetAdditionList() {
        System.out.println("setAdditionList");
        List<AdditionEden> additionList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setAdditionList(additionList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAgregarProducto method, of class AgregarProducto.
     */
    @Test
    public void testSetAgregarProducto() {
        System.out.println("setAgregarProducto");
        AgregarProductoControllerInterface agregarProducto = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setAgregarProducto(agregarProducto);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDragged method, of class AgregarProducto.
     */
    @Test
    public void testGetDragged() {
        System.out.println("getDragged");
        AgregarProducto instance = new AgregarProducto();
        boolean expResult = false;
        boolean result = instance.getDragged();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDragged method, of class AgregarProducto.
     */
    @Test
    public void testSetDragged() {
        System.out.println("setDragged");
        boolean dragged = false;
        AgregarProducto instance = new AgregarProducto();
        instance.setDragged(dragged);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDraggingIdProduct method, of class AgregarProducto.
     */
    @Test
    public void testGetDraggingIdProduct() {
        System.out.println("getDraggingIdProduct");
        AgregarProducto instance = new AgregarProducto();
        int expResult = 0;
        int result = instance.getDraggingIdProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDraggingIdProduct method, of class AgregarProducto.
     */
    @Test
    public void testSetDraggingIdProduct() {
        System.out.println("setDraggingIdProduct");
        int draggingIdProduct = 0;
        AgregarProducto instance = new AgregarProducto();
        instance.setDraggingIdProduct(draggingIdProduct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDragRender method, of class AgregarProducto.
     */
    @Test
    public void testGetDragRender() {
        System.out.println("getDragRender");
        AgregarProducto instance = new AgregarProducto();
        boolean expResult = false;
        boolean result = instance.getDragRender();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDragRender method, of class AgregarProducto.
     */
    @Test
    public void testSetDragRender() {
        System.out.println("setDragRender");
        boolean dragRender = false;
        AgregarProducto instance = new AgregarProducto();
        instance.setDragRender(dragRender);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDragImage method, of class AgregarProducto.
     */
    @Test
    public void testGetDragImage() {
        System.out.println("getDragImage");
        AgregarProducto instance = new AgregarProducto();
        String expResult = null;
        String result = instance.getDragImage();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDragImage method, of class AgregarProducto.
     */
    @Test
    public void testSetDragImage() {
        System.out.println("setDragImage");
        String dragImage = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setDragImage(dragImage);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductoController method, of class AgregarProducto.
     */
    @Test
    public void testGetProductoController() {
        System.out.println("getProductoController");
        AgregarProducto instance = new AgregarProducto();
        ProductoControllerInterface expResult = null;
        ProductoControllerInterface result = instance.getProductoController();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProductoController method, of class AgregarProducto.
     */
    @Test
    public void testSetProductoController() {
        System.out.println("setProductoController");
        ProductoControllerInterface productoController = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setProductoController(productoController);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProducts method, of class AgregarProducto.
     */
    @Test
    public void testGetProducts() {
        System.out.println("getProducts");
        AgregarProducto instance = new AgregarProducto();
        List<ProductoEdenInterface> expResult = null;
        List<ProductoEdenInterface> result = instance.getProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProducts method, of class AgregarProducto.
     */
    @Test
    public void testSetProducts() {
        System.out.println("setProducts");
        List<ProductoEdenInterface> products = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setProducts(products);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSimilarProductList method, of class AgregarProducto.
     */
    @Test
    public void testGetSimilarProductList() {
        System.out.println("getSimilarProductList");
        AgregarProducto instance = new AgregarProducto();
        ProductoPK expResult = null;
        ProductoPK result = instance.getSimilarProductList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSimilarProductList method, of class AgregarProducto.
     */
    @Test
    public void testSetSimilarProductList() {
        System.out.println("setSimilarProductList");
        ProductoPK similarProductList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setSimilarProductList(similarProductList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDescription method, of class AgregarProducto.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setDescription(description);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDescription method, of class AgregarProducto.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        AgregarProducto instance = new AgregarProducto();
        String expResult = "";
        String result = instance.getDescription();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSimilarProducts method, of class AgregarProducto.
     */
    @Test
    public void testGetSimilarProducts() {
        System.out.println("getSimilarProducts");
        AgregarProducto instance = new AgregarProducto();
        List<EdenSimilarProductInterface> expResult = null;
        List<EdenSimilarProductInterface> result = instance.getSimilarProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSimilarProducts method, of class AgregarProducto.
     */
    @Test
    public void testSetSimilarProducts() {
        System.out.println("setSimilarProducts");
        List<EdenSimilarProductInterface> similarProducts = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setSimilarProducts(similarProducts);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCategories method, of class AgregarProducto.
     */
    @Test
    public void testSetCategories() {
        System.out.println("setCategories");
        List<Categoria> categories = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setCategories(categories);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCategories method, of class AgregarProducto.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        AgregarProducto instance = new AgregarProducto();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getCategories();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setComponeUpdate method, of class AgregarProducto.
     */
    @Test
    public void testSetComponeUpdate() {
        System.out.println("setComponeUpdate");
        ComponeUpdate componeUpdate = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setComponeUpdate(componeUpdate);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getComponeUpdate method, of class AgregarProducto.
     */
    @Test
    public void testGetComponeUpdate() {
        System.out.println("getComponeUpdate");
        AgregarProducto instance = new AgregarProducto();
        ComponeUpdate expResult = null;
        ComponeUpdate result = instance.getComponeUpdate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelection method, of class AgregarProducto.
     */
    @Test
    public void testGetSelection() {
        System.out.println("getSelection");
        AgregarProducto instance = new AgregarProducto();
        boolean expResult = false;
        boolean result = instance.getSelection();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelection method, of class AgregarProducto.
     */
    @Test
    public void testSetSelection() {
        System.out.println("setSelection");
        boolean selection = false;
        AgregarProducto instance = new AgregarProducto();
        instance.setSelection(selection);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getComponeListModel method, of class AgregarProducto.
     */
    @Test
    public void testGetComponeListModel() {
        System.out.println("getComponeListModel");
        AgregarProducto instance = new AgregarProducto();
        LazyDataModel<ComponeUpdateInterface> expResult = null;
        LazyDataModel<ComponeUpdateInterface> result = instance.getComponeListModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setComponeListModel method, of class AgregarProducto.
     */
    @Test
    public void testSetComponeListModel() {
        System.out.println("setComponeListModel");
        LazyDataModel<ComponeUpdateInterface> componeListModel = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setComponeListModel(componeListModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteComponeList method, of class AgregarProducto.
     */
    @Test
    public void testGetDeleteComponeList() {
        System.out.println("getDeleteComponeList");
        AgregarProducto instance = new AgregarProducto();
        List<ComponeUpdateInterface> expResult = null;
        List<ComponeUpdateInterface> result = instance.getDeleteComponeList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteComponeList method, of class AgregarProducto.
     */
    @Test
    public void testSetDeleteComponeList() {
        System.out.println("setDeleteComponeList");
        List<ComponeUpdateInterface> deleteComponeList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDeleteComponeList(deleteComponeList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getFileUpload method, of class AgregarProducto.
     */
    @Test
    public void testGetFileUpload() {
        System.out.println("getFileUpload");
        AgregarProducto instance = new AgregarProducto();
        FileUploadInterface expResult = null;
        FileUploadInterface result = instance.getFileUpload();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setFileUpload method, of class AgregarProducto.
     */
    @Test
    public void testSetFileUpload() {
        System.out.println("setFileUpload");
        FileUploadInterface fileUpload = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setFileUpload(fileUpload);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDesplegablesProducto method, of class AgregarProducto.
     */
    @Test
    public void testGetDesplegablesProducto() {
        System.out.println("getDesplegablesProducto");
        AgregarProducto instance = new AgregarProducto();
        desplegablesProductoInterface expResult = null;
        desplegablesProductoInterface result = instance.getDesplegablesProducto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDesplegablesProducto method, of class AgregarProducto.
     */
    @Test
    public void testSetDesplegablesProducto() {
        System.out.println("setDesplegablesProducto");
        desplegablesProductoInterface desplegablesProducto = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDesplegablesProducto(desplegablesProducto);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDesplegableInventario method, of class AgregarProducto.
     */
    @Test
    public void testGetDesplegableInventario() {
        System.out.println("getDesplegableInventario");
        AgregarProducto instance = new AgregarProducto();
        DesplegableInventarioInterface expResult = null;
        DesplegableInventarioInterface result = instance.getDesplegableInventario();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDesplegableInventario method, of class AgregarProducto.
     */
    @Test
    public void testSetDesplegableInventario() {
        System.out.println("setDesplegableInventario");
        DesplegableInventarioInterface desplegableInventario = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDesplegableInventario(desplegableInventario);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSimilarList method, of class AgregarProducto.
     */
    @Test
    public void testSetSimilarList() {
        System.out.println("setSimilarList");
        List<SimilarProductsLayoutInterface> similarList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setSimilarList(similarList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSimilarList method, of class AgregarProducto.
     */
    @Test
    public void testGetSimilarList() {
        System.out.println("getSimilarList");
        AgregarProducto instance = new AgregarProducto();
        List<SimilarProductsLayoutInterface> expResult = null;
        List<SimilarProductsLayoutInterface> result = instance.getSimilarList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeleteProductsList method, of class AgregarProducto.
     */
    @Test
    public void testGetDeleteProductsList() {
        System.out.println("getDeleteProductsList");
        AgregarProducto instance = new AgregarProducto();
        List<ProductoPK> expResult = null;
        List<ProductoPK> result = instance.getDeleteProductsList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeleteProductsList method, of class AgregarProducto.
     */
    @Test
    public void testSetDeleteProductsList() {
        System.out.println("setDeleteProductsList");
        List<ProductoPK> deleteProductsList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDeleteProductsList(deleteProductsList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setComponeList method, of class AgregarProducto.
     */
    @Test
    public void testSetComponeList() {
        System.out.println("setComponeList");
        List<ComponeUpdateInterface> componeList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setComponeList(componeList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getComponeList method, of class AgregarProducto.
     */
    @Test
    public void testGetComponeList() {
        System.out.println("getComponeList");
        AgregarProducto instance = new AgregarProducto();
        List<ComponeUpdateInterface> expResult = null;
        List<ComponeUpdateInterface> result = instance.getComponeList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAuxuliar2 method, of class AgregarProducto.
     */
    @Test
    public void testSetAuxuliar2() {
        System.out.println("setAuxuliar2");
        String auxiliar2 = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setAuxuliar2(auxiliar2);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAuxiliar2 method, of class AgregarProducto.
     */
    @Test
    public void testGetAuxiliar2() {
        System.out.println("getAuxiliar2");
        AgregarProducto instance = new AgregarProducto();
        String expResult = null;
        String result = instance.getAuxiliar2();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAuxiliar method, of class AgregarProducto.
     */
    @Test
    public void testGetAuxiliar() {
        System.out.println("getAuxiliar");
        AgregarProducto instance = new AgregarProducto();
        String expResult = null;
        String result = instance.getAuxiliar();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDeletePK method, of class AgregarProducto.
     */
    @Test
    public void testGetDeletePK() {
        System.out.println("getDeletePK");
        AgregarProducto instance = new AgregarProducto();
        Producto expResult = null;
        Producto result = instance.getDeletePK();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDeletePK method, of class AgregarProducto.
     */
    @Test
    public void testSetDeletePK() {
        System.out.println("setDeletePK");
        Producto deletePK = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setDeletePK(deletePK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setProductList method, of class AgregarProducto.
     */
    @Test
    public void testSetProductList() {
        System.out.println("setProductList");
        LazyDataModel<Producto> ProductList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setProductList(ProductList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductList method, of class AgregarProducto.
     */
    @Test
    public void testGetProductList() {
        System.out.println("getProductList");
        AgregarProducto instance = new AgregarProducto();
        LazyDataModel<Producto> expResult = null;
        LazyDataModel<Producto> result = instance.getProductList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteProducts method, of class AgregarProducto.
     */
    @Test
    public void testDeleteProducts() {
        System.out.println("deleteProducts");
        ActionEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteProducts(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setType method, of class AgregarProducto.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        ComponentSystemEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setType(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateProducts method, of class AgregarProducto.
     */
    @Test
    public void testUpdateProducts() {
        System.out.println("updateProducts");
        AjaxBehaviorEvent ajaxbehavior = null;
        AgregarProducto instance = new AgregarProducto();
        instance.updateProducts(ajaxbehavior);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setAuxiliar method, of class AgregarProducto.
     */
    @Test
    public void testSetAuxiliar() {
        System.out.println("setAuxiliar");
        String auxiliar = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setAuxiliar(auxiliar);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInvList method, of class AgregarProducto.
     */
    @Test
    public void testGetInvList() {
        System.out.println("getInvList");
        AgregarProducto instance = new AgregarProducto();
        List<ProductoInventarioComponeInterface> expResult = null;
        List<ProductoInventarioComponeInterface> result = instance.getInvList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setInvList method, of class AgregarProducto.
     */
    @Test
    public void testSetInvList() {
        System.out.println("setInvList");
        List<ProductoInventarioComponeInterface> InvList = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setInvList(InvList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getId_producto method, of class AgregarProducto.
     */
    @Test
    public void testGetId_producto() {
        System.out.println("getId_producto");
        AgregarProducto instance = new AgregarProducto();
        ProductoPK expResult = null;
        ProductoPK result = instance.getId_producto();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setId_producto method, of class AgregarProducto.
     */
    @Test
    public void testSetId_producto() {
        System.out.println("setId_producto");
        ProductoPK id_producto = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setId_producto(id_producto);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getId_inv method, of class AgregarProducto.
     */
    @Test
    public void testGetId_inv() {
        System.out.println("getId_inv");
        AgregarProducto instance = new AgregarProducto();
        InventarioPK expResult = null;
        InventarioPK result = instance.getId_inv();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setId_inv method, of class AgregarProducto.
     */
    @Test
    public void testSetId_inv() {
        System.out.println("setId_inv");
        InventarioPK id_inv = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setId_inv(id_inv);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCantInv method, of class AgregarProducto.
     */
    @Test
    public void testGetCantInv() {
        System.out.println("getCantInv");
        AgregarProducto instance = new AgregarProducto();
        double expResult = 0.0;
        double result = instance.getCantInv();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setCantInv method, of class AgregarProducto.
     */
    @Test
    public void testSetCantInv() {
        System.out.println("setCantInv");
        double cantInv = 0.0;
        AgregarProducto instance = new AgregarProducto();
        instance.setCantInv(cantInv);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getId_Categoria method, of class AgregarProducto.
     */
    @Test
    public void testGetId_Categoria() {
        System.out.println("getId_Categoria");
        AgregarProducto instance = new AgregarProducto();
        CategoriaPK expResult = null;
        CategoriaPK result = instance.getId_Categoria();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setId_Categoria method, of class AgregarProducto.
     */
    @Test
    public void testSetId_Categoria() {
        System.out.println("setId_Categoria");
        CategoriaPK id_Categoria = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setId_Categoria(id_Categoria);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPrecio method, of class AgregarProducto.
     */
    @Test
    public void testGetPrecio() {
        System.out.println("getPrecio");
        AgregarProducto instance = new AgregarProducto();
        double expResult = 0.0;
        double result = instance.getPrecio();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPrecio method, of class AgregarProducto.
     */
    @Test
    public void testSetPrecio() {
        System.out.println("setPrecio");
        double precio = 0.0;
        AgregarProducto instance = new AgregarProducto();
        instance.setPrecio(precio);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getNombre method, of class AgregarProducto.
     */
    @Test
    public void testGetNombre() {
        System.out.println("getNombre");
        AgregarProducto instance = new AgregarProducto();
        String expResult = null;
        String result = instance.getNombre();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setNombre method, of class AgregarProducto.
     */
    @Test
    public void testSetNombre() {
        System.out.println("setNombre");
        String nombre = "";
        AgregarProducto instance = new AgregarProducto();
        instance.setNombre(nombre);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class AgregarProducto.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        AgregarProducto instance = new AgregarProducto();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUpload method, of class AgregarProducto.
     */
    @Test
    public void testHandleFileUpload() {
        System.out.println("handleFileUpload");
        FileUploadEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.handleFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkExistence method, of class AgregarProducto.
     */
    @Test
    public void testCheckExistence() {
        System.out.println("checkExistence");
        ComponentSystemEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.checkExistence(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateProduct method, of class AgregarProducto.
     */
    @Test
    public void testPostValidateUpdateProduct() {
        System.out.println("postValidateUpdateProduct");
        ComponentSystemEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.postValidateUpdateProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ProductList method, of class AgregarProducto.
     */
    @Test
    public void testProductList() {
        System.out.println("ProductList");
        CategoriaPK categoriaPK = null;
        AgregarProducto instance = new AgregarProducto();
        List<Producto> expResult = new java.util.ArrayList<Entities.Producto>();
        List<Producto> result = instance.ProductList(categoriaPK);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addProducto method, of class AgregarProducto.
     */
    @Test
    public void testAddProducto() {
        System.out.println("addProducto");
        ActionEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.addProducto(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ajaxProduct method, of class AgregarProducto.
     */
    @Test
    public void testAjaxProduct() {
        System.out.println("ajaxProduct");
        AjaxBehaviorEvent ajaxBehaviour = null;
        AgregarProducto instance = new AgregarProducto();
        instance.ajaxProduct(ajaxBehaviour);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteProducto method, of class AgregarProducto.
     */
    @Test
    public void testDeleteProducto() {
        System.out.println("deleteProducto");
        AgregarProducto instance = new AgregarProducto();
        instance.deleteProducto();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addInventarioUpdate method, of class AgregarProducto.
     */
    @Test
    public void testAddInventarioUpdate() {
        System.out.println("addInventarioUpdate");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.addInventarioUpdate(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteInvCompone method, of class AgregarProducto.
     */
    @Test
    public void testDeleteInvCompone() {
        System.out.println("deleteInvCompone");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteInvCompone(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateProduct method, of class AgregarProducto.
     */
    @Test
    public void testUpdateProduct() {
        System.out.println("updateProduct");
        AgregarProducto instance = new AgregarProducto();
        String expResult = "failure";
        String result = instance.updateProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getFranchiseProducts method, of class AgregarProducto.
     */
    @Test
    public void testGetFranchiseProducts() {
        System.out.println("getFranchiseProducts");
        AgregarProducto instance = new AgregarProducto();
        List<Producto> expResult = null;
        List<Producto> result = instance.getFranchiseProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSimilarProduct method, of class AgregarProducto.
     */
    @Test
    public void testAddSimilarProduct() {
        System.out.println("addSimilarProduct");
        String imageRoot = "";
        ProductoPK productId = null;
        AgregarProducto instance = new AgregarProducto();
        instance.addSimilarProduct(imageRoot, productId);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSimilarProduct method, of class AgregarProducto.
     */
    @Test
    public void testRemoveSimilarProduct() {
        System.out.println("removeSimilarProduct");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.removeSimilarProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductListUpdate method, of class AgregarProducto.
     */
    @Test
    public void testGetProductListUpdate() {
        System.out.println("getProductListUpdate");
        AgregarProducto instance = new AgregarProducto();
        List<Producto> expResult = null;
        List<Producto> result = instance.getProductListUpdate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilar method, of class AgregarProducto.
     */
    @Test
    public void testDeleteSimilar() {
        System.out.println("deleteSimilar");
        SimilarProductsLayoutInterface similar = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteSimilar(similar);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of similarSelectOneChanges method, of class AgregarProducto.
     */
    @Test
    public void testSimilarSelectOneChanges() {
        System.out.println("similarSelectOneChanges");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.similarSelectOneChanges(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of test method, of class AgregarProducto.
     */
    @Test
    public void testTest() {
        System.out.println("test");
        ProductoPK idProducto = null;
        String imageRoot = "";
        AgregarProducto instance = new AgregarProducto();
        instance.test(idProducto, imageRoot);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dragProduct method, of class AgregarProducto.
     */
    @Test
    public void testDragProduct() {
        System.out.println("dragProduct");
        DragDropEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.dragProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAdditionsResult method, of class AgregarProducto.
     */
    @Test
    public void testGetAdditionsResult() {
        System.out.println("getAdditionsResult");
        AgregarProducto instance = new AgregarProducto();
        List<Addition> expResult = null;
        List<Addition> result = instance.getAdditionsResult();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilarList method, of class AgregarProducto.
     */
    @Test
    public void testDeleteSimilarList() {
        System.out.println("deleteSimilarList");
        AjaxBehaviorEvent event = null;
        ProductoPK idProduct = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteSimilarList(event, idProduct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of dragAddition method, of class AgregarProducto.
     */
    @Test
    public void testDragAddition() {
        System.out.println("dragAddition");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.dragAddition(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteAddition method, of class AgregarProducto.
     */
    @Test
    public void testDeleteAddition() {
        System.out.println("deleteAddition");
        AdditionPK additionPK = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteAddition(additionPK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeImage method, of class AgregarProducto.
     */
    @Test
    public void testRemoveImage() {
        System.out.println("removeImage");
        AjaxBehaviorEvent ajaxBehavior = null;
        AgregarProducto instance = new AgregarProducto();
        instance.removeImage(ajaxBehavior);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateDeleteKey method, of class AgregarProducto.
     */
    @Test
    public void testUpdateDeleteKey() {
        System.out.println("updateDeleteKey");
        SelectEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.updateDeleteKey(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getInventoryList method, of class AgregarProducto.
     */
    @Test
    public void testGetInventoryList() {
        System.out.println("getInventoryList");
        AgregarProducto instance = new AgregarProducto();
        List<Inventario> expResult = null;
        List<Inventario> result = instance.getInventoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addToSimilarList method, of class AgregarProducto.
     */
    @Test
    public void testAddToSimilarList() {
        System.out.println("addToSimilarList");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.addToSimilarList(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteUpdateProduct method, of class AgregarProducto.
     */
    @Test
    public void testDeleteUpdateProduct() {
        System.out.println("deleteUpdateProduct");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteUpdateProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of captureBarCode method, of class AgregarProducto.
     */
    @Test
    public void testCaptureBarCode() {
        System.out.println("captureBarCode");
        CaptureEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.captureBarCode(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of uploadBarcode method, of class AgregarProducto.
     */
    @Test
    public void testUploadBarcode() {
        System.out.println("uploadBarcode");
        FileUploadEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.uploadBarcode(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of scanCode method, of class AgregarProducto.
     */
    @Test
    public void testScanCode() {
        System.out.println("scanCode");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.scanCode(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of showAuxiliar method, of class AgregarProducto.
     */
    @Test
    public void testShowAuxiliar() {
        System.out.println("showAuxiliar");
        ActionEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.showAuxiliar(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getBusinessProduct method, of class AgregarProducto.
     */
    @Test
    public void testGetBusinessProduct() {
        System.out.println("getBusinessProduct");
        AgregarProducto instance = new AgregarProducto();
        Producto expResult = null;
        Producto result = instance.getBusinessProduct();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setBusinessProduct method, of class AgregarProducto.
     */
    @Test
    public void testSetBusinessProduct() {
        System.out.println("setBusinessProduct");
        Producto businessProduct = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setBusinessProduct(businessProduct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class AgregarProducto.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getCategoryList method, of class AgregarProducto.
     */
    @Test
    public void testGetCategoryList() {
        System.out.println("getCategoryList");
        AgregarProducto instance = new AgregarProducto();
        List<Categoria> expResult = null;
        List<Categoria> result = instance.getCategoryList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getLazyModel method, of class AgregarProducto.
     */
    @Test
    public void testGetLazyModel() {
        System.out.println("getLazyModel");
        AgregarProducto instance = new AgregarProducto();
        LazyDataModel<Compone> expResult = null;
        LazyDataModel<Compone> result = instance.getLazyModel();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setLazyModel method, of class AgregarProducto.
     */
    @Test
    public void testSetLazyModel() {
        System.out.println("setLazyModel");
        LazyDataModel<Compone> lazyModel = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setLazyModel(lazyModel);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectedComponeItem method, of class AgregarProducto.
     */
    @Test
    public void testGetSelectedComponeItem() {
        System.out.println("getSelectedComponeItem");
        AgregarProducto instance = new AgregarProducto();
        Compone expResult = null;
        Compone result = instance.getSelectedComponeItem();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSelectedComponeItem method, of class AgregarProducto.
     */
    @Test
    public void testSetSelectedComponeItem() {
        System.out.println("setSelectedComponeItem");
        Compone selectedComponeItem = null;
        AgregarProducto instance = new AgregarProducto();
        instance.setSelectedComponeItem(selectedComponeItem);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addComponeItem method, of class AgregarProducto.
     */
    @Test
    public void testAddComponeItem() {
        System.out.println("addComponeItem");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.addComponeItem(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteComponeItem method, of class AgregarProducto.
     */
    @Test
    public void testDeleteComponeItem() {
        System.out.println("deleteComponeItem");
        Compone compone = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteComponeItem(compone);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductsList method, of class AgregarProducto.
     */
    @Test
    public void testGetProductsList() {
        System.out.println("getProductsList");
        AgregarProducto instance = new AgregarProducto();
        List<Producto> expResult = null;
        List<Producto> result = instance.getProductsList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createBusinessProduct method, of class AgregarProducto.
     */
    @Test
    public void testCreateBusinessProduct() {
        System.out.println("createBusinessProduct");
        ActionEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.createBusinessProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProductsFranchiseList method, of class AgregarProducto.
     */
    @Test
    public void testGetProductsFranchiseList() {
        System.out.println("getProductsFranchiseList");
        AgregarProducto instance = new AgregarProducto();
        List<EdenLayout<Producto>> expResult = null;
        List<EdenLayout<Producto>> result = instance.getProductsFranchiseList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of AddSimilarProductBusiness method, of class AgregarProducto.
     */
    @Test
    public void testAddSimilarProductBusiness() {
        System.out.println("AddSimilarProductBusiness");
        AjaxBehaviorEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.AddSimilarProductBusiness(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSimilarButinessProduct method, of class AgregarProducto.
     */
    @Test
    public void testDeleteSimilarButinessProduct() {
        System.out.println("deleteSimilarButinessProduct");
        SimilarProducts similar = null;
        AgregarProducto instance = new AgregarProducto();
        instance.deleteSimilarButinessProduct(similar);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of removeSimilarList method, of class AgregarProducto.
     */
    @Test
    public void testRemoveSimilarList() {
        System.out.println("removeSimilarList");
        AjaxBehaviorEvent event = null;
        SimilarProducts product = null;
        AgregarProducto instance = new AgregarProducto();
        instance.removeSimilarList(event, product);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateBusinessProduct method, of class AgregarProducto.
     */
    @Test
    public void testUpdateBusinessProduct() {
        System.out.println("updateBusinessProduct");
        ActionEvent event = null;
        AgregarProducto instance = new AgregarProducto();
        instance.updateBusinessProduct(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteBusinessImage method, of class AgregarProducto.
     */
    @Test
    public void testDeleteBusinessImage() {
        System.out.println("deleteBusinessImage");
        AjaxBehaviorEvent event = null;
        String path = "";
        AgregarProducto instance = new AgregarProducto();
        instance.deleteBusinessImage(event, path);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }
    
}
