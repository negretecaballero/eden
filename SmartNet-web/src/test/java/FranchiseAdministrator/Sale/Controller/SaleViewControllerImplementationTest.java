/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.Controller;

import ENUM.TransactionStatus;
import Entities.SaleHasMenu;
import Entities.SaleHasProducto;
import Entities.SalePK;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author luisnegrete
 */
public class SaleViewControllerImplementationTest {
    
    public SaleViewControllerImplementationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of initPreRenderView method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testInitPreRenderView() {
        System.out.println("initPreRenderView");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.initPreRenderView();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateAddSale method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testPostValidateAddSale() {
        System.out.println("postValidateAddSale");
        String name = "";
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.postValidateAddSale(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkDates method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testCheckDates() {
        System.out.println("checkDates");
        Date startDate = null;
        Date endDate = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.checkDates(startDate, endDate);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSaleHasProducto method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testAddSaleHasProducto() {
        System.out.println("addSaleHasProducto");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.addSaleHasProducto();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSaleHasProducto method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testDeleteSaleHasProducto() {
        System.out.println("deleteSaleHasProducto");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.deleteSaleHasProducto();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSaleHasMenu method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testAddSaleHasMenu() {
        System.out.println("addSaleHasMenu");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.addSaleHasMenu();
        // TODO review the generated test code and remove the default call to fail.
       //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSaleHasMenu method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testDeleteSaleHasMenu() {
        System.out.println("deleteSaleHasMenu");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.deleteSaleHasMenu();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createSale method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testCreateSale() {
        System.out.println("createSale");
        String name = "";
        double price = 0.0;
        Date startDate = null;
        Date finishDate = null;
        List<SaleHasProducto> saleHasProductoList = null;
        List<SaleHasMenu> saleHasMenuList = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.createSale(name, price, startDate, finishDate, saleHasProductoList, saleHasMenuList);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteSale method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testDeleteSale() {
        System.out.println("deleteSale");
        SalePK salePK = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.deleteSale(salePK);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUpload method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testHandleFileUpload() {
        System.out.println("handleFileUpload");
        FileUploadEvent event = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.handleFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addUpdateSaleHasMenu method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testAddUpdateSaleHasMenu() {
        System.out.println("addUpdateSaleHasMenu");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.addUpdateSaleHasMenu();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteUpdateSaleHasMenu method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testDeleteUpdateSaleHasMenu() {
        System.out.println("deleteUpdateSaleHasMenu");
        SaleHasMenu saleHasMenu = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.deleteUpdateSaleHasMenu(saleHasMenu);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addUpdateSaleHasProducto method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testAddUpdateSaleHasProducto() {
        System.out.println("addUpdateSaleHasProducto");
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.addUpdateSaleHasProducto();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteUpdateSaleHasProducto method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testDeleteUpdateSaleHasProducto() {
        System.out.println("deleteUpdateSaleHasProducto");
        SaleHasProducto saleHasProducto = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        instance.deleteUpdateSaleHasProducto(saleHasProducto);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateSale method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testUpdateSale() {
        System.out.println("updateSale");
        String name = "";
        double price = 0.0;
        Date startDate = null;
        Date finishDate = null;
        List<SaleHasProducto> saleHasProductoList = null;
        List<SaleHasMenu> saleHasMenuList = null;
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        TransactionStatus expResult = ENUM.TransactionStatus.DISAPPROVED;
        TransactionStatus result = instance.updateSale(name, price, startDate, finishDate, saleHasProductoList, saleHasMenuList);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateSale method, of class SaleViewControllerImplementation.
     */
    @Test
    public void testPostValidateUpdateSale() {
        System.out.println("postValidateUpdateSale");
        String name = "";
        SaleViewControllerImplementation instance = new SaleViewControllerImplementation();
        boolean expResult = false;
        boolean result = instance.postValidateUpdateSale(name);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
