/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FranchiseAdministrator.Sale.View;

import Entities.Menu;
import Entities.Producto;
import Entities.Sale;
import Entities.SaleHasMenu;
import Entities.SaleHasProducto;
import Entities.SalePK;
import FranchiseAdministrator.Sale.Classes.SaleViewLayout;
import Layouts.EdenLayout;
import java.util.Date;
import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ComponentSystemEvent;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author luisnegrete
 */
public class SaleViewTest {
    
    public SaleViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class SaleView.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        SaleView instance = new SaleView();
        String expResult = null;
        String result = instance.getName();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setName method, of class SaleView.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        SaleView instance = new SaleView();
        instance.setName(name);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getPrice method, of class SaleView.
     */
    @Test
    public void testGetPrice() {
        System.out.println("getPrice");
        SaleView instance = new SaleView();
        double expResult = 0.0;
        double result = instance.getPrice();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setPrice method, of class SaleView.
     */
    @Test
    public void testSetPrice() {
        System.out.println("setPrice");
        double price = 0.0;
        SaleView instance = new SaleView();
        instance.setPrice(price);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getStartDate method, of class SaleView.
     */
    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");
        SaleView instance = new SaleView();
        Date expResult = null;
        Date result = instance.getStartDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setStartDate method, of class SaleView.
     */
    @Test
    public void testSetStartDate() {
        System.out.println("setStartDate");
        Date startDate = null;
        SaleView instance = new SaleView();
        instance.setStartDate(startDate);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getFinishDate method, of class SaleView.
     */
    @Test
    public void testGetFinishDate() {
        System.out.println("getFinishDate");
        SaleView instance = new SaleView();
        Date expResult = null;
        Date result = instance.getFinishDate();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setFinishDate method, of class SaleView.
     */
    @Test
    public void testSetFinishDate() {
        System.out.println("setFinishDate");
        Date finishDate = null;
        SaleView instance = new SaleView();
        instance.setFinishDate(finishDate);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getProducts method, of class SaleView.
     */
    @Test
    public void testGetProducts() {
        System.out.println("getProducts");
        SaleView instance = new SaleView();
        List<Producto> expResult = null;
        List<Producto> result = instance.getProducts();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateUpdateSale method, of class SaleView.
     */
    @Test
    public void testPostValidateUpdateSale() {
        System.out.println("postValidateUpdateSale");
        ComponentSystemEvent event = null;
        SaleView instance = new SaleView();
        instance.postValidateUpdateSale(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSales method, of class SaleView.
     */
    @Test
    public void testGetSales() {
        System.out.println("getSales");
        SaleView instance = new SaleView();
        List<SaleViewLayout> expResult = null;
        List<SaleViewLayout> result = instance.getSales();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSales method, of class SaleView.
     */
    @Test
    public void testSetSales() {
        System.out.println("setSales");
        List<SaleViewLayout> sales = null;
        SaleView instance = new SaleView();
        instance.setSales(sales);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSale method, of class SaleView.
     */
    @Test
    public void testGetSale() {
        System.out.println("getSale");
        SaleView instance = new SaleView();
        Sale expResult = null;
        Sale result = instance.getSale();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSate method, of class SaleView.
     */
    @Test
    public void testSetSate() {
        System.out.println("setSate");
        Sale sale = null;
        SaleView instance = new SaleView();
        instance.setSate(sale);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of init method, of class SaleView.
     */
    @Test
    public void testInit() {
        System.out.println("init");
        SaleView instance = new SaleView();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSaleHasMenuList method, of class SaleView.
     */
    @Test
    public void testGetSaleHasMenuList() {
        System.out.println("getSaleHasMenuList");
        SaleView instance = new SaleView();
        List<SaleHasMenu> expResult = null;
        List<SaleHasMenu> result = instance.getSaleHasMenuList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSaleHasProductoList method, of class SaleView.
     */
    @Test
    public void testGetSaleHasProductoList() {
        System.out.println("getSaleHasProductoList");
        SaleView instance = new SaleView();
        List<SaleHasProducto> expResult = null;
        List<SaleHasProducto> result = instance.getSaleHasProductoList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setSaleHasMenuList method, of class SaleView.
     */
    @Test
    public void testSetSaleHasMenuList() {
        System.out.println("setSaleHasMenuList");
        List<SaleHasMenu> saleHasMenuList = null;
        SaleView instance = new SaleView();
        instance.setSaleHasMenuList(saleHasMenuList);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of setSaleHasProductoList method, of class SaleView.
     */
    @Test
    public void testSetSaleHasProductoList() {
        System.out.println("setSaleHasProductoList");
        List<SaleHasProducto> saleHasProductoList = null;
        SaleView instance = new SaleView();
        instance.setSaleHasProductoList(saleHasProductoList);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of handleFileUpload method, of class SaleView.
     */
    @Test
    public void testHandleFileUpload() {
        System.out.println("handleFileUpload");
        FileUploadEvent event = null;
        SaleView instance = new SaleView();
        instance.handleFileUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of printImages method, of class SaleView.
     */
    @Test
    public void testPrintImages() {
        System.out.println("printImages");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.printImages(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of actionType method, of class SaleView.
     */
    @Test
    public void testActionType() {
        System.out.println("actionType");
        SaleView instance = new SaleView();
        instance.actionType();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSaleHasProducto method, of class SaleView.
     */
    @Test
    public void testAddSaleHasProducto() {
        System.out.println("addSaleHasProducto");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.addSaleHasProducto(event);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of handleUpload method, of class SaleView.
     */
    @Test
    public void testHandleUpload() {
        System.out.println("handleUpload");
        FileUploadEvent event = null;
        SaleView instance = new SaleView();
        instance.handleUpload(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSaleHasProducto method, of class SaleView.
     */
    @Test
    public void testRemoveSaleHasProducto() {
        System.out.println("removeSaleHasProducto");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.removeSaleHasProducto(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getMenuList method, of class SaleView.
     */
    @Test
    public void testGetMenuList() {
        System.out.println("getMenuList");
        SaleView instance = new SaleView();
        List<Menu> expResult = null;
        List<Menu> result = instance.getMenuList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addMenuList method, of class SaleView.
     */
    @Test
    public void testAddMenuList() {
        System.out.println("addMenuList");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.addMenuList(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSaleHasMenu method, of class SaleView.
     */
    @Test
    public void testRemoveSaleHasMenu() {
        System.out.println("removeSaleHasMenu");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.removeSaleHasMenu(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of createSale method, of class SaleView.
     */
    @Test
    public void testCreateSale() {
        System.out.println("createSale");
        ActionEvent event = null;
        SaleView instance = new SaleView();
        instance.createSale(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of postValidateSale method, of class SaleView.
     */
    @Test
    public void testPostValidateSale() {
        System.out.println("postValidateSale");
        ComponentSystemEvent event = null;
        SaleView instance = new SaleView();
        instance.postValidateSale(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of delete method, of class SaleView.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");
        SalePK saleKey = null;
        SaleView instance = new SaleView();
        instance.delete(saleKey);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of actionListener method, of class SaleView.
     */
    @Test
    public void testActionListener() {
        System.out.println("actionListener");
        Sale selectedSale = null;
        SaleView instance = new SaleView();
        String expResult = null;
        String result = instance.actionListener(selectedSale);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeSaleHasProduct method, of class SaleView.
     */
    @Test
    public void testRemoveSaleHasProduct() {
        System.out.println("removeSaleHasProduct");
        SaleHasProducto saleHasProduct = null;
        SaleView instance = new SaleView();
        instance.removeSaleHasProduct(saleHasProduct);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateAddSaleHasProduct method, of class SaleView.
     */
    @Test
    public void testUpdateAddSaleHasProduct() {
        System.out.println("updateAddSaleHasProduct");
        SaleView instance = new SaleView();
        instance.updateAddSaleHasProduct();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteUpdateSaleHasMenu method, of class SaleView.
     */
    @Test
    public void testDeleteUpdateSaleHasMenu() {
        System.out.println("deleteUpdateSaleHasMenu");
        SaleHasMenu saleHasMenu = null;
        SaleView instance = new SaleView();
        instance.deleteUpdateSaleHasMenu(saleHasMenu);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addUpdateSaleHasMenu method, of class SaleView.
     */
    @Test
    public void testAddUpdateSaleHasMenu() {
        System.out.println("addUpdateSaleHasMenu");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.addUpdateSaleHasMenu(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of update method, of class SaleView.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        SaleView instance = new SaleView();
        String expResult = "failure";
        String result = instance.update();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteImage method, of class SaleView.
     */
    @Test
    public void testDeleteImage() {
        System.out.println("deleteImage");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.deleteImage(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of preRenderView method, of class SaleView.
     */
    @Test
    public void testPreRenderView() {
        System.out.println("preRenderView");
        ComponentSystemEvent event = null;
        SaleView instance = new SaleView();
        instance.preRenderView(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addSaleHasProductoBusiness method, of class SaleView.
     */
    @Test
    public void testAddSaleHasProductoBusiness() {
        System.out.println("addSaleHasProductoBusiness");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.addSaleHasProductoBusiness(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of changeDate method, of class SaleView.
     */
    @Test
    public void testChangeDate() {
        System.out.println("changeDate");
        SelectEvent event = null;
        SaleView instance = new SaleView();
        instance.changeDate(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of saveBusinessSale method, of class SaleView.
     */
    @Test
    public void testSaveBusinessSale() {
        System.out.println("saveBusinessSale");
        ActionEvent event = null;
        SaleView instance = new SaleView();
        instance.saveBusinessSale(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getBusinessSaleList method, of class SaleView.
     */
    @Test
    public void testGetBusinessSaleList() {
        System.out.println("getBusinessSaleList");
        SaleView instance = new SaleView();
        List<EdenLayout<Sale>> expResult = null;
        List<EdenLayout<Sale>> result = instance.getBusinessSaleList();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of deleteBusinessSales method, of class SaleView.
     */
    @Test
    public void testDeleteBusinessSales() {
        System.out.println("deleteBusinessSales");
        AjaxBehaviorEvent event = null;
        SalePK key = null;
        SaleView instance = new SaleView();
        instance.deleteBusinessSales(event, key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of updateBusinessSale method, of class SaleView.
     */
    @Test
    public void testUpdateBusinessSale() {
        System.out.println("updateBusinessSale");
        SaleView instance = new SaleView();
        instance.updateBusinessSale();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeBusinessSaleHasProduct method, of class SaleView.
     */
    @Test
    public void testRemoveBusinessSaleHasProduct() {
        System.out.println("removeBusinessSaleHasProduct");
        AjaxBehaviorEvent event = null;
        SaleHasProducto key = null;
        SaleView instance = new SaleView();
        instance.removeBusinessSaleHasProduct(event, key);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeBusinessSalePicture method, of class SaleView.
     */
    @Test
    public void testRemoveBusinessSalePicture() {
        System.out.println("removeBusinessSalePicture");
        AjaxBehaviorEvent event = null;
        SaleView instance = new SaleView();
        instance.removeBusinessSalePicture(event);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
